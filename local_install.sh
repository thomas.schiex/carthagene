#!/bin/bash


# What to output into ~/.bashrc
function RC_BASH() {
	cat - <<EOD

##
# CarthaGene local install
#
export PATH=$WD/bin:\$PATH

EOD
}


# What to output into ~/.cshrc
function RC_CSH() {
	cat - <<EOD

##
# CarthaGene local install
#
setenv PATH $WD/bin:\$PATH

EOD
}



# Check the current state :
# - PATH not set at all
# - PATH set but not updated in current session
# - PATH set and up-to-date
function check_path() {
	test $WD == /usr && return 0		# /usr means system-wide install, so do nothing and don't complain
	echo $PATH|grep $WD > /dev/null 2>&1
	RET=$?
	if test "x$1" == "x"; then
		return $RET			# If we don't know how to handle the shell configuration, there's no point in performing the following tests.
	fi
	if grep "CarthaGene local install" $1 > /dev/null 2>&1; then
		if test $RET != 0; then
			grep "PATH.$WD" $1 > /dev/null 2>&1
			RET=$?
		fi
		if test $RET == 0; then
			echo -e "CarthaGene has already been successfully installed locally in $WD.\nPlease enter the command \"source $1\" or start a new session to start using it."
			return 0
		else
			echo -e "CarthaGene seems to have been previously installed locally in another directory.\nPlease remove the lines '# CarthaGene local install' and following from your $1 and re-run this script."
			return 0
		fi
	fi
	return $RET
}

# Get the script directory
WD=`cd $(dirname $0); pwd`

# Get the shell name
SH=`basename $SHELL`

# Setup environment to tweak (ba)?sh
if [ "$SH" == "sh" -o "$SH" == "bash" ]; then
	SH_NAME="Bourne Shell"
	RC_=RC_BASH
	RC_FILE=~/.bashrc
fi

# Setup environment to tweak zsh
if [ "$SH" == "zsh" ]; then
	SH_NAME="Z-Shell"
	RC_=RC_BASH
	RC_FILE=~/.zshrc
fi

# Setup environment to tweak t?csh
if [ "$SH" == "csh" -o "$SH" == "tcsh" ]; then
	SH_NAME="C-Shell"
	RC_=RC_CSH
	RC_FILE=~/.cshrc
fi

# DEBUG ONLY
#check_path $RC_FILE; echo $?; exit

# Update the hardcoded pathes in bin/carthagene and share/cgsh/cgbootstrap.tcl
sed -i "s,/usr/,$WD/,g" $WD/bin/carthagene
sed -i "s,/usr/,$WD/,g" $WD/share/carthagene/cgsh/cgbootstrap.tcl
sed -i "s,/usr/,$WD/,g" $WD/share/carthagene/gui/CGShellWidget.tcl

# Now for the big hack
if check_path $RC_FILE; then
	# All done, just sit there and be nice.
	echo
else
	# If we detected the shell correctly, append to the ~/.*rc file
	if test "x$SH_NAME" != "x"; then
		echo "Detected a $SH_NAME."
		echo "Setting up PATH for CarthaGene in your $RC_FILE"
		$RC_ >> $RC_FILE
	else
		echo "I don't know how to automatically and permanently setup the PATH for CarthaGene in your current shell ($SHELL). Please update \$PATH as follows :"
		echo "PATH=$WD/bin:\$PATH"
	fi
fi

echo "Thank you for using CarthaGene."

