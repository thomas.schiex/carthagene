#ifndef _CG_TWOPOINT_MATRICES_H_
#define _CG_TWOPOINT_MATRICES_H_

/*#include "config.h"*/
//#define SWAP_IS_FIFO	/* dumb but faster than LRU */
//#define CAN_SWITCH_BACKEND_AT_RUNTIME
/*#include "../matrix/matrix.h"*/

#include "../parallel/parallel.h"
/*#include <semaphore.h>*/

#if defined(WITH_PARALLEL)/* && !defined(WIN32)*/
#	define SYNCHRONIZATION_TRAITS SYNCHRONIZATION_TRAITS
#   include "sync_traits.h"
#else
#	undef SYNCHRONIZATION_TRAITS
//#	warning "NOT USING parallel"
#endif

#include "../matrix/matrix_new.h"

#ifndef SYNCHRONIZATION_TRAITS
typedef matrix::sync::dummy SYNCHRONIZATION_TRAITS;
#endif

using namespace matrix;
using namespace data;
using namespace addressing_space;

//static const int64_t fence_marker = { 0xFFFFFFFFFFFFFFFFLL };	/* -nan for a double TODO: should make that a signalling NaN */

//using matrix::data::fence_resource_type;


class BioJeu;

struct twopoint_traits {
	struct data_type {
		double LOD;
		double FR;
		data_type& operator=(const data_type&d) { LOD=d.LOD; FR=d.FR; return *this; }
		data_type() : LOD(0), FR(0) {}
	};
	typedef BioJeu* context_type;
#if 0
	struct fence_type {
		int64_t fence;
		fence_resource_type fres;
	};
	struct fence_handler_type {
		bool is_fenced(data_type*d) {
			return ((fence_type*)d)->fence==fence_marker;
		}
		void fence(fence_resource_type res, data_type*d) {
			fence_type*f = (fence_type*) d;
			f->fence = fence_marker;
			f->fres = res;
		}
		fence_resource_type get_resource(data_type*d) const { return ((fence_type*)d)->fres; }
	};
#endif
	class pagefile_name_provider_type {
		private:
			std::string n;
		public:
			pagefile_name_provider_type(context_type context);
			operator const char*() const {
				return n=="" ? NULL : n.c_str();
			}
	};
	static const bool delete_pagefile_when_done = false;	/* we want to keep previously computed two-point data */
	struct computation_handler_type {
		static void compute(context_type c, int i, int j, data_type&d) {
			d.LOD = c->ComputeOneTwoPoints(c->IndMarq_bj2cg[j], c->IndMarq_bj2cg[i], c->Epsilon2, &d.FR);
		}
		static bool must_compute(context_type c, data_type&d) { return d.LOD==0&&d.FR==0; }
	};
	static void clear_data(data_type&d) { d.LOD=d.FR=0; }
};

static inline std::ostream& operator << (std::ostream&o, const twopoint_traits::data_type& d) {
	o << "{ " << d.LOD << ", " << d.FR << " }";
	return o;
}


#if 0
namespace matrix {
	namespace addressing_space {
		struct TwoPointAddressingSpace : public TriangularWithoutMainDiag<off_by_one> {
			TwoPointAddressingSpace(int r, int c) :
				TriangularWithoutMainDiag<off_by_one>(r, c)
			{}
		};
	}

	template <typename M>
		struct configurable_fetcher_impl< M, TwoPointAddressingSpace > {
			static const typename M::reference_type const_get(M*m, int i, int j) {
				static typename M::data_type nil;
				/*std::cerr << "triangularW/Omaindiag get(i,j)" << std::endl;*/
				if(i==0||j==0||i==j) {
					return data_reference_selector<M>
							::create(m, (offset_type)-1, nil);
				}
				return m->backend_get(i, j);
			}
			static typename M::reference_type get(M*m, int i, int j) {
				static typename M::data_type nil;
				/*std::cerr << "triangularW/Omaindiag get(i,j)" << std::endl;*/
				if(i==0||j==0||i==j) {
					return data_reference_selector<M>
							::create(m, (offset_type)-1, nil);
				}
				return m->backend_get(i, j);
			}
		};
}
#endif

typedef Matrix<	twopoint_traits,
				TriangularWithoutMainDiag<off_by_one>,
				//storage::buffer_or_file>
				storage::pagefile
				/*storage::buffer*/
				>
		two_point_matrix;


namespace TwoPoint {
	class Matrices {
		private:
			two_point_matrix m;
            double (*DHfunc)(double);
		public:
			Matrices(BioJeu*bj);
			~Matrices() {
                    /*std::cerr << "Destroying Matrices instance" << std::endl;*/
			}
			double getLOD(int i, int j) { if(!(i&&j)) { return 0; } return m[i][j].LOD; }
			double getFR(int i, int j) { if(!(i&&j)) { return 0; } return m[i][j].FR; }
			double getDH(int i, int j) { if(!(i&&j)) { return DHfunc(0); } return DHfunc(m[i][j].FR); }
			int computation_count() const { /* TODO implement that */ return 0; }
	};
}


#endif

