//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_IC_parallel.cc,v 1.1.2.1 2012-09-06 07:09:34 dleroux Exp $
//
// Description : BJS_IC_parallel.
// Divers :
//-----------------------------------------------------------------------------

#include "BJS_IC_parallel.h"
#include "Genetic.h"
#include "System.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

namespace parallel_code {

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_IC_parallel::BJS_IC_parallel() : BioJeuSingle()
{
	/*for (int i = 0; i < 4; i++) {*/
		/*SourceTo[i] = NULL;*/
		/*ToSink[i] = NULL;*/
	/*}*/
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - le type
// - le nombre de marqueurs
// - la taille de l'echantillon
// - le champ de bit
// - la matrice de l'�chantillon
// Valeur de retour :
//-----------------------------------------------------------------------------

BJS_IC_parallel::BJS_IC_parallel(CartaGenePtr cartag,
		int id,
		charPtr nomjeu,
		CrossType cross,
		int nm, 
		int taille,
		int bitjeu,
		int *indmarq,
		Obs **echantil)
:BioJeuSingle(cartag,
		id,
		nomjeu,
		cross, 
		nm, 
		taille, 
		bitjeu, 
		indmarq, 
		echantil)
{

	/*for (int i = 0; i < 4; i++) {*/
		/*SourceTo[i] = NULL;*/
		/*ToSink[i] = NULL;*/
	/*}*/

	NbMeiose = 2*TailleEchant;

	ComputeTwoPoints();
}

	BJS_IC_parallel::BJS_IC_parallel(const BJS_IC_parallel& b)
: BioJeuSingle(b)
{
	/*for (int i = 0; i < 4; i++) {*/
		/*SourceTo[i] = NULL;*/
		/*ToSink[i] = NULL;*/
	/*}*/

	NbMeiose = 2*TailleEchant;

	ComputeTwoPoints();
}


//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_IC_parallel::~BJS_IC_parallel()
{
}

//-----------------------------------------------------------------------------
// Compatibilite des observations entres individus
//-----------------------------------------------------------------------------
int BJS_IC_parallel::Compatible(int numarq1, int numarq2) const
{
	int i;
	enum Obs Obs1, Obs2;

	for (i = 1; i<= TailleEchant; i++) {
		Obs1 = GetEch(numarq1,i);
		Obs2 = GetEch(numarq2,i);
		if (!(Obs1 & Obs2))  return 0;
	}
	return 1;
}

//-----------------------------------------------------------------------------
// Fusion des observations entres individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
//-----------------------------------------------------------------------------
void BJS_IC_parallel::Merge(int numarq1, int numarq2) const
{
	int i;

	// les deux marqueurs sont ils definis dans le jeux de donnees ?
	if ((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu & BitJeu))
	{
		// Oui, on va fusionner dans le premier
		for (i = 1; i<= TailleEchant; i++) {
			/*Echantillon[IndMarq[numarq1]][i] = */
				/*Obs(Echantillon[IndMarq[numarq2]][i] & Echantillon[IndMarq[numarq2]][i]);*/
			Echantillon[numarq1][i] = 
				Obs(Echantillon[numarq2][i] & Echantillon[numarq2][i]);
		}
	}
}

//-----------------------------------------------------------------------------
//  Calcul de la probabilite de passage par une arete suivant la paire de 
// genotypes relies
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux genotypes, la proba de rec
// - 
// Valeur de retour : la proba
//-----------------------------------------------------------------------------

#include <string>

inline double BJS_IC_parallel::ProbArete(int a, int b, double p) const
{
	/* DL
	 * attempt to use pow() instead of the loops, just to see the speed difference
	 * seems it's twice faster this way. FIXME : check that the new formula is right.
	 */
	//*
	double pprob, ptmp;
	// first attempt, naively rephrase the loops
	//prob = pow(p, NCross[a][b]);
	//if(NCross[a][b]<2) {
	//      prob *= pow(1.-p, 2-NCross[a][b]);
	//}
	// second attempt using a switch (some wicked case of loop unrolling)
	switch(NCross[a][b]) {
		case 0:
			ptmp=1.-p;
			pprob = ptmp*ptmp;
			break;
		case 1:
			pprob = p*(1.-p);
			break;
		default:
			pprob = pow(p, NCross[a][b]);
	};
	return pprob;
	/*/
	  double prob = 1.0;

	  for (int n = 0; n < NCross[a][b]; n++)  prob *= p;
	  for (int n = NCross[a][b]; n < 2; n++)  prob *= (1.0-p);

	//  if(prob!=pprob) {
	//          printf("with NCross=%i, got %lf vs %lf\n", NCross[a][b], pprob, prob);
	//          throw std::string();
	//  }

	return prob;
	//*/
}

//-----------------------------------------------------------------------------
// Retourne l'esp�rance du nombre de recombinants.
//-----------------------------------------------------------------------------
// Param�tres :
// - indice du premier marqueur dans le jeu
// - indice du second marqueur dans le jeu
// - le theta
// - le nombre de donn�es informatives
// - le loglike (o)
// Valeur de retour : l'esp�rance du nombre de recombinaisons.
//-----------------------------------------------------------------------------

double BJS_IC_parallel::EspRec(int m1, 
		int m2, 
		double theta, 
		double *loglike) const
{
	int NbEdges[16][16];
	for (int i = 0; i < 16; i++)
		for (int j = 0; j < 16; j++) NbEdges[i][j] = 0;

	for (int i = 1; i <= TailleEchant; i++)
		++NbEdges[GetEch(m1, i)][GetEch(m2, i)];
	return EspRec(NbEdges, theta, loglike);
}

double BJS_IC_parallel::EspRec(int NbEdges[16][16], double theta, double *loglike) const
{
	int i,j,sl,sr;
	double ENRec,LogLike, prob, inv_prob;
	double ProbCross[3];

	LogLike = 0.0;
	ENRec = 0.0;

	for (i = 1; i < 15; ++i) //observation a gauche, pas les inconnus
		for (j = 1; j < 15; ++j) //observation a droite, pas les inconnus

			// Si il existe des occurrences de ce type de paires d'observ.
			if (NbEdges[i][j])
			{
				ProbCross[2] = ProbCross[1] = ProbCross[0] = 0.0;
				// on calcule les proba de 0,1,2 recombinaisons
				for (sl = 0; sl< NPossibles[i]; ++sl)
					for (sr= 0; sr < NPossibles[j]; ++sr)
					{
						int pl=Possibles[i][sl];
						int pr=Possibles[j][sr];
						ProbCross[NCross[pl][pr]] 
							+= ProbArete(pl,
									pr, theta);
						/*ProbCross[NCross[Possibles[i][sl]][Possibles[j][sr]]] */
						/*+= ProbArete(Possibles[i][sl],*/
						/*Possibles[j][sr], theta);*/
					}

				// on en deduit l'esperance du nombre de rec. et donc l'estime
				// de la fraction de rec. et la vraisemblance 2 pt

				prob = ProbCross[2]+ ProbCross[1]+ ProbCross[0];
				/* DL
				 * precompute inverse to remove two divisions
				 */
				inv_prob = 1./prob;
				ProbCross[0] *= inv_prob;
				ProbCross[1] *= inv_prob;
				ProbCross[2] *= inv_prob;

				ENRec += NbEdges[i][j]*(ProbCross[2]*2 +  ProbCross[1]);
				LogLike += log10(prob)*NbEdges[i][j];
			}

	*loglike = LogLike;

	return ENRec;
}
//-----------------------------------------------------------------------------
// Calcule le nombre de donn�es et 
// le log sous l'hypothese d'independance
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux numeros de marqueurs, on doit avoir m1< m2
// - le nombre de donn�es
// Valeur de retour : le log
//-----------------------------------------------------------------------------

double BJS_IC_parallel::LogInd(int m1, int m2, int *nbdata) const {
	int NbEdges[16][16];
	return LogInd(m1, m2, NbEdges, nbdata);
}

double BJS_IC_parallel::LogInd(int m1, 
		int m2,
		int NbEdges[16][16],
		int *nbdata) const
{
	// DL mutualise NbEdges avec EspRec
	/*int NbEdges[16][16];*/
	int data = 0; // pairs of non unknown
	//int i,j,sl,sr;
	double LogLike;
	double ProbCross[3];

	for (int i = 0; i < 16; i++)
	   for (int j = 0; j < 16; j++) NbEdges[i][j] = 0;

	for (int i = 1; i <= TailleEchant; i++) {
		++NbEdges[GetEch(m1, i)][GetEch(m2, i)];
		data += ((GetEch(m1, i) != Obs1111) && (GetEch(m2, i) != Obs1111));
	}

	// On repete le meme calcul en fixant theta a 0.5 (hypothese non lies)
	// bien sur, inutile d'iterer
	LogLike = 0.0;

	for (int i = 1; i < 15; ++i) //observation a gauche
		for (int j = 1; j < 15; ++j) //observation a droite

			// Si il existe des occurrences de ce type de paires d'observ.
			if (NbEdges[i][j])
			{
				ProbCross[2] = ProbCross[1] = ProbCross[0] = 0.0;
				// on calcule les proba de 0,1,2 recombinaisons
				for (int sl = 0; sl < NPossibles[i]; ++sl)
					for (int sr = 0; sr < NPossibles[j]; ++sr)
					{
						ProbCross[NCross[Possibles[i][sl]][Possibles[j][sr]]] 
							+= ProbArete(Possibles[i][sl],
									Possibles[j][sr], 0.5);
					}

				// on en deduit la vraisemblance 2 pt
				LogLike += log10(ProbCross[2]+ ProbCross[1]+ ProbCross[0]) *
					NbEdges[i][j];
			}

	*nbdata = data<<1;
	return LogLike;
}

//-----------------------------------------------------------------------------
// Calcule fraction de rec. et
// LOD scores pour deux marqueurs
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux numeros de marqueurs, on doit avoir m1< m2
// - epsilon
// - la fraction de reconbinant(i,o)
// Valeur de retour : le lod
//-----------------------------------------------------------------------------

double BJS_IC_parallel::ComputeOneTwoPoints(int m1, 
		int m2, 
		double epsilon,
		double *fr) const
{
	int data = 0; // pairs of non unknown
	// DL supprime une variable locale
	double /*ENRec,*/LogLike,LogLikeInd, PrevLogLike,theta;
	int NbEdges[16][16];

	LogLikeInd = LogInd(m1, m2, NbEdges, &data);

	if (!data)  {
		*fr = EM_MAX_THETA;
		return 0.0;
	}

	theta = 0.05;
	LogLike = 0.0;
	// DL précalcule une division
	double inv_data = 1./data;

	do {
		PrevLogLike = LogLike;

		// DL supprime une variable locale et une s�rie de divisions
		/*ENRec = EspRec(m1, m2, theta, &LogLike);*/
		/*theta = ENRec/(data); */
		theta = EspRec(NbEdges, theta, &LogLike)*inv_data; 
		// DL test sans effet de bord sorti de la boucle
		if (theta > EM_MAX_THETA) theta= EM_MAX_THETA;
	}   while (fabs(LogLike-PrevLogLike) > epsilon);

	*fr = theta;

	return LogLike - LogLikeInd;
}

//-----------------------------------------------------------------------------
//  Etape d' � Expectation � de EM.
//-----------------------------------------------------------------------------
// Param�tres : la carte
// - la carte
// - le vecteur d'Expectation(i,o)
// Valeur de retour : le loglike 
//-----------------------------------------------------------------------------

#if 0
//#define PAREXP_USE_LOCK
struct _par_exp {
    Parallel::Spin ll_spin;
    Parallel::Spin acc_spin;
    double loglike;
    BJS_IC_parallel* bj;
    const Carte* map;
    double* expected;
    size_t row_size;
    size_t col_size;
    double* tmp_expected;
    _par_exp(BJS_IC_parallel* _, const Carte* m, double* e)
        : loglike(0), bj(_), map(m), expected(e)
    {
        bj->UpdateEStepArrays(map);
        row_size = 1+map->NbMarqueur;
        col_size = bj->TailleEchant;
#ifndef PAREXP_USE_LOCK
        tmp_expected = new double[row_size*col_size];
        /*memset(tmp_expected, 0, sizeof(double)*row_size*col_size);*/
#endif
    }
    ~_par_exp() {
#ifndef PAREXP_USE_LOCK
        /*memset(expected, 0, sizeof(double)*row_size);*/
        for(size_t j=0;j<row_size;++j) {
            expected[j] = tmp_expected[j];
        }
        for(size_t i=1;i<col_size;++i) {
            for(size_t j=0;j<row_size;++j) {
                expected[j] += tmp_expected[i*row_size+j];
            }
        }
        delete tmp_expected;
#endif
    }
    void operator()(size_t i) {
        std::vector<double> ToSink[4];
        std::vector<double> SourceTo[4];
        size_t nMarq = map->NbMarqueur;
        ToSink[0].resize(nMarq);
        ToSink[1].resize(nMarq);
        ToSink[2].resize(nMarq);
        ToSink[3].resize(nMarq);
        ToSink[0][nMarq-1] = 0;
        ToSink[1][nMarq-1] = 0;
        ToSink[2][nMarq-1] = 0;
        ToSink[3][nMarq-1] = 0;
        SourceTo[0].resize(nMarq);
        SourceTo[1].resize(nMarq);
        SourceTo[2].resize(nMarq);
        SourceTo[3].resize(nMarq);
        SourceTo[0][0] = 0;
        SourceTo[1][0] = 0;
        SourceTo[2][0] = 0;
        SourceTo[3][0] = 0;
        double llincr = bj->ComputeSourceTo(SourceTo, i, map);
        ll_spin.lock();
        loglike += llincr;
        ll_spin.unlock();
        bj->ComputeToSink(ToSink, i, map);
#ifndef PAREXP_USE_LOCK
        bj->ComputeOneExpected(acc_spin, SourceTo, ToSink, i, map, tmp_expected+(row_size*(i-1)));
#else
        bj->ComputeOneExpected(acc_spin, SourceTo, ToSink, i, map, expected);
#endif
    }
};
#endif
/* DL EXPERIMENTAL CODE ! */

//struct par_exp : public BJS_IC_parallel::PCEType {
//    par_exp(BJS_IC_parallel* bj, const Carte* map, double* exp)
//        : BJS_IC_parallel::PCEType(bj, map, exp)
//    { bj->UpdateEStepArrays(map); }
//};

double BJS_IC_parallel::ComputeExpected(const Carte* map, double *expected)
{
#if 1
    /*tbb::task_scheduler_init init(Parallel::WorkerPoolManager::getcount());*/
    /*std::cerr << "BJS_IC_parallel::ComputeExpected" << std::endl;*/
    //_par_exp parallel_expected(this, map, expected);
    UpdateEStepArrays(map);
    BJS_IC_parallel::PCEType parallel_expected(this, map, expected);

    Parallel::Range<>().execute_slices(parallel_expected, 1, TailleEchant+1, 1);

    return parallel_expected.loglike;

#else
    Parallel::Spin spin;
	double loglike = 0.0;
	UpdateEStepArrays(map);	// inserted for speedup, CN 1.18.06

    std::vector<double> ToSink[4];
    std::vector<double> SourceTo[4];

    size_t nMarq = map->NbMarqueur;

    ToSink[0].resize(nMarq);
    ToSink[1].resize(nMarq);
    ToSink[2].resize(nMarq);
    ToSink[3].resize(nMarq);
    ToSink[0][nMarq-1] = 0;
    ToSink[1][nMarq-1] = 0;
    ToSink[2][nMarq-1] = 0;
    ToSink[3][nMarq-1] = 0;
    SourceTo[0].resize(nMarq);
    SourceTo[1].resize(nMarq);
    SourceTo[2].resize(nMarq);
    SourceTo[3].resize(nMarq);
    SourceTo[0][0] = 0;
    SourceTo[1][0] = 0;
    SourceTo[2][0] = 0;
    SourceTo[3][0] = 0;

	for (int i = 1 ; i <= TailleEchant; i++)  {
		loglike += ComputeSourceTo(SourceTo, i, map);
		ComputeToSink(ToSink, i, map);
		ComputeOneExpected(spin, SourceTo, ToSink, i, map, expected);
	}
	// printf("loglike = %g\n",loglike);

	return (loglike);
#endif
}


//-----------------------------------------------------------------------------
//  Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite gauche (source)
//-----------------------------------------------------------------------------
// Param�tres :
// - l'individu 
// - la carte 
// Valeur de retour : le loglike 
// Added/renamed some variables & substituted a lookup for calculation, CN 1.18.06
//-----------------------------------------------------------------------------

double BJS_IC_parallel::ComputeSourceTo(std::vector<double>*SourceTo, int Ind, const Carte *map) const
{
	int i, marq, leftGeno, rightGeno, leftPheno, rightPheno, possibleL, possibleR;
	int nCross;
	int *ordre = map->ordre;
	double norm = 0.0, loglike = 0.0;
	double *probLookups;

	rightPheno = GetEch(ordre[0], Ind);

	for (i = 0; i< NPossibles[rightPheno]; i++)
		SourceTo[Possibles[rightPheno][i]][0] = 0.25;

	norm = SourceTo[0][0]+SourceTo[1][0]+SourceTo[2][0]+SourceTo[3][0];
	loglike += log10(norm);

    // DL : precompute 1/norm and multiply instead of dividing
    norm = 1./norm;
	/*SourceTo[0][0] /= norm;*/
	/*SourceTo[1][0] /= norm;*/
	/*SourceTo[2][0] /= norm;*/
	/*SourceTo[3][0] /= norm;*/
	SourceTo[0][0] *= norm;
	SourceTo[1][0] *= norm;
	SourceTo[2][0] *= norm;
	SourceTo[3][0] *= norm;

	for (marq = 1; marq < map->NbMarqueur; marq++) {
		SourceTo[0][marq] = 0.0;
		SourceTo[1][marq] = 0.0;
		SourceTo[2][marq] = 0.0;
		SourceTo[3][marq] = 0.0;

		leftPheno = rightPheno;
		rightPheno = GetEch(ordre[marq], Ind);
		probLookups = mProbLookups[marq - 1];

		for (leftGeno = 0; leftGeno < NPossibles[leftPheno]; leftGeno++)
		{
			possibleL = Possibles[leftPheno][leftGeno];
			for (rightGeno = 0; rightGeno < NPossibles[rightPheno]; rightGeno++)
			{
				possibleR = Possibles[rightPheno][rightGeno];
				nCross = NCross[possibleL][possibleR];
				SourceTo[possibleR][marq] += 
					SourceTo[possibleL][marq -1] *
					probLookups[nCross]; // lookup replaces calculation CN 1.18.06
			}
		}    
		norm = SourceTo[0][marq]+SourceTo[1][marq]+SourceTo[2][marq]+SourceTo[3][marq];
		loglike += log10(norm);

        // DL : precompute 1/norm and multiply instead of dividing
        norm = 1./norm;
		/*SourceTo[0][marq] /= norm;*/
		/*SourceTo[1][marq] /= norm;*/
		/*SourceTo[2][marq] /= norm;*/
		/*SourceTo[3][marq] /= norm;*/
		SourceTo[0][marq] *= norm;
		SourceTo[1][marq] *= norm;
		SourceTo[2][marq] *= norm;
		SourceTo[3][marq] *= norm;
	}

	return loglike;
}

//-----------------------------------------------------------------------------
// Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite droite (puit)
//-----------------------------------------------------------------------------
// Param�tres : 
// - l'individu
// - la carte 
// Valeur de retour : 
// Some variables given more informative names by CN 1.18.06
//-----------------------------------------------------------------------------

double BJS_IC_parallel::ComputeToSink(std::vector<double>*ToSink, int Ind, const Carte *map) const
{
	int i, marq, nmarq = map->NbMarqueur;
	int leftGeno, rightGeno, leftPheno, rightPheno,  possibleL, possibleR, nCross;
	int *ordre = map->ordre;
	double norm = 0.0, loglike = 0.0;
	double *probLookups;

	leftPheno = GetEch(ordre[nmarq - 1], Ind);

	// initialize start state array with CPs of genotype given phenotype
	for (i = 0; i< NPossibles[leftPheno]; i++)
		ToSink[Possibles[leftPheno][i]][nmarq - 1] = 0.25;

	norm = ToSink[0][nmarq-1]+ToSink[1][nmarq-1]+
		ToSink[2][nmarq-1]+ToSink[3][nmarq-1];
	loglike += log10(norm);

    // DL : precompute 1/norm, replace divisions by multiplications
    norm = 1./norm;
	ToSink[0][nmarq-1] *= norm;
	ToSink[1][nmarq-1] *= norm;
	ToSink[2][nmarq-1] *= norm;
	ToSink[3][nmarq-1] *= norm;

    /*norm = 1/norm;*/
	/*ToSink[0][nmarq-1] *= norm;*/
	/*ToSink[1][nmarq-1] *= norm;*/
	/*ToSink[2][nmarq-1] *= norm;*/
	/*ToSink[3][nmarq-1] *= norm;*/

	for (marq = (nmarq - 2); marq >=0; marq--) {
		ToSink[0][marq] = 0.0;
		ToSink[1][marq] = 0.0;
		ToSink[2][marq] = 0.0;
		ToSink[3][marq] = 0.0;

		rightPheno = leftPheno;
		leftPheno = GetEch(ordre[marq], Ind);
		probLookups = mProbLookups[marq];

		for (rightGeno = 0; rightGeno < NPossibles[rightPheno]; rightGeno++)
		{
			possibleR = Possibles[rightPheno][rightGeno];
			for (leftGeno = 0; leftGeno < NPossibles[leftPheno]; leftGeno++)
			{
				possibleL = Possibles[leftPheno][leftGeno];
				nCross = NCross[possibleR][possibleL];

				ToSink[possibleL][marq] +=
					ToSink[possibleR][marq + 1] *
					probLookups[nCross]; // lookup replaces calculation CN 1.18.06
			}
		}    
		norm = ToSink[0][marq]+ToSink[1][marq]+ToSink[2][marq]+ToSink[3][marq];
		loglike += log10(norm);

        // DL : precompute 1/norm, replace divisions by multiplications
        norm = 1./norm;
		ToSink[0][marq] *= norm;
		ToSink[1][marq] *= norm;
		ToSink[2][marq] *= norm;
		ToSink[3][marq] *= norm;
	}

	return loglike;
}

//-----------------------------------------------------------------------------
//  Etape elementaire d' � Expectation � de EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - l'individu 
// - la carte
// - le vecteur d'expectation
// - 
// Valeur de retour : none
// CN has inserted some local variables for greater readability
// and substituted a fast lookup for calculation of crossover probs
//-----------------------------------------------------------------------------

void BJS_IC_parallel::ComputeOneExpected(
        BJS_IC_parallel::EM_state& state,
        int Ind, 
		BJS_IC_parallel::PCEType* pce) const
//void BJS_IC_parallel::ComputeOneExpected(
//        Parallel::Spin& spin_lock,
//        std::vector<double>* SourceTo,
//        std::vector<double>* ToSink,
//        int Ind, 
//		const Carte* map, 
//		double *expected) const
{
	int marq, nCross, possibleL, possibleR;
	int *ordre = pce->map->ordre;
	int rightPheno = GetEch(ordre[0], Ind);
	int leftPheno, leftGeno, rightGeno;
	double prob, *probLookups, probLR;

	double ProbCross[3];

	for (marq = 0; marq < pce->map->NbMarqueur - 1; marq++)
	{
		ProbCross[2] = ProbCross[1] = ProbCross[0] = 0.0;
		leftPheno = rightPheno;
		rightPheno = GetEch(ordre[marq + 1], Ind);
		probLookups = mProbLookups[marq];

		for (leftGeno = 0; leftGeno < NPossibles[leftPheno]; leftGeno++)
		{	// loop over possible genotype combinations (the hidden states)
			possibleL = Possibles[leftPheno][leftGeno];
			for (rightGeno = 0; rightGeno < NPossibles[rightPheno]; rightGeno++)
			{
				possibleR = Possibles[rightPheno][rightGeno];
				nCross = NCross[possibleL][possibleR];
				probLR = state.SourceTo[possibleL][marq] * state.ToSink[possibleR][marq + 1];
				prob = probLookups[nCross] * probLR;
				ProbCross[nCross] += prob;
			}
		}

		prob = 1./(ProbCross[2]+ ProbCross[1]+ ProbCross[0]);
		ProbCross[2] *= prob;
		ProbCross[1] *= prob;
        // DL : ProbCross[0] doesn't seem to be used past this point, so I disable the division
		/*ProbCross[0] /= prob;*/
        // DL : maybe the division by prob could be factored below, to speed it up a bit
		double tmp_score = ProbCross[2] + ProbCross[2] + ProbCross[1]; // DL : replaced a*2 by a+a
//#ifndef PAREXP_USE_LOCK
//		expected[marq] = tmp_score;
//#else
//        spin_lock.lock();
//		expected[marq] += tmp_score;
//        spin_lock.unlock();
//#endif
        pce->accumulate_expected(Ind, marq, tmp_score);
	}
}

//-----------------------------------------------------------------------------
//  Pr�paration des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_IC_parallel::PreparEM(const Carte *data)
{
	int nMarq = data->NbMarqueur;

	for (int i = 0; i < 4; i++) {
		/*SourceTo[i] = new double[nMarq];*/
		/*ToSink[i] = new double[nMarq];*/
		/*ToSink[i][nMarq-1] = 0;*/
		/*SourceTo[i][0] = 0;*/
	}	// following added by CN 1.18.06 for obviating ProbArete() calculations
	mProbLookups = new doublePtr[nMarq - 1];
	for (int marq = 0; marq < nMarq - 1; marq++)
		mProbLookups[marq] = new double[kMaxF2Crossovers + 1]; 

    /* FIXME: is it safe to move this call here ? Test shows it is NOT safe. Why ? */
    /* FIXED: d'oh! it's the difference between precomputing once and for all and computing at each E step in EM */
    /*UpdateEStepArrays(data);*/
}

//-----------------------------------------------------------------------------
//  nettoyage des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres :
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_IC_parallel::NetEM(const Carte *data)
{
	for (int i = 0; i < 4; i++) {
		/*delete[] SourceTo[i];*/
		/*delete[] ToSink[i];*/
	}	// following added by CN 1.18.06
	for (int marq = 0; marq < data->NbMarqueur - 1; marq++)
		delete [] mProbLookups[marq];
	delete [] mProbLookups;
}

/**
 * Added by CN for speed optimization: precalculation of probability terms in theta
 */
void BJS_IC_parallel::UpdateEStepArrays(const Carte *map)
{
	double *probs, theta, tcomp;

    /*std::cerr << "BJS_IC_parallel::UpdateEStepArrays" << std::endl;*/

	for (int marq = 0; marq < map->NbMarqueur - 1; marq++)
	{
		probs = mProbLookups[marq];
		theta = map->tr[marq];
		tcomp = 1.0 - theta;
		probs[0] = tcomp * tcomp;
		probs[1] = theta * tcomp;
		probs[2] = theta * theta;

        // DL : try and speed up
        /*tcomp = theta*theta;*/
        /*probs[2] = tcomp;*/
        /*probs[1] = theta-tcomp;*/
        /*probs[0] = 1.-theta-theta+tcomp;*/
	}
}

} /* namespace parallel_code */

