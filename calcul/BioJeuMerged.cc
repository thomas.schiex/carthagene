//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BioJeuMerged.cc,v 1.13.6.3 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Classe de base BioJeuMerged.
// Divers : 
//-----------------------------------------------------------------------------

#include "BioJeuMerged.h"
#include "BioJeuSingle.h"

#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "twopoint.h"

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BioJeuMerged::BioJeuMerged()
: BioJeu()
{
	BJgauche = NULL;
	BJdroite = NULL;
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres : 
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - type de jeu de donn�e(Mor)
// - nm nombre de marqueurs.
// - bitjeu champ de bit du jeu de donn�es.
// - gauche jeu de donn�es.
// - droite jeu de donn�es. 
//-----------------------------------------------------------------------------

BioJeuMerged::BioJeuMerged(CartaGenePtr cartag,
		int id,
		CrossType cross,
		int nm, 
		int bitjeu,
		BioJeu *gauche, 
		BioJeu *droite)
: BioJeu(cartag, id, cross, nm, bitjeu)
{

	TailleEchant = gauche->TailleEchant + droite->TailleEchant;

	BJgauche = gauche;
	BJdroite = droite;

	Em_Max_Theta = gauche->Em_Max_Theta;
	Em_Min_Theta = gauche->Em_Min_Theta;

	twopoint = new TwoPoint::Matrices(this);
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BioJeuMerged::~BioJeuMerged()
{  

}

//-----------------------------------------------------------------------------
// Compatibilite des echantillons
//-----------------------------------------------------------------------------
inline int BioJeuMerged::Compatible(int numarq1, int numarq2) const
{
	return (BJgauche->Compatible(numarq1, numarq2) &&
			BJdroite->Compatible(numarq1, numarq2));
}

//-----------------------------------------------------------------------------
// Fusion des observations entres individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
//-----------------------------------------------------------------------------
void BioJeuMerged::Merge(int numarq1, int numarq2) const
{
	BJgauche->Merge(numarq1, numarq2);
	BJdroite->Merge(numarq1, numarq2);
}

//-----------------------------------------------------------------------------
// Affichage des donn�es
//-----------------------------------------------------------------------------

void BioJeuMerged::DumpEch(void) const
{
	int i = 0, k = 0;

	// Nom des marqueurs et les observations.
	while (i < NbMarqueur) 
	{
		if (Cartage->markers[k].BitJeu & BitJeu) {
			i++;
			print_out( "%10s : ", Cartage->markers.keyOf(k).c_str());
			DumpEchMarq(k);
			print_out(  "\n");
		}
		k++;
	}
}

//-----------------------------------------------------------------------------
// Affichage des donn�es pour un marqueur
//-----------------------------------------------------------------------------
// Param�tres : 
// - le num�ro d'un marqueur
// - 
// Valeur de retour : 
//----------------------------------------------------------------------------

void BioJeuMerged::DumpEchMarq(int numarq) const
{

	BJgauche->DumpEchMarq(numarq);
	BJdroite->DumpEchMarq(numarq);

}

//-----------------------------------------------------------------------------
// Retourne l'esp�rance du nombre de recombinants.
//-----------------------------------------------------------------------------
// Param�tres :
// - indice du premier marqueur dans le jeu
// - indice du second marqueur dans le jeu
// - le theta
// - le loglike (o)
// Valeur de retour : l'esp�rance du nombre de recombinants.
//-----------------------------------------------------------------------------

double BioJeuMerged::EspRec(int m1, 
		int m2, 
		double theta, 
		double *loglike) const
{

	double enrec_g, enrec_d;
	double loglike_g = *loglike, loglike_d = *loglike;

	enrec_g = BJgauche->EspRec(m1, m2, theta, &loglike_g);
	enrec_d = BJdroite->EspRec(m1, m2, theta, &loglike_d);

	*loglike = loglike_g + loglike_d;

	return enrec_g + enrec_d;
}

//-----------------------------------------------------------------------------
// Calcule le nombre de donn�es et 
// le log sous l'hypothese d'independance
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux numeros de marqueurs, on doit avoir m1< m2
// - le nombre de donn�es
// Valeur de retour : le log
//-----------------------------------------------------------------------------

double BioJeuMerged::LogInd(int m1, 
		int m2,
		int *nbdata) const
{

	int nbdata_g = 0, nbdata_d = 0;
	double loglike_g, loglike_d;

	loglike_g = BJgauche->LogInd(m1, m2, &nbdata_g);
	loglike_d = BJdroite->LogInd(m1, m2, &nbdata_d);

	*nbdata = nbdata_g + nbdata_d;
	return loglike_g + loglike_d;
}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les num�ro du couple
// - 
// Valeur de retour : le LOD 2 points
//----------------------------------------------------------------------------

double BioJeuMerged::GetTwoPointsLOD(int m1, int m2) const
{

	if (BJgauche->Couplex(m1,m2) &&
			BJdroite->Couplex(m1,m2))
		// DL
		return twopoint->getLOD(m1, m2);
		/*
		return TwoPointsLOD[IndMarq[m1 * ((Cartage->markers[m1].BitJeu 
					& BitJeu) > 0)]]
			[IndMarq[m2 * ((Cartage->markers[m2].BitJeu 
						& BitJeu) > 0)]];
		*/

	return (BJgauche->GetTwoPointsLOD(m1, m2)+
			BJdroite->GetTwoPointsLOD(m1, m2));

}

//-----------------------------------------------------------------------------
// Affichage de la matrice TwoPoints Lod
//-----------------------------------------------------------------------------

void BioJeuMerged::DumpTwoPointsLOD() const
{
	int i = 0, k = 0;
	int j = 0, l = 0;

	// Nom des marqueurs et les observations.
	while (i < NbMarqueur) 
	{
		if (Cartage->markers[k].BitJeu & BitJeu) 
		{
			i++;
			print_out( "%3d :", k);
			j = 0;
			l = 0;
			while (j < NbMarqueur) 
			{
				if (Cartage->markers[l].BitJeu & BitJeu) 
				{
					j++;
					print_out( "%5.2f ", GetTwoPointsLOD(k, l));
				}
				l++;
			}
			print_out(  "\n");
		}	
		k++;
	}   
}

//-----------------------------------------------------------------------------
// Affichage de la matrice TwoPoints FR
//-----------------------------------------------------------------------------

void BioJeuMerged::DumpTwoPointsFR() const
{
	int i = 0, k = 0;
	int j = 0, l = 0;

	// Nom des marqueurs et les observations.
	while (i < NbMarqueur) 
	{
		if (Cartage->markers[k].BitJeu & BitJeu) 
		{
			i++;
			print_out( "%3d :", k);
			j = 0;
			l = 0;
			while (j < NbMarqueur) 
			{
				if (Cartage->markers[l].BitJeu & BitJeu) 
				{
					j++;
					print_out( "%5.2f ", GetTwoPointsFR(k, l));
				}
				l++;
			}
			print_out(  "\n");
		}	
		k++;
	}   
}

