#ifndef _CARTHAGENE_DATASET_H
#define _CARTHAGENE_DATASET_H

#include <vector>
#include "carthagene.h"

class DataSet {
private:
    CartaGene* cg;
    BioJeu* bj;
    std::vector<int> MarkSelect;
    Tas* Heap;

public:
    DataSet(CartaGene* cg_, BioJeu* bj_, std::vector<int>& sel_)
        : cg(cg_)
        , bj(bj_)
        , MarkSelect(sel_)
        , Heap(new Tas())
    {
        Heap->Init(cg);
    }

    DataSet(CartaGene* cg_, BioJeu* bj_)
        : cg(cg_)
        , bj(bj_)
        , MarkSelect()
        , Heap(new Tas())
    {
        if(bj_->Cross != Con) {
            MarkerList::iterator mi=cg_->markers.begin(),
                                 mj=cg_->markers.end();
            for(;mi!=mj;++mi) {
                if(bitjeu&(*mi)->BitJeu) {
                    MarkSelect.push_back(mi.index());
                }
            }
        }
        Heap->Init(cg);
    }

    DataSet(const DataSet& ds)
        : cg(ds.cg)
        , bj(ds.bj)
        , MarkSelect(ds.MarkSelect)
        , Heap(new Tas())
    {
        Heap->Init(cg, ds.Heap.MaxHeapSize);
    }

    ~DataSet() {}


    static DataSet* fromFile(CartaGene* cg_, const char* filename) {
        char* freeme = cg_->CharJeu(filename);
        return new DataSet(cg_, cg_->dernierJeuCharge);
    }
};


#endif

