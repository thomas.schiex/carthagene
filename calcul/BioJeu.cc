//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// BioJeu.cc,v 1.42 2007/01/09 13:13:23 tschiex Exp
//
// Description : Classe de base BioJeu.
// Divers :
//-----------------------------------------------------------------------------

/*
#include "BioJeu.h"

#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>

#include "CartaGene.h"

#include "twopoint.h"
#include "ladj.h"

#include "../parallel/parallel.h"
 */

#include "carthagene_all.h"

#include <ext/rope>

namespace ext=__gnu_cxx;

double BioJeu::Epsilon1 = 0.1;
double BioJeu::Epsilon2 = 0.001;

void BioJeu::_updateIndMarq(int max) {
    int* nim = new int[max];
    int i;
    /* TODO : parcourir le tableau jusqu'à trouver IndMarq[i]==NbMarqueur(-1?), ça indiquera la fin */
    for(i=0;i<_indmarq_sz;++i) {
        nim[i]=IndMarq_cg2bj[i];
    }
    for(;i<max;++i) {
        nim[i]=0;
    }
    delete[] IndMarq_cg2bj;
    IndMarq_cg2bj = nim;
    _indmarq_sz = max;
}

unsigned int BioJeu::computation_count() const { return twopoint->computation_count(); }


//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BioJeu::BioJeu()
{
    Cartage = NULL;
    Cross = Unk;
    OrigCross = Unk;
    BitJeu = 0;
    NbMarqueur = 0;
    TailleEchant = 0;
    OverallR = 0.0;
    // DL
    //TwoPointsFR = NULL;
    //TwoPointsDH = NULL;
    //TwoPointsLOD = NULL;
    twopoint = NULL;
    IndMarq_cg2bj = IndMarq_bj2cg = NULL;
    _indmarq_sz = 0;
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - le r�f�rentiel
// - le num�ro
// - le type
// - le nombre de marqueurs
// - le champ de bit
// Valeur de retour :
//-----------------------------------------------------------------------------

BioJeu::BioJeu(CartaGenePtr cartag,
        int id,
        CrossType cross,
        int nm,
        int bitjeu)
{
    Cartage = cartag;
    Id = id;
    Cross = cross;
    OrigCross = Unk;
    BitJeu = bitjeu;
    NbMarqueur = nm;
    OverallR = 0.0;
    twopoint = NULL;
    _indmarq_sz = Cartage->NbMarqueur+1;

    ResetNbEMCall();
}

BioJeu::BioJeu(const BioJeu& b) 
{
    Cartage = b.Cartage;
    Id = b.Cartage->NbJeu+1;
    Cross = b.Cross;
    OrigCross = Unk;
    BitJeu = 1<<b.Cartage->NbJeu;
    NbMarqueur = b.NbMarqueur;
    OverallR = 0.0;
    twopoint = NULL;
    _indmarq_sz = Cartage->NbMarqueur+1;
    ResetNbEMCall();
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BioJeu::~BioJeu()
{
    // DL
    /*std::cerr << "Destroying BioJeu" << std::endl;*/
    if(twopoint) { delete twopoint; }
    if(IndMarq_bj2cg) { delete[] IndMarq_bj2cg; }
    if(IndMarq_cg2bj) { delete[] IndMarq_cg2bj; }
}

// DL
int* BioJeu::GetFullMarkSel() const {
    int *ms = new int[NbMarqueur+1];
    int *ret = ms;
    for(int i=0;i<Cartage->NbMarqueur;i++) {
        if(IndMarq_cg2bj[i]) {
            *ms=i;
            ++ms;
        }
    }
    return ret;
}


// DL
//-----------------------------------------------------------------------------
// Recreate data type string to save data
//-----------------------------------------------------------------------------
const char* BioJeu::GetDataType() const {
    /* BS is to be redefined in BJS_BS class 'cuz it requires string edition based on specific attributes */
    switch(Cross) {
        case Unk:
            return NULL;
        case BC:
            return "f2 backcross";
        case RISelf:
            return "ri self";
        case RISib:
            return "ri sib";
        case IC:
            return "f2 intercross";
        case RH:
            return "radiated hybrid";
        case RHD:
            return "radiated hybrid diploid";
        case RHE:
            return "radiated hybrid error";
        case Mge:
            /* may return something in a future implementation */
            /* FIXME : throw NotImplemented instead ? */
            return NULL;
        case Mor:
            /* may return something in a future implementation */
            /* FIXME : throw NotImplemented instead ? */
            return NULL;
        case Con:
            return "constraint";
        case Ordre:
            return "order";
        default:
            /* FIXME : throw NotImplemented instead ? */
            return NULL;
    };
}


// DL
//-----------------------------------------------------------------------------
// Sauvegarde les données d'un BioJeu dans un fichier mapmaker-like.
// Par défaut, interdit la sauvegarde. Surchargé dans BioJeuSingle et BJS_*.
//-----------------------------------------------------------------------------
// Param�tres :
// - f le fichier ouvert en écriture
void BioJeu::DumpTo(FILE* outFile) const throw(NotImplemented) {
    throw NotImplemented();
}

//LD
//-----------------------------------------------------------------------------
// Break-points pour la classe BJS_OR, sinon erreur
//-----------------------------------------------------------------------------
// Param�tres :
// - id l'indice du jeu
// Valeur de retour : nb de BP ou 0 en cas d'erreur
//-----------------------------------------------------------------------------

int BioJeu::BreakPoints(BioJeu *Jeu)
{
    print_err("error : wrong data type\n");
    return 0;
}

//LD
//-----------------------------------------------------------------------------
// Break-points pour la classe BJS_OR, sinon erreur
//-----------------------------------------------------------------------------
// Param�tres :
// - map une carte
// Valeur de retour : nb de BP ou 0 en cas d'erreur
//-----------------------------------------------------------------------------

int BioJeu::BreakPointsMap(Carte *map)
{
    print_err("error : wrong data type\n");
    return 0;
}

//TF
//-----------------------------------------------------------------------------
// Imputation of corrected genotypes : only relevant for BJS_RHE data type
//-----------------------------------------------------------------------------
// Paramètres :
// - none
//-----------------------------------------------------------------------------

int BioJeu::Imputation( double ConversionCutoff, double CorrectionCutoff, double UnknownCorrectionCutoff,  char *filename)
{
    /*sprintf(bouf,"error : wrong data type\n");*/
    print_err("error : only relevant for radiation hybrids with errors\n");
    return 0;
}

//LD
//-----------------------------------------------------------------------------
// Retourne la position dans le jeu du marqueur marq
//-----------------------------------------------------------------------------
// Param�tres :
// - marq le marqueur
// Valeur de retour : la position dans le jeu
//-----------------------------------------------------------------------------

int BioJeu::GetMarqPos(int marq)
{
    return IndMarq_cg2bj[marq];
#if 0
    if (marq>0 && marq<=Cartage->NbMarqueur && IndMarq[marq] != 0) return IndMarq[marq];
    else {
        print_err( "error : index %d out of bounds or singleton marker \n", marq);
        return 0;
    }
#endif
}

//LD
//-----------------------------------------------------------------------------
// Retourne le marqueur de position pos dans le jeu
//-----------------------------------------------------------------------------
// Param�tres :
// - pos la position du marqueur dans le jeu
// Valeur de retour : le marqueur
//-----------------------------------------------------------------------------

int BioJeu::GetMarq(int pos)
{
    return IndMarq_bj2cg[pos];
#if 0
    int i = 1;

    while (i<=Cartage->NbMarqueur && IndMarq[i] != pos) {

        //printf("indmarq[%d]= %d\n", i, IndMarq[i]);
        i++;
    }
    if (i<=Cartage->NbMarqueur) return i;
    else {
        return 0;
    }
#endif
}

//-----------------------------------------------------------------------------
// d�tection des groupes, affichage et mise � jour de Cartage.
//-----------------------------------------------------------------------------
// Param�tres :
// - disthres seuil de la distance
// - lodthres seuil du lod
// Valeur de retour : le nombre de groupes trouv�s
//-----------------------------------------------------------------------------

struct parallel_triangle_iterator {
    matrix::sync::default_traits::spin_type spin;
    size_t N, sz;

    typedef std::pair<int, int> iteration_type;

    iteration_type cursor;

    parallel_triangle_iterator(size_t size=0)
        : spin(), N(size*(size-1)/2), sz(size), cursor(0, 0)
    {}

    void init(size_t size) {
        N = size*(size-1)/2;
        sz = size;
        cursor.first = 0;
        cursor.second = 0;
    }

    /*const iteration_type& begin() const { static iteration_type b(sz-1, sz-2); return b; }*/
    /*const iteration_type& end() const { static iteration_type e(0, 0); return e; }*/

    /*bool at_end() const { return cursor == end(); }*/

    inline void next(iteration_type& local_cursor) {
        spin.lock();
        /*if(N==0) {*/
        /*local_cursor.first = 0;*/
        /*local_cursor.second = 0;*/
        /*}*/
        if(cursor.second>=(cursor.first-1)) {
            cursor.first++;
            cursor.second = 0;
        } else {
            cursor.second++;
        }
        local_cursor = cursor;
        --N;
        spin.unlock();
    }

};


struct parallel_ladj : public std::vector<LAdj*> {
    BioJeu*bj;
    CartaGene* Cartage;
    std::vector<int> ms;
    matrix::sync::default_traits::mutex_type mutex;
    std::vector<char> pres;
    unsigned int msz;
    double disthres, lodthres;
    unsigned int outer_count;
    unsigned int inner_count;
    double computation_time;
    Utils::ProgressDisplay prodisp;
    /*std::vector<LAdj*> adj;*/
    parallel_triangle_iterator pti;

    parallel_ladj(BioJeu*_b, CartaGene* _c, double dt, double lt)
        : std::vector<LAdj*>(), bj(_b), Cartage(_c), ms(),
        mutex(),
        pres(Cartage->NbMarqueur+1), disthres(dt), lodthres(lt),
        outer_count(Cartage->NbMS/2),
        inner_count(Cartage->NbMS - !(Cartage->NbMS&1)),  /* makes inner(x)*outer(x) == (x*(x-1))/2 for whatever x */
        prodisp(outer_count)
    {
        reserve(Cartage->NbMarqueur+1);
        ms.reserve(Cartage->NbMS);
        for(int i=0;i<Cartage->NbMS;++i) {
            if(Cartage->markers[Cartage->MarkSelect[i]].BitJeu & bj->BitJeu) {
                pres[Cartage->MarkSelect[i]]=1;
            }
        }
        for(int i=Cartage->NbMarqueur;i>=0;--i) {
            if(pres[i]) {
                ms.push_back(i);
                /*} else {*/
            /*std::cout << "marker " << i << " not selected" << std::endl;*/
        }
        push_back(new LAdj(i+1));
        }
        pres.clear();
        msz = ms.size();
        /*std::cout << "adj.size()=" << size() << " ms.size()=" << ms.size() << " NbMS=" << Cartage->NbMS << std::endl;*/
        std::cerr << "Computing adjacency lists for " << msz << " markers..." << std::endl;
        pti.init(msz);
        prodisp.init(outer_count);

        /*#ifdef WITH_PARALLEL*/
        /*tbb::task_scheduler_init init(tbb::task_scheduler_init::default_num_threads()*4);*/
        /*#endif*/
        /*std::cerr << "Got inner=" << inner_count << " outer=" << outer_count << "i*o=" << (inner_count*outer_count) << " N=" << pti.N << std::endl;*/
        computation_time =
            Parallel::Range<Parallel::Timer>()
            /* FIXED: il faut boucler partager SZ*(SZ-1)/2 entre cette boucle et le for de operator(). */
            .execute_range(*this, 0, outer_count).time();
        /*std::cerr << "N = " << pti.N << std::endl;*/
        std::cerr << "took " << computation_time << " seconds to compute." << std::endl;
    }

    void operator()(offset_type op) {
        /*mutex.lock();*/
        /*std::cerr << "*** " << op << std::endl;*/
        /*mutex.unlock();*/
        parallel_triangle_iterator::iteration_type cursor;
        double t = Utils::UserThreadChrono::now();
        size_t m1, m2;
        for(size_t i=0;i<inner_count;++i) {
            pti.next(cursor);
            m1 = ms[cursor.first];
            m2 = ms[cursor.second];
            /*mutex.lock();*/
            /*std::cout << "cursor (" << cursor.first << "," << cursor.second << ") m1=" << m1 << " m2=" << m2 << std::endl;*/
            /*mutex.unlock();*/
            if(bj->GroupTest(disthres, lodthres, m1, m2)) {
                /*mutex.lock();*/
                /*std::cerr << "Markers " << m1 << " and " << m2 << " feel groupy" << std::endl;*/
                /*mutex.unlock();*/
                (*this)[m1]->add(m2);
                (*this)[m2]->add(m1);
            }
        }
        prodisp.step_done(Utils::UserThreadChrono::now()-t);
    }

#if 0
    void operator()(int m1) {
        /*Utils::Tick t = Utils::Tick::now();*/
        double t = Utils::UserThreadChrono::now();
        for(unsigned int m2=m1+1;m2<msz;++m2) {
            if(bj->GroupTest(disthres, lodthres, ms[m1], ms[m2])) {
                (*this)[ms[m1]]->add(ms[m2]);
                (*this)[ms[m2]]->add(ms[m1]);
            }
        }
#ifdef NOT_AFRAID_OF_THRASHING
        int m3 = msz-1-m1;
        if(m3!=m1) {
            for(unsigned int m2=m3+1;m2<msz;++m2) {
                if(bj->GroupTest(disthres, lodthres, ms[m3], ms[m2])) {
                    (*this)[ms[m3]]->add(ms[m2]);
                    (*this)[ms[m2]]->add(ms[m3]);
                }
            }
        }
#endif
        prodisp.step_done(Utils::UserThreadChrono::now()-t);
    }
#endif
};

int BioJeu::Groupe(double disthres, double lodthres) const
{
    int i, j, k, l, color, nbg;
    /*NodintPtr *ladj;*/
    NodintPtr padj, padjw;
    intPtr visited;
    NodptrPtr llink, plink, wlink;

    // initialisation de la liste d'adjacence(dimension max)

    // DL remplacé par un vecteur de class LAdj
    /*ladj = new NodintPtr[Cartage->NbMarqueur + 1];*/
    /*for (i = 0; i <= Cartage->NbMarqueur; i++)*/
    /*ladj[i]=NULL;*/

    i = 0;
    j = 0;
    k = 0;
    l = 0;
    parallel_ladj adj((BioJeu*)this, Cartage, disthres, lodthres);

#if 0
    /* DL
     * On va afficher un petit pourcentage pour voir l'avancement
     */
    unsigned long long __max = Cartage->NbMS*(Cartage->NbMS+1)>>1;
    unsigned long long __count = 0;

    /* DL
     * Pour simplifier la suite (supprimer tous les if, basiquement), on crée la sous-sélection de marqueurs qui ne contient que les marqueurs présents dans ce jeu
     */
    // En fait, on va d'abord construire un tableau de présence pour construire ms avec les marqueurs triés ( O(n) à chaque fois )
    std::vector<int> ms;
    std::vector<char> pres(Cartage->NbMarqueur+1);
    std::vector<LAdj*> adj;
    adj.reserve(Cartage->NbMarqueur+1);
    ms.reserve(Cartage->NbMS);
    for(i=0;i<Cartage->NbMS;++i) {
        if(Cartage->markers[Cartage->MarkSelect[i]].BitJeu&BitJeu) {
            pres[Cartage->MarkSelect[i]]=1;
        }
    }
    for(i=0;i<=Cartage->NbMarqueur;i++) {
        if(pres[i]) {
            ms.push_back(i);
        } else {
            std::cout << "marker " << i << " not selected" << std::endl;
        }
        adj.push_back(new LAdj(i+1));
    }
    pres.clear();
    std::cout << "adj.size()=" << adj.size() << " ms.size()=" << ms.size() << " NbMS=" << Cartage->NbMS << std::endl;
#if 0
    for(i=0;i<Cartage->NbMS;++i) {
        if(Cartage->markers[Cartage->MarkSelect[i]].BitJeu&BitJeu) {
            ms.push_back(Cartage->MarkSelect[i]);
        }
    }
#endif
    /* DL
     * et maintenant on fait la boucle-boucle simplifiée
     */

    size_t msz = ms.size();
    unsigned int percent_interval=msz/100;

    for(unsigned int m1=0;m1<msz;++m1) {
        if((m1%percent_interval)==0) {
            printf("adj.list %3i%% (%i)\r", (int)(100*__count/__max), m1); flush_out();
        }
        for(unsigned int m2=m1+1;m2<msz;++m2) {
            if(GroupTest(disthres, lodthres, ms[m1], ms[m2])) {
                adj[ms[m1]]->add(ms[m2]);
                adj[ms[m2]]->add(ms[m1]);
                /* FIXME : must we add m1 to adj[m2] ? */

                /*padj = new Nodint;*/
                /*padj->vertex = Cartage->MarkSelect[m2];*/
                /*padj->next = ladj[Cartage->MarkSelect[m1]];*/
                /*ladj[Cartage->MarkSelect[m1]] = padj;*/

                /*padj = new Nodint;*/
                /*padj->vertex = Cartage->MarkSelect[m1];*/
                /*padj->next = ladj[Cartage->MarkSelect[m2]];*/
                /*ladj[Cartage->MarkSelect[m2]] = padj;*/
            }
        }
        __count += msz-m1;
    }

    unsigned int bloat=0;
    for(unsigned int m=0;m<msz;m++) {
        bloat+=adj[m]->bloat();
        std::cout << "bloat for ladj #" << m << " :\t" << adj[m]->bloat() << std::endl;
    }
    std::cout << "total bloat : " << bloat << " (NbMarqueur^2*sizeof(int) = " << Cartage->NbMarqueur*Cartage->NbMarqueur*sizeof(int) << ')' << std::endl;
#endif
    /* DL
     * désactivation du code
     */
#if 0
    // remplissage de la liste d'adjacence
    while (i < Cartage->NbMS)
    {
        if (Cartage->markers[Cartage->MarkSelect[k]].BitJeu & BitJeu)
        {
            i++;
            j = i;
            l = k + 1;
            while (j < Cartage->NbMS)
            {
                if (Cartage->markers[Cartage->MarkSelect[l]].BitJeu & BitJeu)
                {
                    j++;
                    if (GroupTest(disthres, lodthres, Cartage->MarkSelect[k], Cartage->MarkSelect[l]))
                    {
                        padj = new Nodint;
                        padj->vertex = Cartage->MarkSelect[l];
                        padj->next = ladj[Cartage->MarkSelect[k]];
                        ladj[Cartage->MarkSelect[k]] = padj;

                        padj = new Nodint;
                        padj->vertex = Cartage->MarkSelect[k];
                        padj->next = ladj[Cartage->MarkSelect[l]];
                        ladj[Cartage->MarkSelect[l]] = padj;
                    }
                }
                l++;
            }
        }
        k++;
    }
#endif

    /* DL
     * maintenant on efface le pourcentage, le parcours restant doit aller vite
     */
    //printf("                             \r"); flush_out();

    // parcours en profondeur
    // initialisations

    visited = new int[Cartage->NbMarqueur + 1];

    for (i = 0; i <= Cartage->NbMarqueur; i++)
        visited[i] = -1;

    color = 0;

    llink = NULL;

    i = 0;
    k = 0;

    while (i < Cartage->NbMS)
    {
        /*std::cout<<"i="<<i<<" k="<<k<<std::endl;*/
        if (Cartage->markers[Cartage->MarkSelect[k]].BitJeu & BitJeu)
        {
            i++;
            if (visited[Cartage->MarkSelect[k]] == -1)
            {
                /*std::cout << "prepare DFSVisit2" << std::endl << std::flush;*/
                plink = new Nodptr;
                plink->next = llink;
                plink->first = NULL;
                llink = plink;
                ++color;
                DFSVisit2(adj, visited, Cartage->MarkSelect[k], color, plink);
                /*std::cout << "after DFSVisit2" << std::endl << std::flush;*/
            } else {
                /*std::cout << "skip DFSVisit2" << std::endl << std::flush;*/
            }
        }
        k++;
    }

    // nettoyage des structures du parcours

    /*std::cout << "before delete[] visited" << std::endl << std::flush;*/
    delete [] visited;
    /*std::cout << "after delete[] visited" << std::endl << std::flush;*/
    /*delete [] ladj;*/
    /*delete adj;*/

    // Au cas o�

    plink = Cartage->Group;

    while (plink != NULL) {

        /*std::cout << "plink="<<plink << std::endl;*/

        padj = plink->first;

        while (padj != NULL)
        {
            /*std::cout << "padj="<<padj << std::endl;*/

            padjw = padj;
            padj = padj->next;
            delete padjw;
        }

        wlink = plink;
        plink = plink->next;
        delete wlink;
    }

    // Mise � jour

    Cartage->Group = llink;
    Cartage->DisThres = disthres;
    Cartage->LODThres = lodthres;

    // Affichage des groupes.

    /*std::cout << "calling GroupMess(disthres, lodthres)" << std::endl;*/

    GroupMess(disthres, lodthres);

    /*std::cout << "after GroupMess(disthres, lodthres)" << std::endl;*/

    print_out( "\n%9s : Marker ID List ...\n", "Group ID");

    nbg = 0;

    plink = llink;

    /*ext::rope<char> r;*/

    /*r.append("puts -nonewline {");*/

    while (plink != NULL) {

        /*std::cout << "plink@" << plink << ' ' << nbg << std::endl;*/

        /*sprintf(bouf, "%9d :",++nbg);*/
        /*r.append(bouf);*/
        /*pout(bouf);*/
        print_out("%9d :", ++nbg);

        padj = plink->first;

        while (padj != NULL)
        {
            /*std::cout << "	padj@" << padj << ' ' << padj->vertex << std::endl;*/
            /*sprintf(bouf, " %d", padj->vertex);*/
            /*pout(bouf);*/
            /*r.append(bouf);*/
            print_out(" %d", padj->vertex);

            padjw = padj;
            padj = padj->next;
        }

        /*sprintf(bouf,  "\n");*/
        /*pout(bouf);*/
        /*r.append(bouf);*/
        print_out("\n");

        llink = plink;
        plink = plink->next;
    }
    /*r.append("}; flush stdout");*/
    /*#ifdef ihm*/
    /*Tcl_Eval(linterp, r.c_str());*/
    /*#endif*/
    return nbg;

}

//-----------------------------------------------------------------------------
// parcours en profondeur
//-----------------------------------------------------------------------------
// Param�tres :
// - liste d'adjacence
// - vertex
// - couleure
// Valeur de retour :
//-----------------------------------------------------------------------------

void BioJeu::DFSVisit(NodintPtr *ladj,
        intPtr visited,
        int vertex,
        int color,
        NodptrPtr plink) const
{
    NodintPtr padj, padjw;
    NodintPtr pllink;

    visited[vertex] = 0; // blanc pour le premier passage

    padj = ladj[vertex];

    while (padj != NULL) {
        if (visited[padj->vertex] == -1)
            DFSVisit(ladj, visited, padj->vertex, color, plink);
        padjw = padj;
        padj = padj->next;
        delete padjw;
    }

    pllink = new Nodint;
    pllink->vertex = vertex;
    pllink->next = plink->first;
    plink->first = pllink;

    visited[vertex] = color;

}


void BioJeu::DFSVisit2(std::vector<LAdj*>& ladj,
        intPtr visited,
        int vertex,
        int color,
        NodptrPtr plink) const
{
    LAdj::iterator iter(*ladj[vertex]);
    NodintPtr pllink;

    visited[vertex] = 0; // blanc pour le premier passage

    while (!iter.at_end()) {
        int v = (int)iter.next();
        if (visited[v] == -1)
            DFSVisit2(ladj, visited, v, color, plink);
    }

    pllink = new Nodint;
    pllink->vertex = vertex;
    /* perform an insertion sort instead of always inserting at head */
    /*pllink->next = plink->first;*/
    /*plink->first = pllink;*/
    NodintPtr tmp = plink->first;
    if((!tmp) || tmp->vertex > vertex) {
        /* empty list or bigger element at head, insert at head */
        pllink->next = plink->first;
        plink->first = pllink;
    } else if(tmp) {
        NodintPtr prev=tmp;
        while(tmp && tmp->vertex<vertex) {
            prev = tmp;
            tmp = tmp->next;
        }
        pllink->next=tmp;
        prev->next=pllink;
    }

    visited[vertex] = color;

}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// - seuil de convergence
// Valeur de retour : la vraisemblance maximum
//-----------------------------------------------------------------------------

double BioJeu::ComputeEM(Carte *map)
{
    int i,DomainHit = 0;
    double CoutSauv, Cout = -1e100;
    int *ordre = map->ordre;
    double * expected;

    NbEMCall++;

    // si la carte est deja finement estimee, on retourne la logv
    if (map->Converged <= Epsilon2) return map->coutEM;

    PreparEM(map);

    // + 1 = 2 cases en + pour EBreak & ERetained
    expected = new double[map->NbMarqueur + 1];

    for (i = 0; i < map->NbMarqueur + 1; i++) expected[i] = 0.0;

    // si la carte a deja ete estimee grossierement, on efface pas le boulot !
    if (map->Converged > Epsilon1) {
        for (i = 0; i < map->NbMarqueur - 1; i++) {
            map->tr[i] = GetTwoPointsFR(ordre[i],ordre[i+1]);
            /*printf("tr[%i] = %lf\n", i, map->tr[i]);*/
        }
        map-> ret = 0.3;
    }

    // on estime finement
    do   {
        CoutSauv = Cout;
        Cout = ComputeExpected(map, expected);
        /*printf("LogVraisemblance: %lf\n",Cout);*/

        if (((Cout - CoutSauv) < -Close2Zero) && !DomainHit)
            NbEMUnconv++;

        DomainHit |= Maximize(map, expected);
    }
    while (Cout-CoutSauv > Epsilon2);

    if (DomainHit) NbEMHit++;

    map->Converged = Epsilon2;
    map->coutEM = Cout;

    delete [] expected;

    NetEM(map);

    return Cout;
}
//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//  Une interface plus sophistiquee:
//  On converge jusqu'a epsilon1. Si la vraisemblance est inferieure a
//  threshold on laisse tomber, sinon on converge a epsilon2
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// - niveau a partir duquel on convergera a epsilon2
// Valeur de retour : la vraisemblance maximum.
//-----------------------------------------------------------------------------

double BioJeu::ComputeEMS(Carte *map,
        double threshold)
{
    int i,DomainHit = 0;
    int *ordre = map->ordre;
    double CoutSauv = 0.0,Cout = -1e100;
    double * expected;


    // si la carte est deja grossierement estimee et que sa logv est
    // eloignee du seuil, on retourne la logv
    if ((map->Converged <= Epsilon1) &&
            (map->coutEM < threshold))
        return map->coutEM;

    NbEMCall++;

    // + 1 = 2 cases en + pour EBreack & ERetained
    expected = new double[map->NbMarqueur + 1];

    for (i = 0; i < map->NbMarqueur + 1; i++) expected[i] = 0.0;

    PreparEM(map);

    // si la carte n'est pas du tout estimee, on la remplit avec des
    // valeurs de demarrage pas totalement debiles
    if (map->Converged > Epsilon1) {
        for(i = 0; i < map->NbMarqueur - 1; i++) {
            map->tr[i] = GetTwoPointsFR(ordre[i],ordre[i+1]);
            /*printf("tr[%i] = %lf\n", i, map->tr[i]);*/
        }
        map->ret = 0.3;

        // et on l'estime grossierement
        do
        {
            CoutSauv = Cout;
            Cout = ComputeExpected(map, expected);

            if (((Cout - CoutSauv) < -Close2Zero) && !DomainHit)
                NbEMUnconv++;

            DomainHit |= Maximize(map, expected);
        }
        while (Cout-CoutSauv > Epsilon1);
        map->Converged =Epsilon1;
    }
    // sinon, elle a deje ete estimee grossierement et on recupere le logv
    else
        Cout =  map->coutEM;

    // si on est pres du seuil on affine l'estimation
    if (Cout >= threshold) {
        while (Cout-CoutSauv > Epsilon2)
        {
            CoutSauv = Cout;
            Cout = ComputeExpected(map, expected);

            if (((Cout - CoutSauv) < -Close2Zero) && !DomainHit) {
                print_err( "\nBug ComputeEMS 2 : DeltaV = %f\n",Cout-CoutSauv);
            }

            DomainHit |= Maximize(map, expected);
        }
        map->Converged =Epsilon2;
    }

    if (DomainHit) NbEMHit++;

    map->coutEM = Cout;

    delete [] expected;

    NetEM(map);

    return Cout;
}

//-----------------------------------------------------------------------------
// Etape de maximisation
//-----------------------------------------------------------------------------
// Paramètres :
// - la carte
// - le vecteur d'expectation
// Valeur de retour :
//-----------------------------------------------------------------------------
int BioJeu::Maximize(Carte* map, double *expected) const
{
    int j, DomainHit = 0;
    double temp;

    int NbM = map->NbMarqueur;

    for (j = 0; j < map->NbMarqueur - 1; j++) {
        temp = (expected[j]) / (double)NbMeiose;
        if (temp > Em_Max_Theta) {
            temp =  Em_Max_Theta;
            DomainHit = 1;
        }
        else if (temp < Em_Min_Theta) {
            temp =  Em_Min_Theta;
            DomainHit = 1;
        }

        map->tr[j] = temp;
        expected[j] = 0.0;
    }

    //verrue RH...

    if (EBreak > 0.0)
    {
        // estimation de la proba de retention: nbre de retenus casses / casses
        temp = ERetained/EBreak;

        if (temp > EM_MAX_RETAIN)  {
            temp = EM_MAX_RETAIN;
            DomainHit = 1;
        }
        else if (temp < EM_MIN_RETAIN) {
            temp = EM_MIN_RETAIN;
            DomainHit = 1;
        }
        map->ret = temp;
    }

    return DomainHit;
}

//-----------------------------------------------------------------------------
// Affichage simple s�lectif d'une carte
// Seuls les marqueurs du g�notype sont affich�s
//-----------------------------------------------------------------------------
// Param�tres :
// - la carte
// Valeur de retour :
//----------------------------------------------------------------------------

void BioJeu::PrintMap(Carte *data) const
{
    char form[16];

    print_out( "%4d : ", Id);

    for (int i = 0; i < data->NbMarqueur; i++)
    {
        sprintf(form,"%c%d%c%d%c ", '%', (int)Cartage->markers.keyOf(data->ordre[i]).size(), '.', (int)Cartage->markers.keyOf(data->ordre[i]).size(), 's');

        if (Cartage->markers[data->ordre[i]].BitJeu &
                BitJeu) {
            print_out( form, Cartage->markers.keyOf(data->ordre[i]).c_str());
        }
        else {
            print_out( form,"");
        }
    }

    print_out(  "\n");
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Param�tres :
// Valeur de retour :
//----------------------------------------------------------------------------

void BioJeu::PrintTwoPointsDist(const char unit[2]) const
{
    int i,j;

    print_out( "\nData Set Number %2d :\n", Id);

    print_out(  "                 ");
    for (i = 0; i < Cartage->NbMS; i++)
        if (Cartage->markers[Cartage->MarkSelect[i]].BitJeu &
                BitJeu) {
            print_out( "%5.5s ",Cartage->markers.keyOf(Cartage->MarkSelect[i]).c_str());
        }
    print_out(  "\n");
    flush_out();
    print_out(  "                 ");
    for (i = 0; i < Cartage->NbMS; i++)
        if (Cartage->markers[Cartage->MarkSelect[i]].BitJeu &
                BitJeu) {
            print_out(  "------");
        }
    print_out(  "\n");
    flush_out();

    for (i = 0; i< Cartage->NbMS; i++)
    {
        if (Cartage->markers[Cartage->MarkSelect[i]].BitJeu &
                BitJeu)
        {
            print_out( "%15.15s |",Cartage->markers.keyOf(Cartage->MarkSelect[i]).c_str());
            flush_out();
            for (j = 0; j < Cartage->NbMS; j++)
                if (Cartage->markers[Cartage->MarkSelect[j]].BitJeu &
                        BitJeu) {
                    if (i == j)  {
                        print_out(  "------");
                    }
                    else {
                        print_out( "%5.1f ", 100 * Distance(unit, GetTwoPointsFR(Cartage->MarkSelect[i],
                                        Cartage->MarkSelect[j])));
                    }
                }
            print_out(  "\n");
            flush_out();
        }
    }
}

//-----------------------------------------------------------------------------
// retourne une carte sous forme de liste
//-----------------------------------------------------------------------------
// Param�tres :
// - l'unit� de distance
// - la carte
// Valeur de retour :
//----------------------------------------------------------------------------

char*** BioJeu::GetMap(const char unit[2],
        Carte *data) const
{

    char* temp;
    int i, j = 0;
    double dist = 0.0;

    char *** resultg = new char**[2];
    resultg[1] = NULL;

    char ** result = new char*[(2 * data->NbMarqueur) + 3];

    result[(2 * data->NbMarqueur) + 2] = NULL;

    resultg[0] = result;

    temp = new char[8];
    sprintf(temp,
            "%d",
            Id);
    result[0] = temp;

    temp = new char[16];
    sprintf(temp,
            "%.2f",
            data->coutEM);
    result[1] = temp;

    j = 2;
    for (i = 0; i < data->NbMarqueur; i++)
    {
        temp = new char[Cartage->markers.keyOf(data->ordre[i]).size()+ 1];
        strcpy(temp,
                Cartage->markers.keyOf(data->ordre[i]).c_str());
        result[j++] = temp;
        temp = new char[32];

        if (i >= 1)
            dist = dist + Distance(unit, data->tr[i-1])*100;
        else
            dist = 0.0;
        sprintf(temp, "%.1f", dist);
        result[j++] = temp;
    }

    return resultg;
}


//-----------------------------------------------------------------------------
// Affichage détaillé d'une carte
//-----------------------------------------------------------------------------
// Paramètres :
// - la carte
// - le sens
// - la carte de référence
// Valeur de retour :
//----------------------------------------------------------------------------

void BioJeu::PrintDMap(Carte *data, int envers, Carte *dataref)
{

    int i,j,k,itr,suivant;
    unsigned int maxl;
    double d, totald = 0.0;
    double dk, totaldk = 0.0;
    char form[128];
    int *pos;
    bool *reached;

    pos = new int[data->NbMarqueur];
    reached = new bool[Cartage->NbMarqueur];

    // récupération de la taille la plus grande d'un nom de locus.

    maxl = 0;
    for (i = 0; i < data->NbMarqueur; i++) {
        pos[i] = i + 1;
        j = Cartage->markers[data->ordre[i]].Represents;
        while (j != 0) {
            if (Cartage->markers.keyOf(j).size() > maxl)
                maxl = Cartage->markers.keyOf(j).size();
            j = Cartage->markers[j].Represents;
        }
        reached[data->ordre[i]] = true;
        if (Cartage->markers.keyOf(data->ordre[i]).size() > maxl)
            maxl = Cartage->markers.keyOf(data->ordre[i]).size();
    }

    // cas de la fusion sur l'ordre, détermination de la position absolue

    if (data != dataref) {
        // recherche pour chaque marqueur de sa position
        for (i = 0; i < data->NbMarqueur; i++) {
            j = 0;
            while ( data->ordre[i] != dataref->ordre[j])
                j++;
            if (envers)
                pos[data->NbMarqueur - i - 1] = dataref->NbMarqueur - j;
            else
                pos[i] = j + 1;
        }
    }

    print_out( "\nData Set Number %2d :\n", Id);
    print_out(  "\n");

    if (HasRH()) {
        sprintf(form, "               %c%d%cDistance      Cumulative   Theta        2pt\n", '%', maxl +4, 's');
        print_out( form, "Markers ");

        sprintf(form, " Pos  Id %c%d%c                                   (%%sage)       LOD\n\n",'%', maxl, 's');
        print_out( form, "name","%%");
    }
    else {
        sprintf(form, "             %c%d%cDistance    Cumulative  Distance   Theta       2pt\n", '%', maxl+4, 's');
        print_out( form, "Markers ");

        sprintf(form, "Pos  Id %c%d%c         Haldane     Haldane     Kosambi    (%%sage)      LOD\n\n",'%', maxl, 's');
        print_out( form, "name","%%");
    }

    for (k = 0; k < data->NbMarqueur; k++) {

        if (envers) {
            i = data->NbMarqueur - k - 1;
            itr = i - 1;
            suivant = itr;
        } else {
            i = k;
            itr = i;
            suivant = i +1;
        }

        j = Cartage->markers[data->ordre[i]].Represents;
        while (j != 0) {
            if  (reached[j] == false) {
                sprintf(form, "%s %c%d%c ","%3d %3d",'%', maxl, 's');
                print_out( form, pos[k], j, Cartage->markers.keyOf(j).c_str());

                if (HasRH()) {
                    sprintf(form,"         %%6.%1df cR    %%7.%1df cR    %%5.1f %%%%     ------\n",Cartage->DistPrecision,Cartage->DistPrecision);
                    print_out( form, 0.0, totald, 0.0);
                }
                else {
                    sprintf(form,"       %%6.%1df cM   %%6.%1df cM   %%6.%1df cM   %%5.1f %%%%   ------\n",Cartage->DistPrecision,Cartage->DistPrecision,Cartage->DistPrecision);
                    print_out( form, 0.0, totald, 0.0, 0.0);
                }
                reached[j] = true;
            }

            j = Cartage->markers[j].Represents;
        }

        delete[] reached;

        sprintf(form, "%s %c%d%c ","%3d %3d",'%', maxl, 's');
        print_out( form, pos[k], data->ordre[i],
                Cartage->markers.keyOf(data->ordre[i]).c_str());

        if (k < data->NbMarqueur-1) {
            if (HasRH())  {
                d = Theta2Ray(data->tr[itr])*100;
                sprintf(form,"         %%6.%1df cR    %%7.%1df cR    %%5.1f %%%%     %%5.1f\n",Cartage->DistPrecision,Cartage->DistPrecision);
                print_out( form,
                        d, totald, data->tr[itr]*100,
                        GetTwoPointsLOD(data->ordre[i],
                            data->ordre[suivant]));
                totald += d;
            }
            else {
                d = Haldane(data->tr[itr])*100;
                dk = Kosambi(data->tr[itr])*100;
                sprintf(form,"       %%6.%1df cM   %%6.%1df cM   %%6.%1df cM   %%5.1f %%%%    %%5.1f\n",Cartage->DistPrecision,Cartage->DistPrecision,Cartage->DistPrecision);
                print_out( form,
                        d,
                        totald,
                        dk,
                        data->tr[itr]*100,
                        GetTwoPointsLOD(data->ordre[i],
                            data->ordre[suivant]));
                totaldk += dk;
                totald += d;
            }
        }
    }

    if (HasRH()) {
        print_out(  "         ---------\n");
        sprintf(form, "             %c%d%c     %s\n",'%', maxl, 's',"%6.1f cR\n");
        print_out( form, " ", totald);
        print_out( "\n       %2d markers, log10-likelihood = %8.2f",
                data->NbMarqueur,
                (data->coutEM));
        print_out( "\n                   log-e-likelihood = %8.2f",
                (data->coutEM*log((double) 10)));
        print_out( "\n                   retention proba. = %8.2f",
                data->ret);
    }
    else {
        print_out(  "       ----------              ----------\n");

        sprintf(form, "        %c%d%c        %s\n",'%', maxl, 's',"%6.1f cM               %6.1f cM\n");
        print_out( form, " ", totald, totaldk);

        print_out( "\n       %2d markers, log10-likelihood = %8.2f",
                data->NbMarqueur, (data->coutEM));

        print_out( "\n                   log-e-likelihood = %8.2f",
                (data->coutEM*log((double) 10)));
    }
    print_out(  "\n");

    delete [] pos;
}


int BioJeu::Couplex(int m1, int m2) const {
    return (Cartage->markers[m1].BitJeu & BitJeu &&
            Cartage->markers[m2].BitJeu & BitJeu);
};

