//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_RHD.cc,v 1.19.2.5 2012-08-02 15:43:54 dleroux Exp $
//
// Description : BJS_RHD.
// Divers :
//-----------------------------------------------------------------------------

#include "BJS_RH.h"
#include "BJS_RHD.h"
#include "BJS_RHE.h"
#include "Genetic.h"
#include "System.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

/** Esperance du nombre de cassures (extremites gauches) */
// FIXME HORREUR ! au moins, définir ça JUSTE AU-DESSUS DU CODE QUI L'UTILISE !
//#define EBreak expected[NbM - 1]

/** Esperance du nombre d'extremites gauches retenues */
// FIXME HORREUR ! au moins, définir ça JUSTE AU-DESSUS DU CODE QUI L'UTILISE !
//#define ERetained expected[NbM]

/** liste des génotypes possibles pour chaque phénotype */

const int RHD_Possibles[16][4] = {
  {0, 0, 0, 0},  //  Absent
  {1, 2, 3, 0},  //  Présent
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 1, 2, 3}  //  Inconnu
};

/** nombre de possibilités pour chaque phenotype */
const int RHD_NPossibles[16] = {1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,4};

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_RHD::BJS_RHD() : BioJeuSingle()
{
//  for (int i = 0; i < 4; i++) {
//    SourceTo[i] = NULL;
//    ToSink[i] = NULL;
//  }
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Paramètres :
// - accès à l'ensemble des informations du système. 
// - le num�ro du jeu
// - le type
// - le nombre de marqueurs
// - la taille de l'echantillon
// - le champ de bit
// - la matrice de l'�chantillon
// Valeur de retour :
//-----------------------------------------------------------------------------

BJS_RHD::BJS_RHD(CartaGenePtr cartag,
	       int id,
	       charPtr nomjeu,
	       CrossType cross,
	       int nm, 
	       int taille,
	       int bitjeu,
	       int *indmarq,
	       Obs **echantil)
  :BioJeuSingle(cartag,
		id,
                nomjeu,
		cross, 
		nm, 
		taille, 
		bitjeu, 
		indmarq, 
		echantil)
{

//  for (int i = 0; i < 4; i++) {
//    SourceTo[i] = NULL;
//    ToSink[i] = NULL;
//  }

  NbMeiose = 2*TailleEchant;

  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;

  ComputeTwoPoints();
}

// DL
// FIXME : vulgaire copier-coller
BJS_RHD::BJS_RHD(const BJS_RHD& b)
	: BioJeuSingle(b)
{
//  for (int i = 0; i < 4; i++) {
//    SourceTo[i] = NULL;
//    ToSink[i] = NULL;
//  }
  NbMeiose = 2*TailleEchant;
  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;
  ComputeTwoPoints();
}

BJS_RHD::BJS_RHD(const BJS_RH& b)
	: BioJeuSingle(b)
{
//  for (int i = 0; i < 4; i++) {
//    SourceTo[i] = NULL;
//    ToSink[i] = NULL;
//  }
  NbMeiose = 2*TailleEchant;
  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;
  ComputeTwoPoints();
}

BJS_RHD::BJS_RHD(const BJS_RHE& b)
	: BioJeuSingle(b)
{
//  for (int i = 0; i < 4; i++) {
//    SourceTo[i] = NULL;
//    ToSink[i] = NULL;
//  }
  NbMeiose = 2*TailleEchant;
  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;
  ComputeTwoPoints();
}



//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_RHD::~BJS_RHD()
{
}


//-----------------------------------------------------------------------------
// Compatibilité des observations pour 2 marqueurs
//-----------------------------------------------------------------------------
int BJS_RHD::Compatible(int numarq1, int numarq2) const
{
  int i;
  enum Obs Obs1, Obs2;

  for (i = 1; i<= TailleEchant; i++) {
    Obs1 = GetEch(numarq1,i);
    Obs2 = GetEch(numarq2,i);
    if (!((Obs1 == Obs1111) || (Obs2 == Obs1111) || (Obs1 == Obs2)))
      return 0;
  }
  return 1;
}

//-----------------------------------------------------------------------------
// Fusion des observations entres individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
//-----------------------------------------------------------------------------
void BJS_RHD::Merge(int numarq1, int numarq2) const
{
  int i;
  enum Obs TheObs;

  // les deux marqueurs sont ils definis dans le jeux de donnees ?
  if ((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu &
 BitJeu))
    {
      // Oui, on va fusionner dans le premier
      /*for (i = 1; i<= TailleEchant; i++) {*/
        /*if ((TheObs = Echantillon[IndMarq[numarq1]][i]) == Obs1111)*/
          /*TheObs = Echantillon[IndMarq[numarq2]][i];*/
        /*Echantillon[IndMarq[numarq1]][i] = TheObs;*/
      for (i = 1; i<= TailleEchant; i++) {
        if ((TheObs = GetEch(numarq1, i)) == Obs1111)
          TheObs = GetEch(numarq2, i);
        //SetEch(numarq1, i, TheObs);
        Echantillon[numarq1][i] = TheObs;
      }
    }
}

//-----------------------------------------------------------------------------
//  Calcul de la probabilite de passage par une arete suivant la paire de 
// genotypes relies
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux genotypes, la proba de rec
// - 
// Valeur de retour : la proba
//-----------------------------------------------------------------------------

inline double BJS_RHD::ProbArete(int a, int b, double p, double r) const
{
  
  double prob = 1.0;
  
  switch(a)
    {
    case 0 : /*absent absent*/
      switch(b)
	{
	case 0 : /*absent absent
		   (1-pr)(1-pr) */
	  prob = (1.0-p*r);
	  prob *= prob;
	  break;
	  
	case 1 : /*absent pr�sent 
		  (1-pr)*pr */ 
	case 2 : /*pr�sent absent 
		  pr*(1-pr)*/
	  prob = p*r;
	  prob *= (1.0-prob);
	  break;
	  	    
	case 3 : /*pr�sent pr�sent
		  pr*pr */
	  prob = p*r;
	  prob *= prob;
	  break;
	}
      break;
      
    case 1 : /*absent pr�sent*/ 
      switch(b)
	{
	case 0 : /*absent absent
		  (1-pr)(p(1-r)) */
	  prob = p*r;
	  prob = (1.0 -prob)*(p - prob);
	  break;
	  
	case 1 : /*absent pr�sent
		   (1-pr)(1-p(1-r))*/ 
	  prob = p*r;
	  prob = (1.0 - prob)*(1.0 - p +prob);
	  break;
	  
	case 2 : /*pr�sent absent
		   (pr)(p(1-r)) */
	  prob = p*r;
	  prob *= (p-prob);
	  break;
	  
	case 3 : /*pr�sent pr�sent
		  (pr)(1-p(1-r)) */
	  prob = p*r;
	  prob *= (1.0 -p + prob);
	  break;
	}
      break;
      
    case 2 : /*pr�sent absent*/
      switch(b)
	{
	case 0 : /*absent absent
		  p(1-r)(1-pr) */
	  prob = p*r;
	  prob = (1.0 - prob)*(p - prob);
	  break;
	  
	case 1 : /*absent pr�sent
		  p(1-r)pr*/ 
	  prob = p*r;
	  prob *= (p-prob);
	  break;
	  
	case 2 : /*pr�sent absent
		  (1-p(1-r))(1-pr) */
	  prob = p*r;
	  prob = (1.0 -prob)*(1 - p + prob);
	  break;
	  
	case 3 : /*pr�sent pr�sent
		  (1-p(1-r))pr*/
	  prob = p*r;
	  prob *= (1.0 - p + prob);
	  break;
	}
      break;
      
    case 3 : /*pr�sent pr�sent*/
      switch(b)
	{
	case 0 : /*absent absent
		  p(1-r)p(1-r) */
	  prob = p*(1-r);
	  prob *= prob;
	  break;
	  
	case 1 : /* absent pr�sent
		  p(1-r)(1-p(1-r))*/ 
	case 2 : /*pr�sent absent*/
	  prob = p*r;
	  prob = (p - prob)*(1.0 - p + prob);
	  break;
	  
	case 3 : /*pr�sent pr�sent
		  (1-p(1-r))(1-p(1-r)) */
	  prob = (1.0-p*(1-r));
	  prob *= prob;
	  break;
	}
      break;
    }
 
  return prob;
}

//-----------------------------------------------------------------------------
//  Calcul de la probabilite de cassure par une arete suivant la paire de 
// genotypes relies
//-----------------------------------------------------------------------------
// Param�tres : 
// - la proba de cassure et de retention
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

inline void BJS_RHD::CompProbArete(BJS_RHD::EM_state& state, double p, double r)
{
  // proba de deux cassures
  state.ProbAr2[0][0] = state.ProbAr2[1][0] = state.ProbAr2[2][0] = state.ProbAr2[3][0] = p*p*(1-r)*(1-r);
  state.ProbAr2[0][1] = state.ProbAr2[1][1] = state.ProbAr2[2][1] = state.ProbAr2[3][1] =  \
    state.ProbAr2[0][2] = state.ProbAr2[1][2] = state.ProbAr2[2][2] = state.ProbAr2[3][2] = p*p*(1-r)*r;
  state.ProbAr2[0][3] = state.ProbAr2[1][3] = state.ProbAr2[2][3] = state.ProbAr2[3][3] = p*p*r*r;
  
  // une cassure
  state.ProbAr[0][0] = 2*p*(1-p)*(1-r);
  state.ProbAr[1][0] = state.ProbAr[2][0] = p*(1-p)*(1-r);
  state.ProbAr[3][0] = state.ProbAr[0][3] = state.ProbAr[2][1] = state.ProbAr[1][2] = 0.0; 
  state.ProbAr[0][1] =  state.ProbAr[0][2] =  state.ProbAr[1][3] = state.ProbAr[2][3] = p*(1-p)*r;
  state.ProbAr[1][1] = state.ProbAr[2][2] = p*(1-p)*r +  p*(1-p)*(1-r);
  state.ProbAr[3][1] = state.ProbAr[3][2] = p*(1-p)*(1-r);
  state.ProbAr[3][3] = 2*p*(1-p)*r;
 
  // No cassure
  state.ProbArNB[0][0] = state.ProbArNB[1][1] = state.ProbArNB[2][2] = state.ProbArNB[3][3] = (1.0 - p)*(1.0 - p);
  state.ProbArNB[1][0] = state.ProbArNB[2][0] = state.ProbArNB[3][0] =  \
    state.ProbArNB[0][1] = state.ProbArNB[2][1] = state.ProbArNB[3][1] =  \
    state.ProbArNB[0][2] = state.ProbArNB[1][2] = state.ProbArNB[3][2] =  \
    state.ProbArNB[0][3] = state.ProbArNB[1][3] = state.ProbArNB[2][3] =  0.0;
  
  // 2 retentions
    state.ProbArRet2[0][0] = state.ProbArRet2[1][0] = state.ProbArRet2[2][0] = state.ProbArRet2[3][0] = 0.0;
  state.ProbArRet2[0][1] = state.ProbArRet2[1][1] = state.ProbArRet2[2][1] = state.ProbArRet2[3][1] = 0.0;
  state.ProbArRet2[0][2] = state.ProbArRet2[1][2] = state.ProbArRet2[2][2] = state.ProbArRet2[3][2] = 0.0;
  state.ProbArRet2[0][3] =  state.ProbArRet2[1][3] = state.ProbArRet2[2][3] = state.ProbArRet2[3][3] = state.ProbAr2[0][3];
  
  // 1 retention
  state.ProbArRet[0][0] = state.ProbArRet[1][0] = state.ProbArRet[2][0] = state.ProbArRet[3][0] = 0.0;
  state.ProbArRet[0][1] = state.ProbArRet[1][1] = state.ProbArRet[0][2] = state.ProbArRet[2][2] = p*r*(1-p*r);
  state.ProbArRet[2][1] = state.ProbArRet[3][1] = state.ProbArRet[1][2] = state.ProbArRet[3][2] = p*p*r*(1-r);
  state.ProbArRet[0][3] = 0.0; 
  state.ProbArRet[1][3] =  state.ProbArRet[2][3] = p*(1-p)*r;
  state.ProbArRet[3][3] = 2*p*(1-p)*r;
}



//-----------------------------------------------------------------------------
//  Etape d' � Expectation � de EM.
//-----------------------------------------------------------------------------
// Param�tres : la carte
// - la carte
// - le vecteur d'Expectation(i,o)
// Valeur de retour : le loglike 
//-----------------------------------------------------------------------------

double BJS_RHD::ComputeExpected(const Carte* map, double *expected)
{
#if 1
    BJS_RHD::PCEType parallel_expected(this, map, expected);
    Parallel::Range<>().execute_slices(parallel_expected, 1, TailleEchant+1, 10);
    return parallel_expected.loglike;
#else
 double loglike = 0.0;

 for (int i = 1 ; i <= TailleEchant; i++)  {
   loglike += ComputeSourceTo(i, map);
   ComputeToSink(i, map); 
   
   ComputeOneExpected(i, map, expected);
 }

 return (loglike);
#endif
}


//-----------------------------------------------------------------------------
//  Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite gauche (source)
//-----------------------------------------------------------------------------
// Param�tres :
// - l'individu 
// - la carte 
// Valeur de retour : le loglike 
//-----------------------------------------------------------------------------

double BJS_RHD::ComputeSourceTo(std::vector<double>*SourceTo, int Ind, const Carte *map) const
{
  int i,j,k;
  int *ordre = map->ordre;
  double norm = 0.0;
  double loglike = 0.0;
  int PhenoL, PhenoR;

  PhenoR = GetEch(ordre[0], Ind);

  // au d�part de la source ??? pas bien compris, normalisation..., le log ???

  switch(PhenoR)
    {
    case Obs1111 : /* inconnu */
      SourceTo[0][0] = (1.0 - map->ret)*(1.0 - map->ret);
      SourceTo[1][0] = (map->ret)*(1.0 - map->ret);
      SourceTo[2][0] = (1.0 - map->ret)*(map->ret);
      SourceTo[3][0] = (map->ret)*(map->ret);
      break;
    case Obs0000 : /* connu, absent */
      SourceTo[0][0] = 1.0;
      SourceTo[1][0] = 0.0;
      SourceTo[2][0] = 0.0;
      SourceTo[3][0] = 0.0;
      loglike += log10((1.0 - map->ret)*(1.0 - map->ret));
      break;
    case Obs0001 : /* connu, pr�sent */
      SourceTo[0][0] = 0.0;
      SourceTo[1][0] = (map->ret)*(1.0 - map->ret);
      SourceTo[2][0] = (1.0 - map->ret)*(map->ret);
      SourceTo[3][0] = (map->ret)*(map->ret);
      norm = SourceTo[1][0]+SourceTo[2][0]+SourceTo[3][0];
      loglike += log10(norm);

      SourceTo[1][0] /= norm;
      SourceTo[2][0] /= norm;
      SourceTo[3][0] /= norm;
      break;
    default: 
      print_out(  "Unexpected observation in data set\n.");
    }
  
  for (i = 1; i< map->NbMarqueur; i++)
    {
      SourceTo[0][i] = 0.0;
      SourceTo[1][i] = 0.0;
      SourceTo[2][i] = 0.0;
      SourceTo[3][i] = 0.0;

      PhenoL = PhenoR;
      PhenoR = GetEch(ordre[i], Ind);

      for (j = 0; j< RHD_NPossibles[PhenoL]; j++)
	{
	  for (k = 0; k < RHD_NPossibles[PhenoR]; k++)
	    SourceTo[RHD_Possibles[PhenoR][k]][i] += 
	      (SourceTo[RHD_Possibles[PhenoL][j]][i-1]) *
	      ProbArete(RHD_Possibles[PhenoL][j],RHD_Possibles[PhenoR][k],map->tr[i-1],map->ret);
	}
      norm = SourceTo[0][i]+SourceTo[1][i]+SourceTo[2][i]+SourceTo[3][i];
      loglike += log10(norm);
      
      SourceTo[0][i] /= norm;
      SourceTo[1][i] /= norm;
      SourceTo[2][i] /= norm;
      SourceTo[3][i] /= norm;
    }

  return loglike;
}

//-----------------------------------------------------------------------------
// Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite droite (puit)
//-----------------------------------------------------------------------------
// Param�tres : 
// - l'individu
// - la carte 
// Valeur de retour : 
//-----------------------------------------------------------------------------

double BJS_RHD::ComputeToSink(std::vector<double>*ToSink, int Ind, const Carte *map) const
{
  int i,j,k;
  int *ordre = map->ordre;
  double norm = 0.0;
  double loglike = 0.0;
  int PhenoL,PhenoR;

  PhenoL = GetEch(ordre[map->NbMarqueur - 1], Ind);

  // au d�part de l'�vier ??? pas bien compris, normalisation..., le log ???

  switch(PhenoL)
    {
    case Obs1111 : /* inconnu */
      ToSink[0][ map->NbMarqueur-1] = 1.0;
      ToSink[1][ map->NbMarqueur-1] = 1.0;
      ToSink[2][ map->NbMarqueur-1] = 1.0;
      ToSink[3][ map->NbMarqueur-1] = 1.0;
      break;
    case Obs0000 : /* connu, absent */
      ToSink[0][ map->NbMarqueur-1] = 1.0;
      ToSink[1][ map->NbMarqueur-1] = 0.0;
      ToSink[2][ map->NbMarqueur-1] = 0.0;
      ToSink[3][ map->NbMarqueur-1] = 0.0;
      break;
    case Obs0001 : /* connu, pr�sent */
      ToSink[0][ map->NbMarqueur-1] = 0.0;
      ToSink[1][ map->NbMarqueur-1] = 1.0;
      ToSink[2][ map->NbMarqueur-1] = 1.0;
      ToSink[3][ map->NbMarqueur-1] = 1.0;
      break;
    default:
      print_out(  "Unexpected observation in data set\n");
    }
     
  for (i = (map->NbMarqueur - 2); i >=0; i--)
    {
      ToSink[0][i] = 0.0;
      ToSink[1][i] = 0.0;
      ToSink[2][i] = 0.0;
      ToSink[3][i] = 0.0;

      PhenoR = PhenoL;
      PhenoL = GetEch(ordre[i], Ind);

      for (j = 0; j< RHD_NPossibles[PhenoR]; j++) {
	for (k = 0; k < RHD_NPossibles[PhenoL]; k++) {
	  norm = ProbArete(RHD_Possibles[PhenoL][k],RHD_Possibles[PhenoR][j],map->tr[i],map->ret);
	  ToSink[RHD_Possibles[PhenoL][k]][i] += ToSink[RHD_Possibles[PhenoR][j]][i+1] * norm;
	}
      }
      norm = ToSink[0][i]+ToSink[1][i]+ToSink[2][i]+ToSink[3][i];
      loglike += log10(norm);
      
      ToSink[0][i] /= norm;
      ToSink[1][i] /= norm;
      ToSink[2][i] /= norm;
      ToSink[3][i] /= norm;
    }
  
  // compter le retour a la source (le puits est inutile dans SourceTo)

  loglike+=log10(map->ret*map->ret*ToSink[3][0]+
	       (1.0-map->ret)*map->ret*(ToSink[1][0]+ToSink[2][0]) + 
	       (1.0-map->ret)*(1.0-map->ret)*ToSink[0][0]);

  return loglike;
}


//-----------------------------------------------------------------------------
//  Etape elementaire d' � Expectation � de EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - l'individu 
// - la carte
// - le vecteur d'expectation
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_RHD::ComputeOneExpected(
                BJS_RHD::EM_state& state,
                int Ind, 
				BJS_RHD::PCEType* pce)
//void BJS_RHD::ComputeOneExpected(int Ind, 
//				const Carte* map, 
//				double *expected)
{
 int j,sl,sr;
 int *ordre = pce->map->ordre;
 int PhenoR = GetEch(ordre[0], Ind);
 int PhenoL;
 double ProbBreak[3];
 double ProbRet[2];
 double prob;

 int NbM = pce->map->NbMarqueur;

#define EBREAK (NbM - 1)
#define ERETAINED NbM

 // Pour le 1er marqueur
 //EBreak += 2.0;  // la double cassure est sure
 pce->accumulate_expected(Ind, EBREAK, 2.0);
 // !!!
 //ERetained += SourceTo[1][0]+SourceTo[2][0]+SourceTo[3][0]+SourceTo[3][0];
 pce->accumulate_expected(Ind, ERETAINED, state.SourceTo[1][0]+state.SourceTo[2][0]+state.SourceTo[3][0]+state.SourceTo[3][0]);

 for (j = 0; j< pce->map->NbMarqueur - 1; j++)  {

     ProbBreak[2] = ProbBreak[1] = ProbBreak[0] = 0.0;
     ProbRet[1] = ProbRet[0] = 0.0;
     PhenoL = PhenoR;
     PhenoR = GetEch(ordre[j+1], Ind);

     CompProbArete(state, pce->map->tr[j],pce->map->ret);

     for (sl = 0; sl< RHD_NPossibles[PhenoL]; sl++)  {
       for (sr= 0; sr < RHD_NPossibles[PhenoR]; sr++)    {

	 ProbBreak[2] += state.SourceTo[RHD_Possibles[PhenoL][sl]][j]
	   * state.ProbAr2[RHD_Possibles[PhenoL][sl]][RHD_Possibles[PhenoR][sr]]
	   * state.ToSink[RHD_Possibles[PhenoR][sr]][j+1];
	 
	 ProbBreak[1] += state.SourceTo[RHD_Possibles[PhenoL][sl]][j]
	   * state.ProbAr[RHD_Possibles[PhenoL][sl]][RHD_Possibles[PhenoR][sr]]
	   * state.ToSink[RHD_Possibles[PhenoR][sr]][j+1];
	 
	 ProbBreak[0] += state.SourceTo[RHD_Possibles[PhenoL][sl]][j]
	   * state.ProbArNB[RHD_Possibles[PhenoL][sl]][RHD_Possibles[PhenoR][sr]]
	   * state.ToSink[RHD_Possibles[PhenoR][sr]][j+1];
	 
	 ProbRet[1] += state.SourceTo[RHD_Possibles[PhenoL][sl]][j]
	   * state.ProbArRet2[RHD_Possibles[PhenoL][sl]][RHD_Possibles[PhenoR][sr]]
	   * state.ToSink[RHD_Possibles[PhenoR][sr]][j+1];
	 
	 ProbRet[0] += state.SourceTo[RHD_Possibles[PhenoL][sl]][j]
	   * state.ProbArRet[RHD_Possibles[PhenoL][sl]][RHD_Possibles[PhenoR][sr]]
	   * state.ToSink[RHD_Possibles[PhenoR][sr]][j+1];
       }
     }
     
     prob = ProbBreak[2]+ ProbBreak[1]+ ProbBreak[0];
     pce->accumulate_expected(Ind, j, (ProbBreak[2]*2 +  ProbBreak[1])/prob);
     //expected[j] += (ProbBreak[2]*2 +  ProbBreak[1])/prob;
     pce->accumulate_expected(Ind, EBREAK, (ProbBreak[2]*2 +  ProbBreak[1])/prob);
     //EBreak += (ProbBreak[2]*2 +  ProbBreak[1])/prob;
     pce->accumulate_expected(Ind, ERETAINED, (ProbRet[1]*2 +  ProbRet[0]) / prob);
     //ERetained +=  (ProbRet[1]*2 +  ProbRet[0]) / prob;
   }
}
//-----------------------------------------------------------------------------
// Calcul du nombre d'occurrences de chque pattern (stat suffisantes
// pour le calcul de vraisemblance et des estimateurs de max. de
// vraisemblance 2pts)
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja alloue des 
// stats suffisantes
// Valeur de retour : aucune, effet de bord sur ss
//-----------------------------------------------------------------------------
void BJS_RHD::Prepare2pt(int m1, int m2, int *ss) const
{
  for (int m = 1; m <= TailleEchant; m++)    {
    
    if ((GetEch(m1, m) != Obs1111) && (GetEch(m2, m) != Obs1111)) {
      ss[GetEch(m1, m)*2 + GetEch(m2, m)]++;
    }
  }
}
//-----------------------------------------------------------------------------
// Calcul de l estimateur 2pt de max de vraisemblance
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des 
// stats suffisantes, le taux de cassure et de retention a estimer
// Valeur de retour : aucune, effet de bord sur les deux parametres
//-----------------------------------------------------------------------------
void BJS_RHD::Estimate2pt(double *par, int *ss) const
{
  int tot = ss[3]+ss[0]+ss[2]+ss[1];
  double r,theta;
  double nr;

  nr = sqrt((double)(ss[0]+ss[0]+ss[2]+ss[1])/(double)(2*tot));
  // taux de retention
  r = 1.0 -nr;
  
  if (nr == 0.0)
    theta = 1.0;
  else
    // taux de cassure
    theta = (nr-sqrt((double)ss[0]/(double)tot))/(r*nr);
  
  if (theta > EM_MAX_THETA_HR) theta = EM_MAX_THETA_HR;
  if (theta < EM_MIN_THETA_HR) theta =EM_MIN_THETA_HR;

  par[0] = theta;
  par[1] = r;
}
//-----------------------------------------------------------------------------
// Calcul de la vraisemblance 2pts
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des 
// stats suffisantes, le taux de cassure et de retention.
// Valeur de retour : le loglike
//-----------------------------------------------------------------------------
double BJS_RHD::LogLike2pt(double *par, int *ss) const
{
  double loglike,Prob00;
  double nrs = (1 - par[1])*(1 - par[1]);

  Prob00 = nrs*(1.0-par[1]*par[0])*(1.0-par[1]*par[0]);
  loglike = ss[0]*log10(Prob00);
  if (ss[1]+ss[2])
    loglike += (ss[1]+ss[2])*log10(nrs-Prob00);
  loglike += ss[3]*log10(1.0-2.0*nrs+Prob00);

  return loglike;
}
//-----------------------------------------------------------------------------
//  Calcul des distances, et estimations 2 points
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
double BJS_RHD::ComputeOneTwoPoints(int m1, 
				   int m2, 
				   double epsilon,
				   double *fr) const
{
  int n[4];
  int tot;
  double par[2];
  double loglike;

  n[0] = n[1] = n[2] = n[3] = 0;
  Prepare2pt(m1,m2,n);
  tot = n[0] + n[1] + n[2] + n[3];

  Estimate2pt(par,n);

  *fr = par[0];

  loglike = LogLike2pt(par,n);
  par[0] = 1.0;
  
  return loglike-LogLike2pt(par,n);
}
//-----------------------------------------------------------------------------
//  Pr�paration des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_RHD::PreparEM(const Carte *data)
{
//    for (int i = 0; i < 4; i++)
//    {
//      SourceTo[i] = new double[data->NbMarqueur];
//      ToSink[i] = new double[data->NbMarqueur];
//    }
}

//-----------------------------------------------------------------------------
//  nettoyage des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres :
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_RHD::NetEM(const Carte *data)
{
//  for (int i = 0; i < 4; i++)
//    {
//      delete SourceTo[i];
//      delete ToSink[i];
//    }
}

