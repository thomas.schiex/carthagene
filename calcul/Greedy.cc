//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Greedy.cc,v 1.40.2.1 2012-08-02 15:43:55 dleroux Exp $
//
// Description : Greedy (le glouton).
// Divers :
//-----------------------------------------------------------------------------


//  -------------------------------------------------------------------
//    Une tentative de Greedy local search pour le BC
//    -------------------------------------------------------------------

//  quelques idees:
//    - random walk
//    - ajouter hash table separee (plus grande) pour detection des boucles
//    car on peut rester a tourner sur un plateau eloigne de l'optimum
//    puisque seules les N meilleures sont conservees
//    - ajouter autre voisinage
//    - ne pas explorer tout le voisinage, surtout au debut
//    - faire du random biased sampling
//    - faire de l'exploration en parallele au debut (style AG), puis
//    focalisation par suppression de traces communes
//    - ajouter de nouvelles heuristiques


//    - danger: si on ajoute 3 en cas de saut, il faut inhiber la
//    possibilite de resauter pdt >3 ou risque de non terminaison !
   
//    Plus tard:
   
//    rajouter un voisinage base sur la decomposition en cartes intra
//    consistant a retourner l'ensemble des marqueurs d'une des cartes
//    (si faible nombre de communs) dans la carte globale

//    traiter en simultane intra et extra

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <search.h>
#include "CartaGene.h"
#include "BioJeu.h"
#include "BioJeuMerged.h"
#include "BJM_OR.h"
#include "tsp.h"
#ifdef CGBENCH
int insertPP(Tas *tas, double lastbestcost, Carte *map, int iter);
#endif


//-----------------------------------------------------------------------------
// Lancement du glouton.
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void CartaGene::Greedy(int NR, 
		       int NI,
		       int TMin, 
		       int TMax,
		       int Ratio)
{
  if (Ratio < 0 || Ratio > 100)
    {
      print_err(  "Error : value expected for Ratio in [0,100].\n");
      return;
    }
  // Note: GreedyNew(...,100) != GreedyOld(...) car GreedyNew arrete exploration
  //       du voisinage desque meilleure solution trouvee
  if (Ratio == 0) GreedyOld(NR, NI, TMin, TMax);
  else GreedyNew(NR, NI, TMin, TMax, Ratio);
}

void CartaGene::GreedyOld(int NR, 
			  int NI,
			  int TMin, 
			  int TMax)
{

  // quelques v�rifications 

  if (Heap->HeapSize == 0) 
    {
      print_err(  "Error : Empty heap.\n");
      return;
    }
  
  if (NR < 1)
    {
      print_err(  "Error : value expected for NR : > 0.\n");
      return;
    }

  //  if (NI < 0)    {
  //      sprintf(bouf,  "Error : value expected for NI : >= 0.\n");
  //      perr(bouf);
  //      return;   }

  if ((TMin < 0) || (TMin > TMax))
    {
      print_err(  "Error : value expected for Tmin : 0 =< & < Tmax.\n");
      return;
    } 
  
  if ((TMax < 1) || (TMax < TMin))
    {
      print_err(  "Error : value expected for Tmax : 0 =<  & > Tmin.\n");
      return;
    } 
  // fin des v�rifications

  int NbRun;
  int NbIter;
  int TabooMin, TabooMax;
  double finalcost;
  double cost, localbest;
  int Taboolen;
  int TabooLenMin;
  int TabooLenMax;
  Carte FinalMap(this, NbMS, MarkSelect);
  Carte TheMap(this, NbMS, MarkSelect);
  Carte TempMap(this, NbMS, MarkSelect);
  Carte *InitMap;
  int iter;
  int beste1, beste2;
  // bwlen initialisee car pourrait passer dans test sans avoir recue de valeur
  int e1, e2, wlen, bwlen = 0;
  int i, j;
  int Improve;
  int NeighSize;
  int StarMax = NbMS;
  intPtr *Age;

  if (NI < 0) NbIter = -NI;
  else NbIter = NI+2*NbMS;

  NbRun = NR;

  iter = 0;

  TabooLenMin = TMin;
  TabooLenMax = TMax;
  NeighSize = ((NbMS-2)*(NbMS-1))/2;
  TabooMin =(TabooLenMin*NeighSize)/100;
  TabooMax =(TabooLenMax*NeighSize)/100;

  Age = new intPtr[NbMS-1];
  for (i=0; i<NbMS-1; i++) Age[i] = new int [NbMS+1];

  InitMap = Heap->Best();
  InitMap->CopyFMap(&TheMap);

  finalcost = ComputeEM(&TheMap);
  TheMap.CopyFMap(&FinalMap);
#ifdef CGBENCH
  insertPP(&Heap, finalcost, &TheMap, iter);
#else
  Heap->Insert(&TheMap, iter);
#endif
  if (!QuietFlag) PrintMap(&TheMap);
  
  /* Run loop */
  for (j = 0; j< NbRun; j++) {
    if (!QuietFlag) {
      print_out( "Run number %d\n", j);
    }
    flush_out();

    for (e1 = 0; e1 < NbMS-1; e1++)
      for (e2 = e1 + 2; e2 <= NbMS; e2++)
	Age[e1][e2] = INT_MIN;
    /* Iteration loop */
    for (i = 0; i< NbIter; i++) {
      /* Greedy Step */
      localbest = -1e100;
      beste1 = beste2 = 0;
      Improve = 0;
      Taboolen = randomax(TabooMax-TabooMin+1)+TabooMin;
      for (e1 = 0; e1 < NbMS-1; e1++)
	for (e2 = e1 + 2; e2 <= NbMS-(e1==0); e2++)
        {
	  TheMap.CopyMap(&TempMap);
	  TempMap.Apply2Change(e1, e2);

	  cost = ComputeEMS(&TempMap, finalcost-2.0);
	  
#ifdef CGBENCH
	  wlen = insertPP(&Heap, finalcost, &TempMap, iter);
#else
	  wlen = Heap->Insert(&TempMap, iter);
#endif

	  if (VerboseFlag>1) {
	    print_out( "2-change: (%d,%d), Wlen: %d, DLogLike: %f\n",
		   e1, e2, wlen, finalcost-cost);
	  }
	  flush_out();
	  /* localbest est le meilleur cout possible pour l'it�ration
	     Ca peut �tre un cout de Taboo si aspiration */
	  
	  if (cost > finalcost+0.000001)
          {
	    localbest = finalcost = cost;
	    beste1 = e1;
	    beste2 = e2;
	    Improve = 1;
	    TempMap.CopyFMap(&FinalMap);
	  } else if ((cost > localbest) && (Age[e1][e2] <  i - Taboolen)) {
	    localbest = cost;
	    beste1 = e1;
	    beste2 = e2;
	    bwlen = wlen;
	  }
	  
	  // traitement de la demande d'arr�t.
	  if ( StopFlag )
	    { 
	      StopFlag = 0;
	      
	      // lib�ration
	      
	      for (i=0; i<NbMS-1; i++) delete [] Age[i];
	      delete [] Age;
	      
	      // 
	      print_out(  "Aborted!\n");
	      flush_out();
	      return;
	    }
	}
      if (bwlen) {
	TheMap.BuildNewMapL();
	localbest = ComputeEMS(&TheMap, finalcost-2.0);
#ifdef CGBENCH
	insertPP(&Heap, finalcost, &TheMap, iter);
#else
	Heap->Insert(&TheMap, iter);
#endif
	Improve = 2;
      } else {
	if (VerboseFlag>1) {
          print_out( "2-change applied: (%d,%d)\n", beste1, beste2);
	}
	TheMap.Apply2Change(beste1, beste2);
	Age[beste1][beste2] = i;
      }

      flush_out();
      if (!QuietFlag) switch (Improve)
	{
	case 0:
	  print_out(  "-");
	  break;
	case 1:
	  print_out( "\n[%3.2f]",finalcost);
	  break;
	case 2:
	  print_out(  "*");
	  StarMax--;
	  if (StarMax <= 0) Improve = 0;
	  break;
	}
      flush_out();

      NbIter += Improve;
      iter++;
      if (VerboseFlag)  {
	print_out( "(%3.1f) ",(localbest-finalcost));
	PrintMap(&TheMap);
      }
      flush_out();
    }
    
    TheMap.BuildNewMapL();
    cost = ComputeEMS(&TheMap, finalcost-2.0);
#ifdef CGBENCH
    insertPP(&Heap, finalcost, &TheMap, iter);
#else
    Heap->Insert(&TheMap, iter);
#endif
    if (cost > finalcost) {
      finalcost = cost;
      TheMap.CopyFMap(&FinalMap);
    }

  }

  for (i=0; i<NbMS-1; i++) delete [] Age[i];
  delete [] Age;

  print_out(  "\n");
}

/* ************************************************************************ */
/* ************************************************************************ */

#define EPSILON 0.01

typedef int (*cmp_t)(const void *, const void *);

int HeuristicLOD = 1; // = 1 : HeuristicDist = LOD 2pts , = 0 : HeuristicDist = F.Rec. 
double HeuristicDist[MAXNBMARKERS];
double HeuristicDistWorst = -1.0;
double HeuristicDistBest = 1e100;
int NbMrk = -1; // = NbMS // car NbMS inconnu dans la fonction trpair

inline int min(int a, int b)
{
  return ((a<b)?a:b);
}
inline int max(int a, int b)
{
  return ((a>b)?a:b);
}

inline double min(double a, double b)
{
  return ((a<b)?a:b);
}
inline double max(double a, double b)
{
  return ((a>b)?a:b);
}

inline double meandist(int m1, int m2)
{
  double trm1 = ((m1 == 0)?HeuristicDistWorst:HeuristicDist[m1-1]);
  double trm2 = ((m2 < NbMrk)?HeuristicDist[m2-1]:HeuristicDistWorst);
  return (trm1 + trm2) / 2.;
}

int cmpMrkPairsMean(int *p1, int *p2)
{
  double trp1 = meandist(p1[0], p1[1]);
  double trp2 = meandist(p2[0], p2[1]);

  return ((trp1 > trp2)?-1:((trp1 == trp2)?0:1));
}

int cmpMrkPairsMin(int *p1, int *p2)
{
  int m1_1 = p1[0];
  int m1_2 = p1[1];
  double tr1_1min = ((m1_1 == 0)?HeuristicDistBest:HeuristicDist[m1_1-1]);
  double tr1_2min = ((m1_2 < NbMrk)?HeuristicDist[m1_2-1]:HeuristicDistBest);
  double tr1_1max = ((m1_1 == 0)?HeuristicDistWorst:HeuristicDist[m1_1-1]);
  double tr1_2max = ((m1_2 < NbMrk)?HeuristicDist[m1_2-1]:HeuristicDistWorst);
  
  int m2_1 = p2[0];
  int m2_2 = p2[1];
  double tr2_1min = ((m2_1 == 0)?HeuristicDistBest:HeuristicDist[m2_1-1]);
  double tr2_2min = ((m2_2 < NbMrk)?HeuristicDist[m2_2-1]:HeuristicDistBest);
  double tr2_1max = ((m2_1 == 0)?HeuristicDistWorst:HeuristicDist[m2_1-1]);
  double tr2_2max = ((m2_2 < NbMrk)?HeuristicDist[m2_2-1]:HeuristicDistWorst);
  
  if (min(tr1_1min, tr1_2min) < min(tr2_1min, tr2_2min) + EPSILON)
    return -1;
  else if (min(tr1_1min, tr1_2min) > min(tr2_1min, tr2_2min) - EPSILON)
    return 1;
  else {
    double tr1;
    double tr2;
    if (tr1_1min < tr1_2min) tr1 = tr1_2max;
    else tr1 = tr1_1max;
    if (tr2_1min < tr2_2min) tr2 = tr2_2max;
    else tr2 = tr2_1max;
    return ((tr1 < tr2)?-1:((tr1 == tr2)?0:1));
  }
}

int cmpMrkPairsMax(int *p1, int *p2)
{
  int m1_1 = p1[0];
  int m1_2 = p1[1];
  double tr1_1min = ((m1_1 == 0)?HeuristicDistBest:HeuristicDist[m1_1-1]);
  double tr1_2min = ((m1_2 < NbMrk)?HeuristicDist[m1_2-1]:HeuristicDistBest);
  double tr1_1max = ((m1_1 == 0)?HeuristicDistWorst:HeuristicDist[m1_1-1]);
  double tr1_2max = ((m1_2 < NbMrk)?HeuristicDist[m1_2-1]:HeuristicDistWorst);
  
  int m2_1 = p2[0];
  int m2_2 = p2[1];
  double tr2_1min = ((m2_1 == 0)?HeuristicDistBest:HeuristicDist[m2_1-1]);
  double tr2_2min = ((m2_2 < NbMrk)?HeuristicDist[m2_2-1]:HeuristicDistBest);
  double tr2_1max = ((m2_1 == 0)?HeuristicDistWorst:HeuristicDist[m2_1-1]);
  double tr2_2max = ((m2_2 < NbMrk)?HeuristicDist[m2_2-1]:HeuristicDistWorst);
  
  if (max(tr1_1min, tr1_2min) > max(tr2_1min, tr2_2min) + EPSILON)
    return -1;
  else if (max(tr1_1min, tr1_2min) < max(tr2_1min, tr2_2min) - EPSILON)
    return 1;
  else {
    double tr1;
    double tr2;
    if (tr1_1min > tr1_2min) tr1 = tr1_2max;
    else tr1 = tr1_1max;
    if (tr2_1min > tr2_2min) tr2 = tr2_2max;
    else tr2 = tr2_1max;
    return ((tr1 > tr2)?-1:((tr1 == tr2)?0:1));
  }
}

void CartaGene::GreedyNew(int NR, 
			  int NI,
			  int TMin, 
			  int TMax,
			  int Ratio)
{

  // quelques v�rifications 

  if (Heap->HeapSize == 0) 
    {
      print_err(  "Error : Empty heap.\n");
      return;
    }
  
  if (NR < 1)
    {
      print_err(  "Error : value expected for NR : > 0.\n");
      return;
    }

  //  if (NI < 0)    {
  //      sprintf(bouf,  "Error : value expected for NI : >= 0.\n");
  //      perr(bouf);
  //      return;   }

  if ((TMin < 0) || (TMin > TMax))
    {
      print_err(  "Error : value expected for Tmin : 0 =< & < Tmax.\n");
      return;
    } 
  
  if ((TMax < 1) || (TMax < TMin))
    {
      print_err(  "Error : value expected for Tmax : 0 =<  & > Tmin.\n");
      return;
    } 
  // fin des v�rifications

  // tableau pour trie de tout le voisinage avant retenue d'une partie seulement
  int *sortedmrk = new int[(NbMS+1) * (NbMS+1)];
  int bestposition; // uniquement pour affichage

  int NbRun;
  int NbIter;
  int TabooMin, TabooMax;
  double finalcost;
  double cost, localbest;
  int Taboolen;
  int TabooLenMin;
  int TabooLenMax;
  Carte FinalMap(this, NbMS, MarkSelect);
  Carte TheMap(this, NbMS, MarkSelect);
  Carte TempMap(this, NbMS, MarkSelect);
  Carte *InitMap;
  int iter;
  int beste1, beste2;
  // bwlen initialisee car pourrait passer dans test sans avoir recue de valeur
  int e1, e2, wlen, bwlen = 0;
  int i, j;
  int Improve;
  int NeighSize;
  int StarMax = NbMS;
  intPtr *Age;

  if (NI < 0) NbIter = -NI;
  else NbIter = NI+2*NbMS;

  NbRun = NR;

  iter = 0;

  TabooLenMin = TMin;
  TabooLenMax = TMax;
  NeighSize = (int)((double) Ratio * (((NbMS-2)*(NbMS-1))/2) / 100.);
  TabooMin =(TabooLenMin*NeighSize)/100;
  TabooMax =(TabooLenMax*NeighSize)/100;

  Age = new intPtr[NbMS-1];
  for (i=0; i<NbMS-1; i++) Age[i] = new int [NbMS+1];

  InitMap = Heap->Best();
  InitMap->CopyFMap(&TheMap);

  finalcost = ComputeEM(&TheMap);
  TheMap.CopyFMap(&FinalMap);
#ifdef CGBENCH
  insertPP(&Heap, finalcost, &TheMap, iter);
#else
  Heap->Insert(&TheMap, iter);
#endif
  if (ArbreJeu->Cross == Mor) {
    HeuristicLOD = 1;
    HeuristicDistWorst = -1.;
    HeuristicDistBest = 1e100;
  } else {
    HeuristicLOD = 0;
    HeuristicDistWorst = 1.;
    HeuristicDistBest = 0.;
  }
  for (int idx=0; idx<NbMS-1; idx++) {
    if (HeuristicLOD)
      HeuristicDist[idx] = GetTwoPointsLOD(TheMap.ordre[idx], TheMap.ordre[idx+1]);
    else
      HeuristicDist[idx] = TheMap.tr[idx];
  }
  if (!QuietFlag) PrintMap(&TheMap);
  
  NbMrk = NbMS;
#ifdef CGDEBUG
  printf("--- Neighborhood Size Ratio = %d\n", Ratio);
#endif

  /* Run loop */
  for (j = 0; j< NbRun; j++) {
    if (!QuietFlag) {
      print_out( "Run number %d\n", j);
    }
    flush_out();

    for (e1 = 0; e1 < NbMS-1; e1++)
      for (e2 = e1 + 2; e2 <= NbMS; e2++)
	Age[e1][e2] = INT_MIN;
    /* Iteration loop */
    for (i = 0; i< NbIter; i++) {
      /* Greedy Step */
      localbest = -1e100;
      beste1 = beste2 = 0;
      Improve = 0;
      Taboolen = randomax(TabooMax-TabooMin+1)+TabooMin;

      int nbpairs = 0;
      for (e1 = 0; e1 < NbMS-1; e1++) {
	for (e2 = e1 + 2; e2 <= NbMS-(e1==0); e2++) {
	  sortedmrk[nbpairs * 2] = e1;
	  sortedmrk[nbpairs * 2 + 1] = e2;
	  nbpairs++;
	}
      }
      qsort(sortedmrk, nbpairs, sizeof(int) * 2, (cmp_t) ((HeuristicLOD)?cmpMrkPairsMin:cmpMrkPairsMax));
#ifdef CGDEBUG
      printf("\n");
      for (int index=0; index<NbMS; index++) {
	printf(" %4d", TheMap.ordre[index]);
      } 
      printf("\n");
      for (int index=0; index<=NbMS; index++) {
	printf(" %4d", index);
      } 
      printf("\n");
      for (int index=0; index<=NbMS; index++) {
	printf(" %1.2f", (index == 0 || index == NbMS)?HeuristicDistWorst:HeuristicDist[index-1]);
      } 
      printf("\n");
      for (int index=0; index<nbpairs; index++) {
	printf("%.3f,%d,%d ", meandist(sortedmrk[index * 2], sortedmrk[index * 2 + 1]), sortedmrk[index * 2], sortedmrk[index * 2 + 1]);
      }
      printf("\n");
#endif
      // au minimum, examen des permutations des extremites
      int nbpairselected = min(nbpairs, max(NbMS * 2, (int) (((double) Ratio * nbpairs) / 100.)));
#ifdef CGDEBUG
      printf("\nnbpairselected = %d (nbpairs= %d,Ratio= %d)\n", nbpairselected, nbpairs, Ratio);
#endif
      for (int idpair = 0; idpair < nbpairselected; idpair++)
        {
	  e1 = sortedmrk[idpair * 2];
	  e2 = sortedmrk[idpair * 2 + 1];

	  TheMap.CopyMap(&TempMap);
	  TempMap.Apply2Change(e1, e2);

	  cost = ComputeEMS(&TempMap, finalcost-2.0);
	  
#ifdef CGBENCH
	  wlen = insertPP(&Heap, finalcost, &TempMap, iter);
#else
	  wlen = Heap->Insert(&TempMap, iter);
#endif

	  if (VerboseFlag>1) {
	    print_out( "2-change: (%d,%d), Wlen: %d, DLogLike: %f\n",
		   e1, e2, wlen, finalcost-cost);
	  }
	  flush_out();
	  /* localbest est le meilleur cout possible pour l'it�ration
	     Ca peut �tre un cout de Taboo si aspiration */
	  
	  if (cost > finalcost+0.000001)
          {
	    localbest = finalcost = cost;
	    beste1 = e1;
	    beste2 = e2;
	    bestposition = idpair;
	    for (int idx=0; idx<NbMS-1; idx++) {
	      if (HeuristicLOD)
		HeuristicDist[idx] = GetTwoPointsLOD(TempMap.ordre[idx], TempMap.ordre[idx+1]);
	      else
		HeuristicDist[idx] = TempMap.tr[idx];
	    }
	    Improve = 1;
	    TempMap.CopyFMap(&FinalMap);
	    goto Jump1;
	  } else if ((cost > localbest) && (Age[e1][e2] <  i - Taboolen)) {
	    localbest = cost;
	    beste1 = e1;
	    beste2 = e2;
	    bestposition = idpair;
	    for (int idx=0; idx<NbMS-1; idx++) {
	      if (HeuristicLOD)
		HeuristicDist[idx] = GetTwoPointsLOD(TempMap.ordre[idx], TempMap.ordre[idx+1]);
	      else
		HeuristicDist[idx] = TempMap.tr[idx];
	    }
	    bwlen = wlen;
	  }
	  
	  // traitement de la demande d'arr�t.
	  if ( StopFlag )
	    { 
	      StopFlag = 0;
	      
	      // lib�ration
	      
	      for (i=0; i<NbMS-1; i++) delete [] Age[i];
	      delete [] Age;
	      
	      // 
	      print_out(  "Aborted!\n");
	      flush_out();
	      return;
	    }
      }
    Jump1:
      if (bwlen) {
	TheMap.BuildNewMapL();
	localbest = ComputeEMS(&TheMap, finalcost-2.0);
#ifdef CGBENCH
	insertPP(&Heap, finalcost, &TheMap, iter);
#else
	Heap->Insert(&TheMap, iter);
#endif
	for (int idx=0; idx<NbMS-1; idx++) {
	  if (HeuristicLOD)
	    HeuristicDist[idx] = GetTwoPointsLOD(TheMap.ordre[idx], TheMap.ordre[idx+1]);
	  else
	    HeuristicDist[idx] = TheMap.tr[idx];
	}
	Improve = 2;
      } else {
	if (VerboseFlag>1) {
          print_out( "2-change applied: (%d,%d)\n", beste1, beste2);
	}
#ifdef CGDEBUG
	double mindist = 1e100;
	double maxdist = -1e100;
	double meandist = 0.;
	for (int indice=0; indice<NbMS-1; indice++) {
	  meandist += HeuristicDist[indice];
	  if (HeuristicDist[indice] > maxdist) maxdist = HeuristicDist[indice];
	  if (HeuristicDist[indice] < mindist) mindist = HeuristicDist[indice];
	}	
	meandist = meandist / (NbMS-1);
	printf("HeuristicDist: %1.3f(%3d) %1.3f(%3d) min = %1.3f mean = %1.6f max = %1.3f\n", (beste1>0)?HeuristicDist[beste1-1]:HeuristicDistWorst, beste1, (beste2<NbMarqueur)?HeuristicDist[beste2-1]:HeuristicDistWorst, beste2, mindist, meandist, maxdist);

	printf("pos= %f %% Improve= %d\n", 100. * bestposition / nbpairs, Improve);
#endif
	TheMap.Apply2Change(beste1, beste2);
	Age[beste1][beste2] = i;
      }

      flush_out();
      if (!QuietFlag) switch (Improve)
	{
	case 0:
	  print_out(  "-");
	  break;
	case 1:
	  print_out( "\n[%3.2f]",finalcost);
	  break;
	case 2:
	  print_out(  "*");
	  StarMax--;
	  if (StarMax <= 0) Improve = 0;
	  break;
	}
      flush_out();

      NbIter += Improve;
      iter++;
      if (VerboseFlag)  {
	print_out( "(%3.1f) ",(localbest-finalcost));
	PrintMap(&TheMap);
      }
      flush_out();
    }
    
    TheMap.BuildNewMapL();
    cost = ComputeEMS(&TheMap, finalcost-2.0);
#ifdef CGBENCH
    insertPP(&Heap, finalcost, &TheMap, iter);
#else
    Heap->Insert(&TheMap, iter);
#endif
    for (int idx=0; idx<NbMS-1; idx++) {
      if (HeuristicLOD)
	HeuristicDist[idx] = GetTwoPointsLOD(TheMap.ordre[idx], TheMap.ordre[idx+1]);
      else
	HeuristicDist[idx] = TheMap.tr[idx];
    }
    
    if (cost > finalcost) {
      finalcost = cost;
      TheMap.CopyFMap(&FinalMap);
    }

  }

  for (i=0; i<NbMS-1; i++) delete [] Age[i];
  delete [] Age;

  delete [] sortedmrk;

  print_out(  "\n");
}

int breakpointsmap(CartaGene *Cartage, int jeuOrdre, Carte *map);
int breakpointsmap(CartaGene *Cartage, int jeuOrdre, Carte *map)
{
  if ( Cartage->Jeu[jeuOrdre]->Cross == Mor) {
    return breakpointsmap(Cartage, ((BJM_OR *) (Cartage->Jeu[jeuOrdre]))->BJgauche->Id,map) + breakpointsmap(Cartage, ((BJM_OR *) (Cartage->Jeu[jeuOrdre]))->BJdroite->Id,map);
  }
  if ( Cartage->Jeu[jeuOrdre]->Cross != Ordre) return 0;
  return (Cartage->Jeu[jeuOrdre]->BreakPointsMap(map));
}

void CartaGene::ParetoGreedy(int Ratio)
{
  ParetoGreedy(Ratio,0,INT_MAX);
}

void CartaGene::ParetoGreedy(int Ratio, int minbp, int maxbp)
{
  int i,bp,idx,e1,e2,idpair;

  /* some consistency checks */
  if (Ratio<=0)
    {
      print_err( "Error: Anytime Ratio (%d) should be strictly greater than zero.\n", Ratio);
      return;
    }
  if (minbp > maxbp) {
      print_err( "Error: empty breapoint range (MinBP=%d > MaxBP=%d).\n",minbp,maxbp);
      return;
  }
  if (minbp<0) minbp = 0;
  if (NbMS == 0) 
    {
      print_err(  "Error: Empty selection of loci.\n");
      return;
    }
  int ok = 0;
  for (i=1; i<=NbJeu; i++) {
    if (Jeu[i]->Cross == Ordre) {
      ok = 1;
      break;
    }
  }
  if (!ok) {
    print_err("Error: there is no reference order data set loaded! The current data set should be merged by order from one Biological Data Set and one reference order data set.\n");
    return;
  }

  int *sortedmrk = new int[(NbMS+1) * (NbMS+1)];

  Carte **PseudoPareto = new cartePtr[NbMS];
  double *PseudoParetoTrueLikelihood = new double[NbMS];
  int *PseudoParetoVisited = new int[NbMS];
  int *PseudoParetoCounter = new int[NbMS];
  int *PseudoParetoStatus = new int[NbMS];
  int MaxBP = 0;

  for (bp=0; bp<NbMS; bp++) {
    PseudoPareto[bp] = NULL;
    PseudoParetoStatus[bp] = 0;
    PseudoParetoVisited[bp] = 0;
    PseudoParetoTrueLikelihood[bp] = 0;
    PseudoParetoCounter[bp] = 0;
  }
  SetBreakPointCoef(ArbreJeu->Id, 0.);
  /* fill in the current Pareto frontier by evaluating the maps in the Carthagene's heap */
  for (i=0; i<Heap->HeapSize; i++) {
    /* recompute true log-like without breakpoint */
    Carte *mymap = Heap->MapFromId(i);
    mymap->UnConverge();
    double rh = ComputeEM(Heap->MapFromId(i));
    int bp = BreakPointsMap(ArbreJeu->Id, i);
    if (bp > MaxBP) MaxBP = bp;
    if (PseudoPareto[bp] == NULL || PseudoParetoTrueLikelihood[bp] < rh - PARETOEPSILON) {
      if (PseudoPareto[bp]!=NULL) delete PseudoPareto[bp];
      Carte *newmap = new Carte(*Heap->MapFromId(i));
      PseudoPareto[bp] = newmap;
      PseudoParetoTrueLikelihood[bp] = rh;
    }
  }
  if (MaxBP < maxbp) maxbp = MaxBP;
  /* resize the heap */
  ResizeHeap(MAXHEAPSIZETSP);
  int MaxNumberOfMapPerBP = MAXHEAPSIZETSP / NbMS - 3;
  if (MaxNumberOfMapPerBP<1) MaxNumberOfMapPerBP = 1;

  if (ArbreJeu->Cross == Mor &&
      !((((BioJeuMerged *)ArbreJeu)->BJgauche->Cross==RH || ((BioJeuMerged *)ArbreJeu)->BJgauche->Cross==BC) && 
	((BioJeuMerged *)ArbreJeu)->BJdroite->Cross==Ordre) &&
      !(((BioJeuMerged *)ArbreJeu)->BJgauche->Cross==Ordre && 
	(((BioJeuMerged *)ArbreJeu)->BJdroite->Cross==RH || ((BioJeuMerged *)ArbreJeu)->BJdroite->Cross==BC))) {
    HeuristicLOD = 1;
    HeuristicDistWorst = -1.;
    HeuristicDistBest = 1e100;
  } else {
//      sprintf(bouf,"sorted neighborhood using recombination theta!\n");
//      pout(bouf);    
    HeuristicLOD = 0;
    HeuristicDistWorst = 1.;
    HeuristicDistBest = 0.;
  }

  /* iterate until all maps in PseudoPareto have been visited */
  int NextMapToBeVisited = -1;
  if (Heap->HeapSize >= Heap->MaxHeapSize - 1)
    { 
      print_out(  "Heap already full! (%d maps)\n", Heap->HeapSize);
      flush_out();
      goto JumpParetoGreedy;
    }
  do {
    NextMapToBeVisited = -1;
    for (bp=minbp; bp<=maxbp; bp++) {  // bp<NbMS  // warning! do not improve maps with large BP
      if (PseudoPareto[bp] != NULL && PseudoParetoVisited[bp] == 0) {
	PseudoParetoVisited[bp] = 1;
	NextMapToBeVisited = bp;
	break;
      }
    }
    if (NextMapToBeVisited >= 0) {
      //      sprintf(bouf,"Try to improve map with %d breakpoints\r",NextMapToBeVisited);
      //      pout(bouf);

      Carte *TheMap = PseudoPareto[NextMapToBeVisited];
      Carte TempMap(this, NbMS, MarkSelect);

      for (idx=0; idx<NbMS-1; idx++) {
	if (HeuristicLOD)
	  HeuristicDist[idx] = GetTwoPointsLOD(TheMap->ordre[idx], TheMap->ordre[idx+1]);
	else
	  HeuristicDist[idx] = TheMap->tr[idx];
      }
#ifdef CGDEBUG
      printf("--- Neighborhood Size Ratio = %d\n", Ratio);
#endif
      int nbpairs = 0;
      for (e1 = 0; e1 < NbMS-1; e1++) {
	for (e2 = e1 + 2; e2 <= NbMS-(e1==0); e2++) {
	  sortedmrk[nbpairs * 2] = e1;
	  sortedmrk[nbpairs * 2 + 1] = e2;
	  nbpairs++;
	}
      }
      qsort(sortedmrk, nbpairs, sizeof(int) * 2, (cmp_t) ((HeuristicLOD)?cmpMrkPairsMin:cmpMrkPairsMax));
#ifdef CGDEBUG
      printf("\n");
      for (index=0; index<NbMS; index++) {
	printf(" %4d", TheMap->ordre[index]);
      } 
      printf("\n");
      for (index=0; index<=NbMS; index++) {
	printf(" %4d", index);
      } 
      printf("\n");
      for (index=0; index<=NbMS; index++) {
	printf(" %1.2f", (index == 0 || index == NbMS)?HeuristicDistWorst:HeuristicDist[index-1]);
      } 
      printf("\n");
      for (index=0; index<nbpairs; index++) {
	printf("%.3f,%d,%d ", meandist(sortedmrk[index * 2], sortedmrk[index * 2 + 1]), sortedmrk[index * 2], sortedmrk[index * 2 + 1]);
      }
      printf("\n");
#endif
      // au minimum, examen des permutations des extremites
      int nbpairselected = min(nbpairs, max(NbMS * 2, (int) (((double) Ratio * nbpairs) / 100.)));
#ifdef CGDEBUG
      printf("\nnbpairselected = %d (nbpairs= %d,Ratio= %d)\n", nbpairselected, nbpairs, Ratio);
#endif
      for (idpair = 0; idpair < nbpairselected; idpair++)
        {
	  int e1 = sortedmrk[idpair * 2];
	  int e2 = sortedmrk[idpair * 2 + 1];

	  TheMap->CopyMap(&TempMap);
	  TempMap.Apply2Change(e1, e2);

	  double rh = ComputeEM(&TempMap);
	  int bp = breakpointsmap(this, ArbreJeu->Id, &TempMap);

	  if (PseudoPareto[bp] == NULL || PseudoParetoTrueLikelihood[bp] < rh - PARETOEPSILON) {
	    //	    if (PseudoPareto[bp]!=NULL) delete PseudoPareto[bp];
	    Carte *newmap = new Carte(TempMap);
	    PseudoPareto[bp] = newmap;
	    PseudoParetoTrueLikelihood[bp] = rh;
	    PseudoParetoVisited[bp] = 0;
	    PseudoParetoStatus[bp] |= PARETO2OPT;
	    Heap->Insert(&TempMap, 0);
	    print_out("[BP: %d Log10-likelihood: %.3f]\n", bp, rh);
	  } else {
	    PseudoParetoCounter[bp]++;
	    if (PseudoParetoCounter[bp] <= MaxNumberOfMapPerBP) {
	      Heap->Insert(&TempMap, 0);
	    }
	  }

	  if ( StopFlag || (Heap->HeapSize >= Heap->MaxHeapSize - 1) )
	    { 
	      StopFlag = 0;
	      if (Heap->HeapSize >= Heap->MaxHeapSize - 1) {
		print_out(  "Heap is full! (%d maps)\n", Heap->HeapSize);
	      }
	      print_out(  "Aborted!\n");
	      flush_out();
	      goto JumpParetoGreedy;
	    }
	}
    }
  } while (NextMapToBeVisited >= 0);

 JumpParetoGreedy:
  ParetoInfo(0, NULL, NULL, PseudoParetoStatus, 1, NULL);

  /* free memory */
  for (i = 0; i<NbMS; i++) {
    if (PseudoPareto[i] != NULL) delete PseudoPareto[i];
  }
  delete[] PseudoPareto;
  delete[] PseudoParetoTrueLikelihood;
  delete[] PseudoParetoVisited;
  delete[] PseudoParetoCounter;
  delete[] PseudoParetoStatus;
  delete[] sortedmrk;
}
