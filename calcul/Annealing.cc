//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Annealing.cc,v 1.26.2.1 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Annealing (le recuit).
// Divers :
//-----------------------------------------------------------------------------

#include "CartaGene.h"

#include <stdio.h>
#include <math.h> 

//-----------------------------------------------------------------------------
// Lancement du recuit.
//-----------------------------------------------------------------------------

void CartaGene::Annealing(int Tries,
			  double Tinit,
			  double Tfinal,
			  double Cooling){

  // quelques vérifications 
  
  if (Heap->HeapSize == 0) {
    print_err(  "Error : Empty heap.\n");
    return;
  }
  
  if (NbMS <=4) {
    print_err(  "Not enough selected markers (%d), use flips.\n",NbMS);
    return;
  }

  if ( (Cooling < 0.0) || ( Cooling > 1.0)) {
    print_err(  "Error : value expected for Cooling : 0.0 <  & < 1.0.\n");
    return;
  }

  if ( Tfinal < 0.0 || Tfinal >= Tinit) {
    print_err(  "Error : value expected for the final temperature: 0.0 <  & < intial temperature.\n");
    return;
  }

  if ( Tinit < 0.0 || Tinit <= Tfinal) {
    print_err(  "Error : value expected for the initial temperature: 0.0 <  & > final temperature\n");
    return;
  }

  if (Tries <= 0) {
    print_err(  "Error : value expected for the number of tries:  > 0.\n");
    return;
  }

  // fin des vérifications

  double energychange, t;
  int j;
  int iter;
  double thecost, cost, finalcost;
  Carte TheMap(this, NbMS, MarkSelect);
  Carte TempMap(this, NbMS, MarkSelect);
  Carte FinalMap(this, NbMS, MarkSelect);
  Carte *InitMap;
  int im1, im2; // indice de position de marqueurs.
  int wlen, chg, plus;
  // Drapeau pour adapter la température initiale
  // si la tempéraure es
  int AdaptFlag = 0; 
  
  if (wordre == NULL) delete wordre;
  wordre = new int[NbMS];

  Tries = Tries+NbMS*NbMS;

  InitMap = Heap->Best();
  InitMap->CopyFMap(&TheMap);

  thecost = ComputeEM(&TheMap);
  finalcost = thecost;
  TheMap.CopyFMap(&FinalMap);

  iter = 0;
  plus = 0;
  
  Heap->Insert(&TheMap, iter);
  if (!QuietFlag) PrintMap(&TheMap);
  
  for (t = Tinit; t> Tfinal; t *= Cooling) {
 
    if (VerboseFlag || !QuietFlag) {
      print_out( "\nTemp: %3.2f :  ", t);
    }

    if (VerboseFlag) {
      print_out(  "\n");
    }

    chg = 0;
    flush_out();
	
    for (j = 0; j < Tries; j++) {

      im1 = randomax(NbMS); 
      im2 = randomax(NbMS - 3);
      if (im1 == NbMS - 1) im2++;
      else if (im2 >= im1 - 1) im2 += 3 - (im1==0);
      
      TheMap.CopyMap(&TempMap);

      if (randomax(2)) {
	
	if ( im1 < im2)
	  TempMap.Apply2Change(im1, im2);
	else 
	  TempMap.Apply2Change(im2, im1);
	
	cost = ComputeEMS(&TempMap, thecost-2.0);
	
	wlen = Heap->Insert(&TempMap, iter);
	
	energychange = thecost - cost;
	
	if (VerboseFlag>1) {
	  print_out( "2-change: (%d,%d), Wlen: %d, DLogLike: %f\n",
              im1, im2, wlen, energychange);
	}
	
      } else {
	
 	TempMap.Apply3Change(im1, im2, wordre);
	
 	cost = ComputeEMS(&TempMap, thecost-2.0);

 	wlen = Heap->Insert(&TempMap, iter);
	
 	energychange = thecost - cost;
	
 	if (VerboseFlag>1) {
 	  print_out( "3-change: (%d,%d), Wlen: %d, DLogLike: %f\n",
              im1, im2, wlen, energychange);
	}
	
      }
      flush_out();

      if (cost > finalcost) {
	thecost = cost;
	TempMap.CopyMap(&TheMap);
	finalcost = thecost;
	TheMap.CopyFMap(&FinalMap);
	chg++;
	
	if (!QuietFlag) {  
	  print_out(  "+");
	}
	flush_out();
	
      } else if (energychange < 0) {
	thecost = cost;
	TempMap.CopyMap(&TheMap);
	chg++;
	
      } else if (RREAL < exp(-energychange/t)) {
 	thecost = cost;
	TempMap.CopyMap(&TheMap);
	chg++;
      }

      iter++;
      if (VerboseFlag)  {
	print_out( "(%3.1f) ",energychange);
	PrintMap(&TempMap);
      }
      flush_out();

      // traitement de la demande d'arrêt.
      if ( StopFlag )
	{ 
	  StopFlag = 0;
	  
	  print_out(  "Aborted!\n");
	  flush_out();
	  return;
	}
    }
    
    if (chg == 0 && AdaptFlag) {
      print_out(  "\n");
      break;
    }
 
    // adaptation de la température initiale

    if ( !AdaptFlag) {
      if (chg/(double)Tries < 0.8) {
	  Tinit *=2 ;
	  t = Tinit / Cooling;
      } else {
	AdaptFlag = 1;
      }
    }
  } 

  print_out(  "\n");
}
