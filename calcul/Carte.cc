//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// Carte.cc,v 1.70 2007/01/09 13:14:35 tschiex Exp
//
// Description : Gestion des cartes.
// Divers : 
//-----------------------------------------------------------------------------

#include "Carte.h"

#include "CartaGene.h"
#include "Genetic.h"
#include "BioJeu.h"

#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>  // atoi() spécialement pour IRIX
//#include <assert.h>

//-----------------------------------------------------------------------------
// Constructeur
//-----------------------------------------------------------------------------
// Paramètres :
// Valeur de retour : 
//-----------------------------------------------------------------------------
Carte::Carte()
{
  Id = -1;
  Cartage = NULL;
  NbMarqueur = 0;
  ordre = NULL;
  tr = NULL;
  coutEM = 0.0;
  UnConverge();
}

//-----------------------------------------------------------------------------
// Constructeur par copie.
//-----------------------------------------------------------------------------

Carte::Carte(const Carte &Source)
{
  int i;
  
  Id = -1;

  Cartage = Source.Cartage;
  NbMarqueur = Source.NbMarqueur;
  Converged = Source.Converged;

  ordre = new int[NbMarqueur];
  for (i = 0; i < NbMarqueur; i++)
    ordre[i] = Source.ordre[i];

  tr = new double[NbMarqueur-1];
  for (i = 0; i < NbMarqueur-1; i++)
    tr[i] = Source.tr[i];

  ret = Source.ret;
  coutEM = Source.coutEM;

  Vertex = 0;
}

//-----------------------------------------------------------------------------
// Constructeur
//-----------------------------------------------------------------------------
// Paramètres :
// - accès aux informations du système
// - nombre de marqueurs
// Valeur de retour : 
//-----------------------------------------------------------------------------


Carte::Carte(CartaGene *cartage, int n)
{

  Id = -1;
  Cartage = cartage;

  NbMarqueur = n;
  ordre = new int[n];
  tr = new double[n];
  Vertex = 0;
  coutEM = 0.0;
  UnConverge();
}

//-----------------------------------------------------------------------------
// Constructeur
//-----------------------------------------------------------------------------
// Paramètres :
// - pointeur sur CartaGene
// - nombre de marqueurs
// - vecteur d'indices de marqueurs
// Valeur de retour : 
//-----------------------------------------------------------------------------
Carte::Carte(CartaGene *cartage, int nm, intPtr imarq)
{

  int i;

  Id = -1;

  Cartage = cartage;
  NbMarqueur = nm;

  ordre = new int[nm];
  tr = new double[nm];

  for (i = 0; i < nm ; i++) 
      ordre[i] =  imarq[i];

  for (i = 0; i< NbMarqueur - 1; i++)
    tr[i]= Cartage->GetTwoPointsFR(ordre[i], ordre[i+1]);

  UnConverge();

  Vertex = 0;
  ret = 0.3;
  coutEM = 0.0;
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------
// Paramètres :
// Valeur de retour : 
//-----------------------------------------------------------------------------
Carte::~Carte()
{
 delete [] tr;
 delete [] ordre;
}

//-----------------------------------------------------------------------------
//Force a map to be considered as non estimated 
//-----------------------------------------------------------------------------
// Paramètres : 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::UnConverge(void)
{
  Converged = UnConvLevel;
}
//-----------------------------------------------------------------------------
// Canonify a MAP
// (order is lexicographically minimized)
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::Canonify(void)
{
  int i;
  int temp;
  double tempd;

  if (ordre[0] > ordre[NbMarqueur-1]) {
    for (i = 0; i < NbMarqueur/2; i++)  {
      temp = ordre[i];
      ordre[i] = ordre[NbMarqueur-1-i];
      ordre[NbMarqueur-1-i] = temp;
    }

    for (i = 0; i < (NbMarqueur-1)/2; i++) {
      tempd = tr[i];
      tr[i] = tr[NbMarqueur-2-i];
      tr[NbMarqueur-2-i] = tempd;
    }
  }
}

//-----------------------------------------------------------------------------
// Canonify a MAP
// (order is lexicographically minimized)
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
void Carte::CanonifyMor(void)
{
  int i;

  if (Cartage->ArbreJeu->Cross == Mor) {
    
    int gja = Cartage->ArbreJeu->GetBJMO();
    
    intPtr tempo = new int[NbMarqueur];
    tempo[0] = ordre[0];
    
    for (i = 1; i < NbMarqueur; i++) {
      int gj = Cartage->markers[ordre[i]].BitJeu & gja;
      int j = i-1;
	
      while(((Cartage->markers[tempo[j]].BitJeu & gja) & gj) == 0
	    && (Cartage->markers[tempo[j]].BitJeu & gja) > gj
	    && j >= 0)  {
	tempo[j+1]=tempo[j];
	j--;	      
      }
      tempo[j+1]=ordre[i];
    }
    
    for (i = 0; i < NbMarqueur; i++)
      ordre[i] = tempo[i];
    
    UnConverge();
    delete [] tempo;
  }
}
//-----------------------------------------------------------------------------
// Recopie les données de la carte courante sur Dest.
//-----------------------------------------------------------------------------
// Paramètres : 
// - La carte a remplir.
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
void Carte::CopyFMap(Carte* Dest) const
{
  int i;

  (*Dest).Id = -1;
  (*Dest).Converged = Converged;
  (*Dest).Cartage = Cartage;
  (*Dest).NbMarqueur = NbMarqueur;
  for (i = 0; i < NbMarqueur; i++)
      (*Dest).ordre[i] = ordre[i];

  for (i = 0; i < NbMarqueur-1; i++)
    (*Dest).tr[i] = tr[i];
  
  (*Dest).ret = ret;
  (*Dest).coutEM = coutEM;
}

//-----------------------------------------------------------------------------
//  Compare 2 map orders.
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

int Carte::SameMaps(const Carte* map2) const
{

  int i;

  Carte *mapc = new Carte(Cartage, NbMarqueur);
  Carte *mapc2 = new Carte(Cartage, NbMarqueur);

  map2->CopyMap(mapc2);
  CopyMap(mapc);
  if (Cartage->Heap->EquivalenceFlag == 1)  {
    mapc2->CanonifyMor();
    mapc->CanonifyMor();
  }

  for (i = 0; i < NbMarqueur; i++)
    if (mapc->ordre[i] != mapc2->ordre[i]) {
      delete mapc;
      delete mapc2;
      return 0;
    }

  delete mapc;
  delete mapc2;
  return 1;
}

//-----------------------------------------------------------------------------
// Pour une nouvelle carte pas trop bete.
//-----------------------------------------------------------------------------
// Paramètres : aucun
// Valeur de retour : aucun 
//-----------------------------------------------------------------------------

void Carte::BuildNiceMap(void)
{
  int vertex;
  double Like;
  Carte temp(Cartage, Cartage->NbMS, Cartage->MarkSelect);
  double BestLike = -1e200;
  
  for (vertex = 0 ; vertex < NbMarqueur; vertex++) {
    temp.BuildNearestNeighborMap(vertex);

    // traitement de la demande d'arrêt.
    if ( Cartage->StopFlag )	{ 
      Cartage->StopFlag = 0;
      print_out( "Aborted!\n");
      flush_out();
      return;
    }
      
    if ((Like = Cartage->ComputeEM(&temp)) > BestLike)	{
      BestLike = Like;
      GetMap(&temp); // La carte temporaire (temp) est recopiée 
      // dans la carte courante.
    }
    
    if (Cartage->VerboseFlag>1)
      //   Cartage->PrintMap(&temp);
      Cartage->ArbreJeu->PrintDMap(&temp,0,&temp);
  }
}

/* Algorithme MultiFragment
   Heuristique de construction d'une tournee
   Insere la plus petite arete en premier 
   Complexite en temps : O(NbMarqueur ** 4)
   Complexite en memoire : O(NbMarqueur)
   Cf. Greedy page 221 [Local Search in Combinatorial Optimization, Aarts & Lenstra, 1998] */

/* gestion d'un graphe acyclique tel que chaque sommet a au plus deux voisins */
class GraphAcyclicMaxDegree2 {
public:
  int size;
  int *ordre;
  int *degrees;
  int *voisin1;
  int *voisin2;
  int nbdegree0;
  int nbdegree1;
  int nbdegree2;

  GraphAcyclicMaxDegree2(int NbMarqueur) {
    size = NbMarqueur;
    ordre = new int[NbMarqueur];
    degrees = new int[NbMarqueur];
    voisin1 = new int[NbMarqueur];
    voisin2 = new int[NbMarqueur];
    nbdegree0 = NbMarqueur;
    nbdegree1 = 0;
    nbdegree2 = 0;
  
    for (int i = 0; i < NbMarqueur; i++) {
      degrees[i] = 0;
      voisin1[i] = -1;
      voisin2[i] = -1;
    }
  } 
  
  ~GraphAcyclicMaxDegree2() {
    delete [] ordre;
    delete [] degrees;
    delete [] voisin1;
    delete [] voisin2;
  }

  void addEdge(int m1, int m2);
  int availableEdge(int m1, int m2);
  void computeOrdre(void);
};

/* insere une arete sans verifier sa validite */
void GraphAcyclicMaxDegree2::addEdge(int m1, int m2)
{
  if (degrees[m1] == 0) {
    voisin1[m1] = m2;
    nbdegree0--;
    nbdegree1++;
  } else {
    voisin2[m1] = m2;
    nbdegree1--;
    nbdegree2++;
  }
  degrees[m1]++;
  if (degrees[m2] == 0) {
    voisin1[m2] = m1;
    nbdegree0--;
    nbdegree1++;
  } else {
    voisin2[m2] = m1;
    nbdegree1--;
    nbdegree2++;
  }
  degrees[m2]++;
}

/* verifie que l'arete n'a pas deja ete selectionnee, 
   et qu'elle ne cree ni un cycle, ni un sommet de degre egal a 3 */
int GraphAcyclicMaxDegree2::availableEdge(int m1, int m2)
{
  /* arete deja choisie */
  if (voisin1[m1] == m2 || voisin2[m1] == m2) return 0;
  
  /* un sommet deja relie par deux voisins */
  if (degrees[m1] == 2 || degrees[m2] == 2) return 0;
  
  /* un sommet sans voisin */
  if (degrees[m1] == 0 || degrees[m2] == 0) return 1;
  
  /* cas: les deux sommets ont un seul voisin */
  /* test si l'extremite du chemin demarre par m2 correspond a m1 */
  int previous = m2;
  m2 = voisin1[m2]; 
  while (degrees[m2] == 2) {
    if (voisin1[m2] == previous) {
      previous = m2;
      m2 = voisin2[m2];
    } else {
      previous = m2;
      m2 = voisin1[m2];
    }
  }	 
  if (m2 == m1) return 0; 
  else return 1;
}

/* extrait un ordre a partir des aretes selectionnees dans le graphe */
void GraphAcyclicMaxDegree2::computeOrdre(void) 
{
  //  assert( nbdegree0 == 0 && nbdegree1 == 2 && nbdegree2 == size - 2 );

  int index = 0;
  int currm = 0;

  while (degrees[currm] == 2) {
    currm++;
  }
  //  assert( currm < size && degrees[currm] == 1 );
  
  ordre[index++] = currm;
  int previous = currm;
  currm = voisin1[currm];
  while (degrees[currm] == 2) {
    ordre[index++] = currm;
    if (voisin1[currm] == previous) {
      previous = currm;
      currm = voisin2[currm];
    } else {
      previous = currm;
      currm = voisin1[currm];
    }
  }
  ordre[index++] = currm;
  //  assert( index == size );
}

void Carte::BuildNiceMapMultiFragment(void)
{
  int i;
  if (NbMarqueur < 2) return;
  
  GraphAcyclicMaxDegree2 graph(NbMarqueur);
  
  while (!(graph.nbdegree0 == 0 && graph.nbdegree1 == 2)) {
    int bestm1 = -1;
    int bestm2 = -1;
    double minDist = 1e100;
    for (int m1 = 0; m1 < NbMarqueur - 1; m1++) {
      if (graph.degrees[m1] < 2) {
	for (int m2 = m1 + 1; m2 < NbMarqueur; m2++) {
	  if (graph.degrees[m2] < 2) {
	    if (Cartage->GetTwoPointsDH(ordre[m1], ordre[m2]) < minDist &&
		graph.availableEdge(m1, m2)) {
	      minDist = Cartage->GetTwoPointsDH(ordre[m1], ordre[m2]);
	      bestm1 = m1;
	      bestm2 = m2;
	    }
	  }
	}
      }
    }
    /* printf("edge %5.5s(%d) %5.5s(%d)\n", Cartage->markers.keyOf(ordre[bestm1]).c_str(), bestm1, Cartage->markers.keyOf(ordre[bestm2]).c_str(), bestm2); */
    graph.addEdge(bestm1, bestm2);
  }

  /* recuppere le nouvel ordre et le recopie dans l'ordre de la carte */
  intPtr newordre = new int[NbMarqueur];
  graph.computeOrdre();
  for (i = 0; i < NbMarqueur; i++) {
    newordre[i] = ordre[graph.ordre[i]];
  }
  for (i = 0; i < NbMarqueur; i++) {
    ordre[i] = newordre[i];
  }  
  delete [] newordre;

  for (i = 0; i < NbMarqueur-1; i++)
    tr[i] = Cartage->GetTwoPointsFR(ordre[i], ordre[i+1]);
  
  ret = 0.3;

  UnConverge();

  if (Cartage->VerboseFlag>1)
    Cartage->PrintMap(this);
}

//-----------------------------------------------------------------------------
// Pour une nouvelle carte pas trop bete. critère = LOD
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildNiceMapL(void)
{
  int vertex;
  double Like;
  Carte temp(Cartage, Cartage->NbMS, Cartage->MarkSelect);
  double BestLike = -1e200;
  
  for (vertex = 0 ; vertex < NbMarqueur; vertex++)
  {
    temp.BuildNearestNeighborMapL(vertex);

    // traitement de la demande d'arrêt.
    if ( Cartage->StopFlag )   { 
      Cartage->StopFlag = 0;
      print_out( "Aborted!\n");
      flush_out();
      return;
    }

    if ((Like = Cartage->ComputeEM(&temp)) > BestLike) {
      BestLike = Like;
      GetMap(&temp); // La carte temporaire (temp) est recopiée 
      // dans la carte courante.
    }
    if (Cartage->VerboseFlag>1)
      Cartage->PrintMap(&temp);
  }
}

void Carte::BuildNiceMapLMultiFragment(void)
{
  int i;
  if (NbMarqueur < 2) return;
  
  GraphAcyclicMaxDegree2 graph(NbMarqueur);
  
  while (!(graph.nbdegree0 == 0 && graph.nbdegree1 == 2)) {
    int bestm1 = -1;
    int bestm2 = -1;
    double maxLOD = -1;
    for (int m1 = 0; m1 < NbMarqueur - 1; m1++) {
      if (graph.degrees[m1] < 2) {
	for (int m2 = m1 + 1; m2 < NbMarqueur; m2++) {
	  if (graph.degrees[m2] < 2) {
	    if (Cartage->GetTwoPointsLOD(ordre[m1], ordre[m2]) > maxLOD &&
		graph.availableEdge(m1, m2)) {
	      maxLOD = Cartage->GetTwoPointsLOD(ordre[m1], ordre[m2]);
	      bestm1 = m1;
	      bestm2 = m2;
	    }
	  }
	}
      }
    }
    /* printf("edge %5.5s(%d) %5.5s(%d)\n", Cartage->markers.keyOf(ordre[bestm1]).c_str(), bestm1, Cartage->markers.keyOf(ordre[bestm2]).c_str(), bestm2); */
    graph.addEdge(bestm1, bestm2);
  }

  /* recuppere le nouvel ordre et le recopie dans l'ordre de la carte */
  intPtr newordre = new int[NbMarqueur];
  graph.computeOrdre();
  for (i = 0; i < NbMarqueur; i++) {
    newordre[i] = ordre[graph.ordre[i]];
  }
  for (i = 0; i < NbMarqueur; i++) {
    ordre[i] = newordre[i];
  }  
  delete [] newordre;

  for (i = 0; i < NbMarqueur-1; i++)
    tr[i] = Cartage->GetTwoPointsFR(ordre[i], ordre[i+1]);
  
  ret = 0.3;

  UnConverge();

  if (Cartage->VerboseFlag>1)
    Cartage->PrintMap(this);
}

//-----------------------------------------------------------------------------
// Construction de cartes « pas trop bête »
//   - Nearest Neighbor
//-----------------------------------------------------------------------------
// Paramètres :
// - root
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildNearestNeighborMap(int root)
{
  int i, j, Fill = 0, NewRoot = -1;
  intPtr Lock = new int[NbMarqueur];
  intPtr ordrori = new int[NbMarqueur];
  double MinDist;
  
  for (i = 0; i < NbMarqueur; i++) {
    Lock[i] = 0;
    ordrori[i] = ordre[i];
  }
  
  ordre[Fill++] = ordrori[root];
  Lock[root] = 1;
  for  (i = 1; i < NbMarqueur; i++)
    /* On part de i = 1 car le premier de la chaine est deja connu = root */
    {
      MinDist = 1e100;
      for (j = 0; j < NbMarqueur; j++) {
	if (!Lock[j])  {
	  if (MinDist > Cartage->GetTwoPointsDH(ordrori[j], ordrori[root])) {
	    MinDist = Cartage->GetTwoPointsDH(ordrori[j], ordrori[root]);
	    NewRoot = j;
	  }
	}
      }
      ordre[Fill++] = ordrori[NewRoot];
      Lock[NewRoot] = 1;
      root = NewRoot;
    }
  for (i = 0; i < NbMarqueur-1; i++)
    tr[i] = Cartage->GetTwoPointsFR(ordre[i], ordre[i+1]);
  
  ret = 0.3;

  UnConverge();

  delete [] Lock;
  delete [] ordrori;
}
//-----------------------------------------------------------------------------
// Construction de cartes « pas trop bête »
//   - Bigest LOD
//-----------------------------------------------------------------------------
// Paramètres :
// - root
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildNearestNeighborMapL(int root)
{
  int i, j, Fill = 0, NewRoot = -1;
  intPtr Lock = new int[NbMarqueur];
  intPtr ordrori = new int[NbMarqueur];
  double MaxLOD;
  
  for (i = 0; i < NbMarqueur; i++)
    {
      Lock[i] = 0;
      ordrori[i] = ordre[i];
    }
  
  ordre[Fill++] = ordrori[root];
  Lock[root] = 1;
  for  (i = 1; i < NbMarqueur; i++)
    /* On part de i = 1 car le premier de la chaine est deja connu = root */
    {
      MaxLOD = log(0.0); // Minus infinity should be small enough
      for (j = 0; j < NbMarqueur; j++)
	{
	  if (!Lock[j])
	    {
	      if (MaxLOD < Cartage->GetTwoPointsLOD(ordrori[j], 
						    ordrori[root]))
		{
		  MaxLOD = Cartage->GetTwoPointsLOD(ordrori[j], 
						    ordrori[root]);
		  NewRoot = j;
		}
	    }
	}
      ordre[Fill++] = ordrori[NewRoot];
      Lock[NewRoot] = 1;
      root = NewRoot;
    }
  for (i = 0; i < NbMarqueur-1; i++)
    tr[i] = Cartage->GetTwoPointsFR(ordre[i],
				    ordre[i+1]);
  
  ret = 0.3;

  UnConverge();

  delete [] Lock;
}

//-----------------------------------------------------------------------------
// Réinitialise les structures de la carte courante,
// avec le contenu de la carte Source. Sauf pour tr qui est non initialisé.
//-----------------------------------------------------------------------------
// Paramètres : 
// - La carte a recopier.
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::GetMap(const Carte* Source)
{
  int i; 

  Cartage = (*Source).Cartage;
  NbMarqueur = (*Source).NbMarqueur;
  
  for (i = 0; i < NbMarqueur; i++)
      ordre[i] = (*Source).ordre[i];

  UnConverge();
  ret = (*Source).ret;
  coutEM = (*Source).coutEM;
}

//-----------------------------------------------------------------------------
// Applique une insertion de e1 a la place de e2 (repousse e2)
//-----------------------------------------------------------------------------
// Paramètres : 
// - e1, e2: indices de marqueur sur la carte.
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::ApplyInsertion(int e1, int e2)
{
  UnConverge();

  int temp = ordre[e1];
  if (e1 < e2) {
    for (int i = e1+1; i < e2; i++)
      ordre[i-1] = ordre[i];
    ordre[e2-1] = temp;
  } else {
    for (int i = e1-1; i >= e2; i--)
      ordre[i+1] = ordre[i];
    ordre[e2] = temp;
  }
}

//-----------------------------------------------------------------------------
// Applique un 2 changement. Suppose e1 < e2
//-----------------------------------------------------------------------------
// Paramètres : 
// - e1, e2: indices de marqueur sur la carte.
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::Apply2Change(int e1, int e2)
{
  int i, temp;
  
  UnConverge();

  for (i = 0; i < (e2-e1)/2; i++)  {
    temp = ordre[i+e1];
    ordre[e1+i] = ordre[e2-1-i];
    ordre[e2-1-i] = temp;
  }
}

//-----------------------------------------------------------------------------
//  Applique un 3 changement.
//-----------------------------------------------------------------------------
// Paramètres : 
// - e1, e2: indices de marqueur sur la carte.
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::Apply3Change(int e1, int e2, int wordre[]){
  
  int i, j;

  UnConverge();

  if ( e1 < e2) {
    j = 0;
    for (i = e1 + 1 ; i <= e2; i++) wordre[j++] = ordre[i];
    for (i = 0 ; i <= e1; i++) wordre[j++] = ordre[i];
    for (i = 0 ; i <= e2; i++)  {
      ordre[i] = wordre[i];
    }
  } else {
    j = e2;
    for (i = e1 ; i < NbMarqueur; i++) wordre[j++] = ordre[i];
    for (i = e2  ; i < e1; i++) wordre[j++] = ordre[i];
    for (i = e2  ; i < NbMarqueur; i++)  {
      ordre[i] = wordre[i];
    }
  }  
}

//-----------------------------------------------------------------------------
// Copie certains éléments d'une carte.
// Seulement ordre et coutEM. Le contenu du tableau tr n'est pas initialisé.
//-----------------------------------------------------------------------------
// Paramètres : 
// - La carte a remplir.
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::CopyMap(Carte* Dest) const
{
  int i;

  (*Dest).Id = -1;

  (*Dest).Cartage = Cartage;
  (*Dest).NbMarqueur = NbMarqueur;
  
  for (i = 0; i < NbMarqueur; i++)
      (*Dest).ordre[i] = ordre[i];

  (*Dest).UnConverge();
  (*Dest).ret = ret;
  (*Dest).coutEM = coutEM;
}

//-----------------------------------------------------------------------------
// Post traitement: Pour chaque marqueur,
// compare la probabilité courante avec la probabilité obtenue en
// déplassant le marqueur vers un autre intervalle.  Ne met plus à jour
// avec le meilleur élément (s'il y en a un). Affecte seulement le TAS
// Affiche la matrice des différences.
//-----------------------------------------------------------------------------

void Carte::Polish()
{
  int i,j,k;
  int src,dst;
  Carte TempMap(*this);
  double othercost,cost = (*this).coutEM;
  doublePtr *Matrix;
  
  if (!Cartage->QuietFlag)
    {
      print_out( "\nLocal map analysis:\n\n");
      print_out( "       ");

      for (i = 0; i<NbMarqueur; i++) {
	print_out( "%5.5s ",Cartage->markers.keyOf(ordre[i]).c_str());
      }
      print_out( "\n      ");

      for (i = 0; i<NbMarqueur; i++) {
	print_out( "------");
      }
      print_out( "\n");
    }

  Matrix = new doublePtr[NbMarqueur];
  for (i = 0; i< NbMarqueur; i++)
    Matrix[i] = new double[NbMarqueur];
  
  /* on va deplacer le marqueur a la position i */
  for (i = 0; i< NbMarqueur; i++)
    /* dans un des intervalles (j) */
    for (j = 0; j < NbMarqueur; j++)  {
      if (i == j) continue;
      
      src = 0;
      dst = 0;
      
      for (k = 0; k < NbMarqueur; k++)	  {
	if (src == i) src++;
	
	if (k == j) TempMap.ordre[dst++] = ordre[i];
	else TempMap.ordre[dst++] = ordre[src++];
	TempMap.UnConverge();
      }
		 
      // traitement de la demande d'arrêt.
      if ( Cartage->StopFlag ) { 
	Cartage->StopFlag = 0;
	
	// libération
	
	for (i = 0; i< NbMarqueur; i++)
	  delete [] Matrix[i];
	delete [] Matrix;	    
	
	// 
	print_out( "Aborted!\n");
	flush_out();
	return;
      }
      
      othercost = Cartage->ComputeEMS(&TempMap, cost-3.0);
      
      Cartage->Heap->Insert(&TempMap, 0);
      
      Matrix[i][j] = othercost;
    }
  
  if (!Cartage->QuietFlag) {
      for (i = 0; i< NbMarqueur; i++) {
	print_out( "%5.5s |",Cartage->markers.keyOf(ordre[i]).c_str());
	for (j = 0; j < NbMarqueur; j++) {
	  if (i == j) {
	    print_out( "----- ");
	    continue;
	  }
	  
	  print_out( "% 5.1f ",(cost-Matrix[i][j]));
	}
	print_out( "\n");
      }
      
      print_out( "      ");
      for (i = 0; i < NbMarqueur; i++) {
	print_out( "------");
      }
      print_out( "\n");
      
    }
  
  for (i = 0; i< NbMarqueur; i++)
    delete [] Matrix[i];
  delete [] Matrix;
}

//-----------------------------------------------------------------------------
// Post traitement: Pour chaque marqueur,
// compare la probabilité courante avec la probabilité obtenue en
// déplassant le marqueur vers un autre intervalle.  Ne met plus à jour
// avec le meilleur élément (s'il y en a un). N'Affecte PAS le TAS
// Affiche la matrice des différences.
//-----------------------------------------------------------------------------

void Carte::Polishtest(int nbmi)
{
  int i,j,k;
  int src,dst;
  Carte TempMap(*this);
  double othercost,cost = (*this).coutEM;
  doublePtr *Matrix;
  
  if (!Cartage->QuietFlag) {
      printf("\nLocal map analysis:\n\n");
      printf("       ");
      for (i = 0; i<nbmi; i++)
	printf("%5.5s ",Cartage->markers.keyOf(ordre[i]).c_str());
      printf("\n      ");
      for (i = 0; i<nbmi; i++)
	printf("------");
      printf("\n");
    }
  flush_out();

  Matrix = new doublePtr[nbmi];
  for (i = 0; i< nbmi; i++)
    Matrix[i] = new double[nbmi];

  /* on va deplacer le marqueur a la position i */
  for (i = 0; i< nbmi; i++)
    /* dans un des intervalles (j) */
    for (j = 0; j < nbmi; j++)
      {
	if (i == j) continue;
	
	src = 0;
	dst = 0;
	
	for (k = 0; k < nbmi; k++)
	  {
	    if (src == i) src++;
	    
	    if (k == j) TempMap.ordre[dst++] = ordre[i];
	    else TempMap.ordre[dst++] = ordre[src++];
	    TempMap.UnConverge();
	  }
		 
	// traitement de la demande d'arrêt.
	if ( Cartage->StopFlag )
	  { 
	    Cartage->StopFlag = 0;
	    
	    // libération
	    
	    for (i = 0; i< nbmi; i++)
	      delete [] Matrix[i];
	    delete [] Matrix;	    
	    
	    // 
	    printf("Aborted!\n");
	    flush_out();
	    return;
	  }
	
	othercost = Cartage->ComputeEMS(&TempMap, cost-3.0);
	
	//Cartage->Heap->Insert(&TempMap, 0);
	
	Matrix[i][j] = othercost;
      }
  
  if (!Cartage->QuietFlag)
    {
      for (i = 0; i< nbmi; i++)
	{
	  printf("%5.5s |",Cartage->markers.keyOf(ordre[i]).c_str());
	  for (j = 0; j < nbmi; j++)
	    {
	      if (i == j) {
		printf("----- ");
		continue;
	      }
	      
	      printf("%5.1f ",(cost-Matrix[i][j]));
	    }
	  printf("\n");
	}
      
      printf("      ");
      for (i = 0; i < nbmi; i++)
	printf("------");
      printf("\n");
      
    }
  
  for (i = 0; i< nbmi; i++)
    delete [] Matrix[i];
  delete [] Matrix;
}

//-----------------------------------------------------------------------------
// les flips
//-----------------------------------------------------------------------------
// Paramètres : 
// - fenêtre
// - seuil
// - itératif
// Valeur de retour : 
//-----------------------------------------------------------------------------


void Carte::Flips(int n, double thres, int iter)
{ 
  intPtr *PerMat;
  int i,j,k,l,nbe = 0;
  Carte TempMap(*this);
  Carte TempMapC(*this);
  Carte BetMap(*this);
  double othercost,cost = coutEM;
  double costi = coutEM;
  bool improved = false;

  print_out( "\n");

  if (n > NbMarqueur) {
    print_out( "Not enough markers, flipping only over %d markers.\n",
	    NbMarqueur);
    n = NbMarqueur;
  }

  if (!Cartage->QuietFlag) {
    if (iter== 1) {
      print_out( "Repeated Flip(window size : %d, threshold : %4.2f).\n\n",
	      n, thres);
    }
    else if (iter == 0) {
      print_out( "Single Flip(window size : %d, threshold : %4.2f).\n\n",
	      n, thres);
    }
    else if (iter == 2) {
      print_out( "Eager Repeated Flip(window size : %d, threshold : %4.2f).\n\n",
	      n, thres);
    }
    flush_out();
    
    Cartage->PrintMap(this);
    flush_out();
    
    print_out( "\n");
    PrintIVMap();
    flush_out();
  }
  
  // Allocation de la matrice des permutations

  PerMat = new intPtr[n];
  for (i = 0; i < n; i++)
    PerMat[i] = new int[fact(n)];
  
  
  // Initialisation de la matrice des permutations
  
  for (i = 0; i < n; i++) 
    memset(PerMat[i], 0, fact(n)*sizeof(int));    
 
  // Remplissage de la matrice des permutations
  
  PerMat[0][0] = 1;

  for (i = 2; i <= n; i++) 
    {
      for (j = 0; j < fact(i - 1); j++) 
	PerMat[i - 1][j] = i;

      for (j = 1; j <= i; j++) 
	for (k = 1; k <= i - 1; k++) 
	  memcpy(&PerMat[(int)fmod((float) j + k - 1,(float) i)][k * fact(i - 1)],
		 &PerMat[j - 1][0],
		 fact(i - 1)*sizeof(int));
    }

  // glisser la fenêtre
  
  for (i = 0; i < NbMarqueur + 1 - n; i++) 
    {
      for (j = fact(n) - 1; 0 < j; j--)
	// pour éviter les redondances (test lourd)
	if (PerMat[n-1][j] != n || i == 0)
	{
	  for (k = 0; k < n; k++) 
	    TempMap.ordre[i + k] = ordre[i + PerMat[k][j] - 1];
	  TempMap.UnConverge();

	  // traitement de la demande d'arrêt.
	  if ( Cartage->StopFlag )   { 
	      Cartage->StopFlag = 0;
	      
	      // libération
	      
	      for (i = 0; i < n; i++)
		delete [] PerMat[i];
	      delete [] PerMat;
	      
	      // 
	      print_out( "Aborted!\n");
	      flush_out();
	      return;
	    }
	  
	  othercost = Cartage->ComputeEMS(&TempMap, costi-thres-1.5);

	  TempMap.CopyFMap(&TempMapC);
	  
	  Cartage->Heap->Insert(&TempMap, 0);
	  
	  TempMapC.CopyFMap(&TempMap);
	  
	  nbe++;
	  
	  if (othercost > (coutEM - thres)) { 
	    for (l = 0; l < i; l++) {
	      print_out( " -");
	      flush_out();
	    }
	      
	    print_out( "[");
	    flush_out();
	      
	    if (PerMat[0][j] != 1) {
	      print_out( "%d", PerMat[0][j]-1);
	    }
	    else {
	      print_out( "-");
	    }
	    flush_out();
	    
	    for (l = 1; l < n; l++)
	      if (PerMat[l][j] != l+1) {
		print_out( " %d", PerMat[l][j]-1);
	      }
	      else {
		print_out( " -");
	      }
	      flush_out();

	      print_out( "]");
	      flush_out();
	      
	      if (i + n  < NbMarqueur) {
		print_out( "-");
	      }
	      for (l = i + n + 1; l < NbMarqueur; l++) {
		print_out( " -");
	      }
	      
	      print_out( "  %8.2f\n", TempMap.coutEM - coutEM);
	      flush_out();
	    }

	  if (othercost > cost) {
	    improved  = true;
	    cost = othercost;
	    TempMap.CopyFMap(&BetMap);
	  }
	  
	  flush_out();
	}
      for (k = 0; k < n; k++)
	TempMap.ordre[i + k] = ordre[i + PerMat[k][0] - 1];    

      if (iter == 2 && improved) {
	BetMap.CopyFMap(this);
	BetMap.CopyFMap(&TempMap);
	print_out( "Improving permutation applied\n");
	flush_out();
	improved = false;
      }
    }
  
  // Libération de la matrice des permutaions 
  
  for (i = 0; i < n; i++)
    delete [] PerMat[i];
  delete [] PerMat;
  
  //
  
  BetMap.CopyFMap(this);
  if (cost != costi && iter) Flips(n, thres, iter);

    print_out( "\n");
    flush_out();
}

//-----------------------------------------------------------------------------
// Affichage des variations des positions / la meilleure
//-----------------------------------------------------------------------------
// Paramètres : 
// - taboref est l'ordre de référence
// - refordre est le tableau des numéro de marqueur de la meilleure carte
// - nbm est le nombre de maqueur minimum consécutif dont on veut faire remarquer le changement de position
// - blank permet de ne pas afficher les indices intemédiaires des intervalles
// - comp supprime toutes les informations entre les bornes d'un intervalle, permet un affichage conci
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::PrintO(int * taboref, 
		   int * refordre, 
		   int nbm, 
		   int blank, 
		   int comp) const
{
  int i,nbinter = 0;
  unsigned int j,deca;
  char formpos[128],formb[128]; 
  int ouvert = 0;
  char cinter;
  char bufinter[1024];
  char bufid[8];
  bufinter[0] = '\0'; // buffer de l'interval ouvert(délicat la gestion de ][ = I
  bufid[0] = '\0';
  

  //le formattage de l'affichage dépend du nombre de marqueurs
  // concision

  if (NbMarqueur <= 10) 
    {
      sprintf(formpos, "%s%d%c","%c%", 1, 'd');      
      sprintf(formb, "%s%d%c","%c%", 1, 's');
      deca = 3;
    } 
  else if (NbMarqueur <= 100)
    {
      sprintf(formpos, "%s%d%c","%c%", 2, 'd');
      sprintf(formb, "%s%d%c","%c%", 2, 's');
      deca = 4;
    }
  else
    {
      sprintf(formpos, "%s%d%c","%c%", 3, 'd');
      sprintf(formb, "%s%d%c","%c%", 3, 's');
      deca = 5;
    }
    
  print_out( "%6d : %8.2f =",Id, Cartage->Heap->Best()->coutEM - coutEM);

  for (i = 0; i < NbMarqueur; i++) { 

    // affichage uniquement des positions changées 
    if (nbm > 0) {
      
      // différence constatée
      if (ordre[i] != refordre[i]) {

	// on se trouve dans une zone de différences consécutives
	if (ouvert)
	  
	  // la différence est consécutive
	  if (abs(taboref[ordre[i]] - taboref[ordre[i-1]]) == 1)  {
	    // la taille de l'interval augmente
	    nbinter++;
	    // pas d'interval à matérialiser
	    cinter = ' ';
	  } // la différence n'est pas consécutive alors fin de l'interval précédent
	  else  {
	    // si l'interval précédent n'est pas assez grand pour être matérialisé
	    if (nbinter < nbm)   {

	      // si il y avait un intervalle juste avant(à conserver)
	      if (bufinter[0] == 'I')
		// remplacement 
		bufinter[0] = ']';
	      else 
		// pas d'interval (suppression de l'interval ouvert à priori)
		bufinter[0] = ' ';
	      
	      // interval à priori
	      cinter = '[';
	    } // l'intervalle précédent est à matérialiser  
	    else {
	      // matérialisation à priori sachant qu'il y a un interval avant
	      if (comp) 
		cinter = '[';
	      else
		// interval précédent fermé + interval à priori 
		cinter = 'I';
	      
	      // remplacement des position
	      if (blank)
		for  (j = deca; j < (strlen(bufinter) - deca + 1); j++)
		  bufinter[j] = '-';
	      // suppression des position
	      if (comp)  {
		if (nbinter > 1)
		  strcpy(&bufinter[deca - 1], &bufinter[(strlen(bufinter) - deca + 1)]);
		bufinter[strlen(bufinter) + 1] = '\0';
		bufinter[strlen(bufinter)] = ']';
	      }
	    }
	    
	    // affichage de l'interval
            /*strcpy(bouf, bufinter);*/
	    /*pout(bouf);*/
        print_out(bufinter);
	    // on vide le buffer
	    bufinter[0] = '\0';
	    nbinter = 1;
	  }
	else // on ouvre un interval
	  {
	  cinter = '[';
	  ouvert = 1;
	  nbinter = 1;
	}

	// mise à jour du buffer de l'interval
	sprintf(bufid, formpos, cinter, taboref[ordre[i]]);
	sprintf(bufinter, "%s%s", bufinter, bufid);
	
      } // pas de différence constatée 
      else {
	// si un interval etait ouvert, le fermer (voir au dessus)
	if (ouvert) {
	  if (nbinter < nbm) {
	    if (bufinter[0] == 'I')
		bufinter[0] = ']';
	    else
	      bufinter[0] = ' ';
	    cinter = ' ';		  
	  } else {
	    cinter = ']';

	    if (blank)
	      for  (j = deca; j < strlen(bufinter) - deca + 1; j++)
		bufinter[j] = '-';

	    if (comp) {
	      if (nbinter > 1)
		strcpy(&bufinter[deca - 1], &bufinter[(strlen(bufinter) - deca + 1)]);			   
	      bufinter[strlen(bufinter) + 1] = '\0';
	      bufinter[strlen(bufinter)] = ']';
	    }
	  }
          /*strcpy(bouf, bufinter);*/
	  /*pout(bouf);*/
      print_out(bufinter);
	  bufinter[0] = '\0';
	  nbinter = 0;
	  ouvert = 0;
	}
	else // pas de fermeture
	  cinter = ' ';
	
	// affichage des blancs 
	if (comp == 0) { 	
	  print_out( formb, cinter, " ");
	}
      }
    }
    // affichage de toutes les positions
    else {
      cinter = ' ';
      print_out( formpos, cinter, taboref[ordre[i]]);
    } 
  }
  // toutes les positions ont été comparées, reste à conclure
  // les commentaires au dessus.
  if (ouvert) {
      if (nbinter < nbm) {
	if (bufinter[0] == 'I')
	  bufinter[0] = ']';
	else
	  bufinter[0] = ' ';
	cinter = ' ';		  
      } else {
	cinter = ']';
	if (blank)
	  for  (j = deca; j < (strlen(bufinter) - deca + 1); j++)
	    bufinter[j] = '-';
	if (comp) {
	  if (nbinter > 1)
	    strcpy(&bufinter[deca - 1], &bufinter[(strlen(bufinter) - deca + 1)]);
	  bufinter[strlen(bufinter) + 1] = '\0';
		bufinter[strlen(bufinter)] = ']';
	}
      }
      /*strcpy(bouf, bufinter);*/
      /*pout(bouf);*/
      print_out(bufinter);
      if (comp == 0) { 	 
 	print_out( "%c",cinter);
      }
  } else {
    print_out( " ");
  }
   
  char*** lmn = Cartage->GetMap("k", Id);
  char*** lmnb = Cartage->GetMap("k", Cartage->Heap->Best()->Id);
  double bc;
  double c;
  int nj;

  nj = 1;
  print_out( " (");
  // décompte
  while (lmn[nj++] != NULL)
    ;
  
  nj--;nj--;



  for (i = 1; i < nj; i++) { 
    
    sscanf(lmn[i][1], "%lf", &c);
    sscanf(lmnb[i][1], "%lf", &bc);
    print_out( " %8.2f +", bc - c);
  }
  
  sscanf(lmn[nj][1], "%lf", &c);
  sscanf(lmnb[nj][1], "%lf", &bc);
  print_out( " %8.2f )", bc - c);
  
  
  delete [] lmn;
  delete [] lmnb;

  print_out( "\n");
  
}

//-----------------------------------------------------------------------------
// Impression des indice des marqueurs(verticalement)
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::PrintIVMap(void) const
{
  int i;
  for (i = 0; i < NbMarqueur; i++) 
    if ((ordre[i] - (ordre[i]%100))/100) {
      print_out( " %d", (ordre[i] - (ordre[i]%100))/100);
    }
    else {
      print_out( "  ");
    }
  print_out( "\n");
  for (i = 0; i < NbMarqueur; i++)
    if ((ordre[i]%100 - ((ordre[i]%100)%10))/10 || ordre[i] > 99) {
      print_out( " %d", (ordre[i]%100 - ((ordre[i]%100)%10))/10);
    }
    else {
      print_out( "  ");
    }
  print_out( "  log10\n");
  for (i = 0; i < NbMarqueur; i++) {  
    print_out( "% d", ordre[i]%10);
  }
  print_out( "  %8.2f\n",coutEM);
}


//-----------------------------------------------------------------------------
// Pour une nouvelle carte pas trop bête.
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildNewMap(void)
{
  if (Vertex < NbMarqueur)
  {

    BuildNearestNeighborMap(Vertex);
    Vertex++;
  }
  else if (Vertex < 2*NbMarqueur)
    {
     
      BuildMSTMap(Vertex-NbMarqueur);
      Vertex++;
    }
  else
      BuildRandomMap();

}
//-----------------------------------------------------------------------------
// Pour une nouvelle carte pas trop bête. en utilisant le LOD
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildNewMapL(void)
{
  if (Vertex < NbMarqueur)
    {
      BuildNearestNeighborMapL(Vertex);
      Vertex++;
    }
  else if (Vertex < 2*NbMarqueur)
    {
      BuildMSTMapL(Vertex-NbMarqueur);
      Vertex++;
    }
  else   
    BuildRandomMap();
  
}

//-----------------------------------------------------------------------------
// Construction de cartes « pas trop bête »
//   - Par chemin Eulerien sur un MST. 
//-----------------------------------------------------------------------------
// Paramètres : 
// -
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildMSTMap(int root)
{
  /* dist to existing tree */
  doublePtr dist = new double[NbMarqueur];
  /* Noeud pere */
  intPtr Pere = new int[NbMarqueur];;
  /* Pile pour le pre-ordre */
  intPtr Stack = new int[NbMarqueur];;
  /* Ordre Origine */
  intPtr Ordreo = new int[NbMarqueur];

  int i, j;
  int ClosestVertex = -1, ClosestTemp = -1;
  double MinDist = 1e100;

  Pere[root] = -1;
  dist[root] = -1.0;
  
  for (i=0; i<NbMarqueur; i++)
  {
    // recopie de l'ordre d'origine
    Ordreo[i] = ordre[i];
    if (i != root)  {
      dist[i] = Cartage->GetTwoPointsDH(ordre[i], 
					ordre[root]);
      Pere[i]=root;
      if (MinDist > dist[i])  {
	MinDist = dist[i];
	ClosestVertex = i;
      }
    }
  }

  for (i=0; i<NbMarqueur-1; i++) {
    dist[ClosestVertex] = -1.0;
    MinDist = 1e100;
    for (j=0; j<NbMarqueur; j++)
      if (dist[j] >= 0) {
        if (dist[j] > Cartage->GetTwoPointsDH(ordre[j], 
					      ordre[ClosestVertex]))  {
	  dist[j] = Cartage->GetTwoPointsDH(ordre[j], 
					    ordre[ClosestVertex]);
	  Pere[j] = ClosestVertex;
	}
	if (MinDist > dist[j]) {
	  MinDist = dist[j];
          ClosestTemp = j;
	}
      }
    ClosestVertex = ClosestTemp;
    }
  /* Parcours de l'arbre en Pre-ordre */
  ClosestTemp= 0; /* servira de ptr de pile */
  Stack[ClosestTemp++] = root;
  i = 0; /* indice de remplissage de data->ordre */

  while (ClosestTemp)  {
    root = Stack[--ClosestTemp];
    ordre[i] = Ordreo[root];
    i++;

    for (j=0; j < NbMarqueur; j++)
      if (Pere[j] == root)
	Stack[ClosestTemp++] = j;
  }
  
  for (i = 0; i < NbMarqueur-1; i++)
    tr[i]= Cartage->GetTwoPointsFR(ordre[i],ordre[i+1]);
  
  UnConverge();

  delete [] Stack;
  delete [] Pere;
  delete [] dist;
  delete [] Ordreo;
}

//-----------------------------------------------------------------------------
// Construction de cartes « pas trop bête »
//   - Par chemin Eulerien sur un MST. 
//-----------------------------------------------------------------------------
// Paramètres : 
// -
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildMSTMapL(int root)
{
  /* lod to existing tree */
  doublePtr lod = new double[NbMarqueur];
  /* Noeud pere */
  intPtr Pere = new int[NbMarqueur];
  /* Pile pour le pre-ordre */
  intPtr Stack = new int[NbMarqueur];
  /* Ordre Origine */
  intPtr Ordreo = new int[NbMarqueur];

  int i, j;
  int ClosestVertex = -1, ClosestTemp = -1;
  double MaxLOD = -1;

  Pere[root] = -1;
  lod[root] = -1.0;
  
  for (i=0; i<NbMarqueur; i++)
  {
    // recopie de l'ordre d'origine
    Ordreo[i] = ordre[i];

    if (i != root)
    {
      lod[i] = Cartage->GetTwoPointsLOD(ordre[i], 
					ordre[root]);
      Pere[i]=root;
      if (MaxLOD < lod[i])
      {
	MaxLOD = lod[i];
	ClosestVertex = i;
      }
    }
  }

  for (i=0; i<NbMarqueur-1; i++)
  {
    lod[ClosestVertex] = -1.0;
    MaxLOD = -1;
    for (j=0; j<NbMarqueur; j++)
      if (lod[j] >= 0)
      {
        if (lod[j] > Cartage->GetTwoPointsLOD(ordre[j], 
					      ordre[ClosestVertex]))
	{
	  lod[j] = Cartage->GetTwoPointsLOD(ordre[j], 
					    ordre[ClosestVertex]);
	  Pere[j] = ClosestVertex;
	}
	if (MaxLOD < lod[j])
	{
	  MaxLOD = lod[j];
          ClosestTemp = j;
	}
      }
      ClosestVertex = ClosestTemp;
    }
  /* Parcours de l'arbre en Pre-ordre */
  ClosestTemp= 0; /* servira de ptr de pile */
  Stack[ClosestTemp++] = root;
  i = 0; /* indice de remplissage de data->ordre */

  while (ClosestTemp)
  {
    root = Stack[--ClosestTemp];
    ordre[i] = Ordreo[root];
    i++;

    for (j=0; j < NbMarqueur; j++)
      if (Pere[j] == root)
	Stack[ClosestTemp++] = j;
  }
  
  for (i = 0; i < NbMarqueur-1; i++)
    tr[i]= Cartage->GetTwoPointsFR(ordre[i],
				   ordre[i+1]);
  
  UnConverge();

  delete [] Stack;
  delete [] Pere;
  delete [] lod;
  delete [] Ordreo;
}

//-----------------------------------------------------------------------------
// Carte aléatoire.
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Carte::BuildRandomMap(void)
{
  int i, IdxMarqueur, NbMarqueurLibre;
  intPtr TOrdreInit = new int[NbMarqueur];

  for (i=0; i<NbMarqueur; i++)
    TOrdreInit[i]=ordre[i];
      
  NbMarqueurLibre = NbMarqueur;

  for (i=0; i<NbMarqueur; i++)  { 
    IdxMarqueur=randomax((double)NbMarqueurLibre);
    ordre[i]=TOrdreInit[IdxMarqueur];
    Echange(&TOrdreInit[NbMarqueurLibre-1], &TOrdreInit[IdxMarqueur]);
    NbMarqueurLibre--; 
  }

  for (i = 0; i < NbMarqueur-1; i++)
    tr[i]= Cartage->GetTwoPointsFR(ordre[i], ordre[i+1]);

  UnConverge();
  ret = 0.2;

  delete [] TOrdreInit;
}

//-----------------------------------------------------------------------------
//Construit une bonne carte pour des tests aléatoires.
//-----------------------------------------------------------------------------
// Paramètres : 
// - 
// - 
// Valeur de retour : 1 si les données ne sont apparamment pas générées
// de facon aléatoire. Se base sur le nom des marqueurs nommés M1, M2, M3, ...
// lorsque la carte provient d'un générateur.
//-----------------------------------------------------------------------------

int Carte::BuildGoodMap(void)
{
  char Name[20];
  int i, j;
  int k = 0;
  
  for (i = 1; i <= Cartage->NbMS ; i++) {
    
    sprintf(Name, "M%d", i);
    
    for (j = 1; j <=  Cartage->NbMS; j++)
      if (strcmp(Name, Cartage->markers.keyOf(j).c_str()) == 0) {
	ordre[i - 1] = j;
        k++;
      }
  }
  
  if (k !=  Cartage->NbMS) return 1;
  
  for (j = 0; j < NbMarqueur - 1; j++)
    tr[j]= Cartage->GetTwoPointsFR(ordre[j],ordre[j+1]);
  
  ret = 0.3;
  UnConverge();

  return 0;

}

//-----------------------------------------------------------------------------
// test if the map is the correct one in random tests. S'appuie sur le
// fait que les cartes aleatoires sont ordonnees correctement (M1,
// M2...)
//-----------------------------------------------------------------------------
// Paramètres : 
// - une carte
// - 
// Valeur de retour : 0 = mauvaise, 1 bonne carte.
//-----------------------------------------------------------------------------

int Carte::GoodMap(void) const
{
  int i;
  int sens;
  
  sens = (atoi(Cartage->markers.keyOf(ordre[0]).c_str()+1) < atoi(Cartage->markers.keyOf(ordre[1]).c_str()+1));
  
  for (i=1; i< NbMarqueur-1; i++)  
    if ((atoi(Cartage->markers.keyOf(ordre[i]).c_str()+1) < 
	 atoi(Cartage->markers.keyOf(ordre[i+1]).c_str()+1))!= sens)
      return 0;
  
  return 1;
}
