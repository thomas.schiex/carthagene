//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_CO.cc,v 1.11.6.3 2012-08-02 15:43:54 dleroux Exp $
//
// Description : BJS_CO.
// Divers :
//-----------------------------------------------------------------------------

#include "BJS_CO.h"

#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_CO::BJS_CO() : BioJeuSingle()
{
  NbMeiose = 0;

}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - le type
// - le nombre de marqueurs
// - le nombre de contraintes
// - le champ de bit
// - le vecteur d'indice de marqueur
// - la liste des contraintes
// Valeur de retour :
//-----------------------------------------------------------------------------

BJS_CO::BJS_CO(CartaGenePtr cartag,
	       int id,
	       charPtr nomjeu,
	       CrossType cross,
	       int nm, 
	       int taille,
	       int bitjeu,
	       Constraint *cons)
{
  Cartage = cartag;
  Id = id;

  NomJeu = new char[strlen(nomjeu)+1];
  strcpy(NomJeu, nomjeu);

  Cross = cross;
  BitJeu = bitjeu;
  NbMarqueur = nm;


  NbMeiose = TailleEchant = 0;

  TailleEchant = taille;
  IndMarq_cg2bj = IndMarq_bj2cg = NULL;
  Echantillon = NULL; 
  // DL
  //TwoPointsFR = NULL; 
  //TwoPointsDH = NULL; 
  //TwoPointsLOD = NULL; 

  Constraints = cons;

}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_CO::~BJS_CO()
{
  Constraint * cons;
  
  while (Constraints)
    {
      cons = Constraints->Next;
      delete Constraints;
      Constraints = cons;
    }

}
//-----------------------------------------------------------------------------
// Compatibilite des observations entres individus
//-----------------------------------------------------------------------------
int BJS_CO::Compatible(int numarq1, int numarq2) const
{
  Constraint *con = Constraints;

  while (con)
    {
      if ((con->M1 == numarq1) && (con->M3 = numarq2)) return 0;
      if ((con->M1 == numarq2) && (con->M3 = numarq1)) return 0;

      con = con->Next;
    }
  return 1;
}
//-----------------------------------------------------------------------------
// Fusion des observations entres individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
//-----------------------------------------------------------------------------
void BJS_CO::Merge(int numarq1, int numarq2) const
{
  return;
}

//-----------------------------------------------------------------------------
// Affichage des donn�es
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//----------------------------------------------------------------------------

void BJS_CO::DumpEch(void) const
{
  Constraint *con = Constraints;
  
  print_out( "%10s %10s %10s %10s\n", "Marker", "Marker", "Marker", "Penalty");

  while (con)
    {
      print_out( "%10s %10s %10s %10.3f\n", 
	     Cartage->markers.keyOf(con->M1).c_str(),
	     Cartage->markers.keyOf(con->M2).c_str(),
	     Cartage->markers.keyOf(con->M3).c_str(),
	     con->Penalty);
      
      con = con->Next;
    }
}

int BJS_CO::Couplex(int m1, int m2) const {
  return (0);
};

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance
//-----------------------------------------------------------------------------
// Param�tres : 
// - une carte
// - seuil de convergence
// Valeur de retour : le maximum de vraisemblance
//-----------------------------------------------------------------------------

double BJS_CO::ComputeEM(Carte *map)
{
  return (-(Constraints->CheckAll(map)));
}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//  Une interface plus sophistiquee:
//  On converge jusqu'a epsilon1. Si la vraisemblance est inferieure a
//  threshold on laisse tomber, sinon on converge a epsilon2
//-----------------------------------------------------------------------------
// Param�tres : 
// - une carte
// - niveau a partir duquel on convergera a epsilon2
// - premier seuil de convergence (epsilon1)
// - second seuil de convergence (epsilon2)
// Valeur de retour : la vraisemblance maximum.
//-----------------------------------------------------------------------------

double BJS_CO::ComputeEMS(Carte *map, 
			  double threshold)
{  
  return (-(Constraints->CheckAll(map)));
}


//-----------------------------------------------------------------------------
// Affichage d�taill� d'une carte(m�me principe que computeEM)
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte
// -
//-----------------------------------------------------------------------------

void BJS_CO::PrintDMap(Carte *data, int envers, Carte *dataref)
{
  print_out( "\nData Set Number %2d :\n", Id);
  print_out(  "\n");
  Constraints->CheckIAll(data);

}
