//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Greedy.h,v 1.13 2002-10-08 09:43:41 tschiex Exp $
//
// Description : Prototype de la classe Greedy (le glouton).
// Divers :
//-----------------------------------------------------------------------------

#ifndef _GREEDY_H
#define _GREEDY_H

#include "GenotypeBC.h"
#include "GenotypeIC.h"
#include "GenotypeRH.h"
#include "MiniCarte.h"


///
class Greedy {
 public:
  
  /** Constructeur.
      @param GT Le g�notype sur lequel porte le traitement.
  */
  Greedy(Genotype *GT);
  
  /** Destructeur.
    */
  ~Greedy();
  
  /** Lancement de Greedy.
      La recherche de la meilleure carte est faite avec l'algorithme de Greedy.
      Durant la recherche, les cartes trouv�es sont stock�es dans le tas et
      rang�es dans l'ordre de cout d�croissant.
      
      En fin de calcul, le tas est d�pil�. Suivant les drapeaux utilis�s,
      les cartes sont affich�es. Une liste \Ref{ListeMiniCarte} est construite
      � partir de certains �l�ments (\Ref{MiniCarte}) des cartes d�pil�es
      puis retourn�e.

      ATTENTION: la liste ListeMiniCarte est construite au niveau de la
      commande \Ref{Tas}::WriteBest(). Pensez � la d�truire.
      
      @param Vflag drapeau du mode verbeux des r�sultats en cours de traitement.
      @param Qflag drapeau du mode silencieux sur la progression du traitement.
      @param Pflag drapeau de raffinage.
      @param Fsize taille de la fen�tre.
      @param Fthres seuil du flip
      @param Fiter flip it�ratif ou pas
      @param NR Nombre de boucles principales.
      @param NI Nombre d'it�rations.
      @param TMin TabooMin.
      @param TMax TabooMax.
      @param OM nom du fichier de sortie des r�sultats.
      @param OS autre fichier de r�sultats.
      @return la liste de cartes contenues dans le tas en fin de calcul.
  */
  ListeMiniCarte * Run(int Vflag = 0, 
		       int Qflag = 0, 
		       int Pflag = 0,
		       int Fsize = 3,
		       double Fthres = 3.0,
		       int Fiter = 0,
		       int NR = 1, 
		       int NI = 0,
		       int TMin = 1, 
		       int TMax = 15,
		       char *OM = "/dev/null", 
		       char *OS = "/dev/null");
  
 private:
  
  /** Le g�notype �tudi�.
    */
  Genotype *MatGen;
  
};

#endif /* _GREEDY_H */
