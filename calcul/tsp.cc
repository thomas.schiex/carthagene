//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: tsp.cc,v 1.37.2.3 2012-08-02 15:43:55 dleroux Exp $
//
// Description : Classe API du module.
// Divers : 
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <math.h>
#ifdef ppc
#include "/Developer/Headers/FlatCarbon/fp.h"
#endif
#include "CartaGene.h"
#include "BioJeu.h"
#include "BioJeuSingle.h"
#include "BJS_RH.h"
#include "BJS_BC.h"
#include "BJS_OR.h"
#include "BioJeuMerged.h"
#include "tsp.h"
#include "NOrCompMulti.h"

#ifdef sun4
#include <ieeefp.h>
#endif

#ifdef __WIN32__
#include <float.h>
#endif

#ifdef LKHLICENCE
extern "C" {
#include "LKH.h"
}
#endif

/* maximum elementary cost in TSP formulation (LKH uses integer cost) */
/* should work with log10-likelihood greater than -10000 */
#define INFINI 10000000
#define MULTIPLY 1000
#define UNKNOWNCOST 0  /* multiplied by MULTIPLY */
#define MAXCOEF ((INFINI/100.)/MULTIPLY)
#define MINCOEF (1./MULTIPLY)

#ifdef __WIN32__
#   define FINITE _finite
#else
#   define FINITE finite
#endif

/* Return the number of Obligate Chromosome Breaks */
double BioJeuSingle::ContribOCB(int m1, int m2)
{
    if (Cross == Ordre) return 0.;
    if (Cross != RH && Cross != BC) {
        print_out("Error: OCB criterion not implemented for this data type.\n");
        throw BioJeu::NotImplemented();
        return 0.;
    }

    int ocb = 0;
    for (int m = 1; m <= TailleEchant; m++) {
        if (((GetEch(m1, m) == 1) && (GetEch(m2, m) == 0)) || 
                ((GetEch(m1, m) == 0) && (GetEch(m2, m) == 1))) {
            ocb++;
        }
    }
    return (double) ocb;
}

/* Return the number of Obligate Chromosome Breaks */
double BioJeuSingle::NormContribOCB(int m1, int m2)
{
    if (Cross == Ordre) return 0.;
    if (Cross != RH && Cross != BC) {
        print_out("Error: normalized OCB criterion not implemented for this data type.\n");
        throw BioJeu::NotImplemented();
        return 0.;
    }

    int knowns = 0;
    int ocb = 0;
    for (int m = 1; m <= TailleEchant; m++) {
        if ((GetEch(m1, m) != Obs1111) && (GetEch(m2, m) != Obs1111)) {
            knowns++;
            if (((GetEch(m1, m) == 1) && (GetEch(m2, m) == 0)) || 
                    ((GetEch(m1, m) == 0) && (GetEch(m2, m) == 1))) {
                ocb++;
            }
        }
    }
    if (knowns == 0) {
        //    printf("Warning!!! No known for markers %d,%d.\n",m1,m2); 
        return UNKNOWNCOST; /* valeur arbitraire! */
    }
    return (((double) ocb) * TailleEchant) / knowns;
}

int CartaGene::OCB(int nbmap) const
{

    if (Heap->HeapSize == 0) 
    {
        print_err(  "Error : Empty heap.\n");
        return -1;
    }

    if ( nbmap < 0 || nbmap >= Heap->HeapSize )
    {
        print_err(  "Error : This map does not exist.\n");
        return -1;
    }

    Carte *carte = Heap->MapFromId(nbmap);
    int ocb = 0;
    try {
        for (int j = 0; j < NbMS-1; j++) {
            ocb += (int) ArbreJeu->ContribOCB(carte->ordre[j], carte->ordre[j+1]);
        }
    } catch (BioJeu::NotImplemented) { }
    return ocb;
}

double BJS_RH::Retention2pt()
{
    int n1 = 0;
    int n = 0;
    for (int m1 = 0; m1 < Cartage->NbMS; m1++) {
        for (int m = 1; m <= TailleEchant; m++) {
            if (GetEch(Cartage->MarkSelect[m1], m) != Obs1111) {
                if (GetEch(Cartage->MarkSelect[m1], m) == 1) {
                    n1++;
                }
                n++;
            }
        }
    }
    if (n==0) return 1.;
    else return (double)n1/n;
}

void BJS_RH::InitContribLogLike2pt()
{
    /*printf("marksel(%i)=", Cartage->NbMS);
      char* sep="[";
      for(int i=0;i<Cartage->NbMS;++i) {
      printf("%s%i", sep, Cartage->MarkSelect[i]);
      sep = ", ";
      }
      printf("]\n");*/
    RetentionRH = Retention2pt();
}

/* Return -log10(likelihood(m1)) */
/* n1=n+, n0=n- */
double BJS_RH::ContribLogLike2pt(int m1)
{
    int ss[2] = {0,0};
    for (int m = 1; m <= TailleEchant; m++)    {
        if (GetEch(m1, m) != Obs1111) {
            ss[GetEch(m1, m)]++;
        }
    }
    int n1 = ss[1];
    int n0 = ss[0];
    double q = 1 - RetentionRH;

    return -(n1 * log10(RetentionRH) / 2 + n0 * log10(q) / 2);
}

double BJS_RH::NormContribLogLike2pt(int m1)
{
    int ss[2] = {0,0};
    for (int m = 1; m <= TailleEchant; m++)    {
        if (GetEch(m1, m) != Obs1111) {
            ss[GetEch(m1, m)]++;
        }
    }
    int n1 = ss[1];
    int n0 = ss[0];
    double q = 1 - RetentionRH;

    return -((double) TailleEchant * (n1 * log10(RetentionRH) / 2 + n0 * log10(q) / 2) / (n0 + n1));
}

/* Return -log10(likelihood(m1,m2)) */
/* n corresponds to the extended TSP reduction as defined by Agarwala00 */
/* n = number of knowns */
/* n1=n++=n11, n2=n10+n01= n+- + n-+, n3=n--=n00 */
double BJS_RH::ContribLogLike2pt(int m1, int m2)
{
    int ss[4] = {0,0,0,0};
    for (int m = 1; m <= TailleEchant; m++)    {
        if ((GetEch(m1, m) != Obs1111) && (GetEch(m2, m) != Obs1111)) {
            ss[GetEch(m1, m)*2 + GetEch(m2, m)]++;
        }
    }
    int n1 = ss[3];
    int n2 = ss[1] + ss[2];
    int n3 = ss[0];
    int n = n1+n2+n3;
    if (n == 0) {
        //    printf("Warning!!! No known for markers %d,%d.\n",m1,m2); 
        return UNKNOWNCOST; /* valeur arbitraire! */
    }
    double q = 1 - RetentionRH;

    double a = RetentionRH*q*n;
    double b = n - RetentionRH*n1 - q*n3; /* = -b */
    double c = n2;
    double delta = b*b - 4*a*c;

    if (delta < 0) {
        print_out("Warning!!! The breakage probability is unknown (negative delta=%f) for markers %d,%d.\n",delta,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    errno = 0;
    double theta = (b - sqrt(delta))/(2*a);
    if (errno == EDOM) {
        print_out("Warning!!! Error in square root function (delta=%g,errno=%d, EDOM=%d, ERANGE=%d) for markers %d,%d.\n",delta,errno,EDOM,ERANGE,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    if (theta > 1) theta = 1;
    if (theta < EM_MIN_THETA_HR) theta = EM_MIN_THETA_HR;

    errno = 0;
    double loglike = (n1*log10(1-theta*q) + n2*(log10(theta) + (log10(RetentionRH)+log10(q))/2) + n3*log10(1-theta*RetentionRH));
    if (errno == EDOM || errno == ERANGE) {
        print_out("Warning!!! Error in log10 function (errno=%d, EDOM=%d, ERANGE=%d) for markers %d,%d.\n",errno,EDOM,ERANGE,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }

    if (!FINITE(loglike)) {

        print_out("Warning!!! InFINITE loglike for markers m1=%d, m2=%d: loglike=%f\n",m1,m2,loglike);
    }

    return -loglike;
}

/* return theta computed analytically */
double BJS_RH::Theta2pt(int m1, int m2, double retentionRH)
{
    int ss[4] = {0,0,0,0};
    for (int m = 1; m <= TailleEchant; m++)    {
        if ((GetEch(m1, m) != Obs1111) && (GetEch(m2, m) != Obs1111)) {
            ss[GetEch(m1, m)*2 + GetEch(m2, m)]++;
        }
    }
    int n1 = ss[3];
    int n2 = ss[1] + ss[2];
    int n3 = ss[0];
    int n = n1+n2+n3;
    if (n == 0) {
        //    printf("Warning!!! No known for markers %d,%d.\n",m1,m2); 
        return UNKNOWNCOST; /* valeur arbitraire! */
    }
    double q = 1 - retentionRH;

    double a = retentionRH*q*n;
    double b = n - retentionRH*n1 - q*n3; /* = -b */
    double c = n2;
    double delta = b*b - 4*a*c;

    if (delta < 0) {
        print_out("Warning!!! The breakage probability is unknown (negative delta=%f) for markers %d,%d.\n",delta,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    errno = 0;
    double theta = (b - sqrt(delta))/(2*a);
    if (errno == EDOM) {
        print_out("Warning!!! Error in square root function (delta=%g, errno=%d, EDOM=%d, ERANGE=%d) for markers %d,%d.\n",delta,errno,EDOM,ERANGE,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    if (theta > 1) theta = 1;
    if (theta < EM_MIN_THETA_HR) theta = EM_MIN_THETA_HR;

    return theta;
}

/* n = number of knowns */
/* n1=n++=n11, n2=n10+n01= n+- + n-+, n3=n--=n00 */
double BJS_RH::NormContribLogLike2pt(int m1, int m2)
{
    int ss[9] = {0,0,0,0,0,0,0,0,0};
    for (int m = 1; m <= TailleEchant; m++)    {
        int ech1 = ((GetEch(m1, m) != Obs1111) ? GetEch(m1, m) : 2);
        int ech2 = ((GetEch(m2, m) != Obs1111) ? GetEch(m2, m) : 2);
        ss[ech1 * 3 + ech2]++;
    }
    int n00 = ss[0];
    int n01 = ss[1];
    //  int n02 = ss[2];
    int n10 = ss[3];
    int n11 = ss[4];
    //  int n12 = ss[5];
    //  int n20 = ss[6];
    //  int n21 = ss[7];
    //  int n22 = ss[8];
    int n1 = n11;
    int n2 = n10 + n01;
    int n3 = n00;
    int n = n1+n2+n3;
    if (n == 0) {
        //    printf("Warning!!! No known for markers %d,%d.\n",m1,m2); 
        return UNKNOWNCOST; /* valeur arbitraire! */
    }
    double q = 1 - RetentionRH;

    double a = RetentionRH*q*n;
    double b = n - RetentionRH*n1 - q*n3; /* = -b */
    double c = n2;
    double delta = b*b - 4*a*c;

    if (delta < 0) {
        print_out("Warning!!! The breakage probability is unknown (negative delta=%f) for markers %d,%d.\n",delta,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    errno = 0;
    double theta = (b - sqrt(delta))/(2*a);
    if (errno == EDOM) {
        print_out("Warning!!! Error in square root function (delta=%g, errno=%d, EDOM=%d, ERANGE=%d) for markers %d,%d.\n",delta,errno,EDOM,ERANGE,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    if (theta > 1) theta = 1;
    if (theta < EM_MIN_THETA_HR) theta = EM_MIN_THETA_HR;

    errno = 0;
    double A = log10(1-theta*RetentionRH);
    double B = log10(1-theta*q);
    double C = (log10(theta) + (log10(RetentionRH)+log10(q))/2);
    double loglike = ((double) TailleEchant * (n00*A + n11*B + (n10+n01)*C) / n);
    if (errno == EDOM || errno == ERANGE) {
        print_out("Warning!!! Error in log10 function (errno=%d, EDOM=%d, ERANGE=%d) for markers %d,%d.\n",errno,EDOM,ERANGE,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }

    if (!FINITE(loglike)) {
        print_out("Warning!!! InFINITE loglike for markers m1=%d, m2=%d: loglike=%f\n",m1,m2,loglike);
    }

    return -loglike;
}

double BJS_BC::ContribLogLike2pt(int m1)
{
    return -log10(0.5) * TailleEchant / 2.;
}

double BJS_BC::ContribLogLike2pt(int m1, int m2)
{
    int nrec = 0; 
    int ndata = 0;

    for (int i = 1; i <= TailleEchant; i++)
        if ((GetEch(m1, i) != Obs1111) && (GetEch(m2, i) != Obs1111))
        {
            ndata++;
            if (GetEch(m1, i) != GetEch(m2, i)) nrec++;
        }

    if (ndata == 0) {
        //    printf("Warning!!! No known for markers %d,%d.\n",m1,m2); 
        return UNKNOWNCOST; /* valeur arbitraire! */
    }

    double theta = (double) nrec/ndata;
    if (theta > EM_MAX_THETA) theta = EM_MAX_THETA;
    if (theta < EM_MIN_THETA) theta = EM_MIN_THETA;

    errno = 0;
    double loglike = (ndata-nrec)*log10(1-theta) + nrec*log10(theta);
    if (errno == EDOM || errno == ERANGE) {
        print_out("Warning!!! Error in log10 function (theta=%g, errno=%d, EDOM=%d, ERANGE=%d) for markers %d,%d.\n",theta,errno,EDOM,ERANGE,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    return -loglike;
}

double BJS_BC::NormContribLogLike2pt(int m1, int m2)
{
    int nrec = 0; 
    int ndata = 0;

    for (int i = 1; i <= TailleEchant; i++)
        if ((GetEch(m1, i) != Obs1111) && (GetEch(m2, i) != Obs1111))
        {
            ndata++;
            if (GetEch(m1, i) != GetEch(m2, i)) nrec++;
        }

    if (ndata == 0) {
        //    printf("Warning!!! No known for markers %d,%d.\n",m1,m2); 
        return UNKNOWNCOST; /* valeur arbitraire! */
    }

    double theta = (double) nrec/ndata;
    if (theta > EM_MAX_THETA) theta = EM_MAX_THETA;
    if (theta < EM_MIN_THETA) theta = EM_MIN_THETA;

    errno = 0;
    double loglike = (double) TailleEchant * ((ndata-nrec)*log10(1-theta) + nrec*log10(theta)) / ndata;
    if (errno == EDOM || errno == ERANGE) {
        print_out("Warning!!! Error in log10 function (theta=%g, errno=%d, EDOM=%d, ERANGE=%d) for markers %d,%d.\n",theta,errno,EDOM,ERANGE,m1,m2);
        return HUGE_VAL; /* valeur arbitraire! */
    }
    return -loglike;
}

double contribLogLike2pt1(BioJeu *ArbreJeu, int m1) {return ArbreJeu->ContribLogLike2pt(m1);}
double contribLogLike2pt2(BioJeu *ArbreJeu, int m1, int m2) {return ArbreJeu->ContribLogLike2pt(m1,m2);}
double normContribLogLike2pt1(BioJeu *ArbreJeu, int m1) {return ArbreJeu->NormContribLogLike2pt(m1);}
double normContribLogLike2pt2(BioJeu *ArbreJeu, int m1, int m2) {return ArbreJeu->NormContribLogLike2pt(m1,m2);}
double contribOCB(BioJeu *ArbreJeu, int m1, int m2) {return ArbreJeu->ContribOCB(m1,m2);}
double normContribOCB(BioJeu *ArbreJeu, int m1, int m2) {return ArbreJeu->NormContribOCB(m1,m2);}
double contribZero(BioJeu *ArbreJeu, int m1) {return 0.;}
double contribLOD(BioJeu *ArbreJeu, int m1, int m2) {return -1. * ArbreJeu->GetTwoPointsLOD(m1,m2);}
double contribHaldane(BioJeu *ArbreJeu, int m1, int m2) {return ArbreJeu->GetTwoPointsDH(m1,m2);}

void cg2tsp(BioJeu *ArbreJeu, int NbMS, int MarkSelect[], char *tspName, funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
{
    FILE *Tsp;
    if ((Tsp = fopen(tspName, "w")) == NULL) {
        perror(tspName);
        return;
    }
    fprintf(Tsp, "NAME: %d_%d.cg\n", NbMS, MULTIPLY);
    fprintf(Tsp, "TYPE: TSP\n");
    fprintf(Tsp, "DIMENSION: %d\n", NbMS+1);
    fprintf(Tsp, "EDGE_WEIGHT_TYPE: EXPLICIT\n");
    fprintf(Tsp, "EDGE_WEIGHT_FORMAT: UPPER_ROW\n");
    fprintf(Tsp, "EDGE_WEIGHT_SECTION\n");
    try {
        for (int m1 = 0; m1 < NbMS; m1++) {
            for (int m2 = m1+1; m2 <= NbMS; m2++) {
                double res = 0;
                if (m1 == m2) {
                    res = 0;
                } else {
                    if (m1 == 0 || m2 == 0) {
                        if (m1 == 0) {
                            res = MULTIPLY * mycontribLogLike2pt1(ArbreJeu, MarkSelect[m2-1]);
                        } else {
                            res = MULTIPLY * mycontribLogLike2pt1(ArbreJeu, MarkSelect[m1-1]);
                        }	    
                    } else {
                        res = mycontribLogLike2pt2(ArbreJeu, MarkSelect[m1-1],MarkSelect[m2-1]);
                        if (res != HUGE_VAL) {
                            res *= MULTIPLY;
                        }
                    }
                }

                if (!(FINITE(res)) || res == HUGE_VAL || res > INT_MAX) { /* res > INFINI */
                    fprintf(Tsp, "%d ", INFINI);
                } else {
                    fprintf(Tsp, "%d ", (int)res);
                }
            }
            fprintf(Tsp, "\n");
        }
    } catch (BioJeu::NotImplemented) { }
    fclose(Tsp);
}

/* Create a TSPLIB format file, whose name is tspName, with the current loaded data */
/* 0 is the dummy marker (start city) */
/* note that TSPLIB city numbers starts from 1 and not 0 */
/* so, the start city is number 1 */
void CartaGene::cg2tsp(char *tspName)
{
    if (ArbreJeu == NULL) 
    {
        print_err(  "Error: No Data Set Loaded..\n");
        return;
    }

    /* initialisation des structures utiles aux fonctions ContribLogLike2pt */
    ArbreJeu->InitContribLogLike2pt(); 

    print_out("All TSP costs multiplied by %d.\n", MULTIPLY);
    ::cg2tsp(ArbreJeu, NbMS, MarkSelect, tspName, contribLogLike2pt1, contribLogLike2pt2);
    char tspnameext[256];
    strcpy(tspnameext, "norm");
    strcat(tspnameext, tspName);
    ::cg2tsp(ArbreJeu, NbMS, MarkSelect, tspnameext, normContribLogLike2pt1, normContribLogLike2pt2);
    strcpy(tspnameext, "lod");
    strcat(tspnameext, tspName);
    ::cg2tsp(ArbreJeu, NbMS, MarkSelect, tspnameext, contribZero, contribLOD);
    strcpy(tspnameext, "dist");
    strcat(tspnameext, tspName);
    ::cg2tsp(ArbreJeu, NbMS, MarkSelect, tspnameext, contribZero, contribHaldane);
    strcpy(tspnameext, "ocb");
    strcat(tspnameext, tspName);
    ::cg2tsp(ArbreJeu, NbMS, MarkSelect, tspnameext, contribZero, contribOCB);
    strcpy(tspnameext, "ocbnorm");
    strcat(tspnameext, tspName);
    ::cg2tsp(ArbreJeu, NbMS, MarkSelect, tspnameext, contribZero, normContribOCB);
}

/* fill in the current Pareto frontier by evaluating the maps in the Carthagene's heap */
void updatePareto(CartaGene *Cartage, double coef, int side, Carte **PseudoPareto, double *PseudoParetoTrueLikelihood, double *PseudoParetoCoef, int *PseudoParetoStatus)
{
    if (!Cartage->QuietFlag) {
        print_out("LKH found %d solutions.\n", Cartage->Heap->HeapSize);
    }
    int BestId = Cartage->Heap->Best()->Id;
    for (int i = 0; i<Cartage->Heap->HeapSize; i++) {
        int bp =  Cartage->BreakPointsMap(Cartage->ArbreJeu->Id, i);
        double rh =  Cartage->Heap->MapFromId(i)->coutEM - Cartage->ComputeEMOrder(Cartage->ArbreJeu->Id, i);
        if (PseudoPareto[bp]==NULL || PseudoParetoTrueLikelihood[bp] < rh - PARETOEPSILON) {
            if (PseudoPareto[bp]!=NULL) delete PseudoPareto[bp];
            Carte *newmap = new Carte(*Cartage->Heap->MapFromId(i));
            PseudoPareto[bp] = newmap;
            PseudoParetoTrueLikelihood[bp] = rh;
            newmap->coutEM = rh;
            PseudoParetoCoef[bp] = coef;
            PseudoParetoStatus[bp] = side;
            if (i == BestId) PseudoParetoStatus[bp] |= SUPPORTEDPARETO;
        }
    }
    if (PseudoPareto[0]!=NULL) {
        PseudoParetoStatus[0] |= PARETO;
    }
    for (int bp = 1; bp<Cartage->NbMS; bp++) {
        if (PseudoPareto[bp]!=NULL) {
            PseudoParetoStatus[bp] &= (PARETO - 1);
            int dominated = 0;
            for (int bp2 = 0; bp2<bp; bp2++) {
                if (PseudoPareto[bp2]!=NULL && 
                        PseudoParetoTrueLikelihood[bp] < PseudoParetoTrueLikelihood[bp2] - PARETOEPSILON) {
                    dominated = 1;
                    break;
                }
            }
            if (!dominated) {
                PseudoParetoStatus[bp] |= PARETO;
            }
        }
    }
}


// DL
/** Check that ArbreJeu contains an Ordre dataset. */
bool checkJeuOrdre(BioJeu* b, BJS_OR** bjo) {
    BioJeuMerged* bjm;
    if(!b) {
        return false;
    }
    switch(b->Cross) {
        case Mor:
            bjm = dynamic_cast<BioJeuMerged*>(b);
            return checkJeuOrdre(bjm->BJgauche, bjo)||checkJeuOrdre(bjm->BJdroite, bjo);
        case Ordre:
            *bjo = dynamic_cast<BJS_OR*>(b);
            return true;
        default:;
                return false;
    };
}

/* create the Pareto frontier by iteratively applying LKH and a weighted sum objective with different coefficient values. At each step, LKH restarts from the best solution found by the previous iteration. */
void CartaGene::ParetoLKH(int resolution, int nbrun, int backtrack, funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
{
    int i,k,bp;
    BJS_OR* bjo=NULL;

    /* some consistency checks */
    if (NbMS == 0) 
    {
        print_err(  "Error: Empty selection of loci.\n");
        return;
    }
    /*int ok = 0;*/
    /*for (i=1; i<=NbJeu; i++) {*/
    /*if (Jeu[i]->Cross == Ordre) {*/
    /*ok = 1;*/
    /*break;*/
    /*}*/
    /*}*/
    /*if (!ok) {*/
    if(!checkJeuOrdre(ArbreJeu, &bjo)) {
        print_err("Error: there is no reference order data set loaded! The current data set should be merged by order from one Biological Data Set and one reference order data set.\n");
        return;
    }

    /* data structure initialization */
    Carte **PseudoPareto = new cartePtr[NbMS];
    double *PseudoParetoTrueLikelihood = new double[NbMS];
    int *PseudoParetoStatus = new int[NbMS];
    double *PseudoParetoCoef = new double[NbMS];

    for (i=0; i<NbMS; i++) {
        PseudoPareto[i] = NULL;
        PseudoParetoTrueLikelihood[i] = 0.;
        PseudoParetoCoef[i] = -1;
        PseudoParetoStatus[i] = 0;
    }
    Carte *LastMap= NULL;

    /* find objective ranges */
    SetBreakPointCoef(ArbreJeu->Id, MINCOEF);
    Heap->Initsoft(this, MAXHEAPSIZETSP);
    lkh(nbrun,backtrack,mycontribLogLike2pt1,mycontribLogLike2pt2);
    LastMap = Heap->Best();
    Carte BestRHMap = Carte(*LastMap);
    int WorstBP = BreakPointsMap(ArbreJeu->Id, LastMap->Id);
    double BestRH = LastMap->coutEM - ComputeEMOrder(ArbreJeu->Id, LastMap->Id);
    updatePareto(this,MINCOEF,RHFIRST,PseudoPareto,PseudoParetoTrueLikelihood,PseudoParetoCoef,PseudoParetoStatus);

    SetBreakPointCoef(ArbreJeu->Id, MAXCOEF);
    Heap->Initsoft(this, MAXHEAPSIZETSP);
    lkh(nbrun,backtrack,mycontribLogLike2pt1,mycontribLogLike2pt2);
    updatePareto(this,MAXCOEF,BPFIRST,PseudoPareto,PseudoParetoTrueLikelihood,PseudoParetoCoef,PseudoParetoStatus);
    LastMap = Heap->Best();
    Carte BestBPMap = Carte(*LastMap);
    int BestBP = BreakPointsMap(ArbreJeu->Id, LastMap->Id);
    double WorstRH = LastMap->coutEM - ComputeEMOrder(ArbreJeu->Id, LastMap->Id);
    if (BestRH < WorstRH) {
        double tmp = BestRH;
        BestRH = WorstRH;
        WorstRH = tmp;
    }
    if (BestBP == WorstBP) WorstBP = BestBP + 1;
    else if (BestBP > WorstBP) {
        int tmp = BestBP;
        BestBP = WorstBP;
        WorstBP = tmp;
    }
    double RangeRatio = (BestRH-WorstRH)/(WorstBP-BestBP);

    /* try different coeff values starting from the biological data set point of view */
    LastMap = &BestRHMap;
    for (k=1; k<resolution; k++) {
        if (StopFlag) break;
        double newcoef = RangeRatio * (((double) k)/(resolution - k));
        if (newcoef < MINCOEF || newcoef > MAXCOEF) {
            print_out("Step %d: ************* Warning! Weighted objective coef %f outside of its maximum range [%f,%f]\n", k, newcoef, MINCOEF, MAXCOEF);      
        } else {
            if (!QuietFlag) {
                print_out("Step %d: Weighted objective coef %f\n", k, newcoef);      
            }
        }
        SetBreakPointCoef(ArbreJeu->Id, newcoef);
        Heap->Initsoft(this, MAXHEAPSIZETSP);
        LastMap->UnConverge();
        Heap->Insert(LastMap, 0);
        lkh(nbrun,backtrack,mycontribLogLike2pt1,mycontribLogLike2pt2);
        updatePareto(this,newcoef,RHFIRST,PseudoPareto,PseudoParetoTrueLikelihood,PseudoParetoCoef,PseudoParetoStatus);
        LastMap = Heap->Best();
        LastMap = new Carte(*LastMap);
    }

    /* try different coeff values starting from the reference order data set point of view */
    LastMap = &BestBPMap;
    for (k=1; k<resolution; k++) {
        if (StopFlag) break;
        double newcoef = RangeRatio * ((resolution - k)/((double) k));
        if (newcoef < MINCOEF || newcoef > MAXCOEF) {
            print_out("Step %d: ************* Warning! Weighted objective coef %f outside of its maximum range [%f,%f]\n", k, newcoef, MINCOEF, MAXCOEF);      
        } else {
            if (!QuietFlag) {
                print_out("Step %d: Weighted objective coef %f\n", k, newcoef);      
            }
        }
        SetBreakPointCoef(ArbreJeu->Id, newcoef);
        Heap->Initsoft(this, MAXHEAPSIZETSP);
        LastMap->UnConverge();
        Heap->Insert(LastMap, 0);
        lkh(nbrun,backtrack,mycontribLogLike2pt1,mycontribLogLike2pt2);
        updatePareto(this,newcoef,BPFIRST,PseudoPareto,PseudoParetoTrueLikelihood,PseudoParetoCoef,PseudoParetoStatus);
        LastMap = Heap->Best();
        LastMap = new Carte(*LastMap);
    }

    if (StopFlag) {StopFlag = 0;}
    print_out("Initial ranges: Log10-likelihood [%.3f,%.3f] / BP [%d,%d] = %.4f\n", WorstRH, BestRH, BestBP, WorstBP, RangeRatio);

    /* reinsert all pseudo Pareto maps in the heap */
    SetBreakPointCoef(ArbreJeu->Id, 0.);
    Heap->Initsoft(this, NbMS);
    WorstBP = 0;
    BestRH = -1e100;
    for (bp = 0; bp<NbMS; bp++) {
        if (PseudoPareto[bp]) {
            if (PseudoParetoTrueLikelihood[bp] > BestRH) {
                BestRH = PseudoParetoTrueLikelihood[bp];
                WorstBP = bp;
            }
        }
    }
    for (bp = 0; bp<=WorstBP; bp++) {
        if (PseudoPareto[bp]) {
            Heap->Insert(PseudoPareto[bp], 0);
        }
    }

    /* print specific information about the Pareto frontier */
    ParetoInfo(0, NULL, PseudoParetoCoef, PseudoParetoStatus, 1, NULL);

    /* free memory */
    for (i = 0; i<NbMS; i++) {
        if (PseudoPareto[i] != NULL) delete PseudoPareto[i];
    }
    delete[] PseudoPareto;
    delete[] PseudoParetoTrueLikelihood;
    delete[] PseudoParetoCoef;
    delete[] PseudoParetoStatus;
}

/* print information about the Pareto frontier found in the current CartaGene's heap */
void CartaGene::ParetoInfo(int graphicview, char *cgscriptdir, double *PseudoParetoCoef, int *PseudoParetoStatus, double ratio, intPtr *lmapidout)
{
    int i,bp,bp2,j;
    BJS_OR* bjo = NULL;
    static char bouf[4096];
    /* some consistency checks */
    if (Heap->HeapSize == 0) 
    {
        print_err(  "Error : Empty heap.\n");
        return;
    }
    /*int ok = 0;*/
    /*for (i=1; i<=NbJeu; i++) {*/
    /*if (Jeu[i]->Cross == Ordre) {*/
    /*ok = 1;*/
    /*break;*/
    /*}*/
    /*}*/
    /*if (!ok) {*/
    if(!checkJeuOrdre(ArbreJeu, &bjo)) {
        print_err("Error: there is no reference order data set loaded! The current data set should be merged by order from one Biological Data Set and one reference order data set.\n");
        return;
    }

#ifndef __WIN32__
    FILE *tmppareto = NULL;
    if (graphicview>0) tmppareto = fopen("tmppareto","w");
#endif

    int *vc;

    /*int nc =  ((BJS_OR*)Jeu[i])->GetnLocipChrom(&vc);*/
    int nc = bjo->GetnLocipChrom(&vc);
    if (NOrComput != NULL) delete NOrComput;
    NOrComput = new NOrCompMulti(NbMS-1, vc, nc);
    NOrComput->run();

    int *PseudoParetoId = new int[NbMS];
    double *PseudoParetoTrueLikelihood = new double[NbMS];
    int WorstBP = 0;
    int BestBP = INT_MAX;
    double WorstRH = 0;
    double BestRH = -1e100;

    int *dpliss = new int[NbMS];

    for (bp=0; bp<NbMS; bp++) {
        PseudoParetoId[bp] = -1;
        PseudoParetoTrueLikelihood[bp] = 0;
    }
    SetBreakPointCoef(ArbreJeu->Id, 0.);
    for (i=0; i<Heap->HeapSize; i++) {
        /* recompute true log-like without breakpoint */
        Heap->MapFromId(i)->UnConverge();
        double rh = ComputeEM(Heap->MapFromId(i));
        int bp = BreakPointsMap(ArbreJeu->Id, i);
        if (PseudoParetoId[bp] == -1 || PseudoParetoTrueLikelihood[bp] < rh - PARETOEPSILON) {
            PseudoParetoId[bp] = i;
            PseudoParetoTrueLikelihood[bp] = rh;
            if (bp < BestBP) {
                BestBP = bp;
                WorstRH = rh;
            } else if (bp == BestBP) {
                if (rh > WorstRH) WorstRH = rh;
            }
            if (rh > BestRH + PARETOEPSILON) {
                BestRH = rh;
                WorstBP = bp;
            } else if (rh <= BestRH + PARETOEPSILON && rh >= BestRH - PARETOEPSILON) {
                if (bp < WorstBP) WorstBP = bp;
            }
        }
    }
    double RangeRatio = (BestRH-WorstRH)/(WorstBP-BestBP);

    int nbsolutions = 0;
    for (bp = BestBP; bp<=WorstBP; bp++) {
        if (PseudoParetoId[bp] != -1) nbsolutions++;
    }
    print_out("\nPareto frontier approximation (coverage: %d / %d = %.2f %%):\n", nbsolutions, WorstBP - BestBP + 1, (100. * nbsolutions) / (WorstBP - BestBP + 1));
    print_out("Ranges: Log10-likelihood [%.3f,%.3f] / BP [%d,%d] = %.4f\n", WorstRH, BestRH, BestBP, WorstBP, RangeRatio);

    /* check if the marker names are equal to 1,2,...,NbMS */
    int validation = 1;
    for (i = 0; i<NbMS; i++) {
        if (atoi(GetMrkName(MarkSelect[i]))!=i+1) {
            validation = 0;
            break;
        }
    }

    if (PseudoParetoStatus == NULL) {
        PseudoParetoStatus = new int[NbMS];
        for (bp = 0; bp<NbMS; bp++) {
            PseudoParetoStatus[bp] = 0;
        }
    }
    /* update the dominated flag in PseudoParetoStatus */
    if (PseudoParetoId[0]!=-1) {
        PseudoParetoStatus[0] |= PARETO;
    }
    for (bp = 1; bp<NbMS; bp++) {
        if (PseudoParetoId[bp]!=-1) {
            PseudoParetoStatus[bp] &= (PARETO - 1);
            int dominated = 0;
            for (bp2 = 0; bp2<bp; bp2++) {
                if (PseudoParetoId[bp2]!=-1 && 
                        PseudoParetoTrueLikelihood[bp] < PseudoParetoTrueLikelihood[bp2] - PARETOEPSILON) {
                    dominated = 1;
                    break;
                }
            }
            if (!dominated) {
                PseudoParetoStatus[bp] |= PARETO;
            }
        }
    }
    /* update the balanced flag in PseudoParetoStatus */
    double bestrhII = PseudoParetoTrueLikelihood[BestBP] - (double)log10l(NOrComput->getNO(BestBP)/2) + (double)log10l(expl((long double) -ratio) * powl((long double) ratio,(long double) BestBP) / dfact(BestBP));
    int BalancedBP = BestBP;
    for (bp = 0; bp<NbMS; bp++) {
        if (PseudoParetoId[bp] != -1) {
            double rh = PseudoParetoTrueLikelihood[bp];
            double rhII = rh - (double)log10l(NOrComput->getNO(bp)/2);
            rhII += (double)log10l(expl((long double) -ratio) * powl((long double) ratio,(long double) bp) / dfact(bp));
            if (FINITE(rhII) && rhII > bestrhII) {
                bestrhII = rhII;
                BalancedBP = bp;
            }
        }
    }
    if (BalancedBP >= 0) {
        PseudoParetoStatus[BalancedBP] |= BALANCED;
    }
    int lmapidpos = 0;
    if (lmapidout != NULL) {
        *lmapidout = new int[WorstBP+2];
        (*lmapidout)[WorstBP+1] = -1;
    }
    for (bp = WorstBP; bp>=0; bp--) {
        if (PseudoParetoId[bp] != -1) {
            double rh = PseudoParetoTrueLikelihood[bp];
            double nborders = - (double)log10l(NOrComput->getNO(bp)/2);
            double poissonprior = (double)log10l(expl((long double) -ratio) * powl((long double) ratio,(long double) bp) / dfact(bp));
            double rhII = rh + nborders + poissonprior;

            //        sprintf(bouf,"Id: %3d BP: %3d Log10-like: %9.3f Log10-like+bp: %9.3f", PseudoParetoId[bp], bp, rh,rhII);
            sprintf(bouf,"Id: %3d BP: %3d Log10-like: %9.3f", PseudoParetoId[bp], bp, rh);
#ifndef __WIN32__
            if (graphicview>0) fputs(bouf,tmppareto);
#endif
            print_out(bouf);
#ifndef __WIN32__
            if (graphicview>0) {
                sprintf(bouf," Log10-like+bp: %9.3f NumerOfOrders: %9.3f PoissonPrior: %9.3f", rhII, nborders, poissonprior);
                fputs(bouf,tmppareto);
            }
#endif
            if (lmapidout != NULL) (*lmapidout)[lmapidpos++] = PseudoParetoId[bp];
#ifndef __WIN32__
            if (graphicview<0) {
                int *order = Heap->MapFromId(PseudoParetoId[bp])->ordre;
                for (i = 0; i<NbMS; i++) {
                    print_out(" %s", GetMrkName(order[i]));
                }
            }
#endif
            if (PseudoParetoCoef != NULL) {
                sprintf(bouf," Coef: %7.3f", PseudoParetoCoef[bp]);
#ifndef __WIN32__
                if (graphicview>0) fputs(bouf,tmppareto);
#endif
                print_out(bouf);
            }
            if (PseudoParetoStatus != NULL) {
                sprintf(bouf,"          ");
                if (PseudoParetoStatus[bp] & RHFIRST) sprintf(bouf," %s", "(from RH)");
                if (PseudoParetoStatus[bp] & BPFIRST) sprintf(bouf," %s", "(from BP)");
                if (PseudoParetoStatus[bp] & PARETO2OPT) sprintf(bouf," %s", "(from Gr)");
#ifndef __WIN32__
                if (graphicview>0) fputs(bouf,tmppareto);
#endif
                print_out(bouf);
            }
            if (validation) {
                int *order = Heap->MapFromId(PseudoParetoId[bp])->ordre;
                /* compute number of breakpoints with canonical order 0,1,...,NbMS-1 */
                int trueBP = 0;
                for (i = 0; i<NbMS-1; i++) {
                    if (order[i] != order[i+1] + 1 && order[i] != order[i+1] - 1) {
                        trueBP++;
                    }
                }
                /* compute mean disorder with canonical order (Horvitz) */
                double disorder = 0;
                double disorderinv = 0;
                for (i = 0; i<NbMS; i++) {
                    disorder += fabs((double) (i+1) - order[i]); // warning! order[] in (1,NbMS)
                    disorderinv += fabs((double) NbMS - (i+1) - 1 - order[i]);
                }
                disorder /= NbMS * NbMS;
                disorderinv /= NbMS * NbMS;
                /* compute longuest increasing subsequence with canonical order */
                int liss = 0;
                int lissinv = 0;
                for (i = 0; i<NbMS; i++) {
                    dpliss[i] = 0;
                }
                for (i = 0; i<NbMS; i++) {
                    int best = 0;
                    for (j = 0; j<i; j++) {
                        if (order[i] > order[j] && dpliss[j] > best) {
                            best = dpliss[j];
                        }
                    }
                    dpliss[i] = best + 1;
                    if (best + 1 > liss) liss = best + 1;
                }
                for (i = 0; i<NbMS; i++) {
                    dpliss[i] = 0;
                }
                for (i = 0; i<NbMS; i++) {
                    int best = 0;
                    for (j = 0; j<i; j++) {
                        if (order[i] < order[j] && dpliss[j] > best) {
                            best = dpliss[j];
                        }
                    }
                    dpliss[i] = best + 1;
                    if (best + 1 > lissinv) lissinv = best + 1;
                }
                sprintf(bouf," BP: %3d LCS: %3d Disorder: %.5f", trueBP, (liss>lissinv)?liss:lissinv, (disorder<disorderinv)?disorder:disorderinv);
#ifndef __WIN32__
                if (graphicview>0) fputs(bouf,tmppareto);
#endif
                print_out(bouf);
            }
            if (PseudoParetoStatus != NULL && PseudoParetoStatus[bp] & SUPPORTEDPARETO) {
                sprintf(bouf," supported");
#ifndef __WIN32__
                if (graphicview>0) fputs(bouf,tmppareto);
#endif
                print_out(bouf);
            }
            if (PseudoParetoStatus != NULL && !(PseudoParetoStatus[bp] & PARETO)) {
                sprintf(bouf," dominated");
#ifndef __WIN32__
                if (graphicview>0) fputs(bouf,tmppareto);
#endif
                print_out(bouf);
            }
            if (PseudoParetoStatus != NULL && (PseudoParetoStatus[bp] & BALANCED)) {
                sprintf(bouf," ** balanced (%.2f) **", ratio);
#ifndef __WIN32__
                if (graphicview>0) fputs(bouf,tmppareto);
#endif
                print_out(bouf);
            }
            sprintf(bouf,"\n");
#ifndef __WIN32__
            if (graphicview>0) fputs(bouf,tmppareto);
#endif
            print_out(bouf);
        }
    }
    if (lmapidout != NULL) {
        (*lmapidout)[lmapidpos] = -1;
        for (i = 0; i<lmapidpos/2; i++) {
            int temp = (*lmapidout)[lmapidpos - i - 1];
            (*lmapidout)[lmapidpos - i - 1] = (*lmapidout)[i];
            (*lmapidout)[i] = temp;
        }
    }

#ifndef __WIN32__
    if (graphicview>0) {
        /* add dominated maps not in the pseudo Pareto frontier for graphical output */
        for (i = 0; i<Heap->HeapSize; i++) {
            int alreadyseen = 0;
            for (bp = WorstBP; bp>=0; bp--) {
                if (PseudoParetoId[bp] == i) {
                    alreadyseen = 1;
                    break;
                }
            }
            if (!alreadyseen) {
                sprintf(bouf,"Id: %3d BP: %3d Log10-like: %9.3f dominated\n", i, BreakPointsMap(ArbreJeu->Id, i), Heap->MapFromId(i)->coutEM);
                fputs(bouf,tmppareto);
            }
        }
        fclose(tmppareto);
        print_out("Press [return] to return to CarthaGene.\n");
        char execute[1024];
        sprintf(execute, "grep balanced tmppareto > tmpbalanced ; grep dominated tmppareto > tmpdominated; grep -v dominated tmppareto > tmponlypareto ; (echo '#set arrow 1 from %d,%f to %d,%f head lt 4 lw 1' ; cat %spareto.plot) > tmppareto.plot ; gnuplot tmppareto.plot", BestBP, BestRH, BalancedBP, PseudoParetoTrueLikelihood[BalancedBP], cgscriptdir); 
        if(system(execute)) {
            /* FIXME : handle error status instead of just making the compiler happy */
        }
    }
#endif

    delete[] PseudoParetoId;
    delete[] PseudoParetoTrueLikelihood;
    delete[] dpliss;
}
/* print information about the Pareto frontier found in the current CartaGene's heap */

char*** CartaGene::ParetoInfoG(double ratio)
{
    int i,bp,bp2,j;
    BJS_OR* bjo=NULL;

    double *PseudoParetoCoef  = NULL;
    int *PseudoParetoStatus = NULL;

    /* some consistency checks */
    if (Heap->HeapSize == 0) 
    {
        print_err(  "Error : Empty heap.\n");
        return NULL;
    }
    /*int ok = 0;*/
    /*for (i=1; i<=NbJeu; i++) {*/
    /*if (Jeu[i]->Cross == Ordre) {*/
    /*ok = 1;*/
    /*break;*/
    /*}*/
    /*}*/
    /*if (!ok) {*/
    if(!checkJeuOrdre(ArbreJeu, &bjo)) {
        print_err("Error: there is no reference order data set loaded! The current data set should be merged by order from one Biological Data Set and one reference order data set.\n");
        return NULL;
    }

    int *vc;

    /*int nc =  ((BJS_OR*)Jeu[i])->GetnLocipChrom(&vc);*/
    int nc = bjo->GetnLocipChrom(&vc);
    if (NOrComput != NULL) delete NOrComput;
    NOrComput = new NOrCompMulti(NbMS-1, vc, nc);
    NOrComput->run();

    int *PseudoParetoId = new int[NbMS];
    double *PseudoParetoTrueLikelihood = new double[NbMS];
    int WorstBP = 0;
    int BestBP = INT_MAX;
    double WorstRH = 0;
    double BestRH = -1e100;

    int *dpliss = new int[NbMS];

    for (bp=0; bp<NbMS; bp++) {
        PseudoParetoId[bp] = -1;
        PseudoParetoTrueLikelihood[bp] = 0;
    }
    SetBreakPointCoef(ArbreJeu->Id, 0.);
    for (i=0; i<Heap->HeapSize; i++) {
        /* recompute true log-like without breakpoint */
        Heap->MapFromId(i)->UnConverge();
        double rh = ComputeEM(Heap->MapFromId(i));
        int bp = BreakPointsMap(ArbreJeu->Id, i);
        if (PseudoParetoId[bp] == -1 || PseudoParetoTrueLikelihood[bp] < rh - PARETOEPSILON) {
            PseudoParetoId[bp] = i;
            PseudoParetoTrueLikelihood[bp] = rh;
            if (bp < BestBP) {
                BestBP = bp;
                WorstRH = rh;
            } else if (bp == BestBP) {
                if (rh > WorstRH) WorstRH = rh;
            }
            if (rh > BestRH + PARETOEPSILON) {
                BestRH = rh;
                WorstBP = bp;
            } else if (rh <= BestRH + PARETOEPSILON && rh >= BestRH - PARETOEPSILON) {
                if (bp < WorstBP) WorstBP = bp;
            }
        }
    }
    double RangeRatio = (BestRH-WorstRH)/(WorstBP-BestBP);

    int nbsolutions = 0;
    for (bp = BestBP; bp<=WorstBP; bp++) {
        if (PseudoParetoId[bp] != -1) nbsolutions++;
    }
    print_out("\nPareto frontier approximation (coverage: %d / %d = %.2f %%):\n", nbsolutions, WorstBP - BestBP + 1, (100. * nbsolutions) / (WorstBP - BestBP + 1));
    print_out("Ranges: Log10-likelihood [%.3f,%.3f] / BP [%d,%d] = %.4f\n", WorstRH, BestRH, BestBP, WorstBP, RangeRatio);

    /* check if the marker names are equal to 1,2,...,NbMS */
    int validation = 1;
    for (i = 0; i<NbMS; i++) {
        if (atoi(GetMrkName(MarkSelect[i]))!=i+1) {
            validation = 0;
            break;
        }
    }

    if (PseudoParetoStatus == NULL) {
        PseudoParetoStatus = new int[NbMS];
        for (bp = 0; bp<NbMS; bp++) {
            PseudoParetoStatus[bp] = 0;
        }
    }
    /* update the dominated flag in PseudoParetoStatus */
    if (PseudoParetoId[0]!=-1) {
        PseudoParetoStatus[0] |= PARETO;
    }
    for (bp = 1; bp<NbMS; bp++) {
        if (PseudoParetoId[bp]!=-1) {
            PseudoParetoStatus[bp] &= (PARETO - 1);
            int dominated = 0;
            for (bp2 = 0; bp2<bp; bp2++) {
                if (PseudoParetoId[bp2]!=-1 && 
                        PseudoParetoTrueLikelihood[bp] < PseudoParetoTrueLikelihood[bp2] - PARETOEPSILON) {
                    dominated = 1;
                    break;
                }
            }
            if (!dominated) {
                PseudoParetoStatus[bp] |= PARETO;
            }
        }
    }
    /* update the balanced flag in PseudoParetoStatus */
    double bestrhII = PseudoParetoTrueLikelihood[BestBP] - (double)log10l(NOrComput->getNO(BestBP)/2) + (double)log10l(expl((long double) -ratio) * powl((long double) ratio,(long double) BestBP) / dfact(BestBP));
    int BalancedBP = BestBP;
    for (bp = 0; bp<NbMS; bp++) {
        if (PseudoParetoId[bp] != -1) {
            double rh = PseudoParetoTrueLikelihood[bp];
            double rhII = rh - (double)log10l(NOrComput->getNO(bp)/2);
            rhII += (double)log10l(expl((long double) -ratio) * powl((long double) ratio,(long double) bp) / dfact(bp));
            if (FINITE(rhII) && rhII > bestrhII) {
                bestrhII = rhII;
                BalancedBP = bp;
            }
        }
    }
    if (BalancedBP >= 0) {
        PseudoParetoStatus[BalancedBP] |= BALANCED;
    }

    int lmapidpos = 0;

    char * temp;
    char ***  lpareto= new char**[WorstBP+2];
    lpareto[WorstBP+1] = NULL;

    for (bp = WorstBP; bp>=0; bp--) {
        if (PseudoParetoId[bp] != -1) {
            double rh = PseudoParetoTrueLikelihood[bp];
            double rhII = rh - (double)log10l(NOrComput->getNO(bp)/2);
            rhII += (double)log10l(expl((long double) -ratio) * powl((long double) ratio,(long double) bp) / dfact(bp));

            print_out("Id: %3d BP: %3d Log10-like: %9.3f Log10-like+bp: %9.3f", PseudoParetoId[bp], bp, rh,rhII);

            char ** line = new char*[5];
            temp = new char[8];
            sprintf(temp, "%d", PseudoParetoId[bp]);
            line[0] = temp;

            temp = new char[8];
            sprintf(temp, "%d", bp);
            line[1] = temp;

            temp = new char[16];
            sprintf(temp, "%.3f", rh);
            line[2] = temp;

            temp = new char[16];
            sprintf(temp, "%.3f", rhII);
            line[3] = temp;

            temp = new char[16];
            sprintf(temp, "%s", "frontier");
            line[4] = temp;

            lpareto[lmapidpos++] = line;

            if (PseudoParetoCoef != NULL) {
                print_out(" Coef: %7.3f", PseudoParetoCoef[bp]);
            }
            if (PseudoParetoStatus != NULL) {
                if (PseudoParetoStatus[bp] & RHFIRST) print_out(" (from RH)");
                if (PseudoParetoStatus[bp] & BPFIRST) print_out(" (from BP)");
                if (PseudoParetoStatus[bp] & PARETO2OPT) print_out(" (from Gr)");
            }
            if (validation) {
                int *order = Heap->MapFromId(PseudoParetoId[bp])->ordre;
                /* compute number of breakpoints with canonical order 0,1,...,NbMS-1 */
                int trueBP = 0;
                for (i = 0; i<NbMS-1; i++) {
                    if (order[i] != order[i+1] + 1 && order[i] != order[i+1] - 1) {
                        trueBP++;
                    }
                }
                /* compute mean disorder with canonical order (Horvitz) */
                double disorder = 0;
                double disorderinv = 0;
                for (i = 0; i<NbMS; i++) {
                    disorder += fabs((double) (i+1) - order[i]); // warning! order[] in (1,NbMS)
                    disorderinv += fabs((double) NbMS - (i+1) - 1 - order[i]);
                }
                disorder /= NbMS * NbMS;
                disorderinv /= NbMS * NbMS;
                /* compute longuest increasing subsequence with canonical order */
                int liss = 0;
                int lissinv = 0;
                for (i = 0; i<NbMS; i++) {
                    dpliss[i] = 0;
                }
                for (i = 0; i<NbMS; i++) {
                    int best = 0;
                    for (j = 0; j<i; j++) {
                        if (order[i] > order[j] && dpliss[j] > best) {
                            best = dpliss[j];
                        }
                    }
                    dpliss[i] = best + 1;
                    if (best + 1 > liss) liss = best + 1;
                }
                for (i = 0; i<NbMS; i++) {
                    dpliss[i] = 0;
                }
                for (i = 0; i<NbMS; i++) {
                    int best = 0;
                    for (j = 0; j<i; j++) {
                        if (order[i] < order[j] && dpliss[j] > best) {
                            best = dpliss[j];
                        }
                    }
                    dpliss[i] = best + 1;
                    if (best + 1 > lissinv) lissinv = best + 1;
                }
                print_out(" BP: %3d LCS: %3d Disorder: %.5f", trueBP, (liss>lissinv)?liss:lissinv, (disorder<disorderinv)?disorder:disorderinv);
            }
            if (PseudoParetoStatus != NULL && PseudoParetoStatus[bp] & SUPPORTEDPARETO) {
                print_out(" supported");
                sprintf(temp, "%s", "supported");

            }
            if (PseudoParetoStatus != NULL && !(PseudoParetoStatus[bp] & PARETO)) {
                print_out(" dominated");
                sprintf(temp, "%s", "dominated");
            }
            if (PseudoParetoStatus != NULL && (PseudoParetoStatus[bp] & BALANCED)) {
                print_out(" ** balanced (%.2f) **", ratio);
                sprintf(temp, "%s", "balanced");
            }
            print_out("\n");
        }
    }
    lpareto[lmapidpos] = NULL;

    delete[] PseudoParetoId;
    delete[] PseudoParetoTrueLikelihood;
    delete[] dpliss;

    return lpareto;
}

#ifdef LKHLICENCE
Problem * cg2lkh(BioJeu *ArbreJeu, int NbMS, int MarkSelect[], funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
{
    Problem *p = DefaultProblem();
    p->NAME = (char *) calloc(256, sizeof(char));
    sprintf(p->NAME, "%d_%d.cg", NbMS, MULTIPLY);
    p->TYPE = "TSP";
    p->DIMENSION = NbMS+1;
    p->EDGE_WEIGHT_TYPE = "EXPLICIT";
    // p->EDGE_WEIGHT_FORMAT = "UPPER_ROW";
    // p->C = (long *) calloc((NbMS+1) * NbMS / 2, sizeof(long));
    p->EDGE_WEIGHT_FORMAT = "FULL_MATRIX"; /* more simple to manage in iterlkh */
    p->C = (long *) calloc((NbMS+1) * (NbMS+1), sizeof(long));
    int index = 0;
    try {
        // for (int m1 = 0; m1 < NbMS; m1++) {
        //   for (int m2 = m1+1; m2 <= NbMS; m2++) {
        for (int m1 = 0; m1 <= NbMS; m1++) {
            for (int m2 = 0; m2 <= NbMS; m2++) {
                double res = 0;
                if (m1 == m2) {
                    res = 0;
                } else {
                    if (m1 == 0 || m2 == 0) {
                        if (m1 == 0) {
                            res = MULTIPLY * mycontribLogLike2pt1(ArbreJeu, MarkSelect[m2-1]);
                        } else {
                            res = MULTIPLY * mycontribLogLike2pt1(ArbreJeu, MarkSelect[m1-1]);
                        }	    
                    } else {
                        res = mycontribLogLike2pt2(ArbreJeu, MarkSelect[m1-1],MarkSelect[m2-1]);
                        if (res != HUGE_VAL) {
                            res *= MULTIPLY;
                        }
                    }
                }
                if (!(FINITE(res)) || res == HUGE_VAL || res > INT_MAX) { /* res > INFINI */
                    p->C[index++] = INFINI;
                } else {
                    p->C[index++] = (long)res;
                }
            }
        }
    } catch (BioJeu::NotImplemented) { FreeProblem(p); p = NULL; }
    return p;
    }

    CartaGene *CurrentCartage = NULL;
    double BestTSPMapCost = -1e100;
#ifdef __cplusplus
    extern "C" {
#endif
        int CartageQuietFlag = 0;
        int *CurrentStopFlag = NULL;
        int AlwaysComputeEM = 0;
#ifdef __cplusplus
    }
#endif
    Carte *LastTSPMap = NULL;

    void tsp2cg(int NbMS, long *Tour, int verbose)
    {
        int *newMarkSelect = new int[NbMS];
        newMarkSelect[0] = 0;
        int dummymrk = 0;
        for (int i = 1; i <= NbMS + 1; i++) {
            if (Tour[i] == 1) {
                dummymrk = i;
                break;
            }
        }
        if (dummymrk == 0) {
            print_err("error, dummymarker not found!!!");
            exit(2);
        }
        int index = 0;
        for (int j = dummymrk + 1; j <= NbMS + 1; j++) {
            newMarkSelect[index++] = CurrentCartage->MarkSelect[Tour[j] - 2];
        }
        for (int k = 1; k <= dummymrk - 1; k++) {
            newMarkSelect[index++] = CurrentCartage->MarkSelect[Tour[k] - 2];
        }

        Carte *TheMap = new Carte(CurrentCartage, NbMS, newMarkSelect);
        if (LastTSPMap != NULL) delete LastTSPMap;
        LastTSPMap = TheMap;

        double mycost = CurrentCartage->ComputeEM(TheMap);
        if (!CurrentCartage->QuietFlag && 
                mycost > ((CurrentCartage->Heap->HeapSize)?CurrentCartage->Heap->Best()->coutEM:-1e100) + 0.001) {
            print_out("[%.2f]\n",mycost);
        }
        CurrentCartage->Heap->Insert(TheMap,0);
        if (mycost > BestTSPMapCost) BestTSPMapCost = mycost;

        if (verbose && !CurrentCartage->QuietFlag) {
            if (CurrentCartage->VerboseFlag>1) CurrentCartage->PrintDMap(TheMap, 0, TheMap);
            else CurrentCartage->PrintMap(TheMap);  
        }
        delete[] newMarkSelect;
    }

#ifdef __cplusplus
    extern "C" {
#endif
        void tsp2cg(int NbMS, long *Tour) {tsp2cg(NbMS, Tour, 0);}
#ifdef __cplusplus
    }
#endif

    void CartaGene::lkh(int nbrun, int backtrack, funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
    {

        // vérification de base

        if (NbMS == 0) 
        {
            print_err(  "Error : Empty selection of loci.\n");
            return;
        }

        /* initialisation des structures utiles aux fonctions ContribLogLike2pt */
        ArbreJeu->InitContribLogLike2pt(); 
        CurrentCartage = this;
        BestTSPMapCost = -1e100;
        AlwaysComputeEM = ((backtrack >= 0)?1:0);
        CurrentStopFlag = &StopFlag;
        CartageQuietFlag = QuietFlag;
        if (LastTSPMap != NULL) {
            delete LastTSPMap;
            LastTSPMap = NULL;
        }

        Chronometre cpu = Chronometre();
        cpu.Init();

        Problem *p = cg2lkh(ArbreJeu, NbMS, MarkSelect, mycontribLogLike2pt1, mycontribLogLike2pt2);
        if (p != NULL) {
            p->TRACE_LEVEL = -1;
            p->RUNS = nbrun;
            p->BACKTRACK_MOVE_TYPE = ((backtrack >= 0)?backtrack:0);
            // p->PRECISION = 1;

            // reuse best map as initial tour if it exists
            if (Heap->HeapSize >= 1) {
                Carte *InitMap = Heap->Best();
                int inverseMarkSelect[MAXNBMARKERS];
                for (int i = 0; i < NbMS; i++) {
                    inverseMarkSelect[MarkSelect[i]] = i + 2;
                }
                p->INITIAL_TOUR = (long *) calloc(NbMS + 1, sizeof(long));
                p->INITIAL_TOUR[0] = 1; /* dummy marker */
                for (int k=1; k <= NbMS; k++) {
                    p->INITIAL_TOUR[k] = inverseMarkSelect[InitMap->ordre[k - 1]];
                }
            }

            LKH(p);

            if (!QuietFlag) {
                print_out("Best map with log10-likelihood = %.2f\n", BestTSPMapCost);
                if (!StopFlag) {
                    print_out("TSP: optimum= %f lowerbound= %f gap= %f%% totaltime= %.2f\n", p->BestCost / MULTIPLY, p->LowerBound / MULTIPLY, (p->BestCost - p->LowerBound) / p->LowerBound * 100., cpu.Read());
                }
            }

            if (!StopFlag) tsp2cg(NbMS, p->BestTour, 1);

            FreeProblem(p);
        }

        if (LastTSPMap != NULL) {
            delete LastTSPMap;
            LastTSPMap = NULL;
        }
    }

    void CartaGene::lkhiter(int nbrun, int collectmaps, double threshold, int cost, funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
    {
        int i,j,k,l;

        // vérification de base

        if (NbMS == 0) 
        {
            print_err(  "Error: Empty selection of loci.\n");
            return;
        }
        if (ArbreJeu->Cross != RH && ArbreJeu->Cross != BC) {
            print_err("Error: Not implemented for this data type.\n");
            return;
        }

        /* initialisation des structures utiles aux fonctions ContribLogLike2pt */
        ArbreJeu->InitContribLogLike2pt(); 
        CurrentCartage = this;
        BestTSPMapCost = -1e100;
        AlwaysComputeEM = collectmaps;
        CurrentStopFlag = &StopFlag;
        CartageQuietFlag = QuietFlag;
        if (LastTSPMap != NULL) {
            delete LastTSPMap;
            LastTSPMap = NULL;
        }

        Chronometre cpu = Chronometre();
        cpu.Init();

        Problem *p = cg2lkh(ArbreJeu, NbMS, MarkSelect, mycontribLogLike2pt1, mycontribLogLike2pt2);
        if (p != NULL) {
            p->TRACE_LEVEL = -1;
            p->RUNS = 1;
            p->BACKTRACK_MOVE_TYPE = 0;
            // p->PRECISION = 1;

            int inverseMarkSelect[MAXNBMARKERS];
            for (i = 0; i < NbMS; i++) {
                inverseMarkSelect[MarkSelect[i]] = i + 1;
            }

            double retentionRH = ((ArbreJeu->Cross==RH)?((BJS_RH *) ArbreJeu)->Retention2pt():0.);

            double *theta2pt = new double[NbMS];

            for (l = 1; l <= nbrun; l++) {
                print_out("Run %d:\n", l);
                LKH(p);

                if (!QuietFlag) {
                    print_out("Best map with log10-likelihood = %.2f\n", BestTSPMapCost);
                    if (!StopFlag) {
                        print_out("TSP: optimum= %f lowerbound= %f gap= %f%% totaltime= %.2f\n", p->BestCost / MULTIPLY, p->LowerBound / MULTIPLY, (p->BestCost - p->LowerBound) / p->LowerBound * 100., cpu.Read());
                    }
                }

                if (StopFlag) break;

                tsp2cg(NbMS, p->BestTour, 1);

                int nopenalty = 1;
                double maxdiff = 0.;
                for (j = 0; j < NbMS-1; j++) {
                    theta2pt[j] = ((ArbreJeu->Cross==RH)?((BJS_RH *) ArbreJeu)->Theta2pt(LastTSPMap->ordre[j],LastTSPMap->ordre[j+1],retentionRH):GetTwoPointsFR(LastTSPMap->ordre[j],LastTSPMap->ordre[j+1]));
                    if (LastTSPMap->tr[j] - theta2pt[j] > maxdiff) {
                        maxdiff = LastTSPMap->tr[j] - theta2pt[j];
                        nopenalty = 0;
                    }
                }
                if (nopenalty) break;
                for (j = 0; j < NbMS-1; j++) {
                    if (maxdiff - (LastTSPMap->tr[j] - theta2pt[j]) <= threshold) {
                        print_out("%d %d %ld increased by %d\n", LastTSPMap->ordre[j], LastTSPMap->ordre[j+1], p->C[inverseMarkSelect[LastTSPMap->ordre[j]] * (NbMS+1) + inverseMarkSelect[LastTSPMap->ordre[j+1]]], cost);
                        p->C[inverseMarkSelect[LastTSPMap->ordre[j]] * (NbMS+1) + inverseMarkSelect[LastTSPMap->ordre[j+1]]] += cost;
                        p->C[inverseMarkSelect[LastTSPMap->ordre[j+1]] * (NbMS+1) + inverseMarkSelect[LastTSPMap->ordre[j]]] += cost;
                    }
                }

                // reuse best tour as next initial tour
                if (p->INITIAL_TOUR) {
                    free(p->INITIAL_TOUR);
                    p->INITIAL_TOUR = NULL;
                }
                p->INITIAL_TOUR = (long *) calloc(NbMS + 1, sizeof(long));
                for (k=0; k <= NbMS; k++) {
                    p->INITIAL_TOUR[k] = p->BestTour[k + 1];
                }
                free(p->BestTour);
                p->BestTour = NULL;
            }
            FreeProblem(p);

            delete[] theta2pt;
        }

        if (LastTSPMap != NULL) {
            delete LastTSPMap;
            LastTSPMap = NULL;
        }
    }
#else
    void CartaGene::lkh(int nbrun, int backtrack, funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
    {
        print_out("This version of CarthaGene has been compiled without Keld Helsgaun's LKH TSP software. If you want to use this command you must check that you are entitled to use this code (only for research use, see the README file in the LKH directory) and use the research version of CarthaGène(compiled with the LKHLICENCE macro defined).\n");
        StopFlag = 0;
        if(mycontribLogLike2pt1==contribZero && mycontribLogLike2pt2==contribHaldane) {
            CartaGene::BuildNiceMap();
        } else {
            CartaGene::BuildNiceMapL();
        }
        flush_out();
    }

    void CartaGene::lkhiter(int nbrun, int collectmaps, double threshold, int cost, funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
    {
        print_out("This version of CarthaGene has been compiled without Keld Helsgaun's LKH TSP software. If you want to use this command you must check that you are entitled to use this code (only for research use, see the README file in the LKH directory) and use the research version of CarthaGène(compiled with the LKHLICENCE macro defined).\n");
        StopFlag = 0;
        if(mycontribLogLike2pt1==contribZero && mycontribLogLike2pt2==contribHaldane) {
            CartaGene::BuildNiceMap();
        } else {
            CartaGene::BuildNiceMapL();
        }
        flush_out();
    }
#endif
