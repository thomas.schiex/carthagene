//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// $Id: CartaGene.h,v 1.65.2.4 2012-05-31 08:06:58 dleroux Exp $
//
// Description : Prototype de l''API du module.
// Divers :
//-----------------------------------------------------------------------------

#ifndef _CARTAGENE_H
#define _CARTAGENE_H

#include "../config.h"

#include <ctype.h>
#include "CGtypes.h"
#include "read_data.h"
#include "Carte.h"
#include "Tas.h"
#include "NOrCompMulti.h"

#include "BioJeu.h"

#include <iostream>

#ifdef ihm
#include <tcl.h>
#define fflush(stdout) do { /*std::cout << "fflush" << std::endl;*/ fflush(stdout); /*std::cout << "do Tcl events" << std::endl;*/ while (Tcl_DoOneEvent(TCL_DONT_WAIT)); /*std::cout << "done" << std::endl;*/ } while(0)
#endif

#define MAXNBJEU 32
#ifdef CG_BIGMEM
#define MAXNBMARKERS 10000
#else
#define MAXNBMARKERS 100000
#endif

/*class BioJeu;*/
class BJS_OR;

/*#include "Marker.h"*/

/** Interface du noyau de calcul */
class CartaGene {
 public:
  MarkerList markers;

  /** Version du produit */
  char LaVersion[128];

  /** Nombre total de marqueurs */
  int NbMarqueur;

  /** Nombre total de jeux */
  int NbJeu;

  /** Nombre maximum de jeux chargeables simultanement */
  /* Cette limitation est imposee par le type utilise pour BitJeuMarq */
  /*static const int MAXNBJEU = 32;*/

  /** Nombre total d'individus. */
  int NbIndividu;

  /** Nom de chaque marqueur. */
  //charPtr *NomMarq;

  /** Le marqueur est la fusion de qui */
  //intPtr Represents;

  /** Le marqueur a-t-il ete fusionne et dans quel marqueur ? */
  //intPtr Merged;

  /** Champs de bits de chaque marqueur "i". */
  /* BitJeuMarq[i] a le jeme bit positionne a 1 ssi le marqueur i est
     defini dans le jeu de donnees "j" */
  //int *BitJeuMarq;

  /** Tableau des jeux. */
  BioJeu **Jeu;

  /** Racine de l'arbre des jeux. */
  BioJeu *ArbreJeu;

  /** La liste des groupes(derni�re d�termination). */
  NodptrPtr Group;

  /** Le seuil de distance utilis� pour le calcul des groupes. */
  double DisThres;

  /** Le seuil de LOD utilis� pour le calcul des groupes. */
  double LODThres;

  /** Le seuil de fiabilit� des cartes frameworks.
      utilis� pour la v�rification de cartes frameworks.
      arr�te la recherche lorsque L(SecondBestMap) > L(BestMap) + Robustness */
  double Robustness;

  /** Drapeau de demande d'arr�t d'une commande*/
  int StopFlag;

  /** Drapeau du mode verbeux des r�sultats en cours de traitement. */
  int VerboseFlag;

  /** Drapeau du mode silencieux sur la progression du traitement. */
  int QuietFlag;

  /** Precision of map distances output in detailed printing */
  int DistPrecision;

  /** Structure de stockage des cartes */
  //DL passage à un pointeur pour mieux hacker le contexte dans cgpy
  Tas* Heap;

  /** le vecteur des marqueurs actifs pour la recherche */
  int *MarkSelect;

  /** le nombre des marqueurs actifs */
  int NbMS;

  /** Drapeau du mode calcul vraisemblance 2points */
  int EM2pt;

  /** Nombre maximum de cartes memorisees par paretolkh et paretogreedy */
  int MAXHEAPSIZETSP;

  //DL
  /** Dernier jeu charge */
  BioJeu* dernierJeuCharge;

  /** Constructeur. */
  CartaGene();

  /** Destructeur. */
  ~CartaGene();

  /** Demande de trace.
      @param fileName le nom du fichier
    */
  void Tracer(char *fileName);

  /** Affiche la version*/
  void Version(void);

  /** Efface toutes les informations*/
  void Clear(void);

  /** Entre la clef de license */
  void License(const char *key);

  /** Chargement d'un jeu de donn�es.
      @param fileName le nom du fichier
      @return un message d'information sur le jeu de donn�es charg�
    */
  char * CharJeu_OLD(char *fileName);
  BioJeu* CharJeu(char* fileName);
  BioJeu* NouveauJeu(const char* filename, read_data::raw_data& rd);

  // DL
  // Extériorisation de la fin de CharJeu pour pouvoir l'invoquer aussi après un clonage
  void PostTraitementBioJeu(BioJeu* bjp, BioJeu* clonee=NULL);

  // DL
  /** Clone un jeu de données
   */
  BioJeu* ClonaJeu(int id, CrossType coerceInto=Unk, bool addToList=true) throw(BioJeu::NotImplemented);

  /** Chargement d'un jeu de donn�es de type ordre
      @param filename le nom du fichier
      @return un biojeu de type ordre (BJS_OR)
      */
  BJS_OR * CharJeuOrdre(char *filename);

  /** Affichage de tous les marqueurs. */
  void AffMarq(void);

  /** Affichage de tous les jeux. */
  void AffJeu(void);

  /** retourne de l'info sur tous les jeux. */
  char** GetArbre(void);

  /** Affichage de l'�chantillon d'un jeu.
      @param nbjeu le num�ro du jeu
  */
  void DumpEch(int nbjeu);

  /** Affichage des LOD 2 points d'un jeu.
      @param nbjeu le num�ro du jeu
  */
  void DumpTwoPointsLOD(int nbjeu);

  /** Affichage des LOD 2 points des marqueurs de la s�lection
  */
  void PrintTwoPointsLOD(void);

  /** Affichage des marqueurs ayant un haplotype compatible
  */
  void DumpDouble(void);

  /** Affichage des FR 2 points d'un jeu.
  */
  void PrintTwoPointsFR(void);

  /** Affichage des FR 2 points d'un jeu.
  */
  void DumpTwoPointsFR(void);

  /** Acc�s � une valeur LOD score 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return le LOD
  */
  double GetTwoPointsLOD(int m1, int m2) const;

  /** Acc�s � la fraction de recombinaison 2pt.sdist
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return la fraction de recombinant
  */
  double GetTwoPointsFR(int m1, int m2) const;

  /** Acc�s � la distance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return la fraction de recombinant
  */
  double GetTwoPointsDH(int m1, int m2) const;

  /** Affichage des LOD 2 points des marqueurs de la s�lection
        @param unit 1 c. pou sp�cifier l'unit�
  */
  void PrintTwoPointsDist(char unit[2]);

  /** Calcul des groupes d'un jeu.
      @param disthres le seuil de distance
      @param lodthres le seuil de lod
      @return le nombre de groupes trouv�
  */
  int Groupe(double disthres, double lodthres);

  /** R�cup�ration d'un groupe sous la forme d'un vecteur d'indice.
      @param idg le num�ro du groupe
      @param le vecteur de num�ro de marqueurs
      @return le nombre de marqueur dans le groupe
  */
  int GetGroupeI(int idg, intPtr *vm) const;

  /** Fusion g�n�tique entre deux jeux.
      @param gauche le num�ro du premier jeu
      @param droite le num�ro du second jeu
      @return un message d'information sur le jeu de donn�es fusionn�
  */
  char *MerGen(int gauche, int droite);

  /** Fusion sur l'ordre entre deux jeux.
      @param gauche le num�ro du premier jeu
      @param droite le num�ro du second jeu
      @return un message d'information sur le jeu de donn�es fusionn�
  */
  char *MergOr(int gauche, int droite);

  /** Calcul de la vraisemblance maximum par estimation 2point.
      @param map la carte
      @return le maximum de vraisemblance
  */
  double Compute2pt(Carte *map);
  void SetEM2pt(int mode);

  /** Calcul de la vraisemblance maximum par EM.
      @param map la carte
      @return le maximum de vraisemblance
  */
  double ComputeEM(Carte *map);

  /** Calcul de la vraisemblance maximum par EM.
      Une interface plus sophistiqu�e du calcul de la vraisemblance par EM:
      On converge jusqu'� epsilon1. Si la vraisemblance est inf�rieure �
      threshold on laisse tomber, sinon on converge � epsilon2.
      @param data la carte
      @param threshold niveau a partir duquel on convergera a epsilon2
      @return le maximum de vraissemblance
  */
  double ComputeEMS (Carte *data,
		     double threshold);

  /** Construction d'une carte, calcul et insertion dans le tas
    */
  void SinglEM();

  /** retourne une carte sous forme de liste
      @param unit l'unit� de distance: k = kosambi ou centiray, h = haldane ou centiray.
      @param nbmap le num�ro de la carte dans le tas
      @return une liste contenant le nombre d'ordres, les nombres de marqueurs par ordre, les LOD par odre, chaque marqueur et sa position.
  */
  char *** GetMap(const char unit[2],int nbmap) const;

  /** retourne les cartes sous les formes d'une liste.
      @param nbmap le num�ro d'une carte dans le tas
      @return une liste contenant les indices de locus
  */
  int GetOrdMap(int nbmap, intPtr *vm) const;

  /** retourne les cartes sous les formes d'une liste.
      @param unit l'unit� de distance: k = kosambi ou centiray, h = haldane ou centiray.
      @param nbmap les nbmap meilleurs cartes.
      @return une liste contenant le nombre d'ordres, les nombres de marqueurs par ordre, les LOD par odre, chaque marqueur et sa position. Pour chaque carte
  */
  char **** GetHeap(char unit[2],int nbmap) const;

  /** Affichage simple d'une carte
      @param data la carte
  */
  void PrintMap(Carte *data) const;

  /** Affichage d�taill� d'une carte
      @param data la carte
      @param envers � l'envers ou � l'endroit
      @param dataref la carte de r�f�rence
  */
  void PrintDMap(Carte *data, int envers, Carte *dataref) const;

  /** Affichage simple d'une carte
      @param nbmap le num�ro de la carte dans le tas
  */
  void PrintMap(int nbmap) const;

  /** Affichage d�taill� d'une carte
      @param nbmap le num�ro de la carte dans le tas
      @param envers � l'envers ou � l'endroit
  */
  void PrintDMap(int nbmap, int envers) const;

  /** Affichage des deltas entre carte FW et carte FW + marqueur
      @param structure de resultat
      @param Carte Framework
      @param le nombre de marqueur de la carte FW
      @param le nmbre de marqueur a tester/carte FW
      @param le seuil pour afficher le delta
      @param le meilleur intervalle pour inserer le marqueur
  */
  void PrintVMap(struct testmartin *pstruct, Carte *data, double *distd, int nbm, int nbtas, double AM);

  /** Fixation des seuils de convergence EM
      @param Fin le seuild de convergence fine
      @param Gros le seuild de convergence grossiere
  */
  void SetTolerance(double Fin, double Gros);

  /** Fixation du seuil de fiabilit� des cartes frameworks.
      @param �cart de vraisemblance relatif � la meilleure carte
  */
  void SetRobustness(double robust);

  /** Affectation de la verbosit�
      @param flag le niveau
  */
  void SetVerbose(int flag);

  /** Affectation du silencieux
      @param flag le niveau
  */
  void SetQuiet(int flag);

  /** Pour une nouvelle carte pas trop b�te.*/
  void BuildNiceMap(void);
  void BuildNiceMapMultiFragment(void);

  /** Pour une nouvelle carte pas trop b�te.*/
  void BuildNiceMapL(void);
  void BuildNiceMapLMultiFragment(void);

  /** Build

      @param NC le nombre de cartes construites simultan�ments
  */
  void Build(int NC);

  /** Build

      @param SM Delta minimum n�cessaire � l'acceptation de l'insertion d'un marqueur
      @param  AM Seuil � l'int�rieur duquel les ordres sont conserv�s.
      @param vm vecteur d'indice de marqueurs
      @param nombre de marqueurs dans la liste.
      @param Demande test marqueurs non FW.
  */
  void BuildFW(double SM, double AM, int *vmi, int nbmi, int DM);

  /** Recuit Simul�
      La recherche de la meilleure carte est faite avec l'algorithme de Recuit.
      Durant la recherche, les cartes trouv�es sont stock�es dans le tas et
      rang�es dans l'ordre de cout d�croissant.
      @param Tries Nombre d'essai par temp�rature.
      @param Tinit Temp�rature de d�but.
      @param Tfinal Temp�rature finale.
      @param Cooling Vitesse de refroidissement.
  */
  void Annealing(int Tries,
		 double Tinit,
		 double Tfinal,
		 double Cooling);

  /** Lancement de Greedy.
      @param NR Nombre de boucles principales.
      @param NI Nombre d'it�rations.
      @param TMin TabooMin.
      @param TMax TabooMax.
  */
  void Greedy(int NR,
	      int NI,
	      int TMin,
	      int TMax,
	      int Ratio);
  void GreedyOld(int NR,
		 int NI,
		 int TMin,
		 int TMax);
  void GreedyNew(int NR,
		 int NI,
		 int TMin,
		 int TMax,
		 int Ratio);

  /** Algorithme genetique.
      @param nb_gen :Nombre de generations (integer)
      @param nb_elements :Nombre d'elements dans la population (integer)
      @param selection_number:# Selection type :(integer)
      #	0 -> roulette wheel
      #	1 -> stochastic remainder without replacement
      @param pcross: Probabilities of crossover and mutation (float)
      @param pmut: Probabilities of crossover and mutation (float)
      @param evolutive_fitness:Evolutive Fitness (integer)
      (the fitness of the same element changes with the generation number)

  */
  void AlgoGen( int nb_gens,
		int nb_elements,
		int selection_number,
		float pcross,
		float pmut,
		int evolutive_fitness);

  /** Convert CarthaGene data to TSP */
  void cg2tsp(char *tspName);
  /** Provide an approximate map distribution */
  void mcmc(int seed, int nbiter, int burning, double (*)(BioJeu *ArbreJeu, int m1), double (*)(BioJeu *ArbreJeu, int m1, int m2), int slow_computations=0, int lim_group_size=10, int mcmcverbose=0);
  /** solve current marker selection by using LKH */
  void lkh(int nbrun, int backtrack, double (*)(BioJeu *ArbreJeu, int m1), double (*)(BioJeu *ArbreJeu, int m1, int m2));
  void lkhiter(int nbrun, int collectmaps, double threshold, int cost, double (*)(BioJeu *ArbreJeu, int m1), double (*)(BioJeu *ArbreJeu, int m1, int m2));
  int OCB(int nbmap) const;
  /** find Pareto frontier by using LKH in comparative mapping */
  void ParetoLKH(int nbsteps, int nbrun, int backtrack, double (*)(BioJeu *ArbreJeu, int m1), double (*)(BioJeu *ArbreJeu, int m1, int m2));
  /** print the current Pareto frontier from the heap */
  void ParetoInfo(int graphicview, char *cgscriptdir, double *PseudoParetoCoef, int *PseudoParetoStatus, double ratio, intPtr *lmapidout);
  char *** ParetoInfoG(double ratio);
  /** find Pareto frontier by using Pareto 2-opt in comparative mapping */
  void ParetoGreedy(int ratio);
  void ParetoGreedy(int ratio, int minbp, int maxbp);
	
  /** Vérification de la validité de la séelction des marqueurs 
   * id : le numéro du jeu
   * retourne un booléen
   */
  bool ValidMarkerSelection( int id );
  
  /** Estimation du taux d'erreur des données RH
   *  works only for haploid RH data type with errors (BJS_RH)
   *  id : le numéro du jeu 
   */		
  void ErrorEstimation(int id);
  
  /** Imputation des typages erronées
   *   works only for RH data type with errors (BJS_RHE)
   *   filename : the name of the file where the corrected genotypes will be exported
   *   ConversionCutoff : a lower cutoff value for the posterior probability of a genotype above which they will be recalled as unknown
   *   CorrectionCutoff : an upper cutoff value for the posterior probability of a genotype above which they will be corrected (opposite genotype)
   */
  char* Imputation(int id, double ConversionCutoff, double CorrectionCutoff, double UnknownCorrectionCutoff);

  /** Post traitement polish
      Pour chaque marqueur, compare la probabilit� courante avec la
      probabilit� obtenue en d�pla�ant le marqueur vers un autre
      intervalle. Met � jour avec le meilleur �l�ment (s'il y en a un)
      et l'insert dans le tas. Affiche la matrice des diff�rences (en mode verbose).
  */
  void Polish();

  /** Post traitement polishtest
      Pour chaque marqueur, compare la probabilit� courante avec la
      probabilit� obtenue en d�pla�ant le marqueur vers un autre
      intervalle. NE Met PAS � jour avec le meilleur �l�ment (s'il y en a un)
      et NE l'insere PAS dans le tas.
      Affiche la matrice des diff�rences (en mode verbose).
      @param le vecteur des num�ros de marqueurs.
      @param le nombre de marqueurs
  */
  void Polishtest(int *vm, int nbm);

  /** Post traitement flips
      @param n taille de lafen�tre
      @param thres le seuil
      @param iter flag pour it�ratif ou non
  */
  void Flips(int n, double thres, int iter);


  /** Redimensionnement du tas
      L'ancien est recopi� dans le nouveau.
      @param size la taille du nouveau tas.
  */
  void ResizeHeap(int size);


  /** Dimensionnement du futur tas utilise par paretolkh et paretogreedy
      @param size la taille du nouveau tas.
  */
  void SetParetoHeapSize(int size);

  /** Fusion de deux marqueurs
      @param les num�ros de marqueurs.
  */

  int Merge(int m1, int m2);

  /** return a list of list of merged loci by id,
      The first locus of the sublist represent the others that are following.
  */
  char ** GetMerged();
  char ** GetDouble(double lod_threshold);

  /** Changement de la liste des marqueurs exploit�s
      @param le vecteur des num�ros de marqueurs.
      @param le nombre de marqueurs
  */
  void ChangeSel(int *vm, int nbm);


  /** R�cup�ration de la s�lection sous la forme d'un vecteur d'indice.
      @param le vecteur de num�ro de marqueurs
      @return le nombre de marqueur dans le groupe
  */
  int GetSel(intPtr *vm) const;

  /** R�cup�ration d'int�gralit� des loci sous la forme d'un vecteur d'indice.
      @param le vecteur de num�ro de marqueurs
      @return le nombre de marqueur dans le groupe
  */
  int GetAll(intPtr *vm) const;

  /** Retourne le nom d'un marqueur
      @param mid le num�ro d'un marqueur
      @return le nom du marqueur
  */
  char * GetMrkName(int mid);

  /** Retourne le num�ro d'un marqueur
      @param mname le nom d'un marqueur
      @return le num�ro du marqueur
  */
  int GetMrkId(char *mname);

  /** Retourne le num�ro du dernier marqueur
      @return le num�ro du marqueur
  */
  int LastMrk();

  /** Retourne un tableau des distance cumulees
  */
  void CalculDist(double *distd, Carte *data, int NbMS, int NbMF);

  /** Calcule le nombre de break-points entre l'ordre canonique et l'ordre obtenu
   */
  int BreakPoints(Carte *final);

  /** Calcule le nb de BP entre un jeu d'ordre et un autre jeu quelconque
   */
  int BreakPoints(int jeuOrdre, int jeu2);

  /** Calcule le nb de BP entre une carte et un jeu d'ordre
      si jeuOrdre est un mergor alors applique recursivement sur les fils
   */
  int BreakPointsMap(int jeuOrdre, int idMap);

  /* Same as above but with a Carte pointer as argument */
  int BreakPointsMapPointer(int jeuOrdre, Carte *map);

  /** Calcule la contribution du jeu d'ordre dans le calcul de EM pour la carte nbmap
   */
  double ComputeEMOrder(int jeuOrdre, int nbmap);

  /** Change le coefficient de ponderation sur les breakpoints
      si jeuOrdre est un mergor alors applique recursivement sur les fils
   */
  double SetBreakPointCoef(int jeuOrdre, double newcoef);

  /** Change the lambda parameter in a breakpoint dataset jeuOrdre
      lambda is used by ComputeEM and ComputeEMS if the breakpoint coefficient (Poids) is negative
      if jeuOrdre is a mergor then recursively applies this method on each child's dataset
   */
  double SetBreakPointLambda(int jeuOrdre, int CoefAutoFit, double newlambda);

  /* get the lambda prior value */
  double GetLambda(int jeuOrdre);
  int GetPositionInOrder(int jeuOrdre, int marq);
  int GetChromosome(int jeuOrdre, int marq);

  /** Number of orders at a given bp dist.
   */
  NOrCompMulti *NOrComput;


 protected:

  /**un ordre intermédiaire pour le 3changement
    */
  int *wordre;

 private:

  /** Teste le type de donn�es d'un fichier.
      @param DataFile le nom du fichier
      @return un type de croisement
    */
  CrossType ProbeCrossType(char DataFile[256], char *design_string);

  /** Fusionne les listes des marqueurs que representent deux marqueurs
      @param le marqueur 1
      @param le marqueur 2
      @return rien
    */
  void MergeRepresents(int m1, int m2);

};

typedef CartaGene *CartaGenePtr;

#endif /* _CARTAGENE_H */
