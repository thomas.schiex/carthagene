//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_RHD.h,v 1.10.2.1 2011-11-24 14:56:19 dleroux Exp $
//
// Description : Prototype de la classe BJS_RHD.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJS_RHD_H
#define _BJS_RHD_H

#include "BioJeuSingle.h"
#include "parallel_compute_expected.h"

#define EM_MAX_THETA_HR 0.9995
#define EM_MIN_THETA_HR 0.0001
#define EM_MAX_RETAIN   0.9999
#define EM_MIN_RETAIN   0.0001

class BJS_RH;
class BJS_RHE;

/** Classe des jeux de données hybrides irradiés */
class BJS_RHD : public BioJeuSingle {
 public:
    /* Defs for parallel EM computation */
    struct EM_state {
        std::vector<double> SourceTo[4];
        std::vector<double> ToSink[4];
        /* les proba : 2,1,0 cassures & 2,1 rétention */
        double ProbAr2[4][4];
        double ProbAr[4][4];
        double ProbArNB[4][4];
        double ProbArRet2[4][4];
        double ProbArRet[4][4];
        EM_state(int nMarq) {
            for(int i=0;i<4;++i) {
                ToSink[i].resize(nMarq);
                SourceTo[i].resize(nMarq);
            }
        }
    };
    typedef parallel_compute_expected<BJS_RHD, false> PCEType;

  /** Constructeur. */
  BJS_RHD();

  /** Constructeur.
      @param cartag accés à l'ensemble des informations du système. 
      @param id le numéro du jeu.
      @param cross type de jeu de donnée.
      @param nm nombre de marqueurs.
      @param taille de l'échantillon
      @param bitjeu champ de bit du jeu de données.
      @param indmarq le vecteur d'indice de marqueur
      @param echantil la matrice des informations
    */
  BJS_RHD(CartaGenePtr cartag,
	 int id,
	 charPtr nomjeu,
	 CrossType cross,
	 int nm, 
	 int taille,
	 int bitjeu,
	 int *indmarq,
	 Obs **echantil);
  
  /** Destructeur. */
  ~BJS_RHD();

  /** Constructeur par recopie */
  BJS_RHD(const BJS_RHD& b);
  /** Constructeur par recopie (variante transtypante) */
  BJS_RHD(const BJS_RH& b);
  /** Constructeur par recopie (variante transtypante) */
  BJS_RHD(const BJS_RHE& b);


  /** Compatibilité entre deux echantillons d'un marqueur
      @param numarq1 le numero du marqueur 1
      @param numarq2 le numero du marqueur 2
  */
  int Compatible(int numarq1, int numarq2) const;

  /** Fusion de deux marqueurs pour la creation d'un haplotype. Le
      marqueur 1 est utilise pour stocker le resultat.
      @param numarq1 marqueur 1 
      @param numarq2 marqueur 2 
      @return nothing.  
  */
  void Merge(int marq1,int marq2) const;

  /** Etape d'Expectation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le loglike
  */
  double ComputeExpected(const Carte *data, double *expected);

  /** Calcul des probabilités d'existence d'un chemin passant par un sommet depuis chacune des extrémités.
      @param Ind
      @param map la carte
  */
  inline double ComputeSourceTo(std::vector<double>*SourceTo, int Ind, const Carte *map) const;

  /** Calcul des probabilites d'existence d'un chemin passant par un sommet depuis l'extremite droite (puit)
      @param Ind
      @param map la carte
  */
  inline double ComputeToSink(std::vector<double>*ToSink, int Ind, const Carte *map) const;
  
  /** Etape elementaire d' "Expectation" de EM.
      @param Ind
      @param map la carte
      @param expected le vecteur d'expectation
  */
  //void ComputeOneExpected(int Ind, 
	//	     const Carte* map, 
	//	     double *expected);
  void ComputeOneExpected(EM_state& state, int Ind, PCEType* pce);

 private:
  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  void Prepare2pt(int m1, int m2,  int *ss) const;

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const;

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  double LogLike2pt(double *par, int *ss) const;

  /** passage � priv� d'une virtuelle h�rit�e. 
  */
  double ComputeOneTwoPoints(int m1, 
			     int m2, 
			     double epsilon,
			     double *fr) const;
    
  /** passage � priv� d'une virtuelle h�rit�e. 
  */
  double LogInd(int m1, 
		int m2,
		int *nbdata) const { return 0.0;};
  
  /** passage � priv� d'une virtuelle h�rit�e.    
  */
  double EspRec(int m1,
		int m2, 
		double theta,
		double *loglike) const { return 0.0;};

  /** Pr�paration des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  void PreparEM(const Carte *data);

  /** nettoyage des structures pour la programmation dynamique de EM.
      @param data la carte
    */
  void NetEM(const Carte *data);

  /** Pour la programmation dynamique. tableaux des probabilités
      conditionnelles de passage par un sommet depuis la gauche
  */
  //doublePtr SourceTo[4];

  /**  la programmation dynamique. tableaux des probabilit�s
       conditionnelles de passage par un sommet depuis la droite
  */
  //doublePtr ToSink[4];

  /** Calcul de la probabilite de passage par une arete suivant la paire de genotypes relies
      @param a le premier g�notype
      @param b le second g�notype
      @param p la proba de cassure
      @param r la proba de r�tension
      @return la probabilit�
  */
  double ProbArete(int a, int b, double p, double r) const;

  /** Calcul de toutes les probas
      @param p la proba de cassure
      @param r la proba de rétention
  */
  void CompProbArete(EM_state& state, double p, double r);

 
  /** Calcul de la distance
      @param le theta.
      @return la distance
  */
  inline double Distance(double theta) const 
    {
      return (Theta2Ray(theta));
    };
  /** Calcul de la distance
      @param l'unit�
      @param le theta.
      @return la distance
  */
  inline double Distance(const char unit[2], double theta) const 
    {
      return (Theta2Ray(theta));
    }; 
};

#endif /* _BJS_RHD_H */
