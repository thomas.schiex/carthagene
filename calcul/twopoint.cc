#include "BioJeu.h"
#include "BioJeuSingle.h"
#include "BioJeuMerged.h"
#include "twopoint.h"

namespace TwoPoint {
	Matrices::Matrices(BioJeu* bj)
		: m(bj, bj->NbMarqueur, bj->NbMarqueur)
		{
			DHfunc = bj->HasRH() ? Theta2Ray : Haldane;
		}
}

struct extract_path {
    std::string dirname;
    std::string basename;
    extract_path(std::string tmp) {
        size_t f = tmp.find_last_of("/\\");
		if(f!=std::string::npos) {
	        dirname = tmp.substr(0, f);
    	    basename = tmp.substr(f+1);
		} else {
			dirname = ".";
			basename = tmp;
		}
    }
    extract_path(const char*x) {
		extract_path(std::string(x));
    }
};


std::string extract_name(BioJeu* bj) {
    BioJeuSingle* bjs;
    BioJeuMerged* bjm;
    try {
        if((bjs = dynamic_cast<BioJeuSingle*>(bj))) {
            return bjs->NomJeu;
        } else if((bjm = dynamic_cast<BioJeuMerged*>(bj))) {
            extract_path g(extract_name(bjm->BJgauche));
            extract_path d(extract_name(bjm->BJdroite));
            std::string ret = g.dirname;
            ret += '/';
            ret += g.basename;
            ret += "-merged-";
            ret += d.basename;
            return ret;
        }
    } catch(const std::bad_cast& bc) {
    }
    std::stringstream ret;
    ret << "anon-" << bj;
    return ret.str();
}

twopoint_traits::
	pagefile_name_provider_type::
		pagefile_name_provider_type(context_type context) {
            n = extract_name(context);
            n += ".2pt";
		}

