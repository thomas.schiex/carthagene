#ifndef _PACKED_ARRAY_H
#define _PACKED_ARRAY_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef unsigned int word_t;

#define BITSPERWORD (sizeof(word_t)*8)

struct packed_array_ {
        word_t bitsperitem;
        word_t maxitems;
        word_t* data;
};

typedef struct packed_array_* packed_array_t;

/* create a new packed_array instance */
packed_array_t pa_new(word_t maxitems);
/* create a new array of packed arrays. individual arrays can be retrieved using &(mymultiarray[index]) */
packed_array_t pa_multi_new(word_t maxitemsperarray, word_t numberofarrays);
/* retrieve a packed array instance from an array of packed arrays */
packed_array_t pa_multi_get(packed_array_t mpa, word_t index);
/* retrieve index of a packed array instance in an array of packed arrays */
word_t pa_multi_get_index(packed_array_t mpa, packed_array_t item);
/* delete a packed array instance */
void pa_delete(packed_array_t pa);
/* delete an array of packed arrays */
void pa_multi_delete(packed_array_t mpa, word_t numberofarrays);
/* zero all elements in a packed array instance */
void pa_clear(packed_array_t pa);
/* helper : compute bitsize required to store numbers between 0 and sup (included) */
word_t bitsize(word_t sup);
/* pack array of (small) ints into a packed array instance */
void pa_pack(packed_array_t pa, const int* tour);
/* unpack ints from a packed array instance */
void pa_unpack(const packed_array_t pa, int* tour);
/* hash contents of a packed array instance */
unsigned int pa_hash(const packed_array_t pa);
/* compare two packed array instances ; result is sign of (a-b) */
int pa_compare(const packed_array_t a, const packed_array_t b);
/* copy data from a packed array instance into another */
void pa_copy(packed_array_t src, packed_array_t dest);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* !defined(_PACKED_ARRAY_H) */

