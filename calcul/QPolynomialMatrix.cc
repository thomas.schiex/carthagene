#include "QPolynomialMatrix.h"
#include <stdio.h>

/*-----------------------------------------------------------------------------
James C. Nelson 
Dept. of Plant Pathology, Kansas State University, Manhattan, KS, USA

Copyright 2006 James C. Nelson
This file is distributed under the terms of the Q Public License version 1.0.
It may be freely included with CarthaGene.

$Id: BJS_BS.h, v 1.0 2006/02/06

A QPolynomialMatrix is a matrix of QPolynomials, possibly not all of the same degree.
Multiplication by another QPolynomialMatrix follows
the rules of both matrix and QPolynomial multiplication.
-----------------------------------------------------------------------------
*/	
	/**
	*	Constructor that leaves the QPolynomials uninstantiated
	*/
	QPolynomialMatrix::QPolynomialMatrix(int rows, int cols)
	{		
		mMatrix = new QPolynomial**[rows];
		mRows = rows;
		mColumns = cols;
		for (int i = 0; i < rows; i++)
			mMatrix[i] = new QPolynomial*[cols];
	}

	/**
	*	Constructor for a matrix of polynomials with all zero coefficients
	*/
	QPolynomialMatrix::QPolynomialMatrix(int rows, int cols, int degree)
	{
		QPolynomial **rowArr;
		
		mMatrix = new QPolynomial**[rows];
		mRows = rows;
		mColumns = cols;
		mMaxDegree = degree;
		for (int i = 0; i < rows; i++)
		{
			mMatrix[i] = rowArr = new QPolynomial*[cols];
			for (int j = 0; j < cols; j++)
				rowArr[j] = new QPolynomial(degree);
		}
	}

	/*
	 * Constructor from a 3D array of double, where "degree" is one less than the
	 * number of values in the third dimension. Used for loading a constant linearly
	 * addressed array into a pointer-to-array-of-pointers array.
	 */
	QPolynomialMatrix::QPolynomialMatrix(double *P, int rows, int cols, int degree)
	{
		double *dP;
		QPolynomial **rowArr;
		
		mMatrix = new QPolynomial**[rows];
		mRows = rows;
		mColumns = cols;
		mMaxDegree = degree;
		for (int i = 0; i < rows; i++)
		{
			mMatrix[i] = rowArr = new QPolynomial*[cols];
			for (int j = 0; j < cols; j++)
			{
				dP = P + (i * cols + j) * (degree + 1);
				rowArr[j] = new QPolynomial(dP, degree);
			}
		}
	}

	// Destructor
	QPolynomialMatrix::~QPolynomialMatrix()
	{		
		for (int i = 0; i < mRows; i++)
		{
			for (int j = 0; j < mColumns; j++)
				delete mMatrix[i][j];	// delete QPolynomial*
			delete [] mMatrix[i];		// delete array of QPolynomial*
		}
		delete [] mMatrix;	// added this 7.10.06 after discovered memory leak!
	}
	
	int QPolynomialMatrix::getNumRows()
	{	return mRows;}

	int QPolynomialMatrix::getNumColumns()
	{	return mColumns;}	
	
	int QPolynomialMatrix::getMaxDegree()
	{	return mMaxDegree;}

	void QPolynomialMatrix::setMaxDegree(int degree)
	{	mMaxDegree = degree;}

	QPolynomial ***QPolynomialMatrix::getMatrix()
	{	return mMatrix;}
	
	// returns the QPolynomial at the desired row and colum of QPM
	QPolynomial *QPolynomialMatrix::extractPoly(int row, int col)
	{	return mMatrix[row][col];}
	
	/**
		Multiplies all polynomials by a constant, in place. Used in our app for zeroing accumulator matrix.
	*/
	void QPolynomialMatrix::timesScalarEquals(double c)
	{
		for (int i = 0; i < mRows; i++)
			for (int j = 0; j < mColumns; j++)
				mMatrix[i][j]->timesScalarEquals(c);
	}

	/**
		Multiplies all polynomials in each row by the constant of the corresponding scaling-vector row, in place.
	*/
	void QPolynomialMatrix::rowTimesScalarEquals(double *rowScalingArr) const
	{
		double d;
		
		for (int i = 0; i < mRows; i++)
		{
			d = rowScalingArr[i];
			for (int j = 0; j < mColumns; j++)
				mMatrix[i][j]->timesScalarEquals(d);
		}
	}
	/**
	*	Adds addendM matrix to this one, rotating the destination by the specified number of columns,
	* with a plus value indicating right and a minus indicating left. The matrices need not be
	*	of the same dimension. When shiftColumns is 1, the elements in column 0 of the
	*	addend matrix are added to their row counterparts in column 1 of this matrix, and those in
	*	the last column of the addend are added to column 0 of this matrix.
	*	We should raise an exception in case of unequal dimensions, but at present this class is
	*	used only in CG where it's guaranteed to be used right.
	* UPDATED 2.6.06 so that the matrices are NOT assumed to be of the same dimension. We loop
	* over minRows and minColumns, but still do the rotate with respect to the full column
	* dimension of the target (this) matrix.
	*/
	void QPolynomialMatrix::plusEqualsWithRotate(QPolynomialMatrix *addendPM, int rotateColumns)
	{
		int
			addendMaxDegree,
			minCols = mColumns,
			minRows = mRows,
			thoseCols = addendPM->getNumColumns(),
			thoseRows = addendPM->getNumRows();
			
		if (thoseCols < minCols) minCols = thoseCols;
		if (thoseRows < minRows) minRows = thoseRows;
		
		if (rotateColumns < 0)	// convert a left rotate to a right one
			rotateColumns = mColumns - rotateColumns;
		QPolynomial ***addendM = addendPM->getMatrix();
		for (int i = 0; i < minRows; i++)
			for (int j = 0; j < minCols; j++)
				mMatrix[i][(j + rotateColumns) % mColumns]->plusEquals(addendM[i][j]);
		addendMaxDegree = addendPM->getMaxDegree();	// set max degree to the larger of the two maxes
		if (addendMaxDegree > mMaxDegree)
			mMaxDegree = addendMaxDegree;
	}

	/**
	* Postmultiplies this matrix with the matrix argument. The masking matrix (if supplied) is of the same
	*	dimension as this matrix, and only where its elements are equal to testValue are polynomials
	*	from the same row and column of this matrix used in multiplication; otherwise zero polys
	*	are substituted. This is the only kind of PM multiplication used in CG.
	* Updated CN 2.6.06; now if no mask is supplied, ordinary matrix multiplication is done.
	*/
	QPolynomialMatrix *QPolynomialMatrix::maskedTimes(QPolynomialMatrix *postPM, int **maskingMatrix, int testValue)
	{
		int
			newDegree,
			maskValue = testValue;
		QPolynomial **thisPMRowiV, **postPMcoljV, *sumP, ***productM, *subP, *cellProductP;
		QPolynomialMatrix *productPM;
		
		newDegree = mMaxDegree + postPM->getMaxDegree();
		productPM = new QPolynomialMatrix(mRows, postPM->mColumns, newDegree);
		productM = productPM->mMatrix;
		postPMcoljV = new QPolynomial*[mColumns];

		for (int j = 0; j < postPM->mColumns; j++)
		{
			for (int k = 0; k < mColumns; k++)	// mColumns is equal to postPM->mRows
				postPMcoljV[k] = postPM->mMatrix[k][j]; // construct a vector from the jth column of postM
			for (int i = 0; i < mRows; i++)
			{
				thisPMRowiV = mMatrix[i]; // alias for the ith row of the premultiplying matrix
				sumP = productM[i][j];	// another alias for the destination polynomial
				for (int m = 0; m < mColumns; m++)
				{
					if (maskingMatrix)
						maskValue = maskingMatrix[i][m];
					if (maskValue != testValue)
						continue;	// no arithmetic to do
					subP = thisPMRowiV[m];
					cellProductP = subP->times(postPMcoljV[m]);
					sumP->plusEquals(cellProductP);
					delete cellProductP;
				}
			}
		}
		delete [] postPMcoljV; // release temp storage
		productPM->setMaxDegree(newDegree);
		return productPM;
	}
	
	/**
	*	Sets mMaxDegree to the highest degree of any of the component polynomials. This will ensure
	*	that a powers vector used for evaluation is of a length sufficient for each polynomial.
	*/
	void QPolynomialMatrix::updateMaxDegree()
	{
		int degree = 0;
		
		mMaxDegree = 0;
		for (int i = 0; i < mRows; i++)
			for (int j = 0; j < mColumns; j++)
			{
				degree = mMatrix[i][j]->getDegree();
				if (degree > mMaxDegree)
					mMaxDegree = degree;
			}
	}

	// Evaluates the polynomials in the specified column, using the supplied powers vector
	void QPolynomialMatrix::evaluateByColumn(double *powersV, double *evals, int column)
	{
		for (int i = 0; i < mRows; i++)
			evals[i] = mMatrix[i][column]->evaluate(powersV);
	}

	/**
	*	Utility method for avoiding futile ops
	*/
	bool QPolynomialMatrix::columnHasAllZeroCoeffs(int col)
	{
		bool allZero = true;
		
		for (int row = 0; row < mRows; row++)
			if (!mMatrix[row][col]->hasAllZeroCoeffs())
		{	allZero = false;
			break;
		}
		return allZero;
	}
	
	/**
	*	Special method that fills out a r x r matrix from the outer product of two specified
	*	columns of this QPM and another QPM with at least r rows
	*/
	QPolynomialMatrix *QPolynomialMatrix::computeOuterColumnProduct
			(int thisCol, QPolynomialMatrix *thatPM, int thatCol, bool &allZero)
	{
		allZero = true;
		if (columnHasAllZeroCoeffs(thisCol) || thatPM->columnHasAllZeroCoeffs(thatCol))
					return thatPM;			// no use multiplying zeros together
		
		allZero = false;
		QPolynomialMatrix *outerPM = new QPolynomialMatrix(mRows, mRows);
		QPolynomial
			***thatM = thatPM->getMatrix(),
			***outerM = outerPM->getMatrix();
		
		for (int i = 0; i < mRows; i++)
			for (int j = 0; j < mRows; j++)
				outerM[i][j] = mMatrix[i][thisCol]->times(thatM[j][thatCol]);
		
		return outerPM;
	}
	

/**
 * for debugging, since Eclipse plugin for C/C++ debugging is incomplete
 */
void QPolynomialMatrix::printPM()
{
  for (int i = 0; i < mRows; i++)
    {
      for (int j = 0; j < mColumns; j++)
	{
	  mMatrix[i][j]->printPoly();
	  printf("|");
	}
      printf("\n");
    }
  printf("\n");           
}
