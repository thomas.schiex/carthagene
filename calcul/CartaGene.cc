//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// CartaGene.cc,v 1.106 2007/06/11 15:05:52 degivry Exp
//
// Description : Classe API du module.
// Divers :
//-----------------------------------------------------------------------------

#include "config.h"

#include "CartaGene.h"
#include "BJS_BC.h"
#include "BJS_IC_parallel.h"
#include "BJS_BS.h"
#include "BJS_RH.h"
#include "BJS_RHD.h"
#include "BJS_RHE.h"
#include "BJM_GE.h"
#include "BJM_OR.h"
#include "BJS_CO.h"
#include "BJS_OR.h"
#include "BioJeu.h"
#include "Constraints.h"
#include "AlgoGen.h"

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <math.h>
#ifdef ppc
#include "/Developer/Headers/FlatCarbon/fp.h"
#endif
#ifdef __WIN32__
#include <winsock2.h>
#endif

#ifdef sun4
#include <ieeefp.h>
#endif

#ifdef __WIN32__
#include <float.h>
#endif

#include "NOrCompMulti.h"


#include <algorithm>
#include <iostream>
#include <sstream>

using parallel_code::BJS_IC_parallel;

// DL
// table de correspondance observation->lettre.
char Obstoa[16] = {
	'_',	// should be an error
	'A',
	'2',
	'3',
	'4',
	'5',
	'H',
	'D',
	'B',
	'9',
	'a',
	'b',
	'c',
	'd',
	'C',
	'-'
};



//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
CartaGene::CartaGene()
    : markers()
{
    markers.clear();
    markers.add("");

	Fout = NULL;
	NbMarqueur = 0;
	NbJeu = 0;
	NbIndividu = 0;
	//NomMarq = NULL;
	//BitJeuMarq = NULL;
	Jeu = NULL;
	ArbreJeu = NULL;
	Group = NULL;
	DisThres = 0.0;
	//Merged = NULL;
	//Represents = NULL;
	LODThres = 0.0;
	Robustness = 1e100;
	VerboseFlag = 0;
	DistPrecision = 1;
	QuietFlag = 0;
	NbMS = 0;
	EM2pt = 0;
	MAXHEAPSIZETSP = 1024;
	MarkSelect = NULL;
	wordre = NULL;
	NOrComput = NULL;

	StopFlag = 0;

	Heap = new Tas();
	Heap->Init(this);
}
//-----------------------------------------------------------------------------
// Destructeur.
//-----------------------------------------------------------------------------

CartaGene::~CartaGene()
{
	int i;
	NodintPtr padj, padjw;
	NodptrPtr plink;

	markers.clear();

	if (Fout != NULL)
		fclose(Fout);


	//for (i = 1; i <= NbMarqueur; i++) {
	//	delete [] NomMarq[i];
	//}
	//delete [] NomMarq;
	//delete [] Merged;
	delete [] MarkSelect;
	//delete [] Represents;

	for (i = 1; i <= NbJeu; i++) delete Jeu[i];
	delete [] Jeu;
	//delete [] BitJeuMarq;

	plink = Group;

	while (plink != NULL) {

		padj = plink->first;

		while (padj != NULL)
		{
			padjw = padj;
			padj = padj->next;
			delete padjw;
		}

		Group = plink;
		plink = plink->next;
		delete Group;
	}
	delete  NOrComput;
	delete Heap;
}

//-----------------------------------------------------------------------------
// gestion de la trace
//-----------------------------------------------------------------------------

void CartaGene::Tracer(char *fileName)
{
	FILE *ftmp;

	// demande de fermeture
	if (strcmp(fileName, "") == 0)
	{
		// un fichier est d�ja ouvert
		if (Fout != NULL)
		{
			fclose(Fout);
			Fout = NULL;
		}
	} else
	{
		// �chec de l'ouverture (rien faire)
		ftmp = fopen(fileName,"a");
		if (ftmp == NULL)
		{
			print_err("The opening of the file has failed!\n");
		}
		else
		{
			// un fichier est d�ja ouvert
			if (Fout != NULL)
			{
				fclose(Fout);
				Fout = NULL;
			}
			Fout = ftmp;
		}
	}
}

//-----------------------------------------------------------------------------
// affiche la version.
//-----------------------------------------------------------------------------

void CartaGene::Version(void)
{
	print_out("\n");
	print_out("   CarthaGene version " CG_VERSION_STR);
#ifdef LKHLICENCE
	print_out("-LKH");
#endif
    print_out( " " CG_BUILD_STR);
	print_out(", Copyright (c) 1997-2010 (INRA).\n");
	print_out("\n");
	print_out("   CarthaGene comes with ABSOLUTELY NO WARRANTY. \n");
	print_out("   CarthaGene is free software. You are welcome to redistribute it,\n");
	print_out("   under certain conditions. See the ""License"" file for information.\n");
	print_out("\n");
	print_out("Type 'help' for help.\n");
	print_out("\n");
}
//-----------------------------------------------------------------------------
// Netoyeur.
//-----------------------------------------------------------------------------

void CartaGene::Clear(void)
{
	int i;
	NodintPtr padj, padjw;
	NodptrPtr plink;

	//for (i = 1; i <= NbMarqueur; i++) {
	//	delete [] NomMarq[i];
	//}
	//delete [] NomMarq;
	//delete [] BitJeuMarq;
	//delete [] Merged;
	//delete [] Represents;

	for (i = 1; i <= NbJeu; i++) delete Jeu[i];
	delete [] Jeu;

	plink = Group;

	while (plink != NULL) {

		padj = plink->first;

		while (padj != NULL)
		{
			padjw = padj;
			padj = padj->next;
			delete padjw;
		}

		Group = plink;
		plink = plink->next;
		delete Group;
	}

	delete [] wordre;

	delete [] MarkSelect;

	delete NOrComput;

    markers.clear();
    markers.add("");

	//Merged = NULL;
	//Represents = NULL;
	NbMarqueur = 0;
	NbJeu = 0;
	NbIndividu = 0;
	//NomMarq = NULL;
	//BitJeuMarq = NULL;
	Jeu = NULL;
	ArbreJeu = NULL;
	Group = NULL;
	DisThres = 0.0;
	LODThres = 0.0;
	Robustness = 1e100;
	VerboseFlag = 0;
	QuietFlag = 0;
	NbMS = 0;
	EM2pt = 0;
	MAXHEAPSIZETSP = 1024;
	MarkSelect = NULL;
	wordre = NULL;
	NOrComput = NULL;

	Heap->Init(this);

}
//-----------------------------------------------------------------------------
// Lecture de la clef de la license
//-----------------------------------------------------------------------------
// Param�tres : la clef
// -
// -
// Valeur de retour : aucune
//-----------------------------------------------------------------------------
void CartaGene::License(const char *key)
{
	// License supprimee depuis mise sous QPL
}
//-----------------------------------------------------------------------------
// Lit le type de croisement utilis� dans le fichier de donn�es
//-----------------------------------------------------------------------------
// Param�tres : le nom du fichier
// -
// -
// Valeur de retour : le type de croisement
//-----------------------------------------------------------------------------

CrossType CartaGene::ProbeCrossType(char DataFile[256], char *design_string)
{
	FILE *fdata;
	char line[256];
	char *tok;

	if ((fdata=fopen(DataFile, "r")) == NULL)
	{
		perror(DataFile);
		return(Unk);
	}

	if(!fgets(line, 256, fdata)) {
		print_err( "%s : couldn't read line\n", DataFile);
		return(Unk);
	}
	tok= line;
	while (*tok) {
		*tok= tolower(*tok);
		tok++;
	}

	fclose(fdata);

	tok = strtok(line, " \t\f\n\r\v");

	if (strcmp(tok,"data") || strcmp(strtok(NULL," \t\f\n\r\v"),"type")) {
		print_err( "%s : Bad header on line 1\n", DataFile);
		return(Unk);
	}

	tok = strtok(NULL," \t\f\n\r\v");

	if (!strcmp(tok,"f2")) {// backcross ou intercross
		tok = strtok(NULL," \t\f\n\r\v");
		if (!strcmp(tok, "backcross")) return(BC);
		if (!strcmp(tok, "intercross")) return(IC);
		print_err( "%s : Bad header on line 1\n", DataFile);
		return(Unk);
	}

	if (!strcmp(tok,"ri")) {//RIL
		tok = strtok(NULL," \t\f\n\r\v");
		if (!strcmp(tok, "self")) return(RISelf);
		if (!strcmp(tok, "sib")) return(RISib);
		print_err( "%s : Bad header on line 1\n", DataFile);
		return(Unk);
	}

	if (!strcmp(tok,"radiated") && !strcmp(strtok(NULL," \t\f\n\r\v"),"hybrid")) {//RH
		tok = strtok(NULL," \t\f\n\r\v");
		if (!tok)  return(RH);
		if (!strcmp(tok, "diploid")) return(RHD);
		if (!strcmp(tok, "error")) return(RHE);  //Added by TF for haploid error model

		print_err( "%s : Bad header on line 1\n", DataFile);
		return(Unk);
	}

	// next case added by CN 11.10.05.
	if (!strcmp(tok, "bs"))   // backcross/selfing/intercrossing series
	{
		tok = strtok(NULL," \t\f\n\r\v");
		if (tok)
		{
			bool err = ((int)strlen(tok) > MAX_BS_GENERATIONS // design string is too long
					|| (int)strcspn(tok, sMatingSymbols) > 0);       // design string is illegal
			if (!err)
			{
				strncpy(design_string, tok, MAX_BS_GENERATIONS + 1);    // need the design string for passing to BJS_BS co$
				return(BS);
			}
			print_err( "%s : Illegal or missing design string for BS cross type on line 1\n", DataFile);
			return(Unk);
		}
	}
	// end of CN code 11.10.05

	if (!strcmp(tok,"constraint")) return(Con);

	if (!strcmp(tok,"order")) return(Ordre);


	print_err( "%s : Bad header on line 1\n", DataFile);
	return(Unk);
}

//-----------------------------------------------------------------------------
// Charge un jeu de donn�es biologiques � partir d'un fichier :
// - mise � jour des informations sur les marqueurs
// - cr�ation et stockage d'un BioJeu
// - mise � jour de la liste des marqueurs actifs.
//-----------------------------------------------------------------------------
// Param�tres :
// - le nom du fichier
// Valeur de retour : Chaine de car. contenant
// - le num�ro du jeu
// - le type du jeu
// - le nombre de marqeurs
// - le nombre d'individus
// ou une chaine vide en cas d'erreur.
//-----------------------------------------------------------------------------

BioJeu* CartaGene::CharJeu(char *fileName)
{
    read_data::raw_data rd = ::read_data::read_file(fileName);
    return NouveauJeu(fileName, rd);
}


BioJeu* CartaGene::NouveauJeu(const char* fileName, read_data::raw_data& rd) {
    if(rd.Cross == Unk) {  /* the initialization of raw_data failed. */
        return NULL;
    }

	int numjeu = NbJeu + 1;
	int bitjeu = 1<<(numjeu - 1);

    Constraint* con = NULL;
    Constraint* tmp;

    int* indmarq;
    std::vector<int> indmarq_tmp;
    Marker* mrk;
    Obs** Ech = NULL;

    BioJeu* bjp = NULL;

	if (numjeu >= MAXNBJEU) {
		print_err( "%s : Maximum number of simultaneously datasets loaded exceeded (%d)\n", fileName,MAXNBJEU);
		return NULL;
	}

    for(read_data::MarkerNames::iterator i = rd.marker_names.begin();
            i != rd.marker_names.end(); ++i) {
        mrk = markers.add(i->c_str());  /* !!! */
        indmarq_tmp.push_back(markers.indexOf(mrk));
        mrk->BitJeu |= bitjeu;
        /* FIXME: Merged et Represents étaient réinitialisés ici. ??!? */
    }

    if(rd.Cross != Con && rd.Cross != Ordre) {
        Ech = new Obs*[rd.nm + 1];
        Ech[0] = new Obs[rd.ni + 1];
        for(int i = 0; i <= rd.ni; ++i) {
            Ech[0][i] = Obs1111;
        }
        for(int i = 0; i < rd.nm; ++i) {
            Ech[i + 1] = new Obs[rd.ni + 1];
            Ech[i + 1][0] = Obs1111;
            for(int j = 0; j < rd.ni; ++j) {
                char c = rd.tr[(int)rd.marker_data[i][j]];
                switch(c)
                {
                    case ' ': j--; break;
                    case 'A': Ech[i + 1][j + 1] = Obs0001;   break;
                    case 'H': Ech[i + 1][j + 1] = Obs0110;   break;
                    case 'B': Ech[i + 1][j + 1] = Obs1000;   break;
                    case 'C': Ech[i + 1][j + 1] = Obs1110;   break;
                    case 'D': Ech[i + 1][j + 1] = Obs0111;   break;
                    case '-': Ech[i + 1][j + 1] = Obs1111;   break;
                    case '2': Ech[i + 1][j + 1] = Obs0010;   break;
                    case '3': Ech[i + 1][j + 1] = Obs0011;   break;
                    case '4': Ech[i + 1][j + 1] = Obs0100;   break;
                    case '5': Ech[i + 1][j + 1] = Obs0101;   break;
                    case '9': Ech[i + 1][j + 1] = Obs1001;   break;
                    case 'a': Ech[i + 1][j + 1] = Obs1010;   break;
                    case 'b': Ech[i + 1][j + 1] = Obs1011;   break;
                    case 'c': Ech[i + 1][j + 1] = Obs1100;   break;
                    case 'd': Ech[i + 1][j + 1] = Obs1101;   break;
                    case '0':
                              Ech[i + 1][j + 1] = Obs0000;
                              if (rd.Cross != IC && rd.Cross != BS) break; // CN added the BS condition, 12.29.05

                    default:
                              print_out( "%s: Syntax error on marker ", fileName);
                              print_out( "line %d, position %d, code '%c'\n", i + 1, j + 1, rd.marker_data[i][j]);
                              for(int k = 0; k <= rd.nm; ++k) {
                                  delete[] Ech[k];
                              }
                              delete[] Ech;
                              return NULL;
                }
            }
        }
    } else if(rd.Cross == Con) {
        int m1, m2, m3, i;
        con = NULL;
        for(i = 0; i < rd.constraints.size(); ++i) {
            tmp = con;
            m1 = markers.indexOf(rd.constraints[i].m1.c_str());
            if(!m1) {
                print_err("%s: syntax error on line %d; the marker %s does not exist.\n", fileName, rd.constraints[i].lineno, rd.constraints[i].m1.c_str());
                break;
            }
            m2 = markers.indexOf(rd.constraints[i].m2.c_str());
            if(!m2) {
                print_err("%s: syntax error on line %d; the marker %s does not exist.\n", fileName, rd.constraints[i].lineno, rd.constraints[i].m2.c_str());
                break;
            }
            m3 = markers.indexOf(rd.constraints[i].m3.c_str());
            if(!m3) {
                print_err("%s: syntax error on line %d; the marker %s does not exist.\n", fileName, rd.constraints[i].lineno, rd.constraints[i].m3.c_str());
                break;
            }
            con = new Constraint(m1, m2, m3, rd.constraints[i].penalty);
            con->Next = tmp;
        }
        if(i != rd.constraints.size()) {
            while(con) {
                tmp = con;
                con = con->Next;
                delete tmp;
            }
            return NULL;
        }
    }

    /* TODO:
     * virer l'appel à CharJeuOrdre
     * ?
     * ...
     * PROFIT!
     */
    NbMarqueur = markers.size()-1;

    indmarq = new int[NbMarqueur + 1];
    indmarq[0] = 0;
    int i;
    for(i = 0; i < (int) indmarq_tmp.size(); ++i) {
        indmarq[i + 1] = indmarq_tmp[i];
    }
    for(; i < NbMarqueur; ++i) {
        indmarq[i + 1] = 0;
    }

    try {
        switch (rd.Cross)
        {
            case BC:
            case RISelf:
            case RISib:
                bjp = new BJS_BC(this, numjeu, (char*)fileName, rd.Cross,
                                 rd.nm, rd.ni, bitjeu, indmarq, Ech);
                bjp->Cross = rd.Cross;
                break;

            case IC:
                bjp = new BJS_IC_parallel(this, numjeu, (char*)fileName, rd.Cross,
                                          rd.nm, rd.ni, bitjeu, indmarq, Ech);
                break;
            case RH:
                bjp = new BJS_RH(this, numjeu, (char*)fileName, rd.Cross, rd.nm,
                                 rd.ni, bitjeu, indmarq, Ech);
                break;
            case RHD:
                bjp = new BJS_RHD(this, numjeu, (char*)fileName, rd.Cross, rd.nm,
                                  rd.ni, bitjeu, indmarq, Ech);
                break;
            case RHE: // added by TF RHE
                bjp = new BJS_RHE(this, numjeu, (char*)fileName, rd.Cross, rd.nm,
                                  rd.ni, bitjeu, indmarq, Ech);
                break;
            case Con:
                bjp = new BJS_CO(this, numjeu, (char*)fileName, rd.Cross,
                                 rd.nm, rd.constraints.size(), bitjeu, con);
                break;
            case BS:    // case added by CN 11.10.05
                bjp = new BJS_BS(this, numjeu, (char*)fileName, rd.Cross,
                                 (char*)rd.design_string.c_str(),
                                 rd.nm, rd.ni, bitjeu, indmarq, Ech);
                break;
                // end of case added by CN 11.10.05
            case Ordre:
                bjp = new BJS_OR(this, numjeu, rd.Cross, (char*)fileName,
                                 rd.nm, bitjeu, indmarq, rd.coeff,
                                 rd.chrom, rd.chromex, rd.position);
                break;
            default :
                print_out(  "Cartagene.cc::CharJeu() - pedigree impossible\n");
                // exit(1);
                return NULL;
                break;
        };
    } catch(const std::string& new_bj_failed) {
        std::cerr << "Couldn't load the dataset : " << new_bj_failed << std::endl;
        return NULL;
    }

	PostTraitementBioJeu(bjp);

	dernierJeuCharge = bjp;
    return bjp;
}

char * CartaGene::CharJeu_OLD(char *fileName)
{
	struct stat buf;
	FILE *fdata;

	CrossType Cross;
	int nbi = 0, nbm = 0/*, nbnm = 0*/;
	Obs **Ech = NULL;
	int bitjeu;
	BioJeu *bjp;

	int numjeu = NbJeu + 1;
	int *indmarq = NULL;
    std::vector<int> indmarq_tmp;

	char *vide;
	char *lineres;
	char c, d, l[256], tr[256], l2[256], l3[256];
	char *line = l;
	int i, j=1, k, pos, m1, m2, m3;
	double pen;
	char design_string[MAX_BS_GENERATIONS + 1];   // added CN 11.10.05 to capture mating design from a BS CrossType

	// pour retourner une liste vide(voir conventions)
	vide = new char[1];
	vide[0] = '\0';

	// On teste si le nombre maximum de jeu charges est atteint.
	if (NbJeu >= MAXNBJEU) {
		print_err( "%s : Maximum number of simultaneously datasets loaded exceeded (%d)\n", fileName,MAXNBJEU);
		return vide;
	}

	Constraint *cons = NULL, *con;

	if (stat(fileName, &buf) == -1)
	{
		perror(fileName);
		return vide;
	}

	Cross = ProbeCrossType(fileName, design_string);

	if ( Cross == Unk ) return vide;

	lineres = new char[256];

	if (Cross == Ordre) goto Jump1;


	fdata=fopen(fileName, "r"); // no error test needed, done in "ProbeCrossType"
	if(!fgets(lineres, 256, fdata)) { // stockage pour la r�ponse
		print_err( "%s : couldn't read line\n", fileName);
		return vide;
	}


	// chargement diff�rent...
	if ( Cross != Con )
	{
		if(!fgets(line, 256, fdata)) {
			print_err( "%s : couldn't read line\n", fileName);
			return vide;
		}
		if ((i=sscanf(line, "%d %d%n", &nbi, &nbm, &pos)) < 2) {
			print_err( "%s : Syntax error on line 2, number of loci or individual missing\n", fileName);
			delete lineres;
			return vide;
		}

		line += pos;
		if (sscanf(line, "%*d%n", &pos) != EOF) {//  more to read ?
			line += pos;
			if (sscanf(line," case%n",&pos)) line += pos;
		}

		for (i = 0; i < 256;i++) tr[i] = 0;

		if (Cross != IC && Cross != BS) { // CN added the BS condition, 12.29.05
			tr[' '] = ' ';
			tr['-'] = '-';
			tr['A'] = '0';
			tr['B'] = 'A';
			tr['H'] = 'A';
		}
		else {
			tr[' '] = ' ';
			tr['-'] = '-';
			tr['A'] = 'A';
			tr['B'] = 'B';
			tr['C'] = 'C';
			tr['D'] = 'D';
			tr['H'] = 'H';
			tr['0'] = '0'; // 0000
			tr['1'] = 'A'; // 0001
			tr['2'] = '2'; // 0010
			tr['3'] = '3'; // 0011
			tr['4'] = '4'; // 0100
			tr['5'] = '5'; // 0101
			tr['6'] = 'H'; // 0110
			tr['7'] = 'D'; // 0111
			tr['8'] = 'B'; // 1000
			tr['9'] = '9'; // 1001
			tr['a'] = 'a'; // 1010
			tr['b'] = 'b'; // 1011
			tr['c'] = 'c'; // 1100
			tr['d'] = 'd'; // 1101
			tr['e'] = 'C'; // 1110
			tr['f'] = '-'; // 1111
		}

		if (sscanf(line," %*s%n",&pos) != EOF) {
			line += pos;
			while (1)	  {
				if ((i = sscanf(line, " %c=%c%n", &c, &d, &pos)) < 2 ) break;
				line += pos;
				tr[(int)c] = tr[(int)d];
			}
		}
	}

	// bitjeu est le masque bit a bit correspondant au jeu de donnee "numjeu"
	bitjeu = 1<<(numjeu - 1);

	// chargement diff�rent...
	if (Cross != Con)
	{

		// Allocation m�moire pour le tableau de donn�ees.
		Ech = new Obs*[nbm + 1];
		for (i = 0; i <= nbm; i++)
		{
			Ech[i] = new Obs[nbi + 1];
			Ech[i][0] = Obs1111;
		}

		for (i = 0; i <= nbi; i++) Ech[0][i] = Obs1111;

        Marker* mrk;

		for (i = 1; i <= nbm ; i++) {
			if(fscanf(fdata, " *%s ", line)==EOF) {
				print_err( "%s : couldn't read marker name\n", fileName);
				/*return vide;*/
			}

            mrk = markers.add(line);
            indmarq_tmp.push_back(markers.indexOf(mrk));

            if(mrk->BitJeu&bitjeu) {
				print_out( "%s: Syntax error on marker, %s is not uniq.\n", fileName,line);
				for (k = 0; k <= nbm ; k++)
					delete Ech[k];
				//delete marqex;
				delete lineres;
				delete Ech;
				fclose(fdata);
				return vide;
            }

            mrk->BitJeu|=bitjeu;
            mrk->Merged=0;
            mrk->Represents=0;

			do c = getc(fdata);
			while (isspace(c));

			for (j = 1; j <= nbi; j++) {
				c = tr[(int)c];
				switch(c)
				{
					case ' ': j--; break;
					case 'A': Ech[i][j] = Obs0001;   break;
					case 'H': Ech[i][j] = Obs0110;   break;
					case 'B': Ech[i][j] = Obs1000;   break;
					case 'C': Ech[i][j] = Obs1110;   break;
					case 'D': Ech[i][j] = Obs0111;   break;
					case '-': Ech[i][j] = Obs1111;   break;
					case '2': Ech[i][j] = Obs0010;   break;
					case '3': Ech[i][j] = Obs0011;   break;
					case '4': Ech[i][j] = Obs0100;   break;
					case '5': Ech[i][j] = Obs0101;   break;
					case '9': Ech[i][j] = Obs1001;   break;
					case 'a': Ech[i][j] = Obs1010;   break;
					case 'b': Ech[i][j] = Obs1011;   break;
					case 'c': Ech[i][j] = Obs1100;   break;
					case 'd': Ech[i][j] = Obs1101;   break;
					case '0':
							  Ech[i][j] = Obs0000;
							  if (Cross != IC && Cross != BS) break; // CN added the BS condition, 12.29.05

					default:
							  print_out( "%s: Syntax error on marker ", fileName);
							  print_out( "%s, position %d, code '%c'\n", line, j,c);
							  for (k = 0; k <= nbm ; k++)
								  delete Ech[k];
							  delete lineres;
							  delete Ech;
							  fclose(fdata);
							  return vide;
				}
				c = getc(fdata);
			}
		}
	} else
	{
		cons = NULL;
		j = 1;
		while (1)
		{
			i = fscanf(fdata, "%s %s %s %lf", l, l2, l3, &pen);
			j++;
			if (i == EOF)  break;
			if (i<4)
			{
				print_err( "%s : Syntax error on line number %d\n", fileName, j);
				delete lineres;
				return vide;
			}
			m1 = markers.indexOf(l);
			m2 = markers.indexOf(l2);
			m3 = markers.indexOf(l3);
			// trouv� ou pas
			if(!(m1 && m2 && m3)) {
				if (m2 == 0) {
					print_err( "%s : Syntax error on line number %d, the marker %s does not exist.\n", fileName, j, l2);
				}
				else if (m3 == 0) {
					print_err( "%s : Syntax error on line number %d, the marker %s does not exist.\n", fileName, j, l3);
				}
				else {
					print_err( "%s : Syntax error on line number %d, the marker %s does not exist.\n", fileName, j, l);
				}

				// plus de nettoyage.

				while (cons)
				{
					con = cons->Next;
					delete cons;
					cons= con;
				}
				delete lineres;
				return vide;
			}

			con = cons;
			cons = new Constraint(m1,m2,m3,pen);
			cons->Next = con;
		}
	}

	fclose(fdata);

    NbMarqueur = markers.size()-1;

	// chargement diff�rent...
	if ( Cross != Con ) {
		// Allocation m�moire pour le tableau des indices de marqueurs
		indmarq = new int[NbMarqueur + 1];
		indmarq[0] = 0;
        for(i=0;i<indmarq_tmp.size();++i) {
            indmarq[i+1] = indmarq_tmp[i];
        }
        for(;i<NbMarqueur;++i) {
            indmarq[i+1]=0;
        }
	}


Jump1:
    try {
        switch (Cross)
        {
            case BC:
                bjp = new BJS_BC(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);

                sprintf(lineres, "%d f2 backcross %d %d %s", numjeu, nbm, nbi, fileName);
                break;

            case RISelf:
                bjp = new BJS_BC(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);
                bjp->Cross = RISelf;
                sprintf(lineres, "%d ri self %d %d %s", numjeu, nbm, nbi, fileName);
                break;

            case RISib:
                bjp = new BJS_BC(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);
                bjp->Cross = RISib;
                sprintf(lineres, "%d ri sib %d %d %s", numjeu, nbm, nbi, fileName);
                break;

            case IC:
                bjp = new BJS_IC_parallel(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);
                sprintf(lineres, "%d intercross %d %d %s", numjeu, nbm, nbi, fileName);
                break;

            case RH:
                bjp = new BJS_RH(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);
                sprintf(lineres, "%d haploid RH %d %d %s", numjeu, nbm, nbi, fileName);
                break;

            case RHD:
                bjp = new BJS_RHD(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);
                sprintf(lineres, "%d diploid RH %d %d %s", numjeu, nbm, nbi, fileName);
                break;

            case RHE: // added by TF RHE
                bjp = new BJS_RHE(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);
                sprintf(lineres, "%d haploid RH with Errors %d %d %s", numjeu, nbm, nbi, fileName);
                break;

            case Con:
                bjp = new BJS_CO(this,
                        numjeu,
                        fileName,
                        Cross,
                        nbm,
                        j-2,
                        bitjeu,
                        cons);
                sprintf(lineres, "%d constraint %d %s", numjeu, j-2, fileName);
                break;

            case BS:    // case added by CN 11.10.05
                bjp = new BJS_BS(this,
                        numjeu,
                        fileName,
                        Cross,
                        design_string,
                        nbm,
                        nbi,
                        bitjeu,
                        indmarq,
                        Ech);
                sprintf(lineres, "%d BS %s %d %d %s", numjeu, design_string, nbm, nbi, fileName);
                break;
                // end of case added by CN 11.10.05

            case Ordre:
                bjp = CharJeuOrdre(fileName);
                sprintf(lineres, "%d order %d %s", bjp->Id, bjp->NbMarqueur, fileName);
                break;

            default :
                print_out(  "Cartagene.cc::CharJeu() - pedigree impossible\n");
                // exit(1);
                return vide;
                break;
        };
    } catch(const std::string& new_bj_failed) {
        /*if ( Cross != Con ) {*/
            /*for(int i=0;i<=nbm;++i) {*/
                /*delete[] Ech[i];*/
            /*}*/
            /*delete[] Ech;*/
            /*delete[] indmarq;*/
        /*} else {*/
            /*while(cons) {*/
                /*con = cons->Next;*/
                /*delete cons;*/
                /*cons = con;*/
            /*}*/
        /*}*/
        std::cerr << "Couldn't load the dataset : " << new_bj_failed << std::endl;
        return vide;
    }

//	delete [] marqex;
	delete [] vide;

	/*if(Cross!=Ordre) {*/
	PostTraitementBioJeu(bjp);
	/*}*/

	dernierJeuCharge = bjp;

	return lineres;
}


// DL
//-----------------------------------------------------------------------------
// Post-traitement après création d'un nouveau biojeu
//-----------------------------------------------------------------------------
// Param�tres :
// - bjp le pointeur sur le nouveau biojeu
//-----------------------------------------------------------------------------
void CartaGene::PostTraitementBioJeu(BioJeu* bjp, BioJeu* clonee) {
	int numjeu = NbJeu+1;
	BioJeu **jeufus;
	int bitjeu = bjp->BitJeu;
	int i, j;
	int nbi = bjp->TailleEchant;
	int nbm = bjp->NbMarqueur;

	jeufus = new BioJeuPtr[numjeu + 1];
	jeufus[0] = NULL;
	// on fait ici les allocations dont on a besoin pour ordre
	/*if (bjp->Cross == Ordre) {*/

	// Allocation m�moire pour le tableau provisoire des champs de bit

	/*nbm = bjp->NbMarqueur;*/
	/*bitjeu = bjp->BitJeu;*/
	/*}*/

	ArbreJeu = bjp;
	for (i = 1; i <= NbJeu; i++) jeufus[i] = Jeu[i];
	jeufus[numjeu] = bjp;

	if ( Jeu != NULL ) delete [] Jeu;
	Jeu = jeufus;

	if(clonee) {
        MarkerList::iterator mi = markers.begin(), mj = markers.end();
        for(;mi!=mj;++mi) {
            if((*mi)->BitJeu & clonee->BitJeu) {
                (*mi)->BitJeu |= bjp->BitJeu;
            }
            ++i;
        }
	}

	// chargement diff�rent...
	if ( bjp->Cross != Con && !clonee)	/* si l'on clone un jeu, on ne modifie pas la sélection (élimination d'un bug yet-to-be-understood) */
	{
		// mise � jour de la liste des marqueurs actifs

		if ( MarkSelect != NULL ) delete [] MarkSelect;
		NbMS = nbm;
		MarkSelect = new int[nbm];
		j = 0;
        MarkerList::iterator mi=markers.begin(), mj=markers.end();
        for(;mi!=mj;++mi) {
            if(bitjeu&(*mi)->BitJeu) {
                MarkSelect[j++] = mi.index();
            }
        }
		/*for (i = 1; i <= NbMarqueur; i++)*/
			/*if (bitjeu & BitJeuMarq[i]) MarkSelect[j++] = i;*/

		// Le tas n'a plus de sens.

		int th = Heap->MaxHeapSize;
		Heap->Init(this, th);

	}

	NbJeu = numjeu;
	NbIndividu += nbi;

	/* aggrandit au besoin tous les Jeu[x]->IndMarq (pour tous les jeux sauf le présent) */
	for(int jeu=1;jeu<NbJeu;++jeu) {
		Jeu[jeu]->_updateIndMarq(NbMarqueur+1);
	}

	ArbreJeu->InitContribLogLike2pt();
}


// DL
//-----------------------------------------------------------------------------
// Clonage d'un jeu de données
//-----------------------------------------------------------------------------

/* FIXME : define cloning according to SRC and DEST CrossTypes :( naive CrossType coercion can't work without also changing class of cloned BJS (RH=>RHD RH=>RHE) FIXED */
/* FIXME : also, BJ::Imputation must either clone RHE into an RH or receive the clone as an argument */

BioJeu* CartaGene::ClonaJeu(int id, CrossType coerceInto, bool addToList) throw(BioJeu::NotImplemented) {
	BioJeu* ret=NULL;

	if(id>NbJeu) {	/* Start from 1 ! not 0 ! */
		char msg[256];
		print_out( "Dataset #%i doesn't exist.\n", id);
		return NULL;
	}

	BioJeu* b = Jeu[id];
#	define _CLONE(_cls) ret = new _cls(*dynamic_cast<_cls*>(b)); break
#	define _CLONE2(_cls, _cls2) ret = new _cls(*dynamic_cast<_cls2*>(b)); break
#	define _RHCLONE(_cls) \
	switch(coerceInto) {\
		case RH:  _CLONE2(BJS_RH, _cls);\
		case RHE: _CLONE2(BJS_RHE, _cls);\
		case RHD: _CLONE2(BJS_RHD, _cls);\
		default: throw BioJeu::NotImplemented();\
	};\
	ret->Cross = coerceInto;\
	break;

	if(coerceInto==Unk) {	/* Default to same class if undefined */
		coerceInto = b->Cross;
	}

	if(coerceInto==Unk) {
		coerceInto = b->Cross;
	}

	switch(b->Cross) {
		case BC:
		case RISelf:
		case RISib: 	_CLONE(BJS_BC);
		case IC:	_CLONE(BJS_IC_parallel);
		case RH:	_RHCLONE(BJS_RH);
		case RHD:	_RHCLONE(BJS_RHD);
		case RHE:	_RHCLONE(BJS_RHE);
		case BS:	_CLONE(BJS_BS);
		case Mge:
		case Mor:
		case Con:
		case Ordre:
		default:
					throw BioJeu::NotImplemented();
	};
	if(addToList) {
		PostTraitementBioJeu(ret, Jeu[id]);
	} else {
		ret->InitContribLogLike2pt();
	}
	return ret;
}


//LD
//-----------------------------------------------------------------------------
// Chargement des jeux de type ordre
//-----------------------------------------------------------------------------
// Param�tres :
//  - nom du fichier � traiter
// Valeur de retour : *biojeu particulier
//-----------------------------------------------------------------------------

BJS_OR * CartaGene::CharJeuOrdre(char *filename)
{
	FILE *fdata;

	int numjeu = NbJeu + 1;
	int bitjeu;
	int nbm, nbnm;
	int i, j, k;
	int debutchrom = 0;
	double coeff = 0.; //le poids des bp par rapport a la vraisemblance, 0 par defaut
	char coeffstring[256];

	CrossType Cross;

	BJS_OR * biojeures;

	int *indmarq;
	int *repmarq;
	int *chrom;
	int *chromex;
	int *position;

	char lineres[256];
	char nom[256], num[5];
	int pos;

	/*lineres = new char[256];*/

	fdata=fopen(filename, "r"); // no error test needed, done in "ProbeCrossType"
	if(!fgets(lineres, 256, fdata)) { // premiere ligne : d�j� trait�e par ProbCrossType
		/* shouldn't happen, let's just make the compiler happy */
	}

	nbnm = 0;
	nbm = 0;
	Cross = Ordre;

	// traitement de la deuxi�me ligne
	if ((i=fscanf(fdata, "%d %s", &nbm, (char*)&coeffstring)) < 2) {
		print_err( "%s : Syntax error on line 2, number of loci or break-point weight missing\n", filename);
		/*delete [] lineres;*/
		return 0;
	}
	coeff = atof(coeffstring);

	// Allocation m�moire pour les tableaux provisoires
	chrom = new int[nbm + 1];
	chromex = new int[nbm + 1];
	position = new int[nbm + 1];
	for (i=0;i<=nbm;i++) {
		chrom[i] = 0;
		chromex[i] = 0;
		position[i] = i;
	}

	// bitjeu est le masque bit a bit correspondant au jeu de donnee "numjeu"
	bitjeu = 1<<(numjeu - 1);

	//lecture du fichier � partir de la troisieme ligne
	j = 1;
    Marker* mrk;
    std::vector<int> indmarq_tmp;
	while (1)
	{
		// Tentative code to be compatible with order file with or without positions
		// Still does not work!!!!!!
		//        textline[0] = '\0';
		//        if (!fgets(textline, 1000, fdata)) {
		//  	break;
		//        }
		//        i = sscanf(textline, "%s %s %d", &num, &nom, &pos);
		i = fscanf(fdata, "%s %s %d", (char *)&num, (char *)&nom, &pos);

		if (i == EOF) {
			break;
		}
		if (i<2)
		{
			print_err( "%s : Syntax error on line number %d\n", filename, j);
			/*delete [] lineres;*/
			delete [] chrom;
			delete [] chromex;
			delete [] position;
			return NULL;
		}
		if (i==3) position[j] = pos;

        mrk = markers.add(nom);
        indmarq_tmp.push_back(markers.indexOf(mrk));
        if(mrk->BitJeu & bitjeu) {
            /* whine : duplicate marker name */
			print_out( "%s: Syntax error on marker, %s is not uniq.\n", filename,nom);
			delete [] chrom;
			delete [] chromex;
			delete [] position;
			fclose(fdata);
			return NULL;
        }
        mrk->BitJeu |= bitjeu;

//#ifdef __WIN32__
//        if (stricmp(num, "*"))
//#else
        if (strcasecmp(num, "*"))
            //#endif
        {
            // si on est en debut de chromosome
            if (debutchrom == 0) {
                debutchrom=j;
            }

            //si fin du chromosome, on peut remplir le tableau
            else {
                chromex[debutchrom] = atoi(num);
                chromex[j] = atoi(num);
                for (int s=debutchrom;s<=j;s++)
                {
                    chrom[s] = atoi(num);
                }
                debutchrom = 0;

            }
        }
		//printf("***marqex[%d] = %d\n", j, marqex[j]);
		j++;

	}


	fclose(fdata);

	// Allocation m�moire pour le tableau des indices de marqueurs
    NbMarqueur = markers.size()-1;

    /* ATTENTION FIXME? pour Ordre c'est IndMarq_bj2cg qu'on construit ! */
	indmarq = new int[NbMarqueur + 1];
	indmarq[0] = 0;
    for(i=0;i<indmarq_tmp.size();++i) {
        indmarq[i+1] = indmarq_tmp[i];
    }
    for(;i<NbMarqueur;++i) {
        indmarq[i+1]=0;
    }

	biojeures = new BJS_OR(this,
			numjeu,
			Cross,
			filename,
			nbm,
			bitjeu,
			indmarq,
			coeff,
			chrom,
			chromex,
			position);

	/*delete [] lineres;*/
	/*delete [] marqex;*/

	return biojeures;
}



//-----------------------------------------------------------------------------
// Affichage de tous les marqueurs
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------

void CartaGene::AffMarq()
{
	int i, j;

	print_out(  "\nMarkers :\n");
	print_out(  "--------:\n");
	print_out( "%3s %20s : %s", "Num", "Names", "Sets ");

	for (j = 0; j < NbJeu-1; j++) {
		print_out(  "    ");
	}
	print_out(  "Merges\n");

    MarkerList::iterator mi = markers.begin(), mj = markers.end();
    // We want to skip the first dummy element, hence the initial ++mi
    for(++mi;mi!=mj;++mi) {
		print_out( "%3d %20s :", mi.index(), mi.key().c_str());
		for (j = 0; j < NbJeu; j++)
		{
			if ( (*mi)->BitJeu & (1<<j) ) {
				print_out( " %3d", j + 1);
			}
			else {
				print_out( " %3s","");
			}
		}

		if ((*mi)->Merged) {
			print_out("  merged in %d, ",(*mi)->Merged);
		}

		if ((*mi)->Represents) {
			print_out("  merges ");
		}

		j = (*mi)->Represents;
		while (j != 0) {
			print_out( "%3d ",j);
			j = markers[j].Represents;
		}
		print_out(  "\n");
    }

#if 0
	for (i = 1; i <= NbMarqueur; i++)
	{
		print_out( "%3d %20s :", i, NomMarq[i]);
		for (j = 0; j < NbJeu; j++)
		{
			if ( BitJeuMarq[i] & 1<<j ) {
				print_out( " %3d", j + 1);
			}
			else {
				print_out( " %3s","");
			}
		}

		if (Merged[i]) {
			print_out("  merged in %d, ",Merged[i]);
		}

		if (Represents[i]) {
			print_out("  merges ");
		}

		j = Represents[i];
		while (j != 0) {
			print_out( "%3d ",j);
			j = Represents[j];
		}
		print_out(  "\n");
	}
#endif
}

//-----------------------------------------------------------------------------
// Affichage de tous les Jeux
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------

void CartaGene::AffJeu()
{

	print_out("\nData Sets :\n");

	print_out("----------:\n");

	print_out("%2s %16s %10s %11s %20s %11s %7s\n",
			"ID",
			"Data Type",
			"markers",
			"individuals",
			"filename",
			"constraints",
			"merging");

	for (int i = 1; i <= NbJeu; i++)
	{
		print_out("%2d ", i);

		switch (Jeu[i]->Cross)
		{
			case BC:
				print_out("%16s ", "f2 backcross");
				print_out("%10d %11d %20s\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;
			case RISelf:
				print_out("%16s ", "ri self");
				print_out("%10d %11d %20s\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu ));
				break;
			case RISib:
				print_out("%16s ", "ri sib");
				print_out("%10d %11d %20s\n", Jeu[i]->NbMarqueur, Jeu[i]->TailleEchant, Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;
			case IC:
				print_out("%16s ", "intercross");
				print_out("%10d %11d %20s\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;
			case RH:
				print_out("%16s ", "haploid RH");
				print_out("%10d %11d %20s\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;
			case RHD:
				print_out("%16s ", "diploid RH");
				print_out("%10d %11d %20s\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;
			case RHE:
				print_out("%16s ", "haploid RH with errors");
				print_out("%10d %11d %20s\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;

			case Mge:
				print_out("%16s ", "merged genetic");
				print_out("%10d %11d %11s %20s %3d %3d\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						"",
						"",
						((BJM_GE*)Jeu[i])->BJgauche->Id,
						((BJM_GE*)Jeu[i])->BJdroite->Id);
				break;
			case Mor:
				print_out("%16s ", "merged by order");
				print_out("%10d %11d %11s %20s %3d %3d\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						"",
						"",
						((BJM_OR*)Jeu[i])->BJgauche->Id,
						((BJM_OR*)Jeu[i])->BJdroite->Id);
				break;
			case Con:
				print_out("%16s ", "constraint");
				print_out("%10d %11s %20s %11d\n",
						Jeu[i]->NbMarqueur,
						"",
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu),
						Jeu[i]->TailleEchant);
				break;
			case BS:    // Added CN 11.12.05. Repetitive, yeah -- but it follows the code that was here first.
				print_out("%16s ", "backcross/self/intercross series");
				print_out("%10d %11d %20s\n",
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant,
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;
				// end of case added by CN 11.12.05
			case Ordre:
				print_out("%16s ", "order");
				print_out("%10d %11s %20s\n",
						Jeu[i]->NbMarqueur,
						"",
						Cart_basename(((BioJeuSingle *)Jeu[i])->NomJeu));
				break;
			default:
				break;
		}
	}
}

//-----------------------------------------------------------------------------
// retourne la liste des chaines d'info sur les jeux de donn�es
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------

/* TCL PRESENTATION LAYER */
char ** CartaGene::GetArbre()
{

	char* lineres;

	// allocation

	char** arbre =  new char*[NbJeu + 1];
	arbre[NbJeu] = NULL;

	for (int i = 1; i <= NbJeu; i++)
	{

		lineres = new char[1024];

		switch (Jeu[i]->Cross)
		{
			case BC:
				sprintf(lineres, "%d \"f2 backcross\" \"%s\" %d %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;

			case RISelf:

				sprintf(lineres, "%d \"ri self\" \"%s\" %d %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;

			case RISib:

				sprintf(lineres, "%d \"ri sib\" \"%s\" %d %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);

			case IC:

				sprintf(lineres, "%d \"intercross\" \"%s\" %d %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;
			case RH:

				sprintf(lineres, "%d \"radiated hybrid\" \"%s\" %d %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;
			case RHD:

				sprintf(lineres, "%d \"radiated hybrid diploid\" \"%s\" %d %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;
			case Con:

				sprintf(lineres, "%d \"constraint\" \"%s\" %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->TailleEchant);
				break;
			case Ordre:

				sprintf(lineres, "%d \"order\" \"%s\" %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur);
				break;
			case Mge:

				sprintf(lineres, "%d \"merged genetic\" %d %d %d %d",
						i,
						((BioJeuMerged *)Jeu[i])->BJgauche->Id,
						((BioJeuMerged *)Jeu[i])->BJdroite->Id,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;
			case Mor:

				sprintf(lineres, "%d \"merged by order\" %d %d %d %d",
						i,
						((BioJeuMerged *)Jeu[i])->BJgauche->Id,
						((BioJeuMerged *)Jeu[i])->BJdroite->Id,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;


			case BS:
				sprintf(lineres, "%d \"backcross/self/intercross series\" \"%s\" %d %d",
						i,
						((BioJeuSingle *)Jeu[i])->NomJeu,
						Jeu[i]->NbMarqueur,
						Jeu[i]->TailleEchant);
				break;

			default :

				break;
		}

		arbre[i-1] = lineres;
	}

	return (arbre);
}

//-----------------------------------------------------------------------------
// Affichage de l'�chantillon d'un jeu
//-----------------------------------------------------------------------------
// Param�tres :
// - le num�ro du jeu
//-----------------------------------------------------------------------------

void CartaGene::DumpEch(int nbjeu)
{
	print_out( "\nDump Data Set %2d :\n", nbjeu);
	print_out(  "-----------------:\n");
	Jeu[nbjeu]->DumpEch();
}

//-----------------------------------------------------------------------------
// Affichage des LOD 2pt d'un jeu
//-----------------------------------------------------------------------------
// Param�tres :
// - le num�ro du jeu
//-----------------------------------------------------------------------------

void CartaGene::DumpTwoPointsLOD(int nbjeu)
{
	print_out( "\nDump two points LOD %2d :\n", nbjeu);
	print_out(  "-----------------------:\n");
	Jeu[nbjeu]->DumpTwoPointsLOD();
}

//-----------------------------------------------------------------------------
// Affichage des LOD 2pt de la s�lection
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------

void CartaGene::PrintTwoPointsLOD()
{
	int i, j;

	// v�rification de base

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	print_out(  "\n          ");
	for (i = 0; i < NbMS; i++) {
		print_out( "%5d ",MarkSelect[i]);
	}
	flush_out();
	print_out(  "\n          ");

	for (i = 0; i < NbMS; i++) {
		print_out( "%5.5s ",markers.keyOf(MarkSelect[i]).c_str());
	}
	print_out(  "\n");
	flush_out();

	print_out(  "          ");
	for (i = 0; i < NbMS; i++) {
		print_out(  "------");
	}
	print_out(  "\n");
	flush_out();
	for (i = 0; i< NbMS; i++) {
		print_out( "%8.8s |",markers.keyOf(MarkSelect[i]).c_str());
		flush_out();
		for (j = 0; j < NbMS; j++)
			if (i == j)  {
				print_out(  "------");
			}
			else {
				print_out( "%5.1f ", GetTwoPointsLOD(MarkSelect[i], MarkSelect[j]));
			}
		print_out(  "\n");
		flush_out();
	}
}

//-----------------------------------------------------------------------------
// Affichage des LOD 2pt de la s�lection
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------
void CartaGene::DumpDouble()
{
	int i, j,count = 0;

	print_out(  "\nPossible double markers:");
	for (i = 0; i< NbMS-1; i++)
		for (j = i; j < NbMS; j++)
			if ((i != j) &&
					ArbreJeu->Compatible(MarkSelect[i], MarkSelect[j]) &&
					GetTwoPointsLOD(MarkSelect[i], MarkSelect[j])> 0.0) {
				if (!count) {
					print_out(  "\n\n");
				}
				print_out( "    %15s = %-15s [%.1f]\n",markers.keyOf(MarkSelect[i]).c_str(), markers.keyOf(MarkSelect[j]).c_str(),GetTwoPointsLOD(MarkSelect[i], MarkSelect[j]));
				count++;
			}
	if (!count) {
		print_out(  " none.");
	}
	print_out(  "\n");
	flush_out();
}

/* DL
 * replace LISTM and LODS by a single vector to reduce memory overhead
 */
class MrkDoubles {
	private:
		struct MDbl {
			int m1;
			int m2;
			double LOD;
			MDbl(int a, int b, double c) : m1(a), m2(b), LOD(c) {}
			static bool compare(struct MDbl*a, struct MDbl*b) {
				return a->LOD > b->LOD;
			}
		};
		/*static int cmp_LODS(const struct MDbl*a, const struct MDbl*b) {*/
		/*if(a->LOD < b->LOD) { return 1; }*/
		/*if(a->LOD > b->LOD) { return -1; }*/
		/*return 0;*/
		/*}*/
		CartaGene* c;
		typedef std::vector<MDbl*> DblVec;
		DblVec dbls;
		Parallel::Mutex mutex;
		Utils::ProgressDisplay disp;
        double lod_threshold;
	public:
		MrkDoubles(CartaGene*_, double lodthres)
			: c(_), dbls(), mutex(), disp(c->NbMS-1), lod_threshold(lodthres)
		{
			dbls.reserve(c->NbMS);
		}
		~MrkDoubles() {
			DblVec::iterator i=dbls.begin(), j=dbls.end();
			while(i!=j) { delete *i; ++i; }
			dbls.clear();
		}
		void add(int m1, int m2, double LOD) {
			mutex.lock();
			dbls.push_back(new MDbl(m1, m2, LOD));
			mutex.unlock();
		}
		void sort() {
			mutex.lock();
			/*std::cout << "before sort" << std::endl;*/
			std::stable_sort(dbls.begin(), dbls.end(), MDbl::compare);
			/*std::cout << "after sort" << std::endl;*/
			mutex.unlock();
		}
		operator char**() const {
			/*std::cout << "before conversion to char**" << std::endl;*/
			char**ret = new char*[dbls.size()+1];
			DblVec::const_iterator i=dbls.begin(), j=dbls.end();
			char**ptr = ret;
			while(i!=j) {
				std::stringstream tmp;
				tmp << (*i)->m1 << ' ' << (*i)->m2 /* << ' ' << (*i)->LOD */;
				*ptr = new char[tmp.str().length()+1];
				strcpy(*ptr, tmp.str().c_str());
				++ptr;
				++i;
			}
			*ptr=NULL;
			/*std::cout << "after conversion to char**" << std::endl;*/
			return ret;
		}

		/* perform one iteration of the outer loop so we can parallelize stuff */
		void operator()(int i) {
			double lod;
			Utils::Tick t = Utils::Tick::now();
			for (int j = i+1; j < c->NbMS; j++) {
				if (c->ArbreJeu->Compatible(c->MarkSelect[i], c->MarkSelect[j]) &&
						(lod=c->GetTwoPointsLOD(c->MarkSelect[i], c->MarkSelect[j])) > lod_threshold)
				{
					add(c->MarkSelect[i], c->MarkSelect[j], lod);
				}
			}
			disp.step_done((Utils::Tick::now()-t).seconds());
		}
};

/* TCL PRESENTATION LAYER | MUST ONLY CHANGE RETURN TYPE HERE. */
char ** CartaGene::GetDouble(double lod_threshold)
{
	MrkDoubles md(this, lod_threshold);
	/*double total_time = */Parallel::Range<Parallel::Timer>().execute_slices(md, 0, NbMS-1, 10).time();
	md.sort();
	return md;
}

#if 0
char** LISTM = NULL;
double *LODS = NULL;
int cmp_LODS(const void *p1, const void *p2)
{
	if (LODS[((char **) p1)-LISTM] < LODS[((char **) p2)-LISTM]) return 1;
	else if (LODS[((char **) p1)-LISTM] > LODS[((char **) p2)-LISTM]) return -1;
	else return 0;
}


char ** CartaGene::GetDouble()
{
	int i, j,count = 0;
	char* lineres;
	LISTM = new char*[NbMS*NbMS + 1];
	LISTM[NbMS*NbMS] = NULL;
	if (!LODS) delete [] LODS;
	LODS = new double[NbMS*NbMS + 1];

	for (i = 0; i< NbMS-1; i++)
		for (j = i; j < NbMS; j++)
			if ((i != j) &&
					ArbreJeu->Compatible(MarkSelect[i], MarkSelect[j]) &&
					GetTwoPointsLOD(MarkSelect[i], MarkSelect[j])> 0.0) {
				lineres = new char[128];
				/* DL
				 * FIXME (but this is obsolete code) the order seems wrong there, when displaying the LOD along the markers it seems totally unordered, maybe because of cmp_LODS...
				 */
				/*sprintf(lineres,"%d %d %lf",MarkSelect[i], MarkSelect[j], GetTwoPointsLOD(MarkSelect[i], MarkSelect[j]));*/
				sprintf(lineres,"%d %d",MarkSelect[i], MarkSelect[j]);
				LODS[count] = GetTwoPointsLOD(MarkSelect[i], MarkSelect[j]);
				LISTM[count] = lineres;
				count++;
			}
	qsort(LISTM, count, sizeof(char *), cmp_LODS);
	LISTM[count] = NULL;
	return (LISTM);
}
#endif

//-----------------------------------------------------------------------------
// Affichage des Dist 2pt de la s�lection
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------

void CartaGene::PrintTwoPointsDist(char unit[2])
{

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	switch(unit[0])
	{
		case 'h': break;
		case 'k': break;
		default:
				  print_err(  "Error : UnitFlag should be 'k' or 'h'.\n");
				  return;
	}

	print_out(  "\nPrint two points distance matrices of the loci selection :\n");
	print_out(  "---------------------------------------------------------:\n");
	flush_out();

	ArbreJeu->PrintTwoPointsDist(unit);

	flush_out();
}

//-----------------------------------------------------------------------------
// Affichage des FR 2pt de la s�lection
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------

void CartaGene::PrintTwoPointsFR()
{
	int i, j;

	// v�rification de base

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	print_out(  "\nPrint two-points recombination-fraction matrices of the locus selection :\n");
	print_out(  "---------------------------------------------------------------------------:\n");
	flush_out();
	print_out(  "\n       ");

	for (i = 0; i < NbMS; i++) {
		print_out( "%5.5s ",markers.keyOf(MarkSelect[i]).c_str());
	}
	print_out(  "\n");
	flush_out();

	print_out(  "       ");
	for (i = 0; i < NbMS; i++)  {
		print_out(  "------");
	}
	print_out(  "\n");
	flush_out();
	for (i = 0; i< NbMS; i++) {
		print_out( "%5.5s |",markers.keyOf(MarkSelect[i]).c_str());
		flush_out();
		for (j = 0; j < NbMS; j++)
			if (i == j)  {
				print_out(  "------");
			}
			else {
				print_out( "%5.3f ", GetTwoPointsFR(MarkSelect[i], MarkSelect[j]));
			}
		print_out(  "\n");
		flush_out();
	}
	flush_out();
}


//-----------------------------------------------------------------------------
// Affichage des FR d'un jeu
//-----------------------------------------------------------------------------
// Param�tres :
// - le num�ro du jeu
//-----------------------------------------------------------------------------

void CartaGene::DumpTwoPointsFR()
{
	print_out(  "Dump two points :\n");

	ArbreJeu->DumpTwoPointsFR();

	/*  Jeu[nbjeu]->DumpTwoPointsFR();*/
}

//-----------------------------------------------------------------------------
// Calcul et Affichage des groupes d'un jeu
//-----------------------------------------------------------------------------
// Param�tres :
// - disthres seuil de la distance
// - lodthres seuil du lod
// Valeur de retour : le nombre de goupes trouv�s
//-----------------------------------------------------------------------------

int CartaGene::Groupe(double disthres, double lodthres)
{

	if (ArbreJeu == NULL)
	{
		print_err(  "Error : No data set loaded..\n");
		return -1;
	}

	return ArbreJeu->Groupe(disthres, lodthres);
}


//-----------------------------------------------------------------------------
// Affichage de tous l'�chantillon
//-----------------------------------------------------------------------------
// Param�tres :
// - Les indices des deux jeux � fusionner
// -// Valeur de retour : Chaine de car. contenant
// - le num�ro du jeu
// - le type du jeu
// - le nombre de marqeurs
// - le nombre d'individus
// ou une chaine vide en cas d'erreur.
//-----------------------------------------------------------------------------

char *CartaGene::MerGen(int gauche, int droite)
{

	char *lineres ;
	int i,j;

	CrossType Cross;
	int bitjeu;
	int nbm = 0;
	BioJeu **jeufus;
	char *vide;

	// pour retourner une liste vide(voir conventions)
	vide = new char[1];
	vide[0] = '\0';

	// test de l'existence des jeux � fusionner

	if (gauche > NbJeu || gauche < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", gauche);
		return vide;
	}

	if (droite > NbJeu || droite < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", droite);
		return vide;
	}

	// interdiction de merger g�n�tiquement des merg�s sur l'ordre.

	if (Jeu[gauche]->Cross == Mor || Jeu[gauche]->Cross == Con)
	{
		print_err( "Error on Data Set %d : The type is not allowed.\n", gauche);
		return vide;
	}

	if (Jeu[droite]->Cross == Mor || Jeu[droite]->Cross == Con)
	{
		print_err( "Error on Data Set %d : The type merged is not allowed.\n", droite);
		return vide;
	}

	// RH ou Pas

	if (Jeu[gauche]->HasRH() != Jeu[droite]->HasRH())
	{
		print_err(  "Error : Merging radiated hybrid with another type is not allowed.\n");
		return vide;
	}

	if (Jeu[droite]->HasRH() && (Jeu[droite]->Cross != Jeu[gauche]->Cross))
	{
		print_err( "Error on Data Set %d : Cannot merge diploid and haploid data.\n", droite);
		return vide;
	}

	//RI ou pas

	if (Jeu[gauche]->HasRI() || Jeu[droite]->HasRI())
		if (Jeu[gauche]->HasRI() != Jeu[droite]->HasRI()) {
			print_err(  "Error : Merging RISib or RISelf with another type is not allowed.\n");
			return vide;
		}

	//if (Jeu[gauche]->HasRI() && (Jeu[droite]->Cross != Jeu[gauche]->Cross))
	//  {
	//    sprintf(bouf,  "Error : Merging RISib with RISelf is not allowed.\n");
	//    perr(bouf);
	//    return vide;
	//  }

	lineres = new char[256];
	NbJeu++;
	Cross = Mge;
	bitjeu = 1<<(NbJeu - 1);

	for (i = 1; i <= NbMarqueur; i++)
		if ((markers[i].BitJeu & Jeu[gauche]->BitJeu) ||
				(markers[i].BitJeu & Jeu[droite]->BitJeu))
		{
			markers[i].BitJeu |= bitjeu;
			nbm++;
		}

	ArbreJeu = new BJM_GE(this,
			NbJeu,
			Cross,
			nbm,
			bitjeu,
			Jeu[gauche],
			Jeu[droite]);

	// Allocation m�moire pour le tableau provisoire des champs de bit
	jeufus = new BioJeuPtr[NbJeu + 1];
	jeufus[0] = NULL;

	for (i = 1; i < NbJeu; i++) jeufus[i] = Jeu[i];
	jeufus[NbJeu] = ArbreJeu;

	delete Jeu;
	Jeu = jeufus;

	// mise � jour de la liste des marqueurs actifs

	delete MarkSelect;
	NbMS = nbm;
	MarkSelect = new int[nbm];
	j = 0;
	for (i = 1; i <= NbMarqueur; i++)
		if ((markers[i].BitJeu & Jeu[gauche]->BitJeu) ||
				(markers[i].BitJeu & Jeu[droite]->BitJeu))
			MarkSelect[j++] = i;

    /* TCL PRESENTATION LAYER */
	sprintf(lineres, "%d merged genetic %d %d", NbJeu, nbm,
			Jeu[gauche]->TailleEchant + Jeu[droite]->TailleEchant);

	delete vide;

	// Le tas n'a plus de sens.

	int th = Heap->MaxHeapSize;
	Heap->Init(this, th);

	return lineres;

}
//-----------------------------------------------------------------------------
// Affichage de tous l'�chantillon
//-----------------------------------------------------------------------------
// Param�tres :
// - Les indices des deux jeux � fusionner
// -// Valeur de retour : Chaine de car. contenant
// - le num�ro du jeu
// - le type du jeu
// - le nombre de marqeurs
// - le nombre d'individus
// ou une chaine vide en cas d'erreur.
//-----------------------------------------------------------------------------

char *CartaGene::MergOr(int gauche, int droite)
{

	char *lineres ;
	int i,j;

	CrossType Cross;
	int bitjeu;
	int nbm = 0;
	BioJeu **jeufus;
	char *vide;

	// pour retourner une liste vide(voir conventions)
	vide = new char[1];
	vide[0] = '\0';

	// test de l'existence des jeux � fusionner

	if (gauche > NbJeu || gauche < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", gauche);
		return vide;
	}

	if (droite > NbJeu || droite < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", droite);
		return vide;
	}

	lineres = new char[256];
	NbJeu++;
	Cross = Mor;
	bitjeu = 1<<(NbJeu - 1);

	for (i = 1; i <= NbMarqueur; i++)
		if ((markers[i].BitJeu & Jeu[gauche]->BitJeu && Jeu[gauche]->Cross != Con) ||
				(markers[i].BitJeu & Jeu[droite]->BitJeu && Jeu[droite]->Cross != Con))
		{
			markers[i].BitJeu |= bitjeu;
			nbm++;
		}

	ArbreJeu = new BJM_OR(this,
			NbJeu,
			Cross,
			nbm,
			bitjeu,
			Jeu[gauche],
			Jeu[droite]);

	// Allocation m�moire pour le tableau provisoire des champs de bit
	jeufus = new BioJeuPtr[NbJeu + 1];
	jeufus[0] = NULL;

	for (i = 1; i < NbJeu; i++) jeufus[i] = Jeu[i];
	jeufus[NbJeu] = ArbreJeu;

	delete [] Jeu;
	Jeu = jeufus;

	// mise � jour de la liste des marqueurs actifs

	delete [] MarkSelect;
	NbMS = nbm;
	MarkSelect = new int[nbm];
	j = 0;
	for (i = 1; i <= NbMarqueur; i++)
		if (markers[i].BitJeu & bitjeu)
			MarkSelect[j++] = i;

    /* TCL PRESENTATION LAYER */
	sprintf(lineres, "%d merged by order %d %d", NbJeu, nbm,
			Jeu[gauche]->TailleEchant + Jeu[droite]->TailleEchant);

	delete [] vide;

	// Le tas n'a plus de sens.

	int th = Heap->MaxHeapSize;
	Heap->Init(this, th);

	return lineres;

}

//-----------------------------------------------------------------------------
// Acc�s � une valeur LOD score 2pt.
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------
double CartaGene::GetTwoPointsLOD(int m1, int m2) const
{
	return ArbreJeu->GetTwoPointsLOD(m1, m2);
}

//-----------------------------------------------------------------------------
// Acc�s � la fraction de recombinaison 2pt.
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------
double CartaGene::GetTwoPointsFR(int m1, int m2) const
{
	return ArbreJeu->GetTwoPointsFR(m1, m2);
}

//-----------------------------------------------------------------------------
// Acc�s � la distance 2pt.
//-----------------------------------------------------------------------------
// Param�tres :
//-----------------------------------------------------------------------------
double CartaGene::GetTwoPointsDH(int m1, int m2) const
{
	return ArbreJeu->GetTwoPointsDH(m1, m2);
}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par estimation 2point.
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// - seuil de convergence
// Valeur de retour : la vraisemblance maximum
//-----------------------------------------------------------------------------

void CartaGene::SetEM2pt(int mode)
{
	EM2pt = mode;
}

double CartaGene::Compute2pt(Carte *map)
{
	double res = ArbreJeu->ContribLogLike2pt(map->ordre[0]);
	for (int m = 0; m < map->NbMarqueur - 1; m++) {
		res += ArbreJeu->ContribLogLike2pt(map->ordre[m], map->ordre[m+1]);
	}
	res += ArbreJeu->ContribLogLike2pt(map->ordre[map->NbMarqueur - 1]);
	map->Converged = ArbreJeu->Epsilon2;
	map->coutEM = -res;
	return -res;
}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// - seuil de convergence
// Valeur de retour : la vraisemblance maximum
//-----------------------------------------------------------------------------

double CartaGene::ComputeEM(Carte *map)
{
	if (EM2pt) {
        /*std::cout << "EM2pt" << std::endl;*/
		return Compute2pt(map);
	} else {
        /*std::cout << "EM" << std::endl;*/
		return ArbreJeu->ComputeEM(map);
	}
}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//  Une interface plus sophistiquee:
//  On converge jusqu'a epsilon1. Si la vraisemblance est inferieure a
//  threshold on laisse tomber, sinon on converge a epsilon2
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// - niveau a partir duquel on convergera a epsilon2
// - premier seuil de convergence (epsilon1)
// - second seuil de convergence (epsilon2)
// Valeur de retour : la vraisemblance maximum.
//-----------------------------------------------------------------------------

double CartaGene::ComputeEMS(Carte *map,
		double threshold)
{
	if (EM2pt) {
		return Compute2pt(map);
	} else {
		return ArbreJeu->ComputeEMS(map, threshold);
	}
}

//-----------------------------------------------------------------------------
//  Affichage simple s�lectif d'une carte
// Seuls les marqueurs du g�notype sont affich�s
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::PrintMap(Carte *map) const
{
	print_out( "\nMap %2d : log10-likelihood = %8.2f\n",
			map->Id,
			map->coutEM);
	print_out(  "-------:\n");
	print_out( "%4s : Marker List ...\n", "Set");
	ArbreJeu->PrintMap(map);
}

//-----------------------------------------------------------------------------
//  Affichage d�taill� d'une carte
// Seuls les marqueurs du g�notype sont affich�s
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::PrintDMap(Carte *map, int envers, Carte *mapref) const
{
	print_out( "\nMap %2d : log10-likelihood = %8.2f, log-e-likelihood = %8.2f\n",
			map->Id, map->coutEM, map->coutEM * log((double) 10));
	print_out(  "-------:\n");
	ArbreJeu->PrintDMap(map, envers, mapref);
}

//-----------------------------------------------------------------------------
// Affichage d�taill� des deltas carteFW / autres marqueurs
//-----------------------------------------------------------------------------
// Param�tres :
// - La structure des deltas
// - la carte FW
// - Le nombre de marqueur de la carte FW
// - Le nombre de marqueur a testee
// Valeur de retour :
//----------------------------------------------------------------------------

void CartaGene::PrintVMap(struct testmartin *pstruct, Carte *data, double *distd, int nbm, int nbtas, double AM)
{
	// nbm = nb de marqueur dans le framework
	// nbtas = nb de marqueur a tester
	int i,j;
	int avantbest,apresbest; // avantbest apresbest   num marqueur avant,apres la meilleur position
	unsigned int maxl=0;
	char form[128];
	const char *formb;
	double Lodapres,Lodavant,Dhapres,Dhavant,Dhbest,Dhprop;

	formb = " ";

	for (i = 0; i < nbtas; i++)
	{
		j = markers[pstruct[i].numero].Represents;
		while (j != 0)
		{
            if(markers.keyOf(j).size() > maxl)
                maxl = markers.keyOf(j).size();
			/*if (strlen(NomMarq[j]) > maxl)*/
				/*maxl = strlen(NomMarq[j]);*/
			j = markers[j].Represents;
		}
        if(markers.keyOf(pstruct[i].numero).size() > maxl)
            maxl = markers.keyOf(pstruct[i].numero).size();
		/*if (strlen(NomMarq[pstruct[i].numero]) > maxl)*/
			/*maxl = strlen(NomMarq[pstruct[i].numero]);*/
	}

	maxl = (maxl < 5 ? 5 : maxl);

	sprintf(form, " %c%d%c |",'%', maxl, 's');

	// 1ere ligne
	print_out( form,formb);
	for (i = 0; i < nbm; i++)
		if ((data->ordre[i] - (data->ordre[i]%100))/100) {
			print_out( " %d", (data->ordre[i] - (data->ordre[i]%100))/100);
		}
		else {
			print_out(  "  ");
		}

	print_out(  "  |\n");
	// 2eme ligne
	print_out( form,formb);

	for (i = 0; i < nbm ; i++)
		if ((data->ordre[i]%100 - ((data->ordre[i]%100)%10))/10 || data->ordre[i] > 99) {
			print_out( " %d", (data->ordre[i]%100 - ((data->ordre[i] %100)%10))/10);
		}
		else {
			print_out(  "  ");
		}

	print_out(  "  |     Lod2pt         Dist2pt\n");
	// 3eme ligne
	print_out( form,formb);


	for (i = 0; i < nbm; i++) {
		print_out( " %d", data->ordre[i]%10);
	}

	print_out(  "  |  Left<-M->Right Left<-M->Right | 0->N    N->M | Weight Nb:W<AM  Id  | Name\n");
	// 4eme ligne  les tirets
	sprintf(form, "%c%d%c--|",'%', maxl, 's');
	print_out( form,formb);

	for (i = 0; i < nbm; i++) {
		print_out(  "--");
	}

	print_out(  "--|--------------------------------|--------------|---------------------|-------\n");

	for (i = 0; i < nbtas; i++)
	{
		sprintf(form, " %c%d%c |",'%', maxl, 's');
		/*sprintf(bouf, form, NomMarq[pstruct[i].numero]);*/
		print_out( form, markers.keyOf(pstruct[i].numero).c_str());

		for (j = 0; j < nbm+1 ; j++)
		{
			if ((markers[pstruct[i].numero].BitJeu & ArbreJeu->BitJeu) && (pstruct[i].pdelta[j] <= AM))
			{
				if (j == pstruct[i].meilleur)
				{
					print_out(  "+ ");
				}
				else
				{
					print_out( "%d ",pstruct[i].pdelta[j]);
				}
			}
			else
			{
				print_out(  "  ");
			}

			if (j == pstruct[i].meilleur)
			{
				avantbest=data->ordre[j-1];
				apresbest=data->ordre[j];
				Lodavant=0.0;Lodapres=0.0;
				Dhapres=0.0;
				Dhavant=0.0;
				Dhbest=0.0;
				Dhprop=0.0;
				// Bout Gauche
				if ( j == 0 )
				{
					avantbest=0;
					// marqueur apres pas compatible
					if (!((markers[apresbest].BitJeu & ArbreJeu->BitJeu) && (markers[pstruct[i].numero].BitJeu & ArbreJeu->BitJeu)))
					{
						apresbest=0;
						sprintf(form, "|    -       -        -     -    | L End    NA< |  %4d  %4d     %-4d",pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
					}
					// marqueur apres compatible
					else
					{
						Lodapres=GetTwoPointsLOD(apresbest,pstruct[i].numero);
						Dhapres=(100 * GetTwoPointsDH(apresbest,pstruct[i].numero));
						Dhprop=Dhapres;
						// si sur marqueur apres 0->N=Dhapres et N->M=0
						if ( Dhapres == 0)
							sprintf(form, "|    -    %6.2f      -  %5.1f   | %5.1f   0    |  %4d  %4d     %-4d",Lodapres,Dhapres,Dhprop,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
						else
							sprintf(form, "|    -    %6.2f      -  %5.1f   |   -    %5.1f |  %4d  %4d     %-4d",Lodapres,Dhapres,Dhprop,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
					}
				}
				// Pas Bout Gauche
				else
					// Bout Droit
					if ( j == nbm)
					{
						apresbest=0;
						// marqueur avant pas compatible
						if (!((markers[avantbest].BitJeu & ArbreJeu->BitJeu) && (markers[pstruct[i].numero].BitJeu & ArbreJeu->BitJeu)))
						{
							avantbest=0;
							sprintf(form, "|    -       -        -     -    | R End    NA> |  %4d  %4d     %-4d",pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
						}
						// marqueur avant compatible
						else
						{
							Lodavant=GetTwoPointsLOD(avantbest,pstruct[i].numero);
							Dhavant=(100 * GetTwoPointsDH(avantbest,pstruct[i].numero));
							Dhbest=distd[j-1];
							Dhprop=Dhavant;
							// si sur marqueur avant 0->N=Dhbest et N->M=0
							if ( Dhavant == 0)
								sprintf(form, "| %6.2f     -     %5.1f    -    | %5.1f   0    |  %4d  %4d     %-4d",Lodavant,Dhavant,Dhbest,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
							else
								sprintf(form, "| %6.2f     -     %5.1f    -    | %6.1f %5.1f |  %4d  %4d     %-4d",Lodavant,Dhavant,Dhbest,Dhprop,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
						}
					}
				// Ni Bout Gauche Ni Bout Droit
					else
					{
						// marqueur avant pas compatible
						if (!((markers[avantbest].BitJeu & ArbreJeu->BitJeu) && (markers[pstruct[i].numero].BitJeu & ArbreJeu->BitJeu)))
						{
							avantbest=0;
							// marqueur apres pas compatible
							if (!((markers[apresbest].BitJeu & ArbreJeu->BitJeu) && (markers[pstruct[i].numero].BitJeu & ArbreJeu->BitJeu)))
							{
								apresbest=0;
								sprintf(form, "|    -       -        -     -    |   -       -  |  %4d  %4d     %-4d",pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
							}
							// marqueur apres compatible
							else
							{
								Lodapres=GetTwoPointsLOD(apresbest,pstruct[i].numero);
								Dhapres=(100 * GetTwoPointsDH(apresbest,pstruct[i].numero));
								Dhbest=distd[j];                                           // ditance cumulee du marqueur apres
								sprintf(form, "|   nc    %6.2f     nc  %5.1f   | %6.1f  NA<  |  %4d  %4d     %-4d",Lodapres,Dhapres,Dhbest,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
							}
						}
						// marqueur avant compatible
						else
						{
							// marqueur apres pas compatible
							if (!((markers[apresbest].BitJeu & ArbreJeu->BitJeu) && (markers[pstruct[i].numero].BitJeu & ArbreJeu->BitJeu)))
							{
								Lodavant=GetTwoPointsLOD(avantbest,pstruct[i].numero);
								Dhavant=(100 * GetTwoPointsDH(avantbest,pstruct[i].numero));
								Dhbest=distd[j-1];                                         // ditance cumulee du marqueur avant
								sprintf(form, "| %6.2f    nc     %5.1f   nc    | %6.1f   NA> |  %4d  %4d     %-4d",Lodavant,Dhavant,Dhbest,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
							}
							// marqueur apres compatible
							else
							{
								Lodapres=GetTwoPointsLOD(apresbest,pstruct[i].numero);
								Dhapres=(100 * GetTwoPointsDH(apresbest,pstruct[i].numero));
								Lodavant=GetTwoPointsLOD(avantbest,pstruct[i].numero);
								Dhavant=(100 * GetTwoPointsDH(avantbest,pstruct[i].numero));
								Dhbest=distd[j-1];                                         // distance cumulee du marqueur avant
								Dhprop=((distd[j]-distd[j-1])*Dhavant)/(Dhavant+Dhapres);  // marqueur m  (dist(n,n+1)*dist(n,m)) / (dist(n,m) + dist(m,n+1))
								// si sur marqueur avant 0->N=Dhbest et N->M=0
								if ( Dhavant == 0)
									sprintf(form, "| %6.2f  %6.2f   %5.1f %5.1f   | %6.1f  0    |  %4d  %4d     %-4d",Lodavant,Lodapres,Dhavant,Dhapres,Dhbest,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
								else
									// si sur marqueur apres 0->N=distd[j] et N->M=0
									if ( Dhapres == 0)
										sprintf(form, "| %6.2f  %6.2f   %5.1f %5.1f   | %6.1f  0    |  %4d  %4d     %-4d",Lodavant,Lodapres,Dhavant,Dhapres,distd[j],pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
									else
										sprintf(form, "| %6.2f  %6.2f   %5.1f %5.1f   | %6.1f %5.1f |  %4d  %4d     %-4d",Lodavant,Lodapres,Dhavant,Dhapres,Dhbest,Dhprop,pstruct[i].poids,pstruct[i].nbcas,pstruct[i].numero);
							}
						}
					}
			} // Fin if j == pstruct
		} // Fin for j=

		/*sprintf(bouf, form);*/
		/*strcpy(bouf, form);*/
		/*pout(bouf);*/
        print_out(form);
		sprintf(form, "| %c-%d%c\n",'%', maxl, 's');
		print_out( form, markers.keyOf(pstruct[i].numero).c_str());
	}
}




//-----------------------------------------------------------------------------
//  Affichage simple s�lectif d'une carte
// Seuls les marqueurs du g�notype sont affich�s
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::PrintMap(int nbmap) const
{

	if (Heap->HeapSize == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return;
	}

	if ( nbmap < 0 || nbmap >= Heap->HeapSize )
	{
		print_err(  "Error : This map does not exist.\n");
		return;
	}

	print_out( "\nMap %2d : log10-likelihood = %8.2f\n",
			(Heap->MapFromId(nbmap))->Id,
			(Heap->MapFromId(nbmap))->coutEM);
	print_out(  "-------:\n");
	print_out( "%4s : Marker List ...\n", "Set");
	ArbreJeu->PrintMap(Heap->MapFromId(nbmap));
}


// Alloue une chaine a l'octet pres pour stocker un flottant %.2f (utilise new[])
char* _float2str(float f) {
	static char buf[32];
	char* ret;
	sprintf(buf, "%.2f", f);
	ret = new char[strlen(buf)+1];
	strcpy(ret, buf);
	return ret;
}


// Alloue une chaine a l'octet pres pour stocker un entier %d (utilise new[])
char* _int2str(int f) {
	static char buf[32];
	char* ret;
	sprintf(buf, "%d", f);
	ret = new char[strlen(buf)+1];
	strcpy(ret, buf);
	return ret;
}



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// Valeur de retour : une liste
//-----------------------------------------------------------------------------


/* TCL PRESENTATION LAYER */
char *** CartaGene::GetMap(const char unit[2], int nbmap) const
{

	// pour retourner une liste vide(voir conventions)
	//char * vide = new char[1];
	//vide[0] = '\0';

	switch(unit[0]) {
		case 'h': break;
		case 'k': break;
		default:
				  print_err(  "Error : UnitFlag should be 'k' or 'h'.\n");
				  return NULL;
	}

	if (Heap->HeapSize == 0) {
		print_err(  "Error : Empty heap.\n");
		return NULL;
	}

	if ( nbmap < 0 || nbmap >= Heap->HeapSize ) {
		print_err(  "Error : This map does not exist.\n");
		return NULL;
	}

	char*** lm = ArbreJeu->GetMap(unit, Heap->MapFromId(nbmap));
	int i = 0;

	// d�compte des cartes

	while (lm[i++] != NULL)
		;

	// allocation

	char*** lmn =  new char**[i + 1];
	lmn[i] = NULL;

	char ** log = new char*[3];
	log[2] = NULL;

	// remplissage du log g�n�ral

	/*char * temp = new char[10];*/
	/*sprintf(temp, "%.2f",*/
	/*(Heap->MapFromId(nbmap))->coutEM);*/
	log[1] = _float2str((Heap->MapFromId(nbmap))->coutEM);

	// remplissage de l'indice de la carte

	/*temp = new char[10];*/
	/*sprintf(temp, "%d", nbmap);*/
	log[0] = _int2str(nbmap);

	lmn[0] = log;

	i = 0;
	// remplissage des cartes
	while (lm[i] != NULL) {
		lmn[i+1] =  lm[i];
		lm[i++] = NULL;
	}

	delete [] lm;

	return(lmn);

}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// Valeur de retour : une liste
//-----------------------------------------------------------------------------


int CartaGene::GetOrdMap(int nbmap, intPtr *vm) const
{

	*vm = new int[NbMS+1];
	// pour compter avoir la taille du groupe directement dans le vecteur.
	(*vm)[NbMS] = 0;

	if (Heap->HeapSize == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return -1;
	}

	if ( nbmap < 0 || nbmap >= Heap->HeapSize )
	{
		print_err(  "Error : This map does not exist.\n");
		return -1;
	}

	// pour stocker.
	for (int i = 0; i < NbMS; i++)
		(*vm)[i] = Heap->MapFromId(nbmap)->ordre[i];

	return NbMS;
}

//-----------------------------------------------------------------------------
//  d'une carte
// Seuls les marqueurs du g�notype sont affich�s
//-----------------------------------------------------------------------------
// Param�tres :
// - l'unit�
// - une carte
// Valeur de retour : une liste
//-----------------------------------------------------------------------------

/* TCL PRESENTATION LAYER */
char **** CartaGene::GetHeap(char unit[2], int nbmap) const
{

	// pour retourner une liste vide(voir conventions)
	//char * vide = new char[1];
	//vide[0] = '\0';

	switch(unit[0]) {
		case 'h': break;
		case 'k': break;
		default:
				  print_err(  "Error : UnitFlag should be 'k' or 'h'.\n");
				  return NULL;
	}

	if (Heap->HeapSize == 0) {
		print_err(  "Error : Empty heap.\n");
		return NULL;
	}

	if ( nbmap == 0 || nbmap > Heap->HeapSize )
		nbmap = Heap->HeapSize;

	// allocation

	char**** lmn =  new char***[nbmap + 1];
	lmn[nbmap] = NULL;

	int *tabid = Heap->IdSorted();

	for (int i = 0; i < nbmap; i++)
		lmn[i] = GetMap(unit, tabid[i]);

	delete [] tabid;

	return(lmn);
}

//-----------------------------------------------------------------------------
//  Affichage d�taill� d'une carte
// Seuls les marqueurs du g�notype sont affich�s
//-----------------------------------------------------------------------------
// Param�tres :
// - une carte
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::PrintDMap(int nbmap, int envers) const
{

	if (Heap->HeapSize == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return;
	}

	if ( nbmap < 0 || nbmap >= Heap->HeapSize )
	{
		print_err(  "Error : This map does not exist.\n");
		return;
	}

	print_out( "\nMap %2d : log10-likelihood = %8.2f, log-e-likelihood = %8.2f\n",
			(Heap->MapFromId(nbmap))->Id,
			(Heap->MapFromId(nbmap))->coutEM,
			(Heap->MapFromId(nbmap))->coutEM * log((double) 10));
	print_out(  "-------:\n");
	ArbreJeu->PrintDMap(Heap->MapFromId(nbmap), envers, Heap->MapFromId(nbmap));
}


//-----------------------------------------------------------------------------
// R�cup�ration d''un groupe sous la forme d''un vecteur d''indice.
//-----------------------------------------------------------------------------
// Param�tres :
// - un indice de groupe
// - pointeur sur un vecteur d'entier.
// Valeur de retour : le nombre de marqueurs dans le groupe.
//-----------------------------------------------------------------------------

int CartaGene::GetGroupeI(int idg, intPtr *vm) const
{
	int i, nbg, nbm;
	NodintPtr padj, padjw;
	NodptrPtr plink;

	nbg = nbm = 0;

	if (Group == NULL)
	{
		*vm =  new int[1];
		(*vm)[0] = 0;
		return 0;
	}

	// trouver le groupe.
	plink = Group;

	while (plink != NULL && nbg++ < idg - 1)
		plink = plink->next;

	// premier passage pour compter.
	padj = plink->first;
	padjw = padj;
	while (padj != NULL)
	{
		nbm++;
		padj = padj->next;
	}

	*vm = new int[nbm+1];
	// pour compter avoir la taille du groupe directement dans le vecteur.
	(*vm)[nbm] = 0;

	// deuxi�me passage pour stocker.
	i = 0;
	padj = padjw;
	while (padj != NULL)
	{
		(*vm)[i++] = padj->vertex;
		padj = padj->next;
	}

	return nbm;
}

//-----------------------------------------------------------------------------
// Construction d'une carte, calcul et insertion dans le tas
//-----------------------------------------------------------------------------
// Param�tres :
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::SinglEM()
{

	// v�rification de base

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	// v�rification de la pr�sence de tous les marqueurs
	// dans le BioJeu principal = solution un peu rigide.

	for (int i = 0; i < NbMS; i++)
		if ((markers[MarkSelect[i]].BitJeu & ArbreJeu->BitJeu) == 0)
		{
			print_err( "Error : Unknown Loci Id %d in the main data set %d.\n",
					MarkSelect[i],
					ArbreJeu->Id);
			return;
		}

	Carte LaCarte(this, NbMS, MarkSelect);

	ComputeEM(&LaCarte);
	Heap->Insert(&LaCarte,0);

	if (!QuietFlag) {
		if (VerboseFlag>1) PrintDMap(&LaCarte, 0, &LaCarte);
		else PrintMap(&LaCarte);
	}
}

//-----------------------------------------------------------------------------
// Pour une nouvelle carte pas trop bete.
//-----------------------------------------------------------------------------

void CartaGene::BuildNiceMap(void)
{

	// v�rification de base

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	Carte TheMap(this, NbMS, MarkSelect);

	TheMap.BuildNiceMap();

	ComputeEM(&TheMap);
	Heap->Insert(&TheMap,0);

	if (!QuietFlag) {
		if (VerboseFlag>1) PrintDMap(&TheMap, 0, &TheMap);
		else PrintMap(&TheMap);
	}
}
void CartaGene::BuildNiceMapMultiFragment(void)
{

	// v�rification de base

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	Carte TheMap(this, NbMS, MarkSelect);

	TheMap.BuildNiceMapMultiFragment();

	ComputeEM(&TheMap);
	Heap->Insert(&TheMap,0);

	if (!QuietFlag) {
		if (VerboseFlag>1) PrintDMap(&TheMap, 0, &TheMap);
		else PrintMap(&TheMap);
	}
}

//-----------------------------------------------------------------------------
// Pour une nouvelle carte pas trop bete.
//-----------------------------------------------------------------------------

void CartaGene::BuildNiceMapL(void)
{

	// v�rification de base

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	Carte TheMap(this, NbMS, MarkSelect);

	TheMap.BuildNiceMapL();

	ComputeEM(&TheMap);
	Heap->Insert(&TheMap,0);

	if (!QuietFlag) {
		if (VerboseFlag>1) PrintDMap(&TheMap, 0, &TheMap);
		else PrintMap(&TheMap);
	}
}
void CartaGene::BuildNiceMapLMultiFragment(void)
{

	// v�rification de base

	if (NbMS == 0)
	{
		print_err(  "Error : Empty selection of loci.\n");
		return;
	}

	Carte TheMap(this, NbMS, MarkSelect);

	TheMap.BuildNiceMapLMultiFragment();

	ComputeEM(&TheMap);
	Heap->Insert(&TheMap,0);

	if (!QuietFlag) {
		if (VerboseFlag>1) PrintDMap(&TheMap, 0, &TheMap);
		else PrintMap(&TheMap);
	}
}

//-----------------------------------------------------------------------------
// Post traitement: Pour chaque marqueur,
// compare la probabilit� courante avec la probabilit� obtenue en
// d�plassant le marqueur vers un autre intervalle.  Ne met plus � jour
// avec le meilleur �l�ment (s'il y en a un). Affecte seulement le TAS
// Affiche la matrice des diff�rences.
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CartaGene::Polish()
{

	if (Heap->HeapSize == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return;
	}

	Carte *InitMap;

	Carte TheMap(this, NbMS, MarkSelect);

	InitMap = Heap->Best();
	InitMap->CopyFMap(&TheMap);

	TheMap.Polish();
}
//-----------------------------------------------------------------------------
// Post traitement: Pour chaque marqueur,
// compare la probabilit� courante avec la probabilit� obtenue en
// d�plassant le marqueur vers un autre intervalle.  Ne met plus � jour
// avec le meilleur �l�ment (s'il y en a un). N'Affectepas le TAS
// Affiche la matrice des diff�rences.
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CartaGene::Polishtest(int *vmi, int nbmi)
{
	int i=0,j=0;
	// une petite v�rif...
	for (i = 0; i < nbmi; i++)
	{

		if (vmi[i] > NbMarqueur || vmi[i]< 1)
		{
			fprintf(stderr, "Error : Unknown Loci Id %d.\n", vmi[i]);
			return;
		}

		for (j = i+1; j< nbmi; j++)
			if (vmi[i] == vmi[j])
			{
				fprintf(stderr, "Error : Double occurrence Loci Id %d.\n", vmi[i]);
				return;
			}
	}
	if (Heap->HeapSize == 0)
	{
		fprintf(stderr, "Error : Empty heap.\n");
		return;
	}
	flush_out();

	Carte TheMap(this, nbmi, vmi);
	ComputeEM(&TheMap);
	//  InitMap = Heap->Best();
	//  InitMap->CopyFMap(&TheMap);
	flush_out();

	TheMap.Polishtest(nbmi);
}
//-----------------------------------------------------------------------------
// les flips
//-----------------------------------------------------------------------------
// Param�tres :
// - fen�tre
// - seuil
// - it�ratif
// Valeur de retour :
//-----------------------------------------------------------------------------


void CartaGene::Flips(int n, double thres, int iter)
{

	// quelques v�rif

	if (Heap->HeapSize == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return;
	}

	if (n < 2 || n > 9)
	{
		print_err( "Error : The size of the window should be between 2 an 9.\n");
		return;
	}

	if (iter < 0 || iter >2)
	{
		print_err( "Error : The flag for iteration should be 0, 1 or 2.\n");
		return;
	}

	Carte *InitMap;

	Carte TheMap(this, NbMS, MarkSelect);

	InitMap = Heap->Best();
	InitMap->CopyFMap(&TheMap);

	TheMap.Flips(n, thres, iter);

}

//-----------------------------------------------------------------------------
// Dimensionnement du futur tas utilise par paretolkh et paretogreedy
//-----------------------------------------------------------------------------
// Param�tres :
// - la taille du nouveau tas.
//-----------------------------------------------------------------------------

void CartaGene::SetParetoHeapSize(int size)
{
	MAXHEAPSIZETSP = size;
}

//-----------------------------------------------------------------------------
// Redimensionnement du tas
// L'ancien est recopi� dans le nouveau.
//-----------------------------------------------------------------------------
// Param�tres :
// - la taille du nouveau tas.
//-----------------------------------------------------------------------------

void CartaGene::ResizeHeap(int size)
{

	if (size < 1)
	{
		print_err( "Error : The size of the heap should be greater than 0.\n");
		return;
	}

	Tas *newheap = new Tas();
	Carte *carte;

	newheap->EquivalenceFlag = Heap->EquivalenceFlag;

	newheap->Init(this, size);

	// on met temporairement la robustness a 0 pour eviter la sortie
	// precoce.
	double tmpRobustness = Robustness;
	Robustness = 1e111;

	while (Heap->HeapSize)   {
		carte = Heap->Worst();
		// on deconverge la carte pour reevaluation a l'insertion.
		carte->UnConverge();
		newheap->Insert(carte, 0);
		Heap->Extract();
	}
	Robustness = tmpRobustness;

	delete Heap;
	Heap = newheap;
}

//-----------------------------------------------------------------------------
// Fusion des listes des marqueurs que representent deux marqueurs
//-----------------------------------------------------------------------------
// Param�tres :
// - les deux marqueurs
//-----------------------------------------------------------------------------
void CartaGene::MergeRepresents(int m1, int m2)
{
	int i = m1;

	while (markers[i].Represents != 0) i = markers[i].Represents;

	markers[i].Represents = m2;
}
//-----------------------------------------------------------------------------
// Fusion de deux marqueurs
//-----------------------------------------------------------------------------
// Param�tres :
// - les deux marqueurs
// Retourne 0 si Ok 1 si probleme compatibilite.
//-----------------------------------------------------------------------------
int CartaGene::Merge(int m1, int m2)
{
	if (m1 > NbMarqueur || m1 < 1) {
		print_err( "Error : Unknown Locus %d.\n", m1);
		return 1;
	}

	if (m2 > NbMarqueur || m2 < 1) {
		print_err( "Error : Unknown Locus %d.\n", m2);
		return 1;
	}

	// If m1 or m2 have already been merged, we go to their representative markers
	while (markers[m1].Merged != 0) m1 = markers[m1].Merged;
	while (markers[m2].Merged != 0) m2 = markers[m2].Merged;

	if (m1 == m2)  {
		print_err(  "Error : The two locus are identical/merged\n");
		return 1;
	}

	// On verifie la compatibilite
	if (ArbreJeu->Compatible(m1,m2)) {
		ArbreJeu->Merge(m1,m2);
		markers[m2].Merged = m1;
		MergeRepresents(m1,m2);
		print_out( "Markers %d and %d merged in %d.\n",m1,m2,m1);

		// on efface le marqueur source de la selection s'il en fait partie
		int j = 0;

		for (int i = 0; i < NbMS; i++)
			if (MarkSelect[i] == m2) continue;
			else
				MarkSelect[j++] = MarkSelect[i];

		if (j == NbMS - 1)  {
			NbMS--;
			Heap->Init(this, Heap->MaxHeapSize);
		}
		//	ArbreJeu->ComputeTwoPoints();
		return 0;
	}
	else {
		print_out(  "The two markers are incompatible.\n");
		return 1;
	}
}
//-----------------------------------------------------------------------------
// return a list of list of merged loci by id,
// The first locus of the sublist represent the others that are following.
//-----------------------------------------------------------------------------
// Parameters :
//-----------------------------------------------------------------------------

/* TCL PRESENTATION LAYER */
char ** CartaGene::GetMerged()
{

	char* lineres;
	int j,k;

	// allocation

	// major�e
	char** listm =  new char*[NbMarqueur + 1];
	//convention
	listm[NbMarqueur] = NULL;

	k = 0;

	for (int i = 1; i <= NbMarqueur; i++)
	{
		j = markers[i].Represents;

		if (j != 0 && ! markers[i].Merged) {
			// major�e
			lineres = new char[(NbMarqueur + 1)*5];
			sprintf(lineres, "%d ",i);
			while (j != 0) {
				sprintf(lineres, "%s%d ",lineres, j);
				j = markers[j].Represents;
			}
			listm[k++] = lineres;
		}

	}
	//convention
	listm[k] = NULL;
	return (listm);
}

//-----------------------------------------------------------------------------
// Affectation de la liste de marqueurs s�lectionn�s
//-----------------------------------------------------------------------------
// Param�tres :
// - vecteur et nombres de marqueurs.
//-----------------------------------------------------------------------------

void CartaGene::ChangeSel(int *vm, int nbm)
{

	int i,j;

	// une petite v�rif...
	for (i = 0; i < nbm; i++) {

		if (vm[i] > NbMarqueur || vm[i]< 1)
		{
			print_err( "Error : Unknown Loci Id %d.\n", vm[i]);
			return;
		}

		for (j = i+1; j< nbm; j++)
			if (vm[i] == vm[j])
			{
				print_err( "Error : Double occurrence Loci Id %d.\n", vm[i]);
				return;
			}
	}

	// not optimized
	int found, same = (nbm  == NbMS);

	if (same)
		for (i = 0; i < nbm; i++) {
			found = 0;
			for (j = 0; j < nbm; j++)
				if (MarkSelect[j] == vm[i]) {
					found = 1;
					break;
				}
			if (!found) { same = 0; break; }
		}

	if (!same) {
		delete [] MarkSelect;
		MarkSelect = new int[nbm];
		Heap->Init(this, Heap->MaxHeapSize);
	}

	for (i = 0; i < nbm; i++) MarkSelect[i] = vm[i];
	NbMS = nbm;

}

//-----------------------------------------------------------------------------
// R�cup�ration de la s�lection de M. sous la forme d''un vecteur d''indice.
//-----------------------------------------------------------------------------
// Param�tres :
// - pointeur sur un vecteur d'entier.
// Valeur de retour : le nombre de marqueurs dans le groupe.
//-----------------------------------------------------------------------------

int CartaGene::GetSel(intPtr *vm) const
{
	*vm = new int[NbMS+1];
	// pour compter avoir la taille du groupe directement dans le vecteur.
	(*vm)[NbMS] = 0;

	// pour stocker.
	for (int i = 0; i < NbMS; i++)
		(*vm)[i] = MarkSelect[i];

	return NbMS;
}
//-----------------------------------------------------------------------------
// R�cup�ration de tous les M. sous la forme d''un vecteur d''indice.
//-----------------------------------------------------------------------------
// Param�tres :
// - pointeur sur un vecteur d'entier.
// Valeur de retour : le nombre de marqueurs dans le groupe.
//-----------------------------------------------------------------------------

int CartaGene::GetAll(intPtr *vm) const
{
	*vm = new int[NbMarqueur+1];
	// pour compter avoir la taille du groupe directement dans le vecteur.
	(*vm)[NbMarqueur] = 0;

	// pour stocker.
	for (int i = 0; i < NbMarqueur; i++)
		(*vm)[i] = i+1;

	return NbMarqueur;
}
//-----------------------------------------------------------------------------
// Fixer les tolerances de convergence EM
//-----------------------------------------------------------------------------
// Param�tres :
// - le niveau de convergence fin
// - le niveau de convergence grossier
// Valeur de retour : none
//-----------------------------------------------------------------------------
void CartaGene::SetTolerance(double Fin,double Gros)
{
	if (Fin <= 0.0) {
		print_err(  "Error : fine convergence threshold must be > 0\n");
		return;
	}

	if (Gros <= 0.0) {
		print_err(  "Error : raw convergence threshold must be > 0\n");
		return;
	}

	if (Gros < Fin) {
		print_err(  "raw convergence threshold must be > fine one\n");
		return;
	}

	ArbreJeu->Epsilon2 = Fin;
	ArbreJeu->Epsilon1 = Gros;

	// on resize le tas a la meme taille pour reevaluer les cartes du
	// tas.
	if (Heap->HeapSize >0)
		ResizeHeap(Heap->HeapSize);
}
//-----------------------------------------------------------------------------
// Fixer le seuil de fiabilit� des cartes frameworks
//-----------------------------------------------------------------------------
// Param�tres :
// - l'�cart de vraisemblance relatif � la meilleure carte (default = -3.)
// Valeur de retour : none
//-----------------------------------------------------------------------------
void CartaGene::SetRobustness(double robust)
{
	Robustness = robust;

	// verification de la robustesse de la meilleure carte courante
	if (Heap->HeapSize >= 2 && Heap->Delta() + Robustness < 0.) {
		print_out(  "Current best map is not robust!\n");
	}
}
//-----------------------------------------------------------------------------
// Affectation de la verbosit�
//-----------------------------------------------------------------------------
// Param�tres :
// - le niveau
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::SetVerbose(int flag)
{
	if (flag < 0 || flag > 2)
	{
		print_err(  "Error : possible values are 0, 1 or 2.\n");
		return;
	}

	VerboseFlag = flag;
}
//-----------------------------------------------------------------------------
// Affectation du silencieux
//-----------------------------------------------------------------------------
// Param�tres :
// - le niveau
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::SetQuiet(int flag)
{
	if (flag < 0 || flag > 1)
	{
		print_err(  "Error : posible values are 0 or 1.\n");
		return;
	}

	QuietFlag = flag;
}

//-----------------------------------------------------------------------------
// Algorithme genetique.
//-----------------------------------------------------------------------------
// Param�tres :
// - ...
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::AlgoGen( int nb_gens,
		int nb_elements,
		int selection_number,
		float pcross,
		float pmut,
		int evolutive_fitness)
{
	// quelques v�rifications

	if (Heap->HeapSize == 0 && nb_elements == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return;
	}

	if (nb_gens < 0)
	{
		print_err(  "Error : value expected for the number of generation :  >= 0.\n");
		return;
	}
	if (nb_elements < 0)
	{
		print_err(  "Error : value expected for the size of the population: >= 0.\n");
		return;
	}
	if (nb_elements > NbMS)
	{
		print_err(  "Error : value expected for the size of the population <= number of loci.\n");
		return;
	}

	if (selection_number < 0)
	{
		print_err(  "Error : value expected for the type of the selection : >= 0.\n");
		return;
	}
	if ((pcross < 0.0) || (pcross > 1.0))
	{
		print_err(  "Error : value expected for the probability of crossing over : 0.0 < & < 1.0.\n");
		return;
	}
	if ((pmut < 0.0) || (pmut > 1.0))
	{
		print_err(  "Error : value expected for the probability of mutation : 0.0 < & < 1.0.\n");
		return;
	}
	if (evolutive_fitness < 0)
	{
		print_err(  "Error : value expected for the evolutive fitness : >= 0.\n");
		return;
	}

	// fin des v�rifications

	Algogen *lalgogen;

	lalgogen = new Algogen(this);

	lalgogen->Run(nb_gens,
			nb_elements,
			selection_number,
			pcross,
			pmut,
			evolutive_fitness);

	delete lalgogen;

}

//-----------------------------------------------------------------------------
// Retourne le nom d'un marqueur
//-----------------------------------------------------------------------------
// Param�tres :
// - ...
// Valeur de retour :
//-----------------------------------------------------------------------------

char * CartaGene::GetMrkName(int mid)
{
	char *vide;
	char *lineres;

	// pour retourner une liste vide (voir conventions)
	vide = new char[1];
	vide[0] = '\0';

	if (mid > NbMarqueur || mid < 1)
	{
		print_err( "Error : Unknown Locus %d.\n", mid);
		return vide;
	}

	lineres = new char[markers.keyOf(mid).size() + 1];

	sprintf(lineres,  "%s", markers.keyOf(mid).c_str());

	return lineres;

}

//-----------------------------------------------------------------------------
// Retourne le num�ro d'un marqueur
//-----------------------------------------------------------------------------
// Param�tres :
// - ...
// Valeur de retour :
//-----------------------------------------------------------------------------

int CartaGene::GetMrkId(char *mname)
{
    int i = markers.indexOf(mname);
    if(i==-1) {
        print_err( "Error : Unknown Locus %s.\n", mname);
    }

	return i;
}

//-----------------------------------------------------------------------------
// Retourne le num�ro du dernier marqueur
//-----------------------------------------------------------------------------
// Param�tres :
// - ...
// Valeur de retour :
//-----------------------------------------------------------------------------

int CartaGene::LastMrk()
{
	return NbMarqueur;
}

//-----------------------------------------------------------------------------
// Retourne un tableau des distance cumulees
//-----------------------------------------------------------------------------
// Param�tres :
// - ...
// Valeur de retour :
//-----------------------------------------------------------------------------

void CartaGene::CalculDist(double *distd, Carte *data, int NbMS, int NbMF)
{
	int i,k,itr;
	double d = 0.0, totald = 0.0;

	distd[0] = 0;
	for (k = 0; k < data->NbMarqueur; k++)
	{

		i = k;
		itr = i;


		if (k < data->NbMarqueur-1)
			(ArbreJeu->HasRH() ? d = Theta2Ray(data->tr[itr])*100 : d = Haldane(data->tr[itr])*100);
		totald += d;
		distd[k+1] = totald;
	} // for (k=
}

//LD
/** Calcule le nb de BP entre un jeu d'ordre et un autre jeu quelconque
*/
int CartaGene::BreakPoints(int jeuOrdre, int jeu2)
{

	// test de l'existence des jeux � fusionner

	if ( jeuOrdre > NbJeu || jeuOrdre < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", jeuOrdre);
		return -1;
	}

	if (jeu2 > NbJeu || jeu2 < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", jeu2);
		return -1;
	}

	if ( Jeu[jeuOrdre]->Cross != Ordre)
	{
		print_err( "Error : wrong data type, first set must be of type ""order""\n");
		return -1;
	}
	return (Jeu[jeuOrdre]->BreakPoints(Jeu[jeu2]));
}

/** Calcule le nb de BP entre une carte et un jeu d'ordre
*/
int CartaGene::BreakPointsMap(int jeuOrdre, int nbmap)
{
	// test de l'existence des jeux � fusionner

	if ( jeuOrdre > NbJeu || jeuOrdre < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", jeuOrdre);
		return -1;
	}

	if ( Jeu[jeuOrdre]->Cross == Mor) {
		return BreakPointsMap(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id,nbmap) + BreakPointsMap(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id,nbmap);
	}
	if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

	if (Heap->HeapSize == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return -1;
	}

	if ( nbmap < 0 || nbmap >= Heap->HeapSize )
	{
		print_err(  "Error : This map does not exist.\n");
		return -1;
	}

	return (Jeu[jeuOrdre]->BreakPointsMap(Heap->MapFromId(nbmap)));
}

// calcul le nomre de BP entre carte et jeu d'ordre, avec pointeur sur carte
// Retourne -1 si pas d'ordre de ref dans jeuOrdre

int CartaGene::BreakPointsMapPointer(int jeuOrdre, Carte *map)
{
	// test de l'existence des jeux � fusionner

	if ( jeuOrdre > NbJeu || jeuOrdre < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", jeuOrdre);
		return -1;
	}

	if ( Jeu[jeuOrdre]->Cross == Mor) {
		return BreakPointsMapPointer(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id,map) + BreakPointsMapPointer(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id,map);
	}
	if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

	return (Jeu[jeuOrdre]->BreakPointsMap(map));
}

/** Calcule la contribution du jeu d'ordre dans le calcul de EM pour la carte nbmap
*/
double CartaGene::ComputeEMOrder(int jeuOrdre, int nbmap)
{
	// test de l'existence des jeux � fusionner

	if ( jeuOrdre > NbJeu || jeuOrdre < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", jeuOrdre);
		return -1;
	}

	if ( Jeu[jeuOrdre]->Cross == Mor) {
		return ComputeEMOrder(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id,nbmap) + ComputeEMOrder(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id,nbmap);
	}
	if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

	if (Heap->HeapSize == 0)
	{
		print_err(  "Error : Empty heap.\n");
		return -1;
	}

	if ( nbmap < 0 || nbmap >= Heap->HeapSize )
	{
		print_err(  "Error : This map does not exist.\n");
		return -1;
	}

	if (EM2pt) {
		Carte *map = Heap->MapFromId(nbmap);
		double res = Jeu[jeuOrdre]->ContribLogLike2pt(map->ordre[0]);
		for (int m = 0; m < map->NbMarqueur - 1; m++) {
			res += Jeu[jeuOrdre]->ContribLogLike2pt(map->ordre[m], map->ordre[m+1]);
		}
		res += Jeu[jeuOrdre]->ContribLogLike2pt(map->ordre[map->NbMarqueur - 1]);
		return -res;
	} else {
		return Jeu[jeuOrdre]->ComputeEM(Heap->MapFromId(nbmap));
	}
}

double CartaGene::SetBreakPointCoef(int jeuOrdre, double newcoef) {
	// test de l'existence des jeux � fusionner

	if ( jeuOrdre > NbJeu || jeuOrdre < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", jeuOrdre);
		return -1;
	}

	if ( Jeu[jeuOrdre]->Cross == Mor) {
		return SetBreakPointCoef(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id,newcoef) + SetBreakPointCoef(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id,newcoef);
	}
	if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

	double previous = ((BJS_OR *) Jeu[jeuOrdre])->Poids;
	((BJS_OR *) Jeu[jeuOrdre])->Poids = newcoef;
	return previous;
}

/* See Numerical Recipes in C 2nd Ed. p665 */
/* linear regression of ndata points (x,y) on y = a + b.x */
static void linearfit(double x[], double y[], int ndata, double *a, double *b)
{
	int i;
	double t,sxoss,sx=0.0,sy=0.0,st2=0.0,ss;
	*b=0.0;

	for (i=0;i<ndata;i++) {
		sx += x[i];
		sy += y[i];
	}
	ss=ndata;
	sxoss=sx/ss;
	for (i=0;i<ndata;i++) {
		t=x[i]-sxoss;
		st2 += t*t;
		*b += t*y[i];
	}
	*b /= st2;
	*a=(sy-sx*(*b))/ss;
}

double CartaGene::SetBreakPointLambda(int jeuOrdre, int CoefAutoFit, double newlambda) {
	// test de l'existence des jeux � fusionner

	if ( jeuOrdre > NbJeu || jeuOrdre < 1)
	{
		print_err( "Error : Unknown Data Set %d.\n", jeuOrdre);
		return -1;
	}

	if (newlambda < 0)
	{
		print_out( "Negative lambda value: using uniform prior\n");
	}

	if ( Jeu[jeuOrdre]->Cross == Mor) {
		return SetBreakPointLambda(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id,CoefAutoFit,newlambda) + SetBreakPointLambda(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id,CoefAutoFit,newlambda);
	}
	if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

	double previous = ((BJS_OR *) Jeu[jeuOrdre])->Lambda;
	((BJS_OR *) Jeu[jeuOrdre])->Lambda = newlambda;

	if (CoefAutoFit) {
		//  #ifdef __WIN32__
		//      sprintf(bouf,"Sorry! Cannot auto fit coefficient factor value under Windows.\n");
		//      pout(bouf);
		//  #else
		if (NOrComput != NULL) delete NOrComput;
		int *vc;
		int nc =  ((BJS_OR*) Jeu[jeuOrdre])->GetnLocipChrom(&vc);
		NOrComput = new NOrCompMulti(NbMS-1, vc, nc);
		NOrComput->run();

		int glocdim = vc[0];
		for (int h = 1; h < nc; h++)
			glocdim += vc[h];

		//
		//   FILE *tmpfit = NULL;
		//     tmpfit = fopen("tmpfit","w");
		//     char mystring[1024];
		//

		double *x = new double[NbMS];
		double *y = new double[NbMS];
		int ndata = 1;
		double priormass = 0;
		// nombre de BP possibles ([0->bpmax-1])
		// glocdim : nombre total de mk
		// glocdim-nc : nombre total de bp si nc chr (sum_{i [1..nc]} (ni-1))
		// +1 : cas 0 bp
		int bpmax=glocdim-nc+1;
		int l = abs((int)newlambda);
		if (l > bpmax-1) {
			fprintf(stderr,"Uniform prior: lambda must be < %d\n",bpmax-1);
		}

		// first regression point: bp=0, NO(0)=1, Poisson(X=0|lambda)=exp(-lambda)
		x[0]=0;
		if (newlambda>0) {
			y[0]=(double)log10l(expl(-newlambda));
			priormass=exp(-newlambda);
		}
		else {
			y[0]=(double)log10l(0.99/(l+1));
			priormass=0.99/(l+1);
		}

		for (int bp = ((nc>1)?nc-1:1); bp < glocdim; bp++) {
			double rhII = - (double)log10l(NOrComput->getNO(bp)/2);
			//        sprintf(bouf, "%d %f\n", bp, rhII);
			//        pout(bouf);

			// Prior uniforme: 0.99 sur [nc] + 0.01 sur [Lambda+nc+2 .. bpmax]
			if (newlambda < 0) {
				if (ndata > l) {
					priormass+=0.01/(bpmax-l);
					rhII += (double) log10l(0.01/(bpmax-l));
				}
				else {
					priormass += 0.99/(l+1);
					rhII += (double) log10l(0.99/(l+1));
				}
			}
			// Prior Poisson
			else {
				rhII += (double)log10l(expl((long double) -newlambda) * powl((long double) newlambda,(long double) (bp-nc+1)) / dfact(bp-nc+1));
				priormass+=expl((long double) -newlambda) * powl((long double) newlambda,(long double)(bp-nc+1)) / dfact(bp-nc+1);
			}

			//        sprintf(bouf, "%d %f %f\n", bp, newlambda, rhII);
			//        pout(bouf);

#ifdef __WIN32__
			if (_finite(rhII)) {
#else
				if (finite(rhII)) {
#endif

					//
					// 	sprintf(mystring,"%d %f %f\n", bp, rhII, (double)NOrComput->getNO(bp));
					// 	fputs(mystring,tmpfit);
					//
					x[ndata] = bp;
					y[ndata] = rhII;
					ndata++;
				}
			}
			if (priormass < 0.9) {
				print_out("Warning: only %f of the prior mass on the possible range for bp values\n",priormass);
			}
			//
			//printf("prior mass=%f\n",priormass);
			//
			//     fclose(tmpfit);

			//      char execute[1024];
			//      sprintf(execute, "( echo \"f(x)=a*x+b\" ; echo \"fit f(x) 'tmpfit' via a,b\" ; echo \"exit\" ) | gnuplot 2>&1 |  awk 'BEGIN{ok=0} /Final set/{ok=1} ok&&/^a /{print $3}' > tmpfitcoef");
			//      system(execute);
			//      //

			//      FILE *tmpres = NULL;
			//      tmpres = fopen("tmpfitcoef","r");
			//      fgets(mystring, 1023, tmpres);
			//      fclose(tmpres);
			//     double newcoef = atof(mystring);

			double newcoef = 0.0;
			double abscissa = 0.0;
			linearfit(x,y,ndata,&abscissa,&newcoef);

			if (!QuietFlag) {
				print_out("New coefficient factor value is %f.\n",newcoef);
			}
			SetBreakPointCoef(jeuOrdre, newcoef);

			delete[] x;
			delete[] y;
			//  #endif
		}
		return previous;
	}

	bool CartaGene::ValidMarkerSelection( int id )
	{
		if (NbMS == 0)
		{
			print_err(  "Error : Empty selection of loci.\n");
			return false;
		}

		for (int i = 0; i < NbMS; i++)if ((markers[MarkSelect[i]].BitJeu & ArbreJeu->BitJeu) == 0)
		{
			print_err( "Error : Unknown Loci Id %d in the main data set %d.\n",
                    MarkSelect[i],
                    ArbreJeu->Id);
			return false;
		}

		return true;
	}


	/** Only relevant to the BJS_RH data type
	 * Impute the corrected genotypes and prints in the carthagene format
	 */

	void CartaGene::ErrorEstimation(int id)
	{
		//Cross type must be RH
		if ( Jeu[id]->Cross != RH )
		{
			print_err(  "Error : only available for haploid RH data set.\n");
		}

		if ( !ValidMarkerSelection(id) )
			return;

		BJS_RH* a = (BJS_RH*) Jeu[id];
		BJS_RHE* b = new BJS_RHE(*a); 

		for(int i=0;i<=NbMarqueur;++i) {
			//BitJeuMarq[i]|=(BitJeuMarq[i]&a->BitJeu?b->BitJeu:0);	/* si le flag du jeu cloné est positionné, on positionne aussi celui du clone */
            markers[i].BitJeu |= (markers[i].BitJeu&a->BitJeu) ? b->BitJeu : 0;
		}
		b->InitContribLogLike2pt(); 

		/*BJS_RH* c = new BJS_RH(*b);*/

		Carte LaCarte(this, NbMS, MarkSelect);
		b->ComputeEM(&LaCarte); 

		print_out( "Error estimation:\nFalse positive: %.4f\nFalse negative: %.4f \n",b->FalsePositiveRate(),b->FalseNegativeRate());

		delete b;

	}

	/** Only relevant to the BJS_RH data type
	 * Impute the corrected genotypes and prints in the carthagene format
	 */
	char* CartaGene::Imputation( int id, double ConversionCutoff, double CorrectionCutoff,  double UnknownCorrectionCutoff)
	{
		//Cross type must be RH
		if ( Jeu[id]->Cross != RHE )
		{
			print_err(  "Error : only available for haploid RH with Errors data set.\n");
		}

		if ( !ValidMarkerSelection(id) )
			return "";

		BJS_RH* a = (BJS_RH*) Jeu[id];
		BJS_RHE* b = new BJS_RHE(*a); 	

		for(int i=0;i<=NbMarqueur;++i) {
			//BitJeuMarq[i]|=(BitJeuMarq[i]&a->BitJeu?b->BitJeu:0);	// si le flag du jeu cloné est positionné, on positionne aussi celui du clone 
            markers[i].BitJeu |= (markers[i].BitJeu&a->BitJeu) ? b->BitJeu : 0;
		}
		b->InitContribLogLike2pt();

		Carte LaCarte(this, NbMS, MarkSelect);	
		b->ComputeEM(&LaCarte); 
		b->Imputation(&LaCarte, ConversionCutoff, CorrectionCutoff, UnknownCorrectionCutoff);

		BJS_RH* CorrectedJeu = new BJS_RH(*b);
		PostTraitementBioJeu(CorrectedJeu, b);

		char* lineres = new char[256];
		/*sprintf(lineres, "%d corrected imputed genotypes from %d, %d markers", CorrectedJeu->Id, id, NbMS, CorrectedJeu->TailleEchant);*/
		sprintf(lineres, "%d corrected imputed genotypes from %d, %d markers", CorrectedJeu->Id, id, NbMS);

		delete b;

		return lineres;
	}



	/** Only relevant to the BJS_RHE data type
	 * Impute the corrected genotypes and prints in the carthagene format
	 */
	/*void CartaGene::ImputationOLD( double ConversionCutoff, double CorrectionCutoff,  double UnknownCorrectionCutoff, char *filename)
	  {
	  ArbreJeu->Imputation( ConversionCutoff, CorrectionCutoff, UnknownCorrectionCutoff, filename);
	  }*/
