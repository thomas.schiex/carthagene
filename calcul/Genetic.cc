//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Genetic.cc,v 1.10 2002-10-08 09:43:41 tschiex Exp $
//
// Description  : Fonctions de calcul de distance
// Divers :
//-----------------------------------------------------------------------------

#include "Genetic.h"
#include <math.h>

//-----------------------------------------------------------------------------
// Que fait cette fonction ? Calcule la distance en ray8000 a partir de la
//                           probabilite de cassure
//-----------------------------------------------------------------------------
// Param�tres :
// - la distance � theta.
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

double Theta2Ray(double theta)
{
  return  ((theta < 1.0) ? -(log(1.0-theta)) : Theta2Ray(0.9995));
}
//-----------------------------------------------------------------------------
// Que fait cette fonction ? Calcule la distance en ray8000 a partir de la
//                           probabilite de cassure
//-----------------------------------------------------------------------------
// Param�tres :
// - la distance � theta.
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

double Ray2Theta(double ray)
{
  return  (1.0-exp(-ray));
}

//-----------------------------------------------------------------------------
// Que fait cette fonction ? Calcule la f. de recomb. a partir de la distance
//-----------------------------------------------------------------------------
// Param�tres :
// - la distance � theta.
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

double Haldane(double theta)
{
  return  ((theta < 0.5) ? fabs(log(1-2*theta)/2) : Haldane(0.4995));
}

//-----------------------------------------------------------------------------
// Que fait cette fonction ? fonction inverse (de la distance � theta)
//-----------------------------------------------------------------------------
// Param�tres :
// - la distance � theta.
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

double HaldaneInv(double dist)
{
  return (1 - exp(-2*dist))/2;
}

//-----------------------------------------------------------------------------
// Que fait cette fonction ? Calcule la f. de recomb. a partir de la distance
//-----------------------------------------------------------------------------
// Param�tres :
// - la distance
// -
// Valeur de retour : 
//-----------------------------------------------------------------------------

#ifdef __WIN32__
#  define atanh(x)	(0.5*(log(double(1+x)/double(1-x))))
#endif

double Kosambi(double theta)
{
  return ((theta < 0.5) ? atanh(2*theta)/2 : Kosambi(0.4995));
}

//-----------------------------------------------------------------------------
// Que fait cette fonction ?
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

double KosambiInv(double dist)
{
  return (tanh(2*dist)/2);
}
