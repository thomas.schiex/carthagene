//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Damien Leroux, Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// CartaGene.cc,v 1.106 2007/06/11 15:05:52 degivry Exp
//
// Description : Structure de données générique pour fournir à la fois les services d'un tableau et d'une table de hashage (voir MarkerList pour un exemple d'utilisation).
// Divers :
//-----------------------------------------------------------------------------

#ifndef _CARTHAGENE_MARKER_H_
#define _CARTHAGENE_MARKER_H_

#include "KeyList.h"


struct Marker {
    unsigned int BitJeu;
    unsigned int Merged;
    unsigned int Represents;
    Marker() : BitJeu(0), Merged(0), Represents(0) {}
    virtual ~Marker() {}    /* requirement for KeyList */
};




/* The following stuff implements std::strings with case insensitive comparison and hashing */

/* taken from http://stackoverflow.com/questions/11635/case-insensitive-string-comparison-in-c/2886589#2886589 */
struct ci_char_traits : public std::char_traits<char> {
    static bool eq(char c1, char c2) { return toupper(c1) == toupper(c2); }
    static bool ne(char c1, char c2) { return toupper(c1) != toupper(c2); }
    static bool lt(char c1, char c2) { return toupper(c1) <  toupper(c2); }
    static int compare(const char* s1, const char* s2, size_t n) {
        /*std::cout << "compare <" << s1 << "> and <" << s2 << "> for " << n << " characters... ";*/
        do {
            if( toupper(*s1) < toupper(*s2) ) {
                /*std::cout << '<' << std::endl;*/
                return -1;
            }
            if( toupper(*s1) > toupper(*s2) ) {
                /*std::cout << '>' << std::endl;*/
                return 1;
            }
            ++s1; ++s2;
        } while( n-- != 0 );
        /*std::cout << '=' << std::endl;*/
        return 0;
    }
    static const char* find(const char* s, int n, char a) {
        while( n-- > 0 && toupper(*s) != toupper(a) ) {
            ++s;
        }
        return s;
    }
};

typedef std::basic_string<char, ci_char_traits> ci_string;

namespace __gnu_cxx {
    /* STL hash, the case-insensitive way. */
    template<> struct hash<ci_string> {
        size_t operator()(const ci_string&s) const {
            size_t accum=0;
            ci_string::const_iterator i=s.begin(), j=s.end();
            while(i!=j) {
                accum = (accum<<5)+accum + tolower(*i);
                ++i;
            }
            return accum;
        }
    };
}


namespace std {
    /* implement output to an std::stream. */
    static inline ostream& operator<<(ostream&os, ci_string&s) {
        return os<<s.c_str();
    }
    static inline ostream& operator<<(ostream&os, const ci_string&s) {
        return os<<s.c_str();
    }
}



typedef KeyList<ci_string, Marker> MarkerList;

#endif

