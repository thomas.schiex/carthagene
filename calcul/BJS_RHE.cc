//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// $Id: BJS_RHE.cc,v 1.6.2.6 2012-08-02 15:43:54 dleroux Exp $
//
// Description : BJS_RHE
// Divers :
//-----------------------------------------------------------------------------

#include "BJS_RH.h"
#include "BJS_RHD.h"
#include "BJS_RHE.h"
#include "Genetic.h"
#include "System.h"
#include <stdio.h>
#include <string.h>
#include <string>
#include <math.h>
#ifdef WIN32
#include <malloc.h>
#endif


/** Esperance du nombre de cassures (extremites gauches) */
#define EBreak expected[NbM - 1]

/** Esperance du nombre d'extremites gauches retenues */
#define ERetained expected[NbM]

/** liste des génotypes possibles pour chaque phénotype */

const int RHE_Possibles[16][4] = {
  {0, 0, 0, 0},  //  Absent
  {1, 0, 0, 0},  //  Présent
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 1, 0, 0}  //  Inconnu
};

/** nombre de possibilités pour chaque phenotype */
const int RHE_NPossibles[16] = {1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2};

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_RHE::BJS_RHE() : BioJeuSingle()
{

}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Paramètres :
// - accès à l'ensemble des informations du système.
// - le numéro du jeu
// - le type
// - le nombre de marqueurs
// - la taille de l'echantillon
// - le champ de bit
// - la matrice de l'échantillon
// Valeur de retour :
//-----------------------------------------------------------------------------

BJS_RHE::BJS_RHE(CartaGenePtr cartag,
	       int id,
	       charPtr nomjeu,
	       CrossType cross,
	       int nm,
	       int taille,
	       int bitjeu,
	       int *indmarq,
	       Obs **echantil)
  :BioJeuSingle(cartag,
		id,
                nomjeu,
		cross,
		nm,
		taille,
		bitjeu,
		indmarq,
		echantil)
{
  NbMeiose = TailleEchant;

  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;

  pi = FALSE_POSITIVE_INIT;
  nu = FALSE_NEGATIVE_INIT;

  LogLikeliHood = 0;

  ComputeTwoPoints();
}

// DL
// FIXME : vulgaire copier-coller
BJS_RHE::BJS_RHE(const BJS_RHE& b)
	: BioJeuSingle(b)
{
  NbMeiose = TailleEchant;

  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;

  pi = FALSE_POSITIVE_INIT;
  nu = FALSE_NEGATIVE_INIT;

  LogLikeliHood = 0;

  ComputeTwoPoints();
}

BJS_RHE::BJS_RHE(const BJS_RHD& b)
	: BioJeuSingle(b)
{
  NbMeiose = TailleEchant;

  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;

  pi = FALSE_POSITIVE_INIT;
  nu = FALSE_NEGATIVE_INIT;

  LogLikeliHood = 0;

  ComputeTwoPoints();
}

BJS_RHE::BJS_RHE(const BJS_RH& b)
	: BioJeuSingle(b)
{
  NbMeiose = TailleEchant;

  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;

  pi = FALSE_POSITIVE_INIT;
  nu = FALSE_NEGATIVE_INIT;

  LogLikeliHood = 0;

  ComputeTwoPoints();
}


//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_RHE::~BJS_RHE()
{
}

// Note : Thomas F
// Since we are working with haploid model the following methods
// Compatible
// Merge
// Prepare2pt
// Estimate2pt
// LogLike2pt
// ComputeOneTwoPoints
//
// are striclty identical to the BJS_RH data type (and we copy them because BJS_RHE does not derive from BJS_RH)
// (we keep somehow here the principle adopted for the other data types)

//-----------------------------------------------------------------------------
// Compatibilité des observations pour 2 marqueurs
//-----------------------------------------------------------------------------
int BJS_RHE::Compatible(int numarq1, int numarq2) const
{
  //To be on the safe side
  print_out("WARNING : Not implemented for RH data with errors \n");

  /*int i;
  enum Obs Obs1, Obs2;

  for (i = 1; i<= TailleEchant; i++) {
    Obs1 = GetEch(numarq1,i);
    Obs2 = GetEch(numarq2,i);
    if (!((Obs1 == Obs1111) || (Obs2 == Obs1111) || (Obs1 == Obs2)))
      return 0;
  }*/
  return 0;
}

//-----------------------------------------------------------------------------
// Fusion des observations entre individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
//-----------------------------------------------------------------------------
void BJS_RHE::Merge(int numarq1, int numarq2) const
{
	//To be on the safe side
	print_out("WARNING : Not implemented for RH data with errors \n");
	/*
int i;
  enum Obs TheObs;

  // les deux marqueurs sont ils definis dans le jeux de donnees ?
  if ((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu &
 BitJeu))
    {
      // Oui, on va fusionner dans le premier
      for (i = 1; i<= TailleEchant; i++) {
        if ((TheObs = Echantillon[IndMarq[numarq1]][i]) == Obs1111)
          TheObs = Echantillon[IndMarq[numarq2]][i];
        Echantillon[IndMarq[numarq1]][i] = TheObs;
      }
    }
    */
}

//-----------------------------------------------------------------------------
// Calcul du nombre d'occurrences de chque pattern (stat suffisantes
// pour le calcul de vraisemblance et des estimateurs de max. de
// vraisemblance 2pts)
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja alloue des
// stats suffisantes
// Valeur de retour : aucune, effet de bord sur ss
//-----------------------------------------------------------------------------
void BJS_RHE::Prepare2pt(int m1, int m2, int *ss) const
{
  for (int m = 1; m <= TailleEchant; m++)    {

    if ((GetEch(m1, m) != Obs1111) && (GetEch(m2, m) != Obs1111)) {
      ss[GetEch(m1, m)*2 + GetEch(m2, m)]++;
    }
  }
}
//-----------------------------------------------------------------------------
// Calcul de l estimateur 2pt de max de vraisemblance
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des
// stats suffisantes, le taux de cassure et de retention a estimer
// Valeur de retour : aucune, effet de bord sur les deux parametres
//-----------------------------------------------------------------------------
void BJS_RHE::Estimate2pt(double *par, int *ss) const
{
  int tot = ss[3]+ss[0]+ss[2]+ss[1];
  double r,theta;

  if (tot == 0) {
     par[1]= EM_MAX_THETA_HR;
     par[0] = EM_MAX_RETAIN;
     return;
   }

   // taux de retention
   r = (double)(ss[3]+ss[3]+ss[2]+ss[1])/(double)(2*tot);

   if (r > EM_MAX_RETAIN) r = EM_MAX_RETAIN;
   if (r < EM_MIN_RETAIN ) r = EM_MAX_RETAIN;
   // taux de cassure
   theta = (double)(ss[1]+ss[2])/(2*tot*r*(1-r));

  if (theta > EM_MAX_THETA_HR) theta = EM_MAX_THETA_HR;
  if (theta < EM_MIN_THETA_HR) theta =EM_MIN_THETA_HR;

  par[0] = theta;
  par[1] = r;
}
//-----------------------------------------------------------------------------
// Calcul de la vraisemblance 2pts
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des
// stats suffisantes, le taux de cassure et de retention.
// Valeur de retour : le loglike
//-----------------------------------------------------------------------------
double BJS_RHE::LogLike2pt(double *par, int *ss) const
{
	double loglike;

	loglike = ss[0]*log10((1-par[1])*(1-par[0]*par[1]));
	if (ss[1]+ss[2])
		loglike += (ss[1]+ss[2])*log10((1-par[1])*par[0]*par[1]);
	loglike += ss[3]*log10(par[1]*(1-par[0]*(1-par[1])));

	return loglike;
}
//-----------------------------------------------------------------------------
//  Calcul des distances, et estimations 2 points
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------
double BJS_RHE::ComputeOneTwoPoints(int m1,
				   int m2,
				   double epsilon,
				   double *fr) const
{
  int n[4];
  int tot;
  double par[2];
  double loglike;

  n[0] = n[1] = n[2] = n[3] = 0;
  Prepare2pt(m1,m2,n);
  tot = n[0] + n[1] + n[2] + n[3];

  Estimate2pt(par,n);

  *fr = par[0];

  loglike = LogLike2pt(par,n);
  par[0] = 1.0;

  return loglike-LogLike2pt(par,n);
}

// Note : Thomas F
// The code below is specific to the BJS_RHE data type (haploid data with errors)

//HMM formalization of the Radiation Hybrid principle with errors

//-----------------------------------------------------------------------------
//  Structure initialization for the EM algorithm
//-----------------------------------------------------------------------------
// Param  :
//   - the map (la carte)
// Return : none
//-----------------------------------------------------------------------------

void BJS_RHE::PreparEM(const Carte *data)
{

	/* A lot of array structures necessary for the Forward-Backward algorithm :
	 *  Trans and associated partial derivatives (DeltaThetaTrans, DeltaRTrans)
	 *  The Scorevector for the contribution of partial derivatives to the expectation
	 *  alpha, beta, gamma, and ksi
	 *  Error estimation associated structure : PostProb
	 */

	//Inhomogeneous Markov chain. For each marker interval we have to define transition probabilities
	// Trans[k][i][j] := Prob_k(s_j|s_i)
	Trans = new LongDouble**[data->NbMarqueur-1];
	for (int k = 0; k< data->NbMarqueur-1; k++)
	{
		Trans[k]= new LongDouble*[NB_STATES];
		for (int i = 0; i < NB_STATES; i++)
			Trans[k][i] = new LongDouble[NB_STATES];
	}

	//Partial derivatives of transition probabilities (Lange implementation of RH EM)
	// DeltaThetaTrans[k][i][j] := delta (Prob_k(s_j|s_i)) / delta theta_k
	DeltaThetaTrans = new LongDouble**[data->NbMarqueur-1];
	for (int k = 0; k< data->NbMarqueur-1; k++)
	{
		DeltaThetaTrans[k]= new LongDouble*[NB_STATES];
		for (int i = 0; i < NB_STATES; i++)
			DeltaThetaTrans[k][i] = new LongDouble[NB_STATES];
	}

	// DeltaRTrans[k][i][j] := delta (Prob_k(s_j|s_i)) / delta r
	DeltaRTrans = new LongDouble**[data->NbMarqueur-1];
	for (int k = 0; k< data->NbMarqueur-1; k++)
	{
		DeltaRTrans[k]= new LongDouble*[NB_STATES];
		for (int i = 0; i < NB_STATES; i++)
			DeltaRTrans[k][i] = new LongDouble[NB_STATES];
	}

	//Score vector for the partial derivative of the loglikelihood of the observed data (deltaP/P where P is the likelihood)
	// NbM-1 breakage frequencies and 1 retention fraction
	ScoreVector = new LongDouble[data->NbMarqueur];

	// Alpha[k][i] := alpha_k(s_i)
	Alpha = new LongDouble*[data->NbMarqueur];
	for (int k = 0; k< data->NbMarqueur; k++)
	{
		Alpha[k]= new LongDouble[NB_STATES];
	}

	// Beta[k][i] := beta_k(s_i)
	Beta = new LongDouble*[data->NbMarqueur];
	for (int k = 0; k< data->NbMarqueur ; k++)
	{
		Beta[k]= new LongDouble[NB_STATES];
	}


	// Gamma[k][i] := gamma_k(s_i)
	Gamma = new LongDouble*[data->NbMarqueur];
	for (int k = 0; k< data->NbMarqueur; k++)
	{
		Gamma[k]= new LongDouble[NB_STATES];
	}

	// DeltaRTrans[k][i][j] := delta (Prob_k(s_j|s_i)) / delta r
	Ksi = new LongDouble**[data->NbMarqueur];
	for (int k = 0; k< data->NbMarqueur; k++)
	{
		Ksi[k]= new LongDouble*[NB_STATES];
		for (int i = 0; i < NB_STATES; i++)
			Ksi[k][i] = new LongDouble[NB_STATES];
	}

	//False positive and false negative initial values
	pi = FALSE_POSITIVE_INIT;
	nu = FALSE_NEGATIVE_INIT;

	PostProb =   new double**[data->NbMarqueur];
	for (int i = 0; i< data->NbMarqueur; i++)
	{
		PostProb[i]= new double*[TailleEchant];
		for (int j = 1; j <= TailleEchant; j++)
					PostProb[i][j-1] = new double[NB_STATES];
	}
	
	//Rétention dépendant du clone et leur initialisation
	int *ordre = data->ordre;
	enum Obs Pheno;
	Ret = new double[TailleEchant+1];
	for (int i = 1; i<= TailleEchant; i++)
	{
		for (int j = 0; j< data->NbMarqueur; j++)
		{
			Pheno  = GetEch(ordre[j], i);
			Ret[i]+=( Pheno == Obs0001 ) ? 1 : 0;	
		}
		Ret[i]/=data->NbMarqueur;	
	}
		
}

//-----------------------------------------------------------------------------
//  Initializing Transition probalilities for all intervals and the partial
//  derivative contributions
//-----------------------------------------------------------------------------
// Param  :
//   - the map (la carte)
// Return : none
//-----------------------------------------------------------------------------

void BJS_RHE::InitTransProb(const Carte *map)
{
	double theta,r;
	r = map-> ret;

	// Initial distribution for states
	P_0[ 0 ] = 1 - r;
	P_0[ 1 ] = r;
	//Partial derivatives
	DeltaRP_0[ 0 ] = -1;
	DeltaRP_0[ 1 ] = 1;

	// Emission probabilities
	PHI[0][0] = 1 - pi;
	PHI[0][1] = pi;
	PHI[1][0] = nu;
	PHI[1][1] = 1 - nu;

	for (int i = 2 ; i< NB_ALPHA ; i++ )
	{
		PHI[0][i] = 0;
		PHI[1][i] = 0;
	}
	PHI[0][15] = 1;
	PHI[1][15] = 1;

	for (int i = 0 ; i< map->NbMarqueur-1; i++)
	{
		theta = map->tr[i];

		//Transition probabilities
		Trans[i][0][0] = 1 - theta * r			;
		Trans[i][0][1] = theta * r				;
		Trans[i][1][0] = theta * (1 - r)		;
		Trans[i][1][1] = 1 - theta * (1 - r)	;

		//Partial derivatives of transition probabilities with respect to the breakage frequency
		DeltaThetaTrans[i][0][0] =  -1 * r		;
		DeltaThetaTrans[i][0][1] =  r			;
		DeltaThetaTrans[i][1][0] =  1 - r		;
		DeltaThetaTrans[i][1][1] =  r - 1		;

		//Partial derivatives of transition probabilities with respect to the rDeltaRTransetention fraction
		DeltaRTrans[i][0][0] =  -1 * theta		;
		DeltaRTrans[i][0][1] =  theta			;
		DeltaRTrans[i][1][0] =  -1 * theta		;
		DeltaRTrans[i][1][1] =  theta			;
	}

	for (int i = 0 ; i< map->NbMarqueur; i++)
	{
		ScoreVector[i] = 0; //At each EM iteration the score vector is initialized to 0
	}

}

void BJS_RHE::InitTransProbClone(const Carte *map)
{
	double theta;

	// Emission probabilities
	PHI[0][0] = 1 - pi;
	PHI[0][1] = pi;
	PHI[1][0] = nu;
	PHI[1][1] = 1 - nu;

	for (int i = 2 ; i< NB_ALPHA ; i++ )
	{
		PHI[0][i] = 0;
		PHI[1][i] = 0;
	}
	PHI[0][15] = 1;
	PHI[1][15] = 1;

	for (int i = 0 ; i< map->NbMarqueur-1; i++)
	{
		theta = map->tr[i];

		//Partial derivatives of transition probabilities with respect to the rDeltaRTransetention fraction
		DeltaRTrans[i][0][0] =  -1 * theta		;
		DeltaRTrans[i][0][1] =  theta			;
		DeltaRTrans[i][1][0] =  -1 * theta		;
		DeltaRTrans[i][1][1] =  theta			;
	}

	for (int i = 0 ; i< map->NbMarqueur; i++)
	{
		ScoreVector[i] = 0; //At each EM iteration the score vector is initialized to 0
	}

}

//-----------------------------------------------------------------------------
//  Initializing Transition probalilities for all intervals and the partial
//  derivative contributions for the clone Ind
//-----------------------------------------------------------------------------
// Param  :
//   - the map (la carte)
// Return : none
//-----------------------------------------------------------------------------

void BJS_RHE::TransProbCloneSpec(int Ind, const Carte *map)
{
	double theta;
	
	// Initial distribution for states
	P_0[ 0 ] = 1 - Ret[Ind];
	P_0[ 1 ] = Ret[Ind];
	//Partial derivatives
	DeltaRP_0[ 0 ] = -1;
	DeltaRP_0[ 1 ] = 1;

	for (int i = 0 ; i< map->NbMarqueur-1; i++)
	{
		theta = map->tr[i];
		Ret[Ind] = map->ret;
		
		//Transition probabilities
		Trans[i][0][0] = 1 - theta * Ret[Ind]		;
		Trans[i][0][1] = theta * Ret[Ind]			;
		Trans[i][1][0] = theta * (1 - Ret[Ind])		;
		Trans[i][1][1] = 1 - theta * (1 - Ret[Ind])	;

		//Partial derivatives of transition probabilities with respect to the breakage frequency
		DeltaThetaTrans[i][0][0] =  -1 * Ret[Ind]	;
		DeltaThetaTrans[i][0][1] =  Ret[Ind]		;
		DeltaThetaTrans[i][1][0] =  1 - Ret[Ind]	;
		DeltaThetaTrans[i][1][1] =  Ret[Ind] - 1	;

	}

}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//-----------------------------------------------------------------------------
// Paramètres :
// - une carte
// - seuil de convergence
// Valeur de retour : la vraisemblance maximum
//-----------------------------------------------------------------------------

/*
double BJS_RHE::ComputeEM(Carte *map)
{
  int i,DomainHit = 0;
  double CoutSauv, Cout = -1e100;
  int *ordre = map->ordre;
  double * expected;

  NbEMCall++;

  // si la carte est deja finement estimee, on retourne la logv
  if (map->Converged <= Epsilon2) return map->coutEM;

  PreparEM(map);

  // + 1 = 2 cases en + pour EBreak & ERetained
  expected = new double[map->NbMarqueur + 1];

   for (i = 0; i < map->NbMarqueur + 1; i++) expected[i] = 0.0;

   // si la carte a deja ete estimee grossierement, on efface pas le boulot !
   if (map->Converged > Epsilon1) {
     for (i = 0; i < map->NbMarqueur - 1; i++)
       map->tr[i] = GetTwoPointsFR(ordre[i],ordre[i+1]);
     map-> ret = 0.3;
   }

   // on estime finement
  do   {
    CoutSauv = Cout;
    Cout = ComputeExpected(map, expected);
		  //printf("LogVraisemblance: %Le\n",Cout);

    if (((Cout - CoutSauv) < -Close2Zero) && !DomainHit)
      NbEMUnconv++;

    DomainHit |= Maximize(map, expected);
  }
  while (Cout-CoutSauv > Epsilon2);

  if (DomainHit) NbEMHit++;

  map->Converged = Epsilon2;
  map->coutEM = Cout;

  delete [] expected;

  NetEM(map);

  return Cout;
}
*/

//-----------------------------------------------------------------------------
//  A single step of the EM algorithm
//-----------------------------------------------------------------------------
// Parameters :
// - the map
// - the expectation vector
// Return : loglikelihood
//-----------------------------------------------------------------------------

/*void BJS_RHE::_DumpTransProb(void)
{
		
	
	
}
*/

//LongDouble BJS_RHE::ComputeExpectedLongDouble(const Carte* map, double *expected)
double BJS_RHE::ComputeExpected(const Carte* map, double *expected)
{
 double loglike = 0.0;
 LongDouble likelihood = 0.0;
 double theta, r;
 double ExpectedFragments = NbMeiose;

 //Initialize the transition probabilities and other quantities relying on the current estimates
 //InitTransProbClone(map);
 InitTransProb(map);
 
 int NbM = map->NbMarqueur;

 //Initialize error estimation
 FalsePositiveEstimate = 0;
 FalseNegativeEstimate = 0;
 
 for (int i = 1 ; i <= TailleEchant; i++)  {
	 //TransProbCloneSpec(i,map);					  //Clone dependant transition probabilities (specific clone retention fraction) TODO
	 likelihood = Backward(i,map);                   //First backward
	 loglike += Forward(i,map,likelihood);           //then forward
 }

 //Breakage frequencies update
 //  theta_new = theta_old + (theta_old * (1-theta_old) * ScoreVector[i])/(double)NbMeiose;
 for (int i = 0; i < map->NbMarqueur - 1; i++) {
 	 theta = map->tr[i];
 	 expected[i] = theta*NbMeiose + theta * (1-theta) * ScoreVector[i];
 	 ExpectedFragments +=   expected[i];
 	 //sprintf(bouf,"int %d, theta = %4.2f \n",i,theta);
 }

 //Retention fraction update
 r = map->ret;
 ERetained = r*ExpectedFragments + r*(1-r)*ScoreVector[map->NbMarqueur - 1];
 //ERetained = r*ExpectedFragments;
 EBreak = ExpectedFragments;

 //False Positive and False negative update

 pi = FalsePositiveEstimate/NbMeiose;
 nu = FalseNegativeEstimate/NbMeiose;


 LogLikeliHood = loglike;

 return (loglike);
}

//-----------------------------------------------------------------------------
//  HMM Forward algorithm
//-----------------------------------------------------------------------------
// Param :
// - individual
// - the map
// Return : loglikelihood
//-----------------------------------------------------------------------------

double BJS_RHE::Forward(int Ind, const Carte *map, LongDouble likelihood)
{
  int i,j,k;
  int *ordre = map->ordre;
  LongDouble likelihood_check = 0.0;
  int Pheno;
  LongDouble score_theta, score_r;

  //Variables for error estimation
  int PhenoL;
  LongDouble score_error[2][2];
  LongDouble expected_trials[2];

  score_error[0][0] = score_error[1][0] = score_error[0][1] = score_error[1][1] = 0;
  expected_trials[0] = expected_trials[1] = 0;

  Pheno = GetEch(ordre[0], Ind);

  score_r = 0;
  for (j = 0; j< NB_STATES; j++)
  {
	  Alpha[0][j] = P_0[j] * PHI[j][Pheno];
	  score_r += DeltaRP_0[j] * PHI[j][Pheno] * Beta[0][j];
  }

  for (i = 1; i< map->NbMarqueur; i++)
  {
	  PhenoL = GetEch(ordre[i-1], Ind);
	  Pheno  = GetEch(ordre[i], Ind);

	  score_theta = 0;

	  for (j = 0; j< NB_STATES; j++)
	  {
		  Alpha[i][j] =  0;

		  for (k = 0; k < NB_STATES; k++)
		  {
			  Alpha[i][j] += Alpha[i-1][k] * Trans[i-1][k][j];
		  }
		  Alpha[i][j] *= PHI[j][Pheno];

		  //Baum-Welch related structure
		  Gamma[i-1][j] = 0;
		  for (k = 0; k < NB_STATES; k++)
		  {
			  Ksi[i-1][j][k] = Alpha[i-1][j] * Trans[i-1][j][k] * PHI[k][Pheno] * Beta[i][k];
			  Gamma[i-1][j] += Ksi[i-1][j][k];

			  //Recurrence: Lange implementation of EM algorithm (partial derivatives)
			  score_theta += Alpha[i-1][j]*DeltaThetaTrans[i-1][j][k]*PHI[k][Pheno]*Beta[i][k];
			  score_r     += Alpha[i-1][j]*DeltaRTrans[i-1][j][k]*PHI[k][Pheno]*Beta[i][k];
		  }
		  //Error estimation
		  if ( PhenoL != Obs1111 )
		  {
			  score_error[PhenoL][j] += Gamma[i-1][j];
			  expected_trials[j] += Gamma[i-1][j];
		  }
	
		  // We store the posterior probability using the marker order
		  PostProb[i-1][Ind-1][j]=Gamma[i-1][j]/likelihood;

	  }
	  ScoreVector[i-1] += score_theta/likelihood;
  }
  int LastMarker = map->NbMarqueur - 1;

  ScoreVector[ LastMarker ] += score_r/likelihood;

  //Posterior probability for the last marker and likelihood check
  for (j = 0; j< NB_STATES; j++)
  {		
	  //PostProb[ordre[LastMarker]-1][Ind-1][j]=Alpha[ LastMarker ][j]/likelihood;
	  PostProb[LastMarker ][Ind-1][j]=Alpha[ LastMarker ][j]/likelihood;  //We now store the posterior probability using the marker order
	  likelihood_check +=  Alpha[ LastMarker ][j] ;
  }

  //Error estimation update for the hybrid
  FalsePositiveEstimate += score_error[1][0]/expected_trials[0];
  FalseNegativeEstimate += score_error[0][1]/expected_trials[1];

  return (double) log10l(likelihood);

}

//-----------------------------------------------------------------------------
//  HMM Backward algorithm
//-----------------------------------------------------------------------------
// Param :
// - individual
// - the map
// Return : loglikelihood
//-----------------------------------------------------------------------------

LongDouble BJS_RHE::Backward(int Ind, const Carte *map)
{
  int i,j,k;
  int *ordre = map->ordre;
  LongDouble likelihood = 0.0;
  int Pheno;
  int LastMarker = map->NbMarqueur - 1;

  for (j = 0; j< NB_STATES; j++)
  {
	  Beta[LastMarker][j] = 1;
  }

  for (i = LastMarker-1 ; i >= 0 ; i--)
  {
	  Pheno = GetEch(ordre[i+1], Ind);

	  for (j = 0; j< NB_STATES; j++)
	  {
		  Beta[i][j] =  0;

		  for (k = 0; k < NB_STATES; k++)
		  {
			  Beta[i][j] +=  Trans[i][j][k]*PHI[k][Pheno]*Beta[i+1][k];
		  }
	  }
  }

  Pheno = GetEch(ordre[0], Ind);

  for (j = 0; j< NB_STATES; j++)
  {
	  likelihood +=  P_0[j] * PHI[j][Pheno] * Beta[ 0 ][j] ;
  }

  return likelihood;

}

//-----------------------------------------------------------------------------
//  Cleaning the data structure used by EM
//-----------------------------------------------------------------------------
// Param :
// - the map.
// Returns :
// - none
//-----------------------------------------------------------------------------

void BJS_RHE::NetEM(const Carte *data)
{
	for (int i = 0; i< data->NbMarqueur-1; i++)
	{
		for (int j = 0; j < NB_STATES; j++)
			delete Trans[i][j];
		delete Trans[i];
	}
	delete [] Trans;

	//Partial derivatives of transition probabilities
	// DeltaThetaTrans[k][i][j] := delta (Prob_k(s_j|s_i)) / delta theta_k
	for (int i = 0; i< data->NbMarqueur-1; i++)
	{
		for (int j = 0; j < NB_STATES; j++)
			delete DeltaThetaTrans[i][j];
		delete DeltaThetaTrans[i];
	}
	delete [] DeltaThetaTrans;

	// DeltaRTrans[k][i][j] := delta (Prob_k(s_j|s_i)) / delta r
	for (int i = 0; i< data->NbMarqueur-1; i++)
	{
		for (int j = 0; j < NB_STATES; j++)
			delete DeltaRTrans[i][j];
		delete DeltaRTrans[i];
	}
	delete [] DeltaRTrans;

	//Score vector for the partial derivative
	delete [] ScoreVector;

	for (int i = 0; i< data->NbMarqueur; i++)
	{
		delete Alpha[i];
	}
	delete [] Alpha;

	for (int i = 0; i< data->NbMarqueur; i++)
	{
		delete Beta[i];
	}
	delete [] Beta;
}

// NOTE : Thomas F temporary workaround for printing error estimation
//-----------------------------------------------------------------------------
// Affichage simple s�lectif d'une carte
// Seuls les marqueurs du g�notype sont affich�s
//-----------------------------------------------------------------------------
// Param�tres :
// - la carte
// Valeur de retour :
//----------------------------------------------------------------------------

void BJS_RHE::PrintMap(Carte *data) const
{
   print_out( "Loglikelihood = %e, retention = %.2f Error pi = %.4f, nu = %.4f \n",LogLikeliHood,data->ret,pi,nu);
}


// NOTE : printing the map and adding error estimation
//-----------------------------------------------------------------------------
// Affichage d�taill� d'une carte
//-----------------------------------------------------------------------------
// Param�tres :
// - la carte
// - le sens
// - la carte de r�f�rence
// Valeur de retour :
//----------------------------------------------------------------------------

void BJS_RHE::PrintDMap(Carte *data, int envers, Carte *dataref)
{

  int i,j,k,itr,suivant;
  unsigned int maxl;
  double d, totald = 0.0;
  /*double dk, totaldk = 0.0;*/
  char form[128];
  int *pos;

  pos = new int[data->NbMarqueur];

  // récupération de la taille la plus grande d'un nom de locus.

  maxl = 0;
  for (i = 0; i < data->NbMarqueur; i++) {
    pos[i] = i + 1;
    j = Cartage->markers[data->ordre[i]].Represents;
    while (j != 0) {
      if (Cartage->markers.keyOf(j).size() > maxl)
	maxl = Cartage->markers.keyOf(j).size();
	j = Cartage->markers[j].Represents;
    }
    if (Cartage->markers.keyOf(data->ordre[i]).size() > maxl)
      maxl = Cartage->markers.keyOf(data->ordre[i]).size();
  }

  // cas de la fusion sur l'ordre, détermination de la position absolue

  if (data != dataref) {
    // recherche pour chaque marqueur de sa position
    for (i = 0; i < data->NbMarqueur; i++) {
      j = 0;
      while ( data->ordre[i] != dataref->ordre[j])
	j++;
      if (envers)
	pos[data->NbMarqueur - i - 1] = dataref->NbMarqueur - j;
      else
	pos[i] = j + 1;
    }
  }

  //Estimation des probabilités a posteriori d'erreur pour chaque marqueur

  print_out( "\nData Set Number %2d :\n", Id);
  print_out(  "\n");

    sprintf(form, "               %c%d%cDistance      Cumulative   Theta        2pt\n", '%', maxl +4, 's');
    print_out( form, "Markers ");

    sprintf(form, " Pos  Id %c%d%c                                   (%%sage)       LOD\n\n",'%', maxl, 's');
    print_out( form, "name","%%");

  for (k = 0; k < data->NbMarqueur; k++) {

	if (envers) {
	  i = data->NbMarqueur - k - 1;
	  itr = i - 1;
	  suivant = itr;
	} else {
	  i = k;
	  itr = i;
	  suivant = i +1;
	}

	j = Cartage->markers[data->ordre[i]].Represents;
	while (j != 0) {
	  sprintf(form, "%s %c%d%c ","%3d %3d",'%', maxl, 's');
	  print_out( form, pos[k], j, Cartage->markers.keyOf(j).c_str());

	  if (HasRH())
	    print_out( "         %6.1f cR    %7.1f cR    %5.1f %%%%     ------\n", 0.0, totald, 0.0);
	  else
	    print_out( "       %6.1f cM   %6.1f cM   %6.1f cM   %5.1f %%%%   ------\n",
		    0.0, totald, 0.0, 0.0);

	  j = Cartage->markers[j].Represents;
	}

    sprintf(form, "%s %c%d%c ","%3d %3d",'%', maxl, 's');
    print_out( form, pos[k], data->ordre[i],
	   Cartage->markers.keyOf(data->ordre[i]).c_str());

    if (k < data->NbMarqueur-1)
    {
    	d = Theta2Ray(data->tr[itr])*100;
    	print_out( "         %6.1f cR    %7.1f cR    %5.1f %%%%     %5.1f\n",
					d, totald, data->tr[itr]*100,
					GetTwoPointsLOD(data->ordre[i],data->ordre[suivant])
					);
		totald += d;
    }
  }

	  print_out(  "         ---------\n");
	  sprintf(form, "             %c%d%c     %s\n",'%', maxl, 's',"%6.1f cR\n");
	  print_out( form, " ", totald);
	  print_out( "\n      %4d markers, log10-likelihood = %8.2f",
	       data->NbMarqueur,
	       (data->coutEM));
	  print_out( "\n                    log-e-likelihood = %8.2f",
	       (data->coutEM*log((double) 10)));
	  print_out( "\n                    retention proba. = %8.2f",
	       data->ret);
	  print_out( "\n         Error rate (false positive) = %8.2f",
		       pi);
	  print_out( "\n         Error rate (false negative) = %8.2f",
		       nu);

      print_out(  "\n");

      print_out("Loglikelihood = %6.2f\t%6.1f\t%4.2f\t%6.4f\t%6.4f\n",LogLikeliHood,totald,data->ret,pi,nu);

      delete [] pos;
}


int BJS_RHE::Imputation(const Carte* map, double ConversionCutoff, double CorrectionCutoff, double UnknownCorrectionCutoff)
{
	//We propose here to display the posterior probabilities
	enum Obs Pheno;
	Obs CorrectedPheno;
	int *ordre = map->ordre;
	int Corrected = 0;
	int Conserved = 0;
	int Converted = 0;
	int Total = 0;
	double MarkerErrorProb;
	unsigned int known;
	char c;
	
	// For a yet unknown reason GetEch does not return an enum Obs value bu instead 0(A), 1(H) and for 15(-)
		
	for (int i = 0; i< map->NbMarqueur; i++)
	{
	   MarkerErrorProb = 0;
	   known = 0;
	   for (int j = 1; j <= TailleEchant; j++)
	   {
	   		Pheno  = GetEch(ordre[i], j);
	   		if ( Pheno != Obs1111 )
	   		{
				if ( PostProb[i][j-1][1-Pheno]> CorrectionCutoff )   // We invert the phenotype
				{
					c =( Pheno == Obs0110 ) ? 'a' : 'h';    //only for method evaluation purpose
					CorrectedPheno =( Pheno == Obs0001 ) ? Obs0000 : Obs0001 ;      
					Corrected++;
				}
				else if ( PostProb[i][j-1][1-Pheno] < ConversionCutoff ) // We don't change the phenotype
				{
					//c =( Pheno == Obs0110 ) ? 'h' : 'a';    //only for method evaluation purpose
					CorrectedPheno = Pheno ;     
					Conserved++;
				}
				else  // Intermediate value we convert to unknown
				{
					CorrectedPheno = Obs1111;
					Converted++;
				}
				MarkerErrorProb+=PostProb[i][j-1][1-Pheno];
				known++;
	   		}
	   		else  // The phenotype was unknown we see if we can impute it
	   		{
	   			if ( PostProb[i][j-1][1] > UnknownCorrectionCutoff )
	   			{
	   				c = 'h'; //only for method evaluation purpose
	   				CorrectedPheno = Obs0001;                   // Warning not an enum Obs value 
	   				Corrected++;
	   			}
	   			else if ( PostProb[i][j-1][0] > UnknownCorrectionCutoff )
	   			{
	   				c= 'a';  //only for method evaluation purpose
	   				CorrectedPheno = Obs0000;                     // Warning not an enum Obs value 
	   				Corrected++;
	   			}
	   			else
	   			{
	   				CorrectedPheno = Obs1111;   //Obs1111 = 15 <=> '-'
	   				Conserved++;
	   			}

	   		}
	   		SetEch(ordre[i],j,CorrectedPheno);
	   }

	   //fprintf(foutlog,"%s \t %3.2f\n",Cartage->markers.keyOf(i+1).c_str(),MarkerErrorProb/known);
	}
	//fclose(fouttmp);

	Total = Conserved + Corrected + Converted;

	return 0;
}




