//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: mcmc-no_gmp.cc,v 1.2 2009-12-01 15:45:00 dleroux Exp $
//
// Description : Markov Chain Monte Carlo.
// Divers : 
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <math.h>
#include <assert.h>
#ifdef ppc
#include "/Developer/Headers/FlatCarbon/fp.h"
#endif
#include "CartaGene.h"
#include "BioJeu.h"
#include "BioJeuSingle.h"
#include "BJS_RH.h"
#include "BJS_BC.h"
#include "BJS_OR.h"
#include "BJM_OR.h"
#include "BioJeuMerged.h"
#include "tsp.h"
#include "NOrCompMulti.h"
#include "packedarray.h"

#ifdef sun4
#include <ieeefp.h>
#endif

// (DL)
//#ifdef __WIN32__
#if defined(__WIN32__)&&!defined(__MINGW32__)
#include <float.h>
#endif

#ifndef NDEBUG
#define POUTF(_fmt, ...) do { sprintf(bouf, _fmt ,##__VA_ARGS__); pout(bouf); } while(0)
#else
#define POUTF(_fmt, ...)
#endif

#ifdef __WIN32__
/*#define _log10_ ((long double)2.30258509299404568401799145468)*/
#define powl(_a, _b) expl(logl((long double)(_a))*((long double)(_b)))
#else
/*#define powl(_a, _b) powl(((long double) (_a)), ((long double) (_b)))*/
#endif


#ifndef __WIN32__
#include <sys/types.h>
#include <unistd.h>
#else
#include <windows.h>
#endif 

#ifdef LKHLICENCE
#include "LKH.h"
#endif

#if defined(__WIN32__)&&!defined(__MINGW32__)
#include <hash_map>
using namespace stdext;
using namespace std;
#else
#include <ext/hash_map>
using namespace __gnu_cxx;
#endif

#include <algorithm>
#include <vector>

/* MCMC user-defined parameters */
const long long MAXDISTRIB = 95; /* show only 95% of distribution */
const int MAXNBKEYS = 100000; /* maximum number of different visited maps */
const int WEIGHT_GRANULARITY = 100;
/* end of MCMC user-defined parameters */

#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

/* Returns a uniform random deviates between 0.0 and 1.0 (exclusive of the endpoint values). */
/* see Numerical Recipes in C, page 282 */
float ran2(long *idum)
{
	int j;
	long k;
	static long idum2=123456789;
	static long iy=0;
	static long iv[NTAB];
	float temp;

	if (*idum <= 0) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		idum2=(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ1;
			*idum=IA1*(*idum-k*IQ1)-k*IR1;
			if (*idum < 0) *idum += IM1;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ1;
	*idum=IA1*(*idum-k*IQ1)-k*IR1;
	if (*idum < 0) *idum += IM1;
	k=idum2/IQ2;
	idum2=IA2*(idum2-k*IQ2)-k*IR2;
	if (idum2 < 0) idum2 += IM2;
	j=iy/NDIV;
	iy=iv[j]-idum2;
	iv[j] = *idum;
	if (iy < 1) iy += IMM1;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}

// (DL)
//#ifdef __WIN32__
#if defined(__WIN32__)&&!defined(__MINGW32__)
typedef packed_array_t Keytype_ptr;
class Keytype {
	public:
		Keytype_ptr key;
		Keytype() {key = NULL;}
		Keytype(packed_array_t k) {key = k;}
		operator packed_array_t(){return key;}
};
namespace stdext {
	/*size_t hash_value<Keytype>(const Keytype& key) {return pa_hash(key.key);}*/
	size_t hash_value(const Keytype key) {return hash_value((unsigned long) pa_hash(key.key));}
};
#define K(_k) (_k).key
/*bool operator<(const Keytype key1, const Keytype key2) {return mpz_cmp(key1.key, key2.key) < 0;}*/
bool operator<(const Keytype key1, const Keytype key2) {return pa_compare(K(key1), K(key2)) < 0;}
#define HASHMAP hash_map<Keytype, int>
#else
typedef packed_array_t Keytype;
struct eqkey
{
	bool operator()(const Keytype key1, const Keytype key2) const
	{
		/*return mpz_cmp(key1, key2) == 0;*/
		return pa_compare(key1, key2)==0;
	}
};
struct hashkey
{
	size_t operator()(const Keytype key) const
	{
		hash<unsigned long> H;
		/*return H(mpz_get_ui(key));*/
		return H(pa_hash(key));
	}
};
#define HASHMAP hash_map<Keytype, int, hashkey, eqkey>
#define K(_k) (_k)
#endif

long double Count[MAXNBKEYS];
int KeyIndex[MAXNBKEYS];
int cmpKeys(const void *key1, const void *key2)
{
	long double c1 = Count[*((int *) key1)];
	long double c2 = Count[*((int *) key2)];
	if (c1 > c2) return -1;
	else if (c1 < c2) return 1;
	else return 0;
}

void getHashKey(int *tour, int size, Keytype key)
{
	assert(K(key)->maxitems==size);
	pa_pack(K(key), tour);
}

void getTour(int *tour, int size, Keytype key)
{
	assert(K(key)->maxitems==size);
	pa_unpack(K(key), tour);
}


double CartaGene::GetLambda(int jeuOrdre) {
	// test de l'existence des jeux à fusionner

	if ( jeuOrdre > NbJeu || jeuOrdre < 1) 
	{
		sprintf(bouf, "Error : Unknown Data Set %d.\n", jeuOrdre);
		perr(bouf);
		return -1;
	}

	if ( Jeu[jeuOrdre]->Cross == Mor) {
		return GetLambda(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id) + GetLambda(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id);
	}
	if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

	return ((BJS_OR *) Jeu[jeuOrdre])->Lambda;
}


	int CartaGene::GetPositionInOrder(int jeuOrdre, int marq) {
		if ( jeuOrdre > NbJeu || jeuOrdre < 1) 
		{
			sprintf(bouf, "Error : Unknown Data Set %d.\n", jeuOrdre);
			perr(bouf);
			return -1;
		}

		if ( Jeu[jeuOrdre]->Cross == Mor) {
			return GetPositionInOrder(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id,marq) +  
				GetPositionInOrder(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id,marq); 
		}
		if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

		return ((BJS_OR *) Jeu[jeuOrdre])->GetMarqPos(marq);
	}

	int CartaGene::GetChromosome(int jeuOrdre, int marqpos) {
		if ( jeuOrdre > NbJeu || jeuOrdre < 1) 
		{
			sprintf(bouf, "Error : Unknown Data Set %d.\n", jeuOrdre);
			perr(bouf);
			return -1;
		}

		if ( Jeu[jeuOrdre]->Cross == Mor) {
			return GetChromosome(((BJM_OR *) Jeu[jeuOrdre])->BJgauche->Id,marqpos) +  
				GetChromosome(((BJM_OR *) Jeu[jeuOrdre])->BJdroite->Id,marqpos); 
		}
		if ( Jeu[jeuOrdre]->Cross != Ordre) return 0;

		return ((BJS_OR *) Jeu[jeuOrdre])->Chromosome[marqpos];
	}

class paircmp {
	public:
		bool operator() (pair<int,int> e1 , pair<int,int> e2) const {
			return e1.first<e2.first;
		}
};


/* Markov Chain Monte Carlo */
void CartaGene::mcmc(int seed, int nbiter, int burning, funcloglike1 mycontribLogLike2pt1, funcloglike2 mycontribLogLike2pt2)
{


	/* some basic verifications */
	if (NbMS == 0) 
	{
		sprintf(bouf,  "Error : Empty selection of loci.\n");
		perr(bouf);
		return;
	}
	if (Heap.HeapSize == 0) {
		sprintf(bouf,  "Error : Empty heap.\n");
		perr(bouf);
		return;
	}
	int ok = 0;
	for (int i=1; i<=NbJeu; i++) {
		if (Jeu[i]->Cross == Ordre) {
			ok = 1;
			break;
		}
	}
	if (!ok) {
		sprintf(bouf,"Error: there is no reference order data set loaded! The current data set should be merged by order from one Biological Data Set and one reference order data set.\n");
		perr(bouf);
		return;
	}

	Chronometre cpu = Chronometre();
	cpu.Init();

	// MCMC output files
	FILE *relpos;
	char filename[18];
#ifndef __WIN32__
	int pid=(int)getpid();
#else
	int pid=(int)GetCurrentProcessId();
#endif


	/* computes pairwise contributions to 2-point loglikelihood */
	// bring back coef to 0 to turn off prior linearization
	// Save the value for the end
	double coefsave=SetBreakPointCoef(ArbreJeu->Id,0.);
	double Lambda=GetLambda(ArbreJeu->Id);
	bool unif = (Lambda<0);
	int l=0;
	if (unif) {
		l=abs(int(Lambda));
	}

	double *rh = new double[(NbMS+1)*(NbMS+1)];
	ArbreJeu->InitContribLogLike2pt(); 

	for (int m1 = 0; m1 <= NbMS; m1++) {
		for (int m2 = 0; m2 <= NbMS; m2++) {
			if (m1 == m2) {
				rh[m1*(NbMS+1)+m2] = 0;
			} else {
				if (m1 == 0 || m2 == 0) {
					if (m1 == 0) {
						rh[m1*(NbMS+1)+m2] = -mycontribLogLike2pt1(ArbreJeu, MarkSelect[m2-1]);
					} else {
						rh[m1*(NbMS+1)+m2] = -mycontribLogLike2pt1(ArbreJeu, MarkSelect[m1-1]);
					}	    
				} else {
					rh[m1*(NbMS+1)+m2] = -mycontribLogLike2pt2(ArbreJeu, MarkSelect[m1-1],MarkSelect[m2-1]);
				}
			}
		}
	}

	// Build the matrice for bp/no bp
	int *bp = new int[NbMS*NbMS];
	int bpmin=0; // number of obligate bp
	int bpmax; // max number of breakpoints
	for (int m1=0; m1 < NbMS; m1++) {
		for (int m2 = 0; m2 < NbMS; m2++) {
			if (m1 == m2) {
				bp[m1*NbMS+m2] = 0;
			} 
			else {
				bp[m1*NbMS+m2] = 1;
			}
		}
	}

	// Stores: position of marker on the ref order (.first), position of marker in MarkSelect (.second)
	vector< pair<int,int> > mkpairs(NbMS);
	for (int m1=0; m1<NbMS; m1++) {
		mkpairs[m1]=std::pair<int,int>(GetPositionInOrder(ArbreJeu->Id,MarkSelect[m1]),m1);
	}
	// Sort by position on the ref order
	std::sort(mkpairs.begin(),mkpairs.end(),paircmp());


	for (int m=0; m<(NbMS-1); m++) {
		int prev=mkpairs[m].second;
		int next=mkpairs[m+1].second;
		if (GetChromosome(ArbreJeu->Id,mkpairs[m].first) == GetChromosome(ArbreJeu->Id,mkpairs[m+1].first)) {
			bp[prev*NbMS+next]=0;
			bp[next*NbMS+prev]=0;
		}
		else 
			bpmin++;
	}
	bpmax=NbMS-1-bpmin;

	/* copy the best map order found in Carthagene heap as our initial tour */
	int *tour = new int[NbMS+1];
	int *bestour = new int[NbMS+1];
	// Some temporary tours 
	int *maptour2best = new int[NbMS]; // position of marker i in bestour
	int *tmpm2b = new int[NbMS];
	int *tmptour = new int[NbMS+1];
	//  int *proptour = new int[NbMS+1];
	// keep track of the current number of breakpoints 
	int kcur,knew,kprop;
	int kest,kestnew,kestprop;

	Carte *InitMap = Heap.Best();
	Carte *CurMap = new Carte(*InitMap);
	Carte *TmpMap = new Carte(*CurMap);
	int inverseMarkSelect[MAXNBMARKERS];
	for (int i = 0; i < NbMS; i++) {
		inverseMarkSelect[MarkSelect[i]] = i + 1;
	}
	// In tour[k], we have the position (+1), for the kth marker, in array MrkSelect.
	tour[0] = 0; /* dummy marker */
	for (int k=1; k <= NbMS; k++) {
		tour[k] = inverseMarkSelect[InitMap->ordre[k - 1]];
	}

	bestour[0]=0;
	for (int k=0; k< NbMS; k++) {
		bestour[k+1] = tour[k+1];
		maptour2best[k]=k;
	}

	// Get the number of breakpoints of the initial tour
	// remember, here tour[0]=0 !, so we don't care about the first ''marker''
	kest=0;
	for (int ii = 1; ii <= NbMS; ii++) {
		if (ii<NbMS) {
			int prev=tour[ii]-1;
			int next=tour[ii+1]-1;
			kest+=bp[prev*NbMS+next];
		}
		TmpMap->ordre[ii-1] = MarkSelect[tour[ii] - 1];
	}
	kcur=BreakPointsMapPointer(ArbreJeu->Id,TmpMap);

	/* computes the 2-point loglikelihood of the initial tour */
	double loglike = 0;
	for (int k=1; k <= NbMS; k++) {
		loglike += rh[tour[k-1]*(NbMS+1)+tour[k]];
	}
	loglike += rh[tour[NbMS]*(NbMS+1)+tour[0]];
	if (unif) {
		int bp = kcur-bpmin;
		if (bp > l) {
			loglike += (double) log10l(0.01L/((bpmax-l)*0.5L*NOrComput->getNO(kcur)));
		}
		else {
			loglike += (double) log10l(0.99L/((l+1)*0.5L*NOrComput->getNO(kcur)));
		}
	}
	else {
		loglike += (double)log10l(powl((long double)Lambda, (long double)(kcur-bpmin))/(dfact(kcur-bpmin)*0.5L*NOrComput->getNO(kcur)));
	}
	double bestloglike = loglike;
	sprintf(bouf,"[%.2g]\n",loglike);
	pout(bouf);

	/* random number initialization */
	if (seed>0) seed = -seed;
	long idum = (seed < 0)?seed:-1;

	/* hash map for storing tour distribution */
	/*packed_array_t keys[MAXNBKEYS];*/
	/*for(int k=0;k<MAXNBKEYS;k++) {*/
	/*keys[k] = pa_new(NbMS+1);*/
	/*}*/
	packed_array_t keys = pa_multi_new(NbMS+1, MAXNBKEYS);	/* array of packed arrays */
	int freekeys = 0;
	HASHMAP distribution;
	double loglike2pt[MAXNBKEYS];

	/* insert current tour into distribution */
	/*mpz_t curkey;*/
	packed_array_t curkey = pa_new(NbMS+1);
	/*mpz_init(curkey);*/
	getHashKey(tour, NbMS+1, curkey);
	sprintf(bouf,"key#: %d 2pt_loglike: %.2f\n",freekeys, loglike);
	pout(bouf);
	/*mpz_init_set(keys[freekeys], curkey);*/
	pa_copy(curkey, &keys[freekeys]);
	distribution[&keys[freekeys]] = 0;
	loglike2pt[freekeys] = loglike;
	freekeys++;
	/*mpz_clear(curkey);*/
	pa_delete(curkey);

	/* iterate Markov Chain Monte Carlo simulation nbiter times */
	const int n = NbMS+1;
	long double *neighborhoodSum = new long double[n*n];
	long double *neighborhoodLogLike = new long double[n*n];
	long double *neighborhoodSum1D = new long double[n];
	long double *neighborhoodLogLike1D = new long double[n];
        double weight_floor = floor(-bestloglike/((double)WEIGHT_GRANULARITY));
        POUTF("weight_floor = %g bestloglike=%g\n", weight_floor, bestloglike);
	int weight=(((int)weight_floor)-1)*WEIGHT_GRANULARITY;
        if(weight<0) { weight=0; }
	int lim_group_size=20;
	int max_group_size = 1;
	double saved_loglike=0;
        int half_NbMS = (NbMS+1)>>1;
	if (half_NbMS > lim_group_size) {
		max_group_size = lim_group_size;
	}
	else{
		max_group_size=half_NbMS;
	}
	sprintf(bouf,"Maximum transposed segment size = %d\n",max_group_size);
	pout(bouf);
	//int weight=1;

	int iter = 0;
	int accepted=0;
	int proposed=0;

	while (iter < nbiter) {
		/* evaluate 2-change neighborhood */
		long double sumlike = 0;
		for (int i = 0; i < n-2; i++) {
			for (int j = i+2; j < n; j++) {
				if (i==0 && j==n-1) continue;
				int t1 = tour[i];
				int t2 = tour[i+1];
				int t4 = tour[j];
				int t3 = (j<n-1)?tour[j+1]:tour[0];

				// Build New tour and get the nb of BP (prior contribution)
				//  		for (int k=0; k <= i; k++) {
				//  		  proptour[k] = tour[k];
				//  		}
				//  		for (int k=i+1; k <= j; k++) {
				//  		  proptour[k] = tour[j+i+1-k];
				//  		}
				//  		for (int k=j+1; k < n; k++) {
				//  		  proptour[k] = tour[k];
				//  		}

				// Recherche du nombre de BP avec ordre de ref (lent!!)
				// 	for (int ii = 0; ii < NbMS; ii++) {
				// 		  TmpMap->ordre[ii] = MarkSelect[proptour[ii+1] - 1];
				// 		}
				// 		kprop=BreakPointsMapPointer(ArbreJeu->Id,TmpMap);
				kest=kcur;
				if (i==0) 
					kestprop= kest - bp[(t3-1)*NbMS+(t4-1)] + bp[(t2-1)*NbMS+(t3-1)];
				else if (j==n-1)
					kestprop= kest - bp[(t1-1)*NbMS+(t2-1)] + bp[(t4-1)*NbMS+(t1-1)];
				else
					kestprop= kest - bp[(t1-1)*NbMS+(t2-1)] -  bp[(t3-1)*NbMS+(t4-1)] 
						+ bp[(t4-1)*NbMS+(t1-1)] +  bp[(t2-1)*NbMS+(t3-1)];
				kprop=kestprop;

				// Get the 2pt likelihood contribution
				long double newloglike = loglike + rh[t1*n+t4] + rh[t3*n+t2] - rh[t1*n+t2] - rh[t4*n+t3];
                                POUTF("newloglike = %Lg\n", newloglike);
				if (unif) {
					int bpcur = kcur-bpmin;
					int bpprop= kprop-bpmin;
					newloglike += log10l((long double)(NOrComput->getNO(kcur)/NOrComput->getNO(kprop)));
					if (bpcur <= l && bpprop > l) {
						newloglike += log10l(0.01L*(l+1)/(0.99L*(bpmax-l)));
					}
					else if (bpcur > l && bpprop <=l) {
						newloglike += log10l(0.99L*(bpmax-l)/(0.01L*(l+1)));
					}
					// autres cas bpcur et bpprop du meme cote de l donc a priori s'annule
				}
				else {
					if (kprop < kcur) {
						newloglike += log10l(powl(Lambda,kprop-kcur)*dfact2(kprop-bpmin+1,kcur-bpmin)*NOrComput->getNO(kcur)/NOrComput->getNO(kprop));
					} else {
						newloglike += log10l(powl(Lambda,kprop-kcur)/dfact2(kcur-bpmin+1,kprop-bpmin)*NOrComput->getNO(kcur)/NOrComput->getNO(kprop));
					}
				}

				neighborhoodLogLike[i*n+j] = newloglike;
                                POUTF("sumlike=%Lg newloglike=%Lg weight=%d\n", sumlike, newloglike, weight);
                                long double temp_debug = newloglike+(long double)weight;
                                POUTF("temp_debug = %Lg\n", temp_debug);
                                POUTF("10^temp_debug = %Lg\n", powl((long double)10., temp_debug));
				sumlike += powl((long double)10., (long double) (newloglike+(long double)weight));
                                POUTF("sumlike=%Lg\n", sumlike);
				neighborhoodSum[i*n+j] = sumlike;
				//  		sprintf(bouf,"i:%d j:%d t1:%d t4:%d [%.2f] %Lg\n",i,j,t1,t4,newloglike,sumlike);
				//  		pout(bouf);
			}
		}

		// 	bool stop = false;
		/* biased selection of a neighbor based on 2-point likelihoods */
                float ran2_val = ran2(&idum);
		long double randvalue = sumlike * ran2_val;
		POUTF("sumlike: %.3Lg ran2=%.3Lg randvalue: %.3Lg\n",sumlike, ran2_val, randvalue);
		int selecti = 0;
		int selectj = 0;
		for (int i = 0; i < n-2; i++) {
			for (int j = i+2; j < n; j++) {
				if (i==0 && j==n-1) continue;
				if (randvalue < neighborhoodSum[i*n+j]) {
					selecti = i;
					selectj = j;
					i = j = n;
				}
			}
		}
		if (selecti == selectj) {
			sprintf(bouf,  "Error in MCMC stochastic 2-opt selection.\n");
			perr(bouf);
			for (int ii=0; ii<n;ii++) {
				sprintf(bouf,"%4d ",tour[ii]);
				perr(bouf);
			}
			sprintf(bouf,"\n");
			perr(bouf);
			sprintf(bouf,"selecti = selectj = %d %d\n",selecti,selectj);
			perr(bouf);
			sprintf(bouf,"randvalue=%Lf sumlike = %Lf\n",randvalue,sumlike);
			perr(bouf);
			sprintf(bouf,"n= %d\n",n);
			perr(bouf);
			return;
		}
		// Compute bp for proposed move
		int t1 = tour[selecti];
		int t2 = tour[selecti+1];
		int t4 = tour[selectj];
		int t3 = (selectj<n-1)?tour[selectj+1]:tour[0];

		if (selecti==0) 
			kestnew= kest - bp[(t3-1)*NbMS+(t4-1)] + bp[(t2-1)*NbMS+(t3-1)];
		else if (selectj==n-1)
			kestnew= kest - bp[(t1-1)*NbMS+(t2-1)] + bp[(t4-1)*NbMS+(t1-1)];
		else
			kestnew= kest - bp[(t1-1)*NbMS+(t2-1)] -  bp[(t3-1)*NbMS+(t4-1)] 
				+ bp[(t4-1)*NbMS+(t1-1)] +  bp[(t2-1)*NbMS+(t3-1)];

		/* evaluate the transition probability to go back from the selected tour to the current tour */
		long double tmpsumlike = 0;
		long double tmploglike = neighborhoodLogLike[selecti*n+selectj];
		for (int k=0; k <= selecti; k++) {
			tmptour[k] = tour[k];
		}
		for (int k=selecti+1; k <= selectj; k++) {
			tmptour[k] = tour[selectj+selecti+1-k];
		}
		for (int k=selectj+1; k < n; k++) {
			tmptour[k] = tour[k];
		}
		// 	for (int i = 0; i < NbMS; i++) {
		// 	  TmpMap->ordre[i] = MarkSelect[tmptour[i+1] - 1];
		// 	}
		// 	knew=BreakPointsMapPointer(ArbreJeu->Id,TmpMap);
		// 	assert(knew==kestnew);
		knew=kestnew;

		for (int i = 0; i < n-2; i++) {
			for (int j = i+2; j < n; j++) {
				if (i==0 && j==n-1) continue;
				int t1 = tmptour[i];
				int t2 = tmptour[i+1];
				int t4 = tmptour[j];
				int t3 = (j<n-1)?tmptour[j+1]:tmptour[0];

				// Build New tour and get the nb of BP (for prior contribution)
				//  	    for (int k=0; k <= i; k++) {
				//  	      proptour[k] = tmptour[k];
				//  	    }
				//  	    for (int k=i+1; k <= j; k++) {
				//  	      proptour[k] = tmptour[j+i+1-k];
				//  	    }
				//  	    for (int k=j+1; k < n; k++) {
				//  	      proptour[k] = tmptour[k];
				//  	    }

				kest=knew;
				if (i==0) 
					kestprop= kest - bp[(t3-1)*NbMS+(t4-1)] + bp[(t2-1)*NbMS+(t3-1)];
				else if (j==n-1)
					kestprop= kest - bp[(t1-1)*NbMS+(t2-1)] + bp[(t4-1)*NbMS+(t1-1)];
				else
					kestprop= kest - bp[(t1-1)*NbMS+(t2-1)] -  bp[(t3-1)*NbMS+(t4-1)] 
						+ bp[(t4-1)*NbMS+(t1-1)] +  bp[(t2-1)*NbMS+(t3-1)];

				// 	    for (int ii = 0; ii < NbMS; ii++) {
				// 	      TmpMap->ordre[ii] = MarkSelect[proptour[ii+1] - 1];
				// 	    }
				// 	    kprop=BreakPointsMapPointer(ArbreJeu->Id,TmpMap);
				// 	    assert(kprop==kestprop);
				kprop=kestprop;

				////
				long double newloglike = tmploglike + rh[t1*n+t4] + rh[t3*n+t2] - rh[t1*n+t2] - rh[t4*n+t3];
				if (unif) {
					int bpnew = knew-bpmin;
					int bpprop= kprop-bpmin;
					newloglike += log10l(NOrComput->getNO(knew)/NOrComput->getNO(kprop));
					if (bpnew <= l && bpprop > l) {
						newloglike += log10l(0.01L*(l+1)/(0.99L*(bpmax-l)));
					}
					else if (bpnew > l && bpprop <=l) {
						newloglike += log10l(0.99L*(bpmax-l)/(0.01L*(l+1)));
					}
					// autres cas bpcur et bpprop du meme cote de l donc a priori s'annule
				}
				else {
					if (kprop < knew) {
						newloglike += log10l(powl(Lambda,kprop-knew)*dfact2(kprop-bpmin+1,knew-bpmin)*NOrComput->getNO(knew)/NOrComput->getNO(kprop));
					} else {
						newloglike += log10l(powl(Lambda,kprop-knew)/dfact2(knew-bpmin+1,kprop-bpmin)*NOrComput->getNO(knew)/NOrComput->getNO(kprop));
					}
				}
				////
				tmpsumlike += powl(10.L,(long double)(newloglike+weight));
			}
		}
		//    	  sprintf(bouf,"i:%d j:%d oldL:[%.2f] newL:[%.2f] sumfromold:[%.3Lg] sumfromnew:[%.3Lg]\n",selecti,selectj,loglike,tmploglike,sumlike,tmpsumlike);
		//    	  pout(bouf);

		/* test if the selected move is accepted */
		if (ran2(&idum) < sumlike/tmpsumlike) {
			//  		sprintf(bouf,"best 2-change accepted, new loglike: %.2f\n",neighborhoodLogLike[selecti*n+selectj]);
			//  		pout(bouf);

			/* performs 2-change */
			for (int k=selecti+1; k <= (selecti+1+selectj)/2; k++) {
				int swap = tour[k];
				tour[k] = tour[selecti+1+selectj-k];
				tour[selecti+1+selectj-k] = swap;
				// keep track of positions relative to best map
				swap = maptour2best[k-1];
				maptour2best[k-1] = maptour2best[selecti+selectj-k];
				maptour2best[selecti+selectj-k]=swap;
			}
			loglike = neighborhoodLogLike[selecti*n+selectj];
			kcur=knew;
			//fprintf(monitor,"%d\n",kcur);

			if (loglike > bestloglike + 0.001L) {
				bestloglike = loglike;
				if (!QuietFlag) {
					sprintf(bouf,"[%.2f]\n",bestloglike);
					pout(bouf);
				}
			}

			/* insert new tour into hashtable */

		} else {
			//  		sprintf(bouf,"best 2-change rejected, i:%d j:%d loglike: %.2f newloglike: %.2f sumlike: %.3Lg randvalue: %.3Lg\n",selecti,selectj,loglike,neighborhoodLogLike[selecti*n+selectj],sumlike,randvalue);
			//    		pout(bouf);
		}

		/* add current tour into distribution */
		if (iter < burning) {
			if ( (iter%10) == 0) {
				sprintf(bouf,"\rBurning: %.2f %% completed. time elapsed:%.2f sec. logLik : %f",
						100.0f*iter/burning,cpu.Read(),loglike);
				pout(bouf);
			}
		}
		if (iter >= burning) {
			/*mpz_t key;*/
			/*mpz_init(key);*/
			packed_array_t key = pa_new(n);
			getHashKey(tour, n, key);
			HASHMAP::iterator iterkey = distribution.find(key);
			if (iterkey == distribution.end()) {
				// 	    sprintf(bouf,"key#: %d 2pt_loglike: %.2f\n",freekeys, loglike);
				// 	    pout(bouf);
				/*mpz_init_set(keys[freekeys], key);*/
				pa_copy(key, &keys[freekeys]);
				distribution[&keys[freekeys]] = 1;
				loglike2pt[freekeys] = loglike;
				freekeys++;
				if (freekeys >= MAXNBKEYS) {
					sprintf(bouf,"Too many different maps (recompile with larger MAXNBKEYS limit)!\n");
					perr(bouf);
					iter = nbiter;
				}
			} else {
				((*iterkey).second)++;
			}
			if ( (iter%10) == 0) {
				sprintf(bouf,"\rMain iterations: %.2f %% completed. %5d maps visited. time elapsed:%.2f sec. logLik : %f",
						100.0f*(iter-burning)/(nbiter-burning),freekeys,cpu.Read(),loglike);
				pout(bouf);
			}

			/*mpz_clear(key);*/
			pa_delete(key);
		}

		// Gibbs sampling of marker positions 
		// move a group of size gsize 
		for (int i=0; i<NbMS; i++) {
			for (int gsize=1; gsize < max_group_size+1 ; gsize++) {
				long double sumlike = 0;
				int t=(int)floor((NbMS-gsize+1)*ran2(&idum));
				int ttl = tour[t+1]; // left mk in the group
				int ttr = tour[t+gsize]; // right mk in the group
				int t1 = tour[t]; // current left-side
				int t2 = (t<(NbMS-gsize))?tour[t+1+gsize]:tour[0]; // current right-side
				for (int tl=0; tl<(NbMS+1); tl++) {
					if ( t < tl && tl < (t+gsize+1) ) {// wrong because <-> insertion at t
						neighborhoodLogLike1D[tl] = 0; 
						neighborhoodSum1D[tl]=(tl==0)?0:neighborhoodSum1D[tl-1];
						continue; 
					}
					// this is only impacted by gsize if tl==t through t2
					int t3 = tour[tl]; // proposed left-side
					int t4 = (tl<NbMS)?((tl==t)?t2:tour[tl+1]):tour[0]; // proposed right-side

					// Update the number of breakpoints
					kestprop=kcur;
					if (t != 0) {
						kestprop -= bp[(t1-1)*NbMS+ttl-1];
					}
					if (t != NbMS-gsize) {
						kestprop -= bp[(ttr-1)*NbMS+t2-1];
					}
					if (t3 != 0) {
						kestprop += bp[(t3-1)*NbMS+ttl-1];
					}
					if (t4 != 0) {
						kestprop += bp[(ttr-1)*NbMS+t4-1];
					}
					if (t1 != 0 && t2 !=0) { // t1 and t2 are now adjacent
						kestprop += bp[(t1-1)*NbMS+t2-1];
					}
					if (t3 != 0 && t4 !=0) { // t3 and t4 are no longer adjacent
						kestprop -= bp[(t3-1)*NbMS+t4-1];
					}
					kprop=kestprop;
					long double newloglike = loglike + rh[t3*n+ttl] + rh[t4*n+ttr] - rh[t3*n+t4] - rh[t1*n+ttl] - rh[t2*n+ttr] + rh[t1*n+t2];
					assert(newloglike<0);
					if (unif) {
						int bpcur = kcur-bpmin;
						int bpprop= kprop-bpmin;
						newloglike += log10l(NOrComput->getNO(kcur)/NOrComput->getNO(kprop));
						if (bpcur <= l && bpprop > l) {
							newloglike += log10l(0.01L*(l+1)/(0.99L*(bpmax-l)));
						}
						else if (bpcur > l && bpprop <=l) {
							newloglike += log10l(0.99L*(bpmax-l)/(0.01L*(l+1)));
						}
						// autres cas bpcur et bpprop du meme cote de l donc a priori s'annule
					}
					else {
						if (kprop < kcur) {
							newloglike += log10l(powl(Lambda,kprop-kcur)*dfact2(kprop-bpmin+1,kcur-bpmin)*NOrComput->getNO(kcur)/NOrComput->getNO(kprop));
						} else {
							newloglike += log10l(powl(Lambda,kprop-kcur)/dfact2(kcur-bpmin+1,kprop-bpmin)*NOrComput->getNO(kcur)/NOrComput->getNO(kprop));
						}
					}
					assert(newloglike<0);
					neighborhoodLogLike1D[tl] = newloglike;
					sumlike += powl(10.L,(long double)(newloglike+weight));
					neighborhoodSum1D[tl] = sumlike;
				}
				// Draw a new position for marker group (ttl,ttr) from conditional distribution
				randvalue=sumlike*ran2(&idum);
				int selecti=0;

				for (int i=0; i<n; i++) {
					if (neighborhoodSum1D[i] > randvalue) {
						selecti=i;
						break;
					}
				}
				assert(selecti <= t || selecti > (t+gsize));
				int t3 = tour[selecti]; // proposed left-side
				int t4 = (selecti<NbMS)?((selecti==t)?t2:tour[selecti+1]):tour[0]; // proposed right-side
				if (iter >= burning) {
					proposed++;
					if (t3!=t1) 
						accepted++;
				}
				kestnew=kcur;
				if (t != 0) {
					kestnew -= bp[(t1-1)*NbMS+ttl-1];
				}
				if (t != NbMS-gsize) {
					kestnew -= bp[(ttr-1)*NbMS+t2-1];
				}
				if (t3 != 0)
					kestnew += bp[(t3-1)*NbMS+ttl-1];
				if (t4 != 0)
					kestnew += bp[(ttr-1)*NbMS+t4-1];
				if (t1 != 0 && t2 !=0) 
					kestnew += bp[(t1-1)*NbMS+t2-1];
				if (t3 != 0 && t4 !=0) 
					kestnew -= bp[(t3-1)*NbMS+t4-1];

				// Insert at new position (right of selecti)
				loglike = neighborhoodLogLike1D[selecti];
				if (selecti < t+1) {
					for (int m=0; m<(selecti+1); m++) {  
						tmptour[m]=tour[m];
						if (m>0) 
							tmpm2b[m-1]=maptour2best[m-1];
					}
					for (int m=0; m<gsize; m++) {
						tmptour[selecti+1+m]=tour[t+1+m];
						tmpm2b[selecti+m]=maptour2best[t+m];//tm2b is index from 0 to NbMS !!
					}
					for (int m=selecti+1+gsize; m<t+1+gsize; m++) {
						tmptour[m]=tour[m-gsize];
						tmpm2b[m-1]=maptour2best[m-gsize-1];
					}
					for (int m=t+1+gsize; m<n; m++) {
						tmptour[m]=tour[m];
						tmpm2b[m-1]=maptour2best[m-1];
					}
				}
				// A verif
				else {
					assert(selecti < (t+1) || selecti > (t+gsize));
					// fill in on the left: untouched markers
					for (int m=0; m<t+1; m++) {
						tmptour[m]=tour[m];
						if (m>0) 
							tmpm2b[m-1]=maptour2best[m-1];
					}
					// fill in on the left: translated markers
					for (int m=t+1; m<selecti+1-gsize; m++) {
						tmptour[m]=tour[m+gsize];
						tmpm2b[m-1]=maptour2best[m+gsize-1];
					}
					// fill in the moving markers
					for (int m=0; m < gsize; m++) {
						tmptour[selecti+1-gsize+m]=tour[t+1+m];
						tmpm2b[selecti-gsize+m]=maptour2best[t+m];
					}
					// fill in on the right
					for (int m=selecti+1; m<n; m++) {
						tmptour[m]=tour[m];
						tmpm2b[m-1]=maptour2best[m-1];
					}
				}
				//
				for (int ii=0; ii<n; ii++) {
					tour[ii]=tmptour[ii];
					if (ii > 0) {
						maptour2best[ii-1]=tmpm2b[ii-1];
					}
				}
				// 	  

				kcur=kestnew;
				// Insert new tour into stack
				if (iter >= burning) {
					/*mpz_t key;*/
					/*mpz_init(key);*/
					packed_array_t key = pa_new(n);
					getHashKey(tour, n, key);
					HASHMAP::iterator iterkey = distribution.find(key);
					if (iterkey == distribution.end()) {
						/*mpz_init_set(keys[freekeys], key);*/
						pa_copy(key, &keys[freekeys]);
						distribution[&keys[freekeys]] = 1;
						loglike2pt[freekeys] = loglike;
						freekeys++;
						if (freekeys >= MAXNBKEYS) {
							sprintf(bouf,"Too many different maps (recompile with larger MAXNBKEYS limit)!\n");
							perr(bouf);
							iter = nbiter;
						}
					} else {
						((*iterkey).second)++;
					}
					/*mpz_clear(key);*/
					pa_delete(key);
				}
			}

		}

		// End Gibbs Sampling of marker position

		iter++;
		if (StopFlag) {
			sprintf(bouf,"User interruption!\n");
			perr(bouf);
			iter = nbiter;
		}
	}
	sprintf(bouf,"\r100.00 completed. %5d maps visited. time elapsed:%.2f sec.",freekeys,cpu.Read());
	pout(bouf);				
	sprintf(bouf,"\nMCMC done\n");
	pout(bouf);				
	sprintf(bouf,"Gibbs: accepted/proposed = %f\n",((double)accepted)/proposed);
	pout(bouf);				

	nbiter=nbiter*(NbMS+1)*max_group_size;
	burning=burning*(NbMS+1);

	sprintf(filename,"%d.mcmc",pid);
	relpos=fopen(filename,"w");
	assert(relpos!=NULL);

	HASHMAP::iterator iterkey = distribution.find(&keys[0]);
	fprintf(relpos,"startmap: ");
	for (int i=0; i<NbMS; i++) {
		fprintf(relpos,"%s ",NomMarq[MarkSelect[bestour[i+1] - 1]]);
	}
	fprintf(relpos,"\n");
	//   fprintf(relpos,"startmap: ");
	//   for (int i=0; i<NbMS; i++) {
	//     fprintf(relpos,"%s ",NomMarq[MarkSelect[bestour[i+1]]]);
	//   }
	//   fprintf(relpos,"\n");

	// Compute Importance Sampling Weigths and adjust posterior probabilities
	long double K=0;
	long double *ISW = new long double[MAXNBKEYS];
	int nmaps=0;

	sprintf(bouf,"Computing IS weigths:\n");
	pout(bouf);				

	for (HASHMAP::iterator iterkey = distribution.begin(); iterkey != distribution.end(); ++iterkey) {
		// (DL)
		//#ifdef __WIN32__
		/*#if defined(__WIN32__)&&!defined(__MINGW32__)*/
		/*unsigned int pos = ((*iterkey).first.key) - ((Keytype_ptr) &keys[0]);*/
		/*#else*/
		/*unsigned int pos = ((*iterkey).first) - ((Keytype) &keys[0]);*/
		/*#endif*/
		unsigned int pos = pa_multi_get_index(keys, K((*iterkey).first));

		getTour(tour, n, (*iterkey).first);
		/* Get the map and compute multipoint posterior probability */
		int *order = new int[NbMS];
		for (int i = 0; i < NbMS; i++) {
			order[i] = MarkSelect[tour[i+1] - 1];
		}
		Carte *carte = new Carte(this, NbMS, order);
		kcur=BreakPointsMapPointer(ArbreJeu->Id,carte);
		long double loglikemultipoint = ComputeEM(carte);
		if (unif) {
			int bpcur = kcur-bpmin;
			loglikemultipoint += log10l(2.0L/NOrComput->getNO(kcur));
			if (bpcur > l) {
				loglikemultipoint += log10l(0.01L/(bpmax-l));
			}
			else {
				loglikemultipoint += log10l(0.99L/(l+1));
			}
			// autres cas bpcur et bpprop du meme cote de l donc a priori s'annule
		}
		else {
			loglikemultipoint += log10l(powl(Lambda,kcur-bpmin)/(dfact(kcur-bpmin)*0.5L*NOrComput->getNO(kcur)));
		}

		delete carte;
		// Compute IS weights
		ISW[pos] = (loglikemultipoint - loglike2pt[pos])*logl(10);
		Count[pos] = logl((1.0L*(*iterkey).second/(nbiter-burning)))+ISW[pos];
		KeyIndex[pos] = pos;
		K+=expl(Count[pos]);
		sprintf(bouf,"\r%f completed",(100.0L*nmaps)/freekeys);
		pout(bouf);				
		nmaps++;
	}
	sprintf(bouf,"\nDone.\n");
	pout(bouf);				

	/* sort visited tours by decreasing posterior probability */
	qsort(&KeyIndex[0],freekeys,sizeof(int),cmpKeys);


	//   for (HASHMAP::iterator iterkey = distribution.begin(); iterkey != distribution.end(); ++iterkey) {
	// #ifdef __WIN32__
	// 	unsigned int pos = ((*iterkey).first.key) - ((Keytype_ptr) &keys[0]);
	// #else
	// 	unsigned int pos = ((*iterkey).first) - ((Keytype) &keys[0]);
	// #endif
	// 	Count[pos] = (*iterkey).second;
	// 	KeyIndex[pos] = pos;
	//   }
	/* show the tour distribution */

	long long total = 0;
	// posterior probability that  markers (i,i+1) in best are linked 
	//    double **mklinks = new double * [NbMS-1];
	//    for (int i=0; i<(NbMS-1); i++) {
	//      mklinks[i] = new double[NbMS-1-i];
	//      for (int j=0; j<(NbMS-1-i); j++) {
	//        mklinks[i][j]=0;
	//      }
	//    }

	int *invbestour = new int[NbMS+1];
	invbestour[0] = -1;
	for (int j=0; j<NbMS; j++) {
		invbestour[bestour[j+1]] = j;
	}
	for (int k=0; k<freekeys; k++) {
		HASHMAP::iterator iterkey = distribution.find(&keys[KeyIndex[k]]);
		assert(iterkey != distribution.end());
		getTour(tour, n, (*iterkey).first);
		total += (*iterkey).second;

		/* insert this tour into Carthagene heap */
		int *order = new int[NbMS];
		for (int i = 0; i < NbMS; i++) {
			order[i] = MarkSelect[tour[i+1] - 1];
		}
		Carte *carte = new Carte(this, NbMS, order);
		// 	double loglikemultipoint = ComputeEM(carte);
		sprintf(bouf,"\r Printing: %6d /%d maps",k+1,freekeys);
		pout(bouf);
		double papprox= 1.0*((*iterkey).second)/(nbiter-burning);
		long double pestim=expl(Count[KeyIndex[k]])/K;
		// Print tour to file (not efficiently though)
		kcur=BreakPointsMapPointer(ArbreJeu->Id,carte);
		fprintf(relpos,"%.6Lf %.6f %.6Lf %d ",pestim,papprox,exp(ISW[KeyIndex[k]])/K,kcur);
		for (int i=0; i<NbMS; i++) {
			fprintf(relpos,"%d ",invbestour[tour[i+1]]);
		}
		fprintf(relpos,"\n");
		Heap.Insert(carte,0);
		delete carte;
	}
	sprintf(bouf,"\ntotaltime= %.2f sec.\n", cpu.Read());
	pout(bouf);

	fclose(relpos);

	// Put back the penalty coefficient
	SetBreakPointCoef(ArbreJeu->Id,coefsave);
	delete[] rh;
	delete[] bp;
	delete[] tour;
	delete[] tmptour;
	delete[] neighborhoodSum;
	delete[] neighborhoodLogLike;
	delete[] neighborhoodSum1D;
	delete[] neighborhoodLogLike1D;
	delete[] invbestour;
	//    delete[] mklinks;
	/*for (int k=0; k<freekeys; k++) {*/
	/*mpz_clear(keys[k]);*/
	/*pa_delete(keys[k]);*/
	/*}*/
	pa_multi_delete(keys, MAXNBKEYS);
}

