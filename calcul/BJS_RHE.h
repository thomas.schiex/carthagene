//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// $Id: BJS_RHE.h,v 1.5 2010-12-13 13:09:21 thomasfaraut Exp $
//
// Description : Prototype de la classe BJS_RHE.
// Divers :
//-----------------------------------------------------------------------------

#ifndef _BJS_RHE_H
#define _BJS_RHE_H

#include "BJS_RH.h"

#define EM_MAX_THETA_HR 0.9995
#define EM_MIN_THETA_HR 0.0001
#define EM_MAX_RETAIN   0.9999
#define EM_MIN_RETAIN   0.0001

#define NB_STATES 2  // nb of states of the markov chain
#define NB_ALPHA 16   // alphabet size

#define FALSE_POSITIVE_INIT 0.0001
#define FALSE_NEGATIVE_INIT 0.0001

#define UNKNOWN '-'

#define LongDouble long double

/** NOTE : Thomas F, solution de facilité je crée un nouveau jeu de données plutôt que d'hériter de BJS_RH **/

class BJS_RHD;
class BJS_RHE;

/** Classe des jeux de données hybrides irradiés avec modélisation de l'erreur */
class BJS_RHE : public BioJeuSingle {
 public:

  /** Constructeur. */
  BJS_RHE();

  /** Constructeur.
      @param cartag accés à l'ensemble des informations du système.
      @param id le numéro du jeu.
      @param cross type de jeu de donnée.
      @param nm nombre de marqueurs.
      @param taille de l'échantillon (taille du panel)
      @param bitjeu champ de bit du jeu de données.
      @param indmarq le vecteur d'indice de marqueur
      @param echantil la matrice des informations
    */
  BJS_RHE(CartaGenePtr cartag,
	 int id,
	 charPtr nomjeu,
	 CrossType cross,
	 int nm,
	 int taille,
	 int bitjeu,
	 int *indmarq,
	 Obs **echantil);

  /** Destructeur. */
  ~BJS_RHE();

  /** Constructeur par recopie */
  BJS_RHE(const BJS_RHE& b);
  /** Constructeur par recopie (variante transtypante) */
  BJS_RHE(const BJS_RH& b);
  /** Constructeur par recopie (variante transtypante) */
  BJS_RHE(const BJS_RHD& b);

  /** NOTE : Thomas F, solution de facilité je crée un nouveau jeu de données mais je duplique les méthodes déja écrites*/

  /** Compatibilité entre deux echantillons d'un marqueur
      @param numarq1 le numero du marqueur 1
      @param numarq2 le numero du marqueur 2
  */
  int Compatible(int numarq1, int numarq2) const;

  /** Fusion de deux marqueurs pour la creation d'un haplotype. Le
      marqueur 1 est utilise pour stocker le resultat.
      @param numarq1 marqueur 1
      @param numarq2 marqueur 2
      @return nothing.
  */
  void Merge(int marq1,int marq2) const;

  /** Calcul du maximum de vraisemblance par EM. (WARNING surcharge de la méthode de Biojeu pour le développement)
       @param data la carte
       @param epsilon le seuil de convergence
       @return le maximum de vraissemblance
  */
  // double ComputeEM(Carte *data);

  /** Etape d'Expectation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le loglike
  */

  double ComputeExpected(const Carte *data, double *expected);

  /** Imputation of the corrected genotypes
   *  based on the error estimation
   *  @param filename : the name of the file where the corrected genotypes will be exported
   *  @param ConversionCutoff : a lower cutoff value for the posterior probability of a genotype above which they will be recalled as unknown
   *  @param CorrectionCutoff : an upper cutoff value for the posterior probability of a genotype above which they will be corrected (opposite genotype)
      @return none
  */
  int Imputation(const Carte* map, double ConversionCutoff, double CorrectionCutoff, double UnknownCorrectionCutoff);
	
  inline double FalsePositiveRate( void ) const
	{	
		return pi;	
	}
  inline double FalseNegativeRate( void ) const
	{	
		return nu;	
	}
	
 private:
  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  void Prepare2pt(int m1, int m2,  int *ss) const;

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const;

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  double LogLike2pt(double *par, int *ss) const;

  /** passage � priv� d'une virtuelle h�rit�e.
  */
  double ComputeOneTwoPoints(int m1,
			     int m2,
			     double epsilon,
			     double *fr) const;

  /** passage � priv� d'une virtuelle h�rit�e.
  */
  double LogInd(int m1,
		int m2,
		int *nbdata) const { return 0.0;};

  /** passage � priv� d'une virtuelle h�rit�e.
  */
  double EspRec(int m1,
		int m2,
		double theta,
		double *loglike) const { return 0.0;};

  /** nettoyage des structures pour la programmation dynamique de EM.
      @param data la carte
    */
  void NetEM(const Carte *data);

  /** Calcul de la distance
      @param le theta.
      @return la distance
  */
  inline double Distance(double theta) const
    {
      return (Theta2Ray(theta));
    };
  /** Calcul de la distance
      @param l'unit�
      @param le theta.
      @return la distance
  */
  inline double Distance(char unit[2], double theta) const
    {
      return (Theta2Ray(theta));
    };
	
		
	/**
	 *  Retention fraction dependent of the clone, each clone can have a different retention fraction 
	 */	
	double* Ret;
	
	
  /** HMM implementation of radiation hybrids
   *  We follow Rabiner notation in "A tutorial on Hidden Markov Models and selected applications in speech recognition" :
   *  alpha_t(i), beta_t(i), gamma_t(i), ksi_t(i,j)
   *  We use next the Lange implementation of the EM algorithm (namely partial derivatives, hence Deltatheta, DeltaTrans, ....)
   *  from Mathematical and Statistical Methods for Genetic Analysis (Chapter 11)
   */

  /** Data structure for the HMM
   * The states are defined by the possible genotypes : 0 or 1
   * The emitted symbols are the observed phenotypes : 0 or 1 (might also be 2 for unknown, not implemented yet)
  */

  /**
   * Transition probabilities : Trans[k][i][j] := Prob_k(s_j|s_i)
   */
  LongDouble *** Trans;

  /**
   * Partial derivatives of transition probabilities : DeltaThetaTrans[k][i][j] := delta (Prob_k(s_j|s_i)) / delta theta_k
   */
  LongDouble *** DeltaThetaTrans;

  /**
   * Partial derivatives of transition probabilities : DeltaTTrans[k][i][j] := delta (Prob_k(s_j|s_i)) / delta r
   */
  LongDouble *** DeltaRTrans;

  /**
   * Vector for storing deltaP/P, the partial derivative of the observed loglikelihood
   */

  LongDouble *ScoreVector;

  /**
   * Initial probability distribution of states P_0[S_i] := b_s
   */
  LongDouble P_0[NB_STATES];

  /**
   * Partial derivative of P_0 with respect to the retention fraction r
   */
  LongDouble DeltaRP_0[NB_STATES];


  /**
   * Emission probabilities : PHI[s_i][o] := P( X = o | S = s_j)
   */
  LongDouble PHI[NB_STATES][NB_ALPHA];

  /**
    * Derivative of the emission probabilities
    */
   LongDouble DeltaPiPHI[NB_STATES][NB_ALPHA];
   LongDouble DeltaNuPHI[NB_STATES][NB_ALPHA];

  /**
   * Error rates :
   *  pi : false positive rate
   *  nu : false negative rate
   */
   double pi;
   double nu;

  /**
   * Structures for the Forward and Backward algorithms :
   * Alpha[k][i]  := alpha_k(s_i)
   * Beta[k][i]   := beta_k(s_i)
   * Ksi[k][i][j] := ksi_k( s_i, s_j)
   * Gamma[k][i]  := gamma_k(s_i)
   */

  LongDouble **  Alpha;
  LongDouble **  Beta;
  LongDouble *** Ksi;
  LongDouble **  Gamma;

  //double ErrorEstimation;

  double FalsePositiveEstimate;
  double FalseNegativeEstimate;

  double *** PostProb;

  double LogLikeliHood;

  /**
   * Structure allocation for the EM algorithm
   * @param data the map
   */
  void PreparEM(const Carte *data);

  /**
   * Initialization of the transition probabilities according to the current parameters given by map
   * @param data the map
   */
  void InitTransProb(const Carte *map);
	
  /**
   * Initialization of the transition probabilities according to the current parameters given by map
   * and the clone specific retention fraction
   * @param data the map
   */
  void InitTransProbClone(const Carte *map);
  
    /**
   * Initialization of the transition probabilities according to the current parameters given by map
   * and the clone specific retention fraction
   * @param data the map
   */
  void TransProbCloneSpec(int Id,const Carte *map);

  /**
   * The Forward algorithm of HMM
   * @param Ind
   * @param data the map
   */
  double Forward(int Ind, const Carte *map, LongDouble likelihood);

  /**
   * The Backward algorithm of HMM
   * @param Ind
   * @param data the map
   */
  LongDouble Backward(int Ind, const Carte *map);

  /** Affichage simple d'une carte
        @param data la carte
    */
  void PrintMap(Carte *data) const;

  /** Affichage d�taill� d'une carte
      @param data la carte
      @param envers � l'envers ou � l'endroit
      @param data la carte de r�f�rence
  */
  void PrintDMap(Carte *data, int envers, Carte *dataref);

};

#endif /* _BJS_RHD_H */
