//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// $Id: BioJeu.h,v 1.37.2.4 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Prototype de la classe de base BioJeu.
// Divers :
//-----------------------------------------------------------------------------

#ifndef _BIOJEU_H
#define _BIOJEU_H

#include "System.h"
#include "CGtypes.h"
#include "Genetic.h"
#include "Carte.h"

#include <stdio.h>

#include "ladj.h"

/** Esperance du nombre de cassures (extremites gauches) */
#define EBreak expected[NbM - 1]

/** Esperance du nombre d'extremites gauches retenues */
#define ERetained expected[NbM]

const double EM_MAX_RETAIN  =  0.9999;
const double  EM_MIN_RETAIN   = 0.0001;

class CartaGene;

typedef CartaGene *CartaGenePtr;

const double Close2Zero=1e-10;

// Forward declaration to prevent dependency hell
namespace TwoPoint {
	class Matrices;
}

struct twopoint_traits;

/** Classe m�re des jeux de donn�es. (Tas de Bio Jeu tu sais!) */
class BioJeu {

 public:
  /** classe pour generer une exception de type calcul non implemente pour ce jeu */
  class NotImplemented { };

  /** Les seuils de convergence - variables de classe */
  static double Epsilon1;
  static double Epsilon2;

  /** le num�ro du jeu */
  int Id;

  /** Type de population. */
  CrossType Cross;

  // DL
  /** Type de population avant coerce() */
  CrossType OrigCross;

  /** Champ de bit. */
  int BitJeu;

  /** Nombre de marqueurs. */
  int NbMarqueur;

  /** Nombre d'individus. */
  int TailleEchant;

  /** Nombre de M�ioses. */
  int NbMeiose;

  /** Compteur du nombre d'appels aux fonctions EM.
      Il est li� au remplissage d'un Tas.
    */
  unsigned int NbEMCall;
  unsigned int NbEMHit;
  unsigned int NbEMUnconv;

  /**
   */
  double Em_Max_Theta;
  double Em_Min_Theta;
  /** Constructeur. */
  BioJeu();

  /** Constructeur.
      @param cartag acc�s � l'ensemble des informations du syst�me.
      @param id le num�ro du jeu.
      @param cross type de jeu de donn�e.
      @param nm nombre de marqueurs.
      @param bitjeu champ de bit du jeu de donn�es.
  */
  BioJeu(CartaGenePtr cartag,
	 int id,
	 CrossType cross,
	 int nm,
	 int bitjeu);

  /** Destructeur. */
  virtual ~BioJeu();

  /** Constructeur par recopie */
  BioJeu(const BioJeu& b);

  // DL
  /** Recreate data type string to save data */
  virtual const char* GetDataType() const;

  // DL
  /** Output dataset to a character stream */
  virtual void DumpTo(FILE* outFile) const throw(NotImplemented);

  // LD
  /** Pour la classe BJS_OR */
  virtual int BreakPoints(BioJeu *jeu);

  // LD
  /** For the BJS_OR class */
  virtual int BreakPointsMap(Carte *map);

  // TF
  /** For the BJS_RHE class */
  virtual int Imputation(double ConversionCutoff, double CorrectionCutoff, double UnknownCorrectionCutoff, char *filename);

  // LD
  /** Retourne le marqueur d'indice i
      @param index
      @return marqueur
  */
  int GetMarq(int i);

  // LD
  /** Retourne l'indice dans IndMarq du marqueur i
      @param num�ro du marqueur
      @�eturn indice du marqueur
  */
  int GetMarqPos(int marq);

  /** Retourne un champ des jeux fusionn�s sur l'ordre(!=CON).
      @return un champ de bits
  */
  virtual int GetBJMO() const = 0;


  /** Calcul du log sous l'hypoth�se d'ind�pendance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param nbdata le nombre de donn�es informatives(o)
      @return le log
  */
  virtual double LogInd(int m1,
			int m2,
			int *nbdata) const = 0;

  /** Calcul l'esp�rance du nombre de recombinants, (log �volutif).
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param theta
      @param loglike(o)
      @return l'esp�rance
  */
  virtual double EspRec(int m1,
			int m2,
			double theta,
			double *loglike) const = 0;


  /** Test de compatibilite de deux marqueurs pour la creation d'un haplotype
      @param numarq1 marqueur 1
      @param numarq2 marqueur 2
      @return crit�re v�rifi� ou pas.
  */
  virtual int Compatible(int marq1,int marq2) const = 0;

   /** Fusion de deux marqueurs pour la creation d'un haplotype. Le
      marqueur 1 est utilise pour stocker le resultat.
      @param numarq1 marqueur 1
      @param numarq2 marqueur 2
      @return nothing.
   */
  virtual void Merge(int marq1,int marq2) const = 0;

  /** Affichage de l'�chantillon pour un marqueur
      @param numarq le num�ro du marqueur
  */
  virtual void DumpEchMarq(int numarq) const = 0;

  /** Affichage de l'�chantillon complet. */
  virtual void DumpEch(void) const = 0;

  /** Affichage de la matrice des LOD scores 2pt. */
  virtual void DumpTwoPointsLOD() const = 0;

  /** Affichage de la matrice des FR 2pt. */
  virtual void DumpTwoPointsFR() const = 0;

  /** Acc�s � une valeur LOD score 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return le lod
  */
  virtual double GetTwoPointsLOD(int m1, int m2) const = 0;

  /** Acc�s � la distance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return la distance
  */
  virtual double GetTwoPointsDH(int m1, int m2) const = 0;

  /** Acc�s � la fraction de recombinaison 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return la fraction de recombinaison
  */
  virtual double GetTwoPointsFR(int m1, int m2) const = 0;

  /** Calcul analytique de la contribultion elementaire au log de la vraisemblance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return elt_loglike2pt
  */
  virtual void InitContribLogLike2pt() {};
  virtual double ContribLogLike2pt(int m1) {return 0.;};
  virtual double ContribLogLike2pt(int m1, int m2) {
    print_out("Error: TSP reduction not implemented for this data type.\n");
    throw NotImplemented();
    return 0.;
  };
  virtual double NormContribLogLike2pt(int m1) {return ContribLogLike2pt(m1);};
  virtual double NormContribLogLike2pt(int m1, int m2) {return ContribLogLike2pt(m1,m2);};
  virtual double ContribOCB(int m1, int m2) {
    print_out("Error: OCB criterion not implemented for this data type.\n");
    throw NotImplemented();
    return 0;
  };
  virtual double NormContribOCB(int m1, int m2) {
    print_out("Error: normalized OCB criterion not implemented for this data type.\n");
    throw NotImplemented();
    return 0;
  };

  /** Calcul du maximum de vraisemblance par EM.
      @param data la carte
      @param epsilon le seuil de convergence
      @return le maximum de vraissemblance
  */
  virtual double ComputeEM(Carte *data);

  /** Calcul de la vraisemblance maximum par EM.
      Une interface plus sophistiqu�e du calcul de la vraisemblance par EM:
      On converge jusqu'� epsilon1. Si la vraisemblance est inf�rieure �
      threshold on laisse tomber, sinon on converge � epsilon2.
      @param data la carte
      @param threshold niveau a partir duquel on convergera a epsilon2
      @param epsilon1 le premier seuil de convergence
      @param epsilon2 le second seuil de convergence
      @return le maximum de vraissemblance
  */
  virtual double ComputeEMS (Carte *data,
			     double threshold);

  /** Pr�paration des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  virtual void PreparEM(const Carte *data) = 0;

  /** nettoyage des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  virtual void NetEM(const Carte *data) = 0;

  /** Etape d'Expectation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le loglike
  */
  virtual double ComputeExpected(const Carte *data, double *expected) = 0;

  /** Etape de Maximisation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le fait qu'une limite de domaine d'optimisation a ete atteinte
  */
  virtual int Maximize(Carte *data, double *expected) const;

  /** Calcul, affichage et stockage des groupes.
      On �tablit un graphe dont les noeuds sont les marqueurs. Il existe une arr�te entre deux marqueurs, si le critere de lod > seuil et de distance < seuil est v�rifi�. Puis on extrait les composantes connexes qui sont �quivalentes aux groupes.
      @param disthres le seuil de distance 2pt.
      @param lodthres le seuil de LOD 2pt.
      @return le nombre de groupes.
  */
  virtual int Groupe(double disthres, double lodthres) const;

  /** Affichage simple d'une carte
      @param data la carte
  */
  virtual void PrintMap(Carte *data) const;


  /** Matrice 2p des distances
      @param unit l'unit� de distance:
      'k' = kosambi ou centiray,
      'h' = haldane ou centiray.
  */
  virtual void PrintTwoPointsDist(const char unit[2]) const;

  /** retourne une liste contenant une carte
      @param unit l'unit� de distance:
      'k' = kosambi ou centiray,
      'h' = haldane ou centiray.
      @param data la carte
      @return une liste
  */
  virtual char *** GetMap(const char unit[2], Carte *data) const;

  /** Affichage d�taill� d'une carte
      @param data la carte
      @param envers � l'envers ou � l'endroit
      @param data la carte de r�f�rence
  */
  virtual void PrintDMap(Carte *data, int envers, Carte *dataref);

  /** Test l'existance d'un couple
      @param m1 marqueur 1
      @param m2 marqueur 2
      @return 0 ou 1
  */
  virtual int Couplex(int m1, int m2) const;

  /** Mise � 0
    */
  virtual void ResetNbEMCall() {NbEMCall = NbEMHit = NbEMUnconv = 0;};

  /** Affichage
    */
  virtual void PrintEMCall() {
    if (NbEMCall) {
      print_out("           Set %2d : %d (%d,%d)\n", Id, NbEMCall,NbEMHit,NbEMUnconv);
    }
  };

  /** Test de l'existance d'un RH dans l'arborence
      @return 0 ou 1
  */
  virtual int HasRH() const = 0;

  /** Test if a RISib ou RISelf dataset exist in the tree.
      It returns either the CrossType or Unk(used as logical False)
  */
  virtual CrossType HasRI() const = 0;

  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  virtual void Prepare2pt(int m1, int m2,  int *ss) const = 0;

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  virtual void Estimate2pt(double *par, int *ss) const =0;

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  virtual double LogLike2pt(double *par, int *ss) const =0;

  // DL
  /** Crée une sélection de tous les marqueurs du BioJeu dans l'ordre du BioJeu */
  int* GetFullMarkSel() const;

  /*int* _imrk() const { return IndMarq; }*/
  virtual void _updateIndMarq(int max);

  // DL rendu GroupTest public
  /** Crit�re de linkage entre 2 marqueurs.
      @param disthres PAS UTILISE.
      @param lodthres le seuil de LOD 2pt.
      @param k le num�ro du premier marqueur
      @param l le num�ro du second marqueur
      @return crit�re v�rifi� ou pas.
  */
  inline virtual int GroupTest(double disthres, double lodthres, int k, int l) const
    {
      return ( GetTwoPointsDH(k, l) <= disthres &&
	       GetTwoPointsLOD(k, l) >= lodthres);
    };

  unsigned int computation_count() const;
 protected:
  /** Acc�s � l'ensemble des informations du syst�me. */
  CartaGene *Cartage;

  /** indice local de chaque marqueur. */
  int *IndMarq_cg2bj;
  int *IndMarq_bj2cg;

  /** taille d'indmarq à un moment donné (pour _updateIndMarq) */
  int _indmarq_sz;

  /** Taux de retention general (RH Only) */
  double OverallR;

  // DL : supprime les double** et on remplace tout ça par TwoPoint::Matrices
  TwoPoint::Matrices* twopoint;

  /** Fraction de recombinaison 2pt. */
  //doublePtr *TwoPointsFR;

  /** Distance selon Haldane/Ray 2pt. */
  //doublePtr *TwoPointsDH;

  /** LOD score 2pt. */
  //doublePtr *TwoPointsLOD;

  /** Calcul du LOD score et de fraction de recombinants 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param epsilon le seuil de convergence
      @param fr la fraction de recombinants(i,o)
      @return le LOD
  */
  virtual double ComputeOneTwoPoints(int m1,
				     int m2,
				     double epsilon,
				     double *fr) const = 0;

  /** Calcul des valeurs de points et remplissage des matrices.
      En l'occurence, il s'agit uniquement des LOD 2pts des couples de marqueurs pr�sents dans les deux jeux fusionn�s.
  */
  virtual void ComputeTwoPoints(void) = 0;

  /** Parcours en profondeur(d�tection des groupes)
      @param ladj liste d'adjacence
      @param visited vecteur t�moin de visite
      @param vertex le sommet
      @param la couleur
      @param plink pointeur sur la liste des groupes
  */
  void DFSVisit(NodintPtr *ladj,
		intPtr visited,
		int vertex,
		int color,
		NodptrPtr plink) const;

  void DFSVisit2(std::vector<LAdj*>& ladj,
		intPtr visited,
		int vertex,
		int color,
		NodptrPtr plink) const;

  /** affichage du seuil de LOD
      @param disthres PAS UTILISE.
      @param lodthres le seuil de LOD 2pt.
  */
  virtual void GroupMess(double disthres, double lodthres) const
    {
      print_out("\nLinkage Groups :\n");
      print_out("---------------:\n");
      print_out("LOD threshold=%.2f\nDistance threshold=%.2f:\n", lodthres, 100.0*disthres);
    };

  /** Calcul de la distance
      @param le theta.
      @return la distance
  */
  inline virtual double Distance(double theta) const
    {
      return (Haldane(theta));
    };

  /** Calcul de la distance
      @param l'unit�
      @param le theta.
      @return la distance
  */
  inline virtual double Distance(const char unit[2], double theta) const
    {
      switch(unit[0])
	{
	case 'h': return (Haldane(theta));
	case 'k': return (Kosambi(theta));
	}
      return (Haldane(theta));
    };

  private:
	friend class DataSet;
	friend struct twopoint_traits;

};

typedef BioJeu *BioJeuPtr;

#endif /* _BIOJEU_H */
