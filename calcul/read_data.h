#ifndef _CARTAGENE_READ_DATA_H
#define _CARTAGENE_READ_DATA_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <map>
#include <cstring>
#include <algorithm>


#include "CGtypes.h"
/*#include "Constraints.h"*/


namespace read_data {


    typedef std::pair<std::string, CrossType> follow;
    typedef std::multimap<std::string, follow> follow_set_t;
    typedef std::pair<follow_set_t::iterator, follow_set_t::iterator> follow_set_range_t;
    typedef std::pair<CrossType, std::string> cross_metadata_t;




    typedef std::vector<char> MarkerRow;
    typedef std::vector<MarkerRow> MarkerData;
    typedef std::vector<std::string> MarkerNames;

    struct raw_data {
        /* for all */
        CrossType Cross;
        /* for bs */
        std::string design_string;
        /* for all but constraints and order */
        MarkerData marker_data;
        MarkerNames marker_names;
        int nm, ni;
        /* for constraints */
        /*Constraint* constraints;*/
        typedef struct { std::string m1, m2, m3; double penalty; int lineno; } symbolic_constraint;
        typedef std::vector<symbolic_constraint> symbolic_constraints_t;
        symbolic_constraints_t constraints;
        /* for order */
        int* chrom;
        int* chromex;
        int* position;
        double coeff;
        /* for all but constraints and order */
        char tr[256];

        raw_data(CrossType c, std::string& ds)
            : Cross(c),
            design_string(ds),
            marker_data(),
            marker_names(),
            nm(0), ni(0),
            constraints(NULL),
            chrom(NULL),
            chromex(NULL),
            position(NULL),
            coeff(0)
        {
            memset(tr, 0, 256);
            tr[' '] = ' ';
            tr['-'] = '-';
            if(Cross != IC && Cross != BS) { // CN added the BS condition, 12.29.05
                tr['A'] = '0';
                tr['B'] = 'A';
                tr['H'] = 'A';
            } else {
                tr['A'] = 'A';
                tr['B'] = 'B';
                tr['C'] = 'C';
                tr['D'] = 'D';
                tr['H'] = 'H';
                tr['0'] = '0'; // 0000
                tr['1'] = 'A'; // 0001
                tr['2'] = '2'; // 0010
                tr['3'] = '3'; // 0011
                tr['4'] = '4'; // 0100
                tr['5'] = '5'; // 0101
                tr['6'] = 'H'; // 0110
                tr['7'] = 'D'; // 0111
                tr['8'] = 'B'; // 1000
                tr['9'] = '9'; // 1001
                tr['a'] = 'a'; // 1010
                tr['b'] = 'b'; // 1011
                tr['c'] = 'c'; // 1100
                tr['d'] = 'd'; // 1101
                tr['e'] = 'C'; // 1110
                tr['f'] = '-'; // 1111
            }
        }
    };

    raw_data read_file(const char* filename);
}

#endif
