//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJM_GE.h,v 1.11 2006-12-06 19:22:33 tschiex Exp $
//
// Description : Prototype de la classe BJM_GE.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJM_GE_H
#define _BJM_GE_H

#include "BioJeuMerged.h"


/** Classe de la fusion g�n�tique. */  
class BJM_GE : public BioJeuMerged {
 public:

  /** Constructeur. */
  BJM_GE();
  
  /** Constructeur
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param id le num�ro du jeu.
      @param cross type de jeu de donn�e(Mge).
      @param nm nombre de marqueurs.
      @param bitjeu champ de bit du jeu de donn�es.
      @param gauche jeu de donn�es.
      @param droite jeu de donn�es. 
  */
  BJM_GE(CartaGenePtr cartag,
	 int id,
	 CrossType cross,
	 int nm, 
	 int bitjeu,
	 BioJeu *gauche, 
	 BioJeu *droite);

  /** Destructeur. */
  virtual ~BJM_GE();

  /** Retourne un champ des jeux fusionn�s sur l'ordre(!=CON).
      @return un champ de bits
  */
  inline int GetBJMO() const
    {
      return BitJeu;
    };

  /** Acc�s � la distance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return la distance de Haldane
    */
  double GetTwoPointsDH(int m1, int m2) const;

  /** Acc�s � la fraction de recombinaison 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return la fraction de recombinaison 2pt
    */
  double GetTwoPointsFR(int m1, int m2) const;

  /** Etape d'Expectation de EM.
      @param data la carte
      @param expected le vecteur d'Expectation(i,o)
      @return la valeur de loglike
  */
  double ComputeExpected(const Carte *data, double *expected);

 private:

  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  void Prepare2pt(int m1, int m2,  int *ss) const;

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const;

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  double LogLike2pt(double *par, int *ss) const;

  /** Calcul du LOD score et de fraction de recombinants 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param epsilon le seuil de convergence
      @param fr la fraction de recombinants(i,o)
      @return le LOD
  */
  double ComputeOneTwoPoints(int m1, 
			     int m2, 
			     double epsilon,
			     double *fr) const;

  /** Calcul des valeurs de points et remplissage des matrices.
      il s'agit des valeurs LOD, distance et fraction de recombinaison 2pts des couples de marqueurs pr�sents dans les deux jeux fusionn�s.
  */
  void ComputeTwoPoints(void);

  /** Pr�paration des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  void PreparEM(const Carte *data);
  
  /** nettoyage des structures pour la programmation dynamique de EM.
      @param data la carte
    */
  void NetEM(const Carte *data);
  
};

#endif /* _BJM_GE_H */
