//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: NOrComp.cc,v 1.2 2005-01-26 16:55:43 bouchez Exp $
//
// Description :  NorComp class
// Divers :  Computation of the number of orders at a given breakpoint distance
//-----------------------------------------------------------------------------



#include "NOrComp.h"

NOrComp::NOrComp(int n, int k)
{

  locdim = n;
  bpdim = k;
  
  int i,j,l;
  
  Z = new double**[7];
  for (i = 0; i < 7; i++) {
    Z[i] = new double*[n+1];
    for (j = 0; j < n+1; j++) {
      Z[i][j] = new double[k+1];
      //  -1 = unaffected
      for (l = 0; l < k+1; l++)
	Z[i][j][l] = -1;
    }
  }
  
  O  = Z[0];
  Ob = Z[1];
  Oc = Z[2];
  Ib = Z[3];
  Ic = Z[4];
  Sb = Z[5];
  Sc = Z[6];
  
}

NOrComp::~NOrComp()
{  
  int i,j;
  
  for (i = 0; i < 7; i++) {
    for (j = 0; j < locdim+1; j++)
      delete [] Z[i][j];
    delete [] Z[i];
      }
  delete [] Z;
}
double NOrComp::getNO(int n, int k)
{
  return(f_O(n,k));
}

void NOrComp::test(int n, int k)
{
  int i,j;
  
  for (i = 1; i <= n; i++)
    for (j = 0; j <= k; j++)
      printf("%d %d %g\n", i , j , f_O(i,j));
}
