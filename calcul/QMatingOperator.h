//-----------------------------------------------------------------------------
//                            CarthaGene
//
// James C. Nelson
// Dept. of Plant Pathology, Kansas State University, Manhattan, KS, USA
//
// Copyright 2006 James C. Nelson
// This file is distributed under the terms of the Q Public License version 1.0.
// It may be freely included with CarthaGene.
//
// $Id: QMatingOperator.h,v 1.0 2006/02/06
//
//-----------------------------------------------------------------------------
#ifndef __QMatingOperator__
#define __QMatingOperator__

// Class to encapsulate the updating of genotype and crossover probabilities with
// change of generation.
#include "QPolynomialMatrix.h"

class QMatingOperator
{
	protected:
		char mOpSymbol;	 // 'b', 's', 'd', 'i' for example
		QPolynomialMatrix *mOpM;	// used to premultiply genotype or genotype x xover pmf matrices
		
		int **mXoverMaskM;	// used as a filter during multiplication of Xover pmf by mOpM
		int mNumMeioses; // at each generation
		int mRows;	// of op matrix and mask array

	public:
	
		QMatingOperator();

		~QMatingOperator();

		QMatingOperator(char ch);

		QPolynomialMatrix *UpdateAllProbs(double *singleLocusGenoProbs, QPolynomial *RIM_factorP,
						QPolynomialMatrix *genoProbsV, QPolynomialMatrix *xProbsM, QPolynomialMatrix *newXProbsM);
	private:
		void UpdateSingleLocusProbs(double *probs);
		void UpdateRIMs(QPolynomial *curRIM_P, QPolynomialMatrix *genoProbsV);
		void UpdateIntercrossGxC_pmf(QPolynomialMatrix *xoverProbsM,
																			QPolynomialMatrix *newXoverProbsM);
		void UpdateBSD_GxC_pmf(QPolynomialMatrix *xoverProbsM, QPolynomialMatrix *newXoverProbsM);
		void UpdateGxC_pmf(QPolynomialMatrix *xoverProbsM, QPolynomialMatrix *newXoverProbsM);
		void UnwindPunnett(QPolynomialMatrix *gameteCrossPM, QPolynomialMatrix *newXoverProbsM, int destCol);
		QPolynomialMatrix *CombineGametes(QPolynomialMatrix *gameteProbsV);
		QPolynomialMatrix *UpdateTwoLocusProbs(QPolynomialMatrix *genoProbsV);
};

	enum {aa,	Aa,	AA, a_, A_};
	enum {paa, pAa,	paA, pAA};	// phased genotypes
	// indices into our 10-genotype array.
	enum {paabb, paaBb, paaBB, pAabb, pAaBb, paABb, pAaBB, pAAbb, pAABb, pAABB};

// symbols used in the mating string in the data file header; they direct the mating operations.
	const char
		cBackcrossSymbol = 'b',
		cSelfingSymbol = 's',
		cDoubledHaploidSymbol = 'd',
		cIntercrossingSymbol = 'i';
	// If changing or adding to the above symbols, also change sMatingSymbols and kNumBasicMatingOps below.
	const int kNumBasicMatingOps = 4;	// can get rid of this later; for now, change it if a symbol is added
	const char sMatingSymbols[kNumBasicMatingOps] =
		{cBackcrossSymbol, cSelfingSymbol, cDoubledHaploidSymbol, cIntercrossingSymbol};
		
	const int kSingleColumn = 1;	// defined for easy reading of code
	const int kSelfingDegree = 2;	// i.e. highest term in selfing operator is quadratic
	const int kSingleMeiosisDegree = 1;
	const int kZeroDegree = 0;
	const int NUM_DIPLOID_GENOTYPES = 3;
	const int NUM_PHASED_DIPLOID_GENOTYPES = 4;
	const int NUM_DIPLOID_GENOTYPE_COMBOS = 10;

/*
	The genotype labeling in the following operators with dimension 10 is given above by the enum
		enum {paabb, paaBb, paaBB, pAabb, pAaBb, paABb, pAaBB, pAAbb, pAABb, pAABB};
	Where dimension is 4 as for the gamete operators, the labeling is given above by the enum
		enum {paa, pAa,	paA, pAA};	// phased genotypes
		
	Each gives the coefficients of polynomials in theta (with the zero coefficient to the left) for transition
	probabilities from parent to child genotype. Parent genotypes are columns, child genotypes rows, so the entries
	are conditional probabilities that sum to 1 across rows within each column. These operators will be used to
	premultiply genotype probability column vectors.
*/
	const double BackcrossingOperatorM[NUM_DIPLOID_GENOTYPE_COMBOS][NUM_DIPLOID_GENOTYPE_COMBOS][kSingleMeiosisDegree + 1] =
	{
	{{1.0, 0.0},	{0.5, 0.0},	{0.0, 0.0},	{0.5, 0.0},	{0.5, -0.5},	{0.0, 0.5},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.5, 0.0},	{1.0, 0.0},	{0.0, 0.0},	{0.0, 0.5},	{0.5, -0.5},	{0.5, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.5, 0.0},	{0.0, 0.5},	{0.5, -0.5},	{0.0, 0.0},	{1.0, 0.0},	{0.5, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.5, -0.5}, {0.0, 0.5},	{0.5, 0.0},	{0.0, 0.0},	{0.5, 0.0},	{1.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}}
	};

	const double SelfingOperatorM[NUM_DIPLOID_GENOTYPE_COMBOS][NUM_DIPLOID_GENOTYPE_COMBOS][kSelfingDegree + 1] =
	{
	{{1.0, 0.0, 0.0},	{0.25, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.25, 0.0, 0.0},	{0.25, -0.5, 0.25},	{0.0, 0.0, 0.25},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.5, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.5, -0.5},	{0.0, 0.5, -0.5},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.25, 0.0, 0.0},	{1.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.25},	{0.25, -0.5, 0.25},	{0.25, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.5, 0.0, 0.0},	{0.0, 0.5, -0.5},	{0.0, 0.5, -0.5},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.5, -1.0, 0.5},	{0.0, 0.0, 0.5},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.5},	{0.5, -1.0, 0.5},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.5, -0.5},	{0.0, 0.5, -0.5},	{0.5, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.25, 0.0, 0.0},	{0.0, 0.0, 0.25},	{0.25, -0.5, 0.25},	{0.0, 0.0, 0.0},	{1.0, 0.0, 0.0},	{0.25, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.5, -0.5},	{0.0, 0.5, -0.5},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.5, 0.0, 0.0},	{0.0, 0.0, 0.0}},
	{{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.25, -0.5, 0.25},	{0.0, 0.0, 0.25},	{0.25, 0.0, 0.0},	{0.0, 0.0, 0.0},	{0.25, 0.0, 0.0},	{1.0, 0.0, 0.0}}
	};

	const double HaploidDoublingOperatorM[NUM_DIPLOID_GENOTYPE_COMBOS][NUM_DIPLOID_GENOTYPE_COMBOS][kSingleMeiosisDegree + 1] =
	{
		{{1.0, 0.0},	{0.5, 0.0},	{0.0, 0.0},	{0.5, 0.0},	{0.5, -0.5}, {0.0, 0.5},{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.5, 0.0},	{1.0, 0.0},	{0.0, 0.0},	{0.0, 0.5}, {0.5, -0.5},{0.5, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.5, 0.0},	{0.0, 0.5}, {0.5, -0.5},{0.0, 0.0},	{1.0, 0.0},	{0.5, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0}},
		{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.5, -0.5}, {0.0, 0.5},{0.5, 0.0},	{0.0, 0.0},	{0.5, 0.0},	{1.0, 0.0}}
		};
	
	// The rows of this arrays represent the four types of gametes that may arise from a two-locus genotype.
	const double IntercrossingOperatorM[NUM_PHASED_DIPLOID_GENOTYPES][NUM_DIPLOID_GENOTYPE_COMBOS][kSingleMeiosisDegree + 1] =
	{
	{{1.0, 0.0}, {0.5, 0.0}, {0.0, 0.0}, {0.5, 0.0}, {0.5, -0.5}, {0.0, 0.5}, {0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}},
	{{0.0, 0.0},	{0.5, 0.0},	{1.0, 0.0},	{0.0, 0.0},	{0.0, 0.5}, {0.5, -0.5}, {0.5, 0.0}, {0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.5, 0.0},	{0.0, 0.5}, {0.5, -0.5},	{0.0, 0.0},	{1.0, 0.0},	{0.5, 0.0},	{0.0, 0.0}},
	{{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.0, 0.0},	{0.5, -0.5},	{0.0,	0.5},	{0.5, 0.0}, {0.0, 0.0},	{0.5, 0.0}, {1.0, 0.0}}
	};

	// The following arrays give the numbers of crossovers corresponding to each
	// column-to-row genotype transition. They are used as masks in the matrix multiplications
	// used to construct the joint pmf of genotype and crossover number.
	const int BCXoverM[NUM_DIPLOID_GENOTYPE_COMBOS][NUM_DIPLOID_GENOTYPE_COMBOS] =
	{
	{0,	0,	0,	0,	0,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0}};

	const int SelfXoverM[NUM_DIPLOID_GENOTYPE_COMBOS][NUM_DIPLOID_GENOTYPE_COMBOS] =
	{
	{0,	0,	0,	0,	0,	2,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	2,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	2,	0,	0,	0,	0},
	{0,	0,	0,	0,	2,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	2,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	2,	0,	0,	0,	0}};

	const int DHXoverM[NUM_DIPLOID_GENOTYPE_COMBOS][NUM_DIPLOID_GENOTYPE_COMBOS] =
	{
	{0,	0,	0,	0,	0,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	1,	0,	0,	0,	0}
	};

	const int IXoverM[NUM_PHASED_DIPLOID_GENOTYPES][NUM_DIPLOID_GENOTYPE_COMBOS] =
	{
	{0,	0,	0,	0,	0,	1,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	1,	0,	0,	0,	0,	0},
	{0,	0,	0,	0,	0,	1,	0,	0,	0,	0}};

	// This array gives the destination rows in a 10 x 1 genotype probability vector in which
	// the Punnett probabilities are to be accumulated.
	const int PunnettExpanderArr[NUM_PHASED_DIPLOID_GENOTYPES][NUM_PHASED_DIPLOID_GENOTYPES] =
	{
	{0,	1,	3,	4},
	{1,	2,	5,	6},
	{3,	5,	7,	8},
	{4, 6,	8,	9}};

#endif
