//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: System.cc,v 1.22 2003-03-19 14:26:48 bouchez Exp $
//
// Description : Definition d'un chronometre.
// Divers : 
//-----------------------------------------------------------------------------


#include "System.h"
#include <stdio.h>
#ifndef __WIN32__
#  include <unistd.h>	// pour sysconf sous IRIX 5.3
#  include <time.h>	// pour CLK_TCK sous IRIX 5.3
#endif
//-----------------------------------------------------------------------------
// Que fait cette fonction ? le start du chronom�tre
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Chronometre::Init(void)
{
#if macintosh || defined __WIN32__
  time(&InitialTime);
#else
  times(&InitialTime);
#endif
}


//-----------------------------------------------------------------------------
// Que fait cette fonction ? Donne le temps �coul� depuis l'initialisation 
// de l'objet.
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : Delta de temps exprim� en secondes
//-----------------------------------------------------------------------------

double Chronometre::Read(void) const
{
#if macintosh || defined __WIN32__
  time_t ActualTime;

  time(&ActualTime);
  return difftime(ActualTime, InitialTime);
#else
  struct tms ActualTime;

  times(&ActualTime);
  return ((ActualTime.tms_utime - InitialTime.tms_utime)) / (double)sysconf(_SC_CLK_TCK);

#endif
}

//-----------------------------------------------------------------------------
// extraction du nom d'un fichier � partir d'un chemin
//-----------------------------------------------------------------------------
// Param�tres : 
// - name = path
// - 
// Valeur de retour : nom 
//-----------------------------------------------------------------------------

char *Cart_basename( const char*  name )
{
  const char  *base, *current;
  char        c;


  base    = name;
  current = name;

  c = *current;
  
  while ( c )
  {
    if ( c == '/' || c == '\\' )
      base = current + 1;

    current++;
    c = *current;
  }

  return (char*)base;
}
