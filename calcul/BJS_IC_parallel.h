//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_IC_parallel.h,v 1.1.2.1 2012-09-06 07:09:34 dleroux Exp $
//
// Description : Prototype de la classe BJS_IC_parallel.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJS_IC_PARALLEL_H
#define _BJS_IC_PARALLEL_H

#include "BioJeuSingle.h"

#include "parallel_compute_expected.h"

namespace parallel_code {

// Moved these constants from the .cc file so that they will be available
// to classes inheriting from BJS_IC_parallel, CN 1.18.06
/** liste des genotypes possibles pour chaque phenotype */

const int Possibles[16][4] = {
	{0, 0, 0, 0},  // 0 (aka 0 = 0000 = 0 )
	{0, 0, 0, 0},  // A (aka 1 = 0001 = 1 )
	{1, 0, 0, 0},  // 2 (aka 2 = 0010 = 2 )
	{0, 1, 0, 0},  // 3 (aka 3 = 0011 = 3 )
	{2, 0, 0, 0},  // 4 (aka 4 = 0100 = 4 )
	{0, 2, 0, 0},  // 5 (aka 5 = 0101 = 5 )
	{1, 2, 0, 0},  // H (aka 6 = 0110 = 6 )
	{0, 1, 2, 0},  // D (aka 7 = 0111 = 7 )
	{3, 0, 0, 0},  // B (aka 8 = 1000 = 8 )
	{0, 3, 0, 0},  // 9 (aka 9 = 1001 = 9 )
	{1, 3, 0, 0},  // a (aka a = 1010 = 10)
	{0, 1, 3, 0},  // b (aka b = 1011 = 11)
	{2, 3, 0, 0},  // c (aka c = 1100 = 12)
	{0, 2, 3, 0},  // d (aka d = 1101 = 13) 
	{1, 2, 3, 0},  // C (aka e = 1110 = 14)
	{0, 1, 2, 3}}; // - (aka f = 1111 = 15) 

/** nombre de possibilites pour chaque phenotype */
const int NPossibles[16] = {0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4};

/** Nombre de crossovers selon la paire de genotypes */
const int NCross[4][4] =
{{0, 1, 1, 2},
	{1, 0, 2 ,1},
	{1, 2, 0, 1},
	{2, 1, 1, 0}};

const int kMaxF2Crossovers = 2;// added CN 1.18.06

/** Classe des jeux de données intercross. */
class BJS_IC_parallel : public BioJeuSingle {
	public:
        /* Defs for parallel EM computation */
        struct EM_state {
            std::vector<double> SourceTo[4];
            std::vector<double> ToSink[4];
            EM_state(int nMarq) {
                for(int i=0;i<4;++i) {
                    ToSink[i].resize(nMarq);
                    ToSink[i][nMarq-1] = 0;
                    SourceTo[i].resize(nMarq);
                    SourceTo[i][0] = 0;
                }
            }
        };
        typedef parallel_compute_expected<BJS_IC_parallel, false> PCEType;

        /* FIXME: this lock should be associated with parameter 'expected' inside BioJeu::ComputEM
         *        and not with the dataset itself */
        /* FIXED: unused now. */
        //Parallel::Spin spin_lock;

		/** Constructeur. */
		BJS_IC_parallel();

		/** Constructeur.
		  @param cartag accès à l'ensemble des informations du système. 
		  @param id le numéro du jeu.
		  @param cross type de jeu de donnée.
		  @param nm nombre de marqueurs.
		  @param taille de l'échantillon
		  @param bitjeu champ de bit du jeu de données.
		  @param indmarq le vecteur d'indice de marqueur
		  @param echantil la matrice des informations
		  */
		BJS_IC_parallel(CartaGenePtr cartag,
				int id,
				charPtr nomjeu,
				CrossType cross,
				int nm, 
				int taille,
				int bitjeu,
				int *indmarq,
				Obs **echantil);

		/** Destructeur. */
		~BJS_IC_parallel();

		/** Constructeur par recopie */
		BJS_IC_parallel(const BJS_IC_parallel& b);

		/** Compatibilite entre deux echantillons d'un marqueur
		  @param numarq1 le numero du marqueur 1
		  @param numarq2 le numero du marqueur 2
		  @param numind le numero de l'individu
		  */
		int Compatible(int numarq1, int numarq2) const;


		/** Fusion de deux marqueurs pour la creation d'un haplotype. Le
		  marqueur 1 est utilise pour stocker le resultat.
		  @param numarq1 marqueur 1 
		  @param numarq2 marqueur 2 
		  @return nothing.  
		  */
		void Merge(int marq1,int marq2) const;


		/** Calcul du log sous l'hypothèse d'indépendance 2pt.
		  @param m1 le numéro du premier marqueur
		  @param m2 le numéro du second marqueur
		  @param nbdata le nombre de données informatives(o)
		  @return le log
		  */
		double LogInd(int m1, 
				int m2,
				int *nbdata) const;

		double LogInd(int m1, 
				int m2,
				int NbEdges[16][16],
				int *nbdata) const;

		/** Calcul l'espérance du nombre de recombinants, (log évolutif).
		  @param m1 le numéro du premier marqueur
		  @param m2 le numéro du second marqueur
		  @param theta
		  @param loglike(o)
		  @return l'espérance      
		  */
		double EspRec(int m1, 
						int m2, 
				double theta, 
				double *loglike) const;

		double EspRec(int NbEdges[16][16], double theta, double *loglike) const;

		/** Etape d'Expectation de EM.
		  @param data la carte
		  @param expected le vecteur d'expectation
		  @return le loglike
		  */
		double ComputeExpected(const Carte *data, double *expected);

		/** Allows inserting optimization code in ComputeExpected()--  CN 1.18.06  */
		virtual void UpdateEStepArrays(const Carte *map);

		// Moved the following three methods from private to public and made them
		// virtual, so that the subclass BJS_BS can override them (called in ComputeExpected())
		// CN 1.18.06

		/** Calcul des probabilites d''existence d''un chemin "depuis la gauche".
		  @param Ind l'individu
		  @param map la carte
		  @return le loglike
		  */
		virtual double ComputeSourceTo(std::vector<double>* SourceTo, int Ind, const Carte *map) const;

		/** Calcul des probabilités d''existence d''un chemin "depuis la droite".
		  @param Ind l'individu
		  @param map la carte
		  @return le loglike
		  */
		virtual double ComputeToSink(std::vector<double>* ToSink, int Ind, const Carte *map) const;

		/** Etape élémentaire d''Expectation de EM.
		  @param Ind l'individu
		  @param map la carte
		  @param expected vecteur d'expectation
		  */
		//virtual void ComputeOneExpected(Parallel::Spin&, std::vector<double>* SourceTo, std::vector<double>* ToSink, int Ind, const Carte* map, double *expected) const;
		virtual void ComputeOneExpected(EM_state& state, int Ind, PCEType* pce) const;

	private:

		/** Préparation des structures pour la programmation dynamique de EM.
		  @param data la carte
		  */
		void PreparEM(const Carte *data);

		/** nettoyage des structures pour la programmation dynamique de EM.
		  @param data la carte
		  */
		void NetEM(const Carte *data);

		/** Calcul stat. suffisantes pour le 2pt
		  @param m1 premier marqueur
		  @param m2 second marqueur
		  @param ss statistiques suffisantes (n[1..3])
		  */
		void Prepare2pt(int m1, int m2,  int *ss) const {
			print_out( "Bug, méthode interdite = Prepare2pt!\n");
			return;
		};

		/** Calcul des estimateurs 2pt de vrais. max
		  @param par vecteur des parametres du modele (theta,r mis a jour)
		  @param ss statistiques suffisantes (n[1..3])
		  */
		void Estimate2pt(double *par, int *ss) const  {
			print_out( "Bug, méthode interdite = Estimate2pt!\n");
			return;
		};

		/** Calcul de la vraisemblance 2pt étant donnés les paramètres du
		  modèle.
		  @param par vecteur des paramètres du modele (theta,r)
		  @param ss statistiques suffisantes (n[1..3])
		  */
		double LogLike2pt(double *par, int *ss) const  {
			print_out( "Bug, méthode interdite = LogLike2pt!\n");
			return 0.0;
		};

		/** Calcul du LOD score et de fraction de recombinants 2pt.
		  @param m1 le numéro du premier marqueur
		  @param m2 le numéro du second marqueur
		  @param epsilon le seuil de convergence
		  @param fr la fraction de recombinants(i,o)
		  @return le LOD
		  */
		double ComputeOneTwoPoints(int m1, 
				int m2, 
				double epsilon,
				double *fr) const; 

		/** tableaux des probabilités conditionnelles de passage par un sommet depuis la gauche */
        // DL : move the variable to pseudo thread-local storage
		//doublePtr SourceTo[4];

		/** tableaux des probabilités conditionnelles de passage par un sommet depuis la droite */
        // DL : move the variable to pseudo thread-local storage
		//doublePtr ToSink[4];

		doublePtr *mProbLookups;	// added CN 1.18.06 for speedup

		/** Calcul de la probabilité de passage par une arête suivant la paire de génotypes reliés
		  @param a le premier génotype
		  @param b le second génotype
		  @param p la proba de recombinaison
		  @return la probabilité
		  */
		double ProbArete(int a, int b, double p) const;

};

}

#endif /* _BJS_IC_H */
