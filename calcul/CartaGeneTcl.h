//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// $Id: CartaGene.h,v 1.65.2.4 2012-05-31 08:06:58 dleroux Exp $
//
// Description : Prototype de l''API du module.
// Divers :
//-----------------------------------------------------------------------------

#ifndef _CARTAGENE_TCL_H
#define _CARTAGENE_TCL_H

#include "../config.h"

#include "carthagene_all.h"

#ifdef ihm
#include <tcl.h>
#define fflush(stdout) do { /*std::cout << "fflush" << std::endl;*/ fflush(stdout); /*std::cout << "do Tcl events" << std::endl;*/ while (Tcl_DoOneEvent(TCL_DONT_WAIT)); /*std::cout << "done" << std::endl;*/ } while(0)
#endif

#define MAXNBJEU 32
#ifdef CG_BIGMEM
#define MAXNBMARKERS 10000
#else
#define MAXNBMARKERS 100000
#endif

/*class BioJeu;*/
class BJS_OR;

/*#include "Marker.h"*/

/** Interface du noyau de calcul */
class CartaGeneTcl : public CartaGene {
 public:
  /** Constructeur. */
  CartaGeneTcl();

  /** Destructeur. */
  ~CartaGeneTcl();

  /** retourne de l'info sur tous les jeux. */
  char** GetArbre(void);

  /** Fusion g�n�tique entre deux jeux.
      @param gauche le num�ro du premier jeu
      @param droite le num�ro du second jeu
      @return un message d'information sur le jeu de donn�es fusionn�
  */
  char *MerGen(int gauche, int droite);

  /** Fusion sur l'ordre entre deux jeux.
      @param gauche le num�ro du premier jeu
      @param droite le num�ro du second jeu
      @return un message d'information sur le jeu de donn�es fusionn�
  */
  char *MergOr(int gauche, int droite);

  /** retourne une carte sous forme de liste
      @param unit l'unit� de distance: k = kosambi ou centiray, h = haldane ou centiray.
      @param nbmap le num�ro de la carte dans le tas
      @return une liste contenant le nombre d'ordres, les nombres de marqueurs par ordre, les LOD par odre, chaque marqueur et sa position.
  */
  char *** GetMap(const char unit[2],int nbmap) const;

  /** retourne les cartes sous les formes d'une liste.
      @param unit l'unit� de distance: k = kosambi ou centiray, h = haldane ou centiray.
      @param nbmap les nbmap meilleurs cartes.
      @return une liste contenant le nombre d'ordres, les nombres de marqueurs par ordre, les LOD par odre, chaque marqueur et sa position. Pour chaque carte
  */
  char **** GetHeap(char unit[2],int nbmap) const;

  /** print the current Pareto frontier from the heap */
  /* FIXME: what part of this is TCL presentation layer and what part is actual business layer ? */
  char *** ParetoInfoG(double ratio);
  /** return a list of list of merged loci by id,
      The first locus of the sublist represent the others that are following.
  */
  char ** GetMerged();
  char ** GetDouble(double lod_threshold);

};

#endif /* _CARTAGENE_TCL_H */
