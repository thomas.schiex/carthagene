//-----------------------------------------------------------------------------
//                            CarthaGene
//
// James C. Nelson
// Dept. of Plant Pathology, Kansas State University, Manhattan, KS, USA
//
// Copyright 2005 James C. Nelson
// This file is distributed under the terms of the Q Public License version 1.0.
// It may be freely included with CarthaGene.
//
// $Id: QPolynomialMatrix.h,v 1.0 2006/02/06
//
//-----------------------------------------------------------------------------
/**
 * @author Joehanes & Nelson
 *
 * A QPolynomialMatrix is a matrix of QPolynomials.
 * Multiplication by another QPolynomialMatrix follows
 * the rules of both matrix and QPolynomial multiplication.
 */
#ifndef __QPOLYNOMIAL_MATRIX__
#define __QPOLYNOMIAL_MATRIX__
#include "QPolynomial.h"

class QPolynomialMatrix
{
  /* Array for internal storage of elements.*/
protected:
	 QPolynomial ***mMatrix;

	/* Row and column dimensions. */
 	int mRows, mColumns;
 	int mMaxDegree;	// the highest degree of any polynomial in the matrix

public:
	
	/* constructor based only on needed size */
	QPolynomialMatrix(int rows, int cols);

	/* constructor that also instantiates QPolynomials */
	QPolynomialMatrix(int rows, int cols, int degree);
	
	/* constructor based on existing array */
	QPolynomialMatrix(double *P, int rows, int cols, int degree);// was ***P

	/* destructor */
	~QPolynomialMatrix();
	
	int getMaxDegree();
	void setMaxDegree(int degree);
	int getNumRows();
	int getNumColumns();
	QPolynomial ***getMatrix();
	void updateMaxDegree();
	void timesScalarEquals(double c);
	void rowTimesScalarEquals(double *rowScalingArr) const;
	void plusEqualsWithRotate(QPolynomialMatrix *addendPM, int rotateColumns);
	QPolynomialMatrix *maskedTimes(QPolynomialMatrix *postPM, int **maskingMatrix, int testValue);
	QPolynomial *extractPoly(int row, int col);
	void evaluateByColumn(double *powersV, double *evals, int column);
	QPolynomialMatrix *computeOuterColumnProduct(int thisCol, QPolynomialMatrix *thatPM,
																int thatCol, bool &allZero);
	bool columnHasAllZeroCoeffs(int col);
	void printPM();	// for debugging

};
#endif

