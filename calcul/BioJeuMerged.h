//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BioJeuMerged.h,v 1.17.2.1 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Prototype de la classe de base BioJeuMerged.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BIOJEUMERGED_H
#define _BIOJEUMERGED_H

#include "CGtypes.h"

#include "BioJeu.h"
#include "CartaGene.h"

/** Classe des jeux de donn�es fusionn�s. */
class BioJeuMerged :public BioJeu {
 public:

  /** La taille de la charni�re. */
  int TailleCharn;

  /** Le premier BioJeu. */
  BioJeu *BJgauche;

  /** le second BioJeu. */
  BioJeu *BJdroite;

  /** Constructeur. */
  BioJeuMerged();

  /** Constructeur.
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param id le num�ro du jeu.
      @param cross type de jeu de donn�e.
      @param nm nombre de marqueurs.
      @param bitjeu champ de bit du jeu de donn�es.
      @param gauche jeu de donn�es.
      @param droite jeu de donn�es. 
  */
  BioJeuMerged(CartaGenePtr cartag,
	       int id,
	       CrossType cross,
	       int nm, 
	       int bitjeu,
	       BioJeu *gauche, 
	       BioJeu *droite);
 
  /** Destructeur. */
  virtual ~BioJeuMerged();

  
  /** Compatibilite entre deux echantillons d'un marqueur
      @param numarq1 le numero du marqueur 1
      @param numarq2 le numero du marqueur 2
  */
  int Compatible(int numarq1, int numarq2) const;

  /** Fusion de deux marqueurs pour la creation d'un haplotype. Le
      marqueur 1 est utilise pour stocker le resultat.
      @param numarq1 marqueur 1 
      @param numarq2 marqueur 2 
      @return nothing.  
  */
  void Merge(int marq1,int marq2) const;


  /** Affichage de l'�chantillon pour un marqueur
      @param numarq le num�ro du marqueur
  */
  void DumpEchMarq(int numarq) const;

  /** Affichage de l'�chantillon complet.
    */
  void DumpEch(void) const;

  /** Calcul du log sous l'hypoth�se d'ind�pendance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param nbdata le nombre de donn�es informatives(o)
      @return le log
    */
  double LogInd(int m1, 
		int m2,
		int *nbdata) const;

  /** Calcul l'esp�rance du nombre de recombinants, (log �volutif).
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param theta
      @param loglike(o)
      @return l'esp�rance      
    */
  double EspRec(int m1, 
		int m2, 
		double theta, 
		double *loglike) const;

  /** Affichage de la matrice des LOD scores 2pt.
    */
  void DumpTwoPointsLOD() const;
  
  /** Affichage de la matrice des FR 2pt.
    */
  void DumpTwoPointsFR() const;

  /** Mise � 0
    */
  void ResetNbEMCall() {
    NbEMCall = NbEMHit = NbEMUnconv = 0;
    BJgauche->ResetNbEMCall(); 
    BJdroite->ResetNbEMCall();
  };

  /** Affichage     
    */
  void PrintEMCall() {
    if (NbEMCall) {
      print_out("           Set %2d : %d (%d,%d)\n", Id, NbEMCall,NbEMHit,NbEMUnconv);
    } 
    BJgauche->PrintEMCall();
    BJdroite->PrintEMCall();
  };

  
  /** Test de l'existance d'un RH dans l'arborence
      @return 0 ou 1
  */
  int HasRH() const {return(BJgauche->HasRH() || BJdroite->HasRH());};

  /** Test if a RISib or a RISelf dataset exist in the tree.
      It returns either the CrossType or Unk(used as logical False)
  */
  CrossType HasRI() const {
    if (BJgauche->HasRI())
      return(BJgauche->HasRI());
    else
      return (BJdroite->HasRI());
  };

 protected:

  /** Acc�s � une valeur LOD score 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return le lod
  */
  virtual double GetTwoPointsLOD(int m1, int m2) const;
  
};

#endif /* _BIOJEUMERGED_H */
