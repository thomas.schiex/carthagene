#ifndef _CARTHAGENE_PCE_H_
#define _CARTHAGENE_PCE_H_

#include "Genetic.h"
#include "System.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <algorithm>

namespace detail {
template <class BJ>
    struct check_bj {
        typedef typename BJ::ToSinkType ToSinkType;
        typedef typename BJ::SourceToType SourceToType;

        inline void _(...) {}

        check_bj() {
            _(static_cast<void(BJ::*)(ToSinkType)>(&BJ::init_ToSink));
            _(static_cast<void(BJ::*)(SourceToType)>(&BJ::init_SourceTo));
        }
    };
}

/* template to handle parallel computation of the expectation at a given step of EM
 *
 * USE_LOCK defines two flavours :
 * - directly accumulate in the output array ("expected") using a spin lock to synchronize
 * - store every partial result independently in a matrix ("tmp_expected") and only accumulate
 *   after the end of the computation.
 */
template <class BJ, bool USE_LOCK>
struct parallel_compute_expected;

/* Common part
 */
template <class BJ, class PCE>
struct parallel_compute_expected_base {
    Parallel::Spin ll_spin; /* spin lock to safely accumulate loglike value */
    double loglike;         /* loglike accumulator */
    BJ* bj;                 /* BioJeu used to determine the log-likelihood of a map */
    const Carte* map;       /* the map that we want to assess the log-ll for */
    double* expected;       /* pointer to the double[] used in the calling ComputEM */
    size_t row_size;        /* how many markers in the map (PLUS ONE) */
    size_t col_size;        /* how many observations per marker */

    parallel_compute_expected_base(BJ* bj_, const Carte* m, double* e)
        : ll_spin(), loglike(0), bj(bj_), map(m), expected(e)
        , row_size(1+m->NbMarqueur), col_size(bj_->TailleEchant)
    {}

    void operator()(size_t i) {
        typename BJ::EM_state state(map->NbMarqueur);                /* just like thread-local storage :) */
        double llincr = bj->ComputeSourceTo(state.SourceTo, i, map); /* compute loglike increment asynchronously */
        ll_spin.lock();
        loglike += llincr;                                           /* accumulate loglike synchronously */
        ll_spin.unlock();
        bj->ComputeToSink(state.ToSink, i, map);
        bj->ComputeOneExpected(state, i, static_cast<PCE*>(this));
    }
};


/* locking flavour */
template <class BJ>
struct parallel_compute_expected<BJ, true>
    : public parallel_compute_expected_base<BJ, parallel_compute_expected<BJ, true> >
{
    typedef parallel_compute_expected_base<BJ, parallel_compute_expected<BJ, true> > base;
    Parallel::Spin acc_spin; /* spin lock to safely accumulate in expectation vector */

    using base::expected;

    parallel_compute_expected(BJ* bj_, const Carte* m, double* e)
        : base(bj_, m, e)
        , acc_spin()
    {}

    ~parallel_compute_expected() {}

    void accumulate_expected(int i, int mark, double score) {
        acc_spin.lock();
        expected[mark] += score;
        acc_spin.unlock();
    }
};


/* store-and-reduce flavour */
template <class BJ>
struct parallel_compute_expected<BJ, false>
    : public parallel_compute_expected_base<BJ, parallel_compute_expected<BJ, false> >
{
    typedef parallel_compute_expected_base<BJ, parallel_compute_expected<BJ, false> > base;
    double* tmp_expected;

    using base::expected;
    using base::row_size;
    using base::col_size;

    parallel_compute_expected(BJ* bj_, const Carte* m, double* e)
        : base(bj_, m, e)
        , tmp_expected(new double[row_size*col_size])
    {
        std::fill(tmp_expected, tmp_expected+(row_size*col_size), 0.);
    }

    ~parallel_compute_expected() {
        /* accumulate now.
         * at this point, there is only one computation context, so there is no need for locks.
         */
        for(size_t j=0;j<row_size;++j) {
            expected[j] = tmp_expected[j];
        }
        for(size_t i=1;i<col_size;++i) {
            for(size_t j=0;j<row_size;++j) {
                expected[j] += tmp_expected[i*row_size+j];
            }
        }
        delete tmp_expected;
    }

    void accumulate_expected(int i, int mark, double score) {
        *(tmp_expected+(row_size*(i-1))+mark) += score;
    }
};




#endif

