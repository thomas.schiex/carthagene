//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Tas.h,v 1.24.2.1 2012-08-02 15:43:55 dleroux Exp $
//
// Description : Prototype de la classe Tas. Gestion de cartes.
// Divers :
//-----------------------------------------------------------------------------


#ifndef _TAS_H
#define _TAS_H

#include "CGtypes.h"
#include "Carte.h"
#include "System.h"

class CartaGene;

typedef CartaGene *CartaGenePtr;

#define HASHSIZE 2069
#define MAXHEAPSIZE 15
#define MAXHITS 3

/** Classe de stockage d'informations sur une carte */
class StructHMap {

 public:

  /** La carte stock�e dans le tas */
  Carte *map;
  
  /** Temps qu'il a fallu pour trouver cette carte */
  double TimeStamp;

  /** */
  unsigned short BirthDate;

  /** Nombre de fois que la carte a �t� �lue */
  unsigned short NbHits;
  
  /** Pointeur sur la carte suivante dans la liste */
  StructHMap *Next;
  
  /** */
  StructHMap **InH;

  /** Constructeur 
    @param cartage acc�s aux informations du syst�me
    @param n le nombre de marqueurs
  */
  StructHMap(CartaGene *cartage, int n);

  /** Destructeur */
  ~StructHMap();

};


/** Classe de gestion de cartes dans un tas */
class Tas {
 public:
  /** Acc�s � l'ensemble des informations du syst�me. */
  CartaGene *Cartage;

  /** Drapeau du mode emp�chant de stocker des cartes redondantes
   (cas de la fusion sur l'ordre)
  */
  int EquivalenceFlag;

  /** */
  int HeapSize;

  /** */
  int MaxHeapSize;

  /** Constructeur de la classe Tas.
      @param cartag acc�s � l'ensemble des informations du syst�me. 
  */
  Tas();

  /** Destructeur de la classe Tas. */
  ~Tas();

  /** Initialisation du Tas. 
    @param cartag acc�s � l'ensemble des informations du syst�me. 
    @param mhs taille max du tas
  */
  void Init(CartaGenePtr cartag, int mhs = MAXHEAPSIZE);

  /** Initialisation du Tas.
      Le chrono ainsi que le nombre d'appels � EM ne sont pas remis � 0.
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param mhs taille max du tas
  */
  void Initsoft(CartaGenePtr cartag, int mhs = MAXHEAPSIZE);
  
  /** Affiche les cartes contenues dans le Tas.
      Comparaison de l'ordre des marqueurs par rapport
      � la meilleur carte
      @param  nbm le nombre de marqueur minimum par intervalles
      @param  blank indice ou blanc
      @param  comp compr�sion des intervalles ou pas
  */
  void PrintO(int nbm, int blank, int comp) const;

  /** Affiche les cartes contenues dans le Tas. */
  void Print(void) const;

  /** Affiche les cartes contenues dans le Tas. */
  void PrintD(void) const;

  /** Affiche les cartes contenues dans le Tas. */
  void PrintSort() const;

  /** retourne un tableau d'indice de cartes contenues dans le Tas(tri�es). 
      @return le tableau d'indice.
  */
  intPtr IdSorted() const;

  /** Affiche les cartes contenues dans le Tas. */
  void PrintDSort() const;

  /** Insert un nouvel �l�ment (une carte est copi�e). */
  int Insert(Carte *map, int iter);
  
  /** Supprime le plus mauvais �l�ment du tas.
      Suppose que le tas est non vide.
  */
  int Extract(void);

  
  /** Fournit des informations "statistiques"
      
      @param thres un seuil de LOD.
      @return une chaine de car. ....
  */
  char* Stat(double thres);
  
  /** Quelle est la meilleure carte.
      @return  un pointeur sur le meiileur �l�ment du tas.
  */
  Carte *Best(void) const;

  /** Quel est le Delta entre la meilleure et sa suivante
      @return  le delta.
  */
  double Delta(void) const;

  /** Quelle est le derni�re carte dans la liste.
      Retourne un pointeur sur le plus mauvais �l�ment du tas.
  */

  inline void Equi(int flag) {  
    if (flag < 0 || flag > 1) 
      {
	print_err(  "Error : possible values are 0 or 1\n");
	return;
      }
    
    EquivalenceFlag = flag;
  };

  /** Quelle est le derni�re carte dans la liste.
      Retourne un pointeur sur le plus mauvais �l�ment du tas.
  */

  inline Carte *Worst(void) { return Heap[0]->map; };

  /** retourne la carte � partir de sont Id.
      Retourne un pointeur sur le plus mauvais �l�ment du tas.
  */

  inline Carte *MapFromId(int id) const { return Heap[id]->map; };
    
  /** Quelle est le derni�re carte dans la liste.
	Retourne un pointeur sur la structure HMap contenant le plus mauvais
	�l�ment du tas.
  */
  inline HMap *Worst2(void) { return Heap[0]; };
      
  /** Sauve les r�sultats.
      Enregistre la meilleur carte dans le fichier mapfile, enregistre dans
      le fichier otherfile un certains nombre de r�sultats dont : le temps
      de calcul (temps utilisateur pour syst�mes Unix, temps �coul� pour
      Windows et MacOS), le nombre de cartes ayant un LogLike sup�rieur au 
      seuil et le nombre d'appel aux m�thodes EM.
      @param mapfile Fichier dans lequel est enregistr� la meilleure carte.
      @param otherfile Fichier dans lequel est enregistr� les r�sultats statistiques.
      @param thres Seuil ????????
      @param quiet Drapeau du mode silencieux indiquant si les r�sultats sont affich�s.
      @param GT le g�notype sur lequel porte les calculs.
      @param Le r�sum� des cartes contenues dans le tas sous forme de liste.
    */

//  ListeMiniCarte * WriteBest(char *mapfile, char *otherfile, double thres,
//			     int quiet, Genotype *GT);
  
 private:
  /** */
  double worst;
  HMap *BestMap;
  
  /** */
  HMap *HashTable[HASHSIZE];
  
  /** */
  HMap **Heap;
  
  /** */
  Chronometre Chrono;

  /** */
  HMap **HashLocate(const Carte *map, HMap** ptr);

  /** probablement nullasse. */
  unsigned int HashMap(const Carte *map);

  /** Force les propri�t�s du tas. */
  void Heapify(int i);

  /** Force les propri�t�s du tas. */
  void heapify(HMap*** aheap, int heapsize, int i) const;
};

#endif /* _TAS_H */
