#ifndef _CG_LADJ_H_
#define _CG_LADJ_H_

/* ex: set tabstop=4 expandtab! */

#include <vector>

#ifdef DEBUG_LADJ
#include <iostream>
#endif

#define PACK_MASK 0x7F
#define PACK_SHIFT 7
#define PACK_FLAG 0x80

/*#include "config.h"*/
#include "../parallel/parallel.h"
/*#include <tbb/concurrent_vector.h>*/

class LAdj {
	public:
		typedef unsigned int datatype;
		typedef unsigned char packed;
		typedef datatype expanded;
	private:
		std::vector<packed> deltas;

		expanded base;
		Parallel::Mutex mutex;
		expanded last;
		inline void pack(unsigned int x) {
			/*std::cout << "packing " << x << std::endl;*/
			mutex.lock();
			expanded tmp=x;
			x-=last;
			/*std::cout << "  delta is " << x << std::endl;*/
			while(x>=PACK_MASK) {
				/*std::cout << "  packing 7LSB (with flag) " << ((int)(x&PACK_MASK)) << std::endl;*/
				deltas.push_back(PACK_FLAG|(x&PACK_MASK));
				x>>=PACK_SHIFT;
			}
			/*std::cout << "  packing 7MSB " << ((int)x) << std::endl;*/
			deltas.push_back(x);
			last=tmp;
			mutex.unlock();
		}
		friend class iterator;

	public:
		class iterator {
			private:
				const LAdj& _;
				datatype base;
				/*std::vector<packed>::const_iterator b, e;*/
				std::vector<packed>::const_iterator b;
			public:
				iterator(const LAdj& la) :
					_(la),
					base(la.base),
					b(la.deltas.begin()) {}

				datatype next() {
					/*std::cout << "unpacking..." << std::endl;*/
					packed x=*b++;
					datatype delta = (x&PACK_MASK);
					int shift=PACK_SHIFT;
					while(x&PACK_FLAG) {
						/*std::cout << "  got a flag ; delta = " << delta << std::endl;*/
						x = *b++;
						delta |= ((x&PACK_MASK)<<shift);
						shift+=PACK_SHIFT;
					}
					/*std::cout << "  delta = " << delta << std::endl;*/
					base += delta;
					/*std::cout << "  => " << base << std::endl;*/
					return base;
				}

				bool at_end() {
					return b==_.deltas.end();
				}
		};

		LAdj(int b) : deltas(), base(b), mutex(), last(b) {}
		virtual ~LAdj() { deltas.clear(); }

		void add(datatype x) {
			pack(x);
		}

#ifdef DEBUG_LADJ
		void debug() const {
			std::vector<packed>::const_iterator b=deltas.begin(), e=deltas.end();
			while(b!=e) {
				packed x = *b++;
				std::cout << (int)x;
				if(x&PACK_FLAG) {
					std::cout << ' ';
				} else {
					std::cout << std::endl;
				}
			}
			iterator i(*this);
			while(!i.at_end()) {
				std::cout << i.next() << std::endl;
			}
		}
#endif

		iterator iter() const {
			return iterator(*this);
		}

		unsigned long bloat() const {
			return sizeof(LAdj)+deltas.size();
		}
};

#endif

