//-----------------------------------------------------------------------------
//                            CarthaGene
//
// James C. Nelson
// Dept. of Plant Pathology, Kansas State University, Manhattan, KS, USA
//
// Copyright 2006 James C. Nelson
// This file is distributed under the terms of the Q Public License version 1.0.
// It may be freely included with CarthaGene.
//
// $Id: BJS_BS.h,v 1.0 2006/02/06
//
//-----------------------------------------------------------------------------

#ifndef __BJS_BS__
#define __BJS_BS__

#include "BJS_IC.h"
#include "QMatingOperator.h"
#include "QPolynomialMatrix.h"
#include <cstring>

using legacy::BJS_IC;

const int MAX_BS_GENERATIONS = 8;	// why would anyone want to compute more?
const int Num_CG_Phenotypes = 16;
// More consts are defined below the class declaration block

/** Classe des jeux de donn?es backcross/self (sequence of indeterminate length). */
class BJS_BS : public BJS_IC
// If inherit instead from BJS_IC, can use some of that code (such as ComputeExpected())
{
 protected:
 	char mDesignString[MAX_BS_GENERATIONS + 1];
 	QPolynomialMatrix *mXoverGenoProbsM; // a matrix of polynomials representing joint probs of genotype & xover num
 	QPolynomialMatrix *mGenotypeCPPolyV; // " " " " " probs of 2-locus genotypes in the popn
 	QPolynomial *mRIM_factorP;	// a QPolynomial representing 
 		// the number of Recombinationally Informative Meioses available per informative individual
	int mMaxPossibleXovers;	// max crossovers that could occur under present mating design
	int mLocalNPossibles[Num_CG_Phenotypes]; // version of NPossibles with one entry adjustable
	int **mPhenoFreqs;	// array used for temporary storing of 2-locus genotype frequencies
	doubleArrPtr mMarkerGenoCPs; // " " " " " the numerical probabilities of the 10 2-locus genotypes,
																// conditioned on single-locus probs
	doubleArrPtr mThetaPowers;	// saves lots of recomputation of powers of theta in E step
	double *mSingleLocusGenoProbs;// array used for initializing HMM probs for
		// multipoint likelihood calculation, and for conditioning 2-locus and geno+crossover probs
	double *mSingleLocusGenoInvProbs;	// utility array consisting of the inverses of the nonzero
		// elements of mSingleLocusGenoProbs. It's just to save flops, OK?
				
		// Note that SourceTo and ToSink will point at 2D arrays as they do in other CG classes,
		// but the dimensions have been swapped, so instead of 4 pointers to NbMarqueur elements
		// there are NbMarqueur ptrs to 4 elements. Useful for making code smaller & maybe faster.
/** tableaux des probabilit?s conditionnelles de passage par un sommet depuis la gauche */
  doubleArrPtr mSourceTo; // I'd call them state probabilities conditional on the states to the left

  /** tableaux des probabilit?s conditionnelles de passage par un sommet depuis la droite */
  doubleArrPtr mToSink;	// these are conditional on the states to the right
  
 public:
  // DL
  /** redefinition of BioJeu::GetDataType() */
  const char* GetDataType() const {
	static char bsbuf[2+1+MAX_BS_GENERATIONS+1];
	sprintf(bsbuf, "bs %s", mDesignString);
	return bsbuf;
	  
  }

  /** Constructeur. */
  BJS_BS();

  /** Constructeur par recopie. */
  BJS_BS(const BJS_BS&b);

  /** Constructeur.
      @param cartag acc?s ? l'ensemble des informations du syst?me. 
      @param id le num?ro du jeu.
      @param nomjeu nom du jeu
      @param cross type de jeu de donn?e.
      @param nm nombre de marqueurs.
      @param design_string, the sequence of mating-operation symbols
      @param taille de l'?chantillon
      @param bitjeu champ de bit du jeu de donn?es.
      @param indmarq le vecteur d'indice de marqueur
      @param echantil la matrice des informations
    */
  BJS_BS(CartaGenePtr cartag,
	 int id,
	 charPtr nomjeu,
	 CrossType cross,
	 char *design_string,	// added by CN 11.05
	 int nm, 
	 int taille,
	 int bitjeu,
	 int *indmarq,
	 Obs **echantil);
  
  /** Destructeur. */
  ~BJS_BS();
	
	  /** Calcul du LOD score et de fraction de recombinants 2pt.
      @param m1 le num?ro du premier marqueur
      @param m2 le num?ro du second marqueur
      @param epsilon le seuil de convergence
      @param fr la fraction de recombinants(i,o)
      @return le LOD
  */
  double ComputeOneTwoPoints(int m1, 
			     int m2,
			     double epsilon,
			     double *fr) const; 
	int Compatible(int numarq1, int numarq2) const;
  void Merge(int marq1,int marq2) const;
  double LogInd(int m1, int m2, int *nbdata) const;
  // declare & define even though not needed in this class -- to satisfy compiler
  
  double EspRec(int m1,
			int m2, 
			double theta, 
			double *loglike) const;

  /** Etape d'Expectation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le loglike
  */
//	double ComputeExpected(const Carte *data, double *expected);// use BJS_IC version
	void ComputeOneExpected(int Ind, 
				const Carte* map, 
				double *expected) const;

  /** Etape de Maximisation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le fait qu'une limite de domaine d'optimisation a ete atteinte
  */
//	int Maximize(Carte *data, double *expected) const; // we'll use BioJeu's version
	
  void Prepare2pt(int m1, int m2,  int *ss) const {
    print_out("Bug, m?thode interdite = Prepare2pt!\n");
    return;
  };

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const  {
    print_out("Bug, m?thode interdite = Estimate2pt!\n");
    return;
  };

  double LogLike2pt(double *par, int *ss) const  {
    print_out( "Bug, m?thode interdite = LogLike2pt!\n");
    return 0.0;
	};

 private:
 	void SetUpReferenceArrays(int nbMarq);
	void CountPhenos(int m1, int m2) const;
	void SetUpLocalNPossibles();
	QPolynomialMatrix *CreateF1_PMF(int numColumns);
	void ComputeProbabilityCoeffs();
	void FillOutPowersVector(double d, int degree, double *powersV) const;
	bool WasLastOpSelfOrIntercross() const;
	void ComputeMaxPossibleCrossovers();
	double NormalizeAndExpect(double *probs, int numProbs, double &phenoProb, bool calcExpect) const;
	void AdjustProbsForDesign();
	void ConditionGenotypeProbs();
	void UpdateEStepArrays(const Carte *data);	// now use superclass BJS_IC implementation
	QMatingOperator *FetchMatingOperator(char ch, QMatingOperator **qMoPs);
	void DisposeMatingOperators(QMatingOperator **qMoPs);

  /** Pr?paration des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  void PreparEM(const Carte *data);
  
  /** nettoyage des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  void NetEM(const Carte *data);

  /** Calcul de la probabilite de passage par une arete suivant la paire de genotypes relies
      @param a le premier g?notype
      @param b le second g?notype
      @param p la proba de recombinaison
      @return la probabilit?
  */
  // double ProbArete(int a, int b, double p) const;	// unused CN 11.26.05

  /** Calcul des probabilites d''existence d''un chemin "depuis la gauche".
      @param Ind l'individu
      @param map la carte
      @return le loglike
    */
  double ComputeSourceTo(int Ind, const Carte *map) const;

  /** Calcul des probabilites d''existence d''un chemin "depuis la droite".
      @param Ind l'individu
      @param map la carte
      @return le loglike
  */
  double ComputeToSink(int Ind, const Carte *map) const;

  /** Etape elementaire d''Expectation de EM.
      @param Ind l'individu
      @param map la carte
      @param expected vecteur d'expectation
  */

};

	const int CALC_EXPECT = true;
	const int DONT_CALC_EXPECT = false;
	
	const double kInvalidTheta = -1.0; // for returning when theta can't be estimated
	const double kInvalidLogLike = 1000; // " " " likelihood is unspeakable
	const double kInitialThetaEstimate = 0.35; // what CG wants to use, so our class does it too
	// const double kTolerance = 1.0e-3;	// for linkage EM testing
	// const int kMaxIterations = 30;	// for linkage EM iterations
	// const double kLog10EvenProbability = -0.30103; // used in calculating LODs
	 
	// The following array gives indices into the genotype dimension (rows) of
	// matrices mXoverGenoProbsM and mGenotypeCPPolyV. It's analogous to the NPossibles
	// array of CG's BJS_IC class. Labeling in either dimension is aa Aa aA AA as given by the enum above.
	const int GenotypeDirectorArr[NUM_PHASED_DIPLOID_GENOTYPES][NUM_PHASED_DIPLOID_GENOTYPES] =
	{
	{0,	3,	3,	7},
	{1,	4,	5,	8},
	{1,	5,	4,	8},
	{2,	6,	6,	9}};

	// The following array halves some PMF probabilities to compensate for their
	// double counting in CG code. Notice that the halved classes are all but the corner (doubly
	// homozygous) classes indexed by the directing array above.
	const double gPMF_ScalerV[Num_CG_Phenotypes] = {1, 0.5, 1, 0.5, 0.5, 0.5, 0.5, 1, 0.5, 1};

	// The next array gives for each member of a 2-locus genotype pmf array the index of
	// the single-locus prob array entry to condition on (divide by)
	const int gConditionerV[NUM_DIPLOID_GENOTYPE_COMBOS] = {paa, pAa, pAA, paa, pAa, paA, pAA, paa, pAa, pAA};

#endif
