#include "read_data.h"

namespace read_data {

    struct CrossReader {
        static follow_set_t& get_follow_set() {
            static bool is_init = false;
            static follow_set_t follow_set;
            if(!is_init) {
#               define add_follow(k, f) follow_set.insert(std::pair<std::string, follow>(k, f));
                add_follow("", follow("data", Unk));
                add_follow("data", follow("type", Unk));
                add_follow("type", follow("f2", Unk));
                add_follow("f2", follow("backcross", BC));
                add_follow("f2", follow("intercross", IC));
                add_follow("type", follow("ri", Unk));
                add_follow("ri", follow("sib", RISib));
                add_follow("ri", follow("self", RISelf));
                add_follow("type", follow("radiated", Unk));
                add_follow("radiated", follow("hybrid", RH));
                add_follow("hybrid", follow("diploid", RHD));
                add_follow("hybrid", follow("error", RHE));
                add_follow("type", follow("bs", BS));
                add_follow("type", follow("constraint", Con));
                add_follow("type", follow("order", Ordre));
                is_init = true;
#               undef add_follow
            }
            return follow_set;
        }

        static cross_metadata_t read(std::string str) {
            std::istringstream iss(str);
            std::string new_word;
            std::string current = "";
            CrossType ret = Unk;
            follow_set_t follow_set = get_follow_set();
            std::streamsize wpos;
            bool unknown_word;
            while(true) {
                follow_set_range_t range = follow_set.equal_range(current);
                if(range.first == range.second) { break; }
                iss >> std::ws;
                wpos = iss.tellg();
                iss >> new_word;
                unknown_word = range.first == range.second;
                while(range.first != range.second && range.first->second.first != new_word) {
                    ++range.first;
                }
                if(range.first == range.second) { iss.seekg(wpos); break; }
                ret = range.first->second.second;
                if(ret == BS) { break; }
                current = new_word;
            }
            iss >> std::ws;
            if(ret == BS) {
                if(iss.tellg() == -1 || iss.tellg() == (std::streampos)iss.str().size()) {
                    std::cerr << "Error: expected a design string after 'bs' in header line" << std::endl;
                    ret = Unk;
                }
            } else if(ret != Unk) {
                if(iss.tellg() != -1 && iss.tellg() != (std::streampos)iss.str().size()) {
                    std::cerr << "Error: expected end of line after " << current << " in header line" << std::endl;
                    ret = Unk;
                }
            } else {
                follow_set_range_t range = follow_set.equal_range(current);
                if(unknown_word) {
                    std::cerr << "Error: unexpected word in header line at "
                        << wpos << ": " << new_word << std::endl;
                } else {
                    std::cerr << "Error: expected something after '" << current << "' in header line" << std::endl;
                }
                ret = Unk;
            }
            iss >> std::ws;
            if(iss.tellg() != -1 && iss.tellg() < (std::streampos)iss.str().size()) {
                return cross_metadata_t(ret, iss.str().substr(iss.tellg()).c_str());
            } else {
                return cross_metadata_t(ret, "");
            }
        }
    };


    std::string read_line(std::ifstream& ifs, int& lineno) {
        std::vector<char> buffer;
        buffer.resize(32768);
        buffer[0] = '#';
        while(buffer[0] == '#') {
            ifs.getline(&buffer[0], buffer.size() - 1);
            ++lineno;
            if(ifs.fail()) {
                std::cerr << "Error: couldn't read line from file" << std::endl;
            }
        }
        int sz = strlen(&buffer[0]);
        if(buffer[sz] == '\r') {
            buffer[sz] = 0;
        }
        return std::string(&buffer[0]);
    }


    void read_second_line(std::istringstream& second_line, raw_data& ret) {
        int nqt = 0;
        bool casesensitive = false;
        second_line >> ret.ni;
        if(second_line.bad()) {
            std::cerr << "Error: couldn't read number of progeny in line 2 of file" << std::endl;
            ret.Cross = Unk;
            return;
        }
        second_line >> ret.nm;
        if(second_line.bad()) {
            std::cerr << "Error: couldn't read number of markers in line 2 of file" << std::endl;
            ret.Cross = Unk;
            return;
        }
        second_line >> nqt;
        if(second_line.bad()) {
            std::cerr << "Error: couldn't read number of quantitative traits in line 2 of file" << std::endl;
            ret.Cross = Unk;
            return;
        }
        if(nqt != 0) {
            std::cerr << "Error: CarthaGene doesn't support quantitative traits (should be 0)" << std::endl;
            ret.Cross = Unk;
            return;
        }
        if(!second_line.eof()) {
            std::string opt;
            second_line >> opt;
            if(opt == "case") {
                casesensitive = true;
                second_line >> opt;
                /* FIXME: carthagene case-sensitive or not ? always ? never ? */
            }
            if(opt == "symbols") {
                while(!second_line.eof()) {
                    second_line >> opt;
                    if(opt.size()!=3 || opt[1] != '=') {
                        std::cerr << "Error: syntax error on line 2 of file, expected <char>=<char> and found " << opt << std::endl;
                        ret.Cross = Unk;
                        return;
                    }
                    ret.tr[(int)opt[0]] = opt[2];
                }
            }
        }
    }


    MarkerRow read_marker(std::istringstream& ifs, raw_data& ret, int lineno, bool& ok) {
        char c;
        std::string mark;
        MarkerRow row;
        ifs >> c;
        ok = false;
        if(c != '*') {
            std::cerr << "Error: expected a star '*' at beginning of line " << lineno << std::endl;
            return row;
        }
        ifs >> mark;
        if(std::find(ret.marker_names.begin(), ret.marker_names.end(), mark) != ret.marker_names.end()) {
            std::cerr << "Error: marker name is not unique '" << mark << "'" << std::endl;
            return row;
        }
        ret.marker_names.push_back(mark);
        int i;
        for(i = 0; i < ret.ni && !ifs.eof(); ++i) {
            ifs >> c;
            if(!ret.tr[(int)c]) {
                std::cerr << "Error: invalid observation '" << c << "' for marker '" << mark << "' at "
                    << row.size() << std::endl;
                row.clear();
                return row;
            }
            /* translation is not performed here, but in the final step of CharJeu. */
            row.push_back(c);
        }
        ifs >> std::ws;
        if(i != ret.ni) {
            std::cerr << "Error: missing observations for marker '" << mark << "' (expected "
                      << (ret.nm - row.size()) << " more)" << std::endl;
        } else if(!ifs.eof()) {
            std::cerr << "Error: too many observations for marker '" << mark
                      << "' (got " << (ifs.str().size() - ifs.tellg()) << " stray characters)" << std::endl;
        } else {
            ok = true;
        }
        return row;
    }


    raw_data read_file(const char* filename) {
        int lineno = 0;
        std::ifstream ifs(filename);
        cross_metadata_t md = CrossReader::read(read_line(ifs, lineno));
        raw_data ret(md.first, md.second);
        if(ret.Cross == Unk) {
            return ret;
        }
        if(ret.Cross == Ordre) {
            std::istringstream line(read_line(ifs, lineno));
            line >> ret.nm >> ret.coeff;
            if(ifs.bad()) {
                std::cerr << "Error: couldn't read number of loci or break-point weigth at line "
                          << lineno << " in file " << filename << std::endl;
            }
            ret.chrom = new int[ret.nm + 1];
            ret.chromex = new int[ret.nm + 1];
            ret.position = new int[ret.nm + 1];
            for(int i = 0; i <= ret.nm; ++i) {
                ret.chrom[i] = ret.chromex[i] = 0;
                ret.position[i] = i;
            }
            ifs >> std::ws;
            int num, j;
            int debutchrom = 0;
            std::string mark;
            j = 1;
            while(j <= ret.nm && !ifs.eof()) {
                std::istringstream line(read_line(ifs, lineno));
                if(line.peek() != '*') {
                    line >> num >> mark >> std::ws;
                    if(debutchrom == 0) {
                        debutchrom = j;
                    } else {
                        ret.chromex[debutchrom] = num;
                        ret.chromex[j] = num;
                        for(int s = debutchrom; s <= j; ++s) {
                            ret.chrom[s] = num;
                        }
                        debutchrom = 0;
                    }
                } else {
                    line >> mark >> mark >> std::ws;  /* discard num */
                }
                if(!line.eof()) {
                    line >> ret.position[j];
                }
                ret.marker_names.push_back(mark);

                ++j;
            }
        } else {
            std::istringstream line(read_line(ifs, lineno));
            read_second_line(line, ret);
            if(ret.Cross == Con) {
                while(!ifs.eof()) {
                    std::istringstream line(read_line(ifs, lineno));
                    raw_data::symbolic_constraint con;
                    line >> std::ws;
                    if(line.eof()) {
                        continue;
                    }
                    line >> con.m1 >> std::ws;
                    if(!line.good()) {
                        std::cerr << "Error: syntax error at line " << lineno << " in file " << filename << std::endl;
                        ret.Cross = Unk;
                        return ret;
                    }
                    line >> con.m2 >> std::ws;
                    if(!line.good()) {
                        std::cerr << "Error: syntax error at line " << lineno << " in file " << filename << std::endl;
                        ret.Cross = Unk;
                        return ret;
                    }
                    line >> con.m3 >> std::ws;
                    if(!line.good()) {
                        std::cerr << "Error: syntax error at line " << lineno << " in file " << filename << std::endl;
                        ret.Cross = Unk;
                        return ret;
                    }
                    line >> con.penalty >> std::ws;
                    if(!line.eof()) {
                        std::cerr << "Error: syntax error at line " << lineno << " in file " << filename << std::endl;
                        ret.Cross = Unk;
                        return ret;
                    }
                    /* FIXME: error checking */
                    con.lineno = lineno;
                    ret.constraints.push_back(con);
                }
            } else if(ret.Cross != Unk) {
                bool ok = true;
                while(ret.marker_data.size() < (unsigned)ret.nm) {
                    if(ifs.eof()) {
                        std::cerr << "Unexpected end of file (expected " << (ret.nm - ret.marker_data.size())
                                  << " more markers)" << std::endl;
                        ret.Cross = Unk;
                        return ret;
                    }
                    std::istringstream line(read_line(ifs, lineno));
                    ret.marker_data.push_back(read_marker(line, ret, lineno, ok));
                    if(!ok) {
                        ret.Cross = Unk;
                        return ret;
                    }
                }
            }
        }
        return ret;
    }

}

#ifdef TEST_READER

int test_one_cross(const char* hdr, CrossType cross, const char* sup) {
    bool bad_cross=false, bad_str=false;
    read_data::cross_metadata_t md = read_data::CrossReader::read(hdr);
    bad_cross = cross != md.first;
    bad_str = md.second != sup;
    if(bad_cross || bad_str) {
        std::cerr << "Given '" << hdr << "', expected <" << cross << ',' << sup
            << "> and got <" << md.first << ',' << md.second << '>' << std::endl;
    }

    return (int)!(bad_cross || bad_str);
}


int test_crosses() {
    int done = 0;
    int ok = 0;
    typedef struct { const char* a; CrossType b; const char* c; } cross_test_t;
    cross_test_t cross_tests[] = {
        { "data type f2 backcross", BC, "" },
        { "data type f2 intercross", IC, "" },
        { "data type f2 backcross pouet", Unk, "pouet" },
        { "data type ri", Unk, "" },
        { "data type ri sib", RISib, "" },
        { "data type ri self", RISelf, "" },
        { "data type radiated hybrid", RH, "" },
        { "data type radiated hybrid pouet", Unk, "pouet" },
        { "data type radiated hybrid diploid", RHD, "" },
        { "data type radiated hybrid error", RHE, "" },
        { "data type bs", Unk, "" },
        { "data type bs s", BS, "s" },
        { "data type order", Ordre, "" },
        { "data type constraint", Con, "" },
        { NULL, Unk, NULL }
    };
    cross_test_t* c = cross_tests;
    while(c->a) {
        ok += test_one_cross(c->a, c->b, c->c);
        ++done;
        ++c;
    }
    std::cout << "Passed " << ok << '/' << done << " tests" << std::endl;
    return ok != done;
}



int main(int argc, char** argv) {
    read_data::raw_data x = read_data::read_file("doc/user/exemple/Data/rh.cg");
    std::cout << "Cross=" << x.Cross << std::endl;
    std::cout << x.ni << ' ' << x.nm;
    for(int i = 0; i < 256; ++i) {
        if(x.tr[i] && x.tr[i] != i) {
            std::cout << ' ' << ((char)i) << '=' << x.tr[i];
        }
    }
    std::cout << std::endl;
    read_data::MarkerNames::iterator n = x.marker_names.begin();
    for(read_data::MarkerData::iterator r = x.marker_data.begin(); r != x.marker_data.end(); ++r) {
        std::cout << '*' << *n << '\t';
        for(read_data::MarkerRow::iterator o = r->begin(); o != r->end(); ++o) {
            std::cout << *o;
        }
        std::cout << std::endl;
        ++n;
    }

    return test_crosses();
}

#endif

