//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// $Id: BioJeuSingle.h,v 1.26.2.2 2011-02-24 18:30:44 dleroux Exp $
//
// Description : Prototype de la classe de base BioJeuSingle.
// Divers :
//-----------------------------------------------------------------------------

#ifndef _BIOJEUSINGLE_H
#define _BIOJEUSINGLE_H

#include "CGtypes.h"

#include "BioJeu.h"
#include "CartaGene.h"


/** Classe des jeux de donn�es simples. */
class BioJeuSingle : public BioJeu {
	public:

		/** Les observations. */
		Obs **Echantillon;

		/** Chemin+nom fichier donnees  */
		charPtr NomJeu;


		/** Constructeur. */
		BioJeuSingle();

		/** Constructeur.
		  @param cartag acc�s � l'ensemble des informations du syst�me.
		  @param id le num�ro du jeu.
		  @param nomjeu nom du jeu
		  @param cross type de jeu de donn�e.
		  @param nm nombre de marqueurs.
		  @param taille de l'�chantillon
		  @param bitjeu champ de bit du jeu de donn�es.
		  @param indmarq le vecteur d'indice de marqueur
		  @param echantil la matrice des informations
		  */
		BioJeuSingle(CartaGenePtr cartag,
				int id,
				charPtr nomjeu,
				CrossType cross,
				int nm,
				int taille,
				int bitjeu,
				int *indmarq,
				Obs **echantil);

		/** Destructeur. */
		virtual ~BioJeuSingle();

		/** Constructeur par recopie */
		BioJeuSingle(const BioJeuSingle& b);

		// DL
		/** Output dataset to a character stream */
		virtual void DumpTo(FILE* outFile) const throw(BioJeu::NotImplemented);
        // DL
        /** Update some internal state when a new BioJeu is loaded */
        virtual void _updateIndMarq(int max);

		virtual char obs2chr(Obs o) const throw(BioJeu::NotImplemented);

		/** Retourne un champ des jeux fusionn�s sur l'ordre(!=CON).
		  @return un champ de bits
		  */
		inline int GetBJMO() const
		{
			return BitJeu;
		};

		/** Acc�s � une valeur d'un �chantillon.
		  @param numarq le num�ro du marqueur
		  @param numind le num�ro de l'individu
		  @return l'information de l'individu au marqueur
		  */
		inline enum Obs GetEch(int numarq, int numind) const
		{
			//*
			//      return Echantillon[IndMarq[numarq * ((Cartage->markers[numarq].BitJeu & BitJeu) > 0)]][numind];
			//return Echantillon[IndMarq[numarq]][numind];
			return Echantillon[numarq][numind];
			/*/
			  return (Cartage->markers[numarq].BitJeu&BitJeu)
			  ? Echantillon[IndMarq[numarq]][numind]
			  : Obs1111;
			//*/
		}

		/** Modification d'une valeur d'un �chantillon.
		  @param numarq le num�ro du marqueur
		  @param numind le num�ro de l'individu
		  @param obs l'information de l'individu au marqueur
		  */
		inline void SetEch(int numarq, int numind, Obs obs)
		{
			if(IndMarq_cg2bj[numarq]) {
				/* we don't want to modify an observation that didn't exist in the first place */
				Echantillon[numarq][numind] = obs;
			}
		}

		/** Compatibilite entre deux echantillons d'un marqueur
		  @param numarq1 le numero du marqueur 1
		  @param numarq2 le numero du marqueur 2
		  */
		virtual int Compatible(int numarq1, int numarq2) const = 0;

		/** Fusion de deux marqueurs pour la creation d'un haplotype. Le
		  marqueur 1 est utilise pour stocker le resultat.
		  @param numarq1 marqueur 1
		  @param numarq2 marqueur 2
		  @return nothing.
		  */
		virtual void Merge(int marq1,int marq2) const = 0;

		/** Affichage de l'�chantillon pour un marqueur
		  @param numarq le num�ro du marqueur
		  */

		virtual void DumpEchMarq(int numarq) const;

		/** Affichage de l'�chantillon complet. */
		virtual void DumpEch(void) const;

		/** Affichage de la matrice des LOD scores 2pt. */
		virtual void DumpTwoPointsLOD() const;

		/** Affichage de la matrice des FR 2pt. */
		void DumpTwoPointsFR() const;

		/** Test de l'existance d'un RH dans l'arborence
		  @return 0 ou 1
		  */
		int HasRH() const {return(Cross == RH || Cross == RHD || Cross == RHE);};

		/** Test if a RISib or RISelf dataset exist in the tree.
		  It returns either the CrossType or Unk(used as logical False)
		  */
		CrossType HasRI() const {
			if (Cross == RISelf || Cross == RISib)
				return(Cross);
			else
				return(Unk);
		};

		double ContribOCB(int m1, int m2);
		double NormContribOCB(int m1, int m2);

	protected:

		/** Calcul des valeurs deux points et remplissage des matrices.*/
		virtual void ComputeTwoPoints(void);

		/** Acc�s � une valeur LOD score 2pt.
		  @param m1 le num�ro du premier marqueur
		  @param m2 le num�ro du second marqueur
		  @return le lod
		  */
		virtual double GetTwoPointsLOD(int m1, int m2) const;

		/** Acc�s � la distance 2pt.
		  @param m1 le num�ro du premier marqueur
		  @param m2 le num�ro du second marqueur
		  @return la distance
		  */
		virtual double GetTwoPointsDH(int m1, int m2) const;

		/** Acc�s � la fraction de recombinaison 2pt.
		  @param m1 le num�ro du premier marqueur
		  @param m2 le num�ro du second marqueur
		  @return la fraction de recombinaison
		  */
		virtual double GetTwoPointsFR(int m1, int m2) const;
};


#endif /* _BIOJEUSINGLE_H */
