//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: NOrComp.h,v 1.2.4.1 2011-11-22 14:31:57 dleroux Exp $
//
// Description : Prototype of the NorComp class 
// Divers : Computation of the number of orders at a given breakpoint distance
//-----------------------------------------------------------------------------

#ifndef _NORCOMP_H
#define _NORCOMP_H

#include "CGtypes.h"
#include "System.h"

class NOrComp {
 public:
  
  NOrComp(int n, int k);
  
  ~NOrComp();
  
  void test(int n, int k);
  
  double getNO(int n, int k);
  
  inline void run()
    {
      /*double tmp =*/  getNO(locdim, bpdim);
    };
  
 private:
  
  inline double f_O(int n, int k)
    {
      if (k>=0 && O[n][k] != -1) return O[n][k];
      if (k<=0 || n <= k) return f_Ob(n,k) + f_Oc(n,k);
      O[n][k] = f_Ob(n,k) + f_Oc(n,k);
      return O[n][k];
    };
  
  inline double f_Ob(int n, int k) 
    {
      if (k>=0 && Ob[n][k] != -1) return Ob[n][k];
      if (k<=0 || n <= k) return f_Ib(n,k) + f_Sb(n,k);
      Ob[n][k] = f_Ib(n,k) + f_Sb(n,k);
      return Ob[n][k];
    };
  
  inline double f_Oc(int n, int k) 
    {
      if (k>=0 && Oc[n][k] != -1) return Oc[n][k];
      if (k<=0 || n <= k) return f_Ic(n,k) + f_Sc(n,k);
      Oc[n][k] = f_Ic(n,k) + f_Sc(n,k);
      return Oc[n][k];
    };
  
  inline double f_Ib(int n, int k) 
    {
      if (k<=0 || n <= k || n<3) return 0;
      if (Ib[n][k] != -1) return Ib[n][k];
      Ib[n][k] = f_Ob(n-1,k-1) + 2*f_Oc(n-1,k-1);
      return Ib[n][k];
    };
  
  inline double f_Ic(int n, int k) 
    {   
      if (k<=0 || n <= k || n<3) return 0;
      if (Ic[n][k] != -1) return Ic[n][k];     
      Ic[n][k] = (k-1)*f_O(n-1,k-1) - f_Sc(n,k-1) + (n-k)*f_O(n-1,k-2);
      return Ic[n][k];
    };
  
  inline double  f_Sb(int n, int k) 
    {
      if (k == 0) return 2;
      if (n<2 || n <= k) return 0;
      if (k>=0 && Sb[n][k] != -1) return Sb[n][k];
      if (k<0) return f_Ob(n-1,k);
      Sb[n][k] = f_Ob(n-1,k);
      return Sb[n][k];
    };
  
  inline double f_Sc(int n, int k) 
    {
      if (k<=0 || n <= k || n<3) return 0;
      if (Sc[n][k] != -1) return Sc[n][k];     
      Sc[n][k] = f_Ib(n-1,k) + 2*f_Ic(n-1,k) + f_Sc(n-1,k) + f_Sb(n-1,k-1) + f_Sc(n-1,k-1);
      return Sc[n][k];
    };

  // storage area

  int bpdim;
  int locdim;

  doublePtr **Z;
  doublePtr *O;
  doublePtr *Ob;
  doublePtr *Oc;
  doublePtr *Ib;
  doublePtr *Ic;
  doublePtr *Sb;
  doublePtr *Sc;
};

#endif /* _NORCOMP_H */
