//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_BC.cc,v 1.21.2.3 2012-08-02 15:43:54 dleroux Exp $
//
// Description : BJS_BC.
// Divers :
//-----------------------------------------------------------------------------

#include "BJS_BC.h"
#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>


inline double BCrec2RISelf(double rec) { return (rec/(2.0-2.0*rec)); };
inline double BCrec2RISib(double rec) { return (rec/(4.0-6.0*rec)); };
//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_BC::BJS_BC() : BioJeuSingle()
{
  NbMeiose = TailleEchant;

  PairsHead = NULL;
  Flanking = NULL;
  KnownPairs = NULL;
  PreNonRec = NULL;
  ToSink = NULL;
  SourceTo = NULL;
  PreRec = NULL;
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - le type
// - le nombre de marqueurs
// - la taille de l'echantillon
// - le champ de bit
// - la matrice de l'�chantillon
// Valeur de retour :
//-----------------------------------------------------------------------------

BJS_BC::BJS_BC(CartaGenePtr cartag,
	       int id,
	       charPtr nomjeu,
	       CrossType cross,
	       int nm, 
	       int taille,
	       int bitjeu,
	       int *indmarq,
	       Obs **echantil)
  :BioJeuSingle(cartag,
		id,
                nomjeu,
		cross, 
		nm, 
		taille, 
		bitjeu, 
		indmarq, 
		echantil)
{
  NbMeiose = TailleEchant;

  PairsHead = NULL;
  Flanking = NULL;
  KnownPairs = NULL;
  PreNonRec = NULL;
  ToSink = NULL;
  SourceTo = NULL;
  PreRec = NULL;

  ComputeTwoPoints();
}


BJS_BC::BJS_BC(const BJS_BC& b)
        : BioJeuSingle(b)
{
  NbMeiose = TailleEchant;

  PairsHead = NULL;
  Flanking = NULL;
  KnownPairs = NULL;
  PreNonRec = NULL;
  ToSink = NULL;
  SourceTo = NULL;
  PreRec = NULL;

  ComputeTwoPoints();
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_BC::~BJS_BC()
{
}

//-----------------------------------------------------------------------------
// Compatibilite des observations entres individus
//-----------------------------------------------------------------------------
int BJS_BC::Compatible(int numarq1, int numarq2) const
{
  int i;
  enum Obs Obs1, Obs2;

  // on teste si les deux marqueurs sont definis dans le jeu de donnees
  if ((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu & BitJeu))
    {
      // on compare si les donnees sont compatibles en chaque point
      for (i = 1; i<= TailleEchant; i++) {
	Obs1 = GetEch(numarq1,i);
	Obs2 = GetEch(numarq2,i);
	if (!((Obs1 == Obs1111) || (Obs2 == Obs1111) || (Obs1 == Obs2)))
	  return 0;
      }
    }
  return 1;
}

//-----------------------------------------------------------------------------
// Fusion des observations entres individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
//-----------------------------------------------------------------------------
void BJS_BC::Merge(int numarq1, int numarq2) const
{
  int i;
  enum Obs TheObs;

  // les deux marqueurs sont ils definis dans le jeux de donnees ?
  if ((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu & BitJeu))
    {
      // Oui, on va fusionner dans le premier
      for (i = 1; i<= TailleEchant; i++) {
	if ((TheObs = Echantillon[numarq1][i]) == Obs1111)
	  TheObs = Echantillon[numarq2][i];
	Echantillon[numarq1][i] = TheObs;
      }
    }
}

//-----------------------------------------------------------------------------
// Retourne l'esp�rance du nombre de recombinants (cas g�n�ral - 2 points).
//-----------------------------------------------------------------------------
// Param�tres :
// - indice du premier marqueur dans le jeu
// - indice du second marqueur dans le jeu
// - le theta
// - le nombre de donn�es informatives
// - le loglike (o)
// Valeur de retour : l'esp�rance du nombre de recombinaisons.
//-----------------------------------------------------------------------------
 
double BJS_BC::EspRec(int m1, 
		      int m2, 
		      double theta,
		      double *loglike) const
{
  int nrec = 0, ndata = 0;
  double HLink;
  
  for (int i = 1; i <= TailleEchant; i++)
    if ((GetEch(m1, i) != Obs1111) && (GetEch(m2, i) != Obs1111)) {
      ndata++;
      if (GetEch(m1, i) != GetEch(m2, i)) nrec++;
    }

  HLink = (ndata-nrec)*log10(1.-theta);
  if (theta > 0.0)
    HLink += nrec*log10(theta);

  *loglike = HLink;

  return (double)nrec;
}

//-----------------------------------------------------------------------------
// Calcule le nombre de donn�es et 
// le log sous l'hypothese d'absence de liaison (2 points)
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux numeros de marqueurs, on doit avoir m1< m2
// - le nombre de donn�es
// Valeur de retour : le log
//-----------------------------------------------------------------------------

double BJS_BC::LogInd(int m1, 
		      int m2,
		      int *nbdata) const
{
  int  ndata = 0;

  for (int i = 1; i <= TailleEchant; i++)
    if ((GetEch(m1, i) != Obs1111) && (GetEch(m2, i) != Obs1111))
      ndata++;

  *nbdata = ndata;
  return(ndata*log10(0.5));
}

//-----------------------------------------------------------------------------
// Calcule fraction de rec. et
// LOD scores pour deux marqueurs (2 points)
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux numeros de marqueurs, on doit avoir m1< m2
// - epsilon
// - la fraction de reconbinant(i,o)
// Valeur de retour : le lod
//-----------------------------------------------------------------------------

double BJS_BC::ComputeOneTwoPoints(int m1, 
				   int m2, 
				   double epsilon, 
				   double *fr) const
{
  
  int nrec = 0; 
  int  ndata = 0;
  double theta= 0.05;
  double HLink,HNLink;
  double lod;
  
  HNLink = LogInd(m1, m2, &ndata);

  if (ndata) {
    // sp�cificit� 
    for (int i = 1; i <= TailleEchant; i++)
      if ((GetEch(m1, i) != Obs1111) && (GetEch(m2, i) != Obs1111))
	if (GetEch(m1, i) != GetEch(m2, i)) nrec++;
    
    theta = (double)nrec/ndata;

    theta = (theta > EM_MAX_THETA) ? EM_MAX_THETA : 
      ((theta < EM_MIN_THETA) ? EM_MIN_THETA : theta);
    
    nrec = (int)EspRec(m1, m2, theta, &HLink);
    
    switch(Cross) {
    case BC:
      *fr = theta;
      break;
    case RISelf:
      *fr = BCrec2RISelf(theta);
      break;
    case RISib:
      *fr = BCrec2RISib(theta);
      break;
    default :
      print_out(  "BJS_BC.cc::ComputeOneTwoPoints() : impossible pedigree\n");
      return(0.0);
      break;
    }

    lod = HLink-HNLink;
  } else {
    *fr = EM_MAX_THETA;
    lod = 0.0;
  }
  return lod;
}

//-----------------------------------------------------------------------------
//  Etape d' � Expectation � de EM - multipoints
//-----------------------------------------------------------------------------
// Param�tres : la carte
// - la carte
// - le vecteur d'Expectation(i,o)
// Valeur de retour : le loglike 
//-----------------------------------------------------------------------------

double BJS_BC::ComputeExpected(const Carte* map, double *expected)
{
  
  double loglike = 0.0;
  int i, j;
  int k, l;
  Pair *CurPair = PairsHead;
  
  switch(Cross) {
  case BC:
    break;
  case RISelf:
    RISelf2BC(map,expected);
    break;
  case RISib:
    RISib2BC(map,expected);
    break;
  default :
    print_out(  "BJS_BC.cc::ComputeExpected() : pedigree impossible\n");
    //   exit(1);
    return(0.0);
    break;
  }

  while (CurPair) {
    i = CurPair->Left;
    j = CurPair->Right;
    ComputeSourceTo(i, j, map);
    ComputeToSink(i, j, map);
    
    k = CurPair->NumOcc[0];
    l = CurPair->NumOcc[1];
    if (k) {
      UpdateExpectedNR(i, j, k, map, expected);
      loglike  += k*log10(SourceTo[j]);
    }
    if (l) {
      UpdateExpectedR(i, j, l, map, expected);
      loglike  += l*log10(1-SourceTo[j]);
    }
    CurPair = CurPair->Next;
  }
 
  for (i=0; i<map->NbMarqueur; i++) {
    if (Flanking[i][0] > 0)
      for (j=0; j<i; j++)
	expected[j] += Flanking[i][0]*map->tr[j];
    if (Flanking[i][1] > 0)
      for (j = i; j < map->NbMarqueur - 1; j++)
	expected[j] += Flanking[i][1]*map->tr[j];   
  }  
  for (i = 0; i < map->NbMarqueur - 1; i++) {
    loglike += PreNonRec[i]*log10(1.0 - map->tr[i]);
    if (map->tr[i] >= 1e-100)
      loglike += PreRec[i]*log10(map->tr[i]);
  }

  for (j = 0; j < map->NbMarqueur - 1; j++)
    expected[j] += PreRec[j];

  loglike -= log10(2.0) * (TailleEchant-Flanking[0][1]);
  
  switch(Cross) {
  case BC:
    break;
  case RISelf:
    BC2RISelf(map, expected);
    break;
  case RISib:
    BC2RISib(map, expected);
    break;
  default :
    print_out(  "BJS_BC.cc::ComputeExpected() : pedigree impossible\n");
    // exit(1);
    return(0.0);
    break;
  }
  
  return loglike;
}


//-----------------------------------------------------------------------------
//  Pr�paration des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BC::PreparEM(const Carte *data)
{
  int i, j;
  int mark;
  int prevj, prevmark = -1;
  int NbMarqueur = data->NbMarqueur;
  int *ordre = data->ordre;

  PreRec = new int[NbMarqueur - 1];
  SourceTo = new double[NbMarqueur];
  ToSink = new double[NbMarqueur];
  PreNonRec = new int[NbMarqueur - 1];
  KnownPairs = new PairPtrPtr[NbMarqueur];
  Flanking = new CoupleInt[NbMarqueur];
  LocTr =   new double[NbMarqueur-1];
  LocExp =   new double[NbMarqueur-1];

  for (i = 0; i < NbMarqueur; i++) {
    KnownPairs[i] = new PairPtr[NbMarqueur];
    for (int j = 0; j < NbMarqueur; j++)
      KnownPairs[i][j] = NULL;
  }

  // ATTENTION : le programme attend que le contenu du tableau Flanking soit
  // initialis� avec des 0 pour fonctionner correctement.
  // C'est fait avec ComputeKnown() mais pas avec ComputeExpected() et suivant
  // les donn�es trait�es, les r�sultats sont diff�rents entre un tableau
  // dimensionn� � MAX_M ou dynamiquement au nombre de marqueurs (cas de 
  // l'exemple gr3.raw).
  for (i = 0; i < NbMarqueur; i++)
    Flanking[i][0] = Flanking[i][1] = 0;

  for (i = 0; i< NbMarqueur - 1; i++)
   {
     PreRec[i] = PreNonRec[i] = 0;
     for (j=i+1; j < NbMarqueur; j++)
       KnownPairs[i][j] = NULL;
   }
 
  /* pour chaque element de l'echantillon */
  for (i = 1; i <= TailleEchant; i++)
    {
      prevj = -1;
      /* pour chaque marqueur successif, dans l'ordre de l'individu */
      for (j = 0; j< NbMarqueur; j++)
       {
	 mark = ordre[j];
	 /* si on connait le genotype */
	 if (GetEch(mark,i) != Obs1111)
	   {
	     /* que ce n'est pas un flanking */
	     if (prevj != -1)
	       {
		 int recomb = (GetEch(mark,i) != GetEch(prevmark,i));
		 /* si les deux marqueurs sont adjacents */
		 if ((j - prevj) == 1)
		   {
		     /* on a une (non)recombinaion connue */
		     if (recomb) PreRec[prevj]++;
		     else PreNonRec[prevj]++;
		   }
		 /* sinon, c'est un intervalle a traiter */
		 else IncrementPair(prevj, j, recomb);
	       }
	     /* enfin, on comptabilise le flanking */
	     else if (j) Flanking[j][0]++;
	     
	     prevj = j;
	     prevmark = mark;
	   }
       }
      
      /* si on a un inconnu complet....*/
      if (prevj == -1) Flanking[0][1]++;
      /* Si a la fin, le marqueur precedent n'est pas le dernier, on a
	 un flanking */
      
      else if (prevj != NbMarqueur - 1) Flanking[prevj][1]++;
      
    }
}

//-----------------------------------------------------------------------------
//  nettoyage des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres :
// - la carte, 
// Valeur de retour : 
//-----------------------------------------------------------------------------
void BJS_BC::NetEM(const Carte *data)
{
  Pair *PairTemp;

  delete [] LocExp;
  delete [] LocTr;
  delete [] Flanking;
  // Pas besoin de lib�rer le contenu de KnownPairs[i],
  //  c'est fait dans la boucle "while (PairsHead != NULL)"
  for (int i = 0; i < data->NbMarqueur; i++) delete [] KnownPairs[i];
  delete [] KnownPairs;
  delete [] PreNonRec;
  delete [] ToSink;
  delete [] SourceTo;
  delete [] PreRec;

  // On libere la memoire eventuellement allouee
  while (PairsHead != NULL)
  {
    PairTemp = PairsHead->Next;
    delete PairsHead;
    PairsHead = PairTemp;
  }
}

//-----------------------------------------------------------------------------
//  Cree un nouveau cons pour une paire de marqueurs
// encore inexistante si necessaire. Puis incremente le nombre de paires
// rec/non recombinantes.
//-----------------------------------------------------------------------------
// Param�tres : 
// - a,b : la paire de marqueurs
// - r   : 0 ou 1 alias non recombinant ou recombinant.
// Valeur de retour : 
//-----------------------------------------------------------------------------

inline void BJS_BC::IncrementPair(int a, int b, int r)
{
  if (KnownPairs[a][b] == NULL)
  {
    PairPtr NewPair = new Pair;
      
    NewPair->NumOcc[r] = 1;
    NewPair->NumOcc[1-r]= 0;
    NewPair->Left = a;
    NewPair->Right = b;
    NewPair->Next = PairsHead;
    PairsHead = NewPair;
    KnownPairs[a][b] = NewPair; 
  }
  else
    KnownPairs[a][b]->NumOcc[r]++;
}

//-----------------------------------------------------------------------------
// Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite gauche (source)
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

inline void BJS_BC::ComputeSourceTo(int a, int b, const Carte *map)
{

  int i;
  
  /* initialisation au debut du segment */
  SourceTo[a] = 1.0;
  
  for (i=a+1; i<=b; i++) {
    SourceTo[i] = map->tr[i-1]+SourceTo[i-1]-2.0*map->tr[i-1]*SourceTo[i-1];
  }
}


//-----------------------------------------------------------------------------
// Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite droite (puit)
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

inline void BJS_BC::ComputeToSink(int a, int b, const Carte *map)
{
  int i;
  
  /* initialisation en fin de segment */
  ToSink[b] = 1.0;
  
  for (i=b-1; i>=a ; i--)
    ToSink[i]= map->tr[i]+ToSink[i+1]-2.0*map->tr[i]*ToSink[i+1];
}

//-----------------------------------------------------------------------------
//  Mise a jour des � expected � 
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BC::UpdateExpectedR(int a, 
			     int b,
			     int n, 
			     const Carte *map,
			     double *expected)
{

  int i;
  double temp, prec, pnrec;
  
  for (i=a+1; i<=b; i++)
  {
    temp = (SourceTo[i-1]*ToSink[i])+
           ((1-SourceTo[i-1])*(1-ToSink[i]));
      
    prec = temp * map->tr[i-1];
    pnrec = (1-temp)*(1 - map->tr[i-1]);
    temp = prec+pnrec;
    prec /= temp;
    expected[i-1] += n*prec;
  }
}


//-----------------------------------------------------------------------------
//  Mise a jour des � expected � 
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BC::UpdateExpectedNR(int a, 
			      int b,
			      int n, 
			      const Carte *map,
			      double *expected)
{
  int i;
  double temp, prec, pnrec;
  
  for (i=a+1; i<=b; i++)
  {
    temp = (SourceTo[i-1]*(1-ToSink[i]))+
	   ((1-SourceTo[i-1])*ToSink[i]);
      
    prec = temp * map->tr[i-1];
    pnrec = (1-temp)*(1 - map->tr[i-1]);
    temp = prec+pnrec;
    prec /= temp;
    expected[i-1] += n*prec;
  }
}
//-----------------------------------------------------------------------------
// BC2RISelf: adaptation des expected pour un RISelf 
//            traite en BC
//-----------------------------------------------------------------------------
// Param�tres : 
// - The map (estimated as BC)
// - The expectation of the n. rec.
// Valeur de retour :  none, modifies rec rate according to selfing RI 
//-----------------------------------------------------------------------------
void BJS_BC::BC2RISelf(const Carte *map, double *expected)
{
  int i;
  double rec;
  
  for (i = 0; i < map->NbMarqueur - 1; i++) {
    rec =  (expected[i]) / (double)NbMeiose;
    expected[i] = (double)NbMeiose*BCrec2RISelf(rec);
    map->tr[i] = LocTr[i];
    expected[i] += LocExp[i];
  }
}
//-----------------------------------------------------------------------------
// BC2RISelf: adaptation des freq. de recombinaison pour un RISelf 
//            traite en BC
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BC::RISelf2BC(const Carte *map, double* expected)
{
  int i;

  for (i = 0; i < map->NbMarqueur - 1; i++) {
    LocTr[i] = map->tr[i];
    LocExp[i] = expected[i];
    map->tr[i] = (2.0*LocTr[i])/(1.0+2.0*LocTr[i]);
    expected[i] = 0.0;
  }
}
//-----------------------------------------------------------------------------
// BC2RISib: adaptation des expected pour un RISib
//            traite en BC
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BC::BC2RISib(const Carte *map, double *expected)
{
  int i;
  double rec;

  for (i = 0; i < map->NbMarqueur - 1; i++)
    {
      rec =  (expected[i]) / (double)NbMeiose;
      expected[i] = (double)NbMeiose*BCrec2RISib(rec);
      map->tr[i] = LocTr[i];
      expected[i] += LocExp[i];
    }
}

//-----------------------------------------------------------------------------
// BC2RISib: adaptation des freq. de recombinaison pour un RISelf 
//            traite en BC
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BC::RISib2BC(const Carte *map, double* expected)
{
  int i;

  for (i = 0; i < map->NbMarqueur - 1; i++)
    {
      LocTr[i] = map->tr[i];
      LocExp[i] = expected[i];
      map->tr[i] = (4.0*LocTr[i])/(1.0+6.0*LocTr[i]);
      expected[i] = 0.0;
    }
}
