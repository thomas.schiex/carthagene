#ifndef _CG_SYNC_TRAITS_H_
#define _CG_SYNC_TRAITS_H_

#ifdef WITH_PARALLEL

#if TBB_INTERFACE_VERSION < 5000 && !defined(WIN32)
#	include <pthread.h>
#endif

#	warning "Using PARALLEL"
struct SYNCHRONIZATION_TRAITS {
#if TBB_INTERFACE_VERSION >= 5000
	typedef tbb::mutex mutex_type;
	typedef tbb::spin_rw_mutex_v3 rw_spin_type;
	typedef tbb::spin_mutex spin_type;
	typedef tbb::interface5::condition_variable condvar_type;
	typedef tbb::interface5::unique_lock<tbb::mutex> unique_lock_type;
#else
//#	warning Using old TBB ; not everything is available
	class mutex_type {
		public:
			typedef pthread_mutex_t handle_type;
			mutex_type() { pthread_mutex_init(&handle, NULL); }
			void lock() { pthread_mutex_lock(&handle); }
			void unlock() { pthread_mutex_unlock(&handle); }
			handle_type* native_handle() { return &handle; }
		private:
			handle_type handle;
	};
	class rw_spin_type {
		public:
			typedef void handle_type;
			void lock();
			void lock_read();
			void unlock();
	};
	class spin_type {
		public:
			typedef pthread_spinlock_t handle_type;
			spin_type() { pthread_spin_init(&handle, 0); }
			void lock() { pthread_spin_lock(&handle); }
			void unlock() { pthread_spin_unlock(&handle); }
		private:
			handle_type handle;
	};
	class unique_lock_type {
			mutex_type&_;
		public:
			unique_lock_type(mutex_type&m) : _(m) { m.lock(); }
			~unique_lock_type() { _.unlock(); }
			operator mutex_type&() { return _; }
	};
	class condvar_type {
		public:
			typedef pthread_cond_t handle_type;
			condvar_type() { pthread_cond_init(&handle, NULL); }
			void notify_one() { pthread_cond_signal(&handle); }
			void notify_all() { pthread_cond_broadcast(&handle); }
			void wait(unique_lock_type&ul) { pthread_cond_wait(&handle, ((mutex_type&)ul).native_handle()); }
		private:
			handle_type handle;
	};
#endif
};

#endif

#endif

