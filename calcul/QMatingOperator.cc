/*-----------------------------------------------------------------------------
                            Written for use in the CarthaGene application

 James C. Nelson 
 Dept. of Plant Pathology, Kansas State University, Manhattan, KS, USA

 Copyright 2006 James C. Nelson
 This file is distributed under the terms of the Q Public License version 1.0
 and may be freely included with CarthaGene code. Accompanying files are
 QMatingOperator.h, QPolynomial.h and .cc, QPolynomialMatrix.h and .cc,
 BJS_BS.h and .cc, and QArrayUtils.h
 
 $Id: QMatingOperator.cc, v 1.0 2006/02/05

Description: QMatingOperator encapsulates the updating of genotype and joint
genotype-X-crossover-number pmfs, for mating operations including selfing,
backcrossing, haploid-doubling, intercrossing, and any others that may be added.
*/

#include "QMatingOperator.h"
#include "QArrayUtils.h"
#include "QPolynomialMatrix.h"

QMatingOperator::QMatingOperator()
{}

// Destructor
QMatingOperator::~QMatingOperator()
{
	delete mOpM;
	QArrayUtils::Destroy2DIntArr(mXoverMaskM, mRows);
}

/**
*	Main constructor.
*/
QMatingOperator::QMatingOperator(char ch)
{
	int *constMaskArr = NULL;	// these two vars point at arrays of numerical constants
	double *constOperatorArr = NULL;
	int degree = kSingleMeiosisDegree;

	mOpSymbol = ch;
	mNumMeioses = 1;	// default for two of the four current ops
	mRows = NUM_DIPLOID_GENOTYPE_COMBOS; // default for all except intercrossing
	
	switch(ch)
	{
		case cBackcrossSymbol:
			constOperatorArr = (double *)BackcrossingOperatorM;
			constMaskArr = (int *)BCXoverM;
			break;
		case cSelfingSymbol:
			degree = kSelfingDegree;
			mNumMeioses = 2;
			constOperatorArr = (double *)SelfingOperatorM;
			constMaskArr = (int *)SelfXoverM;
			break;
		case cDoubledHaploidSymbol:
			constOperatorArr = (double *)HaploidDoublingOperatorM;
			constMaskArr = (int *)DHXoverM;
			break;
		case cIntercrossingSymbol:
			mNumMeioses = 2;
			mRows = NUM_PHASED_DIPLOID_GENOTYPES;
			constOperatorArr = (double *)IntercrossingOperatorM;
			constMaskArr = (int *)IXoverM;
			break;
	}
	mOpM = new QPolynomialMatrix
		(constOperatorArr, mRows, NUM_DIPLOID_GENOTYPE_COMBOS, degree);
	mXoverMaskM = QArrayUtils::Create2DIntArr(constMaskArr, mRows, NUM_DIPLOID_GENOTYPE_COMBOS);
}

/**
*	Does all the operations required of a QMatingOperator at a given generation
*/
QPolynomialMatrix *QMatingOperator::UpdateAllProbs
	(double *singleLocusGenoProbs, QPolynomial *RIM_factorP, QPolynomialMatrix *genoProbsV,
	QPolynomialMatrix *xProbsM, QPolynomialMatrix *newXProbsM)
{
	UpdateSingleLocusProbs(singleLocusGenoProbs);
	UpdateRIMs(RIM_factorP, genoProbsV);
	UpdateGxC_pmf(xProbsM, newXProbsM);
	return UpdateTwoLocusProbs(genoProbsV);
}

/**
*	Update single-locus probabilities according to the mating-design string op for this generation
*	Developer: add a case to this method if another mating operation is added.
* Could convert this to matrix ops for greater simplicity, but at present have no C++ matrix class
**/
void QMatingOperator::UpdateSingleLocusProbs(double *probs)
{
	double halfProb_Aa = 0.5 * probs[Aa];
	
	switch(mOpSymbol)
	{
		case cBackcrossSymbol:
			probs[aa] += halfProb_Aa;
			probs[Aa] = halfProb_Aa + probs[AA];
			probs[AA] = 0.0;
			break;
		case cDoubledHaploidSymbol:
			probs[aa] += halfProb_Aa;
			probs[Aa] = 0.0;
			probs[AA] += halfProb_Aa;
			break;
		case cSelfingSymbol:
			probs[aa] += 0.5 * halfProb_Aa;
			probs[AA] += 0.5 * halfProb_Aa;
			probs[Aa] -= halfProb_Aa;
			break;
		case cIntercrossingSymbol:
			double
				prob_a = probs[aa] + halfProb_Aa,
				prob_A = probs[AA] + halfProb_Aa;
				
			probs[aa] = prob_a * prob_a;
			probs[AA] = prob_A * prob_A;
			probs[Aa] = 2 * prob_a * prob_A;
			break;
	}
}

// update RIM proportion by the proportion of double hets in the parent generation
void QMatingOperator::UpdateRIMs(QPolynomial *RIM_factorP, QPolynomialMatrix *genoProbsV)
{
	QPolynomial *curRIM_P = new QPolynomial(0);

	curRIM_P->plusEquals(genoProbsV->extractPoly(pAaBb, 0));
	curRIM_P->plusEquals(genoProbsV->extractPoly(paABb, 0));
	
	// update proportion of total parental meioses that were informative
	curRIM_P->timesScalarEquals(mNumMeioses);// scale RIM contribution by the meiosis number for this mating step
	RIM_factorP->plusEquals(curRIM_P);			// accumulate this generation's RIM contribution in the total
	delete curRIM_P;	
}

// Accumulate joint probabilities for progeny genotype and xover number
void QMatingOperator::UpdateGxC_pmf(QPolynomialMatrix *xProbsM, QPolynomialMatrix *newXProbsM)
{
	newXProbsM->timesScalarEquals(0.0); // zero this for prob accumulation
	if (mOpSymbol == cIntercrossingSymbol)	// not very OOPish but it works for now CN 2.6.06
		UpdateIntercrossGxC_pmf(xProbsM, newXProbsM);
	else
		UpdateBSD_GxC_pmf(xProbsM, newXProbsM);
}

// Update genotype probabilities for current generation
QPolynomialMatrix *QMatingOperator::UpdateTwoLocusProbs(QPolynomialMatrix *genoProbsV)
{
	QPolynomialMatrix *tempGenoProbsV = mOpM->maskedTimes(genoProbsV, 0L, 0); // unmasked matrix multiplication
	// tempGenoProbsV is a 4 x 1 vector
	if (mOpSymbol == cIntercrossingSymbol)	// not very OOPish but it works for now CN 2.6.06
		tempGenoProbsV = CombineGametes(tempGenoProbsV);// take outer product with transpose & unwind
	delete genoProbsV;
	return tempGenoProbsV;
}

/**
*	Updates the joint genotype-by-crossover-number pmf for backcrossing, selfing, and haploid-doubling
* mating operations via masked multiply and accumulate with offset.
*/
void QMatingOperator::UpdateBSD_GxC_pmf(QPolynomialMatrix *xProbsM, QPolynomialMatrix *newXProbsM)
{
	QPolynomialMatrix *tempXProbsM;
	
  for (int xoverNum = 0; xoverNum <= mNumMeioses; xoverNum++)
  {
  	tempXProbsM = mOpM->maskedTimes(xProbsM, mXoverMaskM, xoverNum); // sum the probs for this xover num
    newXProbsM->plusEqualsWithRotate(tempXProbsM, xoverNum);	// add them at xoverNum columns right offset to accumulator
    delete tempXProbsM;		// prevent memory leak by freeing the just-created matrix
	}
}

/**
*	Given a 4 x 1 vector of gamete probabilities, postmultiplies it by its transpose and unwinds
*	the resulting 4 x 4 matrix into a 10 x 1 vector of genotype probs
*/
QPolynomialMatrix *QMatingOperator::CombineGametes(QPolynomialMatrix *gameteProbsV)
{
	bool allZero;	// unchecked here, since we know the operands are nonzero
	
  QPolynomialMatrix
  	*gameteCrossProbsM, // for 4 x 4 prob matrix containing outer product of gamete vector
  	*newTwoLocusProbsV;	// for 10 x 1 prob vector into which the above matrix is unwound
  
  gameteCrossProbsM = gameteProbsV->computeOuterColumnProduct(0, gameteProbsV, 0, allZero);
  newTwoLocusProbsV = new QPolynomialMatrix(NUM_DIPLOID_GENOTYPE_COMBOS, kSingleColumn, kZeroDegree);
  UnwindPunnett(gameteCrossProbsM, newTwoLocusProbsV, 0);//  unwind 4 x 4 matrix to 10 x 1 vector
  delete gameteProbsV;	// this 4 x 1 vector's not needed now
  delete gameteCrossProbsM;	// nor is this 4 x 4 matrix
  return newTwoLocusProbsV;
}

/**
*	Given a 4 x 4 matrix of genotype probabilities, unwind it into a 10 x 1 by pooling symmetrical off-diagonal classes.
*/
void QMatingOperator::UnwindPunnett(QPolynomialMatrix *gameteCrossPM,
																		QPolynomialMatrix *newXProbsM, int destCol)
{
	for (int i = 0; i < NUM_PHASED_DIPLOID_GENOTYPES; i++)
		for (int j = 0; j < NUM_PHASED_DIPLOID_GENOTYPES; j++)
	{
		int destRow = PunnettExpanderArr[i][j];
    newXProbsM->extractPoly(destRow, destCol)->plusEquals(gameteCrossPM->extractPoly(i, j));
 	}
  newXProbsM->updateMaxDegree();	// easy to have forgotten to do this -- it's not enforced automatically.
}

/**
*	Carry out the intermating step on the joint pmf of genotype and crossovers. This requires generating all
*	4 x 1 vectors representing combinations of source-genotype crossover numbers and new-crossover numbers
* for the component gametes; then obtaining the cross products of all these vectors, unwinding them into
*	10x1 vectors, and accumulating these in the joint pmf for the progeny generation.
*/
void QMatingOperator::UpdateIntercrossGxC_pmf(QPolynomialMatrix *xProbsM, QPolynomialMatrix *newXProbsM)
{
	int
		xNum = xProbsM->getNumColumns(),	// width of the joint pmf
		col_div, col_mod, row_div, row_mod,	// used to map from cross-product index to pmf column
		dest_pmf_col,										// column of accumulation in the new pmf
		numGameteXClasses = 2 *	xNum;		// for each source-genotype xover num, a gamete can have 0 or 1 new xover.
	
	bool allZero;
	
	QPolynomialMatrix
		*tempXProbsM,
		*gameteXM = new QPolynomialMatrix(NUM_PHASED_DIPLOID_GENOTYPES, numGameteXClasses, kZeroDegree),
		*outerPM;	// for the 4x4 outer product of pairs of gamete prob vectors
	
  for (int xoverNum = 0; xoverNum <= 1; xoverNum++) // a gamete can have only 0 or 1 crossover
  {
  	tempXProbsM = mOpM->maskedTimes(xProbsM, mXoverMaskM, xoverNum); // sum the probs for this xover num
    gameteXM->plusEqualsWithRotate(tempXProbsM, xoverNum * xNum);	// add submatrix, offset to right
    delete tempXProbsM;		// free the just-created matrix
	}
	// Next distribute the gamete X gamete probabilities to the new xover X genotype pmf
	for (int col = 0; col < numGameteXClasses; col++)
	{
    col_div = col/xNum;	// to see how this addressing works, view the note at end of this method.
    col_mod = col % xNum;
    
    for (int row = 0; row <= col; row++)
    {
      row_div = row/xNum;
      row_mod = row % xNum;
      outerPM = gameteXM->computeOuterColumnProduct(row, gameteXM, col, allZero);
      if (allZero)	// at least one of the two columns of polynomials had all zero coefficients. This will happen
      // in all but the final generation, since the joint pmf columns are populated from L to R by generation.
      	continue;		// the final generation, since the joint pmf columns are populated from L to R by generation.
 			dest_pmf_col = row_mod + row_div + col_div;
      UnwindPunnett(outerPM, newXProbsM, dest_pmf_col);
      if (row < col) // write symmetrical probability too
    	{
        dest_pmf_col = col_mod + row_div + col_div;
      	UnwindPunnett(outerPM, newXProbsM, dest_pmf_col);
 			}
 			delete outerPM;
 		}	// end loop over rows
	}	// end loop over cols
	delete gameteXM;
}

// The gameteXM in the preceding method is an array of 4 x 1 vectors, placed side by side. We need
// to compute all vector-by-vector outer products, in order to account for all gamete combinations.
// The tables below are to help visualize this multiplication scheme. In both of these tables, the
// top two rows and left two columns are identical and represent labels. The first (row or column)
// of each pair gives the number of crossovers in the history of the genotype originating the
// gamete, and the second, the number of crossovers involved in the formation of that gamete. These
// 6 x 6 tables represent a generation at which a genotype could have had 0, 1, or 2 crossovers in its
// history, for example the F2. For a mating design extending for more generations, they are bigger
// (always of dimension 2 * (mMaxPossibleXovers + 1)).
// The left table simply shows the total number of new crossovers contributed by the two gametes. The
// right table's entries give the column, in the joint 2-locus-genotype / crossover pmf, in which the
// genotype probabilities represented by the 4x4 matrix product of the gamete probability vectors from
// the corresponding row and column are to be accumulated. For example: if a source genotype class with
// 2 crossovers in its history generates a gamete with 1 crossover, which then mates with a 0-crossover
// gamete generated by a 1-crossover source genotype, the 10x1 probability vector that we get by unwinding
// the 4x4 matrix will be added to both column 3 (= 2 + 1) and column 2 (= 1 + 1) of the pmf. The
// combinations on the main diagonal get only one addition. Thus we could treat an F2 as a random
// intercross between F1 gametes, and the resulting pmf would be identical to that of an F2
// Note that in early generations of a mating series, polynomials in right-hand columns in both L and R
// halves of the RH table will have all zeroes, since they represent crossover numbers that still have
// zero probability. The addressing with div and mod still works as it should.

// 			0	1	2	0	1	2					0	1	2	0	1	2
// 			0	0	0	1	1	1					0	0	0	1	1	1
// ________________     _______________
// 0	0	0	0	0	1	1	1			0	0	0	0	0	1	1	1
// 1	0	0	0	0	1	1	1			1	0	1	1	1	2	2	2
// 2	0	0	0	0	1	1	1			2	0	2	2	2	3	3	3
// 0	1	1	1	1	2	2	2			0	1	1	1	1	2	2	2
// 1	1	1	1	1	2	2	2			1	1	2	2	2	3	3	3
// 2	1	1	1	1	2	2	2			2	1	3	3	3	4	4	4
