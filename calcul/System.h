//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: System.h,v 1.22.2.1 2012-08-02 15:43:55 dleroux Exp $
//
// Description : Prototype de la classe Chronometre.
//
// Divers :
//----------------------------------------------------------------------------

#ifndef _SYSTEM_H
#define _SYSTEM_H

#include <stdio.h>

extern char bouf[2048];
extern char boufi[2048];
extern FILE *Fout;

#ifdef tcl
#include "../Tcl_system.h"
#endif

#if 0
#ifdef ihm
#include "tcl.h"
/** Concat�nation � la fin du Widget */
extern Tcl_Interp *linterp;
#define pout(bouf) do { sprintf(boufi, "puts -nonewline {%s}; flush stdout", bouf); Tcl_Eval(linterp, boufi); if (Fout != NULL) fprintf(Fout, "%s", bouf); } while(0)
#define perr(bouf) do { sprintf(boufi, "puts -nonewline stderr {%s}", bouf); Tcl_Eval(linterp, boufi); if (Fout != NULL) fprintf(Fout, "%s", bouf); } while(0)
#else
#ifdef tcl
#include "tcl.h"
/** Exploitation de la sortie Tcl */
#define pout(bouf) do { Tcl_Write(fout,bouf,strlen(bouf)); Tcl_Flush(fout); if (Fout != NULL) fprintf(Fout, "%s", bouf); } while(0)
#define perr(bouf) do { Tcl_Write(ferr,bouf,strlen(bouf)); if (Fout != NULL) fprintf(Fout, "%s", bouf); } while(0)
#else
/** C*/
#define pout(bouf) do { fputs(bouf, stdout); if (Fout != NULL) fprintf(Fout, "%s", bouf); } while(0)
#define perr(bouf) do { fputs(bouf, stderr); if (Fout != NULL) fprintf(Fout, "%s", bouf); } while(0)
#endif
#endif

#ifndef NDEBUG
#define POUTF(_fmt, ...) do { sprintf(bouf, _fmt ,##__VA_ARGS__); pout(bouf); } while(0)
#else
#define POUTF(_fmt, ...)
#endif

#ifdef tcl
  /** Exploitation de la sortie Tcl */
extern Tcl_Channel fout;
extern Tcl_Channel ferr;
#endif
#endif

#ifdef __WIN32__
#include <time.h>
#else
#include <stdlib.h>  // drand48() pour Solaris et Linux
#include <sys/times.h>
#endif

#define randomax(x) ((int) ((x)*drand48()))
#define Echange(x,y) { int _temp; _temp = *x; *x=*y; *y=_temp;}

#define MOD(i,n)    ((i) % (n) >= 0 ? (i) % (n) : (i) % (n) + (n))
#define RREAL drand48()

#ifdef __WIN32__
#ifdef __cplusplus
extern "C" {
#endif
   double drand48( void );
#ifdef __cplusplus
}
#endif
#endif

/* license */
int CheckDoor();

/** Indicateur du temps �coul� depuis le lancement de la recherche.
  */
class Chronometre {
 public:

  /** Le start du chronom�tre.
      Remise � z�ro du compteur qui enregistre l'heure courante.
    */
  void Init(void);

  /** Donne le temps �coul� depuis l'initialisation de l'objet.
    */
  double Read(void) const;

 private:
#ifdef __WIN32__
  time_t InitialTime;
#else
  /** Structure tms o� est m�moris�e l'heure au moment du lancement du
      chronom�tre.
      */
  tms InitialTime;
#endif
};

char *Cart_basename( const char*  name );

inline int fact(int n) 
{
  int i,f;

  for (f = i = 1; i <= n; ++i) f*= i; 

  return(f);
};

inline long double dfact(int n) 
{
  long double i,f;

  for (f = i = 1; i <= n; ++i) f*= i; 

  return(f);
};

inline long double dfact2(int first, int last) 
{
  long double f = 1.;
  long double i;

  for (i = first; i <= last; ++i) f*= i; 

  return(f);
};


#endif /* _SYSTEM_H */
