//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Carte.h,v 1.34 2008-05-09 11:32:04 tschiex Exp $
//
// Description : Prototype de la classe Carte.
// Divers :
//-----------------------------------------------------------------------------

#ifndef _CARTE_H
#define _CARTE_H
#define UnConvLevel 1e30

#include "CGtypes.h"
#include "System.h"

class CartaGene;

/** La classe des cartes. */
class Carte {
 public:
  
  /** Acc�s � l''ensemble des informations du syst�me. */ 
  CartaGene *Cartage;

  /** Carte vierge ou convergee */
  double Converged;
  
  /** position de la carte dans le tas */
  int Id;

  /** le nombre de marqueurs. */ 
  int NbMarqueur;

  /** Vecteur d'ordre de rangement des marqueurs. */
  intPtr ordre;
  
  /** Vecteur de taux de recombinaisons. */
  doublePtr tr;

  /** Taux de retention des segments pour les hybrides irradies
      dans le modele a un parametre
  */
  double ret;

  /** Cout de la carte �valu� par EM. */
  double coutEM;

  /** Constructeur */
  Carte();

  /** Constructeur par copie.
      Il s'agit d'une copie en profondeur.
      @param Source la carte � dupliquer.
  */
  Carte(const Carte &Source);


  /** Constructeur simple.
      @param cartage acc�s aux informations du syst�me
      @param n nombre de marqueurs.
  */
  Carte(CartaGene *cartage, int n);
  
  /** Constructeur
      @param cartage acc�s aux informations du syst�me
      @param nm le nombre de cartes
      @param imarq le vecteur de num�ro de marqueurs
    */
  Carte(CartaGene *cartage, int nm, intPtr imarq);

  /** Destructeur. */
  ~Carte();

  /** Force a modified map to be considered as non estimated */
  void UnConverge(void);

  /** Canonify a MAP.
      Order is lexicographically minimized.
  */
  void Canonify(void);

  /** Canonify a MAP(Merged by order).
  */
  void CanonifyMor(void);

  /** Recopie les donnees de la carte courante sur Dest.
      @param Dest une carte
    */
  void CopyFMap(Carte *Dest) const;
  
  /** Recopie certaines donn�es de la carte courante sur Dest.
      Seulement ordre et coutEM sont recopi�s.  Le tableau tr est
      initialis� � 0.05, ret a 0.3 et la carte marquee comme non estimee.
      @param Dest une carte
  */
  void CopyMap(Carte *Dest) const;

  /** Compare 2 map orders.
    */
  int SameMaps(const Carte *map2) const;

  /** Affichage des indices des indices, entre autre
      @param taboref tableau de correspondance de l'ordre de r�f�rence
      @param refordre tableau de l'ordre de ref
      @param nbm nombre minimum � partir duquel on consid�re un interval
      @param blank indice au sein de l'interval ou car.spec.
      @param comp compressio de l'intervalle ou pas
   */
  void PrintO(int * taboref, int * refordre, int nbm, int blank, int comp) const;

  /** Affichage des indices des marqueurs(verticalement).
    */
  void PrintIVMap(void) const;

  /** Construction de cartes � pas trop bete � - Nearest Neighbor.
      @param root 
    */
  void BuildNearestNeighborMap(int root);
  
  /** Pour une nouvelle carte pas trop b�te.
    */
  void BuildNiceMap(void);
  void BuildNiceMapMultiFragment(void);

  /** Construction de cartes � pas trop bete � - Nearest Neighbor.
      Crit�re = LOD
      @param root 
    */
  void BuildNearestNeighborMapL(int root);
  
  /** Pour une nouvelle carte pas trop b�te.
      Crit�re = LOD
    */
  void BuildNiceMapL(void);
  void BuildNiceMapLMultiFragment(void);

  /** Pour une nouvelle carte pas trop b�te.
    */
  void BuildNewMap(void);

  /** Pour une nouvelle carte pas trop b�te.
      Crit�re = LOD
    */
  void BuildNewMapL(void);

  /** Construction de cartes �pas trop bete� - par chemin Eulerien sur un MST.
    */
  void BuildMSTMap(int root);

  /** Construction de cartes �pas trop bete� - par chemin Eulerien sur un MST.
    */
  void BuildMSTMapL(int root);

  /** Construit une carte al�atoire.
    */
  void BuildRandomMap(void);

  /** R�initialise les structures de la carte courante,
      avec le contenu de la carte Source. Sauf pour tr qui est non initialis�.
      @param Source La carte a recopier.
  */
  void GetMap(const Carte* Source);

  /** Applique une insertion de e1 a la place de e2 (repousse e2)
      @param e1 position du marqueur a reinserer
      @param e2 nouvelle position du marqueur
  */
  void ApplyInsertion(int e1, int e2);

  /** Applique un 2 changement.
      Suppose e1 < e2. Marque la carte comme non estimee
      @param e1 position du premier marqueur
      @param e2 position du second marqueur
  */
  void Apply2Change(int e1, int e2);

  /** Applique un 3 changement.
      Marque la carte comme non estimee
      @param e1 position du premier marqueur
      @param e2 position du second marqueur
      @param wordre vecteur de travail
    */
  void Apply3Change(int e1, int e2, int wordre[]);

  /** Post traitement polish
      Pour chaque marqueur, compare la probabilit� courante avec la
      probabilit� obtenue en d�pla�ant le marqueur vers un autre
      intervalle. Met � jour avec le meilleur �l�ment (s'il y en a un)
      et l'insert dans le tas. Affiche la matrice des diff�rences (en mode verbose).
  */
  void Polish();

  
  /** Post traitement polishtest avec marqueur donne en ligne de commande
      Pour chaque marqueur, compare la probabilit� courante avec la
      probabilit� obtenue en d�pla�ant le marqueur vers un autre
      intervalle. NE Met PAS � jour avec le meilleur �l�ment (s'il y en a un)
      et NE l'insere pas dans le tas.
      Affiche la matrice des diff�rences (en mode verbose).
      @param nbmi nombre de marqueurs dans la carte
  */
  void Polishtest(int nbmi);

  /** Post traitement flips
      @param n taille de lafen�tre 
      @param thres le seuil
      @param iter flag pour it�ratif ou non
  */
  void Flips(int n, double thres, int iter);

  /** Pour les tests : Construit une bonne carte pour des tests al�atoires.
      Se base sur le nom des marqueurs nomm�s M1, M2, M3, ...
      lorsque la carte provient d'un g�n�rateur.
      @return 1 si les donn�es ne sont apparamment pas g�n�r�es
  */
  int BuildGoodMap(void);

  /** Pour les tests : v�rifie que la carte est correcte
      Se base sur le nom des marqueurs nomm�s M1, M2, M3, ...
      lorsque la carte provient d'un g�n�rateur.
      @return 0 = mauvaise, 1 bonne carte.
  */
  int GoodMap(void) const;

 private:

  /** Permet de g�n�rer une carte diff�rente � chaque fois.
    */
  int Vertex;

};

#endif /* _CARTE_H */
