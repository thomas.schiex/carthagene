//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Constraints.h,v 1.5 2002-10-08 09:43:41 tschiex Exp $
//
// Description : Prototype de la classe Constraint
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _CONSTRAINT_H
#define _CONSTRAINT_H

class Carte;

class Constraint;

/** La classe des contraintes */
class Constraint {
 public:

  /** Le premier marqueur dont on specifie l'ordre. */
  int M1;
  /** Le deuxi�me marqueur dont on specifie l'ordre. */
  int M2;
  /** Le troisi�me marqueur dont on specifie l'ordre. */
  int M3;

  /** La p�nalit� associ�e. */
  double Penalty;

  /** La contrainte suivante. */
  Constraint *Next;

  /** Constructeurs. */
  Constraint();
  
  /** Constructeur avec initialisation.
      @param
      @param
      @param
    */
  Constraint(int m1, int m2, int m3, double pen);

  /** Destructeur.
    */
  ~Constraint();

  /** V�rifie la contrainte dans la carte.
      @param map la carte
      @return la p�nalit�
    */
  double Check(Carte *map);
  
  /** V�rifie les contraintes dans la carte.
      @param map la carte
      @return la somme des p�nalit�s
    */
  double CheckAll(Carte *map);
  
  /** V�rifie la contrainte dans la carte.
      @param map la carte
      @return si la contrainte est viol�e
    */
  int CheckI(Carte *map);
  
  /** V�rifie les contraintes dans la carte.
      @param map la carte
      @return le nombre de contraintes viol�es.
    */
  double CheckIAll(Carte *map);
  
};

#endif /* _CONSTRAINT_H */
