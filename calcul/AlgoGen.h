//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: AlgoGen.h,v 1.11 2002-10-08 09:43:41 tschiex Exp $
//
// Description : Prototype de la classe AlgoGen (Algorithmes Genetiques).
// Divers :
//-----------------------------------------------------------------------------


#ifndef _ALGOGEN_H
#define _ALGOGEN_H

#include "CartaGene.h"

#include <stdio.h>
#include <math.h> 

typedef void SystemFILE;

typedef Carte Data;

typedef struct
{
  double r_fitness ;    // Raw fitness 
  double s_fitness ;    // Scaled fitness 
  int evaluated ;       // True if chromosome already evaluated 
  int newc ;            // 1 if new chromosome, 0 if copy of old one 
  Data *data;
} Chromosome;


class Algogen {
 public:

  /** Acc�s � l''ensemble des informations du syst�me. */ 
  CartaGene *Cartage;
  
  /** Constructeur.
     @param cartage acc�s aux informations du syst�me
  */
  Algogen(CartaGene *cartage);

  /** Destructeur.
  */
  ~Algogen();

  /** Lancement de Algorithme genetique.
      @param nb_gen :Nombre de generations (integer)
      @param nb_elements :Nombre d'elements dans la population (integer)
      @param selection_number:# Selection type :(integer)
                              #	0 -> roulette wheel
                              #	1 -> stochastic remainder without replacement
      @param pcross: Probabilities of crossover and mutation (float)
      @param pmut: Probabilities of crossover and mutation (float)
      @param evolutive_fitness:Evolutive Fitness (integer) 
              (the fitness of the same element changes with the generation number)
 
  */
  void Run( int nb_gens = 10,
	    int nb_elements = 10,
	    int selection_number = 0,
	    float pcross = 0.3,
	    float pmut = 0.5,
	    int evolutive_fitness = 1);
  
  /*------------------------------------------*/
  /* INITIALISATION DE LA POPULATION          */
  /*------------------------------------------*/

  void InitPop(int nb_elements,Chromosome **population,Chromosome **new_population);

    void InitPopFromTas(int nb_elements,Chromosome **population,Chromosome **new_population);
  
  /*------------------------------------------*/
  /* DECLARATION POUR LE GENERATEUR ALEATOIRE */
  /*------------------------------------------*/
  
  /** Returns number with uniform distribution over [0,1] */
  double RandUniform(void);
  
  /** Returns 1 with probability prob */
  int Flip(double);
  
  /** Returns number with gaussian distribution
      with 0 mean and 1 as standard deviation */
  double RandGauss(void);
  
  /** returns number with M shape distribution */
  double RandM(void);
  
  /*-------------------------------------------*/
  /* DECLARATIONS POUR LE MODULE local.h       */
  /*-------------------------------------------*/
  
  /** Copy a Data structure into another one. 
      Mandatory because we don't want to copy
      pointers but values */
  void LocalCopyData(Data *out, Data *in);
  
  /** Allocate mem for one Data structure element
      We must pass a pointer on the pointer to
      be able to initialize the pointer value */
  void LocalAllocMem(Data **data);

  /** Free mem for one Data structure element */
  void LocalFreeMem(Data **data);

  /** Initialization of the population of chromosomes */
  void LocalInitData(Data *data);

  int deja_in_d1(Data *d1,int i1,int i2,int M);
  Data *OrderCrossover(Data *d1, Data *d2, int i1,int i2);

  /** Crossover operator. Takes two chromosomes (the two
      parents) as input and replaces them by the children */
  void LocalCrossover(Data *data1, Data *data2) ;

  /** Mutation operator. Takes one chromosome and mutates it */
  void LocalMutation(Data *data);

  /** Evaluation of the fitness of one data structure.
      You need to pass generation number for evolutive fitness */
  double LocalDataEval(Data *data);

/*-------------------------------------------*/
/* DECLARATIONS POUR LE MODULE eval.h       */
/*-------------------------------------------*/

  /** Evaluate each element of the population which has not been evaluated yet */
  void EvalPopulation(int nb_elements,
		      Chromosome *population,
		      Chromosome **best_chrom) ;

/*-------------------------------------------*/
/* DECLARATIONS POUR LE MODULE crosseval.h       */
/*-------------------------------------------*/

  void ChooseCouple(int *n1,int *n2,int nb_elements,
		    Chromosome *population);
  /** Do the crossovers */
  void Crosseval(int nb_elements,
		 double pcross,
		 Chromosome *population);

/*-------------------------------------------*/
/* DECLARATIONS POUR LE MODULE muteval.h       */
/*-------------------------------------------*/
 
  /** Free memory used for this unit */
  void MutevalFreeMem() ;

  void ChooseChrom(int *n,int nb_elements,Chromosome *population);

  /** Do the mutations */
  void Muteval(int nb_elements,
	       double pmut,
	       Chromosome *best_chrom,
	       Chromosome *population);
  
/*-------------------------------------------*/
/* DECLARATIONS POUR LE MODULE selection.h       */
/*-------------------------------------------*/

  void SelectionFreeMem() ;
  /** Procedure used for selection process */
  void Selection(int nb_elements,
		 int evolutive_fitness,
		 int selection_number,
		 Chromosome **population,
		 Chromosome **new_population);


 private:

  int first;

  int iset;
  double gset;

  int rndcalcflag;
  double rndx2;

  /** Du module selection.c */
  double *tab;
  double *fraction;
  int    *choices;

  /** Du module muteval.c */
  double *tabmut;

  /** Indicateur pour utilisation heuristique du MST */
  int IndMST;
  int chosen[500];
};

#endif /* _ALGOGEN_H */
