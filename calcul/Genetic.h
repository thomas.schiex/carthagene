//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Genetic.h,v 1.4 2002-10-08 09:43:41 tschiex Exp $
//
// Description : Prototype de diverses fonctions de calculs de vraisemblance.
// Divers :
//-----------------------------------------------------------------------------

#ifndef _GENETIC_H
#define _GENETIC_H

///Conversion des Ray en prob. de cassure
double Ray2Theta(double ray);

///Conversion des prob. de cassure en Ray
double Theta2Ray(double theta);

///Calcule une distance � la Haldane
double Haldane(double theta);

///fonction inverse (de la distance � th�ta)
double HaldaneInv(double dist);

///
double Kosambi(double theta);

///
double KosambiInv(double dist);

#endif /* _GENETIC_H */
