//#pragma once	// otherwise getting message that class is redefined
#ifndef __QPOLYNOMIAL__
#define __QPOLYNOMIAL__
/**
 * Class that represents polynomials as arrays of real coefficients.
 * The lowest (0) -degree term is at index 0 and the
 * higher-degree terms follow to the right. No power is left out
 * even if its coefficient is 0.
 * This class contains only the methods used in CG; others could be added.
 * Author J. C. Nelson
 * adapted from a Java class by Roby Joehanes
 */
 
 const bool
 	WITH_NEWLINE = true,
 	WITHOUT_NEWLINE = false;
 	
class QPolynomial
{
 protected:

	double *mCoefficients;
	int mDegree;

 public:
	
	QPolynomial();
	
	QPolynomial(int degree);

	QPolynomial(double *coeffs, int degree);
	
	~QPolynomial();

	double *getCoefficients();
	void setCoefficient(int power, double val);

	int getDegree();

	/*
	 * Add another polynomial to this polynomial
	 */
	void plusEquals(QPolynomial *poly);

	/*
	 * Multiply this polynomial with a constant, in place
	 */
	void timesScalarEquals(double c);

	/*
	 * Multiply this polynomial with another polynomial and store the result into a new instance of QPolynomial
	 */
	QPolynomial *times(QPolynomial *poly);

	/*
	 * Multiply this polynomial with another polynomial, in place
	 */
	void timesEquals(QPolynomial *poly);

	/*
	 * Evaluate this polynomial at x = x0
	 */
	double evaluate(double x0);
	
	/*
	 * Evaluate this polynomial based on a precomputed vector of powers of the variable.
	 * To be used in QPolynomialMatrix. CN 10.31.05
	 */
	double evaluate(double * powersOfX);

	/*
	 * Clone this polynomial
	 */
	QPolynomial *clone();
	double* cloneCoefficients();
	
	bool hasAllZeroCoeffs();
	
	void printPoly(bool withNewline = false);	// for debugging

};

#endif

