//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Damien Leroux, Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// CartaGene.cc,v 1.106 2007/06/11 15:05:52 degivry Exp
//
// Description : Structure de données générique pour fournir à la fois les services d'un tableau et d'une table de hashage (voir MarkerList pour un exemple d'utilisation).
// Divers :
//-----------------------------------------------------------------------------

#ifndef _CARTHAGENE_KEYLIST_H_
#define _CARTHAGENE_KEYLIST_H_

/* TODO : updgrade all the hash_map-related stuff to unordered_map whenever the C++0x standard comes to a final release */

#include <ext/hash_map>
#ifndef WIN32
#include <backward/hash_fun.h>
#endif
#include <algorithm>
#include <vector>
#include <typeinfo>	/* for std::bad_cast */

using __gnu_cxx::hash_map;
using __gnu_cxx::hash;
using std::vector;


/* Provide a generic ordered+hashed map container with O(1) complexity for most operations :
 * - add an element by key (returns new element to be initialized)
 * - find an element by key
 * - find an element by index
 * - find key by index
 * - find index by key
 * - find key by element
 * - find index by element
 */

template <typename K, class V>
class KeyList {
    private:
        struct Item : public V {
            KeyList<K, V>* owner;
            unsigned int _kl_index;
            K _kl_key;
            Item(KeyList<K,V>*o, int i, K k) : V(), owner(o), _kl_index(i), _kl_key(k) {}
        };
        typedef hash_map<K, Item*> ItemHash;
        typedef vector<Item*> ItemOrder;

        ItemHash byKey;
        ItemOrder byIndex;
        /* fugly gcc quirk : it seems I can't directly use ItemHash::iterator */
        typedef __gnu_cxx::_Hashtable_iterator<std::pair<K const, Item*>, K, __gnu_cxx::hash<K>, std::_Select1st<std::pair<K const, Item*> >, std::equal_to<K>, std::allocator<Item*> > HashIter;
        typedef __gnu_cxx::_Hashtable_const_iterator<std::pair<K const, Item*>, K, __gnu_cxx::hash<K>, std::_Select1st<std::pair<K const, Item*> >, std::equal_to<K>, std::allocator<Item*> > HashConstIter;

        static void freeItem(Item*i) { if(i) { delete i; } }

        Item* hashfind(K& k) {
            HashIter i = byKey.find(k);
            Item* ret = i==byKey.end() ? NULL : (*i).second;
            /*std::cerr << "hashfind("<<k<<")"<<" => "<<ret<<std::endl;*/
            return ret;
        }
        Item* hashfind(const K& k) const {
            HashConstIter i = byKey.find(k);
            Item* ret = i==byKey.end() ? NULL : (*i).second;
            /*std::cerr << "hashfind[const]("<<k<<")"<<" => "<<ret<<std::endl;*/
            return ret;
        }

    public:

        class iterator {
            const KeyList<K,V>* kl;
            int i;
            iterator(const KeyList<K,V>*_, int z) : kl(_), i(z) {}
            friend class KeyList<K,V>;
        public:
            iterator(const iterator&_) : kl(_.kl), i(_.i) {}
            iterator& operator++() { ++i; return *this; }
            iterator& operator++(int _) { ++i; return *this; }
            V* operator*() const { return kl->get(i); }
            bool operator==(const iterator&_) const { return i==_.i && kl==_.kl; }
            bool operator!=(const iterator&_) const {
                return i!=_.i || kl!=_.kl;
            }
            int index() const { return i; }
            K key() const { return kl->keyOf(i); }
        };

        KeyList()
            : byKey(), byIndex()
        {}

        ~KeyList() {
            /*std::for_each(byIndex.begin(), byIndex.end(), freeItem);*/
            /*byIndex.clear();*/
            /*byKey.clear();*/
            clear();
        }

        void clear() {
            std::for_each(byIndex.begin(), byIndex.end(), freeItem);
            byIndex.clear();
            byKey.clear();
        }

        V* add(K key) {
            Item* item = hashfind(key);
            if(!item) {
                item = new Item(this, byIndex.size(), key);
                byKey[key] = item;
                byIndex.push_back(item);
                //std::cout << "added " << key << " as #" << (byIndex.size()-1) << std::endl;
            } else {
                //std::cout << key << " already exists as #" << item->_kl_index << std::endl;
            }
            return static_cast<V*>(item);
        }

        V* get(K key) const {
            Item* i = hashfind(key);
            return i?static_cast<V*>(i):NULL;
        }

        V* get(int i) const {
            return static_cast<V*>(byIndex[i]);
        }

        V& operator[](int i) const { return *get(i); }
        V& operator[](K key) const { return *get(key); }

        int indexOf(V*v) const {
            try {
                Item* i = dynamic_cast<Item*>(v);
                return i->_kl_index;
            } catch(std::bad_cast e) {
                return -1;
            }
        }

        int indexOf(K key) const {
            Item* i = hashfind(key);
            /*std::cout << "indexOf("<<key<<") => "<<i<<" k="<<i->_kl_key<<" i="<<i->_kl_index<<std::endl;*/
            return i?i->_kl_index:-1;
        }

        K* keyOf(V*v) const {
            try {
                Item* i = dynamic_cast<Item*>(v);
                return &i->_kl_key;
            } catch(std::bad_cast e) {
                return NULL;
            }
        }

        K keyOf(int i) const {
            return byIndex[i]->_kl_key;
        }

        bool exists(K key) const {
            return byKey.find(key)!=byKey.end();
        }

        bool exists(int i) const {
            return i>=0 && i<byIndex.size();
        }

        bool exists(V* v) const {
            try {
                Item* i = dynamic_cast<Item*>(v);
                return i->owner==this && i->_kl_index<=byIndex.size();
            } catch(std::bad_cast e) {
                return -1;
            }
        }

        iterator begin() {
            return iterator(this, 0);
        }

        iterator end() const {
            return iterator(this, byIndex.size());
        }

        size_t size() const { return byIndex.size(); }
};


#endif

