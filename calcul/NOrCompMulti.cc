//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: NOrCompMulti.cc,v 1.3 2005-10-10 12:44:46 degivry Exp $
//
// Description :  NorCompMulti class
// Divers :  Computation of the number of orders at a given breakpoint distance
// generalized to multiple chromosome 
//-----------------------------------------------------------------------------

#include "NOrCompMulti.h"

NOrCompMulti::NOrCompMulti(int k, intPtr N, int n)
{

  locdim = N;
  chromdim = n;
  bpdim = k;
  
  int h,j,l,m,v;
  
  // number of loci
  
  glocdim = N[0];
  for (h = 1; h < n; h++)
    glocdim += N[h];

  // no limits

  limits = new int[glocdim+1];
  for (h = 0; h < glocdim+1; h++)
    limits[h] = 0;

  // limits
  v = 1;
  for (h = 0; h < n-1; h++) {
    v += N[h];
    limits[v] = 1;
  }
    
  Z = new REALVALUE**[3];
  Y = new REALVALUE***[3];

  for (h = 0; h < 3; h++) {
    
    Z[h] = new REALVALUE*[glocdim+1];
    
    for (j = 0; j < glocdim+1; j++) {
      Z[h][j] = new REALVALUE[k+1];
      //  -1 = unaffected
      for (l = 0; l < k+1; l++)
	Z[h][j][l] = -1;
    }

    Y[h] = new REALVALUE**[2];

    for (j = 0; j < 2; j++) {
      Y[h][j] = new REALVALUE*[glocdim+1];
      for (l = 0; l < glocdim+1; l++) {
	Y[h][j][l] = new REALVALUE[k+1];
	//  -1 = unaffected
	for (m = 0; m < k+1; m++)
	  Y[h][j][l][m] = -1;
      }  
      
    }
        
  }
  
  O = Z[0];
  I = Z[1];
  S = Z[2];
  o = Y[0];
  i = Y[1];
  s = Y[2];
  
}

NOrCompMulti::~NOrCompMulti()
{  
  int i,j,k;
  
  for (i = 0; i < 3; i++) {
    for (j = 0; j < glocdim+1; j++)
      delete [] Z[i][j];
    delete [] Z[i];
      }
  delete [] Z;

  for (i = 0; i < 3; i++) {
    for (k = 0; k < 2; k++) {
      for (j = 0; j < glocdim+1; j++)
	delete [] Y[i][k][j];
      delete [] Y[i][k];
    }
    delete [] Y[i];
  }
  delete [] Y;

  delete [] limits;

  delete [] locdim;

}
REALVALUE NOrCompMulti::getNO(int k)
{
  return(f_O(glocdim, k));
}

void NOrCompMulti::test()
{
  int j;

  for (j = 0; j <chromdim; j++)
    printf("dim %d = %d\n", j, locdim[j]);
 
  for (j = 0; j <= bpdim; j++)
    printf("%d %d %Lg\n", glocdim, j , f_O(glocdim,j));
}
      
