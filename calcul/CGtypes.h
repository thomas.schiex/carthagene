//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
//
// $Id: CGtypes.h,v 1.21 2010-03-12 10:42:45 dleroux Exp $
//
// Description : D�finitions de types
// Divers :
//-----------------------------------------------------------------------------

#ifndef _CGTYPES_H
#define _CGTYPES_H

class Tas;
class Carte;

enum Obs {
  Obs0000 = 0,   // Impossible
  Obs0001 = 1,   // 1 A
  Obs0010 = 2,   // 2
  Obs0011 = 3,   // 3
  Obs0100 = 4,   // 4
  Obs0101 = 5,   // 5
  Obs0110 = 6,   // 6 H
  Obs0111 = 7,   // 7 D
  Obs1000 = 8,   // 8 B
  Obs1001 = 9,   // 9
  Obs1010 = 10,   // a
  Obs1011 = 11,  // b
  Obs1100 = 12,  // c
  Obs1101 = 13,  // d
  Obs1110 = 14,  // e C
  Obs1111 = 15   // f -
};

extern char Obstoa[16];

#define EM_MAX_THETA 0.4995
#define EM_MIN_THETA 0.00001

// also used as logical!(Unk == False == 0)
enum CrossType {Unk, BC, RISelf, RISib, IC, RH, RHD, RHE, Mge, Mor, Con, Ordre, BS};

/**D�finition du type pointeur sur un entier */
typedef int *intPtr;

/**D�finition du type pointeur sur un double */
typedef double *doublePtr;

/** CN adds for convenience 12.06.05 */
typedef double **doubleArrPtr;

/**D�finition du type pointeur sur un caract�re */
typedef char *charPtr;

/**D�finition du type structure de stockage d'informations sur une carte */
typedef struct StructHMap HMap;

/**D�finition du type pointeur sur une structure de stockage d'informations
   sur une carte */
typedef HMap *HMapPtr;

/**D�finition du type couple d'entiers */
typedef int CoupleInt[2];

/**D�finition du type pointeur sur un couple d'entier */
typedef CoupleInt *CoupleIntPtr;

/** */
typedef struct nodint Nodint;
/**Structure utilis�e lors de la d�tection de groupe */
struct  nodint {
   int vertex;
   Nodint *next;
};

/** */
typedef Nodint *NodintPtr;


/**D�finition du type structure de liste */
typedef struct nodptr Nodptr;

/**Structure de liste utilis�e lors de la d�tection de groupe */
struct  nodptr {
   Nodint *first;
   Nodptr *next;
};

/**D�finition du type pointeur sur une liste */
typedef Nodptr *NodptrPtr;

typedef Tas *TasPtr;

typedef Carte *cartePtr;

struct testmartin
    {
       int *pdelta;
       int poids;
       int nbcas;
       int numero;
       int meilleur;
  };

#endif /* _CGTYPES_H */
