#include "QPolynomial.h"
#include <stdio.h>

/**
 * Class that represents polynomials as arrays of real coefficients.
 * The lowest (0) -degree term is at index 0 and the higher-degree terms
 * follow to the right. No power is left out even if its coefficient is 0.
 * The degree is always one less than the size of the coefficients array.
 * Author J. C. Nelson 11.13.05
 * adapted from a Java class by Roby Joehanes
 */
	QPolynomial::QPolynomial()
	{
		mDegree = 0; // to keep degree from being undefined.
	}	

	QPolynomial::QPolynomial(int degree)
	{
		mDegree = degree;
		mCoefficients = new double[degree + 1];
		for (int i = 0; i <= degree; i++)
			mCoefficients[i] = 0.0;
	}

	/**
	*	Copies from a pointer (typically, within a constant array) to
	*	a newly created array of coefficients.
	*/
	QPolynomial::QPolynomial(double *coeffs, int degree)
	{
		mDegree = degree;
		mCoefficients = new double[degree + 1];
		for (int i = 0; i <= degree; i++)	
			mCoefficients[i] = coeffs[i];
	}
	
	// Destructor
	QPolynomial::~QPolynomial()
	{	delete [] mCoefficients;}
	
	double *QPolynomial::getCoefficients()
	{	return mCoefficients;}

	void QPolynomial::setCoefficient(int power, double val)
	{	mCoefficients[power] = val;}
	
	int QPolynomial::getDegree()
	{	return mDegree;}

	/**
	 * Add another polynomial (of possibly different degree) to this polynomial
	 * @param poly
	 */
	void QPolynomial::plusEquals(QPolynomial *poly)
	{
		double *polyCoeffs, *newCoeffs;
		int thatDegree;
		
		thatDegree = poly->getDegree();
		if (thatDegree > mDegree)	// need to expand our coeffs array
		{
			newCoeffs = new double[thatDegree + 1];
			for (int i = 0; i <= mDegree; i++)
				newCoeffs[i] = mCoefficients[i];
			for (int i = mDegree + 1; i <= thatDegree; i++) // zero higher entries
				newCoeffs[i] = 0.0;
			delete [] mCoefficients;
			mCoefficients = newCoeffs;
			mDegree = thatDegree;
		}
		polyCoeffs = poly->getCoefficients();
		for (int i = 0; i <= thatDegree; i++)
			mCoefficients[i] += polyCoeffs[i];
	}

	/**
	 * Multiply this polynomial with a constant, in place
	 * @param c
	 */
	void QPolynomial::timesScalarEquals(double d)
	{
		for (int i = 0; i <= mDegree; i++)
			mCoefficients[i] *= d;
	}

	/**
	 * Multiply this polynomial with another polynomial and store the result into a new instance of QPolynomial
	 * @param poly
	 */
	QPolynomial *QPolynomial::times(QPolynomial *poly)
	{
		QPolynomial *result;
		
		result = clone();
		result->timesEquals(poly);
		return result;
	}

	/**
	 * Multiply this polynomial with another polynomial, in place
	 * @param poly
	 */
	void QPolynomial::timesEquals(QPolynomial *poly)
	{
		int thisDegree, thatDegree, combinedDegree;
		double *productCoeffs, *theseCoeffs, *thoseCoeffs;

		thisDegree = mDegree;
		thatDegree = poly->getDegree();
		combinedDegree = thisDegree + thatDegree;
		
		productCoeffs = new double[combinedDegree + 1];
		for (int i = 0; i <= combinedDegree; i++)
			productCoeffs[i] = 0.0;
		
		theseCoeffs = mCoefficients;
		thoseCoeffs = poly->getCoefficients();

		for (int i = 0; i <= thisDegree; i++)
			for (int j = 0; j <= thatDegree; j++)
				productCoeffs[i + j] += theseCoeffs[i] * thoseCoeffs[j]; 

		delete [] mCoefficients;
		mCoefficients = productCoeffs;
		mDegree = combinedDegree;
	}

	/**
	 * Evaluate this polynomial at x = x0
	 * @param x0
	 */
	double QPolynomial::evaluate(double x0)
	{
		double
			sum = 0.0,
			power = 1.0;
		int i;

		for (i = 0; i <= mDegree; i++)
		{
			sum += mCoefficients[i] * power;
			power *= x0;
		}
		return sum;
	}
	
	/**
	 * Evaluate this polynomial based on a precomputed vector of powers of the variable.
	 * To be used in QPolynomialMatrix. The powers vector is of at least the degree of the
	 * polynomial, but may be longer
	 * @return a scalar, the vector product
	 */
	double QPolynomial::evaluate(double powersOfX[])
	{
		double sum = 0.0;
		
		for (int i = 0; i <= mDegree; i++)
			sum += mCoefficients[i] * powersOfX[i];
		return sum;
	}

	/**
	 * Clone this polynomial
	 */
	QPolynomial *QPolynomial::clone()
	{	
		double *newCoeffs;
		
		newCoeffs = cloneCoefficients();
		return new QPolynomial(newCoeffs, mDegree);
	}

	// Returns the cloned coeffs array of a QPolynomial
	double* QPolynomial::cloneCoefficients()
	{
		double *newCoeffs;
		
		newCoeffs = new double[mDegree + 1];
		for (int i = 0; i <= mDegree; i++)
			newCoeffs[i] = mCoefficients[i];
		return newCoeffs;
	}
	
	bool QPolynomial::hasAllZeroCoeffs()
	{
		bool allZero = true;
		
		for (int coeff = 0; coeff <= mDegree; coeff++)
			if (mCoefficients[coeff] != 0.0)
				allZero = false;
		
		return allZero;
	}
	
	/**
	 * Debugging routine
	 */
	void QPolynomial::printPoly(bool withNewline)
	{
		for (int i = 0; i <= mDegree; i++)
			printf("%1.4f\t", mCoefficients[i]);
		if (withNewline)
			printf("\n");
	}
