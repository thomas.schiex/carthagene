//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: tsp.h,v 1.4.4.1 2011-08-19 12:37:17 dleroux Exp $
//
// Description : Classe API du module.
// Divers : 
//-----------------------------------------------------------------------------


#ifndef _TSP_H
#define _TSP_H

/* special flags used by ParetoLKH in the PseudoParetoStatus data structure */
#define BPFIRST 1
#define RHFIRST 2
#define SUPPORTEDPARETO 4
#define PARETO2OPT 8
#define BALANCED 16
/* should be the last flag */
#define PARETO 32

#define PARETOEPSILON 0.00001

extern "C" {
typedef double (*funcloglike1)(BioJeu *ArbreJeu, int m1);
typedef double (*funcloglike2)(BioJeu *ArbreJeu, int m1, int m2);

extern double contribLogLike2pt1(BioJeu *ArbreJeu, int m1);
extern double contribLogLike2pt2(BioJeu *ArbreJeu, int m1, int m2);
extern double normContribLogLike2pt1(BioJeu *ArbreJeu, int m1);
extern double normContribLogLike2pt2(BioJeu *ArbreJeu, int m1, int m2);
extern double contribOCB(BioJeu *ArbreJeu, int m1, int m2);
extern double normContribOCB(BioJeu *ArbreJeu, int m1, int m2);
extern double contribZero(BioJeu *ArbreJeu, int m1);
extern double contribLOD(BioJeu *ArbreJeu, int m1, int m2);
extern double contribHaldane(BioJeu *ArbreJeu, int m1, int m2);
}

#endif
