//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BioJeuSingle.cc,v 1.20.2.4 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Classe de base BioJeuSingle.
// Divers : 
//-----------------------------------------------------------------------------

#include "BioJeuSingle.h"

#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "twopoint.h"

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BioJeuSingle::BioJeuSingle()
{
	Cartage = NULL;
	NomJeu = NULL;
	Cross = Unk;
	BitJeu = 0;
	NbMarqueur = 0;
	TailleEchant = 0;
	Echantillon = NULL; 
	// DL
	//TwoPointsFR = NULL; 
	//TwoPointsDH = NULL; 
	//TwoPointsLOD = NULL; 
	twopoint = NULL;
    _indmarq_sz = 0;

	Em_Max_Theta = EM_MAX_THETA;
	Em_Min_Theta = EM_MIN_THETA;

}


//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - le r�f�rentiel
// - le num�ro
// - le chemin et nom du fichier de donnees
// - le type
// - le nombre de marqueurs
// - la taille de l'echantillon
// - le champ de bit
// - la matrice de l'�chantillon
// Valeur de retour : 
//-----------------------------------------------------------------------------

BioJeuSingle::BioJeuSingle(CartaGenePtr cartag,
		int id,
		charPtr nomjeu,
		CrossType cross,
		int nm, 
		int taille,
		int bitjeu,
		int *indmarq,
		Obs **echantil)
: BioJeu(cartag, id, cross, nm, bitjeu)
{  

	Em_Max_Theta = EM_MAX_THETA;
	Em_Min_Theta = EM_MIN_THETA;

	NomJeu = new char[strlen(nomjeu)+1];
	strcpy(NomJeu, nomjeu);

	TailleEchant = taille;
	// DL
	/*
    printf("%s ; ordre des marqueurs = [", nomjeu);
	for(int z=0;z<=cartag->NbMarqueur;z++) {
		printf(" %i", indmarq[z]);
	}
	printf(" ]\n");
    //*/
	
	/* DL
	 * r�organisation du tableau Echantillon, dans l'ordre connu par CarthaG�ne, avec des trous.
	 */

    _indmarq_sz = Cartage->NbMarqueur+1;
    //printf("Echantillon : alloc %i rows\n", _indmarq_sz);

	Echantillon = new Obs*[_indmarq_sz];
	//IndMarq = new int[_indmarq_sz];
    IndMarq_cg2bj = new int[Cartage->NbMarqueur+1];
    IndMarq_bj2cg = indmarq;
    for(int i=Cartage->NbMarqueur;i>=0;--i) {
        IndMarq_cg2bj[i] = 0;
    }
    for(int i=NbMarqueur;i>0;--i) {
        IndMarq_cg2bj[indmarq[i]] = i;
    }
  /*printf("indmarq_cg2bj=");*/
  /*char sep = '[';*/
  /*for(int i=0;i<=Cartage->NbMarqueur;++i) {*/
      /*printf("%c%i", sep, IndMarq_cg2bj[i]);*/
      /*sep = ' ';*/
  /*}*/
  /*printf("\n");*/
  /*printf("indmarq_bj2cg=");*/
  /*sep = '[';*/
  /*for(int i=0;i<=NbMarqueur;++i) {*/
      /*printf("%c%i", sep, IndMarq_bj2cg[i]);*/
      /*sep = ' ';*/
  /*}*/
  /*printf("\n");*/
	for(int i=1;i<_indmarq_sz;++i) {
		Echantillon[i] = echantil[0];
		//IndMarq[i] = i*!!indmarq[i]; /* backward compatibility */
	}
	for(int i=0;i<=NbMarqueur;++i) {
		Echantillon[IndMarq_bj2cg[i]] = echantil[i];
	}
	/* DL
	 * maintenant on lib�re les tableaux inutiles
	 */
	delete[] echantil;

	twopoint = new TwoPoint::Matrices(this);
}


void BioJeuSingle::_updateIndMarq(int max) {
    int oldmax = _indmarq_sz;
    BioJeu::_updateIndMarq(max);
    if(Cross==Ordre) {
        return;
    }
    Obs** nech = new Obs*[max];
	int i;
	/* TODO : parcourir le tableau jusqu'� trouver IndMarq[i]==NbMarqueur(-1?), �a indiquera la fin */
	for(i=0;i<oldmax;++i) {
        nech[i]= Echantillon[i];
	}
	for(;i<max;++i) {
        nech[i]=Echantillon[0];
	}
    delete[] Echantillon;
    Echantillon = nech;
}


inline int* clone_im(int* im, int sz) {
	if(!(im&&sz)) {
		return NULL;
	}
	int* ret = new int[sz];
	/*memcpy(ret, im, sizeof(int)*sz);*/
	for(int i=0;i<sz;++i) {
		ret[i]=im[i];
	}
	return ret;
}


inline Obs** clone_ech(Obs** ech, int nm, int ne) {
	if(!(ech&&nm&&ne)) {
		return NULL;
	}
	Obs** ret = new Obs*[nm+1];
    ret[0] = new Obs[ne+1];
    for(int j=0;j<=ne;++j) {
        ret[0][j] = Obs1111;
    }
	for(int i=1;i<nm;++i) {
        if(ech[i]!=ech[0]) {
    		ret[i] = new Obs[ne+1];
	    	/*memcpy(ret[i], ech[i], sizeof(Obs)*nm);*/
		    for(int j=0;j<=ne;++j) {
			    ret[i][j] = ech[i][j];
    		}
        } else {
            ret[i] = ret[0];
        }
	}
	return ret;
}


BioJeuSingle::BioJeuSingle(const BioJeuSingle& b)
: BioJeu(b.Cartage, b.Cartage->NbJeu+1, b.Cross, b.NbMarqueur, 1<<b.Cartage->NbJeu)
{

	Em_Max_Theta = EM_MAX_THETA;
	Em_Min_Theta = EM_MIN_THETA;

	NomJeu = new char[strlen(b.NomJeu)+1];
	strcpy(NomJeu, b.NomJeu);

	TailleEchant = b.TailleEchant;
    _indmarq_sz = b._indmarq_sz;
	IndMarq_cg2bj = clone_im(b.IndMarq_cg2bj, _indmarq_sz);
    IndMarq_bj2cg = new int[NbMarqueur+1];
    for(int i=0;i<_indmarq_sz;++i) {
        IndMarq_bj2cg[IndMarq_cg2bj[i]] = i;
    }
	Echantillon = clone_ech(b.Echantillon, _indmarq_sz, b.TailleEchant);

	// DL
	twopoint = new TwoPoint::Matrices(this);
	/*
	   TwoPointsFR = new doublePtr[NbMarqueur + 1];
	   TwoPointsDH = new doublePtr[NbMarqueur + 1];
	   TwoPointsLOD = new doublePtr[NbMarqueur + 1];

	   for (int i = 0; i <= NbMarqueur; i++) 
	   {
	   TwoPointsFR[i]    = new double[NbMarqueur + 1];
	   TwoPointsDH[i]    = new double[NbMarqueur + 1];
	   TwoPointsLOD[i]    = new double[NbMarqueur + 1];

	   for (int j = 0; j <= NbMarqueur; j++) 
	   {
	   TwoPointsFR[i][j] = 0.0;
	   TwoPointsDH[i][j] = 0.0;
	   TwoPointsLOD[i][j] = 0.0;
	   }
	   }
	   */
}

char* probeSymbols(const char* filename, char tr[256]) {
	FILE*f;
	static char buf[512];
	char* sym;
	char* ret;
	for(int i=0;i<256;++i) {
		tr[i]=i;
	}
	f=fopen(filename, "r");
	/* skip "data type .*" header */
	if(!fgets(buf, 512, f)) {
		/* whine */
	}
	if(!fgets(buf, 512, f)) {
		/* whine */
	}
	fclose(f);
	if(!(sym=strstr(buf, "symbols "))) {
		return (char*)"0";
	}
	ret=sym;
	sym+=8;
	while((sym=strchr(sym, '='))) {
		tr[(int)*(sym+1)]=*(sym-1);
		sym+=2;
	}
	return ret;
}

// DL
/** Output dataset to a character stream */
void BioJeuSingle::DumpTo(FILE* outFile) const throw(BioJeu::NotImplemented) {
	intPtr mrk = Cartage->MarkSelect;
	int N = Cartage->NbMS;
	int sub;
	char tr[256];
	char* second_line = probeSymbols(NomJeu, tr);
#	define MRK_SEP ":"

	fprintf(outFile, "data type %s\n", GetDataType());
	fprintf(outFile, "%i %i %i %s\n\n",
			TailleEchant,
			N,
			0,
			second_line);

	for(int i=0;i<N;++i) {
		sub=mrk[i];
		if((Cartage->markers[sub].BitJeu&BitJeu) && !Cartage->markers[sub].Merged) {
			if(!Cartage->markers.keyOf(sub).size()) {
				printf("PAS DE NOM POUR LE MARQUEUR #%i\n", sub);
			}
			fprintf(outFile, "*%s", Cartage->markers.keyOf(sub).c_str());
			while(Cartage->markers[sub].Represents) {
				sub=Cartage->markers[sub].Represents;
				fputs(MRK_SEP, outFile);
				fputs(Cartage->markers.keyOf(sub).c_str(), outFile);
			}
			fputs("  ", outFile);
			for(int e=1;e<=TailleEchant;++e) {
				fputc(tr[(int)obs2chr(Echantillon[mrk[i]][e])], outFile);
			}
			fputc('\n', outFile);
		}
	}
	fputs("\n# Informations on this dataset :\n", outFile);
	fprintf(outFile, "#   Original data set : %s\n", NomJeu);
	fputs("#   Merged markers :\n", outFile);
	sub=0;
	for(int i=0;i<N;++i) {
		if((Cartage->markers[mrk[i]].BitJeu&BitJeu) && Cartage->markers[mrk[i]].Merged) {
			fprintf(outFile, "#      %s was merged into %s\n",
					Cartage->markers.keyOf(mrk[i]).c_str(),
					Cartage->markers.keyOf(Cartage->markers[mrk[i]].Merged).c_str()
			       );
			++sub;
		}
	}
	if(!sub) {
		fputs("#      None.\n", outFile);
	}
	/* TODO : imputation ? */
	fputs("# TODO Deselected markers that were not included in this file :", outFile);
	fputs("\n\n", outFile);
}

// DL
/** Convert back an observation value into a character
 *
 * @param o observation
 * @return the character corresponding to the observation depending on the BJS_* type
 */
char BioJeuSingle::obs2chr(Obs o) const throw(BioJeu::NotImplemented) {
	/* default behaviour :
	 * - 0 => A
	 * - 1 => H
	 * - F => -
	 */
	switch(Cross) {
		case IC:
		case BS:
			switch(o) {
				case Obs0000 : return '0';
				case Obs0001 : return 'A';
				case Obs0010 : return '2';
				case Obs0011 : return '3';
				case Obs0100 : return '4';
				case Obs0101 : return '5';
				case Obs0110 : return 'H';
				case Obs0111 : return 'D';
				case Obs1000 : return 'B';
				case Obs1001 : return '9';
				case Obs1010 : return 'a';
				case Obs1011 : return 'b';
				case Obs1100 : return 'c';
				case Obs1101 : return 'd';
				case Obs1110 : return 'C';
				case Obs1111 : return '-';
				default: throw BioJeu::NotImplemented();
			};
		default:;
			switch(o) {
				case Obs0001 : return 'H';
				case Obs0000 : return 'A';
				case Obs1111 : return '-';
				default: throw BioJeu::NotImplemented();
			};
	};
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BioJeuSingle::~BioJeuSingle()
{  

	// verrue pour Con

	if (Echantillon) {
		Obs*empty = Echantillon[0];

		for (int i = 0; i <= NbMarqueur; i++)
		{
			// DL
			//delete [] TwoPointsDH[i];
			//delete [] TwoPointsFR[i];
			//delete [] TwoPointsLOD[i];
			if(Echantillon[i]!=empty) {
				delete [] Echantillon[i];
			}
		}
		delete[] empty;
		// DL
		//delete [] TwoPointsDH;
		//delete [] TwoPointsFR;
		//delete [] TwoPointsLOD;
		delete [] Echantillon;
		/*delete [] IndMarq_cg2bj;*/
		/*delete [] IndMarq_bj2cg;*/
	}

	delete [] NomJeu;
}


//-----------------------------------------------------------------------------
// Remplit les tableaux
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BioJeuSingle::ComputeTwoPoints(void)
{
	// DL
	/*
	   double fr;
	   double lod;
	   int i = 0, k = 0;
	   int j = 0, l = 0;

	   while (i < NbMarqueur) 
	   {
	   if (IndMarq[k]) 
	   {
	   i++;
	   j = i + 1;
	   l = k + 1;
	   while (j <= NbMarqueur) 
	   {
	   if (IndMarq[l]) 
	   {
	   lod = ComputeOneTwoPoints(k, l, Epsilon2, &fr);
	   TwoPointsLOD[IndMarq[k]][IndMarq[l]] = 
	   TwoPointsLOD[IndMarq[l]][IndMarq[k]] = lod;
	   TwoPointsFR[IndMarq[k]][IndMarq[l]] = 
	   TwoPointsFR[IndMarq[l]][IndMarq[k]] = fr;
	   TwoPointsDH[IndMarq[k]][IndMarq[l]] = 
	   TwoPointsDH[IndMarq[l]][IndMarq[k]] = 
	   (HasRH() ? Theta2Ray(TwoPointsFR[j][i]) : Haldane(TwoPointsFR[j][i]));
	   j++;
	   }
	   l++;
	   }
	   }
	   k++;
	   }
	   */
}

//-----------------------------------------------------------------------------
// Affichage des donn�es
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//----------------------------------------------------------------------------

void BioJeuSingle::DumpEch(void) const
{
	int i = 0, j, k = 0;

	print_out( "%10s : Individuals...\n","Markers");
	// Nom des marqueurs et les observations.
	while (i < Cartage->NbMarqueur) 
	{
		if (IndMarq_cg2bj[k]) {
			i++;
			print_out( "%10s : ", Cartage->markers.keyOf(k).c_str());
			for (j=1; j<=TailleEchant; j++)

				print_out("%x", GetEch(k, j)); //Echantillon[IndMarq[k]][j]);

			print_out(  "\n");
		}
		k++;
	}
}

//-----------------------------------------------------------------------------
// Affichage des donn�es pour un marqueur
//-----------------------------------------------------------------------------
// Param�tres : 
// - le num�ro d'un marqueur
// - 
// Valeur de retour : 
//----------------------------------------------------------------------------
void BioJeuSingle::DumpEchMarq(int numarq) const
{

	for (int i=1; i <= TailleEchant; i++) 
	{
		print_out( "%x", GetEch(numarq, i));
		flush_out();
	}
}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les num�ro du couple
// - 
// Valeur de retour : le LOD 2 points
//----------------------------------------------------------------------------

double BioJeuSingle::GetTwoPointsLOD(int m1, int m2) const
{
	// DL
	//return TwoPointsLOD[IndMarq[m1 * ((Cartage->markers[m1].BitJeu & BitJeu) > 0)]][IndMarq[m2 * ((Cartage->markers[m2].BitJeu & BitJeu) > 0)]];
	//return TwoPointsLOD[IndMarq[m1]][IndMarq[m2]];
	return twopoint->getLOD(IndMarq_cg2bj[m1], IndMarq_cg2bj[m2]);
}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les num�ro du couple
// - 
// Valeur de retour : la distance 2 points
//----------------------------------------------------------------------------

double BioJeuSingle::GetTwoPointsDH(int m1, int m2) const
{
	// DL
	//return  (HasRH() ? Theta2Ray(TwoPointsFR[IndMarq[m1 * ((Cartage->markers[m1].BitJeu & BitJeu) > 0)]][IndMarq[m2 * ((Cartage->markers[m2].BitJeu & BitJeu) > 0)]]) : Haldane(TwoPointsFR[IndMarq[m1 * ((Cartage->markers[m1].BitJeu & BitJeu) > 0)]][IndMarq[m2 * ((Cartage->markers[m2].BitJeu & BitJeu) > 0)]]));
	return twopoint->getDH(IndMarq_cg2bj[m1], IndMarq_cg2bj[m2]);
}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les num�ro du couple
// - 
// Valeur de retour : la Fraction de recombinaison 2 points
//----------------------------------------------------------------------------

double BioJeuSingle::GetTwoPointsFR(int m1, int m2) const
{
	// DL
	// return TwoPointsFR[IndMarq[m1]][IndMarq[m2]];
	return twopoint->getFR(IndMarq_cg2bj[m1], IndMarq_cg2bj[m2]);
}


//-----------------------------------------------------------------------------
// Affichage de la matrice TwoPoints Lod
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// Valeur de retour :
//----------------------------------------------------------------------------

void BioJeuSingle::DumpTwoPointsLOD() const
{
	int i = 0, k = 0;
	int j = 0, l = 0;

	while (i < Cartage->NbMarqueur) 
	{
		if (IndMarq_cg2bj[k]) 
		{
			i++;
			print_out( "%3d :", k);
			j = 0;
			l = 0;
			while (j < Cartage->NbMarqueur) 
			{
				if (IndMarq_cg2bj[l]) 
				{
					j++;
					print_out( "%5.2f ", GetTwoPointsLOD(k, l));
				}
				l++;
			}
			print_out(  "\n");
		}	
		k++;
	}   
}

void BioJeuSingle::DumpTwoPointsFR() const
{
	int i = 0, k = 0;
	int j = 0, l = 0;

	while (i < Cartage->NbMarqueur) 
	{
		if (IndMarq_cg2bj[k]) 
		{
			i++;
			print_out( "%3d :", k);
			j = 0;
			l = 0;
			while (j < Cartage->NbMarqueur) 
			{
				if (IndMarq_cg2bj[l]) 
				{
					j++;
					print_out( "%5.2f ", GetTwoPointsFR(k, l));
				}
				l++;
			}
			print_out(  "\n");
		}	
		k++;
	}   
}
