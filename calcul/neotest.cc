//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: neotest.cc,v 1.9.2.4 2012-08-02 15:43:55 dleroux Exp $
//
// Description : Programme principal.
// Divers : test
//-----------------------------------------------------------------------------

#include "carthagene_all.h"
/*#include "CartaGene.h"*/
/*#include "Carte.h"*/

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sys/stat.h>

/*#include "twopoint.h"*/

/*char bouf[2048];*/
/*char boufi[2048];*/
FILE *Fout;


double contribLogLike2pt1(BioJeu*, int);
double contribLogLike2pt2(BioJeu*, int, int);

int main(int argc, char *argv[])
{

	struct stat buf;
	int nm;
	int *vm;
	Carte *LaCarte;
	double thecost;
<<<<<<< HEAD
=======


	int DFsize = 5;
	double DFthres = 3.0;
	int DFiter = 0;

	/* pour le recuit */

	int Tries = 50;
	double Tinit = 100;
	double Tfinal = 0.1;
	double Cooling = 0.5;
>>>>>>> First commit in (pre-) 1.3 branch.

	/* pour le glouton*/

<<<<<<< HEAD
	int DFsize = 5;
	double DFthres = 3.0;
	int DFiter = 0;

	/* pour le recuit */

	int Tries = 50;
	double Tinit = 100;
	double Tfinal = 0.1;
	double Cooling = 0.5;

	/* pour le glouton*/

	int NbRun = 1;
	int NbIter = 0;
	int TabooLenMin = 5;
	int TabooLenMax = 25;

	/* pour algorithmes g�n�tiques */

	int NbGens = 10;
	int NbEle = 10;
	int SelNbr = 0;
	float pCross = 0.33;
	float pMut = 0.55;
	int EvolFitness = 1;

	int mode=0;
	const char* modes[] = { "-dump", "-group", "-mrkdouble", NULL };

	CartaGene Cartage;

	int i,j, start=1;

	const char*twopoint_backends[] = { "nocache", "hashcache", "buffer", "swap", NULL };
	for(i=0;twopoint_backends[i]&&start==1;++i) {
		if(!strcmp(twopoint_backends[i], argv[1])) {
			/*TwoPoint::Backend::Factory::set_default(argv[1]);*/
			/*printf("Setting matrix backend to `%s'\n", argv[1]);*/
			start=2;
			break;
		}
	}

	for(mode=0;modes[mode];++mode) {
		if(!strcmp(argv[start], modes[mode])) {
			++start;
			break;
		}
	}

	for (i = start; i < argc; i++) 
	{
		if (stat(argv[i], &buf) == -1) 
		{
			printf("%s: fichier inexistant\n", argv[i]);
			perror(argv[i]);
			return -1;
		}

		printf("Loaded : %s\n\n", Cartage.CharJeu(argv[i]));
	}
	char** dbl;
	switch(mode) {
		case 0:
			Cartage.Jeu[1]->DumpTwoPointsLOD();
			Cartage.Jeu[1]->DumpTwoPointsFR();
			break;
		case 1:
			Cartage.Jeu[1]->Groupe(.5, 3);
			/*Cartage.Jeu[1]->Groupe(3, .3);*/
			break;
		case 2:
			dbl = Cartage.GetDouble();
			while(*dbl) {
				std::cout << *dbl << std::endl;
				++dbl;
			}
			break;
		default:;
	};
	/*std::cerr << " === Memory cache ======" << std::endl << TwoPoint::Backend::Memory::Cache::stats;*/
	/*std::cerr << " === Disk cache ======" << std::endl << TwoPoint::Backend::Disk::impl::PageCache::stats << std::endl;*/
	/*std::cerr << "performed " << Cartage.Jeu[1]->computation_count() << " computations" << std::endl;*/
		/*Cartage.AffMarq();*/
		/*Cartage.AffJeu();*/
		/*Cartage.PrintTwoPointsLOD();      */
		/*Cartage.Jeu[1]->DumpTwoPointsLOD();*/
		/*printf("\n==================\n");*/
		/*Cartage.Jeu[1]->DumpTwoPointsFR();*/
		/*printf("\n==================\n");*/
		/*Cartage.Jeu[1]->Groupe(.5, 3);*/
		/*Cartage.Jeu[1]->Groupe(3, .3);*/
		/*std::cout << "CACHE STATISTICS :" << std::endl;*/
#if 0
		printf("LOD[1,2]=%lf\n", Cartage.Jeu[1]->GetTwoPointsLOD(1, 2));
		printf("FR[3,2]=%lf\n", Cartage.Jeu[1]->GetTwoPointsFR(3, 2));
		printf("LOD[3,2]=%lf\n", Cartage.Jeu[1]->GetTwoPointsLOD(3, 2));
		printf("FR[1,2]=%lf\n", Cartage.Jeu[1]->GetTwoPointsFR(1, 2));
		std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(2);
		std::cout << "CACHE STATISTICS :" << std::endl << " === Memory cache ======" << std::endl;
		std::cout << "cache size           : " << TwoPoint::Backend::Memory::Cache::stats.size() << std::endl;
		std::cout << "cache hits           : " << TwoPoint::Backend::Memory::Cache::stats.hits() << std::endl;
		std::cout << "cache misses         : " << TwoPoint::Backend::Memory::Cache::stats.misses() << std::endl;
		std::cout << "cache inuse          : " << TwoPoint::Backend::Memory::Cache::stats.inuse() << std::endl;
		std::cout << "cache occupation     : " << TwoPoint::Backend::Memory::Cache::stats.occupation() << '%' << std::endl;
		std::cout << "cache efficiency     : " << TwoPoint::Backend::Memory::Cache::stats.efficiency() << '%' << std::endl;
		std::cout << "cache raw efficiency : " << TwoPoint::Backend::Memory::Cache::stats.raw_efficiency() << '%' << std::endl;
		std::cout << " === Disk cache ======" << std::endl;
		std::cout << "cache size           : " << TwoPoint::Backend::Disk::impl::PageCache::stats.size() << std::endl;
		std::cout << "cache hits           : " << TwoPoint::Backend::Disk::impl::PageCache::stats.hits() << std::endl;
		std::cout << "cache misses         : " << TwoPoint::Backend::Disk::impl::PageCache::stats.misses() << std::endl;
		std::cout << "cache inuse          : " << TwoPoint::Backend::Disk::impl::PageCache::stats.inuse() << std::endl;
		std::cout << "cache occupation     : " << TwoPoint::Backend::Disk::impl::PageCache::stats.occupation() << '%' << std::endl;
		std::cout << "cache efficiency     : " << TwoPoint::Backend::Disk::impl::PageCache::stats.efficiency() << '%' << std::endl;
		std::cout << "cache raw efficiency : " << TwoPoint::Backend::Disk::impl::PageCache::stats.raw_efficiency() << '%' << std::endl;
		std::cout << "last but not least : sizeof(Obs) = " << sizeof(Obs) << std::endl;
#endif
	/*}*/

	// printf("Created : %s\n\n", Cartage.MerGen(1,2));
	//  printf("Created : %s\n", Cartage.MergOr(1,2));
	//printf("Created : %s\n\n", Cartage.MergOr(3,4));
	//printf("%s\n", Cartage.MergOr(5,6));

	//  Cartage.AffMarq();
	//  Cartage.AffJeu();
	//  Cartage.BuildFW(3.0,3.0,NULL,0,0);
	//Cartage.DumpEch(1);
	//Cartage.DumpEch(2);
	//Cartage.DumpEch(3);
	//Cartage.DumpEch(4);
	//Cartage.DumpEch(5);
	// Cartage.DumpEch(6);
	// Cartage.DumpEch(7);

	//Cartage.DumpTwoPointsLOD(1);
	// Cartage.DumpTwoPointsFR(1);
	//   Cartage.DumpTwoPointsFR(2);
	//   Cartage.DumpTwoPointsFR(3);
	//   Cartage.DumpTwoPointsLOD(1);
	//   Cartage.DumpTwoPointsLOD(2);
	//   Cartage.DumpTwoPointsLOD(3);
	//Cartage.DumpTwoPointsLOD(2);
	//Cartage.DumpTwoPointsLOD(3);
	//Cartage.DumpTwoPointsLOD(4);
	//Cartage.DumpTwoPointsLOD(5);
	//  Cartage.DumpTwoPointsLOD(6);
	//  Cartage.DumpTwoPointsLOD(7);

	//  Cartage.Groupe(0.5, 3.0);
	//Cartage.Groupe(2, 0.5, 3.0);
	//Cartage.Groupe(3, 0.5, 3.0);

	//Cartage.Groupe(3, 0.5, 3.0);


	//  int *vmi;

	//  int nbm = Cartage.GetGroupeI(20, &vmi);

	// Cartage.ChangeSel(vmi, nbm);

	//   for (i = 0; i < nbm; i++)
	//   {
	//       printf("%d\n", vmi[i]);
	//       flush_out();
	//     }

	//    nm = 12;
	//     vm = new int[nm];
	//     for (i = 0; i < nm; i++) vm[i] = i + 1;

	//    vm[2] = 14;

	//     Cartage.Heap.Print();

	//    LaCarte = new Carte(&Cartage, nm, vm);

	//    LaCarte->PrintMap();

	//    thecost = Cartage.ComputeEM(LaCarte); 

	//    LaCarte->PrintMap();

	//    Cartage.PrintMap(LaCarte);
	//    Cartage.PrintDMap(LaCarte);

	//  Cartage.VerboseFlag = 2;
	//  Cartage.QuietFlag = 0;
	//Cartage.BuildNiceMap();
	//Cartage.Heap.PrintDSort();
	// Cartage.Heap.Print();
	//  for (i=0;i<1000;i++)
	/*Cartage.SinglEM();*/
	// Cartage.Heap.PrintDSort();
	//Cartage.Heap.Print();
	// Cartage.Heap.PrintSort();

	// LaCarte = Cartage.Heap.Best();

	/*char *** roro = Cartage.GetMap("k",0);*/
	/*i = 0;*/
	/*while (roro[i] != NULL) {*/
	/*j = 0;*/
	/*while (roro[i][j] != NULL)*/
	/*{*/
	/*printf("%s\n", roro[i][j]);*/
	/*delete [] roro[i][j++];*/
	/*}*/
	/*delete [] roro[i++];*/
	/*}*/

	//   Cartage.Annealing(Tries, Tinit, Tfinal, Cooling);

	//   Cartage.Heap.PrintDSort();

	//   Cartage.ResizeHeap(3);

	//   Cartage.Heap.PrintDSort();

	//    Cartage.ChangeSel(vm, nm);

	//     Cartage.ChangeSel(vm, nm);

	//     Cartage.Build(1);

	//     Cartage.Heap.PrintDSort();

	//   Cartage.SinglEM();

	//   Cartage.Heap.PrintDSort();


	//   Cartage.Annealing(Tries, Tinit, Tfinal, Cooling);
	/*Cartage.Greedy(NbRun, NbIter,TabooLenMin, TabooLenMax,0);*/
	//    Cartage.AlgoGen(NbGens, NbEle, SelNbr, pCross,
	//	    pMut, EvolFitness);
	/*Cartage.Heap.PrintDSort();*/

	//Cartage.Heap.PrintDSort();


	//   Cartage.Heap.Print();

	//   printf("+++++++++++++++++\n");

	//   Cartage.Heap.PrintSort();

	//      Cartage.Flips(DFsize, DFthres, DFiter);
	//      Cartage.Flips(5, 3.0, 1.0);
	//      Cartage.Heap.PrintSort();

	//      Cartage.Heap.Stat(1.0);

	//   Cartage.Polish();

	//   Cartage.Heap.PrintSort();

	//LaCarte->PrintDMap();
	//

	/* test de LKH */
	/*Cartage.lkh(10, 1,contribLogLike2pt1,contribLogLike2pt2);*/

	/* test de MCMC ; il faut avoir charg� DEUX jeux (donn�es et r�f�rence) */
	/*        printf("*\n*** TESTING MCMC ***\n*\n");
			  Cartage.MergOr(1, 2);
			  Cartage.SetBreakPointLambda(2, 1, 49.);
			  int defmrk[100] = {6,5,4,7,3,2,1,8,10,9,11,12,13,15,14,16,20,19,18,22,23,17,21,24,25,26,27,29,28,30,31,32,35,33,34,38,39,36,37,40,41,43,45,42,44,46,47,48,49,50,51,52,54,55,58,56,57,53,59,61,60,63,62,64,65,66,71,70,69,67,68,72,74,73,79,81,80,77,76,75,78,82,86,85,84,83,87,88,90,89,91,94,92,93,96,95,100,99,98,97};
			  Cartage.ChangeSel(defmrk, 100);
			  Cartage.SinglEM();
			  Cartage.mcmc(1, 1000, 500, contribLogLike2pt1,contribLogLike2pt2);

			  Carte *best;
			  best = Cartage.Heap.Best();
			  Cartage.PrintDMap(best,0,best);
	//Cartage.PrintDMap(LaCarte);
	*/
	return 0;
}
