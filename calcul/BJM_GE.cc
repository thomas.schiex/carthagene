//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJM_GE.cc,v 1.15.6.2 2011-02-24 18:30:44 dleroux Exp $
//
// Description : BJM_GE.
// Divers :
//-----------------------------------------------------------------------------

#include "BJM_GE.h"

#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "twopoint.h"

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------


BJM_GE::BJM_GE() : BioJeuMerged()
{

}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - type de jeu de donn�e(Mor)
// - nm nombre de marqueurs.
// - bitjeu champ de bit du jeu de donn�es.
// - gauche jeu de donn�es.
// - droite jeu de donn�es. 
//-----------------------------------------------------------------------------

BJM_GE::BJM_GE(CartaGenePtr cartag,
		int id,
		CrossType cross,
		int nm, 
		int bitjeu,
		BioJeu *gauche, 
		BioJeu *droite) 
: BioJeuMerged(cartag,
		id,
		cross,
		nm, 
		bitjeu,
		gauche, 
		droite)
{
	NbMeiose = BJgauche->NbMeiose + BJdroite->NbMeiose;

	int i = 0, k = 0;
	int j = 0, l = 0, nbcm = 0;
	int *indmarqpro = new int[Cartage->NbMarqueur + 1];

	for (i = 0; i <= Cartage->NbMarqueur; i++) indmarqpro[i] = 0;

	// recherche des couples communs au deux jeux
	i = 0;
	while (i < NbMarqueur) 
	{
		if (Cartage->markers[k].BitJeu & BitJeu) 
		{
			i++;
			j = i + 1;
			l = k + 1;
			while (j <= NbMarqueur) 
			{
				if (Cartage->markers[l].BitJeu & BitJeu) 
				{
					if (BJgauche->Couplex(k,l) &&
							BJdroite->Couplex(k,l))
					{		    
						if (!indmarqpro[k]) indmarqpro[k] = ++nbcm;
						if(!indmarqpro[l]) indmarqpro[l] = ++nbcm;
					}

					j++;
				}
				l++;
			}
		}	
		k++;
	} 

	// allocations

	//IndMarq = new int[l];
    IndMarq_cg2bj = indmarqpro;
    IndMarq_bj2cg = new int[NbMarqueur+1];
    for(int i=Cartage->NbMarqueur;i>=0;--i) {
    /*for(int i=0;i<=Cartage->NbMarqueur;++i) {*/
        IndMarq_bj2cg[IndMarq_cg2bj[i]] = i;
    }

  /*printf("indmarq_cg2bj=");*/
  /*char sep = '[';*/
  /*for(int i=0;i<=Cartage->NbMarqueur;++i) {*/
      /*printf("%c%i", sep, IndMarq_cg2bj[i]);*/
      /*sep = ' ';*/
  /*}*/
  /*printf("\n");*/
  /*printf("indmarq_bj2cg=");*/
  /*sep = '[';*/
  /*for(int i=0;i<=NbMarqueur;++i) {*/
      /*printf("%c%i", sep, IndMarq_bj2cg[i]);*/
      /*sep = ' ';*/
  /*}*/
  /*printf("\n");*/
	// DL
	/*
	   TwoPointsFR = new doublePtr[nbcm + 1];
	   TwoPointsDH = new doublePtr[nbcm + 1];
	   TwoPointsLOD = new doublePtr[nbcm + 1];

	   for (i = 0; i <= nbcm; i++) 
	   {
	   TwoPointsFR[i]    = new double[nbcm + 1];
	   TwoPointsDH[i]    = new double[nbcm + 1];
	   TwoPointsLOD[i]    = new double[nbcm + 1];

	   for (int j = 0; j <= nbcm; j++) 
	   {
	   TwoPointsFR[i][j] = 0.0;
	   TwoPointsDH[i][j] = 0.0;
	   TwoPointsLOD[i][j] = 0.0;
	   }
	   }
	   */

	// remplissage

	TailleCharn = nbcm;

	//for (i = 0; i < l; i++) IndMarq[i] = indmarqpro[i];

	//delete indmarqpro;

	ComputeTwoPoints();
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJM_GE::~BJM_GE()
{
}

//-----------------------------------------------------------------------------
// Calcul du nombre d'occurrences de chque pattern (stat suffisantes
// pour le calcul de vraisemblance et des estimateurs de max. de
// vraisemblance 2pts)
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja alloue des 
// stats suffisantes
// Valeur de retour : aucune, effet de bord sur ss
//-----------------------------------------------------------------------------
void BJM_GE::Prepare2pt(int m1, int m2, int *ss) const
{
	BJgauche->Prepare2pt(m1,m2,ss);
	BJdroite->Prepare2pt(m1,m2,ss);
}
//-----------------------------------------------------------------------------
// Calcul de l estimateur 2pt de max de vraisemblance
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des 
// stats suffisantes, le taux de cassure et de retention a estimer
// Valeur de retour : aucune, effet de bord sur les deux parametres
//-----------------------------------------------------------------------------
void BJM_GE::Estimate2pt(double *par, int *ss) const
{
	BJgauche->Estimate2pt(par,ss);
}
//-----------------------------------------------------------------------------
// Calcul de la vraisemblance 2pts
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des 
// stats suffisantes, le taux de cassure et de retention.
// Valeur de retour : le loglike
//-----------------------------------------------------------------------------
double BJM_GE::LogLike2pt(double *par, int *ss) const
{
	return BJgauche->LogLike2pt(par, ss);
}
//-----------------------------------------------------------------------------
// Calcule fraction de rec. et
// LOD scores pour deux marqueurs
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux numeros de marqueurs, on doit avoir m1< m2
// - epsilon
// - la fraction de reconbinant(i,o)
// Valeur de retour : le lod
//-----------------------------------------------------------------------------

double BJM_GE::ComputeOneTwoPoints(int m1, 
		int m2, 
		double epsilon,
		double *fr) const
{
	if (HasRH()) {
		int n[4];
		int tot;
		double par[2];
		double loglike;

		n[0] = n[1] = n[2] = n[3] = 0;
		Prepare2pt(m1,m2,n);
		tot = n[0]+n[1]+n[2]+n[3];

		Estimate2pt(par,n);

		*fr = par[0];
		loglike = LogLike2pt(par,n);
		par[0] = 1.0;

		return loglike-LogLike2pt(par,n);
	}

	int data =0; // pairs of non unknown
	double ENRec,LogLike,LogLikeInd, PrevLogLike,theta;

	LogLikeInd = LogInd(m1, m2, &data);

	if (!data) {
		*fr = Em_Max_Theta;
		return 0.0;
	}

	theta = 0.05;
	LogLike = -1e100;

	do {
		PrevLogLike = LogLike;

		ENRec = EspRec(m1, m2, theta, &LogLike);

		theta = ENRec/(data); 

		theta = (theta > Em_Max_Theta) ? Em_Max_Theta : 
			((theta < Em_Min_Theta) ? Em_Min_Theta : theta);
	}   while (fabs(LogLike-PrevLogLike) > epsilon);


	*fr = theta;
	return(LogLike - LogLikeInd);
}

//-----------------------------------------------------------------------------
// Remplissage des matrices
//-----------------------------------------------------------------------------

void BJM_GE::ComputeTwoPoints(void)
{
	// DL
#if 0
	double fr;
	double lod;
	int i = 0, k = 0;
	int j = 0, l = 0;

	while (i < TailleCharn) 
	{
		if (IndMarq[k]) 
		{
			i++;
			j = i + 1;
			l = k + 1;
			while (j <= TailleCharn) 
			{
				if (IndMarq[l]) 
				{
					lod = ComputeOneTwoPoints(k, l, Epsilon2, &fr);
					TwoPointsLOD[IndMarq[k]][IndMarq[l]] = TwoPointsLOD[IndMarq[l]][IndMarq[k]] = lod;
					TwoPointsFR[IndMarq[k]][IndMarq[l]] = TwoPointsFR[IndMarq[l]][IndMarq[k]] = fr;
					TwoPointsDH[IndMarq[k]][IndMarq[l]] = TwoPointsDH[IndMarq[l]][IndMarq[k]] = 
						(HasRH() ? Theta2Ray(TwoPointsFR[j][i]) : Haldane(TwoPointsFR[j][i]));
					j++;
				}
				l++;
			}
		}	
		k++;
	}   
#endif
}


//-----------------------------------------------------------------------------
//  Etape d' � Expectation � de EM.
//-----------------------------------------------------------------------------
// Param�tres : la carte
// - la carte
// - le vecteur d'Expectation(i,o)
// Valeur de retour : le loglike 
//-----------------------------------------------------------------------------

double BJM_GE::ComputeExpected(const Carte* map, double *expected)
{

	double loglike_g , loglike_d;

	loglike_g = BJgauche->ComputeExpected(map, expected);

	loglike_d = BJdroite->ComputeExpected(map, expected);

	return (loglike_g + loglike_d);
}

//-----------------------------------------------------------------------------
//  Pr�paration des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJM_GE::PreparEM(const Carte *data)
{
	BJgauche->PreparEM(data);
	BJdroite->PreparEM(data);
}

//-----------------------------------------------------------------------------
//  nettoyage des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJM_GE::NetEM(const Carte *data)
{
	BJgauche->NetEM(data);
	BJdroite->NetEM(data);
}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les r�f�rences du couple
// - 
// Valeur de retour : la distance 2 points
//----------------------------------------------------------------------------

double BJM_GE::GetTwoPointsDH(int m1, int m2) const
{

	if (BJgauche->Couplex(m1,m2) &&
			BJdroite->Couplex(m1,m2))
		// DL
		return twopoint->getDH(m1, m2);
		/*
		return TwoPointsDH[IndMarq[m1 * ((Cartage->markers[m1].BitJeu 
					& BitJeu) > 0)]]
			[IndMarq[m2 * ((Cartage->markers[m2].BitJeu 
						& BitJeu) > 0)]];
		*/

	if (BJgauche->Couplex(m1,m2))
		return BJgauche->GetTwoPointsDH(m1, m2);

	if  (BJdroite->Couplex(m1,m2))
		return BJdroite->GetTwoPointsDH(m1, m2);

	// inherit the large distance of totally non informative pairs
	else return (BJgauche->GetTwoPointsDH(m1, m2)+
			BJdroite->GetTwoPointsDH(m1, m2))/2.0;
}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les r�f�rences du couple
// - 
// Valeur de retour : la fraction de recombinaison 2 points
//----------------------------------------------------------------------------

double BJM_GE::GetTwoPointsFR(int m1, int m2) const
{
	if (BJgauche->Couplex(m1,m2) &&
			BJdroite->Couplex(m1,m2))
		// DL
		return twopoint->getFR(m1, m2);
		/*
		return TwoPointsFR[IndMarq[m1 * ((Cartage->markers[m1].BitJeu 
					& BitJeu) > 0)]]
			[IndMarq[m2 * ((Cartage->markers[m2].BitJeu 
						& BitJeu) > 0)]];
		*/

	if (BJgauche->Couplex(m1,m2))
		return BJgauche->GetTwoPointsFR(m1, m2);

	if  (BJdroite->Couplex(m1,m2))
		return BJdroite->GetTwoPointsFR(m1, m2);

	else return Em_Max_Theta;

}
