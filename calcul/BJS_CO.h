//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_CO.h,v 1.17.2.1 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Prototype de la classe BJS_CO.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJS_CO_H
#define _BJS_CO_H

#include "BioJeuSingle.h"

#include "Constraints.h"

/** Classe des jeux de donn�es Contraintes*/
class BJS_CO : public BioJeuSingle {
 public:

  /** Liste des contraintes.
  */
  Constraint *Constraints;

  /** Constructeur. */
  BJS_CO();

  /** Constructeur.
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param id le num�ro du jeu.
      @param cross type de jeu de donn�e.
      @param nm nombre de marqueurs.
      @param taille le nombre de contraintes
      @param bitjeu champ de bit du jeu de donn�es.
      @param cons la liste des contraintes
  */
  BJS_CO(CartaGenePtr cartag,
	 int id,
	 charPtr nomjeu,
	 CrossType cross,	 
	 int nm, 
	 int taille,
	 int bitjeu,
	 Constraint *cons);
  
  /** Destructeur. */
  ~BJS_CO();

  /** Retourne un champ des jeux fusionn�s sur l'ordre(!=CON).
      @return un champ de bits
  */
  inline int GetBJMO() const
    {
      return 0;
    };

  /** Compatibilite entre deux echantillons d'un marqueur
      @param numarq1 le numero du marqueur 1
      @param numarq2 le numero du marqueur 2
  */
  int Compatible(int numarq1, int numarq2) const;

  /** Fusion de deux marqueurs pour la creation d'un haplotype. Le
      marqueur 1 est utilise pour stocker le resultat.
      @param numarq1 marqueur 1 
      @param numarq2 marqueur 2 
      @return nothing.  
  */
  void Merge(int marq1,int marq2) const;


  /** Affichage des contraintes*/
  void DumpEch(void) const;

  
  /** Test l'existance d'un couple(pas de sens)
      @param m1 marqueur 1
      @param m2 marqueur 2
      @return 0
  */
  int Couplex(int m1, int m2) const;

  
  /** m�thode qui ne fait rien pour la coh�rence!
    */
  void ComputeTwoPoints(void) {};

  /** m�thode qui ne fait rien pour la coh�rence!
  */
  virtual void DumpEchMarq(int numarq) const {};
  
  /** m�thode qui ne fait rien pour la coh�rence!
  */
  double GetTwoPointsLOD(int m1, int m2) const {return 0.0;};

  /** m�thode qui ne fait rien pour la coh�rence!
    */
  virtual void DumpTwoPointsLOD() const {};

    /** Calcul du maximum de vraisemblance par EM.     
      @param data la carte
      @return le maximum de vraissemblance
  */
  double ComputeEM(Carte *data);  

  /** Calcul de la vraisemblance maximum par EM.
      Une interface plus sophistiqu�e du calcul de la vraisemblance par EM:
      On converge jusqu'� epsilon1. Si la vraisemblance est inf�rieure �
      threshold on laisse tomber, sinon on converge � epsilon2.
      @param data la carte
      @param threshold niveau a partir duquel on convergera a epsilon2
      @return le maximum de vraissemblance
  */
  double ComputeEMS (Carte *data, 
		     double threshold);
  
  /** Affichage d�taill� d'une carte
      @param data la carte
      @param envers � l'envers ou � l'endroit
  */
  void PrintDMap(Carte *data, int envers, Carte *dataref);

  /** Matrice 2p des distances 5bidon)
      @param unit l'unit� de distance: 
      'k' = kosambi ou centiray, 
      'h' = haldane ou centiray.
  */
  inline void PrintTwoPointsDist(const char unit[2]) const {};
  
 private:

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  virtual double LogInd(int m1, int m2, int *nbdata) const {
    print_out("Bug, m�thode interdite = LogInd!\n");
    return 0.0;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  virtual double EspRec(int m1, int m2, double theta, double *loglike) const {
    print_out("Bug, m�thode interdite = EspRec!\n");
    return 0.0; 
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double GetTwoPointsDH(int m1, int m2) const {
    print_out("Bug, m�thode interdite = GetTwoPointsDH!\n");
    return 0.0; 
  };
  
  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double GetTwoPointsFR(int m1, int m2) const {
    return 0.0;
  };
  
  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double ComputeExpected(const Carte *data, double *expected) {
    print_out("Bug, m�thode interdite = ComputeExpected!\n");
    return 0.0;
  };

  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  void Prepare2pt(int m1, int m2,  int *ss) const {
    print_out("Bug, m�thode interdite = Prepare2pt!\n");
    return;
  };

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = Estimate2pt!\n");
    return;
  };

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  double LogLike2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = LogLike2pt!\n");
    return 0.0;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  virtual double ComputeOneTwoPoints(int m1, int m2, double epsilon, double *fr) const {
    print_out("Bug, m�thode interdite = ComputeOneTwoPoints!\n");
    return 0.0;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void PreparEM(const Carte *data) {
    print_out("Bug, m�thode interdite = PreparEM!\n");
  };
  
  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void NetEM(const Carte *data) {
    print_out("Bug, m�thode interdite = NetEM!\n");
  };
  
  /** passage � priv�  = choix d'impl�mentation. */  
  int Maximize(Carte *data, double *expected) const {
    print_out("Bug, m�thode interdite = Maximize!\n");
    return -1;
  };

};

#endif /* _BJS_CO_H */
