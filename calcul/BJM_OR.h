//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJM_OR.h,v 1.25.2.3 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Prototype de la classe BJM_OR.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJM_OR_H
#define _BJM_OR_H

#include "BioJeuMerged.h"


/** Classe de la fusion sur l'ordre. */  
class BJM_OR : public BioJeuMerged {
 public:
  
  /** Constructeur. */
  BJM_OR();
  
  
  /** Constructeur
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param id le num�ro du jeu.
      @param cross type de jeu de donn�e(Mor).
      @param nm nombre de marqueurs.
      @param bitjeu champ de bit du jeu de donn�es.
      @param gauche jeu de donn�es.
      @param droite jeu de donn�es. 
  */
  BJM_OR(CartaGenePtr cartag,
	 int id,
	 CrossType cross,
	 int nm, 
	 int bitjeu,
	 BioJeu *gauche, 
	 BioJeu *droite);
  
  /** Destructeur. */
  virtual ~BJM_OR();

  /** Retourne un champ des jeux fusionn�s sur l'ordre(!=CON).
      @return un champ de bits
  */
  inline int GetBJMO() const
    {
      return (BJgauche->GetBJMO() | BJdroite->GetBJMO());
    };
  
  /** Calcul du maximum de vraisemblance par EM.
      A partir de la carte re�ue, sont compos�es les deux cartes propres aux deux jeux de donn�es fusionn�s. Le maximum de vraisemblance respectif est calcul�. Le r�sultat est la somme des deux valeurs intem�diaires.
      @param data la carte
      @return le maximum de vraissemblance
  */
  double ComputeEM(Carte *data);


  /** Calcul de la vraisemblance maximum par EM.
      Une interface plus sophistiqu�e du calcul de la vraisemblance par EM:
      On converge jusqu'� epsilon1. Si la vraisemblance est inf�rieure �
      threshold on laisse tomber, sinon on converge � epsilon2.
      @param data la carte
      @param threshold niveau a partir duquel on convergera a epsilon2
      @return le maximum de vraissemblance
  */
  double ComputeEMS (Carte *data, 
		     double threshold);
  
  /** Calcul, affichage et stockage des groupes.
      On �tablit un graphe dont les noeuds sont les marqueurs. Il existe une arr�te entre deux marqueurs, si le critere de lod > seuil est v�rifi�. Puis on extrait les composantes connexes qui sont �quivalentes aux groupes.
      @param disthres PAS UTILISE.
      @param lodthres le seuil de LOD 2pt.
      @return le nombre de groupes.
    */
  int Groupe(double disthres, double lodthres) const;

  /** Affichage simple d'une carte
      @param data la carte
  */
  void PrintMap(Carte *data) const;

  /** retourne une liste contenant une carte
      @param unit l'unit� de distance: 
      'k' = kosambi ou centiray, 
      'h' = haldane ou centiray.
      @param data la carte
      @return une liste
  */
  char *** GetMap(const char unit[2], Carte *data) const;

  /** Affichage d�taill� d'une carte
      @param data la carte
      @param envers � l'envers ou � l'endroit
      @param dataref la carte de r�f�rence
  */
  void PrintDMap(Carte *data, int envers, Carte *dataref);

  /** Matrice 2p des distances 5bidon)
      @param unit l'unit� de distance: 
      'k' = kosambi ou centiray, 
      'h' = haldane ou centiray.
  */
  void PrintTwoPointsDist(const char unit[2]) const {
    BJgauche->PrintTwoPointsDist(unit);
    BJdroite->PrintTwoPointsDist(unit);
  };
    
  void InitContribLogLike2pt() {BJgauche->InitContribLogLike2pt(); BJdroite->InitContribLogLike2pt();}
  double ContribLogLike2pt(int m1) {return BJgauche->ContribLogLike2pt(m1) + BJdroite->ContribLogLike2pt(m1);}
  double ContribLogLike2pt(int m1, int m2) {return BJgauche->ContribLogLike2pt(m1,m2) + BJdroite->ContribLogLike2pt(m1,m2);} 
  double NormContribLogLike2pt(int m1) {return BJgauche->NormContribLogLike2pt(m1) + BJdroite->NormContribLogLike2pt(m1);}
  double NormContribLogLike2pt(int m1, int m2) {return BJgauche->NormContribLogLike2pt(m1,m2) + BJdroite->NormContribLogLike2pt(m1,m2);} 
  double ContribOCB(int m1, int m2) {return BJgauche->ContribOCB(m1,m2) + BJdroite->ContribOCB(m1,m2);} 
  double NormContribOCB(int m1, int m2) {return BJgauche->NormContribOCB(m1,m2) + BJdroite->NormContribOCB(m1,m2);} 

 private:

  /** Calcul des valeurs de points et remplissage des matrices.
      En l'occurence, il s'agit uniquement des LOD 2pts des couples de marqueurs pr�sents dans les deux jeux fusionn�s.
  */
  void ComputeTwoPoints(void);

  /** Crit�re de linkage entre 2 marqueurs.
      @param disthres PAS UTILISE.
      @param lodthres le seuil de LOD 2pt.
      @param k le num�ro du premier marqueur
      @param l le num�ro du second marqueur
      @return crit�re v�rifi� ou pas.
  */
  inline int GroupTest(double disthres, double lodthres, int k, int l) const 
    {
      return (GetTwoPointsLOD(k, l) >= lodthres);
    }
  ;
  
  /** affichage du seuil de LOD
      @param disthres PAS UTILISE.
      @param lodthres le seuil de LOD 2pt.
  */
  virtual void GroupMess(double disthres, double lodthres) const
    {
      print_out("\nLinkage Groups :\n");
      print_out("---------------:\n");
      print_out("LOD threshold=%.2f\n", lodthres);
    }
  ;
    
  /** passage � priv� = choix d'impl�mentation. 
      REMARQUE : Cela signifie que cette caract�ristique  n'est pas logique dans cette classe,
      mais que pour des raisons d'impl�mentation, elle figure n�anmoins dans cette classe. Elle n'est donc pas document�e. 
  */
  int NbMeiose;

  /** passage � priv� = choix d'impl�mentation. */
  doublePtr *TwoPointsFR;

  /** passage � priv� = choix d'impl�mentation. */
  doublePtr *TwoPointsDH;

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double GetTwoPointsDH(int m1, int m2) const;
  //const {
  //  sprintf(bouf,"Bug, m�thode interdite = GetTwoPointsDH!!!\n");
  //  pout(bouf);
  //  return 0.0; 
  //};
  
  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double GetTwoPointsFR(int m1, int m2) const {
    double fr = BJgauche->GetTwoPointsFR(m1, m2);
    fr += BJdroite->GetTwoPointsFR(m1, m2);
    if (fr != 0.0) return fr;
    else return Em_Max_Theta;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double ComputeExpected(const Carte *data, double *expected) {
    print_out("Bug, m�thode interdite = ComputeExpected!\n");
    return 0.0;};

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void PreparEM(const Carte *data) {
    print_out("Bug, m�thode interdite = PreparEM!\n");
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void NetEM(const Carte *data) {
    print_out("Bug, m�thode interdite = NetEM!\n");
  };

  /** passage � priv�  = choix d'impl�mentation. */  
  int Maximize(Carte *data, double *expected) const {
    print_out("Bug, m�thode interdite = Maximize!\n");
    return -1;
  };
	
   protected:


  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  void Prepare2pt(int m1, int m2,  int *ss) const {
    print_out("Bug, m�thode interdite = Prepare2pt!\n");
    return;
  };

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = Estimate2pt!\n");
    return;
  };

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  double LogLike2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = LogLike2pt!\n");
    return 0.0;
  };


  /** Calcul du LOD score et de fraction de recombinants 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param epsilon le seuil de convergence
      @param fr la fraction de recombinants(i,o)
      @return le LOD
  */
  double ComputeOneTwoPoints(int m1, 
                             int m2, 
                             double epsilon,
                             double *fr) const;

  /** Acc�s � une valeur LOD score 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @return le lod
  */
  double GetTwoPointsLOD(int m1, int m2) const;
  
		     
};

#endif /* _BJM_OR_H */
