//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_OR.cc,v 1.17.2.4 2012-08-02 15:43:54 dleroux Exp $
//
// Description : BJS_OR.
// Divers :
//-----------------------------------------------------------------------------

#include "BJS_OR.h"

#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#ifdef ppc
#include "/Developer/Headers/FlatCarbon/fp.h"
#endif

#include "NOrCompMulti.h"

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_OR::BJS_OR() : BioJeuSingle()
{
  NbMeiose = 0;
  Lambda = 1;
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - le nom du jeu
// - le nombre de marqueurs
// - le champ de bits
// - le tableau d'indices des marqueurs
// - le poids d'un break-point
// Valeur de retour :
//-----------------------------------------------------------------------------

#include <iostream>

BJS_OR::BJS_OR(CartaGenePtr cartag,
	       int numjeu,
	       CrossType cross,
	       charPtr nomjeu,
	       int nbm,
	       int bitjeu,
	       int *indmarq,
	       double coeff,
	       int *chrom,
	       int *chromex,
	       int *position)
{
  Cartage = cartag;
  Id = numjeu;

  NomJeu = new char[strlen(nomjeu)+1];
  strcpy(NomJeu, nomjeu);

  Cross = cross;
  NbMarqueur = nbm;
  Poids = coeff;
  Lambda = 1;
  BitJeu = bitjeu;

  NbMeiose = TailleEchant = 0;

	// DL
	/*
    printf("%s ; ordre des marqueurs = [", nomjeu);
	for(int z=0;z<=cartag->NbMarqueur;z++) {
		printf(" %i", indmarq[z]);
	}
	printf(" ]\n");
    //*/
  _indmarq_sz = Cartage->NbMarqueur+1;
  IndMarq_bj2cg = indmarq;
  IndMarq_cg2bj = new int[Cartage->NbMarqueur+1];
  for(int i=0;i<=Cartage->NbMarqueur;++i) {
      IndMarq_cg2bj[i]=0;
  }
  for(int i=1;i<=NbMarqueur;++i) {
    //std::cout << "IndMarq_cg2bj[" << indmarq[i] << "] = " << i << std::endl;
    IndMarq_cg2bj[indmarq[i]] = i;
  }
  IndMarq_bj2cg[0]=0;
  IndMarq_cg2bj[0]=0;
  Chromosome = chrom;
  ChromEx = chromex;
  Position = position;

  Echantillon = NULL; 
  // DL
  //TwoPointsFR = NULL; 
  //TwoPointsDH = NULL; 
  //TwoPointsLOD = NULL; 
  

}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_OR::~BJS_OR()
{
  /*delete [] IndMarq_cg2bj;*/
  /*delete [] IndMarq_bj2cg;*/
  delete [] Chromosome;
  delete [] ChromEx;
  delete [] Position;
}

//-----------------------------------------------------------------------------
// Affichage des chromosomes
//-----------------------------------------------------------------------------

void BJS_OR::DumpEch() const
{
  print_out( "%10s %10s\n", "Beginning", "End");

  int i=1;
  for (i=1;i<=NbMarqueur;i++)
    {
      if (Chromosome[i] != 0)
	{
	  print_out( "%10s %10s\n", 
		  Cartage->markers.keyOf(i).c_str(),
		  Cartage->markers.keyOf(Chromosome[i]).c_str());
	}
    }
}

//-----------------------------------------------------------------------------
// returns the number of chromosomes the loci from the current 
//      selection belongs to
//      and for each chromosome the number of loci involved
//-----------------------------------------------------------------------------

int BJS_OR::GetnLocipChrom(int **cvn)
{
  int i;

  int * tmp  = new int[MAXNBCHROM];
  for (i=0;i<MAXNBCHROM;i++)
    tmp[i] = 0;

  for (int j = 0; j < Cartage->NbMS; j++) {
    if (IndMarq_cg2bj[Cartage->MarkSelect[j]] != 0) {
      tmp[Chromosome[IndMarq_cg2bj[Cartage->MarkSelect[j]]]]++;
    }
  }

  int nbc = 0;
  
  for (i=0;i<MAXNBCHROM;i++) 
    if (tmp[i]) tmp[nbc++] = tmp[i]; 	 
  
  *cvn=tmp;
  
  return nbc;
  
}

//-----------------------------------------------------------------------------
// Contribution des chromosomes � la vraisemblance : break-points
//-----------------------------------------------------------------------------

double BJS_OR::ComputeEM(Carte *Lacarte)
{
  int nc = 0;
  /** crit�re lin�aire */
  if (Poids>=0) return -(Poids*BreakPointsMap(Lacarte));

  /** crit�re avec loi de Poisson */
  /*int i, k;
  double proba;
  k = BreakPointsMap(Lacarte);
  //printf("k = %d\n", k);
  proba = pow(Poids, k) * exp(-Poids) / Fact(k);
  //printf("proba = %lf\nlog(proba) = %lf\n", proba, log(proba));
  return log10(proba);*/

  /** crit�re bayesien avec calcul du nombre de permutations et de la loi de Poisson */
  if (Cartage->NOrComput == NULL) {
    int ok = 0;
    int i = 1;
    for (i=1; i<=Cartage->NbJeu; i++) {
      if (Cartage->Jeu[i]->Cross == Ordre) {
	ok = 1;
	break;
      }
    }
    if (!ok) {
      print_err("Error: there is no reference order data set loaded! The current data set should be merged by order from one Biological Data Set and one reference order data set.\n");
      return 0.;
    }
    int *vc;
    nc =  ((BJS_OR*)(Cartage->Jeu[i]))->GetnLocipChrom(&vc);
    Cartage->NOrComput = new NOrCompMulti(Cartage->NbMS-1, vc, nc);
    Cartage->NOrComput->run();
  }

  int bp = BreakPointsMap(Lacarte);
  double rhII = - (double)log10l(Cartage->NOrComput->getNO(bp)/2);
  // Autre Prior: uniforme sur 0->(NbMarqueur-1).
  if (Lambda < 0) {
    int bpmax=NbMarqueur-nc+1;
#ifndef __WIN32__
    int l = abs(int(Lambda));
#else
    int l = int(Lambda);
    if(l<0) l=-l;
#endif
    if (bp-nc > l) {
      rhII += (double) log10l(0.01/(bpmax-l));
    }
    else {
      rhII += (double) log10l(0.99/(l+1));
    }
  }
  else {
    rhII += (double)log10l(expl((long double) -Lambda) * powl((long double) Lambda,(long double) bp) / dfact(bp));
  }
  return rhII;
}

//------------------------------------------------------------------------------
// Contribution des chromosomes � la vraisemblance : break-points
//------------------------------------------------------------------------------

double BJS_OR::ComputeEMS (Carte *data, 
			   double threshold)
{
  return ComputeEM(data);
}

//------------------------------------------------------------------------------
// Retourne le nombre de break-points avec un jeu
//------------------------------------------------------------------------------
// Param�tres :
// - indice du jeu
// Valeur de retour : int nombre de BP
//------------------------------------------------------------------------------

int BJS_OR::BreakPoints(BioJeu *jeu)
{
  int i, j, trouve, trouve2, trouve3, prec, suiv, marqprec = -1, marqsuiv, BP = 0;

  /** le marqueur en cours de traitement : GetMarq(i)
      son indice dans le jeu : i , son indice dans Cartage : GetMarq(i)*/
  
  for (i=2;i<=NbMarqueur;i++) {

    prec = 0;
    suiv = 0;
   
    // on impose que le marqueur appartienne aussi au jeu id, sinon on ne fait rien
    if ((Cartage->markers[GetMarq(i)].BitJeu & jeu->BitJeu)!= 0) {
      
      /** on cherche le premier marqueur pr�c�dent du jeu qui 
	  appartienne aux deux jeux */
      
      trouve = 0;
      trouve2 = 0;
      trouve3 = 0;

      for (j=i-1;j>=1;j--) {
	if ((trouve = ((Cartage->markers[GetMarq(j)].BitJeu & jeu->BitJeu)!= 0))) break;
      }
      // a ce niveau GetMarq(j) est le marqueur pr�c�dent commun
      // son indice dans le jeu : j 
      // la position du marqueur dans le jeu id : jeu->GetMarqPos(GetMarq(i))

      if (trouve == 1 /*&& GetMarqPos(GetMarq(j)) != 0*/) {

	/** on cherche le premier marqueur en arri�re ou en avant du jeu id qui 
	    appartienne aux deux jeux */
	
	// recherche en arri�re
	for (prec=jeu->GetMarqPos(GetMarq(i)) - 1;prec>=0;prec--) {
	  if (prec == 0) break;
	  marqprec = jeu->GetMarq(prec);
	  if ((Cartage->markers[marqprec].BitJeu & BitJeu)!= 0) {
	    trouve2 = 1;
	    break;
	  }
	}
	
	// recherche en avant
	for (suiv = jeu->GetMarqPos(GetMarq(i)) + 1;suiv<=jeu->NbMarqueur;suiv++) { 
	  if (suiv > jeu->NbMarqueur) break;
	  marqsuiv = jeu->GetMarq(suiv);
	  if ((trouve3 = ((Cartage->markers[marqsuiv].BitJeu & BitJeu)!=0))) break;
	}
    /*std::cout << "trouve=" << trouve << " trouve2=" << trouve2 << " trouve3=" << trouve3 << std::endl;*/
    /*std::cout << "i="<<i << " j="<<j << " marqprec="<<marqprec << " marqsuiv="<<marqsuiv << std::endl;*/
	
	// comparaison
	if (trouve2 == 1 && trouve3 == 1) {
	  if (Cartage->markers.keyOf(GetMarq(j)) != Cartage->markers.keyOf(marqprec) && GetMarq(j) != marqsuiv) {
	  /*if (GetMarq(j) != marqprec && GetMarq(j) != marqsuiv) {*/
	    BP++;      
	    //	    printf("BP : %s/%s ou %s/%s\n",Cartage->markers.keyOf(GetMarq(j)).c_str(),Cartage->markers.keyOf(marqprec).c_str(), Cartage->markers.keyOf(GetMarq(j)).c_str(), Cartage->markers.keyOf(marqsuiv).c_str());
	  } 
	  //else if (ChromEx[j] != 0 && ChromEx[i] != 0 && ChromEx[j] != ChromEx[i]) BP++;
	}
	else if (trouve2 == 1 && trouve3 == 0) {
	  if (GetMarq(j) != marqprec) {
	    BP++;
	    //	    printf("BP arriere: %s/%s\n", Cartage->markers.keyOf(GetMarq(j)).c_str(), Cartage->markers.keyOf(marqprec).c_str());
	  }
	  //else if (ChromEx[j] != 0 && ChromEx[i] != 0 && ChromEx[j] != ChromEx[i]) BP++;
	}
	else if (trouve2 == 0 && trouve3 == 1) {
	  if (GetMarq(j) != marqsuiv) {
	    BP++;
	    //	    printf("BP avant : %s/%s\n", Cartage->markers.keyOf(GetMarq(j)).c_str(), Cartage->markers.keyOf(marqsuiv).c_str());
	  }
	  //else if (ChromEx[j] != 0 && ChromEx[i] != 0 && ChromEx[j] != ChromEx[i]) BP++;
	}
      }
    }
  }
  //printf("Nombre de BP : %d\n", BP);
  return BP;        
}



// attention : pour l'instant ne marche pas si extremit�s de chrom absentes de la carte
//------------------------------------------------------------------------------
// Break-points between a map and the given order
//------------------------------------------------------------------------------
// Parameters :
// - a map
// Return value : int number of break-points
//------------------------------------------------------------------------------

int BJS_OR::BreakPointsMap(Carte *map)
{
  int i, j, k, l, trouve, trouve2, trouve3, prec, marqprec, marqsuiv, BP = 0;

  /** le marqueur en cours de traitement : GetMarq(i), marq dans la carte 
      son indice dans le jeu : i, j dans la carte
  */
  
  for (i=2;i<=NbMarqueur;i++) {
    
    prec = 0;
//  printf("je traite le marqueur %s\n", Cartage->markers.keyOf(GetMarq(i)).c_str());

    j = 0;
    while (j < map->NbMarqueur && map->ordre[j] != GetMarq(i)) j++;
    
    // on impose que le marqueur appartienne aussi � l'ordre, sinon on ne fait rien
    if (j < map->NbMarqueur) {
//      marq = map->ordre[j];
                    
      /** on cherche le premier marqueur pr�c�dent du jeu qui 
	  appartienne aussi � la carte */
      
      trouve = 0;
      trouve2 = 0;
      trouve3 = 0;
      
      for (prec=i-1;prec>0;prec--) {
	k = 0;
	while (k<map->NbMarqueur && map->ordre[k] != GetMarq(prec)) k++;
	if (k<map->NbMarqueur) {
	  trouve = 1;
	  break;
	}
      }

      // a ce niveau GetMarq(prec) est le marqueur pr�c�dent commun
      // son indice dans le jeu : prec 

      if (trouve == 1) {

	/** on cherche le premier marqueur en arri�re ou en avant de la carte qui 
	    appartienne aussi � l'ordre */
	
	// recherche en arri�re
	marqprec = 0;
	if (j>0) {
	  k = j - 1;
	  while (k>=0 && (Cartage->markers[map->ordre[k]].BitJeu & BitJeu) == 0) k--;
	  if (k>=0 && k<map->NbMarqueur) {
	    trouve2 = 1;
	    marqprec = map->ordre[k];
	  }
	}
	// recherche en avant
	marqsuiv = 0;
	if (j<map->NbMarqueur-1) {
	  l = j + 1; 
	  while (l<map->NbMarqueur && (Cartage->markers[map->ordre[l]].BitJeu & BitJeu) == 0) {
	    l++;
	  }
	  if (l<map->NbMarqueur) {
	    trouve3 = 1;
	    marqsuiv = map->ordre[l];
	  }
	}
       		
	// comparaison
	if (trouve2 == 1 && trouve3 == 1 && marqsuiv != 0 && marqprec != 0) {
	  if (GetMarq(prec) != marqprec && GetMarq(prec) != marqsuiv) {
	    BP++;      
//	    printf("BP : %s/%s ou %s/%s\n",Cartage->markers.keyOf(GetMarq(prec)).c_str(),Cartage->markers.keyOf(marqprec).c_str(), Cartage->markers.keyOf(GetMarq(prec)).c_str(), Cartage->markers.keyOf(marqsuiv).c_str());
	  } 
	  else if (ChromEx[prec] != 0 && ChromEx[i] != 0 && ChromEx[prec] != ChromEx[i]) BP++;
	}
	else if (trouve2 == 1 && trouve3 == 0 && marqprec != 0) {
	  if (GetMarq(prec) != marqprec) {
	    BP++;
//	    printf("BP arriere: %s/%s\n", Cartage->markers.keyOf(GetMarq(prec)).c_str(), Cartage->markers.keyOf(marqprec).c_str());
	  }
	  else if (ChromEx[prec] != 0 && ChromEx[i] != 0 && ChromEx[prec] != ChromEx[i]) BP++;
	}
	else if (trouve2 == 0 && trouve3 == 1 && marqsuiv != 0) {
	  if (GetMarq(prec) != marqsuiv) {
	    BP++;
//	    printf("BP avant : %s/%s\n", Cartage->markers.keyOf(GetMarq(prec)).c_str(), Cartage->markers.keyOf(marqsuiv).c_str());
	  }
	  else if (ChromEx[prec] != 0 && ChromEx[i] != 0 && ChromEx[prec] != ChromEx[i]) BP++;
	}
      }
    }
  }
//  printf("Nombre de BP : %d\n", BP);
  return BP;
}


//----------------------------------------------------------------------------------------
// Contribution �l�mentaire pour le TSP en termes de break-points
//----------------------------------------------------------------------------------------
// Param�tres : 
// - deux marqueurs
// Valeur de retour : 0 si pas de BP, sinon Poids
//-----------------------------------------------------------------------------------------

// not penalizing unknown cases
//  double BJS_OR::ContribLogLike2pt(int m1, int m2)
//  {
//    int m1pos, m2pos, BP = 0;
  
//    if ((Cartage->markers[m1].BitJeu & BitJeu) == 0 || (Cartage->markers[m2].BitJeu & BitJeu) == 0) return 0.0;
//    // on regarde s'il y a BP ou non
//    m1pos = GetMarqPos(m1);
//    if (m1pos == 0) return 0.0;
//    m2pos = GetMarqPos(m2);
//    if (m2pos == 0) return 0.0;
//    if (m2 != GetMarq(m1pos-1) && m2 != GetMarq(m1pos+1)) BP = 1;
//    else if (ChromEx[m1pos] != 0 && ChromEx[m2pos] != 0 && ChromEx[m1pos] != ChromEx[m2pos]) BP = 1;

//    if (BP == 0) return 0.0;
//    else {
//      //if (Chromosome[m1] != Chromosome[m2]) return 3*Poids;
//      /*else*/ return Poids; 
//    }
//  }

// penalizing unknown cases
//  double BJS_OR::ContribLogLike2pt(int m1, int m2)
//  {
//    int m1pos, m2pos, BP = 0;
  
//    if ((Cartage->markers[m1].BitJeu & BitJeu) == 0 || (Cartage->markers[m2].BitJeu & BitJeu) == 0) return Poids;
//    // on regarde s'il y a BP ou non
//    m1pos = GetMarqPos(m1);
//    if (m1pos == 0) return Poids;
//    m2pos = GetMarqPos(m2);
//    if (m2pos == 0) return Poids;

//    if (m2 != GetMarq(m1pos-1) && m2 != GetMarq(m1pos+1)) BP = 1;
//    else if (ChromEx[m1pos] != 0 && ChromEx[m2pos] != 0 && ChromEx[m1pos] != ChromEx[m2pos]) BP = 1;

//    if (BP == 0) return 0.0;
//    else return Poids; 
//  }

// three cases: 
//  - if it is a breakpoint, penalizes by Poids
//  - if it is an unknown case (unknown marker or absent on this order data file), 
//    penalizes by UNKNOWNCOST * Poids
//  - if it is not a breakpoint, do nothing
#define UNKNOWNCOST 1.
double BJS_OR::ContribLogLike2pt(int m1, int m2)
{
  int m1pos, m2pos, BP = 0;
  
  if ((Cartage->markers[m1].BitJeu & BitJeu) == 0 || (Cartage->markers[m2].BitJeu & BitJeu) == 0) return (UNKNOWNCOST * fabs(Poids));
  // on regarde s'il y a BP ou non
  m1pos = GetMarqPos(m1);
  if (m1pos == 0) return (UNKNOWNCOST * fabs(Poids));
  m2pos = GetMarqPos(m2);
  if (m2pos == 0) return (UNKNOWNCOST * fabs(Poids));

  // not in consecutive order in the reference order (may-be on the same chromosome or not)
  if (m2 != GetMarq(m1pos-1) && m2 != GetMarq(m1pos+1)) BP = 1;
  // on different chromosomes in the reference order
  else if (ChromEx[m1pos] != 0 && ChromEx[m2pos] != 0 && ChromEx[m1pos] != ChromEx[m2pos]) BP = 1;

  if (BP == 0) return 0.;
  else return fabs(Poids); 
}
//-----------------------------------------------------------------------------
// retourne une carte sous forme de liste
//-----------------------------------------------------------------------------
// Param�tres : 
// - l'unit� de distance
// - la carte
// Valeur de retour :
//----------------------------------------------------------------------------
  
char*** BJS_OR::GetMap(const char unit[2],
		       Carte *data) const
{
  
  char* temp;
  int i, j = 0;
  double dist = 0.0;
  int* refo = new int[NbMarqueur + 1];
  
  char *** resultg = new char**[2];
  resultg[1] = NULL;
  
  char ** result = new char*[(3 * data->NbMarqueur) + 3];
  
  result[(3 * data->NbMarqueur) + 2] = NULL;
  
  resultg[0] = result;

  temp = new char[8];
  sprintf(temp, 
 	  "%d", 
 	  Id);
  result[0] = temp;

  temp = new char[16];
  sprintf(temp, 
 	  "%.2f", 
 	  data->coutEM);
  result[1] = temp;
  
  j = 2;

  // to get the reference order
  for (i = 0; i <= NbMarqueur; i++)
    refo[i] = 0;
  for (i = 0; i < data->NbMarqueur; i++)
    refo[IndMarq_cg2bj[data->ordre[i]]] = data->ordre[i];
  
  
  for (i = 0; i < NbMarqueur; i++)  
    {
      if (refo[i+1] != 0) {
	//temp = new char[Cartage->markers.keyOf(data->ordre[i]).size()+ 1];
	temp = new char[Cartage->markers.keyOf(refo[i+1]).size()+ 1];
	//sprintf(temp,  
	//Cartage->markers.keyOf(data->ordre[i]).c_str());
	/*sprintf(temp,  */
	strcpy(temp,
		Cartage->markers.keyOf(refo[i+1]).c_str());
	result[j++] = temp;
	temp = new char[16];
	
	//if (i >= 1)
	  // simple counter
	//  dist = dist + 1;
	//else
	//  dist = 0.0;
	dist = Position[i+1];
	sprintf(temp, "%.1f", dist);
	result[j++] = temp;
	
	temp = new char[8];
	sprintf(temp, 
		"%d", 
		Chromosome[i+1]);
	result[j++] = temp;
	
      }
    }
  
  delete [] refo;

  return resultg;
}
