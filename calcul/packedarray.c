#ifdef __cplusplus
extern "C" {
#endif
#include <assert.h>
#include <stdlib.h>
/*#include <stdio.h>*/
#include <string.h>
#include "packedarray.h"

#ifdef __TEST__PA__
word_t pa_hash_shift = (sizeof(word_t)==4?17:27);
#else
#define pa_hash_shift (sizeof(word_t)==4?17:27)
#endif


void pa_delete(packed_array_t pa) {
        if(!pa) {
                return;
        }
        if(pa->data) {
                free(pa->data);
        }
        free(pa);
}

void pa_clear(packed_array_t pa) {
        assert(pa->data);
        memset(pa->data, 0, (pa->bitsperitem*(pa->maxitems-1)+BITSPERWORD-1)/8);
}

void pa_init(packed_array_t pa, word_t bitsperitem, word_t maxitems) {
        assert(bitsperitem<BITSPERWORD);
        pa->bitsperitem = bitsperitem;
        pa->maxitems = maxitems;
        /*printf("new packed_array : alloc'ing %u bytes to store %u-1 elements.\n",*/
                        /*(bitsperitem*maxitems+BITSPERWORD-1)/8,*/
                        /*maxitems);*/
        pa->data = (word_t*) malloc((bitsperitem*(maxitems-1)+BITSPERWORD-1)/8);
        pa_clear(pa);
}


packed_array_t pa_new(word_t maxitems) {
        packed_array_t pa = (packed_array_t) malloc(sizeof(struct packed_array_));
        word_t bitsperitem = bitsize(maxitems-1);
	pa_init(pa, bitsperitem, maxitems);
        return pa;
}


packed_array_t pa_multi_new(word_t maxitemsperarray, word_t numberofarrays) {
        packed_array_t pa = (packed_array_t) malloc(sizeof(struct packed_array_)*numberofarrays);
        word_t bitsperitem = bitsize(maxitemsperarray-1);
	word_t i;
	for(i=0;i<numberofarrays;i+=1) {
		pa_init(&(pa[i]), bitsperitem, maxitemsperarray);
	}
        return pa;
}


void pa_multi_delete(packed_array_t mpa, word_t numberofarrays) {
	packed_array_t backup = mpa;
	for(;numberofarrays>0;numberofarrays-=1) {
		if(mpa->data) {
			free(mpa->data);
		}
		mpa+=1;
	}
	free(backup);
}


packed_array_t pa_multi_get(packed_array_t mpa, word_t index) {
	return &(mpa[index]);
}


word_t pa_multi_get_index(packed_array_t mpa, packed_array_t item) {
	return item-mpa;
}


word_t bitsize(word_t sup) {
        word_t mask=(word_t) (~0);
        word_t bits=sizeof(word_t)*8;
        do {
                mask>>=1;
                bits-=1;
        } while(mask&&(sup&mask)==sup);
        return bits+1;
}

void pa_pack(packed_array_t pa, const int* tour) {
        word_t i;
        word_t* ptr = pa->data;
        int shift=(int) (BITSPERWORD-pa->bitsperitem);
        int s1, s2;
        word_t T;
        assert(tour[0]==0);
        if(tour[1]<tour[pa->maxitems-1]) {
                for(i=1;i<pa->maxitems;i+=1) {
                        T = (word_t) tour[i];
                        if(shift<0) {
                                s1 = -shift;
                                s2 = BITSPERWORD+shift;
                                *ptr |= (T>>s1);
                                ptr += 1;
                                T &= ((1<<s1)-1);
                                *ptr |= (T<<s2);
                                shift = s2;
                        } else {
                                *ptr |= (T<<shift);
                        }
                        shift -= pa->bitsperitem;
                }
        } else {
                /* plain copy-pasta from above bloc. Simply revert direction of iteration. */
                for(i=pa->maxitems-1;i>0;i-=1) {
                        T = (word_t) tour[i];
                        if(shift<0) {
                                s1 = -shift;
                                s2 = BITSPERWORD+shift;
                                *ptr |= (T>>s1);
                                ptr += 1;
                                T &= ((1<<s1)-1);
                                *ptr |= (T<<s2);
                                shift = s2;
                        } else {
                                *ptr |= (T<<shift);
                        }
                        shift -= pa->bitsperitem;
                }
        }
}

void pa_unpack(const packed_array_t pa, int* tour) {
        word_t i;
        word_t* ptr = pa->data;
        int shift = (int) (BITSPERWORD-pa->bitsperitem);
        int s1, s2;
        word_t mask = ((1<<pa->bitsperitem)-1);
        word_t T;
        tour[0]=0;
        for(i=1;i<pa->maxitems;i+=1) {
                /*printf("unpack #%i with ptr=%p and shift=%i ... got ", i, ptr, shift);*/
                if(shift>=0) {
                        T = ((*ptr) >> shift)&mask;
                } else {
                        s1 = pa->bitsperitem+shift;
                        s2 = BITSPERWORD+shift;
                        T = ((*ptr) & ((1<<s1)-1))<<(-shift);
                        /*printf("(s1=%i s2=%i T=%u) ", s1, s2, T);*/
                        ptr += 1;
                        if(s2) {
                                T |= ((*ptr) >> s2);
                        }
                        shift = s2;
                }
                /*printf("%u\n", T);*/
                tour[i] = (int)T;
                shift -= pa->bitsperitem;
        }
}

unsigned int pa_hash(const packed_array_t pa) {
        word_t accum = *pa->data;
        word_t i = (((pa->maxitems-1)*pa->bitsperitem+BITSPERWORD-1)/BITSPERWORD)-1;
        word_t* ptr = pa->data+1;
        /*pa_hash_shift = 1+(pa->bitsperitem>>1);*/
        /*printf("about to hash, i=%u maxitem=%u, bitsperitem=%u\n", i, pa->maxitems, pa->bitsperitem);*/
        /*printf("ptr=%X accum=%x\n", accum, accum);*/
        for(;i>0;i-=1) {
                /*accum *= *ptr;*/
                accum = (accum << pa_hash_shift) | (accum >> (BITSPERWORD - pa_hash_shift));
                /*accum ^= *ptr;*/
                accum += *ptr;
                /*printf("ptr=%X accum=%x\n", *ptr, accum);*/
                ptr += 1;
        }
        return accum;
}

int pa_compare(const packed_array_t a, const packed_array_t b) {
        word_t i, N = (((a->maxitems-1)*a->bitsperitem+BITSPERWORD-1)/BITSPERWORD)-1;
        assert(a->bitsperitem==b->bitsperitem);
        if(a->maxitems!=b->maxitems) {
                return a->maxitems-b->maxitems;
        }
        for(i=0;i<N;i+=1) {
                if(a->data[i]!=b->data[i]) {
                        return a->data[i] - b->data[i];
                }
        }
        return 0;
}

void pa_copy(packed_array_t src, packed_array_t dest) {
        assert(src->bitsperitem == dest->bitsperitem);
        assert(src->maxitems == dest->maxitems);
        assert(src->data);
        assert(dest->data);
        memcpy(dest->data, src->data, (((src->maxitems-1)*src->bitsperitem+BITSPERWORD-1)/8));
}

#ifdef __TEST__PA__

#include <stdio.h>

#define _SWAP_(_a, _b) do { (_a) ^= (_b); (_b)^=(_a); (_a)^=(_b); } while(0)


int compare_tours(const int* t1, const int* t2, word_t N) {
        word_t i;
        int ret=0;
        for(i=0;i<N;i+=1) {
                if(t1[i]!=t2[i]) {
                        printf("discrepancy between before/after packing at index %i (was %i, now %i)\n", i, t1[i], t2[i]);
                        ret+=1;
                }
        }
        return ret;
}


void shuffle_tour(int* ret, int n) {
        int i, swap;
        for(i=2;i<n;i+=1) {
                swap = 1+(random()%(i-1));
                _SWAP_(ret[i], ret[swap]);
        }
}

int* gen_seq(int n) {
        int i;
        int* ret = (int*) malloc(n*sizeof(int));
        for(i=0;i<n;i+=1) {
                ret[i]=i;
        }
        shuffle_tour(ret, n);
        return ret;
}

void disp_seq(const char* name, int* seq, word_t len) {
        word_t i;
        fputs(name, stdout);
        fputs(" :", stdout);
        for(i=0;i<len;i+=1) {
                printf(" %i", seq[i]);
        }
        fputc('\n', stdout);
}

void disp_pa(const char* name, packed_array_t pa) {
        word_t n_words = ((pa->maxitems*pa->bitsperitem+BITSPERWORD-1)/BITSPERWORD);
        word_t i;
        fputs(name, stdout);
        fputs(" :", stdout);
        for(i=0;i<n_words;i+=1) {
                printf(" %x", pa->data[i]);
        }
        fputc('\n', stdout);
}


#define NITEMS 11

/* rewritten for ANSI C from http://www.cut-the-knot.org/do_you_know/AllPerm.shtml */
void getNext(int* tour, word_t N)
{
  int i = N - 1;

  while (tour[i-1] >= tour[i]) i = i-1;

  int j = N;

  while (tour[j-1] <= tour[i-1]) j = j-1;

// swap values at positions (i-1) and (j-1)
  _SWAP_(tour[i-1], tour[j-1]);

  i+=1; j = N;

  while (i < j)
  {
    _SWAP_(tour[i-1], tour[j-1]);
    i+=1;
    j-=1;
  }
}

int main(int argc, char**argv) {
        int result=0;

        packed_array_t a, b, c;
        int* tour1, * tour2, * tour3;
        word_t i;

        if(argc>1) {
                pa_hash_shift = atoi(argv[1]);
        }
        a = pa_new(NITEMS);

        tour1 = gen_seq(NITEMS);
        tour2 = gen_seq(NITEMS);
        tour3 = gen_seq(NITEMS);

        /*disp_seq("tour1", tour1, NITEMS);*/

        pa_pack(a, tour1);
        /*disp_pa("packed tour1", a);*/
        b = pa_new(NITEMS);
        pa_copy(a, b);
        /*disp_pa("packed tour1", a);*/
        pa_unpack(b, tour2);

        compare_tours(tour1, tour2, NITEMS);

        /*disp_seq("tour2", tour2, NITEMS);*/
        /*pa_pack(a, tour1);*/
        /*printf("hash(tour1) = %lx\n", pa_hash(a));*/
        /*pa_pack(a, tour2);*/
        /*printf("hash(tour2) = %lx\n", pa_hash(a));*/
        /*pa_pack(a, tour3);*/
        /*printf("hash(tour3) = %lx\n", pa_hash(a));*/

        for(i=0;i<NITEMS;i+=1) {
                tour1[i]=i;
        }
        for(i=0;tour1[0]==0&&i<1000000;i+=1) {
                /*disp_seq("permut", tour1, NITEMS);*/
                pa_clear(a);
                pa_pack(a, tour1);
                printf("%x\n", pa_hash(a));
                getNext(tour1, NITEMS);
        }

        /*for(i=0;i<10000;i+=1) {*/
                /*shuffle_tour(tour1, NITEMS);*/
                /*pa_clear(a);*/
                /*pa_pack(a, tour1);*/
                /*//disp_seq("T", tour1, NITEMS);*/
                /*printf("hash : %x ", pa_hash(a));*/
                /*disp_pa("", a);*/
        /*}*/

        pa_delete(a);
        pa_delete(b);
        free(tour1);
        free(tour2);
        free(tour3);

        return result;
}

#endif
#ifdef __cplusplus
}
#endif

