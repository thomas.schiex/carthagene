//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_BC.h,v 1.18.2.1 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Prototype de la classe BJS_BC.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJS_BC_H
#define _BJS_BC_H

#include "BioJeuSingle.h"

/** Classe des jeux de donn�es backcross. */
class BJS_BC : public BioJeuSingle {
 public:

  /** Constructeur. */
  BJS_BC();

  /** Constructeur.
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param id le num�ro du jeu.
      @param nomjeu nom du jeu
      @param cross type de jeu de donn�e.
      @param nm nombre de marqueurs.
      @param taille de l'�chantillon
      @param bitjeu champ de bit du jeu de donn�es.
      @param indmarq le vecteur d'indice de marqueur
      @param echantil la matrice des informations
    */
  BJS_BC(CartaGenePtr cartag,
	 int id,
	 charPtr nomjeu,
	 CrossType cross,
	 int nm, 
	 int taille,
	 int bitjeu,
	 int *indmarq,
	 Obs **echantil);
  
  /** Destructeur. */
  ~BJS_BC();

  /** Constructeur par recopie */
  BJS_BC(const BJS_BC& b);

  
  /** Compatibilite entre deux echantillons d'un marqueur
      @param numarq1 le numero du marqueur 1
      @param numarq2 le numero du marqueur 2
      @param numind le numero de l'individu
  */
  int Compatible(int numarq1, int numarq2) const;


  /** Fusion de deux marqueurs pour la creation d'un haplotype. Le
      marqueur 1 est utilise pour stocker le resultat.
      @param numarq1 marqueur 1 
      @param numarq2 marqueur 2 
      @return nothing.  
  */
  void Merge(int marq1,int marq2) const;

  /** Calcul du log sous l'hypoth�se d'ind�pendance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param nbdata le nombre de donn�es informatives(o)
      @return le log
  */

  double LogInd(int m1, 
		int m2,
		int *nbdata) const;


  /** Calcul l'esp�rance du nombre de recombinants, (log �volutif).
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param theta
      @param loglike(o)
      @return l'esp�rance      
  */
  double EspRec(int m1, 
		int m2, 
		double theta, 
		double *loglike) const;

  /** Etape d'Expectation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le loglike
  */
  double ComputeExpected(const Carte *data, double *expected);

  double ContribLogLike2pt(int m1);  
  double ContribLogLike2pt(int m1, int m2);  
  double NormContribLogLike2pt(int m1, int m2);  

 private:

  /** Pr�paration des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  void PreparEM(const Carte *data);

  /** nettoyage des structures pour la programmation dynamique de EM.
      @param data la carte
    */
  void NetEM(const Carte *data);


  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  void Prepare2pt(int m1, int m2,  int *ss) const {
    print_out("Bug, m�thode interdite = Prepare2pt!\n");
    return;
  };

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = Estimate2pt!\n");
    return;
  };

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  double LogLike2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = LogLike2pt!\n");
    return 0.0;
  };

  /** Calcul du LOD score et de fraction de recombinants 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param epsilon le seuil de convergence
      @param fr la fraction de recombinants(i,o)
      @return le LOD
  */
  double ComputeOneTwoPoints(int m1, 
			     int m2, 
			     double epsilon,
			     double *fr) const; 

  /** frequence de rec (recopie de Carte->tr avec eventuelle prise en compte du CrossType RIL). */
  doublePtr LocTr;
  doublePtr LocExp;
  
  /** tableaux des probabilit�s conditionnelles de passage par un sommet depuis la gauche. */
  doublePtr SourceTo;

  /** tableaux des probabilit�s conditionnelles de passage par un sommet depuis la droite. */
  doublePtr ToSink;

  /** Nombre de recombinants connus dans chaque intervalle. */
  intPtr PreRec;
  
  /** Nombre de non-recombinants connus dans chaque intervalle. */
  intPtr PreNonRec;
  
  /** Nombre d'occurrences d'une paire de connus, recombinants ou non, et s�par�es par des inconnus (un au moins). */
  struct Pair {
    Pair *Next;
    int Left;
    int Right;
    int NumOcc[2];
  };

  /** */
  typedef struct Pair HPair;

  /** */
  typedef HPair *PairPtr;

  /** */
  typedef PairPtr *PairPtrPtr;
  
  /** Pour remplir la liste efficacement. */
  PairPtrPtr *KnownPairs;

  /** Pour remplir la liste efficacement. */
  PairPtr PairsHead;

  /** Nombre d'occurrences de connus s�par�es de l'extr�mit� (debut/fin) par des inconnus (un au moins). */
  CoupleIntPtr Flanking;

  /** Cree un nouveau consensus pour une paire de marqueurs. 
    @param a indice du premier marqueur
    @param b indice du second marqueur
    @param r 0 ou 1 alias non recombinant ou recombinant.
  */
  inline void IncrementPair(int a, int b, int r);

  /** Calcul des probabilit�s d'existence d'un chemin passant par un sommet depuis une extr�mit�. 
    @param a indice du premier marqueur
    @param b indice du second marqueur
    @param map la carte
  */
  inline void ComputeSourceTo(int a, int b, const Carte *map);

  /** Calcul des probabilit�s d'existence d'un chemin passant par un sommet depuis l'autre extr�mit�. 
      @param a indice du premier marqueur
      @param b indice du second marqueur
      @param map la carte
  */
  inline void ComputeToSink(int a, int b, const Carte *map);

  /** Mise a jour des � expected �.
      @param a indice du premier marqueur
      @param b indice du second marqueur
      @param n 
      @param map la carte
      @param expected vecteur d'expectation
    */
  void UpdateExpectedR(int a,
		       int b, 
		       int n, 
		       const Carte *map,
		       double *expected);

 /** Mise a jour des � expected �.
      @param a indice du premier marqueur
      @param b indice du second marqueur
      @param n 
      @param map la carte
      @param expected vecteur d'expectation
 */
  void UpdateExpectedNR(int a, 
			int b, 
			int n, 
			const Carte *map, 
			double *expected);

  /** Conversion des expected pour prendre en compte le caractere RISelf.
      @param map la carte
      @param expected le vecteur des esperances
  */
  void BC2RISelf(const Carte *map, double *expected);
  
  /** Conversion des theta pour prendre en compte le caractere RISelf.
      @param map la carte
  */
  void RISelf2BC(const Carte *map, double *expected);
  
  /** Conversion des esperance de rec. pour prendre en compte le caractere RISib.
      @param map la carte
      @param expected le vectuer des esperances
  */
  void BC2RISib(const Carte *map, double *expected);

  /** Conversion des theta pour prendre en compte le caractere RISib.
      @param map la carte
  */
  void RISib2BC(const Carte *map, double *expected);
	
};

#endif /* _BJS_BC_H */
