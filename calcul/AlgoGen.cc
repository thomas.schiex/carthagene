//-----------------------------------------------------------------------------
//			      CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: AlgoGen.cc,v 1.24.2.1 2012-08-02 15:43:54 dleroux Exp $
//
// Description : Algorithme genetique
// Divers :
//-----------------------------------------------------------------------------


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
//#include <unistd.h>

#include "AlgoGen.h"
#include "Carte.h"
#include "CGtypes.h"

// Les define et types de system.h

#ifndef DBL_MAX
#define DBL_MAX 1e37
#endif
#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif
#define SystemEOF -1

// Les define et types de types.h

#define TRUE 1
#define FALSE 0

// Les define de share.c
#define sigma_share 1
#define alpha_share 1

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - un genotype
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

Algogen::Algogen(CartaGene *cartage)
{

  Cartage = cartage;

  int i;
  
  first=0; 
  iset=0;
  rndcalcflag=1;

  // Du module selection.c
  tab = NULL;
  fraction = NULL;
  choices = NULL;

  // Du module muteval.c
  tabmut = NULL;


  // Indicateur pour utilisation heuristique du MST 
  IndMST = 0;
  for (i=0; i<500; i++) chosen[i] = 0;

}


//-----------------------------------------------------------------------------
// Destructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - 
// -
// Valeur de retour :
//-----------------------------------------------------------------------------


Algogen::~Algogen()
{
}

//****************************************************************************
// Origine: module init_pop.c
//----------------------------------------------------------------------------
//  Creation et Initialisation d'une population d'individus 
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
void Algogen::InitPop(int nb_elements,
		      Chromosome **population,
		      Chromosome **new_population)
{
  int i;
  /* First is used to prevent memory to be reallocated for each run of */
  /* the program */

  if (first==0) 
    {
      *population = new Chromosome[nb_elements];
      *new_population = new Chromosome[nb_elements];
    }
 
  for (i=0;i<nb_elements;i++)
    {
      /* Allocate memory */
      if (first==0)
	{
	  LocalAllocMem(&((*population)[i].data));
	  LocalAllocMem(&((*new_population)[i].data));
	}
	(*population)[i].newc=FALSE;
        (*population)[i].s_fitness=0.0;

	LocalInitData((*population)[i].data);
	(*population)[i].evaluated=FALSE ;

    }
  first=1;
}

//----------------------------------------------------------------------------
//  Creation et Initialisation d'une population d'individus � partir du tas
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
void Algogen::InitPopFromTas(int nb_elements,
			     Chromosome **population,
			     Chromosome **new_population)
{
  int * tabid; // tableau des indices ordonn�es du tas.
  int i;
  /* First is used to prevent memory to be reallocated for each run of */
  /* the program */

  if (first==0) 
    {
      *population = new Chromosome[nb_elements];
      *new_population = new Chromosome[nb_elements];
    }
 
  tabid = Cartage->Heap->IdSorted();

  for (i=0;i<nb_elements;i++)
    {
      /* Allocate memory */
      if (first==0)
	{
	  LocalAllocMem(&((*population)[i].data));
	  LocalAllocMem(&((*new_population)[i].data));
	}
	(*population)[i].newc=FALSE;
        (*population)[i].s_fitness=0.0;

	(Cartage->Heap->MapFromId(i % Cartage->Heap->HeapSize))->CopyFMap((*population)[i].data);

	(*population)[i].evaluated=FALSE ;

    }
  first=1;

  delete tabid;
}

//----------------------------------------------------------------------------
// Que fait cette fonction ? Initialisation du generateur aleatoire
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
double Algogen::RandUniform(void)
{
  return drand48();
}

//----------------------------------------------------------------------------
// Que fait cette fonction ? Initialisation du generateur aleatoire
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

int Algogen::Flip(double prob)
/* Flip a biased coin - true if heads */
{
  if((RandUniform()) <= prob)
    return(1);
  else
    return(0);
}

//----------------------------------------------------------------------------
// Que fait cette fonction ? Initialisation du generateur aleatoire
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

double Algogen::RandGauss(void)
{
  double fac,r,v1,v2;


  if (iset==0)
    {
      do
	{
	  v1=2.0*RandUniform()-1.0;
	  v2=2.0*RandUniform()-1.0;
	  r=v1*v1+v2*v2;
	} while (r>=1.0);
      fac=sqrt(-2.0*log(r)/r);
      gset=v1*fac;
      iset=1;
      return v2*fac;
    }
  else
    {
      iset=0;
      return gset;
    }
}

//----------------------------------------------------------------------------
// Que fait cette fonction ? Initialisation du generateur aleatoire
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

double Algogen::RandM(void)
{
  double t, rndx1;

  if(rndcalcflag) 
    {
      rndx1 = sqrt(- 2.0*log((double) RandUniform()));
      t = 2* M_PI * (double) RandUniform();
      rndx2 = sin(t);
      rndcalcflag = 0;
      return(rndx1 * cos(t));
    }
  else
    {
      rndcalcflag = 1;
      return(rndx2);
    }

}

//****************************************************************************
// 
//Module local.c
//----------------------------------------------------------------------------
// Que fait cette fonction ?
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
void Algogen::LocalCopyData(Data *out, Data *in)
{
  in->CopyFMap(out);
}/*LocalCopyData*/

//----------------------------------------------------------------------------
// Que fait cette fonction ?  /* Alloue la memoire pour une zone de donnee */
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
 
void Algogen::LocalAllocMem(Data **data)
{
 *data = new Carte(Cartage, Cartage->NbMS, Cartage->MarkSelect);
}/*LocalAllocMem*/


//----------------------------------------------------------------------------
// Que fait cette fonction ?
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::LocalFreeMem(Data **data)
{
delete *data;
}

//----------------------------------------------------------------------------
// Que fait cette fonction ?
// Calcul d'un individu pas completement stupide: on se sert des
//   estimations des taux de rec. pour utiliser qqs heuristiques
//   classiques pour le TSP
//
//   Chemin eulerien sur un MST. Pour changer un peu, j'utilise
//   Prim/Jarnik, mais sans file de priorite .
//
//   La Matrix est une matrice de distance, symmetrique, diagonale a 0.
//   Pour l'instant: TwoPoints (mais une distance ou une contribution au
//   log-likelihood serait moins stupide (eventuellement reajustee par EM)
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::LocalInitData(Data *data)
{
  
  int vertex;
  
   if (IndMST==0) 
     {
       if (Cartage->VerboseFlag) {
	 print_out(  "MST\n");
       }
       flush_out();

       data->BuildMSTMap(0);
       IndMST=1;
     }
   else
     {     
       vertex=randomax(Cartage->NbMS);
       while (chosen[vertex]==1) 
	 vertex=randomax(Cartage->NbMS);             
       chosen[vertex]=1;
       if (Cartage->VerboseFlag) { 
	 print_out( "NearestNeighborMap:vertex choisi :%d\n",vertex);
       }
       flush_out();
       data->BuildNearestNeighborMap(vertex);       
     }
   if (Cartage->VerboseFlag) 
     Cartage->PrintMap(data);
}/*LocalInitData*/


/* ------------------------------------------------ */
/* Order-crossover p. 52 du rapport de Grant Telfar */
/* Idee: on choisit deux points de coupure sur les  */
/*       chromose ce qui nous donne 3 morceaux pour */
/*       chacun. Pour les nouveaux individus : on   */
/*       recopie le morceau du milieu puis, pour    */
/*       l'individu 1 les elements des morceaux 3   */
/*       puis 1 puis 2 (dans cet ordre)de l'individu*/
/*       2 si ils  n'appartiennet pas deja a        */
/*       l'individu 1. On fait la meme chose  pour  */
/*       l'individu 2 avec les morceaux de          */
/*       l'individu 1                               */
/* ------------------------------------------------ */
int Algogen::deja_in_d1(Data *d1,int i1,int i2,int M)
{
int i,ok;
ok=0;
for (i=i1;(i<=i2) && (!ok);i++)
   if (d1->ordre[i]==M) ok=1;
return ok;  
}

Data *Algogen::OrderCrossover(Data *d1, Data *d2, int i1,int i2)
{
int i,j,M;

/* ON RECOPIE LE TROISIEME MORCEAU DE d2 dans le troisieme
morceau de d1*/

j=i2+1;i=i2+1;
while ((i<Cartage->NbMS) && (j<Cartage->NbMS))
  {
     M=d2->ordre[i];
     if (!deja_in_d1(d1,i1,i2,M))
      {
       d1->ordre[j]=d2->ordre[i];
       j++;
      }
     i++;
   }

/* LE TROISIEME MORCEAU DE d2 EST TERMINE : ON FINIT */
/* LE TROISIEME MORCEAU DE d1 AVEC LES PREMIER ET    */
/* DEUXIEME MORCEAU DE d2 JUSQU'A d1 REMPLI          */

if (i>=Cartage->NbMS) /* LE TROISIEME MORCEAU DE d2 EST TERMINE */
  {
     i=0;
     while ((i<i2) && (j<Cartage->NbMS))
     /* TQ TROISIEME MORCEAU DE d1 PAS FINI ET PREMIER ET */
     /* DEUXIEME MORCEAU DE d2 PAS FINIS */
     {
          M=d2->ordre[i];
          if (!deja_in_d1(d1,i1,i2,M))
            {
             d1->ordre[j]=d2->ordre[i];
             j++;
            }
          i++;
     }
  
     if (j>=Cartage->NbMS)
     /* SI TROISIEME MORCEAU DE d1 FINI, ON REMPLIT PREMIER 
     MORCEAU DE d1 */
      {
       j=0;
       while (j<i1)
         {
          M=d2->ordre[i];
          if (!deja_in_d1(d1,i1,i2,M))
               {
                d1->ordre[j]=d2->ordre[i];
                j++;
                }
           i++;
          }
       }
    }

 d1->UnConverge(); 
 d2->UnConverge(); 
 return d1;
 }


//----------------------------------------------------------------------------
// Que fait cette fonction ?
// Order-crossover p. 52 du rapport de Grant Telfar 
// Idee: on choisit deux points de coupure sur les  
//       chromose ce qui nous donne 3 morceaux pour 
//       chacun. Pour les nouveaux individus : on   
//       recopie le morceau du milieu puis, pour    
//       l'individu 1 les elements des morceaux 3   
//       puis 1 puis 2 (dans cet ordre)de l'individu
//       2 si ils  n'appartiennet pas deja a        
//       l'individu 1. On fait la meme chose  pour  
//       l'individu 2 avec les morceaux de          
//       l'individu 1                               
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::LocalCrossover(Data *data1, Data *data2)
{
  int  IdxMarqueur1, IdxMarqueur2,i1,i2;
  IdxMarqueur1=randomax(Cartage->NbMS-1); 
  IdxMarqueur2=randomax(Cartage->NbMS-1); 
  while (IdxMarqueur2==IdxMarqueur1)
          IdxMarqueur2=randomax(Cartage->NbMS-1); 
  if (IdxMarqueur1>IdxMarqueur2) 
    {
     i1=IdxMarqueur1;i2=IdxMarqueur2;
    }
  else
    {
    i1=IdxMarqueur2;i2=IdxMarqueur1;
    }
  data1=OrderCrossover(data1,data2,i1,i2);
  data2=OrderCrossover(data2,data1,i1,i2);
}/*LocalCrossover*/


//----------------------------------------------------------------------------
// Que fait cette fonction ?
// Attention ! Ce code peut generer une boucle infinie si l'on a moins de
//   4 marqueurs 
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::LocalMutation(Data *data)
{
  int Idx1,Idx2,sauv;

  
  Idx1 = randomax(Cartage->NbMS);
  Idx2 = randomax(Cartage->NbMS);
  sauv=data->ordre[Idx1];
  data->ordre[Idx1]=data->ordre[Idx2];
  data->ordre[Idx2]=sauv;
  data->UnConverge(); 
}/*LocalMutation*/


//----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

double Algogen::LocalDataEval(Data *data)
{
  int e1,e2;
  Carte TempMap(Cartage, Cartage->NbMS, Cartage->MarkSelect);
  Carte  FinalMap(Cartage, Cartage->NbMS, Cartage->MarkSelect);
  double cost,finalcost;
  int continu;
  
  cost = Cartage->ComputeEM(data);
  finalcost = cost;
  
  Cartage->Heap->Insert(data,0);
  data->CopyFMap(&FinalMap);

  continu=1;
  while (continu)
    {
      continu=0;
      for (e1=0;e1<Cartage->NbMS-1;e1++)
	for (e2=e1+2;e2<=Cartage->NbMS;e2++)
	  {
	    data->CopyFMap(&TempMap);
	    TempMap.Apply2Change(e1,e2);
	    cost = Cartage->ComputeEMS(&TempMap,finalcost-2.0);
	    Cartage->Heap->Insert(&TempMap,0);
	    if (cost>finalcost+0.0001)
	      {
                finalcost=cost; 
                TempMap.CopyFMap(&FinalMap);
                continu=1;
		
	      }

	    // traitement de la demande d'arr�t.
	    if ( Cartage->StopFlag )
	       return finalcost;
	  } 
      FinalMap.CopyFMap(data);
    }
  return finalcost;
}/*LocalDataEval*/




//****************************************************************************
// 
//Module Eval.c
//----------------------------------------------------------------------------
// Que fait cette fonction ?
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::EvalPopulation(int nb_elements,Chromosome *population,Chromosome **best_chrom)
{
    int i;
  double maxfit=-DBL_MAX ;

  for (i=0;i<nb_elements;i++) 
   {
    if (!population[i].evaluated) 
        {
         population[i].evaluated=TRUE ;
         population[i].r_fitness=LocalDataEval(population[i].data) ;

	  if ( Cartage->StopFlag )
	    return;

         }
  
     if (population[i].r_fitness>maxfit) 
        {
         *best_chrom=&(population[i]) ;
         maxfit=population[i].r_fitness ;
         }
   }
}

//****************************************************************************
// 
//Module crosseval.c
//----------------------------------------------------------------------------
// Que fait cette fonction ?
// Choose 2 elements to crossover
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::ChooseCouple(int *n1,int *n2,int nb_elements,
		  Chromosome *population)
{
      do
	*n1=(int)(RandUniform()*nb_elements);
      while (population[*n1].newc==TRUE);
      do
	*n2=(int)(RandUniform()*nb_elements);
      while ((population[*n2].newc==TRUE) || (*n2==*n1));

}

//----------------------------------------------------------------------------
// Que fait cette fonction ?
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::Crosseval(int nb_elements,
	       double pcross,
	       Chromosome *population)
{
  /* Number of elements to crossover */
  int max_elems=(int)((double)nb_elements*pcross) ;
  int i,n1,n2;

  /* Allocate memory only once */
    for (i=0;i<max_elems;i+=2) {
      /* step 2, because we change 2 elements each time */
      ChooseCouple(&n1,&n2,nb_elements,population) ;
      /* Children will override parents so no need to copy them */
      LocalCrossover(population[n1].data,population[n2].data) ;
      /* No need to evaluate parents yet, all evaluations will */
      /* be done later */
      population[n1].newc=TRUE ;
      population[n2].newc=TRUE ;
      population[n1].evaluated=FALSE ;
      population[n2].evaluated=FALSE ;
    } /*for*/
}

//****************************************************************************
// 
//Module muteval.c
//----------------------------------------------------------------------------
// Que fait cette fonction ?
// Choose 2 elements to crossover
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::MutevalFreeMem() 
{
  delete tabmut ;
}

//----------------------------------------------------------------------------
// Que fait cette fonction ?
// Choose 2 elements to crossover
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::ChooseChrom(int *n,int nb_elements,Chromosome *population)
{
      do
	*n=(int)(RandUniform()*nb_elements);
      while (population[*n].newc==TRUE);
}

//----------------------------------------------------------------------------
// Que fait cette fonction ?
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::Muteval(int nb_elements,
	     double pmut,
             Chromosome *best_chrom,
	     Chromosome *population)
{
  int i,j,n;
 int p1,p2,p3,ptot,diff;

 p1=0;p2=0;p3=0;ptot=0;


    for (i=0;i<nb_elements;i++)
      {
	if (&population[i]==best_chrom) p1++;
	if (population[i].newc==TRUE) p2++;
      }

    ptot=p1+p2+p3;
    diff=nb_elements-ptot;

/* Code remplace par du plus simple. Ca ne doit rien changer */
  n=(int)(nb_elements*pmut+0.5);  
  if (n>diff) n=diff;

/* Fin du changement de code */
  
    for (i=0;i<n;i++) 
     {
      ChooseChrom(&j,nb_elements,population) ;
      LocalMutation(population[j].data) ;
      population[j].newc=TRUE ;
      population[j].evaluated=FALSE ;
     }
}


//****************************************************************************
// 
//Module selection.c
//----------------------------------------------------------------------------
// Que fait cette fonction ?
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::SelectionFreeMem() {
  if (tab!=NULL)
    delete [] tab ;
  else {
     delete fraction;
     delete choices;
  }
}

//----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::Selection(int nb_elements,
			int evolutive_fitness,
			int selection_number,
			Chromosome **population,
			Chromosome **new_population)
{
  int i;
  double total_fitness=0.0;
  double a;
  int p;
  int n=0;
  Chromosome *buf_pop;
  /******************************/
  //  static int nremain;
  int nremain;
  int j,k,jassign;
  double expected,avg_s_fitness;
  int jpick;
  /******************************/
  
  if (selection_number==0)
    {
      
      /*************************************************************/
      /*       Roulette Wheel Selection                            */
      /*************************************************************/
      if (tab==NULL)
	{
	  tab=new double [nb_elements];
	}

      for (i=0;i<nb_elements;i++) {
	total_fitness+=(*population)[i].s_fitness;
      } /*for*/

      tab[0]=(*population)[0].s_fitness/total_fitness;
      for (i=1;i<nb_elements;i++)
	{
	  tab[i]=(*population)[i].s_fitness/total_fitness+tab[i-1];
	} /*for*/
      
      for (i=0;i<nb_elements;i++)
	{
	  a=RandUniform();
	  n=nb_elements/2;
	  p=(n+1)/2;
	  while(TRUE)
	    {
	      if (a<tab[n]) 
		{
		  if ((n==0) || (a> tab[n-1])) break;
		  n=n-p;
		  if (n<0) n=0;
		}
	      else
		{
		  if ((n==nb_elements-1)||(a<tab[n+1])) break;
		  n=n+p;
		  if (n>=nb_elements) n=nb_elements-1;
		} /*if*/
	      p=(p+1)/2;
	    } /*while*/

	  (*new_population)[i].newc=FALSE;
	  LocalCopyData((*new_population)[i].data,(*population)[n].data);
	  if (evolutive_fitness)
	    (*new_population)[i].evaluated=FALSE ;
	  else {
	    (*new_population)[i].r_fitness=(*population)[n].r_fitness ;
	    (*new_population)[i].evaluated=(*population)[n].evaluated ;
	  }
	} /*for*/
    }
  /*************************************************************/
  else
    {
      /*************************************************************/
      /*  Stochastic Remainder without Replacement Selection       */
      /*************************************************************/

      if (choices==NULL) {
	choices = new int[nb_elements];
	fraction = new double[nb_elements];
      }

      /* calcul de la moyenne de la scaled fitness */
      /*-------------------------------------------*/
      avg_s_fitness=0.0;
      for (i=0;i<nb_elements;i++)
	{
	  avg_s_fitness=avg_s_fitness+(*population)[i].s_fitness;
	} /*for*/
      avg_s_fitness=avg_s_fitness/(double)nb_elements;


      if(avg_s_fitness == 0)
	{
	  for(j = 0; j < nb_elements; j++) choices[j] = j;
	}
      else
	{
	  j = 0;
	  k = 0;

	  /* Assign whole numbers */
	  do 
	    {
	      expected = (((*population)[j].s_fitness)/avg_s_fitness);
	      jassign = (int)expected; 
	      /* note that expected is automatically truncated */
	      fraction[j] = expected - jassign;
	      while(jassign > 0)
		{
		  jassign--;
		  if ((k>=nb_elements)|| (k<0))
		    {
		      }
		  else
		    choices[k] = j;
		  k++;
		} /*while*/
	      j++;
	    }
	  while(j < nb_elements);
        
	  j = 0;
	  /* Assign fractional parts */
	  while(k < nb_elements)
	    { 
	      if(j >= nb_elements) j = 0;
	      if(fraction[j] > 0.0)
		{
		  /* A winner if true */
		  if(Flip(fraction[j])) 
		    {
		      choices[k] = j;
		      fraction[j] = fraction[j] - 1.0;
		      k++;
		    } /*if*/
		} /*if*/
	      j++;
	    } /*while*/
	} /*if*/
      nremain = nb_elements - 1;

      for (i=0;i<nb_elements;i++)
	{
	  jpick =(int)(RandUniform()*nremain);
	  n = choices[jpick];
	  choices[jpick] = choices[nremain];
	  nremain--;

	  (*new_population)[i].newc=FALSE;
	  LocalCopyData((*new_population)[i].data,(*population)[n].data);
	  if (evolutive_fitness)
	    (*new_population)[i].evaluated=FALSE ;
	  else {
	    (*new_population)[i].r_fitness=(*population)[n].r_fitness;
	    (*new_population)[i].evaluated=(*population)[n].evaluated ;
	  }
	} /*for*/


      /*************************************************************/
    } /*if*/

  buf_pop=*population;
  *population=*new_population;
  /* Sert a ne pas avoir a re-reserver l'espace memoire la fois suivante */
  /* on sauvegarde ainsi l'adresse d'une zone allouee a la bonne taille  */
  *new_population=buf_pop; 
}




//****************************************************************************
// 
//Module master.c
//-----------------------------------------------------------------------------
// Lancement de l'algorithme genetique.
//-----------------------------------------------------------------------------
// Param�tres : 
// - Voir AlgoGen.h
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Algogen::Run(int nb_gens,
		  int nb_elements,
		  int selection_number,
		  float pcross,
		  float pmut,
		  int evolutive_fitness)
{
  
  /* Value of best fitness for this generation */ 
  double best_fitness;
  /* current generation number */
  int gen;
  /* Pointers to population & new_population */
  Chromosome *population,*new_population;
  /* Pointers to the best chromosome of the population */
  Chromosome *best_chrom;
  Carte FinalBest(Cartage, Cartage->NbMS, Cartage->MarkSelect);
  int i,*sauv_chrom,premiere_fois,ok;
  int gen_debut=0 ;
  long double maxB;
  int NbEdges=0;
  int flagtas = nb_elements;

  if (nb_elements < ((Cartage->NbMS) / 2))
    nb_elements = (Cartage->NbMS) / 2;

  if (Cartage->VerboseFlag) 
    {
      
      print_out(  "\n\n\n        **************************************\n") ;
      print_out(  "        *       Algorithmes Genetiques       *\n") ;
      print_out(  "        *       Version non parallele        *\n") ;
      print_out(  "        **************************************\n\n") ;
      print_out(  " => Parametres utilises :\n\n") ;
      
      print_out( "nb_gens           = %d \n",nb_gens);
      print_out( "nb_elements       = %d \n",nb_elements);
      print_out( "selection_number  = %d \n",selection_number);
      print_out( "evolutive_fitness = %d \n",evolutive_fitness);
      print_out( "pcross            = %f \n",pcross);
      print_out( "pmut              = %f \n",pmut);
    }
  flush_out();

  NbEdges = (Cartage->NbMS * ((Cartage->NbMS) - 1)) / 2;
  
  sauv_chrom = new int [Cartage->NbMS];
  premiere_fois = 1;
  
  /* ============================== Main loop =========================== */
  
  /* Allocates memory and initializes population if needed (i.e if  */
  /* no restart) */
  
  if (flagtas != 0)
    InitPop(nb_elements,&population,&new_population);
  else 
    InitPopFromTas(nb_elements,&population,&new_population);
  
  gen_debut=0;
  maxB=-9999999.0;
  gen=gen_debut;

  while (gen<nb_gens)
    {
      /* Evaluate elements of the population that have not been */
      /* evaluated yet */
      EvalPopulation(nb_elements,population,&best_chrom) ;

      // traitement de la demande d'arr�t.
      if ( Cartage->StopFlag )
	{ 
	  Cartage->StopFlag = 0;
	  
	  // lib�ration
	  
	  /* Free memory allocated by each of the units */
	  SelectionFreeMem() ;
	  MutevalFreeMem() ;
	  
	  // 
	  print_out(  "Aborted!\n");
	  flush_out();
	  return;
	}
      
      best_fitness=best_chrom->r_fitness;
      if (best_fitness==0.0) {
	if (Cartage->VerboseFlag) {
	  print_out(  "aucun element admissible\n");
	}
	else 
	  {
	    if (!Cartage->QuietFlag) 
	      {
		print_out( "Gen: %d \n",gen) ;
		print_out( "fitness=%f ",best_chrom->r_fitness);
		flush_out();
	      }
	    if (best_chrom->r_fitness>999999999999.0) gen=nb_gens;
	  }
      }
      flush_out();
      ok=0;
      if ((long double) best_chrom->data->coutEM > (maxB + 0.0001)) 
	{
	  maxB=best_chrom->data->coutEM;
	  (best_chrom->data)->CopyFMap(&FinalBest);
	  ok=1;
	  if (!Cartage->QuietFlag) {
	    print_out(  "+\n");
	  }
	  flush_out();
	}
      else if (!Cartage->QuietFlag) {
	print_out(  "-\n");
      }
      flush_out();
      
      if (premiere_fois == 1)
	{ 
         for (i = 0; i < Cartage->NbMS; i++) 
	   sauv_chrom[i]=best_chrom->data->ordre[i];
         premiere_fois=0;
        }
      else
        {
	  i=0;
         while (i < Cartage->NbMS && 
		best_chrom->data->ordre[i] == sauv_chrom[i]) 
	   i++;
         if (i >= Cartage->NbMS);
         else
	   {
             for (i = 0; i < Cartage->NbMS; i++) 
	       sauv_chrom[i] = best_chrom->data->ordre[i];
             if (ok)
	       { 
		 nb_gens++;
		 ok=0;
	       }
	   }
	}
      



      /* If last generation then ev
	 erything is finished so skip following code */
      if (gen < (nb_gens - 1)) 
	{
	  /* For next generation, we select elements then crossover and       */
	  /* mutate them */
	  Selection(nb_elements,
		    evolutive_fitness,
		    selection_number,
		    &population,
		    &new_population);
	  Crosseval(nb_elements,
		    pcross,
		    population);
	  Muteval(nb_elements,
		  pmut,
		  best_chrom,
		  population);
	}
      
      if (Cartage->VerboseFlag) { 
	print_out( "*******GENERATION %d sur %d finie\n",gen+1,nb_gens);
      }
      flush_out();
      gen++;
    } /*for*/

  
  /* Free memory allocated by each of the units */
  SelectionFreeMem() ;
  MutevalFreeMem() ;

}





