//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_RH.h,v 1.20 2010-03-12 10:42:44 dleroux Exp $
//
// Description : Prototype de la classe BJS_RH.
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJS_RH_H
#define _BJS_RH_H

#include "BioJeuSingle.h"

#define EM_MAX_THETA_HR 0.9995
#define EM_MIN_THETA_HR 0.0001
#define EM_MAX_RETAIN   0.9999
#define EM_MIN_RETAIN   0.0001 

class BJS_RHD;
class BJS_RHE;

/** Classe des jeux de donn�es hybrides irradi�s */
class BJS_RH : public BioJeuSingle {
 public:

  /** Constructeur. */
  BJS_RH();

  /** Constructeur.
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param id le num�ro du jeu.
      @param cross type de jeu de donn�e.
      @param nm nombre de marqueurs.
      @param taille de l'�chantillon
      @param bitjeu champ de bit du jeu de donn�es.
      @param indmarq le vecteur d'indice de marqueur
      @param echantil la matrice des informations
    */
  BJS_RH(CartaGenePtr cartag,
	 int id,
	 charPtr nomjeu,
	 CrossType cross,
	 int nm, 
	 int taille,
	 int bitjeu,
	 int *indmarq,
	 Obs **echantil);
  
  /** Destructeur. */
  ~BJS_RH();

  /** Constructeur par recopie */
  BJS_RH(const BJS_RH& b);
  /** Constructeur par recopie (variante transtypante) */
  BJS_RH(const BJS_RHE& b);
  /** Constructeur par recopie (variante transtypante) */
  BJS_RH(const BJS_RHD& b);

  /** Compatibilite entre deux echantillons d'un marqueur
      @param numarq1 le numero du marqueur 1
      @param numarq2 le numero du marqueur 2
  */
  int Compatible(int numarq1, int numarq2) const;

  /** Fusion de deux marqueurs pour la creation d'un haplotype. Le
      marqueur 1 est utilise pour stocker le resultat.
      @param numarq1 marqueur 1 
      @param numarq2 marqueur 2 
      @return nothing.  
  */
  void Merge(int marq1,int marq2) const;

  /** Etape d'Expectation de EM.
      @param data la carte
      @param expected le vecteur d'expectation
      @return le loglike
  */
  double ComputeExpected(const Carte *data, double *expected);

  double Retention2pt();
  double Theta2pt(int m1, int m2, double r);
  void InitContribLogLike2pt();
  double ContribLogLike2pt(int m1);  
  double ContribLogLike2pt(int m1, int m2);  
  double NormContribLogLike2pt(int m1);  
  double NormContribLogLike2pt(int m1, int m2);  

 private:

  /** Calcul stat. suffisantes pour le 2pt
      @param m1 premier marqueur
      @param m2 second marqueur
      @param ss statistiques suffisantes (n[1..3])
  */
  void Prepare2pt(int m1, int m2,  int *ss) const;

  /** Calcul des estimateurs 2pt de vrais. max
      @param par vecteur des parametres du modele (theta,r mis a jour)
      @param ss statistiques suffisantes (n[1..3])
  */
  void Estimate2pt(double *par, int *ss) const;

  /** Calcul de la vraisemblance 2pt etant donnes les parametres du
      modele.
      @param par vecteur des parametres du modele (theta,r)
      @param ss statistiques suffisantes (n[1..3])
  */
  double LogLike2pt(double *par, int *ss) const;

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. 
  */
  double ComputeOneTwoPoints(int m1, 
			     int m2, 
			     double epsilon,
			     double *fr) const;
    
  /** Calcul du log sous l'hypoth�se d'ind�pendance 2pt.
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param nbdata le nombre de donn�es informatives(o)
      @return le log
  */
  double LogInd(int m1, 
		int m2,
		int *nbdata) const {return 0.0;};
  
  /** Calcul l'esp�rance du nombre de cassures, (log �volutif).
      @param m1 le num�ro du premier marqueur
      @param m2 le num�ro du second marqueur
      @param theta
      @param loglike(o)
      @return l'esp�rance      
  */
  double EspRec(int m1,
		int m2, 
		double theta, 
		double *loglike) const {return 0.0;};

  /** Pr�paration des structures pour la programmation dynamique de EM.
      @param data la carte
  */
  void PreparEM(const Carte *data);

  /** nettoyage des structures pour la programmation dynamique de EM.
      @param data la carte
    */
  void NetEM(const Carte *data);


  /** Pour la programmation dynamique. tableaux des probabilit�s
      conditionnelles de passage par un sommet depuis la gauche
      
      Comme les proba conditionnelles sont utilis�es, on sait que la proba
      de passer par le sommet 1 est 1 moins la proba de passer par
      l'autre sommet. Il est donc inutile de conserver les deux probas !
      Seule celle correspondant au sommet 1 est conserv�e.
  */
  doublePtr SourceTo[2];

  /**  la programmation dynamique. tableaux des probabilit�s
       conditionnelles de passage par un sommet depuis la droite
  */
  doublePtr ToSink[2];
  
  /** */
  intPtr Known[4];

  /** les individus totalement non informatifs */
  int Unknown;
  
  /** Nombre d'occurrences d'une paire de connus, recombinants ou non, et
      séparés par des inconnus (un au moins).
  */

  struct PairRH {
    PairRH *Next;
    int Left;
    int Right;
    int NumOcc[4];
  };

  /** */
  typedef struct PairRH HPairRH;
  /** */
  typedef HPairRH *PairRHPtr;
  /** */
  typedef PairRHPtr *PairRHPtrPtr;
  
  /** Pour remplir la liste efficacement.
    */
  PairRHPtrPtr *KnownPairs;

  /** Pour remplir la liste efficacement.
    */
  PairRHPtr PairsHead;
  
  /** Nombre d'occurrences de connus s�par�es de l'extr�mit� (debut/fin) par 
      des inconnus (un au moins).
  */
  CoupleIntPtr FlankingR;
  
  /** Nombre d'occurrences de connus s�par�es de l'extr�mit� (debut/fin) par 
      des inconnus (un au moins).
  */
  CoupleIntPtr FlankingL;
  
  /** Mise a jour des � expected �.
      @param a indice du premier marqueur
      @param b indice du second marqueur
      @param l
      @param r
      @param n 
      @param map la carte
      @param expected vecteur d'expectation
  */
  void UpdateExpected(int a, 
		      int b, 
		      int l, 
		      int r, 
		      int n, 
		      const Carte *map, 
		      double *expected);

  /** Gestion des paires de connus separees par des inconnus
      @param a indice du premier marqueur
      @param b indice du second marqueur
      @param l
      @param r 0 ou 1 alias non recombinant ou recombinant.
  */
  inline void IncrementPair(int a, int b, int l, int r);
  
  /** Calcul des probabilit�s d'existence d'un chemin passant par un sommet depuis chacune des extr�mit�s.
      @param a indice du premier marqueur
      @param b indice du second marqueur
      @param map la carte
  */
  inline void ComputeSourceTo(int a, int b, const Carte *map);

  /** Calcul des probabilites d'existence d'un chemin passant par un sommet depuis l'extremite droite (puit)
      @param a indice du premier marqueur
      @param b indice du second marqueur
      @param map la carte
  */
  inline void ComputeToSink(int a, int b, const Carte *map);

  /** Calcul de la distance
      @param le theta.
      @return la distance
  */
  inline double Distance(double theta) const 
    {
      return (Theta2Ray(theta));
    };
  /** Calcul de la distance
      @param l'unit�
      @param le theta.
      @return la distance
  */
  inline double Distance(const char unit[2], double theta) const 
    {
      return (Theta2Ray(theta));
    }; 
  /* Internal computation done by InitContribLogLike2pt for TSP reduction  */
  double RetentionRH;
};

#endif /* _BJS_RH_H */
