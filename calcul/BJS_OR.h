//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// BJS_OR.h,v 1.8 2006/12/06 19:22:33 tschiex Exp
//
// Description : ordre des marqueurs sur un g�nome de r�f�rence
// Divers : 
//-----------------------------------------------------------------------------

#ifndef _BJS_OR_H
#define _BJS_OR_H

#include "BioJeuSingle.h"

#define MAXNBCHROM 10000

/** Classe des jeux de donn�es Ordre complet*/
class BJS_OR : public BioJeuSingle {
 public:

  /** poids d'un break-point */
  double Poids;

  /** lambda parameter in Poisson law, used in ComputeEM(S) if Poids is negative  */
  double Lambda;

  /** tableau des marqueurs extremit�s ou non des chromosomes */
  int *ChromEx;
  
  /** tableau d'appartenance aux chromosomes */
  int *Chromosome;

  /** tableau des positions en paire de base dans les chromosomes */
  int *Position;

  /** Constructeur. */
  BJS_OR();

  /** Constructeur.
      @param cartag acc�s � l'ensemble des informations du syst�me. 
      @param id le num�ro du jeu.
      @param cross type de jeu de donn�e.
      @param nm nombre de marqueurs.
      @param bitjeu
      @param le tableau d'indices des marqueurs
      @param coeff le poids des BP par rapport � la vraisemblance
      @param chrom tableau d'appartenance aux chromosomes
      @param chromex tableau de marqueurs extremit�s ou non
      @param position tableau des position des marqueurs sur les chromosomes
  */
  BJS_OR(CartaGenePtr cartag,
	 int numjeu,
	 CrossType cross,
	 charPtr nomjeu,
	 int nbm,
	 int bitjeu,
	 int *indmarq,
	 double coeff,
	 int *chrom,
	 int *chromex,
	 int *position);
	
  
  /** Destructeur. */
  ~BJS_OR();

    
  /** nombre de break-points entre l'ordre et un jeu de donn�es
      @param id le num�ro du jeu
      @return BP le nb de points de cassure
  */
  int BreakPoints(BioJeu *jeu);

  /** number of break-points between the given order and a map
      @param a map
      @return BP the number of break-points
  */
  int BreakPointsMap(Carte *map);
  
  /** ne fait rien */
  int Compatible(int numarq1, int numarq2) const {return 1;};


  /** Ne fait rien, pour la coh�rence
  */
  inline int GetBJMO() const
    {
      return 0;
    };

  /** Ne fait rien, pour la coh�rence
   */
  void Merge(int marq1,int marq2) const {};


  
  /** m�thode qui affiche les marqueurs de debut et fin de segment
    */
  void DumpEch(void) const;

  /** returns the number of chromosomes the loci from the current 
      selection belongs to
      and for each chromosome the number of loci involved
    */
  int GetnLocipChrom(int **);

  
  /** m�thode qui ne fait rien pour la coh�rence!
    */
  int Couplex(int m1, int m2) const {return 0;};

  
  /** m�thode qui ne fait rien pour la coh�rence!
    */
  void ComputeTwoPoints(void) {};

  /** m�thode qui ne fait rien pour la coh�rence!
  */
  virtual void DumpEchMarq(int numarq) const {};
  
  /** m�thode qui ne fait rien pour la coh�rence!
  */
  double GetTwoPointsLOD(int m1, int m2) const {return 0.0;};

  /** m�thode qui ne fait rien pour la coh�rence!
    */
  virtual void DumpTwoPointsLOD() const {};

  /** m�thode qui retourne le poids*nb de BP
   */
  double ComputeEM(Carte *data);  

  /** m�thode qui retourne le poids*nb de BP
   */
  double ComputeEMS (Carte *data, 
		     double threshold);

  /** m�thode qui ne fait rien
   */
  void PrintDMap(Carte *data, int envers, Carte *dataref) {};

  /** Matrice 2p des distances 5bidon)
      @param unit l'unit� de distance: 
      'k' = kosambi ou centiray, 
      'h' = haldane ou centiray.
  */
  inline void PrintTwoPointsDist(const char unit[2]) const {};
  
 private:

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  virtual double LogInd(int m1, int m2, int *nbdata) const {
    print_out("Bug, m�thode interdite = LogInd!\n");
    return 0.0;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  virtual double EspRec(int m1, int m2, double theta, double *loglike) const {
    print_out("Bug, m�thode interdite = EspRec!\n");
    return 0.0; 
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double GetTwoPointsDH(int m1, int m2) const {
    print_out("Bug, m�thode interdite = GetTwoPointsDH!\n");
    return 0.0; 
  };
  
  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double GetTwoPointsFR(int m1, int m2) const {
    return 0.0;
  };
  
  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  double ComputeExpected(const Carte *data, double *expected) {
    print_out("Bug, m�thode interdite = ComputeExpected!\n");
    return 0.0;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void Prepare2pt(int m1, int m2,  int *ss) const {
    print_out("Bug, m�thode interdite = Prepare2pt!\n");
    return;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void Estimate2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = Estimate2pt!\n");
    return;
  };

  /** ne sert pas */
  double LogLike2pt(double *par, int *ss) const  {
    print_out("Bug, m�thode interdite = LogLike2pt!\n");
    return 0.0;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  virtual double ComputeOneTwoPoints(int m1, int m2, double epsilon, double *fr) const {
    print_out("Bug, m�thode interdite = ComputeOneTwoPoints!\n");
    return 0.0;
  };

  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void PreparEM(const Carte *data) {
    print_out("Bug, m�thode interdite = PreparEM!\n");
  };
  
  /** passage � priv� d'une virtuelle h�rit�e = choix d'impl�mentation. */
  void NetEM(const Carte *data) {
    print_out("Bug, m�thode interdite = NetEM!\n");
  };
  
  /** passage � priv�  = choix d'impl�mentation. */  
  int Maximize(Carte *data, double *expected) const {
    print_out("Bug, m�thode interdite = Maximize!\n");
    return -1;
  };

  /** pour le TSP */
  double ContribLogLike2pt(int m1, int m2);

  
  /** to provide a specific behaviour */
  char *** GetMap(const char unit[2], Carte *data) const;

};

#endif /* _BJS_OR_H */
