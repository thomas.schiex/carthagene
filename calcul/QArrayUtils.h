#include <stdio.h>	// for printf()

/** Class for some static utility methods */
#ifndef __QARRAY_UTILS__
#define __QARRAY_UTILS__
class QArrayUtils
{
	public:
// Fills out a vector of powers of a quantity d, increasing rightward.
// Used for evaluating polynomials. Degree equals highest power; need another element for 0th.
static void FillOutPowersVector(double d, int degree, double *powers)
{
	double term;
	int i;
	
	powers[0] = term = 1;
	for (i = 1; i <= degree; i++)
		powers[i] = term *= d;
}

/**
* Creates a "pointer-to-array-of-pointers" from a "pointer to continuous array of values"
*/
static int **Create2DIntArr(int *const2DIntPtr, int rows, int cols)
{
	int **int2D, *p;
	
	int2D = new int*[rows];
	for (int i = 0; i < rows; i++)
	{
		int2D[i] = new int[cols];
		for (int j = 0; j < cols; j++)
		{
			p = const2DIntPtr + i * cols + j;
			int2D[i][j] = *p;
		}
	}
	return int2D;
}

/**
* Creates a "pointer-to-array-of-pointers" and writes initialValue to all elements
*/
static int **Create2DIntArr(int initialValue, int rows, int cols)
{
	int **int2D;
	
	int2D = new int*[rows];
	for (int i = 0; i < rows; i++)
	{
		int2D[i] = new int[cols];
		for (int j = 0; j < cols; j++)
			int2D[i][j] = initialValue;
	}
	return int2D;
}

	/**
	*	Frees memory of pointer to array of pointers
	*/
	static void Destroy2DIntArr(int **int2D, int rows)
	{
		for (int i = 0; i < rows; i++)
			delete [] int2D[i];
		delete [] int2D;
	}

	/**
	 * Prints out array to console, for debugging
	 */
	static void printArray(double *arr, int len)
	{
		for (int i = 0; i < len; i++)
			printf("%1.4f\t", arr[i]);
		printf("\n");
	}

	static void printIntArray(int *arr, int len)
	{
		for (int i = 0; i < len; i++)
			printf("%i\t", arr[i]);
		printf("\n");
	}

	static void print2DIntArray(int **arr, int numRows, int rowLen)
	{
		for (int i = 0; i < numRows; i++)
			printIntArray(arr[i], rowLen);
		printf("\n");
	}
		
};
#endif
