/*-----------------------------------------------------------------------------
                            Written for use in the CarthaGene application

 James C. Nelson 
 Dept. of Plant Pathology, Kansas State University, Manhattan, KS, USA

 Copyright 2005 James C. Nelson
 This file is distributed under the terms of the Q Public License version 1.0
 and may be freely included with CarthaGene code. Accompanying files are
 QPolynomial.h and .cc, QPolynomialMatrix.h and .cc, QMatingOperator.h and .cc,
 and QArrayUtils.h.
 Minor modifications in native CarthaGene files CGtypes.h, Cartagene.h, and
 Cartagene.cc were made for integration. To make the CarthaGene application
 independent of these classes, simply delete the #include BJS_BS.h statement
 in Cartagene.cc and comment out the code blocks referring to the "BS" cross
 type in that file.
 
 $Id: BJS_BS.cc, v 1.0 2006/02/05

Description: BJS_BS computes linkages and log likelihoods for a population
descended from a line cross by any sequence of backcrossing, selfing, or
random-intercrossing steps.

A NOTE ON MULTIPOINT OPTIMIZATION
For multipoint likelihood calculation, CG treats each individual as a realization of a Markov
chain where hidden states are marker genotypes and observations are marker phenotypes.
The contributions of each individual to the overall likelihood and to crossover expectations
in each interval are computed in a loop over individuals and intervals. The machinery in BJS_BS,
this class, is a generalization of the IC and BC classes to arbitrary series of BC, selfing, and
random-intercrossing operations.

TWO-POINT LINKAGE CALCULATION BY EM
The idea is to use CG's in-place machinery to compute expected crossovers in each interval.
To do this for one individual, CG loops over each possible two-locus genotype and accumulates
the joint probabilities of the genotype and each number (0, 1, or 2) of crossovers corresponding
to the observed two-marker phenotype. CG can then compute the expected number of crossovers in
that individual interval, sum these over the popn (the E step), and divide by the number of
informative meioses in that interval, which for designs based on only one generation is N (for BC1)
and 2N (for IC) (less any individuals lacking marker data). This is the M step and provides the
updated theta estimate for the next E step. I've generalized this method to work for all designs,
by expressing the joint probability distribution of two-locus genotype and crossover number as a
matrix of polynomials in theta, stored as their coefficients. Thus the r(1 - r) computed by CG
for IC designs during the E step would be stored as 0 1 -1, the coefficients of 1, r, and r^2.
The second adjustment is in computing the expected number of informative meioses per individual
from the cumulative sum of the probabilities of double heterozygotes at each generation beginning
with the F1, which depend on the current estimate of theta. It too is stored as an array of
polynomial coefficients. So that the superclass (BioJeu) can continue to use N in the M step,
we reduce the estimated crossover expectation by this factor.
Note: CG takes rigorous account of the parental origin of each allele, hence the 16 possible 2-locus
configurations, since any of the 4 alleles can originate in either of the 2 parents. In this class,
in contrast, I distinguish by parental origin only each 2-allele genotype, plus the two possible
phases of the double heterozygote; thus 3 genotypes in 2 parents (3^2 = 9) + 1 = 10. If needed for
some future update, conversion of these matrices from 10 to the full 16 is possible. As it is,
I had to insert code to halve the probabilities of the 6 classes that are doubly counted in cases of
linkage estimation for dominant markers -- so that all genotype class probabilities will sum to 1.
*/
//-----------------------------------------------------------------------------

#include "BJS_BS.h"
#include "QMatingOperator.h"
#include "QArrayUtils.h"
#include "QPolynomialMatrix.h"

#include <string.h>
#include <math.h>

using namespace legacy;

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_BS::BJS_BS() : BJS_IC()
{}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param?tres :
// - acc?s ? l'ensemble des informations du syst?me. 
// - le num?ro du jeu
// - le type
// - le nombre de marqueurs
// - la taille de l'echantillon
// - le champ de bit
// - la matrice de l'?chantillon
// Valeur de retour :
//-----------------------------------------------------------------------------

BJS_BS::BJS_BS(CartaGenePtr cartag,
	       int id,
	       charPtr nomjeu,
	       CrossType cross,
	       char *design_string,	// added by CN
	       int nm, 
	       int taille,
	       int bitjeu,
	       int *indmarq,
	       Obs **echantil)
  :BJS_IC(cartag,
		id,
    nomjeu,
		cross, 
		nm, 
		taille, 
		bitjeu, 
		indmarq, 
		echantil)
{
 	strncpy(mDesignString, design_string, MAX_BS_GENERATIONS + 1);
 	SetUpLocalNPossibles();
 	SetUpReferenceArrays(nm);
	ComputeMaxPossibleCrossovers();
 	ComputeProbabilityCoeffs();
  ComputeTwoPoints();
  NbMeiose = TailleEchant;	// for use by BioJeu::Maximize(). In fact the number of
  // informative meioses is usually different for each interval, but we scale the expected
  // crossovers passed to the EM solver so that this standard value works.
}


BJS_BS::BJS_BS(const BJS_BS& b)
        : BJS_IC(b)
{
 	strncpy(mDesignString, b.mDesignString, MAX_BS_GENERATIONS + 1);
 	SetUpLocalNPossibles();
 	SetUpReferenceArrays(b.NbMarqueur);
	ComputeMaxPossibleCrossovers();
 	ComputeProbabilityCoeffs();
        ComputeTwoPoints();
        NbMeiose = TailleEchant;	// for use by BioJeu::Maximize(). In fact the number of
        // informative meioses is usually different for each interval, but we scale the expected
        // crossovers passed to the EM solver so that this standard value works.
}



void BJS_BS::SetUpReferenceArrays(int nbMarq)
{
	mRIM_factorP = new QPolynomial(kZeroDegree);	// To be used for accumulating RIM probs
 	mPhenoFreqs = QArrayUtils::Create2DIntArr(0, Num_CG_Phenotypes, Num_CG_Phenotypes);
	mSingleLocusGenoProbs = new double[NUM_PHASED_DIPLOID_GENOTYPES];
	mSingleLocusGenoInvProbs = new double[NUM_PHASED_DIPLOID_GENOTYPES];
	for (int i = 0; i < NUM_PHASED_DIPLOID_GENOTYPES; i++)
		mSingleLocusGenoProbs[i] = mSingleLocusGenoInvProbs[i] = 0;	// added CN 2.10.06
	// as trying to find bug that causes absurd large likelihood values sometimes.
	mSingleLocusGenoProbs[Aa] = 1.0; // initialize to F1 probability
}

/**
*	Ensures that for designs ending in backcrossing, heterozygotes come in only one phase
* and missing data has only two variants
*/
void BJS_BS::SetUpLocalNPossibles()
{
	for (int i = 0; i < Num_CG_Phenotypes; i++)
		mLocalNPossibles[i] = NPossibles[i];
  if (!WasLastOpSelfOrIntercross())
  {
  	mLocalNPossibles[Obs0110] = 1;
  	mLocalNPossibles[Obs1111] = 2; // made this correction 7.6.06 CN
  }
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_BS::~BJS_BS()
{
	delete mXoverGenoProbsM;
	delete mRIM_factorP;
	delete mGenotypeCPPolyV;
	delete [] mSingleLocusGenoProbs;
	delete [] mSingleLocusGenoInvProbs;
	
	QArrayUtils::Destroy2DIntArr(mPhenoFreqs, Num_CG_Phenotypes);
}

//-----------------------------------------------------------------------------
// Applies mating operators to the F1 joint crossover & genotype pmf matrix to
// produce the pmf that holds after the mating design has been applied.
// Similarly produces the pmf of two-locus genotype probabilities, for use in HMM.
// Also computes the expansion factor that represents the ratio of the true number of
// recombinationally informative meioses to the population size (TailleEchant).
// Also computes the single-locus genotype probabilities for this mating design, for
// use in initializing the Markov chain termini for multipoint likelihood maxing, and
// conditioning two-locus genotype pmfs on the genotype of one of them.
// The design string is guaranteed to contain only legal characters (checked in Cartagene->ProbeCrossType())
//-----------------------------------------------------------------------------

void BJS_BS::ComputeProbabilityCoeffs()
{
	QPolynomialMatrix *xProbsM, *newXProbsM, *tempXProbsM, *genoProbsV;
	QMatingOperator *qMoP, *qMoPs[kNumBasicMatingOps];
	
	genoProbsV = CreateF1_PMF(kSingleColumn);
	xProbsM = CreateF1_PMF(mMaxPossibleXovers + 1);
	newXProbsM = CreateF1_PMF(mMaxPossibleXovers + 1);// will be zeroed before use
	for (int op = 0; op < kNumBasicMatingOps; op++) qMoPs[op] = NULL;

	for (int generation = 0; generation < (int)strlen(mDesignString); generation++)
	{
		qMoP = FetchMatingOperator(mDesignString[generation], qMoPs);
		genoProbsV = qMoP->UpdateAllProbs(mSingleLocusGenoProbs, mRIM_factorP, genoProbsV, xProbsM, newXProbsM);
		tempXProbsM = xProbsM;	// swap labels on the two crossover prob arrays, for next loop
		xProbsM = newXProbsM;
		newXProbsM = tempXProbsM;
	} // end loop over generations. Now clean up.
	DisposeMatingOperators(qMoPs);
	delete newXProbsM;
	mGenotypeCPPolyV = genoProbsV;	// store for later use in the HMM routines for multipoint likelihood
	mXoverGenoProbsM = xProbsM;		// store for use in both two- and multipoint linkage estimation
	AdjustProbsForDesign();		// the probs need to be modified a bit differently for BC/DH and S/I
}

/**
*	Compact creation method for genotype pmfs
*/
QPolynomialMatrix *BJS_BS::CreateF1_PMF(int numColumns)
{
	QPolynomialMatrix *qPM = new QPolynomialMatrix	
	(	NUM_DIPLOID_GENOTYPE_COMBOS, numColumns, kZeroDegree);
	qPM->extractPoly(pAaBb, 0)->setCoefficient(0, 1.0); // at F1, the AaBb genotype has all the probability
	return qPM;
}
	
/**
*	Replace this with a hash CN 2.6.06
*	Retrieves a previously created QMatingOperator if one exists for the op symbol
*	presented; otherwise creates one.
*/
QMatingOperator *BJS_BS::FetchMatingOperator(char ch, QMatingOperator **qMoPs)
{
	QMatingOperator *qMoP = NULL;
	
	for (int i = 0; i < kNumBasicMatingOps; i++)
		if (ch == sMatingSymbols[i])
	{
		qMoP = qMoPs[i];
		if (qMoP == NULL)
			qMoP = qMoPs[i] = new QMatingOperator(ch);
		break;
	}
	return qMoP;
}

void BJS_BS::DisposeMatingOperators(QMatingOperator **qMoPs)
{
	for (int i = 0; i < kNumBasicMatingOps; i++)
		if (qMoPs[i] != NULL)
			delete qMoPs[i];
}

/**
 * Adjusts the raw genotype probabilities (1- and 2-locus) differently for use in
 * designs ending in a self or backcross operation. For selfing, we halve the probabilities
 * of all locus pairs of which at least one is a heterozygote, since CG's counting method
 * will count them twice, one for each phase. For both, we prepare to compute the conditional
 * probability of RH given LH genotype; this CP must be used in the HMM method but we want the
 * unconditional pmf of 2-locus genotype in the EspRec() method.
*/
void BJS_BS::AdjustProbsForDesign()
{
	double d;
	
	if (WasLastOpSelfOrIntercross()) // expand the phase-indifferent probabilities into phase-known ones.
	{
		mSingleLocusGenoProbs[pAA] = mSingleLocusGenoProbs[AA];
		mSingleLocusGenoProbs[pAa] = mSingleLocusGenoProbs[paA] = mSingleLocusGenoProbs[Aa] * 0.5;
		mXoverGenoProbsM->rowTimesScalarEquals((double *)gPMF_ScalerV); // halve probs that will be double-counted owing to indeterminate phase
		mGenotypeCPPolyV->rowTimesScalarEquals((double *)gPMF_ScalerV);// in xover calc as well as in the HMM routines
	}
	for (int i = 0; i < NUM_PHASED_DIPLOID_GENOTYPES; i++)
	{
		d = mSingleLocusGenoProbs[i];
		mSingleLocusGenoInvProbs[i] = d > 0.0 ? 1.0/d : 0.0;
	}
	ConditionGenotypeProbs();	// turn the unconditional 2-locus probs into (prob of RH genotype given the LH)
}

/*
*	We have 2-locus genotype probs, but we want the prob of the R locus given the L one, so we
*	divide these joint probs by the marginal over the R (namely the 1-locus prob of the L).
*/
void BJS_BS::ConditionGenotypeProbs()
{
	double d;
	for (int i = 0; i < NUM_DIPLOID_GENOTYPE_COMBOS; i++)
	{
		d = mSingleLocusGenoInvProbs[gConditionerV[i]];	// the conditioning factor for this genotype class
		mGenotypeCPPolyV->extractPoly(i, 0)->timesScalarEquals(d);
	}
}

bool BJS_BS::WasLastOpSelfOrIntercross() const
{
	int lastOp = strlen(mDesignString) - 1;
	char ch = mDesignString[lastOp];
	return (ch == cSelfingSymbol || ch == cIntercrossingSymbol);
}

//-----------------------------------------------------------------------------
// Calcule fraction de rec. et
// LOD scores pour deux marqueurs
//-----------------------------------------------------------------------------
// Param?tres : 
// - les deux numeros de marqueurs, on doit avoir m1 < m2
// - epsilon
// - la fraction de recombinant(i,o)
// Valeur de retour : le lod
//-----------------------------------------------------------------------------

double BJS_BS::ComputeOneTwoPoints(int m1, int m2, double epsilon, double *fr) const
{
  int data;
  double logLike = -10000.0, prevLogLike = -100000.0, logLikeInd, theta, thetaHat;
  bool converged = false;
  
  CountPhenos(m1, m2); // Clear & populate the marker-phenotype frequency array
        
  logLikeInd = LogInd(m1, m2, &data);
  if (logLikeInd == kInvalidLogLike) // there were no data
  {
    *fr = EM_MAX_THETA;
    return 0.0;
  }
  
  theta = kInitialThetaEstimate;
// Modified the iteration test, since it can happen that thetaHat increases and
// likelihood decreases, when the likelihood curve is shallow. If likelihood is
// increasing, we stop iterating and return the last values for theta and loglike.
// However this gives almost identical estimates to CG's version.
  do {
  	if (logLike < prevLogLike)
  	{
  		converged = true;
  		logLike = prevLogLike;
  	}
  	else
    {
    	prevLogLike = logLike;
	    thetaHat = EspRec(m1, m2, theta, &logLike);
	    if (thetaHat > EM_MAX_THETA)
	    	thetaHat = EM_MAX_THETA;
			theta = thetaHat;
			converged = (fabs(logLike - prevLogLike) <= epsilon);
    }
  } while (!converged);
  
  *fr = theta;
  return(logLike - logLikeInd);
}

/**
*	Determines how many crossovers could have occurred in one descent in the
*	current mating design. Crossovers can follow only generations in which double
* heterozygotes are possible, beginning with the F1. In each selfing generation
*	we may have 2, plus at most 1 in any series of backcross runs not followed by a
* self. So we just count the selfing steps, then add 1 for any trailing BC steps.
* Updated to include intercrossing as a selfing step, CN 2.6.06
*/
void BJS_BS::ComputeMaxPossibleCrossovers()
{	
	int generation, generations, selfCount = 0, lastSelfIndex = -1;
	char ch;

	generations = strlen(mDesignString);

	for (generation = 0; generation < generations; generation++)
		if ((ch = mDesignString[generation]) == cSelfingSymbol
				|| ch == cIntercrossingSymbol)
	{
		lastSelfIndex = generation;
		selfCount++;
	}
	mMaxPossibleXovers = 2 * selfCount + (generations > lastSelfIndex + 1);
}

//-----------------------------------------------------------------------------
// Compatibilite des observations entres individus
// Applies the methods from BJS_IC and BJS_BC, depending on whether
// the mating sequence ends with a self or a BC operation. CN 11.20.05
//-----------------------------------------------------------------------------
int BJS_BS::Compatible(int numarq1, int numarq2) const
{	
  int i;
  enum Obs Obs1, Obs2;
  
  if (WasLastOpSelfOrIntercross())
  	return BJS_IC::Compatible(numarq1, numarq2);// else last operation was a backcross
  // return BJS_BC::Compatible(numarq1, numarq2);// no, we would need a BJS_BC object!
	if ((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu & BitJeu))
	{
		// on compare si les donnees sont compatibles en chaque point
		for (i = 1; i <= TailleEchant; i++)
		{
			Obs1 = GetEch(numarq1, i);
			Obs2 = GetEch(numarq2, i);
			if (!((Obs1 == Obs1111) || (Obs2 == Obs1111) || (Obs1 == Obs2)))
			  return 0;
		}
	}
  return 1;
}

// Blank and then populate the geno frequency array per CG's standard method
void BJS_BS::CountPhenos(int m1, int m2) const
{
  for (int leftGeno = 0; leftGeno < Num_CG_Phenotypes; leftGeno++)
    for (int rightGeno = 0; rightGeno < Num_CG_Phenotypes; rightGeno++)
    	mPhenoFreqs[leftGeno][rightGeno] = 0;
  
  for (int i = 1; i <= TailleEchant; i++)
		mPhenoFreqs[GetEch(m1, i)][GetEch(m2, i)]++;
}

//-----------------------------------------------------------------------------
// Retourne l'esp?rance du nombre de recombinants.
//-----------------------------------------------------------------------------
// Param?tres :
// - indice du premier marqueur dans le jeu
// - indice du second marqueur dans le jeu
// - le theta
// - le nombre de donn?es informatives
// - le loglike (o)
// Valeur de retour : l'esp?rance du nombre de recombinaisons.
// Changed by CN: now return the updated recombination estimate. The purpose is
// efficiency in calculation. We calculate in the method both the number of
// informative meioses and the expansion factor. We also use the joint pmf of
// crossover number and genotype, where CG uses the conditional distribution of
// crossover number and RH genotype given LH genotype.
//-----------------------------------------------------------------------------
 
double BJS_BS::EspRec(int m1, int m2, double theta, double *loglike) const
{
  int leftGeno, rightGeno, leftPheno, rightPheno, possibleL, possibleR,
  	maxDegree, classFreq, informatives = 0, pmfGenotypeRow;
  double ENRec = 0.0, locLogLike = 0.0, indivExpect, sumGenoXoverProbs, RIMScaleFactor;
  double *powers, *probs;
  QPolynomialMatrix *cumulXoverProbsV;
  
  maxDegree = mXoverGenoProbsM->getMaxDegree();
  powers = new double[maxDegree + 1];	// the + 1 is for the zeroth degree
  probs = new double[mMaxPossibleXovers + 1];	// the + 1 is for the zero-xover case
	QArrayUtils::FillOutPowersVector(theta, maxDegree, powers);
  cumulXoverProbsV = new QPolynomialMatrix(mMaxPossibleXovers + 1, 1, maxDegree);
// contains a probability expression for each crossover number in the current marker-pair phenotype.
// A generalization of CG's ProbCross array (compare class BJS_IC)

  for (leftPheno = 1; leftPheno < Num_CG_Phenotypes - 1; leftPheno++) // observation a gauche, pas les inconnus
    for (rightPheno = 1; rightPheno < Num_CG_Phenotypes - 1; rightPheno++) // observation a droite, pas les inconnus
      // first and last indices point at nothing we need
	{
		classFreq = mPhenoFreqs[leftPheno][rightPheno];
		if (!classFreq)// Si il n'existe pas des occurrences de ce type de paires d'observ.
			continue;
		informatives += classFreq;	// need to know how many individuals had phenotypes for this marker pair
		cumulXoverProbsV->timesScalarEquals(0.0); // zero this for current phenotype's joint geno/xover prob accumulation
	  // on accumule les proba de 0, 1, 2 ... recombinaisons
	  for (leftGeno = 0; leftGeno < mLocalNPossibles[leftPheno]; leftGeno++)	// loop over all possible LH genotypes
	  {																							// consistent with the observed phenotype
	   	possibleL = Possibles[leftPheno][leftGeno];
	    for (rightGeno = 0; rightGeno < mLocalNPossibles[rightPheno]; rightGeno++)	// ditto for RH genotypes
      {
      	possibleR = Possibles[rightPheno][rightGeno];
			  pmfGenotypeRow = GenotypeDirectorArr[possibleL][possibleR]; // find index into our column vector of genotype probs
        for (int xoverNum = 0; xoverNum <= mMaxPossibleXovers; xoverNum++)
					cumulXoverProbsV->extractPoly(xoverNum, 0)->plusEquals(mXoverGenoProbsM->extractPoly(pmfGenotypeRow, xoverNum));
			}	// note that the above addition is of coefficients of polynomials in theta. Sum's evaluated below.
	 	}
	  // on en deduit l'esperance du nombre de rec. et donc l'estime
	  // de la fraction de rec. et la vraisemblance 2 pt
	  cumulXoverProbsV->evaluateByColumn(powers, probs, 0);
	  indivExpect = NormalizeAndExpect(probs, mMaxPossibleXovers + 1, sumGenoXoverProbs, CALC_EXPECT);
	  ENRec += classFreq * indivExpect; // increment the grand total of expected crossovers
	  locLogLike += log10(sumGenoXoverProbs) * classFreq;	// increment the log likelihood
	}
	RIMScaleFactor = mRIM_factorP->evaluate(powers);
  delete [] powers;
  delete cumulXoverProbsV;
  delete [] probs;
  *loglike = locLogLike;
 	if (informatives == 0) // all marker phenotypes were missing
		return kInvalidTheta;
  return ENRec/(RIMScaleFactor * informatives);
}

//-----------------------------------------------------------------------------
// Fusion des observations entres individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
// CN copied from BJS_IC and BJS_BC, same as for Compatible(). 11.20.05
//-----------------------------------------------------------------------------
void BJS_BS::Merge(int numarq1, int numarq2) const
{
  int i;
  enum Obs TheObs;
  // les deux marqueurs sont ils definis dans le jeux de donnees?
  if (!((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu & BitJeu)))
  	return;
  if (WasLastOpSelfOrIntercross())	 // Oui, on va fusionner dans le premier
  	BJS_IC::Merge(numarq1, numarq2);
	else	// last op was a backcross. But we can't call BJS_BC::Merge(numarq1, numarq2);
	// because it's not a static method. We have to reproduce the code, below.
  {
    for (i = 1; i <= TailleEchant; i++)
    {
			if ((TheObs = Echantillon[numarq1][i]) == Obs1111)
			  TheObs = Echantillon[numarq2][i];
			Echantillon[numarq1][i] = TheObs;
    }
	}
}

//-----------------------------------------------------------------------------
// Param?tres :
// - the index of the RH marker in the current map order 
// - la carte 
// Valeur de retour : le loglike 
//-----------------------------------------------------------------------------

double BJS_BS::ComputeSourceTo(int Ind, const Carte *map) const
{
  int marq, leftGeno, rightGeno, leftPheno, rightPheno, possibleL, possibleR,
  	phenoPossibles, genoRow;
  int *ordre = map->ordre;
  double norm, loglike = 0.0, sumSingleLocusProbs = 0.0;
  double *markerGenoCPs, *curSourceTo, *prevSourceTo;  
  double prevSource, genoCP; // for debugging

  rightPheno = GetEch(ordre[0], Ind);
// Initialize chain with the conditional probs of the possible genotypes given marker phenotype
	phenoPossibles = mLocalNPossibles[rightPheno];
  for (int i = 0; i < phenoPossibles; i++)
  	sumSingleLocusProbs += mSingleLocusGenoProbs[Possibles[rightPheno][i]];
  	
  loglike += log10(sumSingleLocusProbs);// added on TS's reminder! CN 1.12.06
	curSourceTo = mSourceTo[0];
  for (int i = 0; i < phenoPossibles; i++)
    curSourceTo[Possibles[rightPheno][i]] =
    	mSingleLocusGenoProbs[Possibles[rightPheno][i]]/sumSingleLocusProbs;

  for (marq = 1; marq < map->NbMarqueur; marq++)
  {	// initialize array of state probability accumulators
  	prevSourceTo = curSourceTo;
    curSourceTo = mSourceTo[marq];
    for (int i = 0; i < NUM_PHASED_DIPLOID_GENOTYPES; i++)
    	curSourceTo[i] = 0.0;

    leftPheno = rightPheno;
    rightPheno = GetEch(ordre[marq], Ind);
		markerGenoCPs = mMarkerGenoCPs[marq - 1];
    for (leftGeno = 0; leftGeno < mLocalNPossibles[leftPheno]; leftGeno++)
		{
			possibleL = Possibles[leftPheno][leftGeno];
		  prevSource = prevSourceTo[possibleL];
		  for (rightGeno = 0; rightGeno < mLocalNPossibles[rightPheno]; rightGeno++)
		  {
		  	possibleR = Possibles[rightPheno][rightGeno];
		  	genoRow = GenotypeDirectorArr[possibleL][possibleR];
		  	genoCP = markerGenoCPs[genoRow];
		    curSourceTo[possibleR] += prevSource * genoCP;
		  }
		}
	  NormalizeAndExpect(curSourceTo, NUM_PHASED_DIPLOID_GENOTYPES, norm, DONT_CALC_EXPECT);	// normalize only
	  if (norm > 1e-100)	// norm (the sum of the probabilities) can be 0 in some cases, and we don't like log(0).
    	loglike += log10(norm); // increment the likelihood by that of this marker phenotype given those to the left
  }	/* For example, consider marker data ordered in this way:
  m1 A A A
  m2 A _ A
  m3 A H A
  
  Individual 2 will give a multipoint likelihood of 0, since r(M1 M2) = 0 and r(M2 M3) = 0.
  */
  return loglike;
}

//-----------------------------------------------------------------------------
// Param?tres : 
// - the index of the RH marker in the current map order 
// - la carte
// vector of expected crossover numbers for each interval
// Valeur de retour : 
//-----------------------------------------------------------------------------

double BJS_BS::ComputeToSink(int Ind, const Carte *map) const
{
  int marq, nbMarq, leftGeno, rightGeno, leftPheno, rightPheno, possibleL, possibleR,
  	genoRow, phenoPossibles;
  int *ordre = map->ordre;
  double norm, loglike = 0.0, rightSink, genoProb, sumSingleLocusProbs = 0.0;
  double *markerGenoCPs, *curToSink, *prevToSink;  

	nbMarq = map->NbMarqueur;
  leftPheno = GetEch(ordre[nbMarq - 1], Ind);

	phenoPossibles = mLocalNPossibles[leftPheno];
  for (int i = 0; i < phenoPossibles; i++)
  	sumSingleLocusProbs += mSingleLocusGenoProbs[Possibles[leftPheno][i]];
// Initialize chain with the conditional probs of the possible genotypes given marker phenotype
	curToSink = mToSink[nbMarq - 1];
  for (int i = 0; i < phenoPossibles; i++)
    curToSink[Possibles[leftPheno][i]] = // 1.0; // retain only for testing (original CG code used this number)
    	mSingleLocusGenoProbs[Possibles[leftPheno][i]]/sumSingleLocusProbs;

 // loglike += log10(sumSingleLocusProbs);// added on TS's reminder! CN 1.12.06
 // but unused since we don't need the loglike from the reverse pass
 
  for (marq = nbMarq - 2; marq >= 0; marq--)
  {
		prevToSink = curToSink;
  	curToSink = mToSink[marq];
    for (int i = 0; i < NUM_PHASED_DIPLOID_GENOTYPES; i++)
    	curToSink[i] = 0.0;

		markerGenoCPs = mMarkerGenoCPs[marq];
    rightPheno = leftPheno;
    leftPheno = GetEch(ordre[marq], Ind);

	  for (rightGeno = 0; rightGeno < mLocalNPossibles[rightPheno]; rightGeno++)
		{
		  possibleR = Possibles[rightPheno][rightGeno];
	  	for (leftGeno = 0; leftGeno < mLocalNPossibles[leftPheno]; leftGeno++)
	    {
				possibleL = Possibles[leftPheno][leftGeno];
				genoRow = GenotypeDirectorArr[possibleL][possibleR];
				genoProb = markerGenoCPs[genoRow];
				rightSink = prevToSink[possibleR];
	    	curToSink[possibleL] += rightSink * genoProb;
			}
		}
	  NormalizeAndExpect(curToSink, NUM_PHASED_DIPLOID_GENOTYPES, norm, DONT_CALC_EXPECT);	// normalize only
    // loglike += log10(norm);	// leave this unused calculation only for debugging CN 1.7.06
  }
  return loglike;
}

//-----------------------------------------------------------------------------
// Etape elementaire d' ? Expectation ? de EM.
// For one individual, computes expected crossovers in each interval and adds them
// to the cumulative (over all individuals) expected crossovers in that interval.
// Does this by summing the full path probabilities involving each crossover number,
// then normalizing them and taking the expectation.
//-----------------------------------------------------------------------------
// Param?tres : 
// - l'individu 
// - la carte
// - le vecteur d'expectation
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BS::ComputeOneExpected(int Ind, 
				const Carte* map, 
				double *expected) const
{
	int leftGeno, rightGeno, possibleL, possibleR, pmfGenotypeRow, maxDegree;
	int *ordre = map->ordre;
	int leftPheno, rightPheno = GetEch(ordre[0], Ind);
	double sumPathsProb, RIMScaleFactor, leftConditioningFactor, *powers, *cumulXoverProbs;
	double *curSourceTo, *curToSink;
	double probFromSource, probToSink, probLR, indivExpect;	// for debugging only
	QPolynomialMatrix *cumulXoverProbsV, *weightedXProbsV;

	maxDegree = mXoverGenoProbsM->getMaxDegree();
  cumulXoverProbsV = new QPolynomialMatrix(mMaxPossibleXovers + 1, 1, maxDegree);
  weightedXProbsV = new QPolynomialMatrix(mMaxPossibleXovers + 1, 1, maxDegree); // weighting will be by L & R probs
  cumulXoverProbs = new double[mMaxPossibleXovers + 1];	// the + 1 is for the zero-xover case

 	for (int marq = 0; marq < map->NbMarqueur - 1; marq++)
	{
		curSourceTo = mSourceTo[marq];
		curToSink = mToSink[marq + 1];
		cumulXoverProbsV->timesScalarEquals(0.0); // zero this for next phenotype's joint geno/xover prob accumulation
		powers = mThetaPowers[marq];
		leftPheno = rightPheno;
		rightPheno = GetEch(ordre[marq + 1], Ind);
	
		for (leftGeno = 0; leftGeno < mLocalNPossibles[leftPheno]; leftGeno++)
		{	// loop over possible genotype combinations (the hidden states)
			possibleL = Possibles[leftPheno][leftGeno];
			leftConditioningFactor = mSingleLocusGenoInvProbs[possibleL];	// added CN 1.8.06
			for (rightGeno = 0; rightGeno < mLocalNPossibles[rightPheno]; rightGeno++)
			{
			  possibleR = Possibles[rightPheno][rightGeno];
			  probFromSource = curSourceTo[possibleL];
			  probToSink = curToSink[possibleR];
			  probLR = probFromSource * probToSink;
			  if (probLR == 0.0)	// a transition (and path) is excluded, so skip further calculation
			  	continue;
			  pmfGenotypeRow = GenotypeDirectorArr[possibleL][possibleR];
			  probLR *= leftConditioningFactor; // mXoverGenoProbsM is a pmf. We need
			  // Pr(RH geno, xoverNum | LH geno), so must divide by marginal prob of LH geno CN 1.8.06
				weightedXProbsV->timesScalarEquals(0.0); // zero for next genotype combination
				
        for (int xoverNum = 0; xoverNum <= mMaxPossibleXovers; xoverNum++) // loop over possible xover numbers
					weightedXProbsV->extractPoly(xoverNum, 0)->plusEquals
						(mXoverGenoProbsM->extractPoly(pmfGenotypeRow, xoverNum));
				weightedXProbsV->timesScalarEquals(probLR);
				cumulXoverProbsV->plusEqualsWithRotate(weightedXProbsV, 0);// (there's no rotate here; just using the adder)
			}
		}	// end of loop over possible genotype combinations
	  cumulXoverProbsV->evaluateByColumn(powers, cumulXoverProbs, 0);
	  indivExpect = NormalizeAndExpect(cumulXoverProbs, mMaxPossibleXovers + 1, sumPathsProb, CALC_EXPECT);
	  RIMScaleFactor = mRIM_factorP->evaluate(powers);	// correct for extra RIM chances before dividing in Maximize() by NbMeiose
		expected[marq] += indivExpect/RIMScaleFactor; // increment expected crossovers in this interval from this individual
	}
  delete [] cumulXoverProbs;
	delete weightedXProbsV;
	delete cumulXoverProbsV;
}

/**
*	Normalizes the probs array to its sum and returns that in probSum. If expectation is
*	desired, returns the expectation where the value of an outcome is its index.
*	Used for calculating expected number of crossovers for an array of probabilities of two-locus
*	genotypes corresponding to a fully or partially informative two-locus marker phenotype.
*	Sum of these probabilities is the probability of the marker phenotype.
*/
double BJS_BS::NormalizeAndExpect(double *probs, int numProbs, double &probSum, bool calcExpect) const
{
	double sum = 0.0, expect = 0.0;;
	int i;
	
	for (i = 0; i < numProbs; i++)	// summing step
		sum += probs[i];
	probSum = sum;
	if (sum > 1e-100)	// we now (7.7.06) allowed 0 likelihood of each genotype at some positions in the chain.
	// See ComputeSourceTo() for an example of this, when there are missing data.
	for (i = 0; i < numProbs; i++)	// normalization step
		probs[i] /= sum;

	if (calcExpect)
	{
		for (i = 1; i < numProbs; i++)	// expectation-calculating step
		expect += i * probs[i];
	}
	return expect;
}

/**
 * Estimates of theta have just been provided. Compute arrays of powers and
 * genotype probabilities for use in the E step of multipoint likelihood maximization.
 * Called in ComputeExpected()
 */
void BJS_BS::UpdateEStepArrays(const Carte *map)
{
	int maxGDegree, maxDegree;
	double *probs, *powers;
	
	maxDegree = mXoverGenoProbsM->getMaxDegree();	// need a powers vector long enough to evaluate...
	maxGDegree = mGenotypeCPPolyV->getMaxDegree();	// ... both genotype and xover probs
	if (maxGDegree > maxDegree)
		maxDegree = maxGDegree;

	for (int marq = 0; marq < map->NbMarqueur - 1; marq++)
	{
		probs = mMarkerGenoCPs[marq];
		powers = mThetaPowers[marq];
		QArrayUtils::FillOutPowersVector(map->tr[marq], maxDegree, powers);
		mGenotypeCPPolyV->evaluateByColumn(powers, probs, 0);
	}
}

//-----------------------------------------------------------------------------
//  Pr?paration des structures pour la programmation dynamique de EM.
// CN: I can see the FB algorithm, but DP? We're not trying to induce the
// hidden genotypes, only maximize the likelihood.
//-----------------------------------------------------------------------------
// Param?tres : 
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BS::PreparEM(const Carte *map)
{
  //  int *ordre = map->ordre;
	int nbMarq = map->NbMarqueur, maxGDegree, maxDegree;
		
	maxDegree = mXoverGenoProbsM->getMaxDegree();	// need a powers vector long enough to evaluate...
	maxGDegree = mGenotypeCPPolyV->getMaxDegree();	// ... both genotype and xover probs
	if (maxGDegree > maxDegree)
		maxDegree = maxGDegree;
	mSourceTo = new doublePtr[nbMarq];
	mToSink = new doublePtr[nbMarq];
	mMarkerGenoCPs = new doublePtr[nbMarq];	// though we really need only nbMarq - 1
	mThetaPowers = new doublePtr[nbMarq];			// ditto; see below
	for (int marq = 0; marq < nbMarq; marq++)
	{
	  mSourceTo[marq] = new double[NUM_PHASED_DIPLOID_GENOTYPES];	// Note that we are swapping dimensions wrt CG's native SourceTo[]
	  mToSink[marq] = new double[NUM_PHASED_DIPLOID_GENOTYPES];		// ditto for mToSink[]. Reason: to boost addressing efficiency.
	  // of the following we really need only nbMarq - 1 instances. The extra is allocated
	  // only because it's convenient to put all the mem allocation in the same loop.
		mMarkerGenoCPs[marq] = new double[NUM_DIPLOID_GENOTYPE_COMBOS];	// for computing HMM forward & backward probs
		mThetaPowers[marq] = new double[maxDegree + 1];	// for evaluating polynomials of genotype & crossover probs
	}
}

//-----------------------------------------------------------------------------
//  nettoyage des structures pour la programmation dynamique de EM.
//-----------------------------------------------------------------------------
// Param?tres :
// - la carte.
// Valeur de retour : 
//-----------------------------------------------------------------------------

void BJS_BS::NetEM(const Carte *map)
{
	for (int marq = 0; marq < map->NbMarqueur; marq++)
	{
	  delete mSourceTo[marq];
	  delete mToSink[marq];
		delete [] mMarkerGenoCPs[marq];
		delete [] mThetaPowers[marq];
	}
	delete [] mMarkerGenoCPs;
	delete [] mThetaPowers;
	delete [] mSourceTo;
	delete [] mToSink;
}

/**
*	This is implemented only because the virtual method must be overridden. So we
*	just wrap a different method with it.
*/
 double BJS_BS::LogInd(int m1, int m2, int *nbdata) const
 {
 	double unusedTheta, logLikeInd;

	unusedTheta = EspRec(m1, m2, EM_MAX_THETA, &logLikeInd); // want the loglike under H0: no linkage
	if (logLikeInd == kInvalidTheta)
		return kInvalidLogLike;
	return logLikeInd;
 }
