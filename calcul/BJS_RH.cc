//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJS_RH.cc,v 1.33.2.4 2012-06-14 13:42:41 dleroux Exp $
//
// Description : BJS_RH.
// Divers :
//-----------------------------------------------------------------------------

#include "BJS_RH.h"
#include "BJS_RHD.h"
#include "BJS_RHE.h"
#include "System.h"
#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJS_RH::BJS_RH() : BioJeuSingle()
{
  NbMeiose = TailleEchant;

  SourceTo[0] = NULL;
  PairsHead = NULL;
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - le type
// - le nombre de marqueurs
// - la taille de l'echantillon
// - le champ de bit
// - la matrice de l'�chantillon
// Valeur de retour :
//-----------------------------------------------------------------------------

BJS_RH::BJS_RH(CartaGenePtr cartag,
	       int id,
	       charPtr nomjeu,
	       CrossType cross,
	       int nm, 
	       int taille,
	       int bitjeu,
	       int *indmarq,
	       Obs **echantil)
  :BioJeuSingle(cartag,
		id,
                nomjeu,
		cross, 
		nm, 
		taille, 
		bitjeu, 
		indmarq, 
		echantil)
{
  NbMeiose = TailleEchant;

  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;

  SourceTo[0] = NULL;
  PairsHead = NULL;

  ComputeTwoPoints();
}

// DL
// FIXME : vulgaire copier-coller
BJS_RH::BJS_RH(const BJS_RH& b)
	: BioJeuSingle(b)
{
  NbMeiose = TailleEchant;
  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;
  SourceTo[0] = NULL;
  PairsHead = NULL;
  ComputeTwoPoints();
}

BJS_RH::BJS_RH(const BJS_RHE& b)
	: BioJeuSingle(b)
{
  NbMeiose = TailleEchant;
  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;
  SourceTo[0] = NULL;
  PairsHead = NULL;
  ComputeTwoPoints();
}

BJS_RH::BJS_RH(const BJS_RHD& b)
	: BioJeuSingle(b)
{
  NbMeiose = TailleEchant;
  Em_Max_Theta = EM_MAX_THETA_HR;
  Em_Min_Theta = EM_MIN_THETA_HR;
  SourceTo[0] = NULL;
  PairsHead = NULL;
  ComputeTwoPoints();
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJS_RH::~BJS_RH()
{
 
}
//-----------------------------------------------------------------------------
// Compatibilite des observations entres individus
//-----------------------------------------------------------------------------
int BJS_RH::Compatible(int numarq1, int numarq2) const
{
  int i;
  enum Obs Obs1, Obs2;

  for (i = 1; i<= TailleEchant; i++) {
    Obs1 = GetEch(numarq1,i);
    Obs2 = GetEch(numarq2,i);
    if (!((Obs1 == Obs1111) || (Obs2 == Obs1111) || (Obs1 == Obs2)))
      return 0;
  }
  return 1;
}

//-----------------------------------------------------------------------------
// Fusion des observations entres individus
// Cela suppose que la compatibilite a ete verifie AVANT !!!
//-----------------------------------------------------------------------------
void BJS_RH::Merge(int numarq1, int numarq2) const
{
  int i;
  enum Obs TheObs;

  // les deux marqueurs sont ils definis dans le jeux de donnees ?
  if ((Cartage->markers[numarq1].BitJeu & BitJeu) && (Cartage->markers[numarq2].BitJeu &
 BitJeu))
    {
      // Oui, on va fusionner dans le premier
      for (i = 1; i<= TailleEchant; i++) {
        if ((TheObs = GetEch(numarq1, i)) == Obs1111)
          TheObs = GetEch(numarq2, i);
        //SetEch(numarq1, i, TheObs);
        Echantillon[numarq1][i] = TheObs;
      }
    }
}

//-----------------------------------------------------------------------------
//  Etape d' � Expectation � de EM.
//-----------------------------------------------------------------------------
// Param�tres : la carte
// - la carte
// - le vecteur d'Expectation(i,o)
// Valeur de retour : le loglike
//-----------------------------------------------------------------------------
double BJS_RH::ComputeExpected(const Carte* map, double *expected)
{

  int i,j,k,l;
  double loglike = 0.0;
  PairRH *CurPair = PairsHead;
  double pb,pnb,pbn;

  int NbM = map->NbMarqueur;

  while (CurPair) {
    i = CurPair->Left;
    j = CurPair->Right;
    
    k = CurPair->NumOcc[0];     // 00
    l = CurPair->NumOcc[1];     // 01
    
    SourceTo[1][i]= 1.0;
    SourceTo[0][i]= 0.0;
    ComputeSourceTo(i, j, map);
    
    ToSink[1][j]= 1.0;
    ToSink[0][j]= 0.0;
    ComputeToSink(i, j, map);
    
    if (k)	{
      UpdateExpected(i, j, 0, 0, k, map, expected); 
      loglike  += k*log10(1.0-SourceTo[0][j]);
    }
    
    if (l) {
      UpdateExpected(i, j, 0, 1, l, map, expected); 
      loglike  += l*log10(SourceTo[0][j]);
    }
    
    k = CurPair->NumOcc[2];     // 10
    l = CurPair->NumOcc[3];     // 11
    
    if (k)	{
      UpdateExpected(i, j, 1, 0, k, map, expected); 
      loglike  += k*log10(1.0-SourceTo[1][j]);
    }
    
    if (l)	{
      UpdateExpected(i, j, 1, 1, l, map, expected); 
      loglike  += l*log10(SourceTo[1][j]);
    }
    CurPair = CurPair->Next;
  }
  
  // Penalite de loglike pour les extremites gauches connues 
  
  loglike += log10(map->ret)*FlankingL[0][1]+
    log10(1.0-map->ret)*FlankingL[0][0];
  // et mise a jour des ERetained/Breaks
  // Les Retained sont les casses et retenus
  // les Break sont les casses.
  ERetained += FlankingL[0][1];
  EBreak+= TailleEchant-Unknown;
  
  for (i = 1; i<map->NbMarqueur; i++) {
    
    if (FlankingL[i][0] || FlankingL[i][1]) {
      SourceTo[1][0] = map->ret;
      SourceTo[0][0] = 1.0-map->ret;
      ComputeSourceTo(0,i,map);
      
      ToSink[1][i] = 1.0;
      ToSink[0][i] = 0.0;
      ComputeToSink(0,i,map);
      
      if (FlankingL[i][0]) {
	UpdateExpected(0, i, 1, 0, FlankingL[i][0], map, expected); 
	loglike  += FlankingL[i][0]*log10(1.0-SourceTo[1][i]);
	pb = map->ret*ToSink[0][0]; // proba retention
	pnb = (1.0 - map->ret)*(1.0- ToSink[0][0]);
	pbn = pb/(pb+pnb);
	ERetained += pbn*FlankingL[i][0]; // pour le 1er marqueur
      }
      if (FlankingL[i][1]) {
	UpdateExpected(0, i, 1, 1, FlankingL[i][1], map, expected); 
	loglike  += FlankingL[i][1]*log10(SourceTo[1][i]);
	pb = map->ret*ToSink[1][0]; // proba retention
	pnb = (1.0 - map->ret)*(1.0- ToSink[1][0]);
	pbn = pb/(pb+pnb);
	ERetained += pbn*FlankingL[i][1]; // pour le 1er marqueur
      }
    }
  }
  
  for (i = 0; i<map->NbMarqueur-1; i++)  {
    
    if (FlankingR[i][0] || FlankingR[i][1]) {
      SourceTo[1][i] =1.0;
      SourceTo[0][i] =0.0;
      ComputeSourceTo(i, map->NbMarqueur-1, map);
      
      ToSink[1][map->NbMarqueur-1] = 0.5;
      ToSink[0][map->NbMarqueur-1] = 0.5;
      ComputeToSink(i, map->NbMarqueur-1, map);
      
      if (FlankingR[i][0])
	UpdateExpected(i, map->NbMarqueur-1, 0, 1, FlankingR[i][0], map, expected); 
      
      if (FlankingR[i][1])
	UpdateExpected(i, map->NbMarqueur-1, 1, 1, FlankingR[i][1], map, expected); 
    }
  }
  
  for (i=0; i<map->NbMarqueur-1; i++) {
    // Cas 00
    pb = map->tr[i]*(1.0 - map->ret);        // proba de cassure
    pnb = 1.0 - map->tr[i];                  // proba de non cassure
    pbn = pb/(pb+pnb);                       // proba de cassure normalisee
    loglike += Known[0][i]*log10(pb+pnb);      
    expected[i] += Known[0][i]*pbn;
    EBreak += Known[0][i]*pbn;
    
    // Cas 10
    loglike += Known[2][i]*log10(pb);
    expected[i] += Known[2][i];              // Cassures sures
    EBreak += Known[2][i];                    // Idem, aucune retenues
    
    // Cas 01
    pb = map->tr[i]*map->ret;                // proba de cassure dans 01/11
    loglike += Known[1][i]*log10(pb);       
    expected[i] += Known[1][i];              // Cassures sures
    EBreak += Known[1][i];                   // Idem
    ERetained += Known[1][i];                // Retenues sures
    
    // Cas 11
    pbn = pb/(pb+pnb);
    loglike += Known[3][i]*log10(pb+pnb);                   
    expected[i] += Known[3][i]*pbn;
    EBreak += Known[3][i]*pbn;
    ERetained += Known[3][i]*pbn;
  } 
  return (loglike);
}

void BJS_RH::UpdateExpected(int a, 
			    int b, 
			    int l, 
			    int r,
			    int n, 
			    const Carte *map,
			    double *expected)
{
  int j;
  double pbreak,pnobreak,norm;
  double pret;
  
  int NbM = map->NbMarqueur;
  
  for (j=a+1; j<=b; j++) {
    pbreak = map->tr[j-1]*
      (1.0 - map->ret - ToSink[r][j] + 2*map->ret*ToSink[r][j]);
      
    pnobreak = (1 -  map->tr[j-1])*
      (1 - SourceTo[l][j-1] - ToSink[r][j] + 2*SourceTo[l][j-1]*ToSink[r][j]);
      
    pret = map->tr[j-1]*map->ret*ToSink[r][j];

    norm = pbreak + pnobreak;
    pbreak /= norm;
    pret /= norm;
    expected[j-1] += n*pbreak;
    EBreak += n*pbreak;
    ERetained += n*pret;
  }
}

//-----------------------------------------------------------------------------
//  Cree un nouveau cons pour une paire de marqueurs
// encore inexistante si necessaire. Puis incremente le nombre de paires
// rec/non recombinantes.
//-----------------------------------------------------------------------------
// Param�tres : 
// - a,b : la paire de marqueurs
// - r   : 0 ou 1 alias non recombinant ou recombinant.
// Valeur de retour : 
//-----------------------------------------------------------------------------

inline void BJS_RH::IncrementPair(int a, int b, int l, int r)
{
  if (KnownPairs[a][b] == NULL)
    {
      PairRHPtr NewPair = new PairRH;
      
      NewPair->NumOcc[0] = 0;
      NewPair->NumOcc[1] = 0;
      NewPair->NumOcc[2] = 0;
      NewPair->NumOcc[3] = 0;
      NewPair->NumOcc[l*2+r] = 1;
      NewPair->Left = a;
      NewPair->Right = b;
      NewPair->Next = PairsHead;
      PairsHead = NewPair;
      KnownPairs[a][b] = NewPair; 
    }
  else
    KnownPairs[a][b]->NumOcc[l*2+r]++;
}

//-----------------------------------------------------------------------------
// Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite gauche (source)
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

inline void BJS_RH::ComputeSourceTo(int a, int b, const Carte *map)
{

  int i;

  for (i = a+1; i<= b; i++)
    {
      SourceTo[1][i] = SourceTo[1][i-1]*(1.0 - (map->tr[i-1]))+
	((map->tr[i-1]) * (map->ret));
      SourceTo[0][i] = SourceTo[0][i-1]*(1.0 - (map->tr[i-1]))+
	((map->tr[i-1]) * (map->ret));
    }
}


//-----------------------------------------------------------------------------
// Calcul des probabilites d'existence d'un chemin
// passant par un sommet depuis l'extremite droite (puit)
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

inline void BJS_RH::ComputeToSink(int a, int b, const Carte *map)
{
  int i;
  double norm =0.0;
    
  for (i = b-1; i >= a ; i--)
    {
      ToSink[1][i] = 
	map->tr[i]*((1 - map->ret)*(1 - 2*ToSink[1][i+1]))+ToSink[1][i+1];
      norm = (1 - 2*ToSink[1][i+1])*(1 - map->tr[i]*map->ret)+ToSink[1][i+1]+
	ToSink[1][i];
      ToSink[1][i] /= norm;

      ToSink[0][i] = 
	map->tr[i]*((1 - map->ret)*(1 - 2*ToSink[0][i+1]))+ToSink[0][i+1];
      norm = (1 - 2*ToSink[0][i+1])*(1 - map->tr[i]*map->ret)+
	ToSink[0][i+1]+ToSink[0][i];
	ToSink[0][i] /= norm;

    }
}
//-----------------------------------------------------------------------------
// Calcul du nombre d'occurrences de chque pattern (stat suffisantes
// pour le calcul de vraisemblance et des estimateurs de max. de
// vraisemblance 2pts)
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja alloue des 
// stats suffisantes
// Valeur de retour : aucune, effet de bord sur ss
//-----------------------------------------------------------------------------
void BJS_RH::Prepare2pt(int m1, int m2, int *ss) const
{
  /*printf("in Prepare2pt(%i,%i, @%p), with TE=%i\n", m1, m2, ss, TailleEchant);*/
  for (int m = 1; m <= TailleEchant; m++)    {
    
    if ((GetEch(m1, m) != Obs1111) && (GetEch(m2, m) != Obs1111)) {
      ss[GetEch(m1, m)*2 + GetEch(m2, m)]++;
    }
  }
}
//-----------------------------------------------------------------------------
// Calcul de l estimateur 2pt de max de vraisemblance
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des 
// stats suffisantes, le taux de cassure et de retention a estimer
// Valeur de retour : aucune, effet de bord sur les deux parametres
//-----------------------------------------------------------------------------
void BJS_RH::Estimate2pt(double *par, int *ss) const
{
  int tot = ss[3]+ss[0]+ss[2]+ss[1];
  double r,theta;

  if (tot == 0) { 
    par[1]= EM_MAX_THETA_HR;
    par[0] = EM_MAX_RETAIN;
    return;
  }

  // taux de retention
  r = (double)(ss[3]+ss[3]+ss[2]+ss[1])/(double)(2*tot);

  if (r > EM_MAX_RETAIN) r = EM_MAX_RETAIN;
  if (r < EM_MIN_RETAIN ) r = EM_MIN_RETAIN;
  // taux de cassure
  theta = (double)(ss[1]+ss[2])/(2*tot*r*(1-r));

  if (theta > EM_MAX_THETA_HR) theta = EM_MAX_THETA_HR;
  if (theta < EM_MIN_THETA_HR) theta =EM_MIN_THETA_HR;

  par[0] = theta;
  par[1] = r;
}
//-----------------------------------------------------------------------------
// Calcul de la vraisemblance 2pts
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs et le tableau deja calcule  des 
// stats suffisantes, le taux de cassure et de retention.
// Valeur de retour : le loglike
//-----------------------------------------------------------------------------
double BJS_RH::LogLike2pt(double *par, int *ss) const
{
  double loglike;

  loglike = ss[0]*log10((1-par[1])*(1-par[0]*par[1]));
  if (ss[1]+ss[2])
    loglike += (ss[1]+ss[2])*log10((1-par[1])*par[0]*par[1]);
  loglike += ss[3]*log10(par[1]*(1-par[0]*(1-par[1])));

  return loglike;
}
//-----------------------------------------------------------------------------
//  Calcul des distances, et estimations 2 points
//-----------------------------------------------------------------------------
// Param�tres : les deux marqueurs, la finesse de convergence et
// un endroit ou ecrite le taux de rec.
// Valeur de retour : le LOD
//-----------------------------------------------------------------------------
double BJS_RH::ComputeOneTwoPoints(int m1, 
				   int m2, 
				   double epsilon,
				   double *fr) const
{
  int n[4];
  int tot;
  double par[2];
  double loglike;

  n[0] = n[1] = n[2] = n[3] = 0;
  /*printf("Prepare2pt(%i,%i, @%p)\n", m1, m2, n);*/
  Prepare2pt(m1,m2,n);
  /*printf("Prepare2pt(%i,%i) => %i %i %i %i\n", m1, m2, n[0], n[1], n[2], n[3]);*/
  tot = n[0]+n[1]+n[2]+n[3];
  
  Estimate2pt(par,n);

  *fr = par[0];
  loglike = LogLike2pt(par,n);
  par[0] = 1.0;

  return loglike-LogLike2pt(par,n);
}	  
//-----------------------------------------------------------------------------
// Remplit les tableaux
//-----------------------------------------------------------------------------
// Param�tres : 
// - 
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------
void BJS_RH::PreparEM(const Carte *data)
{

 int mark;
 int i, j;
 int prevj, prevmark = -1;
 int NbMarqueur = data->NbMarqueur;
 int *ordre = data->ordre;

 SourceTo[0] = new double[NbMarqueur];
 SourceTo[1] = new double[NbMarqueur];
 
 ToSink[0] = new double[NbMarqueur];
 ToSink[1] = new double[NbMarqueur];
 
 Known[0] = new int[NbMarqueur-1];
 Known[1] = new int[NbMarqueur-1];
 Known[2] = new int[NbMarqueur-1];
 Known[3] = new int[NbMarqueur-1];
 
 FlankingL = new CoupleInt[NbMarqueur];
 FlankingR = new CoupleInt[NbMarqueur];
 
 KnownPairs = new PairRHPtrPtr[NbMarqueur];
 
 for (i = 0; i < NbMarqueur; i++)
   {
     KnownPairs[i] = new PairRHPtr[NbMarqueur];
      FlankingR[i][0] = FlankingR[i][1] = 0;
      FlankingL[i][0] = FlankingL[i][1] = 0;
      for (int j = 0; j < NbMarqueur; j++)
	KnownPairs[i][j] = NULL;
   }
 
 /* On initialise les structures (demi matrice pour KnownPairs !) */
 Unknown = 0;
  for (i = 0; i< NbMarqueur-1; i++)
   {
     Known[0][i] = Known[1][i] = 0;
     Known[2][i] = Known[3][i] = 0;
     for (j=i+1; j < NbMarqueur; j++)
       KnownPairs[i][j] = NULL;
   }
 
 /* pour chaque element de l'echantillon */
 for (i=1; i <= TailleEchant; i++)
   {
     prevj = -1;
     /* pour chaque marqueur successif, dans l'ordre de l'individu */
     for (j = 0; j< NbMarqueur; j++)
       {
	 mark = ordre[j];
	 /* si on connait le genotype */
	 if (GetEch(mark,i) != Obs1111)
	   {
	     /* que ce n'est pas un flanking */
	     if (prevj != -1)
	       {
		 /* si les deux marqueurs sont adjacents */
		 if ((j - prevj) == 1)
		   {
		     /* on a une paire de connus adjacents */
		     Known[GetEch(prevmark,i)*2+GetEch(mark,i)][prevj]++;
		   }
		 /* sinon, c'est un intervalle a traiter */
		 else IncrementPair(prevj, j, 
				    GetEch(prevmark,i),
				    GetEch(mark,i));
	       }
	     /* enfin, on comptabilise le flanking */
	     /* ici meme si j = 0 car il faudra compter la 
		proba de retention et la modif du loglike */
	     else FlankingL[j][GetEch(mark,i)]++;
	     
	     prevj = j;
	     prevmark = mark;
	   }
       }

     /* si on a un inconnu complet....*/
     if (prevj == -1) 
       Unknown++;

     /* Si a la fin, le marqueur precedent n'est pas le dernier, on a
	un flanking */
     else if (prevj != NbMarqueur-1) 
       FlankingR[prevj][GetEch(prevmark,i)]++;
   }
 NbMeiose = TailleEchant-Unknown;
}

void BJS_RH::NetEM(const Carte *data){

  PairRH *PairTemp;
  int NbMarqueur = data->NbMarqueur;

  delete [] ToSink[0];
  delete [] ToSink[1];
  delete [] SourceTo[0];
  delete [] SourceTo[1];
  
  delete [] FlankingL;
  delete [] FlankingR;
  
  for (int i = 0; i< NbMarqueur; i++) delete [] KnownPairs[i];
  
  delete [] KnownPairs;
  
  delete [] Known[0];
  delete [] Known[1];
  delete [] Known[2];
  delete [] Known[3];

  /* On libere la memoire  allouee */
  while (PairsHead != NULL)
    {
      PairTemp = PairsHead->Next;
      delete PairsHead;
      PairsHead = PairTemp;
    }
}
