//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BJM_OR.cc,v 1.21.2.3 2012-05-31 08:11:44 dleroux Exp $
//
// Description : BJM_OR.
// Divers :
//-----------------------------------------------------------------------------

#include "BJM_OR.h"

#include "Genetic.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "twopoint.h"

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------

BJM_OR::BJM_OR() : BioJeuMerged()
{
}

//-----------------------------------------------------------------------------
// Constructeur.
//-----------------------------------------------------------------------------
// Param�tres :
// - acc�s � l'ensemble des informations du syst�me. 
// - le num�ro du jeu
// - type de jeu de donn�e(Mor)
// - nm nombre de marqueurs.
// - bitjeu champ de bit du jeu de donn�es.
// - gauche jeu de donn�es.
// - droite jeu de donn�es. 
//-----------------------------------------------------------------------------

BJM_OR::BJM_OR(CartaGenePtr cartag,
		int id,
		CrossType cross,
		int nm, 
		int bitjeu,
		BioJeu *gauche, 
		BioJeu *droite) 
: BioJeuMerged(cartag,
		id,
		cross,
		nm, 
		bitjeu,
		gauche, 
		droite)
{
	int i = 0, k = 0;
	int j = 0, l = 0, nbcm = 0;
	int *indmarqpro = new int[Cartage->NbMarqueur + 1];

	for (i = 0; i <= Cartage->NbMarqueur; i++) indmarqpro[i] = 0;

	// recherche des couples communs au deux jeux
	i = 0;
	while (i < NbMarqueur) 
	{
		if (Cartage->markers[k].BitJeu & BitJeu) 
		{
			i++;
			j = i + 1;
			l = k + 1;
			while (j <= NbMarqueur) 
			{
				if (Cartage->markers[l].BitJeu & BitJeu) 
				{
					if (BJgauche->Couplex(k,l) &&
							BJdroite->Couplex(k,l))
					{		    
						if (!indmarqpro[k]) indmarqpro[k] = ++nbcm;
						if(!indmarqpro[l]) indmarqpro[l] = ++nbcm;
					}

					j++;
				}
				l++;
			}
		}	
		k++;
	} 

	// allocations

	//IndMarq = new int[l];
    IndMarq_cg2bj = indmarqpro;
    IndMarq_bj2cg = new int[NbMarqueur+1];
    for(int i=Cartage->NbMarqueur;i>=0;--i) {
    }
    for(int i=Cartage->NbMarqueur;i>=0;--i) {
    /*for(int i=0;i<=Cartage->NbMarqueur;++i) {*/
        IndMarq_bj2cg[IndMarq_cg2bj[i]] = i;
    }
    IndMarq_bj2cg[0] = 0;

	// DL
	/*TwoPointsLOD = new doublePtr[nbcm + 1];

	for (i = 0; i <= nbcm; i++) 
	{
		TwoPointsLOD[i]    = new double[nbcm + 1];

		for (int j = 0; j <= nbcm; j++) 
		{
			TwoPointsLOD[i][j] = 0.0;
		}
	}
	*/

	// remplissage

	TailleCharn = nbcm;

	//for (i = 0; i < l; i++) IndMarq[i] = indmarqpro[i];

	//delete [] indmarqpro;

	ComputeTwoPoints();
}

//-----------------------------------------------------------------------------
// Destructeur
//-----------------------------------------------------------------------------

BJM_OR::~BJM_OR()
{
}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//-----------------------------------------------------------------------------
// Param�tres : 
// - une carte
// - seuil de convergence
// Valeur de retour : le maximum de vraisemblance
//-----------------------------------------------------------------------------

double BJM_OR::ComputeEM(Carte *map)
{
	int i, j, k;
	int nbmg = 0, nbmd = 0;
	int *vmg, *vmd;
	Carte *cg, *cd, *mapwithtr;
	double coutg, coutd;

	NbEMCall++;

	// d�compte � gauche, d�compte � droite
	for (i = 0; i < map->NbMarqueur; i++)
	{
		if (Cartage->markers[map->ordre[i]].BitJeu &
				BJgauche->BitJeu) nbmg++;
		if (Cartage->markers[map->ordre[i]].BitJeu &
				BJdroite->BitJeu) nbmd++;
	}

	vmg = new int[nbmg];
	vmd = new int[nbmd];

	// remplissage � gauche, remplissage � droite
	j = 0;
	k = 0;
	for (i = 0; i < map->NbMarqueur; i++)
	{
		if (Cartage->markers[map->ordre[i]].BitJeu & 
				BJgauche->BitJeu) vmg[j++] = map->ordre[i];
		if (Cartage->markers[map->ordre[i]].BitJeu & 
				BJdroite->BitJeu) vmd[k++] = map->ordre[i];
	}

	// cr�ation des cartes
	cg = new Carte(Cartage, nbmg, vmg);
	cd = new Carte(Cartage, nbmd, vmd);

	// calcul   

	if (nbmg == 0)
		coutg = 0.0;
	else 
		coutg = BJgauche->ComputeEM(cg);

	if (nbmd == 0)
		coutd = 0.0;
	else 
		coutd =  BJdroite->ComputeEM(cd);

	map->coutEM = coutg + coutd;

	// Verrue pour la cartographie comparee (mergor BC ou RH avec Ordre)
	// recuperation des taux de recombinaison du BC ou RH
	if (((BJgauche->Cross==RH || BJgauche->Cross==BC) && BJdroite->Cross==Ordre) ||
			(BJgauche->Cross==Ordre && (BJdroite->Cross==RH || BJdroite->Cross==BC))) {
		if ((BJgauche->Cross==RH || BJgauche->Cross==BC) && BJdroite->Cross==Ordre) {
			mapwithtr = cg;
		} else {
			mapwithtr = cg;
		}
		if (mapwithtr->NbMarqueur == map->NbMarqueur) {
			for (j = 0; j < mapwithtr->NbMarqueur - 1; j++) {
				map->tr[j] = mapwithtr->tr[j];
				//  	sprintf(bouf,"%d: %.3f\n", j, map->tr[j]);
				//  	pout(bouf);
			}
		}
	}

	// nettoyage
	delete [] vmg;
	delete [] vmd;
	delete cd;
	delete cg;

	return  map->coutEM;
}

//-----------------------------------------------------------------------------
//  Calcul de la vraisemblance maximum par EM.
//  Une interface plus sophistiquee:
//  On converge jusqu'a epsilon1. Si la vraisemblance est inferieure a
//  threshold on laisse tomber, sinon on converge a epsilon2
//-----------------------------------------------------------------------------
// Param�tres : 
// - une carte
// - niveau a partir duquel on convergera a epsilon2
// Valeur de retour : la vraisemblance maximum.
//-----------------------------------------------------------------------------

double BJM_OR::ComputeEMS(Carte *map, 
		double threshold)
{
	int i, j, k;
	int nbmg = 0, nbmd = 0;
	int *vmg, *vmd;
	Carte *cg, *cd;
	double coutg, coutd;

	NbEMCall++;

	// d�compte � gauche, d�compte � droite
	for (i = 0; i < map->NbMarqueur; i++)
	{
		if (Cartage->markers[map->ordre[i]].BitJeu &
				BJgauche->BitJeu) nbmg++;
		if (Cartage->markers[map->ordre[i]].BitJeu &
				BJdroite->BitJeu) nbmd++;
	}

	vmg = new int[nbmg];
	vmd = new int[nbmd];

	// remplissage � gauche, remplissage � droite
	j = 0;
	k = 0;
	for (i = 0; i < map->NbMarqueur; i++)
	{
		if (Cartage->markers[map->ordre[i]].BitJeu & 
				BJgauche->BitJeu) vmg[j++] = map->ordre[i];
		if (Cartage->markers[map->ordre[i]].BitJeu & 
				BJdroite->BitJeu) vmd[k++] = map->ordre[i];    
	}

	// cr�ation des cartes
	cg = new Carte(Cartage, nbmg, vmg);
	cd = new Carte(Cartage, nbmd, vmd);

	// calcul   

	if (nbmg == 0)
		coutg = 0.0;
	else 
		coutg = BJgauche->ComputeEMS(cg, threshold);

	if (nbmd == 0)
		coutd = 0.0;
	else 
		coutd =  BJdroite->ComputeEMS(cd, threshold);

	map->coutEM = coutg + coutd;

	// nettoyage
	delete vmg;
	delete vmd;
	delete cd;
	delete cg;

	return  map->coutEM;
}
//-----------------------------------------------------------------------------
// Calcule fraction de rec. et
// LOD scores pour deux marqueurs
//-----------------------------------------------------------------------------
// Param�tres : 
// - les deux numeros de marqueurs, on doit avoir m1< m2
// - epsilon
// - la fraction de reconbinant(i,o)
// Valeur de retour : le lod
//-----------------------------------------------------------------------------

double BJM_OR::ComputeOneTwoPoints(int m1, 
		int m2, 
		double epsilon,
		double *fr) const
{
    /**fr = BJgauche->GetTwoPointsFR(m1, m2) +*/
          /*BJdroite->GetTwoPointsFR(m1, m2);*/
	return BJgauche->GetTwoPointsLOD(m1, m2)+
		BJdroite->GetTwoPointsLOD(m1, m2);
}

//-----------------------------------------------------------------------------
// Remplit les tableaux
//-----------------------------------------------------------------------------

void BJM_OR::ComputeTwoPoints(void)
{
	// DL
	/*
	double lod;
	int i = 0, k = 0;
	int j = 0, l = 0;

	while (i < TailleCharn) 
	{
		if (IndMarq[k]) 
		{
			i++;
			j = i + 1;
			l = k + 1;
			while (j <= TailleCharn) 
			{
				if (IndMarq[l]) 
				{
					lod = BJgauche->GetTwoPointsLOD(k, l)+
						BJdroite->GetTwoPointsLOD(k, l);
					TwoPointsLOD[IndMarq[k]][IndMarq[l]] = TwoPointsLOD[IndMarq[l]][IndMarq[k]] = lod;
					j++;
				}
				l++;
			}
		}
		k++;
	}
	*/
}

//-----------------------------------------------------------------------------
// d�tection des groupes, affichage et mise � jour de Cartage.
//-----------------------------------------------------------------------------
// Param�tres : 
// - disthres seuil de la distance 
// - lodthres seuil du lod
// Valeur de retour : le nombre de groupes trouv�s
//-----------------------------------------------------------------------------

int BJM_OR::Groupe(double disthres, double lodthres) const
{
	return BioJeu::Groupe(10000.0, lodthres); // 10000.0 = "l'infini"
}


//-----------------------------------------------------------------------------
// Affichage simple d'une carte
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte
// -
//-----------------------------------------------------------------------------

void BJM_OR::PrintMap(Carte *data) const
{
	BJgauche->PrintMap(data);
	BJdroite->PrintMap(data);
}

//-----------------------------------------------------------------------------
// renvoie d'une carte sous forme de liste
//-----------------------------------------------------------------------------
// Param�tres :
// - l'unit� 
// - la carte
//-----------------------------------------------------------------------------

char *** BJM_OR::GetMap(const char unit[2], Carte *data) const
{  
	int i, j, k;
	int nbmg = 0, nbmd = 0;
	int *vmg, *vmd;
	Carte *cg, *cd;

	// d�compte � gauche, d�compte � droite
	for (i = 0; i < data->NbMarqueur; i++)
	{
		if (Cartage->markers[data->ordre[i]].BitJeu &
				BJgauche->BitJeu) nbmg++;
		if (Cartage->markers[data->ordre[i]].BitJeu &
				BJdroite->BitJeu) nbmd++;
	}

	vmg = new int[nbmg];
	vmd = new int[nbmd];

	// remplissage � gauche, remplissage � droite
	j = 0;
	k = 0;
	for (i = 0; i < data->NbMarqueur; i++)
	{
		if (Cartage->markers[data->ordre[i]].BitJeu & 
				BJgauche->BitJeu) vmg[j++] = data->ordre[i];
		if (Cartage->markers[data->ordre[i]].BitJeu & 
				BJdroite->BitJeu) vmd[k++] = data->ordre[i];
	}

	// cr�ation des cartes
	cg = new Carte(Cartage, nbmg, vmg);
	cd = new Carte(Cartage, nbmd, vmd);

	if (nbmg == 0)
		cg->coutEM = 0.0;
	else 
		BJgauche->ComputeEM(cg);

	if (nbmd == 0)
		cd->coutEM = 0.0;
	else 
		BJdroite->ComputeEM(cd);

	char*** lmg = BJgauche->GetMap(unit,cg);
	char*** lmd = BJdroite->GetMap(unit,cd);
	i = 0;
	j = 0;
	k = 0;

	// d�compte des cartes

	while (lmg[i++] != NULL);
	while (lmd[j++] != NULL);

	char*** lm =  new char**[i + j - 1];

	lm[i + j - 2] = NULL;

	// fusion

	i = 0;
	j = 0;
	while (lmg[i] != NULL)
		lm[k++] = lmg[i++];
	while (lmd[j] != NULL)
		lm[k++] = lmd[j++];

	// nettoyage
	delete []vmg;
	delete []vmd;
	delete cd;
	delete cg;

	delete [] lmg;
	delete [] lmd;

	return (lm);
}
//-----------------------------------------------------------------------------
// Affichage d�taill� d'une carte(m�me principe que computeEM)
//-----------------------------------------------------------------------------
// Param�tres : 
// - la carte
// - le sens
// - la carte globale
//-----------------------------------------------------------------------------

void BJM_OR::PrintDMap(Carte *data, int envers, Carte *dataref)
{
	int i, j, k;
	int nbmg = 0, nbmd = 0;
	int *vmg, *vmd;
	Carte *cg, *cd;

	// d�compte � gauche, d�compte � droite
	for (i = 0; i < data->NbMarqueur; i++)
	{
		if (Cartage->markers[data->ordre[i]].BitJeu &
				BJgauche->BitJeu) nbmg++;
		if (Cartage->markers[data->ordre[i]].BitJeu &
				BJdroite->BitJeu) nbmd++;
	}

	vmg = new int[nbmg];
	vmd = new int[nbmd];

	// remplissage � gauche, remplissage � droite
	j = 0;
	k = 0;
	for (i = 0; i < data->NbMarqueur; i++)
	{
		if (Cartage->markers[data->ordre[i]].BitJeu & 
				BJgauche->BitJeu) vmg[j++] = data->ordre[i];
		if (Cartage->markers[data->ordre[i]].BitJeu & 
				BJdroite->BitJeu) vmd[k++] = data->ordre[i];
	}

	// cr�ation des cartes
	cg = new Carte(Cartage, nbmg, vmg);
	cd = new Carte(Cartage, nbmd, vmd);

	if (nbmg != 0) 
	{
		BJgauche->ComputeEM(cg);
		BJgauche->PrintDMap(cg, envers, dataref);
	}

	if (nbmd != 0)
	{   
		BJdroite->ComputeEM(cd);
		BJdroite->PrintDMap(cd, envers, dataref);
	}

	// nettoyage
	delete vmg;
	delete vmd;
	delete cd;
	delete cg;

}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les num�ro du couple
// - 
// Valeur de retour : le LOD 2 points
//----------------------------------------------------------------------------

double BJM_OR::GetTwoPointsLOD(int m1, int m2) const
{

	return (BJgauche->GetTwoPointsLOD(m1, m2)+
			BJdroite->GetTwoPointsLOD(m1, m2));

}

//-----------------------------------------------------------------------------
// Acc�s � une valeur TwoPoints
//-----------------------------------------------------------------------------
// Param�tres : 
// - les r�f�rences du couple
// - 
// Valeur de retour : la distance 2 points
//----------------------------------------------------------------------------

double BJM_OR::GetTwoPointsDH(int m1, int m2) const
{

	if (BJgauche->Couplex(m1,m2) && BJdroite->Couplex(m1,m2)) {
		if (HasRH())
		{return (BJgauche->GetTwoPointsDH(m1, m2)); }
		else
		{return (BJgauche->GetTwoPointsDH(m1, m2)+BJdroite->GetTwoPointsDH(m1, m2))/2.0; }
	}

	if (BJgauche->Couplex(m1,m2))
		return BJgauche->GetTwoPointsDH(m1, m2);

	if  (BJdroite->Couplex(m1,m2))
		return BJdroite->GetTwoPointsDH(m1, m2);
	// inherit the large distance of totally non informative pairs

	if (HasRH())
	{return (BJgauche->GetTwoPointsDH(m1, m2)); }
	else
	{return (BJgauche->GetTwoPointsDH(m1, m2)+BJdroite->GetTwoPointsDH(m1, m2))/2.0; }
	//return (BJgauche->GetTwoPointsDH(m1, m2)+BJdroite->GetTwoPointsDH(m1, m2))/2.0;
}
