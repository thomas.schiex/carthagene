//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// Tas.cc,v 1.52 2004/06/11 11:43:24 tschiex Exp
//
// Description : Classe de gestion du tas.
// Divers :
//-----------------------------------------------------------------------------

#include "Tas.h"
#include <stdio.h>
#include <math.h>
#include <iostream>

#include "CartaGene.h"
#include "BioJeu.h"

#define Parent(i) ((i-1)/2)
#define LeftSon(i) (2*i+1)
#define RightSon(i) (2*i+2)
#define Value(i) (Heap[i]->map->coutEM)

#define value(i) (heap[i]->map->coutEM)

#define ElH(a,b) ((a > b) ? (a + (b * NbM)) : ((a * NbM) + b))
#define ElH2(a,b) ((a < b) ? (a + (b * NbM)) : ((a * NbM) + b))

//-----------------------------------------------------------------------------
// Constructeur de la classe STructHMap
//-----------------------------------------------------------------------------

StructHMap::StructHMap(CartaGene *cartage, int n)
{
    map = new Carte(cartage ,n);
}


//-----------------------------------------------------------------------------
// Destructeur de la classe STructHMap
//-----------------------------------------------------------------------------

StructHMap::~StructHMap()
{
    delete map;
}


//-----------------------------------------------------------------------------
// Constructeur de la classe Tas.
//-----------------------------------------------------------------------------
// Param�tres :
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

Tas::Tas() 
    : HeapSize(0)
{
    int i;

    Heap = NULL;
    BestMap = NULL;

    MaxHeapSize =  0;
    EquivalenceFlag = 0;

    for (i = 0; i < HASHSIZE; i++)
        HashTable[i] = NULL;

    Chrono.Init();

}


//-----------------------------------------------------------------------------
// Destructeur de la classe Tas.
//-----------------------------------------------------------------------------

Tas::~Tas()
{ 
    if (HeapSize) {
        for (int i = 0; i < HeapSize; i++)
            delete Heap[i];
        delete [] Heap;
    } 
    else delete [] Heap;
}

//-----------------------------------------------------------------------------
// Initialisation du tas.
//-----------------------------------------------------------------------------
// Param�tres :
// - le r�f�rentiel
// - taille du tas
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

void Tas::Init(CartaGenePtr cartag, int mhs)
{
    int i;
    Cartage = cartag;

    MaxHeapSize = mhs;

    if (HeapSize)  {
        for (int i = 0; i < HeapSize; i++)
            delete Heap[i];
        delete [] Heap;
    } 
    else delete [] Heap;

    Heap = new HMapPtr[MaxHeapSize];

    HeapSize = 0;

    worst =-1e100;
    BestMap = NULL;

    for (i = 0; i < HASHSIZE; i++)
        HashTable[i] = NULL;

    Chrono.Init();

    if (Cartage->ArbreJeu) Cartage->ArbreJeu->ResetNbEMCall();
}

//-----------------------------------------------------------------------------
// Initialisation du tas soft le chrono et le nombre d'appels � EM
// ne sont pas remis � 0
//-----------------------------------------------------------------------------
// Param�tres :
// - le r�f�rentiel
// - taille du tas
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

void Tas::Initsoft(CartaGenePtr cartag, int mhs)
{
    int i;
    Cartage = cartag;

    MaxHeapSize = mhs;

    if (HeapSize) {
        for (int i = 0; i < HeapSize; i++)
            delete Heap[i];
        delete [] Heap;
    }
    else delete [] Heap;

    Heap = new HMapPtr[MaxHeapSize];

    HeapSize = 0;

    worst =-1e100;
    BestMap = NULL;

    for (i = 0; i < HASHSIZE; i++)
        HashTable[i] = NULL;
}

//-----------------------------------------------------------------------------
// affichage du tas
//-----------------------------------------------------------------------------
// Param�tres : 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Tas::Print(void) const
{
    int i;

    print_out( "HeapSize = %d\n", HeapSize);
    for (i=0; i<HeapSize; i++)
    {
        print_out( "Heap[%d] = ", i);
        Cartage->PrintMap(Heap[i]->map);
    }
}

//-----------------------------------------------------------------------------
// affichage du tas
//-----------------------------------------------------------------------------
// Param�tres : 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void Tas::PrintD(void) const
{
    int i;

    print_out( "HeapSize = %d\n", HeapSize);
    for (i=0; i<HeapSize; i++)
    {
        print_out( "Heap[%d] = ", i);
        Cartage->PrintDMap(Heap[i]->map, 0, Heap[i]->map);
    }
}

//-----------------------------------------------------------------------------
// retourne le meilleur �l�ment du tas
//-----------------------------------------------------------------------------
// Param�tres : 
// Valeur de retour : 
//-----------------------------------------------------------------------------

Carte *Tas::Best(void) const{

    int i, best = 0;

    for (i = 1; i < HeapSize; i++)
        if (Heap[i]->map->coutEM > Heap[best]->map->coutEM ) 
            best = i;

    return Heap[best]->map; 
}


//-----------------------------------------------------------------------------
// retourne le delta
//-----------------------------------------------------------------------------
// Param�tres : 
// Valeur de retour : 
//-----------------------------------------------------------------------------

double Tas::Delta(void) const{

    int i, best = 0, best2 = 0;

    for (i = 1; i < HeapSize; i++)
        if (Heap[i]->map->coutEM > Heap[best]->map->coutEM ) {
            best2 = best;
            best = i;
        } 
        else  if (Heap[i]->map->coutEM > Heap[best2]->map->coutEM ) {
            best2 = i;
        }

    return Heap[best]->map->coutEM  - Heap[best2]->map->coutEM; 
}

//-----------------------------------------------------------------------------
// Insert un nouvel element (une carte est copiee).
//-----------------------------------------------------------------------------
// Param�tres : 
// - map : la carte a inserer
// - iter : 
// Valeur de retour : 
//-----------------------------------------------------------------------------

int Tas::Insert(Carte *map, int iter)
{
    int i;
    HMap **h;

    if (HeapSize == MaxHeapSize)
        worst = Value(0);

    // si la carte est meilleure que la pire du tas, elle va entrer
    if (map->coutEM > worst) {

        // sauf si elle deja dedans
        map->Canonify();
        h = HashTable+HashMap(map);

        if (*(HashLocate(map,h)) == NULL) {
            // on la converge correctement
            Cartage->ComputeEM(map);

            if (HeapSize == MaxHeapSize) 
                Extract();

            i = HeapSize++;
            while ((i > 0) && (Value(Parent(i)) > map->coutEM)) {
                Heap[i] = Heap[Parent(i)];

                // affectation de l'Id de la carte.
                Heap[i]->map->Id = i;
                i = Parent(i);
            }
            Heap[i] = new StructHMap(map->Cartage, map->NbMarqueur);
            // recall since one may have deleted the previously h found in Extract
            h = HashLocate(map,h);
            (*h) = Heap[i];
            Heap[i]->InH = h;
            Heap[i]->BirthDate = iter;
            Heap[i]->NbHits = 0;
            Heap[i]->Next = NULL;
            map->CopyFMap(Heap[i]->map);

            // affectation de l'Id de la carte.
            Heap[i]->map->Id = i;
            Heap[i]->TimeStamp = Chrono.Read();

            // v�rification de la robustesse de la derni�re meilleure carte
            if (BestMap && Heap[i]->map->coutEM > BestMap->map->coutEM + Cartage->Robustness) {
                if (Heap[i]->map->coutEM > BestMap->map->coutEM &&
                        Heap[i]->map->coutEM - BestMap->map->coutEM >= fabs(Cartage->Robustness)) {
                    print_out("Previous best map was not robust!\n");
                } else {
                    print_out("Current best map is not robust!\n");
                }
                Cartage->StopFlag = 1;
            }
            // mise a jour de la meilleure carte
            if (!BestMap || Heap[i]->map->coutEM > BestMap->map->coutEM)
                BestMap = Heap[i];

            return 0;
        }
        else
        {
            ((*h)->NbHits)++;
            i = (*h)->BirthDate;
            (*h)->BirthDate = iter;
            if ((*h)->NbHits> MAXHITS) return i;
            return 0;
        }
    }
    return 0;
}


//-----------------------------------------------------------------------------
// Supprime le plus mauvais element du tas.
// Suppose que le tas est non vide.
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

int Tas::Extract()
{
    if (HeapSize == 0) {
        print_err(  "Tas::Extract(): ERREUR: le tas est vide\n");
        // exit (1);
        return(-1);
    }

    if ((*(Heap[0]->InH) = Heap[0]->Next) != NULL)
        Heap[0]->Next->InH = Heap[0]->InH;
    delete Heap[0];

    HeapSize--;

    Heap[0] = Heap[HeapSize];

    if (HeapSize) Heapify(0);
    else BestMap = NULL;

    return 0;
}

//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

HMap **Tas::HashLocate(const Carte *map,HMap **ptr)
{
    if (*ptr)
        for (;;) {
            if (map->SameMaps((*ptr)->map))
                return ptr;
            if ((*ptr)->Next == NULL)
                return &((*ptr)->Next);
            else ptr = &((*ptr)->Next);
        }
    return ptr;
}

//-----------------------------------------------------------------------------
//  probablement nullasse.
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

unsigned int Tas::HashMap(const Carte* map)
{
    const double  Om = ((sqrt((double) 5)-1.0)/2.0);

    int i;
    double h = 1.0;
    int NbM = map->NbMarqueur;

    Carte *mapc = new Carte(Cartage, NbM);

    map->CopyMap(mapc);
    if (EquivalenceFlag == 1)
        mapc->CanonifyMor();

    for (i = 0; i<NbM-1; i++)
    {
        h = h + (((i+1) * Om) * (ElH(mapc->ordre[i], 
                        mapc->ordre[i+1]))) / NbM;
    }
    h = h * Om;
    h = h - floor(h);
    h = h * HASHSIZE;

    delete mapc;

    return ((unsigned int) h);  
}


//-----------------------------------------------------------------------------
// Force les proprietes du tas.
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

void Tas::Heapify(int i)
{
    int l, r;
    int largest;
    HMap *temp;

    // affectation de l'Id de la carte.

    Heap[i]->map->Id = i;

    l = LeftSon(i);
    r = RightSon(i);

    if ((l < HeapSize) && (Value(l) < Value(i)))
        largest = l;
    else largest = i;

    if ((r < HeapSize) && (Value(r) < Value(largest)))
        largest = r;

    if (largest != i)  {
        temp = Heap[i];
        Heap[i] = Heap[largest];

        // affectation de l'Id de la carte.
        Heap[i]->map->Id = i;

        Heap[largest] = temp;
        Heapify(largest);
    }
}

//-----------------------------------------------------------------------------
// Force les proprietes du tas.
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

void Tas::heapify(HMap*** aheap, int heapsize, int i) const
{
    int l, r;
    int largest;
    HMap *temp;
    HMap **heap = *aheap;

    l = LeftSon(i);
    r = RightSon(i);

    if ((l < heapsize) && (value(l) < value(i)))
        largest = l;
    else largest = i;

    if ((r < heapsize) && (value(r) < value(largest)))
        largest = r;

    if (largest != i)
    {
        temp = heap[i];
        heap[i] = heap[largest];
        heap[largest] = temp;
        heapify(aheap, heapsize, largest);
    }
}


//-----------------------------------------------------------------------------
// Comparaison des cartes du tas
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

void Tas::PrintO(int nbm, int blank, int comp) const
{ 
    int *tabid, *taboref, *tordre;
    int i;
    int nbma;
    char formpos[128]; 
    char formposs[128]; 

    // quelques v�rif

    if (comp)
        blank = 0;

    if (HeapSize == 0) 
    {
        print_err(  "Error : Empty heap.\n");
        return;
    }

    if (nbm < 0)
    {
        print_err( "Error : The minimum length of consecutive differences you want to be materialised is expected to be positive. When this value is set to 0, the command will always and only show all the positions of all the loci of all the maps.\n");
        return;
    }  

    if (blank != 0 && blank != 1)
    {
        print_err( "Error : The boolean for replacing position of loci by blank should be 0 or 1.\n");
        return;
    }  

    if (comp != 0 && comp != 1)
    {
        print_err( "Error : The boolean for compressing the intervals should be 0 or 1.\n");
        return;
    }  


    // r�cup�ration des num�ro des cartes ordonn� en fonction de la vraisemblance
    tabid = IdSorted();

    nbma = Heap[0]->map->NbMarqueur;

    // le format de l'affichage d�pend du nombre de marqueurs
    if (nbma <= 10) {
        sprintf(formpos, " %c%d%c",'%', 1, 'd');
        sprintf(formposs, " %c%d%c",'%', 1, 's');
    }
    else if (nbma <= 100) {
        sprintf(formpos, " %c%d%c",'%', 2, 'd');
        sprintf(formposs, " %c%d%c",'%', 2, 's');
    }
    else {
        sprintf(formpos, " %c%d%c",'%', 3, 'd');
        sprintf(formposs, " %c%d%c",'%', 3, 's');
    }

    tordre = Heap[tabid[0]]->map->ordre;

    // affichage vertical des indices des marqueurs de la meilleure carte
    print_out("Loci Id  ..........");
    for (i = 0; i < nbma; i++) 
        if ((tordre[i] - (tordre[i]%100))/100) {
            print_out( formpos, (tordre[i] - (tordre[i]%100))/100);
        }
        else {
            print_out( formposs, " "); 
        }

    print_out("\n");
    print_out("                  :");
    for (i = 0; i < nbma; i++)
        if ((tordre[i]%100 - ((tordre[i]%100)%10))/10 || tordre[i] > 99) {
            print_out( formpos, (tordre[i]%100 - ((tordre[i]%100)%10))/10);
        }
        else {
            print_out( formposs," ");
        }
    print_out("\n");

    print_out("                  :");
    for (i = 0; i < nbma; i++) {  
        print_out( formpos, tordre[i]%10);
    }
    print_out("\n");

    // affichage de la num�rotation des positions
    // C'est le changement des positions qui nous int�resse
    print_out("Loci Pos ..........");

    for (i = 0; i < nbma; i++) {  
        print_out( formposs,"|"); 
    }
    print_out("\n");

    print_out("Map Id : log10    :");
    for (i = 0; i < nbma; i++) {  
        print_out( formposs,"|"); 
    }

    print_out("  (Delta lod per set)");

    print_out("\n");

    print_out( "%6d : %8.2f :", tabid[0], Heap[tabid[0]]->map->coutEM); 
    for (i = 1; i<= nbma; i++) {
        print_out( formpos, i);
    }

    char*** lmn = Cartage->GetMap("k", Best()->Id);
    int nj;

    print_out("  (");

    nj = 1;

    while (lmn[nj++] != NULL);

    nj--;nj--;

    for (i = 1; i < nj; i++) {   
        print_out( " %8s +", lmn[i][0]);
    }

    print_out( " %8s )", lmn[nj][0]);

    delete [] lmn;

    print_out("\n");


    // l'ordre de r�f�rence est celui de la meilleure carte
    taboref = new int[Cartage->NbMarqueur + 1];
    for (i = 0; i< nbma; i++)
        taboref[Heap[tabid[0]]->map->ordre[i]] = i;

    // pour chaque carte, affichage des changements de positions.
    for (i = 1; i< HeapSize; i++)
        Heap[tabid[i]]->map->PrintO(taboref, Heap[tabid[0]]->map->ordre, nbm, blank, comp);

    delete [] tabid;
}

//-----------------------------------------------------------------------------
// afiichage tri�
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

void Tas::PrintSort() const
{

    int i;
    int heapsize;
    HMap** heap = new HMapPtr[MaxHeapSize];

    // recopie dans une structure provisoire
    for (i = 0; i< HeapSize; i++)
        heap[i]= Heap[i];

    heapsize = HeapSize;

    while (heapsize != 0) {

        Cartage->PrintMap(heap[0]->map);

        // suppression du premier

        heapsize--;
        heap[0] = heap[heapsize];

        // heapify

        heapify(&heap, heapsize, 0);

    }

    delete [] heap;
}

//-----------------------------------------------------------------------------
// retourne un tableau d'Id de cartes tri�es 
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

intPtr Tas::IdSorted() const
{

    int i;
    int heapsize;
    HMap** heap = new HMapPtr[MaxHeapSize];
    int *tabid = new int[HeapSize];

    // recopie dans une structure provisoire
    for (i = 0; i< HeapSize; i++)
        heap[i]= Heap[i];

    heapsize = HeapSize;

    while (heapsize != 0) {

        tabid[heapsize - 1] = heap[0]->map->Id;

        // suppression du premier

        heapsize--;
        heap[0] = heap[heapsize];

        // heapify

        heapify(&heap, heapsize, 0);

    }

    delete [] heap;

    return tabid;
}

//-----------------------------------------------------------------------------
// afiichage tri� d�taill�
//-----------------------------------------------------------------------------
// Param�tres :
// -
// -
// Valeur de retour :
//-----------------------------------------------------------------------------

void Tas::PrintDSort() const
{

    int i;
    int heapsize;
    HMap** heap;
    Carte * cart;
    double bestcout;
    int count = 0;

    if (HeapSize) {
        heap = new HMapPtr[MaxHeapSize];
        cart  = Best();
        bestcout = cart->coutEM;

        // recopie dans une structure provisoire
        for (i = 0; i< HeapSize; i++)
            heap[i]= Heap[i];

        heapsize = HeapSize;

        while (heapsize != 0) {

            Cartage->PrintDMap(heap[0]->map, 0, heap[0]->map);
            if (bestcout - heap[0]->map->coutEM < 3.0) count++;

            // suppression du premier
            heapsize--;
            heap[0] = heap[heapsize];

            // heapify
            heapify(&heap, heapsize, 0);
        }

        print_out("\n        EM calls:\n");

        if (Cartage->ArbreJeu) Cartage->ArbreJeu->PrintEMCall();

        print_out( "        CPU Time (secs): %.2f\n", Chrono.Read());
        print_out( "        Maps within -3.0: %d\n", count);

        delete [] heap;
    }
}

//-----------------------------------------------------------------------------
// enregistre la meilleure carte dans mapfile, 
// ainsi que le temps de calcul (utilisateur), les bonnes cartes memorisees 
// dans le tas et le nombre de cartes avec un LogLike superieur au seuil.
// Une partie du contenu des cartes est m�moris� dans une liste pour post-
// traitement.
//-----------------------------------------------------------------------------
// Param�tres : 
// - thres : seuil de LogLike pour comptabilisation des cartes.
// Valeur de retour :
//   chaine de caract�re...
//-----------------------------------------------------------------------------

char* Tas::Stat(double thres)
{
    /* TCL PRESENTATION LAYER */
    char *lineres;
    int heapsize;
    Carte *Good = new Carte(Cartage, Cartage->NbMS);
    int i;
    int BestGood = 0 ;
    int found = 0;
    int count = 0;
    double temps = Chrono.Read();
    double time;
    double GoodLike = 0.0;
    double BestLike;
    int random;
    HMap** heap = new HMapPtr[MaxHeapSize];
    char *vide;

    // pour retourner une liste vide(voir conventions)
    vide = new char[1];
    vide[0] = '\0';

    // quelques v�rifications 

    if (HeapSize == 0) 
    {
        print_err(  "Error : Empty heap.\n");
        return vide;
    }

    lineres = new char[256];

    thres = (Best())->coutEM - thres;

    // recopie dans une structure provisoire
    for (i = 0; i< HeapSize; i++)
        heap[i]= Heap[i];

    heapsize = HeapSize;

    while (heapsize != 1) {

        if (heap[0]->map->GoodMap())
            found = 1;

        if (heap[0]->map->coutEM > thres){
            count++;
            if (!Cartage->QuietFlag) Cartage->PrintDMap(heap[0]->map, 0, heap[0]->map);
        }
        // suppression du premier

        heapsize--;
        heap[0] = heap[heapsize];

        // heapify

        heapify(&heap, heapsize, 0);
    }

    // Traitement de la derni�re carte contenue dans le tas
    if (heap[0]->map->GoodMap()) BestGood = 1;
    time = Heap[0]->TimeStamp;

    random = !Good->BuildGoodMap();
    if (random)
        GoodLike = Cartage->ComputeEM(Good);

    /* intialize the map with identical theta */
    for (i = 0; i < Cartage->NbMS - 1; i++)
        heap[0]->map->tr[i]=0.05;
    heap[0]->map->UnConverge();
    BestLike = Cartage->ComputeEM(heap[0]->map);

    sprintf(lineres,"%d %f %d %f %f %d %d",
            BestGood, temps, found,
            (random ? (BestLike-GoodLike) : 0.0),
            time, count, Cartage->ArbreJeu->NbEMCall);

    if (!Cartage->QuietFlag)
    {
        print_out( "Total CPU Time (secondes): %.2f\n", temps);
        print_out( "Number of calls to EM: %d\n", Cartage->ArbreJeu->NbEMCall);
        print_out( "# of maps within %f: %d\n", thres, count);
        print_out(  "Current best LogLike is %f and worst is %f\n", (BestMap)?BestMap->map->coutEM:Best()->coutEM, Worst()->coutEM);
        if (random)
        {
            print_out( "Delta LogLike with good map: %f\n", (BestLike-GoodLike));
            if (found){
                print_out( "Found in heap at time: %.2f\n", time);
            }
            if (BestGood) {
                print_out("It was the best map\n");
            }
        }
        print_out(  "Reliability Threshold = %g\n", Cartage->Robustness); 
        if (Cartage->ArbreJeu) {
            print_out(  "Tolerance Threshold1 = %g\n", Cartage->ArbreJeu->Epsilon1); 
            print_out(  "Tolerance Threshold2 = %g\n", Cartage->ArbreJeu->Epsilon2); 
        }
    }

    delete [] heap;
    delete [] vide;

    return lineres;
} 
