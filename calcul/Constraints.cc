//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Constraints.cc,v 1.10.6.2 2012-08-02 15:43:55 dleroux Exp $
//
// Description : Classe de manipulation de contraintes.
// Divers : 
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include "Constraints.h"
#include "Carte.h"
#include "CartaGene.h"

//-----------------------------------------------------------------------------
// Constructeur de la classe Constraints
//-----------------------------------------------------------------------------
// Param�tres : 
// -
// - 
// Valeur de retour :
//-----------------------------------------------------------------------------

Constraint::Constraint()
{
 Next = NULL;
}

//-----------------------------------------------------------------------------
// Destructeur associe
//-----------------------------------------------------------------------------
// Param�tres : 
// -
// - 
// Valeur de retour :
//-----------------------------------------------------------------------------

Constraint::~Constraint()
{
 
}

//-----------------------------------------------------------------------------
// Constructeur avec initialisation
//-----------------------------------------------------------------------------
// Param�tres : 
// - les trois marqueurs
// - la penalite associee
// Valeur de retour : 
//-----------------------------------------------------------------------------

Constraint::Constraint(int m1, int m2, int m3, double pen)
{
  M1 = m1;
  M2 = m2;
  M3 = m3;
  Penalty = pen;
  Next = NULL;
}



//-----------------------------------------------------------------------------
// Verification d'une contrainte
//-----------------------------------------------------------------------------
// Param�tres : 
// - R�f�rence d'une carte
// - 
// Valeur de retour : la penalite de la contrainte si violee, 0.0 sinon
//-----------------------------------------------------------------------------

double Constraint::Check(Carte *map)
{

  int i, posm1 = -1, posm2 = -1, posm3 = -1;
  
  for (i = 0; i< map->NbMarqueur; i++)
    {
      if (map->ordre[i] == M1) posm1 = i;
      if (map->ordre[i] == M2) posm2 = i;
      if (map->ordre[i] == M3) posm3 = i;
      
      if (posm1 != -1 && posm2 != -1 && posm3 != -1) break;
    }
  
  if((posm1 - posm2) *
     (posm2 - posm3) < 0)
    return Penalty;
  else
    return 0.0;
}

//-----------------------------------------------------------------------------
// Verification des contraintes, a appliquer a la premiere contrainte
//-----------------------------------------------------------------------------
// Param�tres : 
// - R�f�rence d'une carte
// Valeur de retour : la penalite globale
//-----------------------------------------------------------------------------

double Constraint::CheckAll(Carte *map)
{
  double Somme = 0.0;
  Constraint* con = this;

  while (con)
    {
      Somme += con->Check(map);
      con = con->Next;
    }

  return Somme;
}

//-----------------------------------------------------------------------------
// Verification d'une contrainte.
//-----------------------------------------------------------------------------
// Param�tres : 
// - R�f�rence d'une carte
// - 
// Valeur de retour : 1 si contrainte violee, 0 sinon
//-----------------------------------------------------------------------------

int Constraint::CheckI(Carte *map)
{
  int i, posm1 = -1, posm2 = -1, posm3 = -1;
  
  for (i = 0; i< map->NbMarqueur; i++)
    {
      if (map->ordre[i] == M1) posm1 = i;
      if (map->ordre[i] == M2) posm2 = i;
      if (map->ordre[i] == M3) posm3 = i;

      if (posm1 != -1 && posm2 != -1 && posm3 != -1) break;
    }
  
  return ((posm1 - posm2) *
	  (posm2 - posm3) < 0);
}
//-----------------------------------------------------------------------------
// Verification des contraintes, a appliquer a la premiere contrainte
//-----------------------------------------------------------------------------
// Param�tres : 
// - R�f�rence d'une carte
// Valeur de retour : le nombre de contraintes violees
//-----------------------------------------------------------------------------

double Constraint::CheckIAll(Carte *map)
{
  double Somme = 0.0;
  Constraint* con = this;

  print_out(  "Violated Constraint :");

  while (con)
    {
      if (con->CheckI(map))
	{
	  print_out( "\n%10s %10s %10s %5.1f", 
		 map->Cartage->markers.keyOf(con->M1).c_str(),
		 map->Cartage->markers.keyOf(con->M2).c_str(),
		 map->Cartage->markers.keyOf(con->M3).c_str(),
		 con->Penalty);

	  Somme+=con->Penalty;
	}
      con = con->Next;
    }
  if (Somme == 0.0) {
    print_out(  "none");
  }
  else {
    print_out( "\n%10s %10s %10s %5s\n%10s %10s %10s %5.1f", 
	   "", "", "", "-------", "", "Penalty", "=", Somme);
  }
  print_out(  "\n");

  return Somme;
}
