//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: Build.cc,v 1.23.2.1 2012-08-02 15:43:55 dleroux Exp $
//
// Description : Build
// Divers :
//-----------------------------------------------------------------------------


#include "CartaGene.h"

#include <stdio.h>
#include <math.h> 

//-----------------------------------------------------------------------------
// Lancement de Build.
//-----------------------------------------------------------------------------
// Param�tres : 
// - NC nombres de cartes en //
// - 
// Valeur de retour : 
//-----------------------------------------------------------------------------

void CartaGene::Build(int NC)
{

  // v�rification de base
  
  if (NbMS == 0) 
    {
      print_err(  "Error : Empty selection of loci.\n");
      return;
    }
  
  // quelques v�rifications 

  if ( NC < 1)
    {
      print_err(  "Error : value expected for the number of maps to build : > 1.\n");
      return;
    }

  flush_out();


  // on met temporairement la robustness a bcp pour eviter la sortie
  // precoce.
  double tmpRobustness = Robustness;
  Robustness = 1e111;
  
  int mi = -1; // le marqueur
  double costmi = 0,costw = 0; // moyenne des lod
  int tm; // temporaire
  double cosd, finalcost; //provisoire et destination
 
  Tas TasS;
  Tas TasD;
  TasS.Initsoft(this,NC);
  TasD.Initsoft(this,NC);
  Tas *PTasS = &TasD; 
  Tas *PTasD = &TasS;
  Tas *PTasP;

  // I: recherche d'un point de d�part = un couple.
   
  int i,j,k,wlen;
  intPtr vm = new int[NbMS]; // vecteur des marqueurs en cours

  if (!QuietFlag)  
    {
      print_out( "\nBuild(%d) : ||", NC);
      flush_out();
    }
  
  for (i = 0; i < NbMS; i++) 
    {
      for (j = i + 1; j < NbMS; j++) 
	{      
	  
	  vm[0] = MarkSelect[i];
	  vm[1] = MarkSelect[j];
	  
	  // afin de d�compter correctement les appels � EM
	  // if (GP != NULL) 
	  // 	{
	  // 	  nbe = GP->GetNbEMCall();
	  // 	  delete GP;
	  // 	}
	  //       else 
	  // 	nbe = 0;
	  
	  // afin de d�compter correctement les appels � EM      
	  //      RGP.SetNbEMCall(nbe);
	  
	  Carte MapS(this, 2, vm);
	  
	  cosd = ComputeEM(&MapS);  
	  
	  if (VerboseFlag>1) {
	    print_out( "LogLike: %f ", cosd);
	  }
	  if (VerboseFlag) PrintMap(&MapS);
	  flush_out();

	  wlen = TasS.Insert(&MapS,0);
	}      
    }
  
  //   // II: On construit progressivement la carte en ajoutant
  //   // les marqueurs 1 � 1
  
  // traitement de la demande d'arr�t.
  if (!StopFlag)
    for (i = 3; i <= NbMS; i++) 
      {
	if (!QuietFlag)   
	  {
	    print_out(  "|");
	    flush_out();
	  }
	
	PTasP = PTasS;
	PTasS = PTasD;
	PTasD = PTasP;
	
	PTasD->Initsoft(this, NC);
	
	while (PTasS->HeapSize != 0) 
	  {
	    Carte &MapS= *(PTasS->Worst());

	    // III: Choix du marqueur parmi ceux restants
	    // peut mieux faire.
	    
	    costw = 0; costmi = log(0.0); // -infinity
	    for (j = 0; j < NbMS; j++) {
	      
	      //verif de l'utilisation
	      
	      for (k = 0; k < i - 1; k++)
		if (MapS.ordre[k] == MarkSelect[j]) break;
	      if (k != i - 1) continue;
	      
	      for (k = 0; k < i - 1; k++)
		costw+=GetTwoPointsLOD(MarkSelect[j],
				       MapS.ordre[k]);
	      
	      costw/=(i-1);
	      if (costw > costmi) {
		costmi = costw;
		mi = MarkSelect[j];
	      }
	    }
	    
	    // IV: on essaye toutes les cartes possibles:
	    
	    vm[0] = mi;
	    
	    for (j = 1; j < i; j++) 
	      {
		vm[j] = MapS.ordre[j - 1];
	      }
	    
	    //       // afin de d�compter correctement les appels � EM
	    //       nbe = GP->GetNbEMCall();
	    
	    //       // afin de d�compter correctement les appels � EM      
	    //       RGP.SetNbEMCall(nbe);
	    
	    Carte MapP(this, i, vm);      
	    cosd = ComputeEM(&MapP);
	    
	    
	    wlen = PTasD->Insert(&MapP,0);
	    
	    if (VerboseFlag>1) {
	      print_out( "LogLike: %f ", cosd);
	    }
	    if (VerboseFlag) PrintMap(&MapP);      
	    flush_out();
	    finalcost = cosd;
	    
	    for (j = 1; j < i; j++) 
	      {
		tm = vm[j - 1];
		vm[j - 1] = vm[j];
		vm[j] = tm;
		
		
		Carte MapP(this, i, vm);
		
		cosd = ComputeEM(&MapP);
		
		wlen = PTasD->Insert(&MapP,0);
		
		if (VerboseFlag>1) {
		  print_out( "LogLike: %f ", cosd);
		}
		if (VerboseFlag) PrintMap(&MapP);
		flush_out();
		
		// traitement de la demande d'arr�t.
		if ( StopFlag )
		  break;
	      }
	    
	    PTasS->Extract();
	    
	    // traitement de la demande d'arr�t.
	    if ( StopFlag )
	      break;
	  }    
	// traitement de la demande d'arr�t.
	if ( StopFlag )
	  break;
      }
  
  if (!QuietFlag)   
    {
      print_out(  "\n");
      flush_out();
    }
  
  //   // afin de d�compter correctement les appels � EM      
  //   RGT.SetNbEMCall(GP->GetNbEMCall());

  // reste � restaurer dans le vrai tas + la selection des marqueurs
   if ( StopFlag )
     ChangeSel((PTasD->Worst())->ordre, i);

  // mise � jour du tas de r�f�rence � partir du dernier tas.
  
  while (PTasD->HeapSize != 0) 
    {
      
      Carte &MapS= *(PTasD->Worst());
      
      ComputeEM(&MapS);
      Heap->Insert(&MapS, 0);
      
      PTasD->Extract();
    }  
  
  Carte *best;
  best = Heap->Best();
  
  ComputeEM(best);
  
  if (!QuietFlag) {
    if (VerboseFlag>1) PrintDMap(best, 0, best);
    else PrintMap(best);
  }
  
  delete [] vm;

  // traitement de la demande d'arr�t.
  if ( StopFlag )
    { 
      StopFlag = 0;
      // 
      print_out(  "Aborted!\n");
    }
  
  flush_out();
  Robustness = tmpRobustness;
}

