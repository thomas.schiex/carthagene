//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: NOrCompMulti.h,v 1.5 2009-12-11 16:08:28 dleroux Exp $
//
// Description : Prototype of the NorComp class 
// Divers : Computation of the number of orders at a given breakpoint distance
//-----------------------------------------------------------------------------

#ifndef _NORCOMPMULTI_H
#define _NORCOMPMULTI_H

#include "CGtypes.h"
#include "System.h"

//#define REALVALUE double
//#define REALVALUEPtr doublePtr
#define REALVALUE long double
#define REALVALUEPtr long double*

class NOrCompMulti {
 public:
  
  NOrCompMulti(int k, intPtr N, int n);
  
  ~NOrCompMulti();
  
  void test();
  
  REALVALUE getNO(int k);
  
  inline void run()
    {
      getNO(bpdim);
    };
  
 private:
  
  inline REALVALUE f_O(int n, int k)
    {
      if (k<0 || n<0) return 0;
      if (O[n][k] != -1) return O[n][k];
      O[n][k] = f_I(n,k) + f_S(n,k);
      return O[n][k];
    };
  
  inline REALVALUE f_I(int n, int k) 
    {
      if (k<0 || n<0) return 0;
      if (I[n][k] != -1) return O[n][k];
      I[n][k] = f_is(0,n,k) + f_is(1,n,k);
      return I[n][k];
    };
  
  inline REALVALUE f_S(int n, int k) 
    { 
      if (k<0 || n<0) return 0;
      if (S[n][k] != -1) return S[n][k];
      S[n][k] = f_se(0,n,k) + f_se(1,n,k);
      return S[n][k];
    };
  
  inline REALVALUE f_o(int s, int n, int k) 
    {
      if (o[s][n][k] != -1) return o[s][n][k];
      o[s][n][k] = f_is(s,n,k) + f_se(s,n,k);
      return o[s][n][k];
    };
  
  inline REALVALUE f_is(int s, int n, int k) 
    {
      REALVALUE Is;
      
      if (k<=0 || n <= k || n<3) return 0;
      if (limits[n]) {
	if (s>0)
	  Is = 2* f_O(n-1,k-1);
	else
	  Is = (k-1)*f_O(n-1,k-1) + (n-k)*f_O(n-1,k-2);
	i[s][n][k] = Is;
      }
      if (i[s][n][k] != -1) return i[s][n][k];
      
      if (s>0)
	i[s][n][k] = f_o(1, n-1, k-1) + 2*f_o(0, n-1, k-1);
      else
	i[s][n][k] = (k-1)*f_O(n-1,k-1) - f_is(1, n-1, k-1) - f_se(0, n-1, k-1) - 2*f_is(0, n-1, k-1) + (n-k)*f_O(n-1,k-2) - f_S(n-1,k-2);
      return i[s][n][k];
    };
  
  inline REALVALUE  f_se(int t, int n, int k) 
    {
      REALVALUE Se;

      if ((s && n < 2) || n <= k) return 0;
      if (n == 1 && k == 0) return 1;
      if (t && n == 2 && k == 0) return 2;
      if (limits[n]) {
	Se  = 0;
 	s[t][n][k] = Se;
	return Se;
      };

      if (k < 0 || n <= k || n < 3) return 0;
      if (s[t][n][k] != -1) return s[t][n][k];

       if (t>0)
	s[t][n][k] = f_is(1, n-1, k) + f_se(1, n-1, k);
      else
	s[t][n][k] = f_is(1,n-1,k) + 2*f_is(0, n-1, k) + f_se(0, n-1, k) + f_S(n-1,k-1);
      return s[t][n][k];
      
    }
  // storage area

  int bpdim;
  intPtr locdim;
  int chromdim;
  int glocdim;
  intPtr limits;
  

  REALVALUEPtr **Z;
  REALVALUEPtr *O;
  REALVALUEPtr *I;
  REALVALUEPtr *S;
  REALVALUEPtr ***Y;
  REALVALUEPtr **o;
  REALVALUEPtr **i;
  REALVALUEPtr **s;
 
};

#endif /* _NORCOMPMULTI_H */
