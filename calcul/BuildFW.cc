//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
// Copyright 1997 INRA (Institut National de la Recherche Agronomique).
// This file is distributed under the terms of the Q Public License version 1.0.
// 
// $Id: BuildFW.cc,v 1.29.2.2 2012-08-02 15:43:55 dleroux Exp $
//
// Description : nouveau Build permettant de trouver des cartes FrameWorks
// Divers :
//-----------------------------------------------------------------------------


#include "CartaGene.h"
#include "Carte.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h> 

//-----------------------------------------------------------------------------
// Lancement de BuildFW.
//-----------------------------------------------------------------------------
// Param�tres : 
// - SM Delta minimum n�cessaire � l'acceptation de l'insertion d'un marqueur
// - AM Seuil � l'int�rieur duquel les ordres sont conserv�s.
// - vecteur d'indice de marqueurs
// - nombre de marqueurs dans la liste.
// - DM Flag pour demander l'affichage matrice delta pour chaque marqueur
// Valeur de retour : 
//-----------------------------------------------------------------------------


void desalloue(struct testmartin *pstruct, int nbtas)
{
    int i;

    for (i = 0 ; i < nbtas; i++)
    {
        delete [] (pstruct[i].pdelta);
    }
    free(pstruct);

}

void CartaGene::BuildFW(double SM, double AM, int *vmi, int nbmi, int DM)
{

    // v�rification de base
    if (NbMS < 3) {
        print_err(  "Error : At least three loci should be available in the selection list..\n");
        return;
    }

    // on met temporairement la robustness a bcp pour eviter la sortie
    // precoce.
    double tmpRobustness = Robustness;
    Robustness = 1e111;

    intPtr vm = new int[3]; // vecteur de travail d' indices de marqueurs.
    double cosd;
    int i, j, k, l, tempo, best;
    int sol; // le nombre de tas acceptables
    Carte * Mapw, * macarte; 
    TasPtr tastrip; //tas solution intem�diaire
    int nbm = 3; // taille de la carte
    int mi = 0; // le marqueur � tester
    int nontrouv = 1;
    int nbtas;
    int bestl = 0;
    int marq;
    struct testmartin *pstruct;

    double *coutd,*distd;  // ditsd = tableau des distances cumulees pour une carte nbm+1  indice 1 = distance du marqueur 1 au debut
    double dbest;

    TasPtr wtas; // tas de travail
    TasPtr bestas;
    double bestd = -1.0;

    if (!QuietFlag)  {
        print_out( "\nBuildFW, Adding  Threshold = %.2f, Saving Threshold = %.2f.\n", AM, SM);
        flush_out();
    }

    // aucun marqueur initial
    if (nbmi == 0) {
        if (DM == 2) {
            print_err( "Error : with postprocessing option 2, you must provide a map as third argument\n");
            Robustness = tmpRobustness;
            return;
        }

        sol = 0; // d�compte des tas respectant la condition
        wtas =  new Tas();
        bestas = new Tas();

        // phase des triplets
        for (i = 0; i < NbMS - 2; i++) {
            for(j = i + 1; j < NbMS - 1; j++) {
                for(k = j + 1; k < NbMS; k++) { 

                    // initialisation du tas de travail
                    wtas->Initsoft(this,3);

                    // insertion de M1 M2 M3
                    vm[0] = MarkSelect[i];
                    vm[1] = MarkSelect[j];
                    vm[2] = MarkSelect[k];
                    Mapw = new Carte(this, 3 , vm);
                    cosd = ComputeEM(Mapw);
                    //PrintMap(Mapw);
                    wtas->Insert(Mapw,0);
                    delete Mapw;

                    // insertions de M2 M1 M3 et M2 M3 M1
                    for (l = 1; l < 3; l++) {
                        tempo = vm[l - 1];
                        vm[l - 1] = vm[l];
                        vm[l] = tempo;
                        Mapw = new Carte(this, 3 , vm);
                        cosd = ComputeEM(Mapw);  	  		  
                        //PrintMap(Mapw);
                        wtas->Insert(Mapw,0);
                        delete Mapw;
                    }

                    // crit�re de conservation du tas
                    if ((wtas->Delta() >= AM)  && (wtas->Delta() > bestd))  {
                        delete bestas;
                        bestas = wtas; 
                        wtas = new Tas();
                        bestd = bestas->Delta();
                    }

                    // traitement de la demande d'arr�t.
                    if ( StopFlag ) break;
                }
                // traitement de la demande d'arr�t.
                if ( StopFlag ) break;
            }
            // traitement de la demande d'arr�t.
            if ( StopFlag ) break;
        }

        delete wtas;
        delete [] vm;

        if ( bestd == -1.0) {
            delete bestas;
            print_err(  "Error : No loci triplet satisfy the threshold constraints.\n");

            // traitement de la demande d'arr�t.
            if ( StopFlag ) {
                StopFlag = 0;
                print_out(  "Aborted!\n");
            }

            flush_out();
            Robustness = tmpRobustness;
            return;
        }

        tastrip = bestas;

        // Au sein du tas on vire les ordres qui depassent le seuil SM
        cosd = tastrip->Best()->coutEM;
        while ((cosd - tastrip->Worst()->coutEM) > SM)
            tastrip->Extract();

        if (!QuietFlag)  {
            print_out( "\n>>> Delta = %.2f :\n", bestd);
            if (VerboseFlag) tastrip->PrintSort();
            else PrintMap(tastrip->Best());
            flush_out();
        }

        // nbmi <> 0
    } else {
        // qqs petites v�rif...
        for (i = 0; i < nbmi; i++)  {
            int notfound = 1;

            // le marqueur en question existe-t-il ?
            if (vmi[i] > NbMarqueur || vmi[i] < 1) {
                print_err( "Error : Unknown Loci Id %d.\n", vmi[i]);
                Robustness = tmpRobustness;
                return;
            }

            // Le marqueur en question est il selectionne
            notfound = 1;
            for (j = 0; j < NbMS; j++)
                if (vmi[i] == MarkSelect[j]) notfound = 0;

            if (notfound)  {
                print_err( "\nError : Loci Id %d not in selected marker.\n", vmi[i]);
                Robustness = tmpRobustness;
                return;
            }

            // Le marqueur est il en double ?
            for (j = i+1; j < nbmi; j++)
                if (vmi[i] == vmi[j]) notfound = 1;

            if (notfound)  {
                print_err( "\nError : Loci Id %d appears twice.\n", vmi[i]);
                Robustness = tmpRobustness;
                return;
            }
        }

        tastrip =  new Tas();
        tastrip->Initsoft(this,1);
        Mapw = new Carte(this, nbmi , vmi); 
        cosd = ComputeEM(Mapw);  
        tastrip->Insert(Mapw,0);
        delete Mapw;
        nbm = nbmi;
    }

    // le premier tas est initialis�, reste � continuer
    bestd = 0.0; 

    // ...&&...&& traitement de la demande d'arr�t.
    while (NbMS - nbm > 0 && bestd != -1.0 && !StopFlag && DM != 2) {
        nbtas = NbMS - nbm; // nbre de marqueurs inserables
        bestd = -1.0;
        wtas = new Tas();
        bestas = new Tas();

        while (tastrip->HeapSize > 0) {

            // on va tester les differents marqueurs a inserer
            for (mi =0; mi < NbMS; mi++) {

                // le marqueur mi est il deja dans le tas
                int notfound = 1;
                for (j = 0; (j < nbm && notfound); j++)
                    notfound = (MarkSelect[mi] != tastrip->Worst()->ordre[j]);
                if (!notfound) continue; // on passe au marqueur suivant sinon

                // cr�ation des cartes;
                vm = new int[nbm + 1];
                vm[0] = MarkSelect[mi];
                for (k = 0;  k < nbm; k++) {
                    vm[k + 1] = (tastrip->Worst())->ordre[k];
                }

                // la premi�re
                wtas->Initsoft(this, nbm + 1);
                Mapw = new Carte(this, nbm + 1 , vm); 
                cosd = ComputeEM(Mapw);  
                wtas->Insert(Mapw,0);
                delete Mapw;

                // traitement de la demande d'arr�t.
                if ( StopFlag )
                    break;

                // les autres
                int tempo;
                for (k = 1; k < nbm + 1; k++) {
                    tempo = vm[k - 1];
                    vm[k - 1] = vm[k];
                    vm[k] = tempo;

                    Mapw = new Carte(this, nbm+1 , vm);
                    cosd = ComputeEM(Mapw);  
                    wtas->Insert(Mapw,0);
                    delete Mapw;

                    // traitement de la demande d'arr�t.
                    if ( StopFlag )
                        break;
                }

                // traitement de la demande d'arr�t.
                if ( StopFlag ) break;

                // on garde le meilleur
                if ((wtas->Delta() > AM) && (wtas->Delta() > bestd)) {
                    delete bestas;
                    bestas = wtas; 
                    wtas = new Tas();
                    bestd = bestas->Delta();
                    bestl = mi;
                }
                delete [] vm;
            }
            // on passe a l'ordre suivant
            if (tastrip->HeapSize == 1) break;
            tastrip->Extract();
        }

        delete wtas;

        // d�pouillement;
        if ( bestd != -1.0 ) {
            delete tastrip;
            tastrip = bestas;

            // Au sein du tas on cherche le nombre d'ordres � garder.
            cosd = tastrip->Best()->coutEM;
            while ((cosd - tastrip->Worst()->coutEM) > SM)
                tastrip->Extract();

            nbm++;

            if (!QuietFlag) {
                print_out( "\n>>> Delta = %.2f , Id = %d,  Locus = %s :\n", 
                        bestd, MarkSelect[bestl],markers.keyOf(MarkSelect[bestl]).c_str());
                if (VerboseFlag) 
                    tastrip->PrintSort();
                else PrintMap(tastrip->Best());
                flush_out();
            }
        } else delete bestas;
    }

    // traitement de la demande d'arr�t.
    if ( StopFlag ) {
        StopFlag = 0;
        //
        print_out(  "Aborted!\n");
        flush_out();
        Robustness = tmpRobustness;
        return;
    }

    // Insere les marqueurs restants et donne le cout
    flush_out();

    if ( (DM == 1 || DM == 2) && NbMS > nbm) {
        print_out(  "\nBuildFW, remaining loci test :\n");
        flush_out();

        // la meilleure carte du tas
        macarte = tastrip->Best();
        // le nombre de marqueur a tester

        nbtas = NbMS - nbm;
        // le tableau de structure
        pstruct = (struct testmartin *)(calloc(nbtas, sizeof(struct testmartin)));
        coutd = new double[nbm+1];
        distd = new double[nbm+1];
        CalculDist(distd, macarte, NbMS, nbm);
        //
        for (j = 0 ; j < nbtas ; j++) {
            pstruct[j].pdelta = new int[nbm+1];
            pstruct[j].poids    = 0;
            pstruct[j].numero   = 0;
            pstruct[j].nbcas    = 0;
            for (k = 0; k < nbm+1; k++)  {
                pstruct[j].pdelta[k] = 0;
            }
        }

        mi   = -1;
        marq = -1 ;
        vm   = new int[nbm + 1];
        for (i = 0; i < NbMS; i++)  {
            // d�termination du marqueur
            mi++;
            nontrouv = 1;
            while (nontrouv) {
                for (j = 0; j < nbm; j++)
                    if (MarkSelect[mi] == macarte->ordre[j]) 
                    {
                        nontrouv=0;
                        break;
                    }
                if (j == nbm) 
                    break;
            }
            if (!nontrouv) continue;
            marq++;

            // cr�ation carte
            vm[0] = MarkSelect[mi];
            pstruct[marq].numero = MarkSelect[mi];
            for (k = 0;  k < nbm; k++) {
                vm[k + 1] = macarte->ordre[k];
            }
            Mapw = new Carte(this, nbm + 1 , vm); 
            cosd = ComputeEM(Mapw);
            coutd[0] = cosd;
            dbest = cosd;
            best  = 0;

            delete Mapw;
            // les autres
            int tempo;
            // traitement de la demande d'arr�t.
            if ( StopFlag ) break;
            for (k = 1; k < nbm+1; k++) {
                tempo = vm[k - 1];
                vm[k - 1] = vm[k];
                vm[k] = tempo;
                Mapw = new Carte(this, nbm+1 , vm);
                cosd = ComputeEM(Mapw);  
                coutd[k]= cosd;
                if ( cosd > dbest) {
                    dbest = cosd;
                    best  = k;
                }
                delete Mapw;
            }
            for (k = 0; k < nbm+1 ; k++) {
                cosd = dbest - coutd[k];
                pstruct[marq].pdelta[k] = (int)(cosd - floor(cosd) >= .5 ? ceil(cosd) : floor(cosd)) ;
                if (pstruct[marq].pdelta[k] <= AM) {
                    pstruct[marq].poids  += pstruct[marq].pdelta[k];
                    pstruct[marq].nbcas++;
                }
            }
            pstruct[marq].meilleur = best;
        }

        delete[] (vm);
        delete[] (coutd);

        PrintVMap(pstruct,macarte,distd,nbm,nbtas,SM);

        desalloue(pstruct, nbtas);
    }

    // reste � restaurer dans le vrai tas + la selection des marqueurs
    // si DM =2 teste mais pas restaure

    if (DM != 2) {
        ChangeSel((tastrip->Worst())->ordre, nbm);

        // mise � jour du tas de r�f�rence � partir du dernier tas.

        while (tastrip->HeapSize != 0) {

            Carte &MapS= *(tastrip->Worst());

            ComputeEM(&MapS);
            Heap->Insert(&MapS, 0);

            tastrip->Extract();
        }  
    }
    delete tastrip;

    // traitement de la demande d'arr�t.
    if ( StopFlag ) { 
        StopFlag = 0;
        // 
        print_out(  "Aborted!\n");
    }
    Robustness = tmpRobustness;

    //  flush_out();

}

