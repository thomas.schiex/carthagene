#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigSearch.tcl,v 1.7 2005-01-14 17:24:17 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue pour la configuration des algo
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------


#
#
# Usual options.
#
itk::usual CGconfigSearch {
    keep -activebackground -activerelief -background -borderwidth -cursor \
	 -elementborderwidth -foreground -highlightcolor -highlightthickness \
	 -insertbackground -insertborderwidth -insertofftime -insertontime \
	 -insertwidth -jump -labelfont -modality -selectbackground \
	 -selectborderwidth -selectforeground -textbackground -textfont \
	 -troughcolor
}

#
#
#
class CGconfigSearch {
    inherit iwidgets::Dialog

    constructor {args} {} 
    public method get {arg}
}

#
# Provide a lowercased access method for the Selectiondialog class.
# 

proc cgconfigsearch {pathName args} {
    uplevel CGconfigSearch $pathName $args
}
#
#
#
#
body CGconfigSearch::constructor {args} {
    #
    # Set the borderwidth to zero.
    #
    component hull configure -borderwidth 0 
    
    itk_component add tabenotbook {
	iwidgets::tabnotebook $itk_interior.tnb -tabpos w -height 333 -width 666
    }
    
    # Page Build
    # ----------------------------------------------------------------------

    itk_component add buildp {
	$itk_component(tabenotbook) add -label "Build"
    }

    itk_component add build {
	cgconfigbuild $itk_component(buildp).build
    }

    pack $itk_component(build)
 
    # Page BuildFW
    # ----------------------------------------------------------------------

    itk_component add buildfwp {
	$itk_component(tabenotbook) add -label "BuildFW"
    }

    itk_component add buildfw {
	cgconfigbuildfw $itk_component(buildfwp).buildfw
    }

    pack $itk_component(buildfw)

    
    # Page ParetoLKH
    # ----------------------------------------------------------------------

    itk_component add paretolkhp {
 	$itk_component(tabenotbook) add -label "C.Frontier"
    }
    
    itk_component add paretolkh {
 	cgconfigparetolkh $itk_component(paretolkhp).paretolkh
    }
    
    pack $itk_component(paretolkh)
    
 
    # Page Annealing
    # ----------------------------------------------------------------------
    
    itk_component add annealingp {
	$itk_component(tabenotbook) add -label "Annealing"
    }

    itk_component add annealing {
	cgconfigrecuit $itk_component(annealingp).annealing
    }

    pack $itk_component(annealing)

    # Page Greedy
    # ----------------------------------------------------------------------

    itk_component add greedyp {
	$itk_component(tabenotbook) add -label "Greedy"
    }

    itk_component add greedy {
	cgconfigglouton $itk_component(greedyp).greedy
    }

    pack $itk_component(greedy)

    # Page Algogen
    # ----------------------------------------------------------------------

    itk_component add algogenp {
	$itk_component(tabenotbook) add -label "Genetic_A"
    }

    itk_component add algogen {
	cgconfigalgogen $itk_component(algogenp).algogen
    }

    pack $itk_component(algogen)

    # Page Flips
    # ----------------------------------------------------------------------

    itk_component add flipsp {
	$itk_component(tabenotbook) add -label "Flips"
    }

    itk_component add flips {
	cgconfigflips $itk_component(flipsp).flips
    }

    pack $itk_component(flips)

    hide Help

    $itk_component(tabenotbook) select 0

    set cmd1 [concat $itk_component(annealing) "configure \
-tries \[" $itk_component(annealing) " get -tries\] \
-tinit \[" $itk_component(annealing) " get -tinit\] \
-tfinal \[" $itk_component(annealing) " get -tfinal\] \
-cooling \[" $itk_component(annealing) " get -cooling\] ;" \
		  $itk_component(build) "configure \
-nbmap \[" $itk_component(build) " get -nbmap\] ;" \
		  $itk_component(buildfw) "configure \
-keepthres \[" $itk_component(buildfw) " get -keepthres\] \
-addthres \[" $itk_component(buildfw) " get -addthres\]  ;" \
		  $itk_component(paretolkh) "configure \
-res \[" $itk_component(paretolkh) " get -res\] ;"\
		  $itk_component(greedy) "configure \
-nbiter \[" $itk_component(greedy) " get -nbiter\] \
-nbrun \[" $itk_component(greedy) " get -nbrun\] \
-taboolenmax \[" $itk_component(greedy) " get -taboolenmax\] \
-taboolenmin \[" $itk_component(greedy) " get -taboolenmin\] \
-anytime \[" $itk_component(greedy) " get -anytime\] ;" \
		  $itk_component(algogen) "configure \
-nbgens \[" $itk_component(algogen) " get -nbgens\] \
-nbele \[" $itk_component(algogen) " get -nbele\] \
-selnbr \[" $itk_component(algogen) " get -selnbr\] \
-pcross \[" $itk_component(algogen) " get -pcross\] \
-pmut \[" $itk_component(algogen) " get -pmut \] \
-evolfitness \[" $itk_component(algogen) " get -evolfitness\] ;" \
		  $itk_component(flips) "configure \
-wins \[" $itk_component(flips) " get -wins\] \
-fthres \[" $itk_component(flips) " get -fthres\] \
-fiter \[" $itk_component(flips) " get -fiter\] ;" \
		  $this "deactivate 1" ]
    
    set cmd2 [concat "uplevel 0 CGWMsearch \[lindex \[" $itk_component(tabenotbook) " pageconfigure select -label\] end\]"]

    set cmd3 [concat $itk_component(annealing) "configure \
-tries \[" $itk_component(annealing) " cget -tries\] \
-tinit \[" $itk_component(annealing) " cget -tinit\] \
-tfinal \[" $itk_component(annealing) " cget -tfinal\] \
-cooling \[" $itk_component(annealing) " cget -cooling\] ;" \
		 $itk_component(build) "configure \
-nbmap \[" $itk_component(build) " cget -nbmap\] ;" \
		  $itk_component(buildfw) "configure \
-keepthres \[" $itk_component(buildfw) " cget -keepthres\] \
-addthres \[" $itk_component(buildfw) " cget -addthres\]  ;" \
		  $itk_component(paretolkh) "configure \
-res \[" $itk_component(paretolkh) " cget -res\] ;" \
		 $itk_component(greedy) "configure \
-nbiter \[" $itk_component(greedy) " cget -nbiter\] \
-nbrun \[" $itk_component(greedy) " cget -nbrun\] \
-taboolenmax \[" $itk_component(greedy) " cget -taboolenmax\] \
-taboolenmin \[" $itk_component(greedy) " cget -taboolenmin\] \
-anytime \[" $itk_component(greedy) " cget -anytime\] ;" \
		  $itk_component(algogen) "configure \
-nbgens \[" $itk_component(algogen) " cget -nbgens\] \
-nbele \[" $itk_component(algogen) " cget -nbele\] \
-selnbr \[" $itk_component(algogen) " cget -selnbr\] \
-pcross \[" $itk_component(algogen) " cget -pcross\] \
-pmut \[" $itk_component(algogen) " cget -pmut \] \
-evolfitness \[" $itk_component(algogen) " cget -evolfitness\] ;" \
		 $itk_component(flips) "configure \
-wins \[" $itk_component(flips) " cget -wins\] \
-fthres \[" $itk_component(flips) " cget -fthres\] \
-fiter \[" $itk_component(flips) " cget -fiter\] ;" \
		 $this "deactivate 0" ]

    buttonconfigure Apply \
	-command "$cmd1; $cmd2; $cmd3" \
	-text Try
    
    buttonconfigure Cancel \
	-command $cmd3
    
    buttonconfigure OK \
	-command $cmd1

    pack $itk_component(tabenotbook) -fill both -expand yes


    eval itk_initialize $args
}

body CGconfigSearch::get {arg} {
    
    switch -- $arg {
	"-res" { return [$itk_component(paretolkh) get -res]}
#	"-nbrunp" { return [$itk_component(paretolkh) get -nbrunp]}
#	"-colm" { return [$itk_component(paretolkh) get -colm]}
        "-tries" { return [$itk_component(annealing) get -tries]}
        "-tinit" { return [$itk_component(annealing) get -tinit]}
        "-tfinal" { return [$itk_component(annealing) get -tfinal]}
        "-cooling" { return [$itk_component(annealing) get -cooling]}
	"-nbmap" { return [$itk_component(build) get -nbmap]}
	"-keepthres" { return [$itk_component(buildfw) get -keepthres]}
	"-addthres" { return [$itk_component(buildfw) get -addthres]}
	"-nbiter" { return [$itk_component(greedy) get -nbiter]}
	"-nbrun" { return [$itk_component(greedy) get -nbrun]}
	"-taboolenmax" { return [$itk_component(greedy) get -taboolenmax]}
	"-taboolenmin" { return [$itk_component(greedy) get -taboolenmin]}
	"-anytime" { return [$itk_component(greedy) get -anytime]}
	"-nbgens" { return [$itk_component(algogen) get -nbgens]}
	"-nbele" { return [$itk_component(algogen) get -nbele ]}
	"-selnbr" { return [$itk_component(algogen) get -selnbr]}
	"-pcross" { return [$itk_component(algogen) get -pcross]}
	"-pmut" { return [$itk_component(algogen) get -pmut]}
	"-evolfitness" { return [$itk_component(algogen) get -evolfitness]}
	"-wins" { return [$itk_component(flips) get -wins]}
	"-fthres" { return [$itk_component(flips) get -fthres]}
	"-fiter" { return [$itk_component(flips) get -fiter]}
        default {
            error "unknown parameter $arg."
        }
    }
}

