#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGparetoS.tcl,v 1.2 2004-12-16 10:05:30 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Shell widget for the pareto canvas
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

# ------------------------------------------------------------------
#                           SELECTIONDIALOG
# ------------------------------------------------------------------
class CGpp {
    inherit iwidgets::Shell

    constructor {args} {}
    
    public method plot {pl}

}

proc cgpp {pathName args} {
    uplevel CGpp $pathName $args
}

body CGpp::constructor {args} {
    
    component hull configure -borderwidth 0

    itk_component add li {
	
	label $itk_interior.li
    } {

    }

    pack $itk_component(li) -side bottom -fill x

    itk_component add pp {
	
	cgparetoplot $itk_interior.pp \
	    -width [expr int(floor(500 *29.7/21))]\
	    -height 500 \
	    -wli $itk_component(li)
    } {
	ignore -height -width
    }

    pack $itk_component(pp)   -after $itk_component(li) -fill both -expand yes
    
    eval itk_initialize $args

    configure -title "CarthaG�ne : Pareto frontier approximation"
}   

body CGpp::plot {pl} {
 
    $itk_component(pp) plot $pl

}
