#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigParetoLKH.tcl,v 1.1 2005-01-14 17:25:27 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this progam; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Class that provide a dialog to choose the parameters of the
# ParetoLKH command
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#

#
# Options usuelles.
#
itk::usual CGconfigParetoLKH {
    keep -background -cursor -foreground
    # -modality
}


class CGconfigParetoLKH {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -res res Res 5
    #itk_option define -nbrunp nbrunp Nbrunp 3
    #itk_option define -colm colm ColM 2

    constructor {args} {}
    public method get {arg}

}

proc cgconfigparetolkh {pathName args} {
    uplevel CGconfigParetoLKH $pathName $args
}

body CGconfigParetoLKH::constructor {args} {
    
    itk_component add commentaires {
        label $itk_interior.commentaires \
	    -text "Creating a Pareto Frontier"
    }
    pack $itk_component(commentaires) -padx 4 -pady 5
    
    itk_component add res {
	iwidgets::spinint $itk_interior.res \
	    -range {1 300} \
	    -width 3 \
	    -labeltext "Resolution(number of markers)"
    }
    pack $itk_component(res) -padx 10 -pady 10 -anchor e
    
#   itk_component add nbrunp {
#         iwidgets::spinint $itk_interior.nbrunp \
# 	    -range {0 20} -width 2 \
# 	    -labeltext "Number of runs"
#     }
#     pack $itk_component(nbrunp) -padx 10 -pady 10 -anchor e
    
#     itk_component add colm {
# 	iwidgets::spinint $itk_interior.colm \
# 	    -range {0 5} -width 2 \
# 	    -labeltext "Backtrack move type"
# 	}
#     pack $itk_component(colm) -padx 10 -pady 10 -anchor e
    
    eval itk_initialize $args
}


configbody CGconfigParetoLKH::res {

    set nbr $itk_option(-res)
    if {$nbr < 0 || $nbr > 300} {
        error "wrong value \"res\": should be between 0 and 300"
    } else {
        $itk_component(res) delete 0 end
        $itk_component(res) insert 0 $nbr
    }
}


# configbody CGconfigParetoLKH::nbrunp {

#     set nbr $itk_option(-nbrunp)
#     if {$nbr < 0 || $nbr > 20} {
#         error "wrong value \"nbrunp\": should be between 0 and 20"
#     } else {
#         $itk_component(nbrunp) delete 0 end
#         $itk_component(nbrunp) insert 0 $nbr
#     }
# }


# configbody CGconfigParetoLKH::colm {
    
#     set nbr $itk_option(-colm)
#     if {$nbr < 0 || $nbr > 5} {
#         error "wrong value \"colm\": should be between 0 and 5"
#     } else {
#         $itk_component(colm) delete 0 end
#         $itk_component(colm) insert 0 $nbr
#     }
# }




body CGconfigParetoLKH::get {arg} {
    
    switch -- $arg {
        "-res" { return [$itk_component(res) get]}
#         "-nbrunp" { return [$itk_component(nbrunp) get]}
#         "-colm" { return [$itk_component(colm) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}
