#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGcanvasprintbox.tcl,v 1.1 2004-11-16 11:43:59 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Module 	: cgwin 
# Projet	: CartaGene
#
# Description : just to provide enhancement of the original Canvasprintbox
#
# Divers : should be proposed to the maintainer
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

class CGCanvasprintbox {
    inherit iwidgets::Canvasprintbox
    
    constructor { args } {}
    
    public method print {}
    
    
}

#
# Provide a lowercased access method for the Canvasprintbox class.
# 
proc cgcanvasprintbox {args} {
	uplevel CGCanvasprintbox $args
}

body CGCanvasprintbox::constructor { args } {
    
    eval itk_initialize $args
}


#
# Almost the same function(only one dimension of the paper size is
# provided to the canvas postcript command, it does enhance
# the postcript output. Only in the vertical case as been enhanced.
#

body CGCanvasprintbox::print {} {
    
    global env tcl_platform
    
    stop

    # enable the command field to be used as soon as possible
    _update_attr now

    if {$itk_option(-output) == "file"} {
	set nm $_globVar($this,fileef)
	if {[string range $nm 0 1] == "~/"} {
	    set nm "$env(HOME)/[string range $nm 2 end]"
	}
    } else {
	set nm "/tmp/xge[winfo id $canvas]"
    }
    
    set pr [_calc_print_region]
    set x1 [lindex $pr 0]
    set y1 [lindex $pr 1]
    set x2 [lindex $pr 2]
    set y2 [lindex $pr 3]
    set cx [expr {int(($x2 + $x1) / 2)}]
    set cy [expr {int(($y2 + $y1) / 2)}]
    if {!$itk_option(-stretch)} {
	set ps [_calc_poster_size]
	set pshw [expr {int([lindex $ps 0] / 2)}]
	set pshh [expr {int([lindex $ps 1] / 2)}]
	set x [expr {$cx - $pshw}]
	set y [expr {$cy - $pshh}]
	set w [ezPaperInfo $itk_option(-pagesize) pwidth $itk_option(-orient) $win]
	set h [ezPaperInfo $itk_option(-pagesize) pheight $itk_option(-orient) $win]
    } else {
	set x $x1
	set y $y1
	set w [expr {($x2-$x1) / $_globVar($this,hpc)}]
	set h [expr {($y2-$y1) / $_globVar($this,vpc)}]
    }
    
    set i 0
    set px $x
    while {$i < $_globVar($this,hpc)} {
	set j 0
	set py $y
	while {$j < $_globVar($this,vpc)} {
	    set nm2 [expr {$_globVar($this,hpc) > 1 || $_globVar($this,vpc) > 1 ? "$nm$i.$j" : $nm}]
	    
	    if {$itk_option(-stretch)} {
		$canvas postscript \
		    -file $nm2 \
		    -rotate $rotate \
		    -x $px -y $py \
		    -width $w \
		    -height $h \
		    -pagex [ezPaperInfo $itk_option(-pagesize) centerx] \
		    -pagey [ezPaperInfo $itk_option(-pagesize) centery] \
		    -pageheight [ezPaperInfo $itk_option(-pagesize) pheight $itk_option(-orient)]		   
	
	    } else {
		$canvas postscript \
		    -file $nm2 \
		    -rotate $rotate \
		    -x $px -y $py \
		    -width $w \
		    -height $h \
		    -pagex [ezPaperInfo $itk_option(-pagesize) centerx] \
		    -pagey [ezPaperInfo $itk_option(-pagesize) centery]
	    }
	    
	    if {$itk_option(-output) == "printer"} {
		set cmd "$itk_option(-printcmd) < $nm2"
		if {[catch {eval exec $cmd &}]} {
		    return 0
		}
	    }
	    
	    set py [expr {$py + $h}]
	    incr j
	}
	set px [expr {$px + $w}]
	incr i
    }
    
    return 1
}
