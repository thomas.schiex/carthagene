#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGdlbDialog.tcl,v 1.4 2002-11-07 16:59:02 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue pour la s�lection
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Usual options.
#
itk::usual CGdlbDialog {
    keep -activebackground -activerelief -background -borderwidth -cursor \
	 -elementborderwidth -foreground -highlightcolor -highlightthickness \
	 -insertbackground -insertborderwidth -insertofftime -insertontime \
	 -insertwidth -jump -labelfont -modality -selectbackground \
	 -selectborderwidth -selectforeground -textbackground -textfont \
	 -troughcolor
}

# ------------------------------------------------------------------
#                           SELECTIONDIALOG
# ------------------------------------------------------------------
class CGdlbDialog {
    inherit iwidgets::Dialog

    constructor {args} {}

    public method setlhs { larg }
    public method setrhs { larg }
    public method get {}

}
    
proc cgdlbdialog {pathName args} {
    uplevel CGdlbDialog $pathName $args
}

body ::iwidgets::Disjointlistbox::insert {theListbox items} {

  set curritems [$theListbox get 0 end]

  foreach item $items {
    #
    # if the item is not already present in the Listbox then insert it
    #
    if {[lsearch -exact $curritems $item] == -1} {
      $theListbox insert end $item
    }
  }
#  $theListbox sort increasing
  showCount
}

body ::iwidgets::Disjointlistbox::transfer {} {

  if {[$sourceListbox selecteditemcount] == 0} {
    return
  }
  set selectedindices [lsort -integer -decreasing [$sourceListbox curselection]]
  set selecteditems [$sourceListbox getcurselection]

  foreach index $selectedindices {
    $sourceListbox delete $index
  }

  foreach item $selecteditems {
    $destinationListbox insert end $item
  }
  #$destinationListbox sort increasing

  showCount
}

# ------------------------------------------------------------------
#                        CONSTRUCTOR
# ------------------------------------------------------------------
body CGdlbDialog::constructor {args} {
    #
    # Set the borderwidth to zero.
    #
    component hull configure -borderwidth 0
    
    # 
    # Instantiate a selection box widget.
    #
    itk_component add dlbox {
	iwidgets::disjointlistbox $itk_interior.disjointlistbox
    } {
	usual
    }
    
    pack $itk_component(dlbox) -fill both -expand yes
    
    hide Help
    hide Apply

    eval itk_initialize $args
}   

# ------------------------------------------------------------------
#                            METHODS
# ------------------------------------------------------------------


body CGdlbDialog::setlhs { larg } {
    return [$itk_component(dlbox) setlhs $larg]
}

body CGdlbDialog::setrhs { larg } {
    return [$itk_component(dlbox) setrhs $larg]
}

body CGdlbDialog::get {} {
    return [$itk_component(dlbox) getrhs]
}
