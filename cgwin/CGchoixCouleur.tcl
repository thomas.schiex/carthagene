#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGchoixCouleur.tcl,v 1.9 2005-01-13 16:17:47 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue de s�lection de couleurs pour les diff�rents
# �l�ments de repr�sentation des cartes dans le tas
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
## much to complicate
## this class should disappear 


option add *ColorDialog.title "Cartagene : Color preferences" widgetDefault

#
#
#
itk::usual ColorDialog {
    keep -cursor -foreground -modality
}

#
#
#
class ColorDialog {
    inherit iwidgets::Dialog

    itk_option define -logtxt logtxt LogTxt magenta
    itk_option define -logfle logfle LogFle cyan
    itk_option define -carte carte Carte blue
    itk_option define -mrqtxt mrqtxt MrqTxt black
    itk_option define -mrqtxtsel mrqtxtsel MrqTxtSel orange
    itk_option define -mrqlineon  mrqlineon MrqLineOn green 
    itk_option define -mrqlineoff mrqlineoff MrqLineOff black 
    itk_option define -infoline infoline InfoLine red
    itk_option define -infotxt infotxt InfoTxt red
    itk_option define -infobox infobox InfoBox yellow

    itk_option define -infofont infofont InfoFont "-adobe-times-bold-r-normal--10-100-75-75-p-57-iso8859-1"
    itk_option define -mrqfont mrqfont MrqFont "-adobe-times-bold-r-normal--12-120-75-75-p-67-iso8859-1"
    itk_option define -logfont logfont LogFont "-adobe-times-bold-r-normal--12-120-75-75-p-67-iso8859-1"

    public variable couleurs "" 
    private variable _typeObjet ""
    private variable _configCouleurs 0
    private variable _info
    private variable _objets {-logtxt -carte -mrqtxt -mrqlineoff -infotxt -infobox}

    constructor {args} {}
    public method cget {arg}
    private method _creeBoutonsCouleurs {}
    private method _afficheNomCouleur {focus couleur}
    private method _afficheCanvasTas {canvas}
    private method _activeChangementCouleur {w}
    private method _desactiveChangementCouleur {w}
    private method _changeCouleur {couleur}
    private method _afficheInfo {w focus}
    private method _afficheMessage {}
    private method _changeEtatBoutons {state}
    private method _cmdValider {}
    private method _cmdAbandonner {}

}


#
#
#
proc colordialog {pathName args} {
    uplevel ColorDialog $pathName $args
}


#
#
#
body ColorDialog::constructor {args} {

    eval itk_initialize $args
    
    # Message d'informations en haut de la fenetre
    itk_component add modeEmploi {
	label $itk_interior.modeEmploi \
	    -text "double-clic to select an item \n single-clic to hide an item"
    }
    pack $itk_component(modeEmploi) -pady 5

    # Repr�sentation du tas
    itk_component add canvasTas {
	canvas $itk_interior.canvasTas \
		-closeenough 2 \
		-width 410 \
		-height 250 \
                -borderwidth 2 \
		-relief raised
    }
    pack $itk_component(canvasTas) -pady 5
    _afficheCanvasTas $itk_component(canvasTas)

    # Zone d'affichage du type d'objet sur lequel passe la souris et couleur
    itk_component add selectionInfo {
	label $itk_interior.selInfo
    }
    pack $itk_component(selectionInfo) -pady 5

    # Zones d'affichage des boutons de couleur
    itk_component add zoneBoutons {
        iwidgets::labeledframe $itk_interior.zoneBoutons \
	    -labelpos s \
	    -labeltext "color box"
    }
    set win [$itk_component(zoneBoutons) childsite]
    itk_component add buttonBox0 {
	iwidgets::buttonbox $win.buttonBox0
    }
    itk_component add buttonBox1 {
	iwidgets::buttonbox $win.buttonBox1
    }
    pack $itk_component(buttonBox0) -padx 3 -pady 3 -expand yes -fill both
    pack $itk_component(buttonBox1) -padx 3 -pady 3 -expand yes -fill both
    pack $itk_component(zoneBoutons) -padx 3 -pady 5 -expand yes -fill both

    # Zone d'affichage des noms des couleurs
    itk_component add focusInfo {
	label $itk_interior.focusInfo
    }
    pack $itk_component(focusInfo)


    _creeBoutonsCouleurs

    default Cancel
    hide Help
    hide Apply
    buttonconfigure OK -command [code $this _cmdValider]
    buttonconfigure Cancel -command [code $this _cmdAbandonner]
}


#
#
#
body ColorDialog::_creeBoutonsCouleurs {} {
    
    set couleurs [split $itk_option(-couleurs)]

    if {[llength $couleurs] > 0} {

        # Ajoute les couleurs r�f�renc�es
        # et celles pass�es en param�tre
        set ref ""
        foreach obj $_objets {
            if {[lsearch $ref $itk_option($obj)] == -1} {
                lappend ref $itk_option($obj)
            }
        }

        # Retire les couleurs en double
        foreach coul $couleurs {
            if {[lsearch $ref $coul] == -1} {
                lappend ref $coul
            }
        }
        set couleurs $ref
        unset ref

        # Limite le nombre de couleurs affich�es � 16
        if {[llength $couleurs] > 22} {
            set couleurs [lrange $couleurs 0 21]
        }

        # Si le nombre de couleurs est trop grand on affiche sur deux lignes
        if {[llength $couleurs] > 8} {
            set moitie [expr [llength $couleurs] / 2]
            lappend groupesCouleurs [lrange $couleurs 0 [expr $moitie - 1]]
            lappend groupesCouleurs [lrange $couleurs $moitie end]
        } else {
            set groupesCouleurs [list $couleurs]
        }

        set i 0
        foreach groupeCouleurs $groupesCouleurs {
            foreach couleur $groupeCouleurs {
                $itk_component(buttonBox$i) add $couleur \
		    -height 25 \
		    -width 25 \
		    -background $couleur \
		    -activebackground $couleur \
		    -defaultring yes \
		    -defaultringpad 0 \
		    -command [code $this _changeCouleur $couleur]
                $itk_component(buttonBox$i) hide $couleur
            }
            incr i
        }
    }
}


#
#
#
body ColorDialog::_afficheNomCouleur {focus couleur} {

    if {"$focus" == "Enter"} {
	$itk_component(focusInfo) configure -text $couleur
    } else {
	$itk_component(focusInfo) configure -text ""
    }
}


#
#
#
body ColorDialog::_changeEtatBoutons {state} {

    set child0 [llength [winfo children $itk_component(buttonBox0)]]
    set child1 [llength [winfo children $itk_component(buttonBox1)]]
    
#    puts _changeEtatBoutons

    switch $state {
        normal {
            for {set i 0} {$i < $child0} {incr i} {
#		puts "show $i"
                $itk_component(buttonBox0) show $i
            }
            for {set i 0} {$i < $child1} {incr i} {
                $itk_component(buttonBox1) show $i
            }
        }
        disabled {
            for {set i 0} {$i < $child0} {incr i} {
                $itk_component(buttonBox0) hide $i
            }
            for {set i 0} {$i < $child1} {incr i} {
                $itk_component(buttonBox1) hide $i
            }
        }
        active { }
        default {
            puts "ERREUR=$state : les boutons ne peuvent prendre que les �tats normal, active ou disabled"
        }
    }
}


#
#
#
body ColorDialog::_changeCouleur {couleur} {

    set w $itk_component(canvasTas)
    set listeObjets [$w find withtag $_typeObjet]
    #$w itemconfigure $_typeObjet -fill $couleur

    if {"$_typeObjet" == "-infotxt"} {
	foreach objet $listeObjets {
	    if {[$w type $objet] == "rectangle"} {
		$w itemconfigure $objet -outline $couleur
	    } else {
		$w itemconfigure $objet -fill $couleur
	    }
	}
    } elseif {"$_typeObjet" == "-infobox"} {
	foreach objet $listeObjets {
	    if {[$w type $objet] == "rectangle"} {
		$w itemconfigure $objet -fill $couleur
	    } else {
		$w itemconfigure $objet -outline $couleur
	    }
	}
    } else {
	$w itemconfigure $_typeObjet -fill $couleur
    }

    # Met � jour la variable qui m�morise la couleur
    set _info($_typeObjet,couleur) $couleur

    # Met � jour la ligne d'information
    set texte $_info($_typeObjet,info)
    set message [concat $texte " : " $couleur]
     $itk_component(selectionInfo) configure -text $message

}


#
#
#
body ColorDialog::_afficheCanvasTas {canvas} {

    set marqueurs {il2a An5v Li3b Pa1}
    # dans l'ordre: 
    # cout, hauteur, longueur, dist. mqr0, dist. mqr1, dist. mqr2, dist. mqr3.
    lappend cartes [set carte(1) {-582.87   0 190  0 50 150 190}]
    lappend cartes [set carte(2) {-583.05  80 220 40  0 220 140}]
    lappend cartes [set carte(3) {-583.55 120 150  0 90 150 110}]
    #set cartes [list $carte(1) $carte(2) $carte(3)]

    set nbMrq [llength $marqueurs]
    set nbCrt [llength $cartes]

    set margeV 100
    set margeH 100

    #
    # Affiche les cartes
    #
    for {set iC 1} {$iC <= $nbCrt} {incr iC 1} {

	$canvas create line \
		$margeH \
		[expr $margeV + [lindex $carte($iC) 1]] \
		[expr $margeH + [lindex $carte($iC) 2]] \
		[expr $margeV + [lindex $carte($iC) 1]] \
		-tag "-carte appel" \
		-width 3 \
		-fill $itk_option(-carte)
    }
    set _info(-carte,info) "Cartes"
    set _info(-carte,couleur) $itk_option(-carte)

    #
    # Affiche les lignes liant les marqueurs d'une carte � l'autre
    #

    for {set iM 0} {$iM < $nbMrq} {incr iM 1} {

	for {set iC 1} {$iC < [llength $cartes]} {incr iC 1} {
	    
	    $canvas create line \
		    [expr $margeH + [lindex $carte($iC) [expr 3+$iM]]] \
		    [expr $margeV + [lindex $carte($iC) 1]] \
		    [expr $margeH + [lindex $carte([expr $iC + 1]) [expr 3+$iM]]] \
		    [expr $margeV + [lindex $carte([expr $iC + 1]) 1]] \
		    -tag "-mrqlineoff appel" \
		    -fill $itk_option(-mrqlineoff)
	}
	
    }
    set _info(-mrqlineoff,info) "Marqueurs et liens d'une carte � l'autre"
    set _info(-mrqlineoff,couleur) $itk_option(-mrqlineoff)


    #
    # Affiche les losanges symbolisant des marqueurs
    #
    for {set iM 0} {$iM < $nbMrq} {incr iM 1} {

	for {set iC 1} {$iC <= [llength $cartes]} {incr iC 1} {
	    set xm [expr $margeH + [lindex $carte($iC) [expr 3+$iM]]]
	    set ym [expr $margeV + [lindex $carte($iC) 1]]
	    $canvas create polygon \
		    $xm \
		    [expr $ym - 4] \
		    [expr $xm + 4] \
		    $ym \
		    $xm \
		    [expr $ym + 4] \
		    [expr $xm - 4] \
		    $ym \
		    -tag "-mrqlineoff appel" \
		    -fill $itk_option(-mrqlineoff)
	    
	}
    }


    #
    # Affiche le nom des marqueurs
    #
    set interval [expr [lindex $carte(1) 2] / ($nbMrq - 1)]

    for {set iM 0} {$iM < $nbMrq} {incr iM 1} {

	$canvas create text \
		[expr $margeH + ($iM * $interval)] \
		[expr $margeV / 2] \
		-tag "-mrqtxt appel" \
		-text [lindex $marqueurs $iM] \
		-anchor s \
                -font $itk_option(-mrqfont) \
		-fill $itk_option(-mrqtxt)
    }
    set _info(-mrqtxt,info) "Nom des marqueurs"
    set _info(-mrqtxt,couleur) $itk_option(-mrqtxt)


    #
    # Affiche les fl�ches reliant les noms des marqueurs
    # aux marqueurs de la premi�re carte

    for {set iM 0} {$iM < $nbMrq} {incr iM 1} {

	$canvas create line \
		[expr $margeH + ($iM * $interval)] \
		[expr $margeV / 2] \
		[expr $margeH + [lindex $carte(1) [expr 3+$iM]]] \
		[expr $margeV + [lindex $carte(1) 1] - 5] \
		-tag "-mrqlineoff appel" \
		-arrow last \
		-fill $itk_option(-mrqlineoff)
    }

    #
    # Affiche les vraisemblances de chaque carte
    #
    for {set iC 1} {$iC <= [llength $cartes]} {incr iC 1} {

	$canvas create text \
		0 \
		[expr $margeV + [lindex $carte($iC) 1]] \
		-tag "-logtxt appel" \
		-text [lindex $carte($iC) 0] \
		-anchor w \
	        -font $itk_option(-logfont) \
		-fill $itk_option(-logtxt)
    }
    set _info(-logtxt,info) "Vraisemblance des cartes"
    set _info(-logtxt,couleur) $itk_option(-logtxt)


    #
    # Affiche la fl�che reliant la vraisemblance � sa carte
    #
    for {set iC 1} {$iC <= [llength $cartes]} {incr iC 1} {

	$canvas create line \
		[expr $margeH / 2] \
		[expr $margeV + [lindex $carte($iC) 1]] \
		[expr $margeH - 5] \
		[expr $margeV + [lindex $carte($iC) 1]] \
		-arrow last \
		-tag "-carte appel" \
		-fill $itk_option(-carte)
    }


    #
    # Affiche les informations des marqueurs d'une carte (la deuxi�me)
    #

    for {set iM 0} {$iM < $nbMrq} {incr iM 1} {
	
	set crt [lindex $cartes 1]
	
	set xm [expr $margeH + [lindex $crt [expr 3+$iM]]]
	set ym [expr $margeV + [lindex $crt 1]]

	set xe $xm
	set ye [expr $ym - 30]

	# Trace la ligne reliant le marqueur � l'�tiquette
	
	$canvas create line $xm $ym $xe $ye \
		-tag "-infotxt" \
		-fill $itk_option(-infoline)

	# Affiche le texte de l'information (la position du marqueur sur la carte)

	set T [$canvas create text $xe $ye \
		-fill $itk_option(-infotxt) \
		-tag "-infotxt message" \
 	        -font $itk_option(-infofont) \
		-text [lindex $crt [expr $iM + 3]]]

	# Affiche le rectangle qui entoure le texte

	set coord [$canvas bbox $T]

	set R [$canvas create rectangle \
		[expr [lindex $coord 0] - 2] \
		[expr [lindex $coord 1] - 2] \
		[lindex $coord 2] \
		[lindex $coord 3] \
		-fill $itk_option(-infobox) \
		-tag "-infobox -infotxt" \
		-outline $itk_option(-infotxt)]

	$canvas lower $R $T

    }

    # Ajoute dans un cartouche
    # - un rectangle sans bordure pour symboliser le fond de l'�tiquette
    # - un rectangle avec un nombre pour repr�senter le nombre
    # - des commentaires

    set infoRect {320 50 400 120}

    $canvas create rectangle \
	    [lindex $infoRect 0] \
	    [lindex $infoRect 1] \
	    [lindex $infoRect 2] \
	    [lindex $infoRect 3] 

    #
    $canvas create text \
	    [expr [lindex $infoRect 0] + 40] \
	    [expr [lindex $infoRect 1] + 15] \
 	    -font $itk_option(-infofont) \
	    -text "Labels"

    #
    set T [$canvas create text \
	    [expr [lindex $infoRect 0] + 15] \
	    [expr [lindex $infoRect 1] + 35] \
	    -text "6" \
 	    -font $itk_option(-infofont) \
	    -fill $itk_option(-infotxt) \
	    -tag "-infotxt appel"]

    set coord [$canvas bbox $T]

    $canvas create rectangle             \
	    [expr [lindex $coord 0] - 2] \
	    [expr [lindex $coord 1] - 2] \
	    [lindex $coord 2] \
	    [lindex $coord 3] \
	    -tag "-infobox appel" \
	    -fill $itk_option(-infobox)

    $canvas create text \
	    [expr [lindex $infoRect 0] + 45] \
	    [expr [lindex $infoRect 1] + 35] \
 	    -font $itk_option(-infofont) \
	    -text "background"

    #
    set T [$canvas create text \
	    [expr [lindex $infoRect 0] + 15] \
	    [expr [lindex $infoRect 3] - 15] \
	    -text "6"  \
 	    -font $itk_option(-infofont) \
	    -fill $itk_option(-infotxt) \
	    -tag "-infotxt appel"]

    set coord [$canvas bbox $T]

    set R [$canvas create rectangle \
	    [expr [lindex $coord 0] - 2] \
	    [expr [lindex $coord 1] - 2] \
	    [lindex $coord 2] \
	    [lindex $coord 3] \
	    -tag "-infotxt"       \
	    -outline $itk_option(-infotxt)]

	$canvas lower $R $T

    $canvas create text \
	    [expr [lindex $infoRect 0] + 45] \
	    [expr [lindex $infoRect 3] - 15] \
 	    -font $itk_option(-infofont) \
 	    -text "text"

    set _info(-infotxt,info) "Text of the label (distance)"
    set _info(-infotxt,couleur) $itk_option(-infotxt)

    set _info(-infobox,info) "Label background"
    set _info(-infobox,couleur) $itk_option(-infobox)


    #
    # S�lectionne une "zone de marqueur" constitu�e de :
    # - nom du marqueur
    # - fl�che du nom du marqueur vers sa place sur la premi�re carte
    # - les symboles du marqueur sur les cartes
    # - les lignes reliant le marqueur d'une carte � l'autre
    # et affecte la couleur de s�lection sp�cifique � chaque �l�ment


    $canvas bind appel <Double-1> [code $this _activeChangementCouleur $canvas]

    bind $canvas <Button-1> [code $this _desactiveChangementCouleur $canvas]
    bind $canvas <Shift-Button-1> { }
    bind $canvas <Double-1> { }

    $canvas bind appel <Enter> [code $this _afficheInfo %W Enter]
    $canvas bind appel <Leave> [code $this _afficheInfo %W Leave]

    $canvas bind message <Enter> [code $this _afficheMessage]
    $canvas bind message <Leave> [code $this _afficheInfo %W Leave]

}


#
#
#
body ColorDialog::_activeChangementCouleur {w} {

    set tagId [$w find withtag current]
    set _typeObjet [lindex [$w gettags $tagId] 0]

    _changeEtatBoutons normal

    $w bind appel <Enter> ""
    $w bind appel <Leave> ""

    $w bind message <Enter> ""
    $w bind message <Leave> ""
}


#
#
#
body ColorDialog::_desactiveChangementCouleur {w} {

#    set tagId [$w find withtag current]
    _changeEtatBoutons disabled

    $w bind appel <Enter> [code $this _afficheInfo %W Enter]
    $w bind appel <Leave> [code $this _afficheInfo %W Leave]

    $w bind message <Enter> [code $this _afficheMessage]
    $w bind message <Leave> [code $this _afficheInfo %W Leave]

    set objetEnCours ""
}


#
#
#
body ColorDialog::_afficheInfo {w focus} {

    if {"$focus" == "Enter"} {
	set tagId [$w find withtag current]
	#set couleur [$w itemcget $tagId -fill]
	set tagName [lindex [$w gettags $tagId] 0]
	set couleur $_info($tagName,couleur)
	set texte $_info($tagName,info)
	set message [concat $texte " : " $couleur]
	$itk_component(selectionInfo) configure -text $message
    } else {
	$itk_component(selectionInfo) configure -text ""
    }    
}


#
#
#
body ColorDialog::_afficheMessage {} {
    $itk_component(selectionInfo) configure \
	    -text "See the box \"Labels\""
}

configbody ColorDialog::logtxt {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-logtxt)
    
    if {[lsearch $couleurs $couleur]== -1} {
	error "wrong value \"-logtxt $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::logfle {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-logfle)
    
    if {[lsearch $couleurs $couleur]== -1} {
	error "wrong value \"-logfle $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::carte {

    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-carte)
  
    if { [lsearch $couleurs $couleur] == -1} {
	error "wrong value \"-carte $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::mrqtxt {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-mrqtxt)
    
    if {[lsearch $couleurs $couleur]== -1} {
	error "wrong value \"-mrqtxt $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::mrqtxtsel {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-mrqtxtsel)
    
    if {[lsearch $couleurs $couleur] == -1} {
	error "wrong value \"-mrqtxtsel $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::mrqlineon {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-mrqlineon)
    
    if {[lsearch $couleurs $couleur]== -1} {
	error "wrong value \"-mrqlineon $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::mrqlineoff {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-mrqlineoff)

   if {[lsearch $couleurs $couleur] == -1} {
	error "wrong value \"-mrqlineoff $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::infoline {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-infoline)
    
    if {[lsearch $couleurs $couleur]== -1} {
	error "wrong value \"-infoline $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::infotxt {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-infotxt)
    
    if {[lsearch $couleurs $couleur]== -1} {
	error "wrong value \"-infotxt $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::infobox {
    
    set couleurs [split $itk_option(-couleurs)]
    set couleur $itk_option(-infobox)
    
    if {[lsearch $couleurs $couleur]== -1} {
	error "wrong value \"-infobox $couleur\".\nUnextected color."
    }
}

configbody ColorDialog::infofont {
    
}

configbody ColorDialog::mrqfont {
    
}

configbody ColorDialog::logfont {
    
}


#
#
#
body ColorDialog::_cmdValider {} {
    
    foreach obj $_objets {  
	if {$itk_option($obj) != $_info($obj,couleur) } {
	    set itk_option($obj) $_info($obj,couleur)
	    uplevel #0 set CGWhaschanged 1
	}
    }
    
    $this deactivate 1
}

#
#
#
body ColorDialog::_cmdAbandonner {} {
    
    foreach obj $_objets {  
	$this configure $obj $itk_option($obj)	
    }

    $itk_component(canvasTas) delete all

    _afficheCanvasTas $itk_component(canvasTas)
  
    $this deactivate 1
}

body ColorDialog::cget {arg} {

    return $itk_option($arg)

}
