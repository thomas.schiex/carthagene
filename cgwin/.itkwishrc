#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: .itkwishrc,v 1.3 2002-11-07 16:58:08 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module       : cgwin
# Projet       : CartaGene
#
# Description  : pour utiliser CartaGene directement � travers tclsh.
#
#-----------------------------------------------------------------------------
#-INRA--------------------Station de Biom�trie et d''Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# to load Cartagene
#
load ~/CartaGene/dev/cgsh/libcartagene.so
#
# to make easier to use
# to make the commands visible
namespace inscope CG namespace export *
# to use without prefix
namespace import ::CG::*
#
# to use the widgets of CartaGene
set auto_path "$tcl_pkgPath ~/CartaGene/dev/cgwin"
package require CGwin
#
# to introduce
#
puts ""
puts "!!!!!!!!!!!!!!!!!!!!!!!!!!"
puts "!  Welcome to CartaGene  !"
puts "!!!!!!!!!!!!!!!!!!!!!!!!!!"
#
if {$tcl_interactive} {
    puts "Type 'help' for help."
    puts ""
    puts "Provisoirement, pour une visualitation graphique du tas :"
    puts "la commande tasser, avec comme param. u et n,"
    puts "respectivement l'unit� 'h' ou 'k' et un nombre de cartes."
}
#
# to help
#
proc help {} {

    set cmdlist [info commands ::CG::*]
    
    puts "CartaGene commands : "
    puts "===================="
    puts "miscellaneous :"
    puts "---------------"
    foreach cmd [info commands ::CG::cg*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate data sets :"
    puts "-------------------------"
    foreach cmd [info commands ::CG::ds*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate groups :"
    puts "----------------------"
    foreach cmd [info commands ::CG::group*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate loci :"
    puts "--------------------"
    foreach cmd [info commands ::CG::mrk*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate the heap :"
    puts "------------------------"
    foreach cmd [info commands ::CG::heap*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate a map :"
    puts "---------------------"
    foreach cmd [info commands ::CG::map*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to search for good maps :"
    puts "-------------------------"
    foreach cmd $cmdlist {
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }

    puts ""
    puts "Type '<command-name> -H' for more help with a particular command."
    puts "Some commands have a default behaviour :"
    puts "Type 'd_<command-name>' to use it."
    puts "To change the defaults, edit the '.tclshrc'"
    
}

#
# to provide default behavior
#
proc d_dsload {} {

    set dsfile "../../Data/mouse0.raw"

    puts "by default : loads $dsfile"

    ::CG::dsload $dsfile

}

proc d_build {} {

    set nbmap 3

    puts "by default : builds $nbmap maps at the same time"

    ::CG::build $nbmap

}

proc d_flips {} {

    set winsize 5
    set lodthr 3.0
    set iter 0
    
    puts "by default : flips once, into a window size of $winsize, with a LOD thresholf of $lodthr."
    
    ::CG::flips $winsize $lodthr $iter
    
}	

proc d_annealing {} {
    
    set tries 0
    set tempi 100.0
    set tempf 0.1
    set cooling 0.9

    puts "by default : annealing starts from a temperature of $tempi, to stop at a temperature of $tempf."
    puts "The additional number of iteration per temperature is $tries, and the cooling speed is $cooling."

    ::CG::annealing $tries $tempi $tempf $cooling
}
	
proc d_greedy {} {

    set NbRun 1
    set NbIter 0
    set TabooLenMin 1
    set TabooLenMax 15

    puts "by default : greedy does $NbRun loop, with an additional number of it�ration of $NbIter."
    puts "The minimum size of the taboo list is $TabooLenMin, and the maximum size of the list is $TabooLenMax."

    ::CG::greedy $NbRun $NbIter $TabooLenMin $TabooLenMax
}

proc d_algogen {} {

    set nb_gens 10
    set nb_elements 0
    set selectype 0
    set pcross 0.3
    set pmut 0.5
    set evol_fitness 1

    puts "by default : algogen simulate $nb_gens generation, initializing the first one from the heap."
    puts "The type of selection is the roulette wheel."
    puts "The crossing over probability is $pcross"
    puts "The mutation probability is $pmut"
    puts "The fitness is evolutive"

    ::CG::algogen $nb_gens $nb_elements $selectype $pcross $pmut $evol_fitness
}

proc d_group {} {

    set DistThres 0.5 
    set	LODThres 3.0
    puts "by default : group uses a distance threshold of $DistThres,"
    puts "and a LOD threshold of $LODThres to identify linkage groups."

    ::CG::group $DistThres $LODThres
}


#
# to pass to the unrestrained state
#

::CG::cglicense CBRUHBSFMIVQUJ

#
# to use tclreadline
#
if {$tcl_interactive} {
    package require tclreadline

    proc ::tclreadline::prompt1 {} {
	return "CG> "
    }

    foreach cmd [info commands ::CG::*] {
	::tclreadline::readline add [$cmd -u]
    }   
 
    ::tclreadline::Loop
}
