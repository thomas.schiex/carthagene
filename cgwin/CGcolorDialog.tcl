#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGcolorDialog.tcl,v 1.3 2002-11-07 16:58:08 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue pour la s�lection des couleurs des diff�rents
# objets affich�s
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
# package require Iwidgets
# package provide cgwin

#option add *CGcolorDialog.background white widgetDefault
#option add *CGcolorDialog.OK Appliquer widgetDefault
#option add *CGcolorDialog.Cancel.text Annuler widgetDefault


#
#
#
itk::usual CGcolorDialog {
    keep -background -cursor -foreground -modality
}


#
#
#
class CGcolorDialog {
    inherit iwidgets::Dialog

    public variable couleurs "" _creeBoutonsCouleurs
    public variable defaut "" _selectionCouleur

    private variable selection ""

    constructor {args} {}

    public method get {}

    private method _creeBoutonsCouleurs {}
    private method _selectionCouleur {}
    private method _afficheNomCouleur {focus couleur}
    private method _cmdValider {couleur}
}


#
#
#
proc cgcolordialog {pathName args} {   
     uplevel CGcolorDialog $pathName $args
}


#
#
#
body CGcolorDialog::constructor {args} {

    itk_component add display {
	iwidgets::buttonbox $itk_interior.display -orient vertical -pady 0
    }

    itk_component add focusInfo {
	label $itk_interior.focusInfo
    }

    default Cancel
    hide Help
    hide OK
    hide Apply

    eval itk_initialize $args

    if {$itk_option(-defaut) == ""} {
        set selection [lindex $couleurs 0]
    }

    pack $itk_component(display) $itk_component(focusInfo)
}


#
#
#
body CGcolorDialog::get {} {
    return $selection
}


#
#
#
body CGcolorDialog::_creeBoutonsCouleurs {} {

    # Retire les couleurs en double
    set ref ""
    foreach coul $couleurs {
        if {[lsearch $ref $coul] == -1} {
            lappend ref $coul
        }
    }
    set couleurs $ref
    unset ref

    foreach couleur $couleurs {
        $itk_component(display) add $couleur \
                -height 20 \
                -width 20 \
                -background $couleur \
                -activebackground $couleur \
                -defaultring yes \
                -defaultringpad 0 \
                -command [code $this _cmdValider $couleur]

    }

    foreach fils [winfo children $itk_component(display)] couleur $couleurs {
        bind $fils <Enter> \
                [code $this _afficheNomCouleur Enter $couleur]

        bind $fils <Leave> \
                [code $this _afficheNomCouleur Leave $couleur]
    }
}


#
#
#
body CGcolorDialog::_cmdValider {couleur} {

    set selection $couleur
    $this deactivate 1
}


#
#
#
body CGcolorDialog::_afficheNomCouleur {focus couleur} {
    if {"$focus" == "Enter"} {
	$itk_component(focusInfo) configure -text $couleur
    } else {
	$itk_component(focusInfo) configure -text ""
    }
}


#
#
#
body CGcolorDialog::_selectionCouleur {} {
    if {"$defaut" != ""} {
	if {[lsearch -exact $couleurs $defaut] == -1} {
	    error "The default color $defaut does not belong to the list."
	    set defaut ""
	}
    }
    set selection $defaut
}

################################################################################
# 
# button .b -text "Terminer" -command "exit"
# pack .b
# 
# CGcolorDialog .cd -modality application
# .cd configure -couleurs "black white red blue yellow green pink moccasin SlateBlue NavyBlue RoyalBlue SkyBlue PaleTurquoise SeaGreen" -defaut red
# 
# if {[.cd activate]} {
# puts "[.cd get]"
# }
# delete object .cd
# exit
