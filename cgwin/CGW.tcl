#!/bin/csh
#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGW.tcl,v 1.25.2.1 2011-11-22 14:31:57 dleroux Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Programme principal de l'Interface Homme-Machine de CartaGene
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#\
exec wish "$0"

option add *textBackground seashell
option add *Scrolledlistbox*relief flat
option add *Entryfield*relief flat
puts [pid]
wm withdraw .


# Valeur par defaut pour le repertoire par defaut du dialogue de chargement de jeu de donnees
set dataset_directory ~

# pour windows
if { $tcl_platform(platform) == "windows" } {
    source ~/carthagenerc.tcl
    lappend auto_path [file dirname [file dirname [info script]]]
    package require CGsh
} else { 
    source ~/.carthagenerc
}

lappend auto_path [file dirname [file dirname [info script]]]

package require CGwin

#afin "d'am�liorer" l'utilisation des "float" � creuser...
set tcl_precision 7

# pour windows
if { $tcl_platform(platform) == "windows" } {
    option readfile ~/cgwopt
} else {
    option readfile ~/.cgwopt
}

global CGWhaschanged
set CGWhaschanged 0

#
# pour savoir si les groupes ont �t� calcul�
#

global CGWnbgroup
set CGWnbgroup 0


#
# La principale  
#

iwidgets::mainwindow .cgmw -statusline no

wm title .cgmw "CarthaGene Window"

wm geometry .cgmw 999x888+5+5

wm protocol .cgmw WM_DELETE_WINDOW "CGWMExit"
#
# La zone de retour d'informations.
#
cgshellwidget [.cgmw childsite].st -visibleitems 40x8
[.cgmw childsite].st EvalNoTyped "lappend auto_path [file dirname [file dirname [info script]]]"
[.cgmw childsite].st EvalNoTyped "package require CGsh"


#
# Le browser de fichiers
#

iwidgets::extfileselectiondialog  .cgfb  \
    -modality application \
	-directory $dataset_directory \
    -title "CarthaGene File Selection Dialog"

#
# L'index de l'aide
#

iwidgets::selectiondialog .cghi \
    -modality application \
    -title "CarthaGene Help Index"

.cghi configure -selectionlabel "Topic:"
.cghi hide Apply


#
# Le dialog pour l'export de la meilleure carte
#

cgexportdialog .cged \
    -filebrowser .cgfb \
    -modality application \
    -selectfiln "File Name" \
    -title "CarthaGene Export Dialog"

#
# Le dialog pour la fusion de Data Set
#

cgfusiondialog .cgfd \
    -modality application \
    -selectlabg "Merging" \
    -selectlabd "with" \
    -title "CarthaGene Merging Dialog"

#
# Le dialog pour la fusion
#

cgconfigsearch .cgcs \
    -modality application \
    -title "CarthaGene Search Config"

#
# Le dialog pour les maps
#

cgcmdialog .cgcm \
    -modality application \
    -title "CarthaGene Maps Config"

#
# Le dialog pour la s�lection des locus
#

cgdlbdialog .cgsl \
    -modality application \
    -title "CarthaGene Locus Selection"

#
# Le dialog pour la config des groupes
#

cgcgdialog .cgcg \
    -modality application \
    -title "CarthaGene Group Config"


# Le dialog pour la s�lection des group
#

iwidgets::selectiondialog .cgsg \
    -modality application \
    -title "CarthaGene Group Selection"

.cgsg configure -selectionlabel "Linkage Group"

#
# Le dialog pour la fusion de deux marqueurs
#

cgfusiondialog .cgmm \
    -modality application \
    -selectlabg "Merging" \
    -selectlabd "with" \
    -title "CarthaGene Merging two markers Dialog"
#
# petite question
#


iwidgets::messagedialog .cgcq -title "CarthaGene Question" -modality application \
    -bitmap questhead -text "Save the configuration file?"

.cgcq hide Help
.cgcq buttonconfigure OK -text "Yes"
.cgcq buttonconfigure Cancel -text "No"


iwidgets::messagedialog .cgfq -title "CarthaGene Question" -modality application \
    -bitmap questhead -text "The file already exist! Do you want to replace it?"

.cgfq hide Help
.cgfq buttonconfigure OK -text "Yes"
.cgfq buttonconfigure Cancel -text "No"

iwidgets::messagedialog .cgfq2 -title "CarthaGene Question" -modality application \
    -bitmap questhead -text "The file already exist! Do you want to append it?"

.cgfq2 hide Help
.cgfq2 buttonconfigure OK -text "Yes"
.cgfq2 buttonconfigure Cancel -text "No"

#
# graphical heap dispdisplay dialog
#

cgtv .cggr

#
# pareto plot dialog
#

cgpp .cgpp


#
set imagedir "images"

#
# le menu des data set
#
.cgmw menubar add menubutton data -text "Data" -underline 0 -padx 8 -pady 2 \
    -menu {options -tearoff no
	command load -label "Load" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::dsload -h"] \
	       -command "CGWMdsload"
	command mergen -label "MerGen" -underline 3 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::dsmergen -h"] \
 	       -command "CGWMdsmergen"
	command mergor -label "MergOr" -underline 4 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::dsmergor -h"] \
 	       -command "CGWMdsmergor"
	command info -label "Info" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::dsinfo -h"] \
 	       -command "CGWMdsinfo"
	separator sep1
	command output -label "Output" -underline 0 \
	       -helpstr "Save the text output to a file." \
 	       -command "CGWMsavout"
	separator sep2
	cascade session -label "Session" -underline 0 -menu {\
	    command ssave -label "Save" -underline 0 \
		-helpstr "Save the session to a file." \
		-command "CGWMsaves"
	    command sretore -label "Restore" -underline 0 \
		-helpstr "Restore a session." \
		-command "CGWMrestores"
	    command reset -label "Reset" -underline 0 \
		-helpstr [[.cgmw childsite].st EvalNoTyped "::CG::cgrestart -h"]\
		-command "CGWMcgrestart"
	}
	separator sep3
	command exit -label "Exit" -underline 1 \
	       -helpstr "Exit this application" \
	       -command "CGWMExit"
    }

#
# le menu des Locus
#
.cgmw menubar add menubutton loci -text "Loci" -underline 0 -padx 8 -pady 2 \
    -menu {options -tearoff no
	command confgroup -label "Identify groups" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::group -h"] \
	       -command "CGWMrungroup"
	command sgroup -label "Select a group" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::mrkselset -h"] \
	       -command "CGWMselectgroup"
	command cgroup -label "Config" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::group -h"] \
	       -command "CGWMconfigroup"
	separator sep1	
	command selection -label "select by Locus" -underline 10 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::mrkselset -h"] \
	       -command "CGWMmrkselset"
	command mrkmerge -label "merge two locus" -underline 10 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::mrkmerge -h"] \
	       -command "CGWMmrkmerge"
	separator sep2
	command info -label "Info" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::mrkinfo -h"] \
	       -command "CGWMmrkinfo"
	command lod -label "Lod2pt" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::mrklod2p -h"] \
	       -command "CGWMmrklod2p"
 	command dist -label "Dist2pt" -underline 0 \
 	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::mrkdist2p -h"] \
	       -command "CGWMmrkdist2p"
 	command distfr -label "Fr2pt" -underline 0 \
 	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::mrkfr2p -h"] \
	       -command "CGWMmrkfr2p"
    }

#
# le menu de config de la recherche
#
.cgmw menubar add menubutton search -text "Search" -underline 0 -padx 8 -pady 2 \
    -menu {options -tearoff no
	command sem -label "SEM" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::sem -h"] \
	       -command "CGWMsem"	       
	command nicemapd -label "Nicemapd" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::nicemapd -h"] \
	       -command "CGWMnicemapd"	       
	command nicemapl -label "Nicemapl" -underline 7 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::nicemapl -h"] \
	       -command "CGWMnicemapl"	 
	command build -label "Build" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::build -h"] \
	       -command "CGWMbuild"
	command buildfw -label "BuildFW" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::buildfw -h"] \
	       -command "CGWMbuildfw"
	command paretolkh -label "Create Frontier" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::paretolkh -h"] \
	       -command "CGWMparetolkh"
	command annealing -label "Annealing" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::annealing -h"] \
	       -command "CGWMannealing"
	command greedy -label "Taboo" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::greedy -h"] \
	       -command "CGWMgreedy"
	command algogen -label "Genetic" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::algogen -h"] \
	       -command "CGWMalgogen"
	command flips -label "Flips" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::flips -h"] \
	       -command "CGWMflips"
	command polish -label "Polish" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::polish -h"] \
	       -command "CGWMpolish"
	separator sep1	
	command config -label "Config" -underline 0 \
	       -helpstr "Configure local search." \
	       -command "CGWMconfigsearch"		
    }

#
# le menu de config des cartes
#
.cgmw menubar add menubutton maps -text "Maps" -underline 0 -padx 8 -pady 2 \
    -menu {options -tearoff no
	command info -label "Info" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::heaprint -h"] \
	       -command "CGWMheaprint"
	command detail -label "Detail" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::heaprintd -h"] \
	       -command "CGWMheaprintd"
	command detailed -label "Detailed" -underline 0 \
	       -helpstr [[.cgmw childsite].st EvalNoTyped "::CG::heaprinto -h"] \
	       -command "CGWMheaprinto"
	separator sep1
	command graphical -label "Graphical" -underline 0 \
	       -helpstr "Draw maps." \
	       -command "CGWMgraphical"	     
	command paretoplot -label "Pareto Plot" -underline 0 \
	       -helpstr "Plot the pareto frontier." \
	       -command "CGWMparetoplot"	  
	separator sep2	
	command dsbplambda -label "Comparative Mapping" -underline 0 \
	       -helpstr "Select the comparative mapping criterion." \
	       -command "CGWMdsbplambda"	  
	separator sep3	
	command config -label "Config" -underline 0 \
	       -helpstr "Configure the maps features." \
	       -command "CGWMconfigmaps"		
  }

#
# un peu d'aide
#
.cgmw menubar add menubutton help -text "Help" -underline 0 -padx 8 -pady 2 \
    -menu {options -tearoff no
	command onwindow -label "About Cartagene..." -underline 0 \
	       -helpstr "About Cartagene..." \
	       -command {puts "Cartagene = Apha!!!"}	
	command index -label "Index" -underline 0 \
	       -helpstr "View the help index" \
	       -command "CGWMhelpindex"	
    }

#
# La barre de recherche
#
.cgmw toolbar add label filler1 \
    -image [image create photo -file [file join [file dirname [info script]] $imagedir petitlogo.gif]] \
    -helpstr "Unit� de Biom�trie et Intelligence Artificielle de TOULOUSE" \
    -width 128 \
    -relief raised \
    -borderwidth 2

.cgmw toolbar add button default\
    -text "Defalgo" \
    -helpstr [set hs "Run a default set of algorithm"] \
    -balloonstr $hs \
    -command "CGWMdefalgo"

.cgmw toolbar add button sem\
    -text "SEM" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::sem -h"]] \
    -balloonstr $hs \
    -command "CGWMsem"

.cgmw toolbar add button nicemapd\
    -text "Nicemapd" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::nicemapd -h"]] \
    -balloonstr $hs \
    -command "CGWMnicemapd"

.cgmw toolbar add button nicemapl\
    -text "Nicemapl" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::nicemapl -h"]] \
    -balloonstr $hs \
    -command "CGWMnicemapl"

.cgmw toolbar add button build\
    -text "Build" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::build -h"]] \
    -balloonstr $hs \
    -command "CGWMbuild"

.cgmw toolbar add button buildfw\
    -text "BuildFW" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::buildfw -h"]] \
    -balloonstr $hs \
    -command "CGWMbuildfw"

.cgmw toolbar add button paretolkh\
    -text "C.Frontier" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::paretolkh -h"]] \
    -balloonstr $hs \
    -command "CGWMparetolkh"

.cgmw toolbar add button annealing\
    -text "Annealing" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::annealing -h"]] \
    -balloonstr $hs \
    -command "CGWMannealing"

.cgmw toolbar add button greedy\
    -text "Taboo" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::greedy -h"]] \
    -balloonstr $hs \
    -command "CGWMgreedy"

.cgmw toolbar add button algogen\
    -text "Genetic" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::algogen -h"]] \
    -balloonstr $hs \
    -command "CGWMalgogen"

.cgmw toolbar add button flips\
    -text "Flips" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::flips -h"]] \
    -balloonstr $hs \
    -command "CGWMflips"

.cgmw toolbar add button polish\
    -text "Polish" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::polish -h"]] \
    -balloonstr $hs \
    -command "CGWMpolish"

.cgmw toolbar add frame filler2 -width 20 -relief raised -borderwidth 2

.cgmw toolbar add frame filler3 -relief raised -borderwidth 2

#
# La barre des donn�es
#
.cgmw mousebar add button load \
    -text "Load" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::dsload -h"]] \
    -balloonstr $hs \
    -command "CGWMdsload"

.cgmw mousebar add button export \
    -text "Export" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "cgexport -h"]] \
    -balloonstr $hs \
    -command "CGWMcgexport"

.cgmw mousebar add button mergen\
    -text "Mergen" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::dsmergen -h"]] \
    -balloonstr $hs \
    -command "CGWMdsmergen"

.cgmw mousebar add button mergor\
    -text "Mergor" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::dsmergor -h"]] \
    -balloonstr $hs \
    -command "CGWMdsmergor"

.cgmw mousebar add button info\
    -text "Info" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::dsinfo -h"]] \
    -balloonstr $hs \
    -command "CGWMdsinfo"

.cgmw mousebar add button graphical\
    -text "Graphical" \
    -helpstr [set hs "Provide Graphical view of the maps stored into the heap"] \
    -balloonstr $hs \
    -command "CGWMgraphical"

.cgmw mousebar add button paretoplot\
    -text "Pareto Plot" \
    -helpstr [set hs "Provide a Preto Frontier Plot."] \
    -balloonstr $hs \
    -command "CGWMparetoplot"

.cgmw mousebar add button detail\
    -text "Detail" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::heaprintd -h"]] \
    -balloonstr $hs \
    -command "CGWMheaprintd"

.cgmw mousebar add frame filler1 -height 20 -relief raised -borderwidth 2

.cgmw mousebar add cgconfigquiet quiet\
    -command "CGWMquietset" \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::quietset -h"]] \
    -balloonstr $hs

.cgmw mousebar add cgconfigverbose verbose \
    -helpstr [set hs [[.cgmw childsite].st EvalNoTyped "::CG::verbset -h"]] \
    -balloonstr $hs \
    -command "CGWMverboseset" \
    -labeltext "Verbose" \
    -orient vertical \
    -labelpos n

.cgmw mousebar add label filler2 \
    -image [image create photo -file [file join [file dirname [info script]] $imagedir inra-logo.gif]] \
    -helpstr "Institut National de la Recherche Agronomique" \
    -relief raised \
    -borderwidth 2

.cgmw mousebar add button stop \
    -text "Stop" \
    -helpstr [set hs "To Stop the current command"] \
    -balloonstr $hs \
    -command cgstop

.cgmw mousebar add button help \
    -image [image create photo -file [file join [file dirname [info script]] $imagedir help.gif]] \
    -helpstr "Obtain help for this window" \
    -command "CGWMhelpindex"

.cgmw mousebar add button exit \
    -image [image create photo -file [file join [file dirname [info script]] $imagedir exit.gif]] \
    -helpstr "Exit this application" \
    -command "CGWMExit"

.cgmw mousebar add frame filler3 -height 5

#
# Change the packing of the last fillers in the tool and mouse bar
# so that it expands across and down the rest of the mainwindow.
#
pack [.cgmw toolbar component filler3] -expand yes -fill both
pack [.cgmw mousebar component filler2] -expand yes -fill both
pack [.cgmw childsite].st -fill both -expand yes
#
# Activation de la fen�tre principale..
#
.cgmw activate

# initialisation de CartaGene en fonction des ressources
CGWMquietset

# initialisation de CartaGene en fonction des ressources
CGWMverboseset

CGWMresizestorage

.cghi insert items end Load
.cghi insert items end Mergen
.cghi insert items end Mergor
.cghi insert items end Info
.cghi insert items end Output
.cghi insert items end Group
.cghi insert items end Group_selection
.cghi insert items end Group_config
.cghi insert items end Select
.cghi insert items end Loci_merge
.cghi insert items end Loci_info
.cghi insert items end Lod2p
.cghi insert items end Dist2p
.cghi insert items end Fr2p
.cghi insert items end Defalgo
.cghi insert items end SEM
.cghi insert items end Nicemapl
.cghi insert items end Nicemapd
.cghi insert items end Build
.cghi insert items end BuildFW
.cghi insert items end "Create Frontier"
.cghi insert items end Annealing
.cghi insert items end Taboo
.cghi insert items end "Genetic_A"
.cghi insert items end Flips
.cghi insert items end Polish
.cghi insert items end Search_config
.cghi insert items end Maps_info
.cghi insert items end Maps_detail
.cghi insert items end Maps_detailed
.cghi insert items end Graphical
.cghi insert items end "Pareto_Plot"
.cghi insert items end Maps_config
.cghi insert items end Quiet
.cghi insert items end Verbose
.cghi insert items end "Session Reset"
.cghi insert items end "Session Save"
.cghi insert items end "Session Restore"
.cghi insert items end "Map Export"
.cghi insert items end Stop

