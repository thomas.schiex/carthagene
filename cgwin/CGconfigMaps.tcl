#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigMaps.tcl,v 1.5 2005-01-14 17:24:17 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres du menu maps
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#

#
# Options usuelles.
#
itk::usual CGconfigMaps {
    keep -background -cursor -foreground
    # -modality
}

class CGconfigMaps {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -storage storage Storage 16
    itk_option define -visuala visuala Visuala 5
    itk_option define -unit unit Unit "h"
    itk_option define -coef coef Coef 3.0
    
    constructor {args} {}
    public method get {arg}
    public method cget {arg}
    public method Valider {}
    public method Abandonner {}
    
}

proc cgconfigmaps {pathName args} {
    uplevel CGconfigMaps $pathName $args
}

body CGconfigMaps::constructor {args} {
    
    itk_component add commentaires {
        label $itk_interior.commentaires \
	    -text "Specific parameters to store and visualize maps."
    }
    pack $itk_component(commentaires) -padx 4 -pady 5
    
    itk_component add storage {
	iwidgets::spinint $itk_interior.storage \
	    -range {1 128} -width 3\
	    -labeltext "Maximum size of the container of maps"
    }
    pack $itk_component(storage) -padx 10 -pady 10 -anchor e
    
    itk_component add visuala {
	iwidgets::spinint $itk_interior.visuala \
	    -range {1 128} -width 3\
	    -labeltext "Amount of maps to display, or print"
    }
    pack $itk_component(visuala) -padx 10 -pady 10 -anchor e
    
    itk_component add unit {
	iwidgets::radiobox $itk_interior.unit \
	    -labeltext Unit \
	    -labelpos n
    } {
	usual
    }

    $itk_component(unit) add very\
	-text "Haldane" \
	-indicatoron 1
  
    $itk_component(unit) add fairly \
	-text "Kosambi" \
	-indicatoron 1

    $itk_component(unit) buttonconfigure 0 -value "h"
    $itk_component(unit) buttonconfigure 1 -value "k"

    pack $itk_component(unit)

    itk_component add coef {
        spinreal $itk_interior.coef \
	    -range {0.0 10.0} -width 4 \
	    -labeltext "Pareto plot relative coefficient value"
    }
    
    pack $itk_component(coef) -padx 10 -pady 10 -anchor e

    
    eval itk_initialize $args
}

configbody CGconfigMaps::storage {
    
    set nbr $itk_option(-storage)
    if {$nbr < 1 || $nbr > 128} {
        error "wrong value \"storage\": should be between 1 and 128"
    } else {
        $itk_component(storage) delete 0 end
        $itk_component(storage) insert 0 $nbr
    }
}

configbody CGconfigMaps::visuala {
    
    set nbr $itk_option(-visuala)
    if {$nbr < 1 || $nbr > 128} {
        error "wrong value \"visuala\": should be between 1 and 128"
    } else {
        $itk_component(visuala) delete 0 end
        $itk_component(visuala) insert 0 $nbr
    }
}

configbody CGconfigMaps::unit {
    
    set nbr $itk_option(-unit)
    if {[string compare $nbr "h"] == -1 && [string compare $nbr "k"] == -1}   {
        error "wrong value \"unit\": should be kor h"
    } elseif {[string compare $nbr "h"] == 0} {
	$itk_component(unit) select 0
    } else {
	$itk_component(unit) select 1	
    }
}

configbody CGconfigMaps::coef {
    
    set nbr $itk_option(-coef)
    if {$nbr < 0.0 || $nbr > 10.0} {
        error "wrong value \"coef\": should be between 0.0 and 10.0"
    } else {
        $itk_component(coef) delete 0 end
        $itk_component(coef) insert 0 $nbr
    }
}

#
#
#
body CGconfigMaps::Valider {} {
    
    if {$itk_option(-storage) != [$itk_component(storage) get]} {
	set itk_option(-storage) [$itk_component(storage) get]    
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-visuala) != [$itk_component(visuala) get]} {
	set itk_option(-visuala) [$itk_component(visuala) get]    
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-unit) != [$itk_component(unit) get]} {
	set itk_option(-unit) [$itk_component(unit) get]
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-coef) != [$itk_component(coef) get]} {
	set itk_option(-coef) [$itk_component(coef) get]
	uplevel #0 set CGWhaschanged 1
    }

}

body CGconfigMaps::Abandonner {} {

    configure -storage $itk_option(-storage)
    configure -visuala $itk_option(-visuala)
    configure -unit $itk_option(-unit)
    configure -coef $itk_option(-coef)
}

body CGconfigMaps::cget {arg} {

    return $itk_option($arg)

}

body CGconfigMaps::get {arg} {
    
    switch -- $arg {
        "-storage" { return [$itk_component(storage) get]}
        "-visuala" { return [$itk_component(visuala) get]}
	"-unit" { return [$itk_component(unit) get]}
	"-coef" { return [$itk_component(coef) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}
