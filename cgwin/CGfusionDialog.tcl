#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGfusionDialog.tcl,v 1.3 2002-11-07 16:59:02 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue pour la fusion
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# Usual options.
#
itk::usual CGfusionDialog {
    keep -activebackground -activerelief -background -borderwidth -cursor \
	 -elementborderwidth -foreground -highlightcolor -highlightthickness \
	 -insertbackground -insertborderwidth -insertofftime -insertontime \
	 -insertwidth -jump -labelfont -modality -selectbackground \
	 -selectborderwidth -selectforeground -textbackground -textfont \
	 -troughcolor
}

# ------------------------------------------------------------------
#                           SELECTIONDIALOG
# ------------------------------------------------------------------
class CGfusionDialog {
    inherit iwidgets::Dialog

    itk_option define -selectlabg selectlabg Text "Merging"
    itk_option define -selectlabd selectlabd Text "With"

    constructor {args} {}

    public method childsite {}
    public method getg {}
    public method getd {}
    public method curselection {}
    public method clear {component}
    public method insert {component index args}
    public method delete {first {last {}}}
    public method size {}
    public method scan {option args}
    public method nearest {y}
    public method index {index}
    public method selection {option args}
    public method selectitem {}
}
    
option add *fusionDialog.title "CartaGene Merging Dialog" widgetDefault
option add *fusionDialog.master "." widgetDefault

#
# Provide a lowercased access method for the Selectiondialog class.
# 

proc cgfusiondialog {pathName args} {
    uplevel CGfusionDialog $pathName $args
}

# ------------------------------------------------------------------
#                        CONSTRUCTEUR
# ------------------------------------------------------------------
body CGfusionDialog::constructor {args} {
    #
    # Set the borderwidth to zero.
    #
    component hull configure -borderwidth 0
    
    # 
    # Instantiation des deux, 1 pou l'instatnt
    #
    itk_component add selectionbox {
	cgfusionbox $itk_interior.selectionbox \
		-dblclickcommand [code $this invoke]
    } {
	usual

	keep -childsitepos -exportselection -itemscommand -itemslabel \
	    -itemson -selectionon -selectioncommand \
	    -selectlabg -selectlabd
    }
    configure -itemscommand [code $this selectitem]
    
    pack $itk_component(selectionbox) -fill both -expand yes
    set itk_interior [$itk_component(selectionbox) childsite]
    
    hide Help
    hide Apply

    eval itk_initialize $args
}   

# ------------------------------------------------------------------
#                            METHODS
# ------------------------------------------------------------------

# ------------------------------------------------------------------
# METHOD: childsite
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::childsite {} {
    return [$itk_component(selectionbox) childsite]
}

# ------------------------------------------------------------------
# METHOD: get
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::getg {} {
    return [$itk_component(selectionbox) getg]
}

# ------------------------------------------------------------------
# METHOD: get
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::getd {} {
    return [$itk_component(selectionbox) getd]
}
# ------------------------------------------------------------------
# METHOD: curselection
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::curselection {} {
    return [$itk_component(selectionbox) curselection]
}

# ------------------------------------------------------------------
# METHOD: clear component
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::clear {component} {
    $itk_component(selectionbox) clear $component

    return
}

# ------------------------------------------------------------------
# METHOD: insert component index args
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::insert {component index args} {
    eval $itk_component(selectionbox) insert $component $index $args

    return
}

# ------------------------------------------------------------------
# METHOD: delete first ?last?
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::delete {first {last {}}} {
    $itk_component(selectionbox) delete $first $last

    return
}

# ------------------------------------------------------------------
# METHOD: size
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::size {} {
    return [$itk_component(selectionbox) size]
}

# ------------------------------------------------------------------
# METHOD: scan option args
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::scan {option args} {
    return [eval $itk_component(selectionbox) scan $option $args]
}

# ------------------------------------------------------------------
# METHOD: nearest y
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::nearest {y} {
    return [$itk_component(selectionbox) nearest $y]
}

# ------------------------------------------------------------------
# METHOD: index index
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::index {index} {
    return [$itk_component(selectionbox) index $index]
}

# ------------------------------------------------------------------
# METHOD: selection option args
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGfusionDialog::selection {option args} {
    eval $itk_component(selectionbox) selection $option $args
}

# ------------------------------------------------------------------
# METHOD: selectitem
#
# Set the default button to ok and select the item.
# ------------------------------------------------------------------
body CGfusionDialog::selectitem {} {
    default OK
    $itk_component(selectionbox) selectitem
}

