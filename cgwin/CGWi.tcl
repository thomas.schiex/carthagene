#
# Toutes les fonctionalit�s sp�cifiques 
#
#
# Saving and resing a session
#


proc CGWMsaves {} {
    
    .cgmw configure -cursor watch
    
    if [.cgfb activate] {
	set fichier [.cgfb get]
	if {[file exist $fichier] == 0 || [.cgfq activate]} {
	    if [catch {open $fichier w} fid] {
		puts stderr "Cannot open $arg : $fid"
	    } else {		    
		[.cgmw childsite].st EvalNoTyped "cgsavegui $fid"
		close $fid	    
	    }
	}
    }
    .cgmw configure -cursor ""
}

proc CGWMrestores {} {

  
    
    .cgmw configure -cursor watch
    
    if [.cgfb activate] {
	set fichier [.cgfb get]
	[.cgmw childsite].st EvalNoTyped "source $fichier"
    }
    
    .cgmw configure -cursor ""
}

proc CGWMdsload {} {

  
    
    .cgmw configure -cursor watch
    
    if [.cgfb activate] {
	set fichier [.cgfb get]
	[.cgmw childsite].st EvalNoTyped "::CG::dsload {$fichier}"
    }
    
    .cgmw configure -cursor ""
}

proc CGWMcgexport {} {

    .cgmw configure -cursor watch

#    .cged clear selection

#    .cged clear items

    if [.cged activate] {

	set expfn [.cged getfn]
	set expfnxml $expfn.xml
	set epmn  [.cged getmn ]
	set expcn [.cged getcn ]

	if {[file isdirectory $expfn]} {
	    puts stderr "Error : $expfn is a directory, a regular file is expected."
	} elseif {[file exist $expfn] == 1} {
	    if {[.cgfq2 activate]} {
		
		set expfn1 $expfn~
		file copy -force $expfn $expfn1
		
		set expfn1xml $expfnxml~
		file rename -force $expfnxml $expfn1xml

		if [catch {open $expfn {WRONLY CREAT APPEND}} fid] {
		    puts stderr "Error : Cannot open $expfn."
		    puts stderr "Export aborted!"
		} elseif [catch {open $expfn1 {RDONLY}} fid1] {
		    puts stderr "Error : Cannot open $expfn1."
		    puts stderr "Export aborted!"
		} elseif [catch {open $expfn1xml {RDONLY}} fidxml1] {
		    puts stderr "Error : Cannot open $expfn1xml."
		    puts stderr "Export aborted!"
		} elseif [catch {open $expfnxml {WRONLY CREAT}} fidxml] {
		    puts stderr "Error : Cannot open $expfnxml."
		    puts stderr "Export aborted!"
		} else {
		    [.cgmw childsite].st EvalNoTyped "cgexportgui $fid1 $fid $fidxml1 $fidxml \"$epmn\" \"$expcn\""
		    close $fid1
		    close $fid
		    close $fidxml1
		    close $fidxml
		}
	    } else {
		puts stderr "Export aborted!"
	    }
	} elseif {[file exist $expfn] == 0} {
	    
	    if [catch {open $expfn {WRONLY CREAT APPEND}} fid] {
		puts stderr "Error : Cannot open $expfn."
		puts stderr "Export aborted!"
	    } elseif [catch {open $expfnxml {WRONLY CREAT}} fidxml] {
		puts stderr "Error : Cannot open $expfnxml."
		puts stderr "Export aborted!"
	    } else {
		[.cgmw childsite].st EvalNoTyped "cgexportgui $fid $fidxml \"$epmn\" \"$expcn\""
		close $fid
		close $fidxml
	    }		
	}
    }
    
    .cgmw configure -cursor ""
}
    
proc CGWMdsmergen {} {

    .cgmw configure -cursor watch

    .cgfd clear selection

    .cgfd clear items

    foreach ds [[.cgmw childsite].st EvalNoTyped "::CG::dsget"] {
	.cgfd insert items end $ds
    } 

    if [.cgfd activate] {
	set dsidlg [lindex [split [.cgfd getg]] 0]
	set dsidld [lindex [split [.cgfd getd]] 0]
	[.cgmw childsite].st EvalNoTyped "::CG::dsmergen $dsidlg $dsidld"
    }

    .cgmw configure -cursor ""
}

proc CGWMdsmergor {} {

    .cgmw configure -cursor watch

    .cgfd clear selection

    .cgfd clear items
    
    foreach ds [[.cgmw childsite].st EvalNoTyped "::CG::dsget"] {
	.cgfd insert items end $ds
    } 
    if [.cgfd activate] {
	set dsidlg [lindex [split [.cgfd getg]] 0]
	set dsidld [lindex [split [.cgfd getd]] 0]
	[.cgmw childsite].st EvalNoTyped "::CG::dsmergor $dsidlg $dsidld"
    }

    .cgmw configure -cursor ""
}

proc CGWMdsinfo {} {

	[.cgmw childsite].st EvalNoTyped "::CG::dsinfo"
}

proc CGWMcgrestart {} {

	[.cgmw childsite].st EvalNoTyped "::CG::cgrestart"
}

proc CGWMconfigsearch {} {
    
    global CGWhaschanged

    .cgmw configure -cursor watch
    
    if [.cgcs activate] {
	set CGWhaschanged 1
    }

    .cgmw configure -cursor ""
}

proc CGWMconfigmaps {} {
    
    global CGWhaschanged

    .cgmw configure -cursor watch
    
    if [.cgcm activate] {
	CGWMresizestorage
    }

    .cgmw configure -cursor ""
}

proc CGWMdefalgo {} {
    
    # pour windows
    if { $::tcl_platform(platform) == "windows" } {
	set fidef "~/cgwdefalgo"
    } else {
	set fidef "~/.cgwdefalgo"
    }

    if [catch {open $fidef r } defid] {
	[.cgmw childsite].st PutsAlias stderr "Cannot open  ~/.cgwdefalgo"
    } else {
	foreach line [split [read $defid] \n] {
	    
	    [.cgmw childsite].st EvalNoTyped $line
	    
	}
	
	close $defid
    }
    
}

proc CGWMsem {} {

	[.cgmw childsite].st EvalNoTyped "::CG::sem"
}

proc CGWMnicemapd {} {

	[.cgmw childsite].st EvalNoTyped "::CG::nicemapd"
}

proc CGWMnicemapl {} {

	[.cgmw childsite].st EvalNoTyped "::CG::nicemapl"
}

proc CGWMannealing {} {

	[.cgmw childsite].st EvalNoTyped "::CG::annealing [.cgcs get -tries] [.cgcs get -tinit] [.cgcs get -tfinal] [.cgcs get -cooling]"
}

proc CGWMbuild {} {

	[.cgmw childsite].st EvalNoTyped "::CG::build [.cgcs get -nbmap]"
}

proc CGWMbuildfw {} {


    [.cgmw childsite].st EvalNoTyped "::CG::buildfw [.cgcs get -keepthres] [.cgcs get -addthres] {} 0"
}

proc CGWMparetolkh {} {


    [.cgmw childsite].st EvalNoTyped "::CG::paretolkh [.cgcs get -res] 1 1"
}

proc CGWMgreedy {} {

    [.cgmw childsite].st EvalNoTyped "::CG::greedy [.cgcs get -nbrun] [.cgcs get -nbiter] [.cgcs get -taboolenmin] [.cgcs get -taboolenmax] [.cgcs get -anytime]"
}

proc CGWMalgogen {} {

    [.cgmw childsite].st EvalNoTyped ":::CG::algogen [.cgcs get -nbgens] [.cgcs get -nbele] [.cgcs get -selnbr] [.cgcs get -pcross] [.cgcs get -pmut] [.cgcs get -evolfitness]"
}

proc CGWMflips {} {

    [.cgmw childsite].st EvalNoTyped ":::CG::flips [.cgcs get -wins] [.cgcs get -fthres] [.cgcs get -fiter]"
    
}

proc CGWMpolish {} {

 
    [.cgmw childsite].st EvalNoTyped ":::CG::polish"
    
 
}

#utilitaire pour l'apply de config

proc CGWMsearch { met } {
    
    switch -- $met {
	Annealing {CGWMannealing}
	Greedy {CGWMgreedy}
	"Genetic_A" {CGWMalgogen}
	Build {CGWMbuild}
	Buildfw {CGWMbuildfw}
	Flips {CGWMflips}
	"C.Frontier" {CGWMparetolkh}
	
    }
}

proc CGWMExit {} {
    
    global CGWhaschanged
    
    if $CGWhaschanged {
     	if [.cgcq activate] {
     	    CGWMconfigSave 
     	}
    }

    delete object .cgfb
 
    exit
}

proc CGWMconfigroup {} {
    
    global CGWhaschanged CGWnbgroup
    
    .cgmw configure -cursor watch

    #pour memo
    
    set olodthres [.cgcg get -lodthres]
    set odisthres [.cgcg get -disthres]
    
    if [.cgcg activate] {
	
	if { $olodthres != [.cgcg get -lodthres] ||
	     $odisthres != [.cgcg get -disthres] } {
	    set CGWhaschanged 1
	}
    }
    
#     set CGWnbgroup [[.cgmw childsite].st EvalNoTyped "::CG::group [.cgcg get -lodthres] [.cgcg get -disthres]"]
    
    .cgmw configure -cursor ""
}


proc CGWMrungroup {} {
    
    global CGWnbgroup

    set CGWnbgroup [[.cgmw childsite].st EvalNoTyped "::CG::group [.cgcg get -disthres] [.cgcg get -lodthres]"]
    
}

proc CGWMmrkinfo {} {
    
    [.cgmw childsite].st EvalNoTyped "::CG::mrkinfo"
    
}

proc CGWMmrklod2p {} {
    
    [.cgmw childsite].st EvalNoTyped "::CG::mrklod2p"
    
}

proc CGWMmrkdist2p {} {
    
    [.cgmw childsite].st EvalNoTyped "::CG::mrkdist2p [.cgcm get -unit]"
    
}

proc CGWMmrkfr2p {} {
    
    [.cgmw childsite].st EvalNoTyped "::CG::mrkfr2p"
    
}

proc CGWMselectgroup {} {
    
    global CGWnbgroup

    .cgmw configure -cursor watch

    .cgsg clear items
    .cgsg clear selection

    for {set i 1} {$i <= $CGWnbgroup} {incr i}  {
	
	set loli [[.cgmw childsite].st EvalNoTyped "::CG::groupget $i"]
	
	set sloli [format "%-4s : " $i]
	
	foreach locid $loli {
	    
	    append sloli [[.cgmw childsite].st EvalNoTyped "::CG::mrkname $locid"] " "
	    
	}
	
	.cgsg insert items end $sloli
    }
    
    if [.cgsg activate] {

	set lres [.cgsg get]
	if { $lres != {} } {

	    set lress [split $lres]
	    set i2p [lsearch $lress ":"]
	    set lress [lreplace $lress end end]
	    set lress [lreplace $lress 0 $i2p]
	    
	    foreach locid $lress {
		
		lappend nloli  [[.cgmw childsite].st EvalNoTyped "::CG::mrkid $locid"]
	    }

	    [.cgmw childsite].st EvalNoTyped "::CG::mrkselset {$nloli}"
	}
    }

    .cgmw configure -cursor ""
}
proc CGWMmrkmerge {} {

    .cgmm configure -cursor watch

    .cgmm clear selection

    .cgmm clear items

    set selid [[.cgmw childsite].st EvalNoTyped "::CG::mrkselget"]
    
    set selname {}

    foreach mid $selid {
	.cgmm insert items end $mid
    }

    if [.cgmm activate] {
	set mrkidlg [lindex [split [.cgmm getg]] 0]
	set mrkidld [lindex [split [.cgmm getd]] 0]
	[.cgmw childsite].st EvalNoTyped "::CG::mrkmerge $mrkidlg $mrkidld"
    }

    .cgmw configure -cursor ""

}



#
# Sauvegarde de toutes les options
#

proc CGWMconfigSave {} {

    # pour windows
    if { $::tcl_platform(platform) == "windows" } {
	set fidopt  [open ~/cgwopt w]
    } else {
	set fidopt  [open ~/.cgwopt w]
    }

    puts $fidopt "*annealing.tries: [.cgcs get -tries]"
    puts $fidopt "*annealing.tinit: [.cgcs get -tinit]"
    puts $fidopt "*annealing.tfinal: [.cgcs get -tfinal]"
    puts $fidopt "*annealing.cooling: [.cgcs get -cooling]"
    puts $fidopt "*build.nbmap: [.cgcs get -nbmap]"
    puts $fidopt "*buildfw.keepthres: [.cgcs get -keepthres]"
    puts $fidopt "*buildfw.addthres: [.cgcs get -addthres]"
    puts $fidopt "*paretolkh.res: [.cgcs get -res]"
 #   puts $fidopt "*paretolkh.nbrunp: [.cgcs get -nbrunp]"
 #   puts $fidopt "*paretolkh.colm: [.cgcs get -colm]"
    puts $fidopt "*greedy.nbiter: [.cgcs get -nbiter]"
    puts $fidopt "*greedy.nbrun: [.cgcs get -nbrun]"
    puts $fidopt "*greedy.taboolenmin: [.cgcs get -taboolenmin]"
    puts $fidopt "*greedy.taboolenmax: [.cgcs get -taboolenmax]"
    puts $fidopt "*greedy.anytime: [.cgcs get -anytime]"
    puts $fidopt "*algogen.nbgens: [.cgcs get -nbgens]"
    puts $fidopt "*algogen.nbele: [.cgcs get -nbele]"
    puts $fidopt "*algogen.selnbr: [.cgcs get -selnbr]"
    puts $fidopt "*algogen.pcross: [.cgcs get -pcross]"
    puts $fidopt "*algogen.pmut: [.cgcs get -pmut]"
    puts $fidopt "*algogen.evolfitness: [.cgcs get -evolfitness]"
    puts $fidopt "*flips.wins: [.cgcs get -wins]"
    puts $fidopt "*flips.fthres: [.cgcs get -fthres]"
    puts $fidopt "*flips.fiter: [.cgcs get -fiter]"
    puts $fidopt "*cg.lodthres: [.cgcg get -lodthres]"
    puts $fidopt "*cg.disthres: [.cgcg get -disthres]"
    puts $fidopt "*quiet.quiet: [.cgmw.shellchildsite.mousebar.quiet get -quiet]"
    puts $fidopt "*verbose.verbose: [.cgmw.shellchildsite.mousebar.verbose get]"
    puts $fidopt "*cm.unit: [.cgcm get -unit]"
    puts $fidopt "*cm.visuala: [.cgcm get -visuala]"
    puts $fidopt "*cm.storage: [.cgcm get -storage]"
     puts $fidopt "*cm.coef: [.cgcm get -coef]"

    puts $fidopt "*optAffichage.mode: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -mode]"
    puts $fidopt "*optAffichage.repreCarte: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreCarte]"
    puts $fidopt "*optAffichage.repreMrq: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreMrq]"
    puts $fidopt "*optAffichage.zoomMrq: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -zoomMrq]" 
    puts $fidopt "*optAffichage.repreVraisem: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreVraisem]"
    puts $fidopt "*optAffichage.repreEtiq: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreEtiq]"
    puts $fidopt "*optAffichage.zoomMap: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -zoomMap]"

    puts $fidopt "*colorDialog.logtxt: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -logtxt]"
    puts $fidopt "*colorDialog.logfle: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -logfle]"
    puts $fidopt "*colorDialog.carte: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -carte]"
    puts $fidopt "*colorDialog.mrqtxt: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqtxt]"
    puts $fidopt "*colorDialog.mrqtxtsel: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqtxtsel]"
    puts $fidopt "*colorDialog.mrqlineon: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqlineon]"
    puts $fidopt "*colorDialog.mrqlineoff: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqlineoff]"
    puts $fidopt "*colorDialog.infoline: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infoline]"
    puts $fidopt "*colorDialog.infotxt: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infotxt]"
    puts $fidopt "*colorDialog.infobox: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infobox]"
    #quelques options non configurables interactivement.

    puts $fidopt "*colorDialog.infofont: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infofont]"
    puts $fidopt "*colorDialog.mrqfont: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqfont]"
    puts $fidopt "*colorDialog.logfont: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -logfont]"

    puts $fidopt "*tv.locfont: [.cggr.shellchildsite.tv get -locfont]"

    close $fidopt
}


proc CGWMmrkselset {} {

    .cgmw configure -cursor watch

    # � blanc

    .cgsl setlhs ""
    .cgsl setrhs ""

    # composition de la liste de la s�lection courante

    set selid [[.cgmw childsite].st EvalNoTyped "::CG::mrkselget"]
    
    set selname {}

    foreach mid $selid {
	lappend selname [[.cgmw childsite].st EvalNoTyped "::CG::mrkname $mid"]
    }

    # composition de la liste des possibles

    set maxm [[.cgmw childsite].st EvalNoTyped "::CG::mrklast"]

    set allname {}

    for {set i 1} {$i <= $maxm } {incr i}  {
	lappend allname [[.cgmw childsite].st EvalNoTyped "::CG::mrkname $i"]
    }

    .cgsl setlhs $allname
    .cgsl setrhs $selname
    
    if [.cgsl activate] {
	
	set newsel [.cgsl get]
	
	if { $newsel != {} } {
	    
	    foreach mname $newsel {
		lappend newselid [[.cgmw childsite].st EvalNoTyped "::CG::mrkid $mname"]
	    }
	    
	    [.cgmw childsite].st EvalNoTyped "::CG::mrkselset {$newselid}"
	    
	}
	
    }

    .cgmw configure -cursor ""
}

proc CGWMsavout {} {
    
    .cgmw configure -cursor watch
    
    if [.cgfb activate] {
	set fichier [.cgfb get]
	if {$fichier != {} } {
	    [.cgmw childsite].st export $fichier
	}
    }
    
    .cgmw configure -cursor ""
}

proc CGWMquietset {} {

    global CGWhaschanged
    set bool [.cgmw.shellchildsite.mousebar.quiet get -quiet]
    set orig [.cgmw.shellchildsite.mousebar.quiet cget -quiet]
    if { $orig != $bool}  {
	set CGWhaschanged 1
    }
    [.cgmw childsite].st EvalNoTyped "::CG::quietset $bool"
}



proc CGWMverboseset {} {

    global CGWhaschanged
  
    set flag [.cgmw.shellchildsite.mousebar.verbose get]
    set orig [.cgmw.shellchildsite.mousebar.verbose cget -verbose]
    if { $orig != $flag}  {
	 set CGWhaschanged 1
    }
    [.cgmw childsite].st EvalNoTyped "::CG::verbset $flag"
}
 

proc CGWMresizestorage {} {
    
    set storage [.cgcm get -storage]
    [.cgmw childsite].st EvalNoTyped "::CG::heapsize $storage"
} 



proc CGWMgraphical {} {

    .cggr activate
    
    set unit [.cgcm get -unit]
    
    set amount [.cgcm get -visuala]

    set cartbrut [[.cgmw childsite].st EvalNoTyped "::CG::heapget $unit $amount"]

    set lds [[.cgmw childsite].st EvalNoTyped "::CG::dsget"]

    if {$cartbrut != {} } {

	.cggr tasser $unit $amount $cartbrut $lds

    }
}

proc CGWMparetoplot {} {

    .cgpp activate

    set coef [.cgcm get -coef]

    set li [[.cgmw childsite].st EvalNoTyped "::CG::paretoinfog $coef"]

    if {$li != {} } {

	.cgpp plot $li

    }
}

proc CGWMdsbplambda {} {

    set coef [.cgcm get -coef]
    set lds [[.cgmw childsite].st EvalNoTyped "::CG::dsget"]
    set cds [lindex $lds end]
    set cdsid [lindex $cds 0]

    [.cgmw childsite].st EvalNoTyped "::CG::dsbplambda $cdsid 1 $coef"
}

proc CGWMheaprint {} {
    
    [.cgmw childsite].st EvalNoTyped "::CG::heaprint"
} 

proc CGWMheaprintd {} {
    
    [.cgmw childsite].st EvalNoTyped "::CG::heaprintd"
} 

proc CGWMheaprinto {} {


    [.cgmw childsite].st EvalNoTyped "::CG::heaprinto 0 0 0"
}


proc CGWMhelpindex {} {

    .cgmw configure -cursor watch

    if [.cghi activate] {
	set topic [.cghi get]
	switch -- $topic {
	    Load {[.cgmw childsite].st EvalNoTyped "::CG::dsload -H"}
	    Mergen {[.cgmw childsite].st EvalNoTyped "::CG::dsmergen -H"}
	    Mergor {[.cgmw childsite].st EvalNoTyped "::CG::dsmergor -H"}
	    Info {[.cgmw childsite].st EvalNoTyped "::CG::dsinfo -H"} 
	    Output {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Save the textual screen to a file."}
	    Defalgo {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Enable to run a default selection of algorithm. By editing the file ~/.cgwdefalgo(unix) or ~/defalgo(windows), you can specify your own sequence of command"}
	    SEM {[.cgmw childsite].st EvalNoTyped "::CG::sem -H"}
	    Annealing {[.cgmw childsite].st EvalNoTyped "::CG::annealing -H"}
	    Graphical {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Open a graphical screen to draw maps. To get the menu, press the right mouse button on the background. Click on the loci names to change the color of a link. Click on a map to display labels(movable)."}
	    "Pareto_Plot" {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Open a graphical screen to draw the Pareto frontier frontier. To get the menu, press the right mouse button on the backgroung. Click on the points of the plot to get the corresponding map displayed on graphical screen."}
	    Nicemapl {[.cgmw childsite].st EvalNoTyped "::CG::nicemapl -H"}
	    Nicemapd {[.cgmw childsite].st EvalNoTyped "::CG::nicemapd -H"}
	    Build {[.cgmw childsite].st EvalNoTyped "::CG::build -H"}
	    BuildFW {[.cgmw childsite].st EvalNoTyped "::CG::buildfw -H"}
	    "Create Frontier" {[.cgmw childsite].st EvalNoTyped "::CG::paretolkh -H"}
	    Taboo {[.cgmw childsite].st EvalNoTyped "::CG::greedy -H"}
	    "Genetic_A" {[.cgmw childsite].st EvalNoTyped "::CG::algogen -H"}
	    Flips {[.cgmw childsite].st EvalNoTyped "::CG::flips -H"}
	    Polish {[.cgmw childsite].st EvalNoTyped "::CG::polish -H"}
	    Search_config {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Configure the search algorithms."}
	    Group_selection {[.cgmw childsite].st EvalNoTyped "::CG::mrkselset -H"}
	    Group_config {[.cgmw childsite].st EvalNoTyped "::CG::group -H"}
	    Select {[.cgmw childsite].st EvalNoTyped "::CG::mrkselset -H"}
	    Group {[.cgmw childsite].st EvalNoTyped "::CG::group -H"}
	    Quiet {[.cgmw childsite].st EvalNoTyped "::CG::quietset -H"}
	    Verbose {[.cgmw childsite].st EvalNoTyped "::CG::verbset -H"}
	    Dist2p {[.cgmw childsite].st EvalNoTyped "::CG::mrkdist2p -H"}
	    Lod2p {[.cgmw childsite].st EvalNoTyped "::CG::mrklod2p -H"}
	    Fr2p {[.cgmw childsite].st EvalNoTyped "::CG::mrkfr2p -H"}
	    Loci_info {[.cgmw childsite].st EvalNoTyped "::CG::mrkinfo -H"}
	    Loci_merge {[.cgmw childsite].st EvalNoTyped "::CG::mrkmerge -H"}
	    Maps_info {[.cgmw childsite].st EvalNoTyped "::CG::heaprint -H"}
	    Maps_detail {[.cgmw childsite].st EvalNoTyped "::CG::heaprintd -H"}
	    Maps_detailed {[.cgmw childsite].st EvalNoTyped "::CG::heaprinto -H"}
	    Maps_config {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Configure the maps features\n."}
	    
            "Session Reset" {[.cgmw childsite].st EvalNoTyped "::CG::cgrestart -H"}
	    "Session Save" {[.cgmw childsite].st EvalNoTyped "cgsave -H"}
	    "Session Restore" {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Restore from a file the state of a previous session."}
	    "Map Export" {[.cgmw childsite].st EvalNoTyped "cgexport -H"}
	    Stop {[.cgmw childsite].st PutsAlias stdout "\nGUI feature : Stop a running command, keeping the heap of maps coherent."}
	}
    }

    .cgmw configure -cursor ""
}
