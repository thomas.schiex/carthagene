#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGexportDialog.tcl,v 1.1 2004-06-08 12:13:48 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue pour l'export
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# Usual options.
#
itk::usual CGexportDialog {
    keep -activebackground -activerelief -background -borderwidth -cursor \
	 -elementborderwidth -foreground -highlightcolor -highlightthickness \
	 -insertbackground -insertborderwidth -insertofftime -insertontime \
	 -insertwidth -jump -labelfont -modality -selectbackground \
	 -selectborderwidth -selectforeground -textbackground -textfont \
	 -troughcolor
}

# ------------------------------------------------------------------
#                           SELECTIONDIALOG
# ------------------------------------------------------------------
class CGexportDialog {
    inherit iwidgets::Dialog

    itk_option define -selectfiln selectlabg Text "File Name"

    constructor {args} {}

    public method getfn {}
    public method getmn {}
    public method getcn {}
    public method setfn {}

    private variable fb {} ;#le browser de fichiers
 
}
    
option add *exportDialog.title "CartaGene Export Dialog" widgetDefault
option add *exportDialog.master "." widgetDefault

#
# Provide a lowercased access method for the Selectiondialog class.
# 

proc cgexportdialog {pathName args} {
    uplevel CGexportDialog $pathName $args
}

# ------------------------------------------------------------------
#                        CONSTRUCTEUR
# ------------------------------------------------------------------
body CGexportDialog::constructor {args} {
    #
    # Set the borderwidth to zero.
    #
    component hull configure -borderwidth 0
    
    set fb [lindex $args [expr 1 + [lsearch $args "-filebrowser"]]]
    
    itk_component add mapname {
	iwidgets::Entryfield $itk_interior.mapname \
	    -labeltext "Map Name"
    }
    
    itk_component add chroname {
	iwidgets::Entryfield $itk_interior.chroname \
	    -labeltext "Chromosome Name"
    }
    
    itk_component add mapfile {
	iwidgets::Entryfield $itk_interior.mapfile \
	    -labeltext "Map File"
    } 
    
    itk_component add butbrowse {
	button $itk_interior.mapsearch \
	    -text "Browse ..." \
	    -command [code $this setfn]
    }
    $itk_component(mapname) insert 0 "The best map!"
    $itk_component(chroname) insert 0 "X"

    pack $itk_component(mapname) -fill x -expand yes
    pack $itk_component(chroname) -fill x -expand yes
    pack $itk_component(mapfile) -side left -fill x -expand yes
    pack $itk_component(butbrowse) -side right
    
    
    hide Help
    hide Apply
    
    #bidouille?
    
    eval itk_initialize [lrange $args [expr 2 + [lsearch $args "-filebrowser"]] end ]
}   

# ------------------------------------------------------------------
#                            METHODS
# ------------------------------------------------------------------
   

# ------------------------------------------------------------------
# METHOD: get
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGexportDialog::getfn {} {
    return [$itk_component(mapfile) get]
}

# ------------------------------------------------------------------
# METHOD: get
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGexportDialog::getmn {} {
    return [$itk_component(mapname) get]
}

# ------------------------------------------------------------------
# METHOD: get
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGexportDialog::getcn {} {
    return [$itk_component(chroname) get]
}
# ------------------------------------------------------------------
# METHOD: set
#
# Thinwrapped method of selection box class.
# ------------------------------------------------------------------
body CGexportDialog::setfn {} {
    if {[.cgfb activate]} {
	$itk_component(mapfile) delete 0 end
	$itk_component(mapfile) insert 0 [$fb get]
    }
}