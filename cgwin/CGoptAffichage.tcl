#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGoptAffichage.tcl,v 1.3 2002-11-07 16:59:02 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Classe de gestion des options d'affichage
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par défaut
#

#
# Options usuelles.
#
itk::usual CGoptAffichage {
    keep -background -cursor -foreground -modality \
            -font -labelfont -textfont -titlefont
}

class CGoptAffichage {
    inherit iwidgets::Dialog

    itk_option define -mode mode Mode "vertical"
    itk_option define -repreCarte repreCarte RepreCarte "ratio"
    itk_option define -repreMrq repreMrq RepreMrq "ratio"
    itk_option define -zoomMap zoomMap ZoomMap 100
    itk_option define -zoomMrq zoomMrq ZoomMrq 100
    itk_option define -repreVraisem repreVraisem RepreVraisem delta
    itk_option define -repreEtiq repreEtiq RepreEtiq name&distance

    constructor {args} {}
    public method cget {arg}
    private method _cmdValider {}
    private method _cmdAbandonner {}

}


proc cgoptaffichage {pathName args} {
    uplevel CGoptAffichage $pathName $args
}


body CGoptAffichage::constructor {args} {

    itk_component add commentaires {
        label $itk_interior.commentaires \
                -text "Graphic options to display the maps." 
    } {
        rename -font -titlefont titleFont Font
    }
    pack $itk_component(commentaires) -padx 4 -pady 20

    # Mode d'affichage des cartes: horizontal ou vertical
    itk_component add mode {
	iwidgets::optionmenu $itk_interior.mode \
	    -labeltext "Orientation of the maps :" \
	    -labelpos w \
	    -labelmargin 5 \
	    -cyclicon 1 
    }
    pack $itk_component(mode) -padx 4 -pady 10 -side top -anchor w

    $itk_component(mode) insert end "horizontal" "vertical"

    # Mode de représentation des cartes: fixed ou ratio
    itk_component add repreCarte {
	iwidgets::optionmenu $itk_interior.repreCarte \
                -labeltext "Spacing of the maps :" \
                -labelpos w \
                -labelmargin 5 \
                -cyclicon 1 
    }
    pack $itk_component(repreCarte) -padx 4 -pady 10 -side top -anchor w

    $itk_component(repreCarte) insert end "fixed" "ratio"

    # Mode de représentation des marqueurs: fixed ou ratio
    itk_component add repreMrq {
	iwidgets::optionmenu $itk_interior.repreMrq \
                -labeltext "Spacing of the markers :"  \
                -labelpos w \
                -labelmargin 5 \
                -cyclicon 1 
    }
    pack $itk_component(repreMrq) -padx 4 -pady 10 -side top -anchor w

    $itk_component(repreMrq) insert end "fixed" "ratio"

    # Affichage de la valeur de la vraisemblance d'une carte
    itk_component add repreVraisem {
	iwidgets::optionmenu $itk_interior.repreVraisem \
                -labeltext "LOD of the Maps :" \
                -labelpos w \
                -labelmargin 5 \
                -cyclicon 1
    }
    pack $itk_component(repreVraisem) -padx 4 -pady 10 -side top -anchor w
    
    $itk_component(repreVraisem) insert end "value" "delta" "value&delta"

    # Contenu des étiquettes des marqueurs
    itk_component add repreEtiq {
	iwidgets::optionmenu $itk_interior.repreEtiq \
                -labeltext "Label of the markers :" \
                -labelpos w \
                -labelmargin 5 \
                -cyclicon 1
    }
    pack $itk_component(repreEtiq) -padx 4 -pady 10 -side top -anchor w

    $itk_component(repreEtiq) insert end "name" "distance" "name&distance"

    iwidgets::Labeledwidget::alignlabels \
            $itk_component(mode) \
            $itk_component(repreCarte) \
            $itk_component(repreMrq) \
            $itk_component(repreVraisem) \
            $itk_component(repreEtiq)

    # Séparateur
    itk_component add separateur {
	frame $itk_interior.separateur \
                -relief sunken \
                -height 3 \
                -width 3 \
                -borderwidth 1

    } {
	keep -background -cursor
    }
    pack $itk_component(separateur) -expand no -fill x -padx 50 -pady 10

 
    # Effet ZOOM
    itk_component add frameZoom {
        iwidgets::labeledframe $itk_interior.frameZoom \
                -labelpos n \
                -labeltext "Resizing"
    }
    set itk_interiorZoom [$itk_component(frameZoom) childsite]
  
    # Zoom sur l'axe des marqueurs
    itk_component add zoomMrq {
        iwidgets::spinint $itk_interiorZoom.zoomMrq \
                -labeltext "Maps" \
                -range {50 200} \
                -width 3 \
                -step 10 
    }

    # Zoom sur l'axe des cartes
    itk_component add zoomMap {
        iwidgets::spinint $itk_interiorZoom.zoomMap \
                -labeltext "Between the maps" \
                -range {50 200} \
                -width 3 \
                -step 10 
    }

    pack $itk_component(zoomMrq) $itk_component(zoomMap) \
	-side left -padx 4 -pady 10

    pack $itk_component(frameZoom) -padx 4 -pady 10

    default Cancel
    hide Help
    hide Apply
    buttonconfigure OK  -command [code $this _cmdValider]
    buttonconfigure Cancel  -command [code $this _cmdAbandonner]

    eval itk_initialize $args

}


configbody CGoptAffichage::mode {

    set mode $itk_option(-mode)
    set modes {horizontal vertical}

    if {[lsearch $modes $mode] != -1} {
            $itk_component(mode) select $mode
    } else {
            error "wrong value \"-mode $mode\".\nPossible values are \n\"horizontal\"\nand \"vertical\""
    }
}


configbody CGoptAffichage::repreCarte {

    set mode $itk_option(-repreCarte)
    set modes {fixed ratio}

    if {[lsearch $modes $mode] != -1} {
            $itk_component(repreCarte) select $mode
    } else {
            error "wrong value \"-repreCarte $mode\".\nPossible values are \n\"fixed\"\nand \"ratio\""
    }
}


configbody CGoptAffichage::repreMrq {

    set mode $itk_option(-repreMrq)
    set modes {fixed ratio}

    if {[lsearch $modes $mode] != -1} {
            $itk_component(repreMrq) select $mode
    } else {
            error "wrong value \"-repreMrq $mode\".\nPossible values are \n\"fixed\"\nand \"ratio\""
    }
}


configbody CGoptAffichage::zoomMrq {

    if {"$itk_option(-zoomMrq)" < "50" || "$itk_option(-zoomMrq)" > "200"} {
        error "wrong value \"-zoomMrq $itk_option(-zoomMrq)\".\nExpected between\n 50 and 200"
    } else {
        $itk_component(zoomMrq) delete 0 end
        $itk_component(zoomMrq) insert 0 $itk_option(-zoomMrq)
    }
}


configbody CGoptAffichage::zoomMap {

    if {"$itk_option(-zoomMap)" < "50" || "$itk_option(-zoomMap)" > "200"} {
        error "wrong value \"-zoomMap $itk_option(-zoomMap)\".\nExpected between\n 50 and 200"
    } else {
        $itk_component(zoomMap) delete 0 end
        $itk_component(zoomMap) insert 0 $itk_option(-zoomMap)
    }
}


configbody CGoptAffichage::repreVraisem {

    set mode $itk_option(-repreVraisem)
    set modes {value delta value&delta}

    if {[lsearch $modes $mode] != -1} {
            $itk_component(repreVraisem) select $mode
    } else {
            error "wrong value \"-repreVraisem $mode\".\nPossible values are \n\"value\"\n\"delta\"\nand \"value&delta\""
    }
}

configbody CGoptAffichage::repreEtiq {

    set mode $itk_option(-repreEtiq)
    set  modes {name distance name&distance}

    if {[lsearch $modes $mode] != -1} {
            $itk_component(repreEtiq) select $mode
    } else {
            error "wrong value \"-repreEtiq $mode\".\nPossible values are \n\"name\"\n\"distance\"\nand \"name&distance\""
    }
}


body CGoptAffichage::_cmdValider {} {

    if {$itk_option(-mode) != [$itk_component(mode) get]} {
	set itk_option(-mode) [$itk_component(mode) get]    
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-repreCarte) != [$itk_component(repreCarte) get]} {
	set itk_option(-repreCarte) [$itk_component(repreCarte) get]     
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-repreMrq) != [$itk_component(repreMrq) get] } {
	set itk_option(-repreMrq) [$itk_component(repreMrq) get]     
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-zoomMrq) != [$itk_component(zoomMrq) get]} {
	set itk_option(-zoomMrq) [$itk_component(zoomMrq) get]     
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-zoomMap) != [$itk_component(zoomMap) get]} {
	set itk_option(-zoomMap) [$itk_component(zoomMap) get]    
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-repreVraisem) != [$itk_component(repreVraisem) get]} {
	set itk_option(-repreVraisem) [$itk_component(repreVraisem) get]    
	uplevel #0 set CGWhaschanged 1
    }
    if {$itk_option(-repreEtiq) != [$itk_component(repreEtiq) get]} {
	set itk_option(-repreEtiq) [$itk_component(repreEtiq) get]    
	uplevel #0 set CGWhaschanged 1
    }
    
    $this deactivate 1
}

#
#
#
body CGoptAffichage::_cmdAbandonner {} {
    
    configure -mode $itk_option(-mode)
    configure -repreCarte $itk_option(-repreCarte)
    configure -repreMrq $itk_option(-repreMrq)
    configure -zoomMrq $itk_option(-zoomMrq)
    configure -zoomMap $itk_option(-zoomMap)
    configure -repreVraisem $itk_option(-repreVraisem)
    configure -repreEtiq $itk_option(-repreEtiq)
    
    $this deactivate 1
}

body CGoptAffichage::cget {arg} {

    return $itk_option($arg)

}
