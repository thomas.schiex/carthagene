#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGcmDialog.tcl,v 1.4 2005-01-14 17:24:17 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue pour la configuration du calcul des groupes
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------


#
#
# Usual options.
#
itk::usual CGcmDialog {
    keep -activebackground -activerelief -background -borderwidth -cursor \
	-elementborderwidth -foreground -highlightcolor -highlightthickness \
	-insertbackground -insertborderwidth -insertofftime -insertontime \
	-insertwidth -jump -labelfont -modality -selectbackground \
	-selectborderwidth -selectforeground -textbackground -textfont \
	-troughcolor
}

#
#
#
class CGcmDialog {
    inherit iwidgets::Dialog
    
    constructor {args} {} 
    public method get {arg}
}


#
# Provide a lowercased access method for the Buttonbox class.
# 
proc cgcmdialog {pathName args} {
    uplevel CGcmDialog $pathName $args
}
#
#
#
#
body CGcmDialog::constructor {args} {
    #
    # Set the borderwidth to zero.
    #
    component hull configure -borderwidth 0
    
    itk_component add cm {
	cgconfigmaps $itk_interior.cm
    }
    
    pack $itk_component(cm) -fill both -expand yes

    hide Help

    buttonconfigure Apply \
	-command CGWMresizestorage \
	-text Try
    
    set cmd [concat $itk_component(cm) "Abandonner;" \
		 $this "deactivate 0" ]
    
    buttonconfigure Cancel \
	-command $cmd
    
    set cmd [concat  $itk_component(cm) "Valider;" \
		 $this "deactivate 1" ]
    
    buttonconfigure OK \
	-command $cmd
    eval itk_initialize $args
}

body CGcmDialog::get {arg} {
    
    switch -- $arg {
	"-storage" { return [$itk_component(cm) get -storage]}
	"-visuala" { return [$itk_component(cm) get -visuala]}
	"-unit" { return [$itk_component(cm) get -unit]}
	"-coef" { return [$itk_component(cm) get -coef]}
        default {
            error "unknown parameter $arg."
        }
    }
}

