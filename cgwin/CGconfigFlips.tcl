#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigFlips.tcl,v 1.3 2002-11-07 16:58:25 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres de la m�thode flips
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#

#
# Options usuelles.
#
itk::usual CGconfigFlips {
    keep -background -cursor -foreground
    # -modality
}


class CGconfigFlips {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -wins wins Wins 5
    itk_option define -fthres fthres Fthres 0.3
    itk_option define -fiter fiter Fiter 1

    common itera 0

    constructor {args} {}
    public method get {arg}

}

proc cgconfigflips {pathName args} {
    uplevel CGconfigFlips $pathName $args
}

body CGconfigFlips::constructor {args} {
    
    itk_component add commentaires {
        label $itk_interior.commentaires \
	    -text "Specific parameters of the Flip Algorithm"
    }
    pack $itk_component(commentaires) -padx 4 -pady 5
    
    itk_component add wins {
	iwidgets::spinint $itk_interior.wins \
	    -range {0 9} \
	    -width 2 \
	    -labeltext "Window Size"
    }
    pack $itk_component(wins) -padx 10 -pady 10 -anchor e
    
    itk_component add fthres {
        spinreal $itk_interior.fthres \
	    -range {-5.0 5.0} -width 4 \
	    -labeltext "Threshold"
    }
    pack $itk_component(fthres) -padx 10 -pady 10 -anchor e
    
    itk_component add fiter {
	checkbutton $itk_interior.fiter \
	    -variable [scope itera] \
	    -text "Iterative"
	}
    pack $itk_component(fiter) -padx 10 -pady 10 -anchor e
    
    eval itk_initialize $args
}


configbody CGconfigFlips::wins {

    set nbr $itk_option(-wins)
    if {$nbr < 0 || $nbr > 9} {
        error "wrong value \"wins\": should be between 0 and 9"
    } else {
        $itk_component(wins) delete 0 end
        $itk_component(wins) insert 0 $nbr
    }
}


configbody CGconfigFlips::fthres {

    set nbr $itk_option(-fthres)
    if {$nbr < -5.0 || $nbr > 5.0} {
        error "wrong value \"fthres\": should be between -5.0 and 5.0"
    } else {
        $itk_component(fthres) delete 0 end
        $itk_component(fthres) insert 0 $nbr
    }
}


configbody CGconfigFlips::fiter {
    
    if {$itk_option(-fiter) != 0 && $itk_option(-fiter) != 1} {
	 error "wrong value \"flips\": should be 0 or  1"
     }
     
     if {$itk_option(-fiter) == 0} {
	 $itk_component(fiter) deselect
     } else {
	 $itk_component(fiter) select
     }  
     
 }




body CGconfigFlips::get {arg} {
    
    switch -- $arg {
        "-wins" { return [$itk_component(wins) get]}
        "-fthres" { return [$itk_component(fthres) get]}
        "-fiter" { return $itera}
        default {
            error "unknown parameter $arg."
        }
    }
}
