#-----------------------------------------------------------------------------
# 
# $Id: CGconfigBuildFW.tcl,v 1.1 2002-10-31 13:35:01 tschiex Exp $
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres de la m�thode du Buildfw
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#
#option add *CGconfigBuildFW.keepthres 3 widgetDefault
#option add *CGconfigBuildFW.addthres 3 widgetDefault

#
# Options usuelles.
#
itk::usual CGconfigBuildFW {
    keep -background -cursor -foreground
# -modality
}

#<
#
#
#>
class CGconfigBuildFW {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -keepthres keepthres Keepthres 3
    itk_option define -addthres  addthres  Addthres 3

    constructor {args} {}
    public method get {arg}

}


proc cgconfigbuildfw {pathName args} {
    uplevel CGconfigBuildFW $pathName $args
}


body CGconfigBuildFW::constructor {args} {

    itk_component add commentaires {
        label $itk_interior.commentaires \
                -text "Specific parameter of the method BuildFW"
    }
    pack $itk_component(commentaires) -padx 4 -pady 5

    itk_component add  keepthres {
        spinreal $itk_interior.keepthres     \
                -range {0.5 10.0} -width 4 \
                -labeltext "keepthres:"
    }
    pack $itk_component(keepthres) -padx 10 -pady 10 -anchor e

    itk_component add  addthres {
        spinreal $itk_interior.addthres     \
                -range {0.5 10.0} -width 4 \
                -labeltext "addthres:"
    }
    pack $itk_component(addthres) -padx 10 -pady 10 -anchor e


    eval itk_initialize $args

}


configbody CGconfigBuildFW::keepthres {

    set nbr $itk_option(-keepthres)
    if {$nbr < 0.5 || $nbr > 10.0} {
        error "wrong value \"keepthres\": should be between 0.5 and 10.0"
    } else {
        $itk_component(keepthres) delete 0 end
        $itk_component(keepthres) insert 0 $nbr
    }
}

configbody CGconfigBuildFW::addthres {

    set nbr $itk_option(-addthres)
    if {$nbr < .5 || $nbr > 10} {
        error "wrong value \"addthres\": should be between .5 and 10"
    } else {
        $itk_component(addthres) delete 0 end
        $itk_component(addthres) insert 0 $nbr
    }
}

body CGconfigBuildFW::get {arg} {

    switch -- $arg {
        "-keepthres" { return [$itk_component(keepthres) get]}
        "-addthres"  { return [$itk_component(addthres) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}
