#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigGroup.tcl,v 1.4 2002-11-07 16:58:25 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres de la commande group
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#

#
# Options usuelles.
#
itk::usual CGconfigGroup {
    keep -background -cursor -foreground
    # -modality
}

class CGconfigGroup {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -lodthres lodthres Lodthres 3.0
    itk_option define -disthres disthres Disthres 0.5

    constructor {args} {}
    public method get {arg}

}

proc cgconfiggroup {pathName args} {
    uplevel CGconfigGroup $pathName $args
}

body CGconfigGroup::constructor {args} {
    
    itk_component add commentaires {
        label $itk_interior.commentaires \
	    -text "Specific parameters for the identification of groups."
    }
    pack $itk_component(commentaires) -padx 4 -pady 5
    
    itk_component add lodthres {
        spinreal $itk_interior.lodthres \
	    -range {1.0 9.0} -width 4 \
	    -labeltext "LOD Threshold"
    }
    pack $itk_component(lodthres) -padx 10 -pady 10 -anchor e
    
    itk_component add disthres {
        spinreal $itk_interior.disthres \
	    -range {0.05 0.5} -width 4 \
	    -labeltext "Distance Threshold (Morgan)"
    }
    pack $itk_component(disthres) -padx 10 -pady 10 -anchor e
    
    eval itk_initialize $args
}

configbody CGconfigGroup::lodthres {
    
    set nbr $itk_option(-lodthres)
    if {$nbr < 1.0 || $nbr > 9.0} {
        error "wrong value \"lodthres\": should be between 1.0 and 9.0"
    } else {
        $itk_component(lodthres) delete 0 end
        $itk_component(lodthres) insert 0 $nbr
    }
}

configbody CGconfigGroup::disthres {
    
    set nbr $itk_option(-disthres)
    if {$nbr < 0.05 || $nbr > 0.5} {
        error "wrong value \"disthres\": should be between 0.05 and 0.5"
    } else {
        $itk_component(disthres) delete 0 end
        $itk_component(disthres) insert 0 $nbr
    }
}

body CGconfigGroup::get {arg} {
    
    switch -- $arg {
        "-lodthres" { return [$itk_component(lodthres) get]}
        "-disthres" { return [$itk_component(disthres) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}
