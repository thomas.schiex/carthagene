#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGDefaut.tcl,v 1.2 2002-11-07 16:58:08 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Les variables globales, en cours de suppression
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#package require Iwidgets
#package provide cgwin

#set couleurs "black white red blue yellow green magenta VioletRed pink moccasin NavyBlue RoyalBlue SkyBlue cyan SeaGreen black SpringGreen khaki gold firebrick"

set CGW_color(logTxt)      magenta
set CGW_color(carte)       blue
set CGW_color(mrqTxt)      black
set CGW_color(mrqTxtSel)   orange
set CGW_color(mrqLineOn)   green 
set CGW_color(mrqLineOff)  black 
set CGW_color(infoLine)    red
set CGW_color(infoTxt)     red
set CGW_color(infoBox)     yellow

#set CGW_option(ToileLargeur) 600
#set CGW_option(ToileHauteur) 2000
set CGW_option(margeH)       100
set CGW_option(margeV)       80

#set CGW_font(infoTxt)      "fixed"
#set CGW_font(mrqTxt)       "fixed"
#set CGW_font(logTxt)       "fixed"
#set CGW_font(infoTxt)      "*-times-*-*-*--10-*-*-*-*-*-*-*"
#set CGW_font(mrqTxt)       "*-times-*-*-*--12-*-*-*-*-*-*-*"
#set CGW_font(logTxt)       "*-times-*-*-*--12-*-*-*-*-*-*-*"
set CGW_font(infoTxt) "-adobe-times-bold-r-normal--10-100-75-75-p-57-iso8859-1"
set CGW_font(mrqTxt)  "-adobe-times-bold-r-normal--12-120-75-75-p-67-iso8859-1"
set CGW_font(logTxt)  "-adobe-times-bold-r-normal--12-120-75-75-p-67-iso8859-1"

set G(outputMap) map.txt
set G(outputStats) stats.txt
set G(dVerbeux) 0
set G(dPolissage) 0
set G(dSilencieux) 0
set G(flipWSize) 0
set G(flipThres) 3.0
set G(flipIterB) 0
set G(methode) Greedy
set G(nbIter) 1 
set G(nbRun) 1
set G(taboolenMin) 1
set G(taboolenMax) 14
set G(nbGens) 10
set G(nbEle) 12
set G(selNbr) 1
set G(pCross) 0.33
set G(pMut) 0.55
set G(evolFitness) 1 
set G(tries) 10 
set G(tinit) 0.4
set G(tfinal) 0.2 
set G(cooling) 0.92
set G(nbMap) 3	

# set G(mode) "horizontal"
# set G(repreCarte) "ratio"
# set G(repreMrq) "ratio"
# set G(zoomMap) 100
# set G(zoomMrq) 100
# set G(repreVraisem) "value&delta"
# set G(repreEtiq) "name"
# set G(toutesEtiq) 0
