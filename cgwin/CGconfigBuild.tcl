#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigBuild.tcl,v 1.3 2002-11-07 16:58:08 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres de la m�thode du Recuit
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#
#option add *CGconfigBuild.nbMap 3 widgetDefault

#
# Options usuelles.
#
itk::usual CGconfigBuild {
    keep -background -cursor -foreground
# -modality
}

#<
#
#
#>
class CGconfigBuild {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -nbmap nbmap Nbmap 3

    constructor {args} {}
    public method get {arg}

}


proc cgconfigbuild {pathName args} {
    uplevel CGconfigBuild $pathName $args
}


body CGconfigBuild::constructor {args} {

    itk_component add commentaires {
        label $itk_interior.commentaires \
                -text "Specific parameter of the method Build"
    }
    pack $itk_component(commentaires) -padx 4 -pady 5

    itk_component add nbmap {
        iwidgets::spinint $itk_interior.nbmap     \
                -range {1 16} -width 3 \
                -labeltext "Number of maps:"
    }
    pack $itk_component(nbmap) -padx 10 -pady 10 -anchor e

    eval itk_initialize $args

}


configbody CGconfigBuild::nbmap {

    set nbr $itk_option(-nbmap)
    if {$nbr < 0 || $nbr > 16} {
        error "wrong value \"nbmap\": should be between 0 and 16"
    } else {
        $itk_component(nbmap) delete 0 end
        $itk_component(nbmap) insert 0 $nbr
    }
}


body CGconfigBuild::get {arg} {

    switch -- $arg {
        "-nbmap" { return [$itk_component(nbmap) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}
