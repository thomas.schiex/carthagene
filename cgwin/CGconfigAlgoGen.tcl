#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigAlgoGen.tcl,v 1.3 2002-11-07 16:58:08 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres de la m�thode AlgoGen
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#
#option add *CGconfigAlgoGen.nbGens 10 widgetDefault
#option add *CGconfigAlgoGen.nbEle 10 widgetDefault
#option add *CGconfigAlgoGen.selNbr 0 widgetDefault
#option add *CGconfigAlgoGen.pCross 0.3 widgetDefault
#option add *CGconfigAlgoGen.pMut 0.5 widgetDefault
#option add *CGconfigAlgoGen.evolFitness 1 widgetDefault

#
# Options usuelles.
#
itk::usual CGconfigAlgoGen {
    keep -background -cursor -foreground
}

class CGconfigAlgoGen {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -nbgens nbgens Nbgens 10
    itk_option define -nbele nbele Nbele 10
    itk_option define -selnbr selnbr Selnbr 0
    itk_option define -pcross pcross Pcross 0.3
    itk_option define -pmut pmut Pmut 0.5
    itk_option define -evolfitness evolfitness Evolfitness 1

    constructor {args} {}
    public method get {arg}

}


proc cgconfigalgogen {pathName args} {
    uplevel CGconfigAlgoGen $pathName $args
}


body CGconfigAlgoGen::constructor {args} {

    itk_component add commentaires {
        label $itk_interior.commentaires \
                -text "Specific parameters of the local search method genetic algorithm"
    }
    pack $itk_component(commentaires) -padx 4 -pady 5

    itk_component add nbgens {
        iwidgets::spinint $itk_interior.nbgens     \
                -range {1 100} -width 3 \
                -labeltext "Number of  successive generations:"
    }
    pack $itk_component(nbgens) -padx 10 -pady 10 -anchor e

    itk_component add nbele {
        iwidgets::spinint $itk_interior.nbele    \
                -range {1 100} -width 3 \
                -labeltext "Population size:"
    }
    pack $itk_component(nbele) -padx 10 -pady 10 -anchor e

    itk_component add selnbr {
        iwidgets::spinint $itk_interior.selnbr \
                -range {0 1} -width 3 \
                -labeltext "Selection Number:"
    }
    pack $itk_component(selnbr) -padx 10 -pady 10 -anchor e

    itk_component add pcross {
        spinreal $itk_interior.pcross \
                -range {0.0 1.0} -width 4 \
                -labeltext "Crossover Probability:"
    }
    pack $itk_component(pcross) -padx 10 -pady 10 -anchor e

    itk_component add pmut {
        spinreal $itk_interior.pmut \
                -range {0.0 1.0} -width 4 \
                -labeltext "Mutation probability:"
    }
    pack $itk_component(pmut) -padx 10 -pady 10 -anchor e

    itk_component add evolfitness {
        iwidgets::spinint $itk_interior.evolfitness \
                -range {0 1} -width 3 \
                -labeltext "Evolutive fitness:"
    }
    pack $itk_component(evolfitness) -padx 10 -pady 10 -anchor e

    eval itk_initialize $args

}


configbody CGconfigAlgoGen::nbgens {

    set nbr $itk_option(-nbgens)
    if {$nbr < 0 || $nbr > 100} {
        error "wrong value \"nbgens\": should be between 1 or 1O0"
    } else {
        $itk_component(nbgens) delete 0 end
        $itk_component(nbgens) insert 0 $nbr
    }
}


configbody CGconfigAlgoGen::nbele {

    set nbr $itk_option(-nbele)
    if {$nbr < 0 || $nbr > 100} {
        error "wrong value \"nbele\": should be between 1 or 1O0"
    } else {
        $itk_component(nbele) delete 0 end
        $itk_component(nbele) insert 0 $nbr
    }
}


configbody CGconfigAlgoGen::selnbr {

    set nbr $itk_option(-selnbr)
    if {$nbr < 0 || $nbr > 100} {
        error "wrong value \"selnbr\": should be between 1 or 1O0"
    } else {
        $itk_component(selnbr) delete 0 end
        $itk_component(selnbr) insert 0 $nbr
    }
}


configbody CGconfigAlgoGen::pcross {

    set nbr $itk_option(-pcross)
    if {$nbr < 0.0 || $nbr > 1.0} {
        error "wrong value \"pcross\": should be between 0.0 or 1.0"
    } else {
        $itk_component(pcross) delete 0 end
        $itk_component(pcross) insert 0 $nbr
    }
}


configbody CGconfigAlgoGen::pmut {

    set nbr $itk_option(-pmut)
    if {$nbr < 0.0 || $nbr > 1.0} {
        error "wrong value \"pmut\": should be between 0.0 or 1.0"
    } else {
        $itk_component(pmut) delete 0 end
        $itk_component(pmut) insert 0 $nbr
    }
}


configbody CGconfigAlgoGen::evolfitness {

    set nbr $itk_option(-evolfitness)
    if {$nbr != 0 && $nbr != 1} {
        error "wrong value \"evolfitness\": should be 0 or 1"
    } else {
        $itk_component(evolfitness) delete 0 end
        $itk_component(evolfitness) insert 0 $nbr
    }
}


body CGconfigAlgoGen::get {arg} {

    switch -- $arg {
        "-nbgens" { return [$itk_component(nbgens) get]}
        "-nbele" { return [$itk_component(nbele) get]}
        "-selnbr" { return [$itk_component(selnbr) get]}
        "-pcross" { return [$itk_component(pcross) get]}
        "-pmut" { return [$itk_component(pmut) get]}
        "-evolfitness" { return [$itk_component(evolfitness) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}


