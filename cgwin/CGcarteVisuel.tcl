#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGcarteVisuel.tcl,v 1.8 2005-05-11 08:38:10 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Module 	: cgwin 
# Projet	: CartaGene
#
# Description : Classe de gestion graphique des cartes
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# Liste des tags employ�s et utilisation
# Les tags li�s aux d�clenchement d'�v�nements sont sans �quivoque (pas
# d'ast�risque).
# Ceux qui peuvent rev�tir plusieurs formes servent � restreindre la port�e
# de l'�v�nement.
#
# ::cGcarteVisuel* :
#     d�termine pour quelle carte on demande des info
# ::cGcarteVisuel*:$mrq :
#     d�termine quel marqueur est trait�
# ::cGcarteVisuel*:$mrq:etiq :
#     d�termine quelle �tiquette est trait�e (utile pour la destruction)
# ::cGcarteVisuel*:info :
#     permet de d�truire les info
# carte :
#     li� � la m�thode "afficheInfoCarte".
#     Clic sur une carte = demande � afficher les informations
# coulLien :
#     li� � la m�thode "choixCouleurLien" quand la roulette � 4 positions est
#     sur la position 2 ou 3.
# enregis :
#     li� � la m�thode "changeCouleurR" pour restituer les couleurs
# etiq$mrq :
#     permet de s�lectionner l'item sur lequel on vient de cliquer
# info :
#     li� aux m�thodes "pushMrq", "depMrq" et "popMrq"
#     D�placement des �tiquettes des marqueurs
# lienMrq :
#     li� aux m�thodes "changeCouleur" et "changeCouleurP"
# mrq :
#     gestion de la roulette � 4 positions sur les noms des marqueurs
#

class CGcarteVisuel {
    
    # Toile d'affichage (canvas sp�cial tas) + acc�s aux information du tas
    common tasvisuel

    # Vraisemblance (Log Likelyhood)
    variable score
    
    # Id corresponding to the heap
    variable realId
    # type of the data set
    variable dstype 
    # Id oh the data set
    variable dsId
    # Taille (distance) de la carte
    variable taille
    
    # Distance � la meilleure carte dans le tas
    variable delta
    
    # Chromosomes for each locus
    public variable liseg

    # True position for each locus
    public variable listruepos
    
    # Distances de chaque marqueur sur la carte
    public variable marqueurs

    # Ordre des marqueurs sur la carte
    public variable ordre

    # Num�ro d'ordre de la carte dans le tas et liste des cartes.
    variable numCarte
 
    # Indicateur d'affichage des �tiquettes d'info des marqueurs
    #  1 = affich�es
    #  0 = pas affich�es
    public variable info

    # Position des �tiquettes sur la carte
    variable pos

    public common Orientation "horizontal"
    public common RepreCarte "ratio"
    public variable RepreMrq "ratio"
    public common RepreVraisem "delta"
    public common RepreEtiq "name&distance"
 

    constructor { can indice rid type dsi listecarte\
		      orientation repreCarte repreMrq\
		      repreVraisem repreEtiq lis listp} {}

    public method dessiner_1 {}
    public method dessiner_2 {}
    public method positionH { mrq }
    public method positionV { mrq }

    # retourne la carte pr�c�dente d�tenant le marqueur
    public method has { mrq }
    public method afficheEtiq { mrq actuel action tabCoul}
    public method afficheInfo { tabCoul etiq }
    private method _calculePosEtiq {}
    public method ajustePos { mrq x y }
     # pour acc�der � des variables
    public method get { variabl }


    public method flipSeg { numseg }
    public method pushUpSeg { numseg }

}

#
# Provide a lowercased access method for the Buttonbox class.
# 
# proc cgcartevisuel {pathName can indice liscarte orientation repreCarte repreMrq repreVraisem repreEtiq} {
#     uplevel CGcarteVisuel $pathName $can $indice $liscarte $orientation $repreCarte $repreMrq $repreVraisem $repreEtiq
# }
#
#
#

# Provide a lowercased access method for the Buttonbox class.
 
proc cgcartevisuel {pathName args} {
    uplevel CGcarteVisuel $pathName $args
}
#
#
#

body CGcarteVisuel::constructor { can indice rid type dsi liscarte orientation repreCarte \
        repreMrq repreVraisem repreEtiq lis listp} {

    set tasvisuel $can
    set numCarte $indice
    set realId $rid
    set dstype $type
    set dsId $dsi
    set Orientation $orientation
    set RepreCarte $repreCarte
    # a map belonging to a order dataset has no distance info
    #if {$lis != {}} {
    #	set RepreMrq "fixed"
    #} else {
    set RepreMrq $repreMrq
    #}
    set RepreVraisem $repreVraisem
    set RepreEtiq $repreEtiq

    set score [lindex $liscarte end]
    set liscarte [lreplace $liscarte end end]

    set liseg $lis

    set listruepos $listp

    set delta [expr $score - [lindex $liscarte end]]
    set liscarte [lreplace $liscarte end end]

    set taille [lindex $liscarte end]
    # set liscarte [lreplace $liscarte end end]

    # Indicateur d'affichage des �tiquettes d'info des marqueurs
    set info 0
    
    set dist 0.0
    set j 0
    for {set i 0} {$i < [llength $liscarte]} {incr i 2} {
	
        # Calcule la position (distance) du Ieme marqueur sur la carte.
        set dist [lindex $liscarte [expr $i + 1]]
		
        # Ordre des marqueurs sur la carte.
        lappend ordre [lindex $liscarte $i]
	
        # Position (distance) du marqueur sur la carte.
        set marqueurs([lindex $liscarte $i]) $dist

	set marqueurs([lindex $liscarte $i],vrai) [lindex $listp $j]

	incr j
    }
    
}

body CGcarteVisuel::dessiner_1 {} {

    set margeH [$tasvisuel get -margeH]
    set margeV [$tasvisuel get -margeV]
    set echelleH [$tasvisuel get -echelleH]
    set echelleV [$tasvisuel get -echelleV]
    set couleurs [$tasvisuel get -couleurs]
    
    set listeCartes [$tasvisuel get -cartes]
    set nbcarte [llength $listeCartes]

    # Calcule les emplacements des �tiquettes et des marqueurs
    _calculePosEtiq

    if {$liseg != {}} {
	
	# Dessine les barres symbolisant les segments
	
	# retrouver les marqueurs extr�mit�s
	
	set lext {}
	set lid {}
	
	lappend lext 0
	set curseg [lindex $liseg 0]
	set iseg 1
	lappend lid $curseg

	foreach seg [lrange $liseg 1 end] {
	    if {$curseg != $seg} {
		lappend lext [expr $iseg - 1]
		lappend lext $iseg
		set curseg $seg
		lappend lid $curseg
	    }
	    incr iseg
	}
	
	lappend lext [expr [llength $liseg] - 1]
	
	for {set i 0} {$i < [llength $lext]} {incr i 2} {
	    set xo [positionH [lindex $ordre [lindex $lext $i]]]
	    set yo [positionV [lindex $ordre [lindex $lext $i]]]
	    
	    set xd [positionH [lindex $ordre [lindex $lext [expr $i + 1]]]]
	    set yd [positionV [lindex $ordre [lindex $lext [expr $i + 1]]]]
	    
	    set sid [lindex $lid [expr $i/2]]
	    
	    $tasvisuel create line \
		$xo $yo $xd $yd \
		-fill  [lindex $couleurs $sid] \
		-tags "$this carte seg_$sid" \
		-width 3
	    	#	-fill  [lindex $couleurs [expr int(fmod($i/2, [llength $lext] - 1))]]
	}
	
    } else {
	set xo [positionH [lindex $ordre 0]]
	set yo [positionV [lindex $ordre 0]]
	
	set xd [positionH [lindex $ordre end]]
	set yd [positionV [lindex $ordre end]]	

	# Dessine la barre symbolisant la carte
	$tasvisuel create line \
	    $xo $yo $xd $yd \
	    -fill  [$tasvisuel get -carte] \
	    -tags "$this carte" \
	    -width 3
    }
    # Dessine le symbole repr�sentant le marqueur sur la carte   

    foreach mrq $ordre {
	set xm [positionH $mrq]
	set ym [positionV $mrq]

	$tasvisuel affectdpm $mrq $xm $ym
	if { [$tasvisuel affectionppm $mrq] == 0 } {
	    $tasvisuel affectppm $mrq $xm $ym
	}
	
	# Dessine un losange
	set x1m $xm
	set y1m [expr $ym - 4]
	set x2m [expr $xm + 4]
	set y2m $ym
	set x3m $xm
	set y3m [expr $ym + 4]
	set x4m [expr $xm - 4]
	set y4m $ym

# 	$tasvisuel create polygon \
# 	    $x1m $y1m \
# 	    $x2m $y2m \
# 	    $x3m $y3m \
# 	    $x4m $y4m \
# 	    -fill  [$tasvisuel get -mrqlineoff] \
# 	    -tag "etiq$mrq lienMrq"
	
	# Dessine les traits liant les marqueurs d'une carte � l'autre
	# On saute la premi�re carte
	if {$numCarte != 0} {
	    
	    # on ne dessine la ligne uniquement si le marqueur existe
	    
	    if {[set cartePred [[lindex [$tasvisuel get -cartes] [expr $numCarte -1]] has $mrq]] != {}} {
	   
		set xmp [$cartePred positionH $mrq]
		set ymp [$cartePred positionV $mrq]
		set xm  [positionH $mrq]
		set ym  [positionV $mrq]
				
		# avec des losanges
		if {$Orientation == "horizontal"} {
		    $tasvisuel create line $xm $y1m $xmp [expr $ymp + 4] \
			-tag "etiq$mrq lienMrq" \
			-fill  [$tasvisuel get -mrqlineoff]
		} else {
		    $tasvisuel create line $x4m $y4m [expr $xmp + 4] $ymp \
			-tag "etiq$mrq lienMrq" \
			-fill  [$tasvisuel get -mrqlineoff]
		}
	    }
	}
    }
}

body CGcarteVisuel::dessiner_2 {} {

    set margeH [$tasvisuel get -margeH]
    set margeV [$tasvisuel get -margeV]
    set echelleH [$tasvisuel get -echelleH]
    set echelleV [$tasvisuel get -echelleV]
    
    
    set listeCartes [$tasvisuel get -cartes]
    set nbcarte [llength $listeCartes]   

    set xo [positionH [lindex $ordre 0]]
    set yo [positionV [lindex $ordre 0]]

    # Calcule des "constantes" pour l'affichage de:
    # - la vraisemblance
    # - la fl�che associ�e
    
    if {$Orientation == "horizontal"} {
	
	set poslasc [[lindex $listeCartes end] positionV bidon]

	if {$nbcarte == 1} {
	    set log_step [expr $echelleV - 2 * $margeV]
	} else {
	    set log_step [expr ($poslasc - $margeV) / ($nbcarte - 1)]
	}
	set log_x [expr $margeH /2]
	set log_y [expr ($log_step * $numCarte) + $margeV]

	set log_anc e
    } else {
	
	set poslasc [[lindex $listeCartes end] positionH bidon]

	if {$nbcarte == 1} {
	    set log_step [expr ($echelleH - (2 * $margeH))]
	} else {
	    set log_step [expr ($poslasc - $margeH) / ($nbcarte - 1)]
	}
	set log_x [expr ($log_step * $numCarte) + $margeH]
	set log_y [expr $margeV / 2]
	set log_anc s
    }
    
    # Affiche le log-likelywood de la carte
    # Que doit-on afficher?
    switch $RepreVraisem {
        "delta" { set texte $delta }
        "value" { set texte $score }
        "value&delta" { set texte "$score\n$delta" }
        default { puts "error: unknown mode $RepreVraisem"}
    }
    
    $tasvisuel create text \
	$log_x \
	$log_y \
	-anchor $log_anc \
	-text $texte \
	-tags "$this carte" \
	-fill [$tasvisuel get -logtxt] \
	-font [$tasvisuel get -locfont]
    
    # Dessine une fleche, du log-likelywood de la carte
    # vers le trait repr�sentant cette carte
    
    if {$Orientation == "horizontal"} {
	$tasvisuel create line \
	    [expr $log_x + 2] \
	    $log_y \
	    [expr $xo - 2] \
	    $yo \
	    -tags "$this carte" \
	    -arrow last \
	    -fill  [$tasvisuel get -carte]
	#	    -fill  [$tasvisuel get -logfle]
    } else {
	$tasvisuel create line \
	    $log_x \
	    [expr $log_y + 2] \
	    $xo \
	    [expr $yo - 2] \
	    -tags "$this carte" \
	    -arrow last \
	    -fill  [$tasvisuel get -carte]
	#	    -fill  [$tasvisuel get -logfle]
    }

}


#
#
#
body CGcarteVisuel::positionH { mrq } {
    
    set margeH [$tasvisuel get -margeH]
    set echelleH [$tasvisuel get -echelleH]
    set facteurH [$tasvisuel get -facteurH]
    set minSpace [$tasvisuel get -minSpace]
    
    if {$Orientation == "horizontal"} {
	
        if {$RepreMrq == "ratio"} {
            return [expr ($marqueurs($mrq) * $facteurH) + $margeH]
	    #return [expr ($marqueurs($mrq) * $facteurH)]
        } else {
            set numMrq [lsearch $ordre $mrq]
            #return [expr (($echelleH - (2 * $margeH)) / ([llength $ordre] - 1) * $numMrq) + $margeH]
	    return [expr ($echelleH / ([llength $ordre] - 1) * $numMrq) + $margeH]
        }
	
    } else {
        if {$RepreCarte == "ratio"} {

	    # prise en compte des cartes qui sont � la m�me position
	    # amm�liorations possibles
	    
	    if { $numCarte == 0 } {
		return [expr (-$delta * $facteurH) + $margeH]
	    } else {
		
		set cartep [lindex [$tasvisuel get -cartes] [expr $numCarte -1]]
		set deltap [$cartep get -delta]
		set Hp [$cartep positionH mrq]
		if { $delta == $deltap} {
		    return [expr $Hp + $minSpace]
		} else {		
		    return [expr  $Hp + (-$delta - -$deltap) * $facteurH]
		}
	    }
        } else {
	    
	    if {[llength [$tasvisuel get -cartes]] == 1} {
		set log_step [expr ($echelleH - (2 * $margeH))]
		
	    } else {
		set log_step [expr ($echelleH - (2 * $margeH)) / ([llength [$tasvisuel get -cartes]] - 1)]
	    }
	    
	    if {[llength [$tasvisuel get -cartes]] == 1} {
		return $margeH		
	    } else {
		return [expr ($echelleH / ([llength [$tasvisuel get -cartes]] - 1) * $numCarte) + $margeH]
		#return [expr (($echelleH - (2 * $margeH)) / ([llength [$tasvisuel get -cartes]] - 1) * $numCarte) + $margeH]
	    }
	}	
    }
}


#
#
#
body CGcarteVisuel::positionV {mrq} {

    set margeV [$tasvisuel get -margeV]
    set echelleV [$tasvisuel get -echelleV]
    set facteurV [$tasvisuel get -facteurV]
    set minSpace [$tasvisuel get -minSpace]
    
    if {$Orientation == "horizontal"} {
        if {$RepreCarte == "ratio"} {
	    
	    # prise en compte des cartes qui sont � la m�me position
	    
	    if { $numCarte == 0 } {
		return [expr (-$delta * $facteurV) + $margeV]
	    } else {
		
		set cartep [lindex [$tasvisuel get -cartes] [expr $numCarte -1]]
		set deltap [$cartep get -delta]
		set Vp [$cartep positionV mrq]
		if { $delta == $deltap} {
		    return [expr $Vp + $minSpace]
		} else {		
		    return [expr  $Vp + (-$delta - -$deltap) * $facteurV]
		}
	    }
	} else {
	   
	    if {[llength [$tasvisuel get -cartes]] == 1} {
		return $margeV		
	    } else {
		return [expr ($echelleV / ([llength [$tasvisuel get -cartes]] - 1) * $numCarte) + $margeV]
		#return [expr (($echelleV - (2 * $margeV)) / ([llength [$tasvisuel get -cartes]] - 1) * $numCarte) + $margeV]
	    }
	}
    } else {
	# orientation verticale
        if {$RepreMrq == "ratio"} {
            return [expr ($marqueurs($mrq) * $facteurV) + $margeV]
        } else {
            set numMrq [lsearch $ordre $mrq]
            return [expr (($echelleV - (2 * $margeV)) / ([llength $ordre] - 1) * $numMrq) + $margeV]
        }
    }
}

#
# "actuel" informe de la situation actuelle et peut prendre 4 valeurs
# -> 0 : pas affich�
# -> 1 : affich�e pour la carte (ttes �tiq de la carte)
# -> 2 : affich�e pour le marqueur (ttes �tiq d'un mrq)
# -> 3 : affich�e pour la carte et le marqueur
# "action" informe de l'action � mener
# -> -2 : suppression pour le marqueur
# -> -1 : suppression pour la carte
# -> +1 : ajout pour la carte
# -> +2 : ajout pour le marqueur
#
# Ajout du tag etiq$carte$mrq pour faciliter rep�rage (destruction)
#
#body CGcarteVisuel::afficheEtiq {mrq actuel action {tabCoul {}}} {
body CGcarteVisuel::afficheEtiq {mrq actuel action tabCoul} {

    switch -- $action {
        -2 {
            if {$actuel == 2} {
                $tasvisuel delete $this:$mrq:etiq
            }
        }
        -1 {
            if {$actuel == 1} {
                $tasvisuel delete $this:$mrq:etiq
            }
        }
        +1 - +2 {
            if {$actuel == 0} {
                # On affiche pour la carte ou le marqueur
                # recherche si le marqueur est d�j� s�lectionn�
                set listTagId [$tasvisuel find withtag "etiq$mrq"]
                set tagId [$tasvisuel gettags [lindex $listTagId 0]]
                set mrqPresent [lsearch $tagId "enregis"]
                
                # Que doit-on afficher sur l'�tiquette?
                switch $RepreEtiq {
                    name { set texte $mrq }
                    distance { set texte $marqueurs($mrq) }
                    name&distance {  
			if { $dstype != "order"} {
			    set texte "$mrq\n$marqueurs($mrq)" 
			} else {
			    set texte "$mrq\n$marqueurs($mrq,vrai)" 
			}
		    }
                }
                
                # Le marqueur a d�j� �t� s�lectionn�. La bordure de l'�tiquette
                # doit donc �tre de la m�me couleur que les liens.
                # Sinon couleur standard
                if {$mrqPresent != -1} {
                    upvar #1 $tabCoul tC
                    set coul $tC($mrq)
                    set tag "$this:$mrq enregis $this:info etiq$mrq info $this:$mrq:etiq"
                } else {
                    set coul  [$tasvisuel get -infotxt]
#                    set tag "$this:$mrq $this:info etiq$mrq info $this:$mrq:etiq"
                    set tag "$this:$mrq lienMrq $this:info etiq$mrq info $this:$mrq:etiq"
                }
                #set tag "$this:$mrq $this:info etiq$mrq info $this:$mrq:etiq"
                
                set xo $pos($mrq,xo)
                set yo $pos($mrq,yo)
                set xd $pos($mrq,xd)
                set yd $pos($mrq,yd)
                
                $tasvisuel create line \
                        $xo $yo $xd $yd \
		    -fill  [$tasvisuel get -infoline] \
                        -tag "$this:$mrq $this:info $this:$mrq:etiq"
                
                set T [$tasvisuel create text $xd $yd \
                        -anchor $pos(anc) \
                        -text $texte \
			   -fill  [$tasvisuel get -infotxt] \
			   -font [$tasvisuel get -locfont] \
                        -justify center \
                        -tag $tag]
                
                set coord [$tasvisuel bbox $T]
                
                set B [$tasvisuel create rectangle \
                        [expr [lindex $coord 0] - 2] \
                        [expr [lindex $coord 1] - 2] \
                        [lindex $coord 2] \
			   [lindex $coord 3] \
			   -fill  [$tasvisuel get -infobox] \
                        -outline $coul \
                        -tag "$this:$mrq $this:info etiq$mrq info $this:$mrq:etiq"]
                
                $tasvisuel lower $B $T
            }
        }
    }
}


#
#
#
body CGcarteVisuel::afficheInfo {tabCoul etiq} {

    upvar $etiq etiquette
    foreach mrq $ordre {
        if {($etiquette($this,$mrq) == 1) || ($etiquette($this,$mrq) == 3)} {
            afficheEtiq $mrq $etiquette($this,$mrq) -1 $tabCoul
            set info 0
            incr etiquette($this,$mrq) -1
        } else {
            afficheEtiq $mrq $etiquette($this,$mrq) +1 $tabCoul
            set info 1
            incr etiquette($this,$mrq) 1
       }
    }
}


#
# L'�tiquette vient d'�tre d�plac�e. On m�morise sa nouvelle position
#
body CGcarteVisuel::ajustePos {mrq x y} {

    set pos($mrq,xd) $x
    set pos($mrq,yd) $y
}

#-----------------------------------------------------------------------------
# pour acc�der � la carte pr�c�dente contenant ce marqueur.
#-----------------------------------------------------------------------------
# Param�tres : 
# - mrq : le nom du marqueur 
# Retourne :
# la r�f�rence � la carte cherch�e ou {}
#-----------------------------------------------------------------------------
body CGcarteVisuel::has {mrq} {

    if {[lsearch $ordre $mrq] == -1} {
	if {$numCarte == 0} {
	    return {}
	}
	return [[lindex [$tasvisuel get -cartes] [expr $numCarte - 1]]  has $mrq]
    } else {
	return $this
    }
}

#
# Calcule et m�morise la position des �tiquettes
# La position est m�moris�e dans le tableau pos(<nom_mrq>,<coordonn�es>)
# o� <coordonn�es> peut �tre xo, xd, yo, yd.
#
body CGcarteVisuel::_calculePosEtiq {} {

    if {$Orientation == "horizontal"} {
        set origine [positionH [lindex $ordre 0]]
        set dimMrq [expr [positionH [lindex $ordre end]] \
                - $origine]
        set yo [positionV [lindex $ordre 0]]
        set yd [expr $yo + 35]
        # 35 = distance arbitraire entre l'�tiquette et la carte
    } else {
        set origine [positionV [lindex $ordre 0]]
        set dimMrq [expr [positionV [lindex $ordre end]] \
                - $origine]
        set xo [positionH [lindex $ordre end]]
        set xd [expr $xo + 25]
        # 25 = distance arbitraire entre l'�tiquette et la carte
    }

    set dimMrq2 [expr $dimMrq / 2]

    # Premier traitement anti-chevauchement.
    # Pour �viter que les �tiquettes se chevauchent lorsque les
    # cartes sont trop pr�t.
    # De quel c�t� va t-on les afficher ?
    # Si la carte suivante est trop pr�t on affiche de l'autre
    # c�t�.

    if {$numCarte != [expr [llength [$tasvisuel get -cartes]] - 1]} {

        set carteSuiv [lindex [$tasvisuel get -cartes] [expr $numCarte + 1]]

        if {$Orientation == "horizontal"} {

            # Distance entre 2 cartes mise arbitrairement, pour
            # affichage des �tiquettes vers le bas.
            set deltaC 60

            set posSuivY [$carteSuiv positionV [lindex $ordre 0]]
            if {[expr $posSuivY - $yo] < $deltaC} {
                set yd [expr $yo - ($yd - $yo)]
                set anc s
            } else {
                set anc n
            }
        } else {

            # Distance entre 2 cartes mise arbitrairement, pour
            # affichage des �tiquettes vers la droite.
            set deltaC 45

            set posSuivX [$carteSuiv positionH [lindex $ordre 0]]
            if {[expr $posSuivX - $xo] < $deltaC} {
                set xd [expr $xo - ($xd - $xo)]
                set anc e
            } else {
                set anc w
            }
        }
    } else {
        if {$Orientation == "horizontal"} {
            set yd [expr $yo - ($yd - $yo)]
            set anc s
        } else {
            set xd [expr $xo - ($xd - $xo)]
            set anc e
        }
    }

    # On r�partit les �tiquettes sur une plage un peu plus grande
    # que celle que couvre la carte (20 pixels de plus de chaque cot�).
    
    foreach mrq $ordre {
        
        if {$Orientation == "horizontal"} {
            set pos($mrq,xo) [positionH $mrq]
            set pos($mrq,xd) [expr $pos($mrq,xo) \
                    + (((($pos($mrq,xo) - $origine) \
                    / $dimMrq2) - 1) * 20)]
            set pos($mrq,yo) $yo
            set pos($mrq,yd) $yd
            set pos(anc) $anc
        } else {
            set pos($mrq,xo) $xo
            set pos($mrq,xd) $xd
            set pos($mrq,yo) [positionV $mrq]
            set pos($mrq,yd) [expr $pos($mrq,yo) \
                    + (((($pos($mrq,yo) - $origine) \
                    / $dimMrq2) - 1) * 20)]
            set pos(anc) $anc
        }
    }

    # Deuxi�me traitement anti-chevauchement.
    # Une autre m�thode, plus lourde, pourrait etre d'afficher
    # normalement les �tiquettes puis de rechercher celles qui se
    # chevauchent et de les d�placer.
    # Il ne serait plus alors n�cessaire de m�moriser les 
    # emplacements de chaque �tiquette.

    if {$Orientation == "horizontal"} {

        # Distance entre 2 �tiquettes mise arbitrairement
        set deltaE 22

        for {set i 0} {$i < [expr [llength $ordre] - 1]} {incr i}  {
            set mrq [lindex $ordre $i]
            set mrqSuiv [lindex $ordre [expr $i + 1]]
            set diff [expr $pos($mrqSuiv,xd) - $pos($mrq,xd)]
            if {$diff < $deltaE} {
                set pos($mrqSuiv,xd) [expr $pos($mrqSuiv,xd) \
                        + ($deltaE - $diff)]
            }
        }
    } else {

        # Distance entre 2 �tiquettes mise arbitrairement
        set deltaE 15

        for {set i 0} {$i < [expr [llength $ordre] - 1]} {incr i}  {
            set mrq [lindex $ordre $i]
            set mrqSuiv [lindex $ordre [expr $i + 1]]
            set diff [expr $pos($mrqSuiv,yd) - $pos($mrq,yd)]
            if {$diff < $deltaE} {
                set pos($mrqSuiv,yd) [expr $pos($mrqSuiv,yd) \
                        + ($deltaE - $diff)]
            }
        }
    }
}

#-----------------------------------------------------------------------------
# acc�der � la valeur d'une variable.
#-----------------------------------------------------------------------------
# Param�tres : 
# - variabl : le nom d'une variable pr�fix� par -
# Retourne :
# la valeur de la variable
# Remarque :
# C'est diff�rent de cget dans la mesure o� il ne s'agit pas de configuration 
#-----------------------------------------------------------------------------
body CGcarteVisuel::get { variabl } {

    switch -- $variabl {
	-delta {return $delta}
	-value {return $score}
	-id {return $realId}
	-type {return $dstype}
	-dsid {return $dsId}
    }
}



#
# very ugly
#
body CGcarteVisuel::flipSeg {numseg} {

    set listindswp {}

    set i 0
    foreach is $liseg {
	if {$is == $numseg} {
	    lappend listindswp $i
	}
	    incr i
    }

    $tasvisuel flipSeg2 $numCarte $dsId $listindswp 
}

body CGcarteVisuel::pushUpSeg {numseg} {

    set listindshp {}

    set i 0
    foreach is $liseg {
	if {$is == $numseg} {
	    lappend listindshp $i
	}
	    incr i
    }

    if {[lsearch $liseg $numseg] == 0} {
	set numseg [lindex $liseg end]
    } else {
	set numseg [lindex $liseg [expr [lsearch $liseg $numseg] - 1]]
    }

    set listindshup {}
    set i 0
    foreach is $liseg {
	if {$is == $numseg} {
	    lappend listindshup $i
	}
	    incr i
    }

    $tasvisuel pushUpSeg2 $numCarte $dsId $listindshp  $listindshup

}