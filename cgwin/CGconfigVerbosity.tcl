#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigVerbosity.tcl,v 1.2 2002-11-07 16:59:02 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : widget quiet : impl�mentation pour les valeurs par d�faut
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#

#
# Options usuelles.
#
itk::usual CGconfigQuiet {
    keep -background -cursor -foreground
    # -modality
}

#<
#
#
#>
class CGconfigQuiet {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -quiet quiet Quiet 0
  
    common quietv

    constructor {args} {}
    public method get {arg}

}

body CGconfigQuiet::constructor {args} {
    
    itk_component add quiet {
        checkbutton $itk_interior.quiet \
	    -indicatoron 1 \
	    -variable  quietv
    } {
	usual
	
	keep -command -helpstr -balloonstr
    }
    
    eval itk_initialize $args
}

configbody CGconfigQuiet::quiet {
    
    set nbr $itk_option(-quiet)
    if {$nbr != 0 || $nbr != 1}  {
        error "wrong value \"quiet\": should be 0 or 1"
    } else {
	if {$itk_option(-quiet) == 0} {
	    $itk_component(quiet) deselect
	} else {
	    $itk_component(quiet) select
	}  
    }
}

body CGconfigQuiet::get {arg} {
    
    switch -- $arg {
        "-quiet" { return $quietv}
        default {
            error "unknown parameter $arg."
        }
    }
}
