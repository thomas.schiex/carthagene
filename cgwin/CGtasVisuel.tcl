#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGtasVisuel.tcl,v 1.15 2006-04-14 08:31:18 degivry Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Classe de gestion des cartes du tas
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

# Liste des tags employ�s et utilisation
# Les tags li�s aux d�clenchement d'�v�nements sont sans �quivoque (pas
# d'ast�risque).
# Ceux qui peuvent rev�tir plusieurs formes servent � restreindre la port�e
# de l'�v�nement.
#
# ::cGcarteVisuel* :
#     d�termine pour quelle carte on demande des info
# ::cGcarteVisuel*:$mrq :
#     d�termine quel marqueur est trait�
# ::cGcarteVisuel*:$mrq:etiq :
#     d�termine quelle �tiquette est trait�e (utile pour la destruction)
# ::cGcarteVisuel*:info :
#     permet de d�truire les info
# carte :
#     li� � la m�thode "afficheInfoCarte"
#     Clic sur une carte = demande � afficher les informations
# coulLien :
#     li� � la m�thode "choixCouleurLien" quand la roulette � 4 positions est
#     sur la position 2 ou 3.
# enregis :
#     li� � la m�thode "changeCouleurR" pour restituer les couleurs
# etiq$mrq :
#     permet de s�lectionner l'item sur lequel on vient de cliquer
# info :
#     li� aux m�thodes "pushMrq", "depMrq" et "popMrq"
#     D�placement des �tiquettes des marqueurs
# lienMrq :
#     li� aux m�thodes "changeCouleur" et "changeCouleurP"
# mrq :
#     gestion de la roulette � 4 positions sur les noms des marqueurs
#


# option add *CGtasVisuel.orientation "vertical" widgetDefault
# option add *CGtasVisuel.repreCarte "fixed" widgetDefault
# option add *CGtasVisuel.repreMrq "fixed" widgetDefault
# option add *CGtasVisuel.repreVraisem "delta" widgetDefault
# option add *CGtasVisuel.repreEtiq "name" widgetDefault
# option add *CGtasVisuel.zoomMap 100 widgetDefault
# option add *CGtasVisuel.zoomMrq 100 widgetDefault

option add *CGtasVisuel.height 500 widgetDefault
option add *CGtasVisuel.width 500 widgetDefault

#
# Options usuelles.
#
itk::usual CGtasVisuel {
    keep -background -cursor -foreground -scrollregion
}


#
#
#
class CGtasVisuel {
    inherit iwidgets::Scrolledcanvas

    itk_option define -locfont locfont LocFont "-adobe-times-bold-r-normal--12-120-75-75-p-67-iso8859-1" 

    # Liste des ref. des cartes contruites
    variable cartes {}

    # Liste des marqueurs
    variable marqueurs
    # M�morisation d'informations temporaires pour le changement de couleur
    # et le d�placement d'une �tiquette
    common CGW_Drag
    common CGtasCoul

    # Gestion cyclique des couleurs de s�lection des zones de marqueurs
    common tabCouleurs "violet pink PaleVioletRed orange coral peru sienna tomato RosyBrown goldenrod red blue NavyBlue aquamarine DarkGreen chartreuse IndianRed orange white grey"
    # couleur non visible (donnent du blanc)
    # MintCream azure AliceBlue 

    # Stockage de la couleur utilis�e pour les liens d'un marqueur
    common coulMrq

    # Indicateur de la pr�sence d'une �tiquette sur les marqueurs d'une carte
    common etiquetees

    # Compteur de position indiquant l'�tat et la couleur
    # des marqueurs et �tiquettes
    common rouletteMrq 

    # Tableau indication pr�sence d'�tiquettes
    # pour gestion selon carte et ligne de marqueur
    common etiquette

    # Stockage brut des cartes 
	
    common cartbrut {}

    common datasetype

    # liste de couleurs pour les composants
	
    common couleurs "black red blue yellow green magenta VioletRed pink moccasin NavyBlue RoyalBlue SkyBlue cyan SeaGreen black SpringGreen khaki gold firebrick orange white grey"

    # Dimensions de la zone d'affichage

    common ToileLargeur 500
    common ToileHauteur 500

    # Facteurs d'agrandissement

    common facteurH 0
    common facteurV 0

    # Marges

    common margeH 100
    common margeV 100

    # Echelles

    common echelleH 0
    common echelleV 0

    # min space between maps when same valuation

    common minSpace 200

    # la position derni�re et premi�re pour l'afichage des noms

    variable PremPosMrq
    variable DernPosMrq

    # Est-on en train de faire la mise � jour de l'affichage?
    private variable _pending ""

    #the label info
    common wli

    constructor { args } {}

    public method pushInfo { w x y }
    public method depInfo { w x y }
    public method popInfo { w x y }
    public method afficheInfoCarte {}
    public method afficheInfoToutesCartes { bool }
    public method changeCouleur { focus }
    public method _postDessiner { premiere_carte derniere_carte }
    public method dessiner {}
    public method _dessiner {}
    public method effacer {{action memorise}}
    public method rouletteMrq {mrq}
    public method choixCouleurLien1 {w}
    public method choixCouleurLien2 {w}
    public method vide {}
    public method tasser {unit nbc cb lds}
    public method charger {fic}

    #r�glage des options d'affichage par l'utilisation d'un dialog
    public method OptAffichage {}

    #r�glage des options d'affichage par l'utilisation d'un dialog
    public method ColorDial {}

    # show the font dioalog
    public method FontDial {}

    public method menuPrint {}

    public method _post {w x y}

    #affectation dans le tableau des premi�res coordonn�es d'un marqueur sur une carte
    public  method affectppm {m x y}

    #affectation dans le tableau des derni�res coordonn�es d'un marqueur sur une carte
    public  method affectdpm {m x y}

    #affectation des coordonn�es?

    public  method affectionppm {m}

    private variable _posted ""      ;# List of tags at posted menu position

    private variable cur_map {}

    # to enable menu command to get information on
    # the clicked item
    private variable lisTagMemo {}
    
    # pour acc�der � des variables
    public method get { variabl }

    # to size the canvas by considering the size of the markers
    # (vertically orientation) and no limit 
    public method MaxY {}

    # to size the canvas by considering the size of the markers
    # (horizontally orientation) and no limit
    public method MaxX {}

    public method provideMapInfo {}

    public method eraseMapInfo {}

    public method flipSeg {}
    public method pushUpSeg {}

    #retourne un seg.  
    public method flipSeg2 {nc nj li}

    #pousse un seg.  
    public method pushUpSeg2 {nc nj li lu}

    #manage the map menu.  
    public method MapMenu {w x y}

    itk_option define -wli wli Wli "" {
        set wli $itk_option(-wli)
    }
}

#
#
#
proc cgtasvisuel { pathName args } {
    uplevel CGtasVisuel $pathName $args
}


#
#
#
body CGtasVisuel::constructor { args } {

    eval itk_initialize $args

    # NOTE: une zone de marqueur est constitu�e de l'ensemble
    # - nom du marqueur (texte)
    # - fl�che reliant le nom du marqueur
    #   � sa position sur la carte voisine
    # - lignes reliant un meme marqueur d'une carte � la carte voisine
    # - symboles repr�sentant la position du marqueur sur les cartes

    # Le curseur de la souris entre dans une zone de marqueur
    bind lienMrq <Enter> \
	"$this changeCouleur Enter"

    # Le curseur de la souris sort d'une zone de marqueur
    bind lienMrq <Leave> \
            "$this changeCouleur Leave"

    # Roulette � quatre positions : quand on clic sur le nom d'un marqueur
    # les liens qui les relient d'une carte � une autre peuvent appara�tre
    # ou dispaitre ainsi que les �tiquettes.
    bind mrq <Button-1> "$this rouletteMrq _NULL_"
        
    # Clic sur un marqueur = affiche menu popup de choix de couleur
    #    bind coulLien <Shift-Button-1> bind coulLien <Button-1> \
            "$this choixCouleurLien1 %W"
    bind coulLien <Button-1> \
            "$this choixCouleurLien1 %W"

    
    # To popup a dedicated menu on map bar
    bind carte <Button-1> \
	"$this MapMenu %W %x %y"

    bind carte <Enter> "$this provideMapInfo"
    bind carte <Leave> "$this eraseMapInfo"

    # D�placement des �tiquettes d'information
    bind info <Button-1> \
            "$this pushInfo %W %x %y"

    bind info <B1-Motion> \
            "$this depInfo %W %x %y"

    bind info <ButtonRelease-1> \
            "$this popInfo %W %x %y"

    #
    # Ajout des menus d�di� au tas et d�di�s aux cartes
    #
    itk_component add tasMenu {
	menu $itk_component(canvas).tasmenu \
	    -tearoff 0 \
    } {
	usual
	ignore -tearoff
	rename -cursor -menucursor menuCursor Cursor
    }
    
    itk_component add cartMenu {
	menu $itk_component(canvas).cartmenu \
	    -tearoff 0 \
    } {
	usual
	ignore -tearoff
	rename -cursor -menucursor menuCursor Cursor
    } 
   
    $itk_component(cartMenu) add command -label "Push Up" \
    	-command "$this pushUpSeg"

    $itk_component(cartMenu) add command -label "Flip" \
    	-command "$this flipSeg"

    set w [winfo parent [namespace tail $this]]

    $itk_component(tasMenu) add command -label "Graphic..." \
	-command "$this OptAffichage"
    
    
    $itk_component(tasMenu) add command -label "Colors..." \
	-command "$this ColorDial"
    
   $itk_component(tasMenu) add command -label "Fonts..." \
	-command "$this FontDial"

    $itk_component(tasMenu) add command -label "Print..." \
	-command "$this menuPrint"

    uplevel #0 "bind $itk_component(canvas) <Button-3> \"$this _post %W %x %y\""

    itk_component add optAffichage {
	cgoptaffichage $itk_interior.optAffichage \
	    -modality application
	#     -mode [$this cget -orientation] \
	    # 	    -repreCarte [$this cget -repreCarte] \
	    # 	    -repreMrq [$this cget -repreMrq] \
	    # 	    -repreVraisem [$this cget -repreVraisem] \
	    # 	    -repreEtiq [$this cget -repreEtiq] \
	    # 	    -zoomMrq [$this cget -zoomMrq] \
	    # 	    -zoomMap [$this cget -zoomMap]
    }

    itk_component add colorDialog {
	colordialog $itk_interior.colorDialog \
	    -modality application \
	    -couleurs $couleurs
    }

    itk_component add pcd {
	cgcanvasprintdialog $itk_interior.pcd \
	    -modality application
    }
    
    $itk_component(pcd) setcanvas $itk_component(canvas)
}

# m�thode prot�g�e(inspiration Hirarchy): _post x y
#
# Si la souris se trouve sur un item, 
# c'est celui le menu de la carte qui est affich� (provisoire)
# sinon c'est celui du tas.
# ----------------------------------------------------------------------
body CGtasVisuel::_post {w x y} {

    set rx [expr [winfo rootx $itk_component(canvas)]+$x]
    set ry [expr [winfo rooty $itk_component(canvas)]+$y]
    
    if {[$itk_component(canvas) find overlapping $x $y $x $y] != {}} {
	
	# r�cup�ration de l'identificateur de la zone graphique courante
	set tagId [find withtag current]
	
	# liste des tags de l'identificateur
	set listTag [gettags $tagId]

	puts $listTag
	
	# on recherche celui qui commence par "::cGcarteVisuel*"
	set iTag [lsearch $listTag ::cGcarteVisuel*]
	
	# ce tag est le nom de la carte qui a re�u l'�v�nement (clic souris)
	set cur_map [lindex $listTag $iTag]
	
 	tk_popup $itk_component(cartMenu) $rx $ry
    } else {	
 	tk_popup $itk_component(tasMenu) $rx $ry
    }
}

# M�thode appel�e par tas::dessiner et par l'�v�nement <Ctrl-B1> sur marqueur
# L'�v�nement fait un appel sans param�tre d'ou _NULL_ par d�faut
#
#body CGtasVisuel::rouletteMrq { {mrq _NULL_ } } {
body CGtasVisuel::rouletteMrq {mrq} {

    if {$mrq == "_NULL_"} {
        # la m�thode vient d'etre appel�e par l'�v�nement d�clanch�
        # par <Control-Button-1> sur le nom d'un marqueur

        set tagId [find withtag current]

        # parmi les tags de l'objet courant 
        # on recherche l'indice de celui qui commence par "etiq*"
        set iTag [lsearch [set listTag [gettags $tagId]] etiq*]

        # on r�cup�re le tag en question
        set tagH [lindex $listTag $iTag]
        regsub "etiq" $tagH "" mrq

        # on recherche la liste des objets ayant ce tag
        set listTagId [find withtag $tagH]
    } else {
        # Appel depuis tas::dessiner
        set listTagId [find withtag etiq$mrq]
    }

    # on recherche le num�ro de tag le plus petit
    # et qui a donc le niveau d'affichage le plus bas
    set plusPetitTag [lindex [find all] 0]

    set rouletteMrq($mrq) [expr ($rouletteMrq($mrq) + 1) % 4]

    switch $rouletteMrq($mrq) {
        0 {
            set couleur [$itk_component(colorDialog) cget -mrqlineoff]
            foreach carte $cartes {
                $carte afficheEtiq $mrq $etiquette($carte,$mrq) -2 coulMrq
                incr etiquette($carte,$mrq) -2
            }
        }
        1 {
            # Recherche la couleur du fond (effet de transparence)
            set couleur [cget -background]
        }
        2 {
            set couleur $coulMrq($mrq)
        }
        3 {
            foreach carte $cartes {
                $carte afficheEtiq $mrq $etiquette($carte,$mrq) +2 coulMrq
                incr etiquette($carte,$mrq) 2
            }
        } 
    }

    foreach tagId $listTagId {
        switch [type $tagId] {
            line {
                if {$rouletteMrq($mrq) != 3} {
                    itemconfigure $tagId -fill $couleur
                    lower $tagId $plusPetitTag
                }
            }
            polygon {
                itemconfigure $tagId -fill [$itk_component(colorDialog) cget -mrqlineoff]
            }
	    rectangle {
                itemconfigure $tagId -outline [$itk_component(colorDialog) cget -infotxt]
	    }
            default {}
        }
        if {($rouletteMrq($mrq) == 0)} {
            addtag "lienMrq" withtag $tagId
            dtag $tagId "coulLien"
        } else {
            dtag $tagId "lienMrq"
            addtag "coulLien" withtag $tagId
        }
    }
}


#
# D�placement d'une �tiquette: clic maintenu sur le texte.
#
# La premi�re id�e �tait, lors de la s�lection d'un item,
# de supprimer la ligne et le texte qui composent les �tiquettes
# d'un marqueur sur une carte, puis de d�placer le rectangle.
# Impossible car la destruction d'un item provoque l'arr�t du
# processus, comme si le rectangle �tait d�s�lectionn�.
# On en revient donc � la m�thode la plus lourde qui consiste
# � tout d�placer (ligne, texte et rectangle).
#
body CGtasVisuel::pushInfo { w x y } {

    # r�cup�ration de l'identificateur de la zone graphique courante
    # la valeur est identique aux $w $x $y (ou presque)->
    # il ne faut pas se fier au couple ($x $y) car cela peut �tre :
    # - la ligne (qui relie le texte au marqueur sur la carte)
    # - le rectangle (valeur recherch�e)
    # - le texte (la distance du marqueur sur la carte)
    # - n'importe quel �l�ment plac� sous un des trois pr�c�dents.

    set tagId [find withtag current]

    # liste des tags de l'identificateur
    set listTag [gettags $tagId]

    # on recherche le tag qui contient le nom d'un marqueur
    foreach mrq $marqueurs {
	set res [lsearch $listTag ::cGcarteVisuel*:$mrq]
	if {$res != -1} {
	    set infoMarq [lindex $listTag $res]
	}
    }

    # on recherche la liste des objets ayant ce tag
    set listTagId [find withtag $infoMarq]

    # on m�morise les info sur les trois �l�ments
    set CGW_Drag(x) $x
    set CGW_Drag(y) $y
    foreach tagId $listTagId {
	set typeTagId [type $tagId]
	set CGW_Drag($typeTagId) $tagId
    }
	
}


#
# D�placement d'une �tiquette: d�placement de la souris dont le bouton est 
# maintenu enfonc�.
#
body CGtasVisuel::depInfo { w x y } {

    set dx [expr $x - $CGW_Drag(x)]
    set dy [expr $y - $CGW_Drag(y)]
    $w move $CGW_Drag(rectangle) $dx $dy
    $w move $CGW_Drag(text) $dx $dy

    set coordLine [$w coord $CGW_Drag(line)]
    $w coords $CGW_Drag(line) \
	[lindex $coordLine 0] \
	[lindex $coordLine 1] \
	[expr [lindex $coordLine 2] + $dx] \
	[expr [lindex $coordLine 3] + $dy]
    
    set CGW_Drag(x) $x
    set CGW_Drag(y) $y
    
}


#
#
#
body CGtasVisuel::popInfo { w x y } {

    set tagId [find withtag current]

    # Recherche les coordonn�es d'�tiquette
    set coord [$w coord $tagId]

    # liste des tags de l'identificateur
    set listTag [gettags $tagId]

    # on recherche celui qui commence par "::cGcarteVisuel*:info"
    set iTag [lsearch $listTag ::cGcarteVisuel*:info]

    # ce tag est le nom de la carte qui a re�u l'�v�nement (clic souris)
    regsub :info [lindex $listTag $iTag] "" carte

    # on recherche celui qui commence par "etiq*"
    set iTag [lsearch $listTag etiq*]

    # ce tag est le nom du marqueur qui a re�u l'�v�nement (clic souris)
    regsub etiq [lindex $listTag $iTag] "" mrq

    $carte ajustePos $mrq [lindex $coord 0] [lindex $coord 1]

}


#
#
#
body CGtasVisuel::afficheInfoCarte {} {

    # r�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]

    # liste des tags de l'identificateur
    set listTag [gettags $tagId]

    # on recherche celui qui commence par "::cGcarteVisuel*"
    set iTag [lsearch $listTag ::cGcarteVisuel*]

    # ce tag est le nom de la carte qui a re�u l'�v�nement (clic souris)
    set carte [lindex $listTag $iTag]

    # on demande � la carte d'afficher/effacer les distances des marqueurs
    $carte afficheInfo coulMrq etiquette
}

body CGtasVisuel::provideMapInfo {} {

    # r�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]

    # liste des tags de l'identificateur
    set listTag [gettags $tagId]

    # on recherche celui qui commence par "::cGcarteVisuel*"
    set iTag [lsearch $listTag ::cGcarteVisuel*]

    # ce tag est le nom de la carte qui a re�u l'�v�nement (clic souris)
    set carte [lindex $listTag $iTag]

    # if this is related to a segment
    set iTag [lsearch $listTag seg_*]

    set infochrom ""
    if {$iTag != -1} {
	set seg [lindex $listTag $iTag]
	set chromnum [string range $seg 4 end]
	set infochrom ",Segment : $chromnum"
    }
    
    # on demande � la carte d'afficher/effacer les distances des marqueurs
    set score [$carte get -value]
    set id [$carte get -id]
    set type [$carte get -type]
    set dsid [$carte get -dsid]
    
    
    $wli configure -text "Id : $id, Type : $type ($dsid), Score : $score$infochrom"
}

body CGtasVisuel::eraseMapInfo {} {

   $wli configure -text ""
}

#
#
#
body CGtasVisuel::afficheInfoToutesCartes {bool} {

    foreach carte $cartes {
        if {[$carte cget -info] != $bool} {
            $carte afficheInfo coulMrq etiquette
        }
    }
}


#
# C'est � partir du "focus" que l'on peut d�terminer si la souris 
# entre sur un zone "sensible" ou si elle en sort.
#
body CGtasVisuel::changeCouleur {focus} {

    # R�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]

    # Parmi les tags de l'objet courant 
    # on recherche l'indice de celui qui commence par "etiq"
    set iTag [lsearch [set listTag [gettags $tagId]] etiq*]

    # On r�cup�re le tag en question
    set tagH [lindex $listTag $iTag]

    # On recherche la liste des objets ayant ce tag
    set listTagId [find withtag $tagH]

    if {$focus == "Leave"} {
        set coulRec [$itk_component(colorDialog) cget -infotxt]
        set couleur $CGtasCoul
        set CGtasCoul [$itk_component(colorDialog) cget -mrqlineoff]
        #set couleur [$itk_component(colorDialog) cget -mrqLineOff]
    } else {
        # On m�morise la couleur en cours
        foreach tagId $listTagId {
            if {[type $tagId] == "line"} {
                set CGtasCoul [itemcget $tagId -fill]
                break
            }
        }
        set coulRec [$itk_component(colorDialog) cget -mrqlineon]
	set couleur [$itk_component(colorDialog) cget -mrqlineon]
    }

    # On modifie la couleur de ces objets
    foreach tagId $listTagId {

	if {[lsearch -regexp [gettags $tagId] "mrq|info"] == -1} {
	    itemconfigure $tagId -fill $couleur
	}

	if {[type $tagId] == "rectangle"} {
	    itemconfigure $tagId \
		-outline $coulRec
	}
    }
    
}


#
#
#
body CGtasVisuel::choixCouleurLien1 {w} {

    # R�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]

    if {![winfo exists $w.cCL]} {
        cgcolordialog $w.cCL
    }
    $w.cCL configure -couleurs $tabCouleurs -defaut red -modality application


    if {[$w.cCL activate]} {
        set couleur [$w.cCL get]

        # Parmi les tags de l'objet courant 
        # on recherche l'indice de celui qui commence par "etiq"
        set iTag [lsearch [set listTag [gettags $tagId]] etiq*]

        # On r�cup�re le tag en question
        set tagH [lindex $listTag $iTag]

        # Nom du marqueur et enregistrement de la couleur
        regsub "etiq" $tagH "" mrq
        set coulMrq($mrq) $couleur

        # On recherche la liste des objets ayant ce tag
        set listTagId [find withtag $tagH]

        foreach tagId $listTagId {
            switch [type $tagId] {
                text {}
                rectangle {
                    itemconfigure $tagId -outline $couleur
                }
                line {
                    itemconfigure $tagId \
                            -fill $couleur
                }
            }
        }
    }
#    delete object $w.cCL
    destroy $w.cCL
}


#
#
#
body CGtasVisuel::choixCouleurLien2 {w} {

    # R�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]

    dialogshell $w.ds -modality application
    $w.ds add annuler -text "X" -command {$w.ds deactivate 0}
    $w.ds default annuler

    set win [$w.ds childsite]

    buttonbox $win.bb -orient vertical -pady 0

    foreach couleur $tabCouleurs {
        $win.bb add $couleur \
                -height 20 \
                -width 20 \
                -background $couleur \
                -activebackground $couleur \
                -defaultring yes \
                -defaultringpad 0 \
                -command [code $w.ds deactivate $couleur]
    }
    bind $w.ds <Escape> {$w.ds deactivate 0}

    pack $win.bb

    set couleur [$w.ds activate]
    if { $couleur != 0} {

        # Parmi les tags de l'objet courant 
        # on recherche l'indice de celui qui commence par "etiq"
        set iTag [lsearch [set listTag [gettags $tagId]] etiq*]

        # On r�cup�re le tag en question
        set tagH [lindex $listTag $iTag]

        # Nom du marqueur et enregistrement de la couleur
        regsub "etiq" $tagH "" mrq
        set coulMrq($mrq) $couleur

        # On recherche la liste des objets ayant ce tag
        set listTagId [find withtag $tagH]

        foreach tagId $listTagId {
            switch [type $tagId] {
                text {}
                rectangle {
                    itemconfigure $tagId -outline $couleur
                }
                line {
                    itemconfigure $tagId \
                            -fill $couleur
                }
            }
        }
    }

#    delete object $w.ds
    destroy $w.ds
}


#
#
#
body CGtasVisuel::_postDessiner {premiere_carte derniere_carte} {

    set Orientation [$itk_interior.optAffichage cget -mode]

    # d�termination de l'ordre

    proc xcomp {a b} {
	return [expr [lindex $a 1] < [lindex $b 1]]	
    }

     proc ycomp {a b} {
	return [expr [lindex $a 2] < [lindex $b 2]]	
    }

    foreach mrqi [array names PremPosMrq] {
	lappend prem_ordre [linsert $PremPosMrq($mrqi) 0 $mrqi]	
    }
    
    foreach mrqi [array names DernPosMrq] {
	lappend dern_ordre [linsert $DernPosMrq($mrqi) 0 $mrqi]	
    }

    unset PremPosMrq
    unset DernPosMrq

    if {$Orientation == "horizontal"} {
	set prem_ordre [lsort -command xcomp $prem_ordre]
	set dern_ordre [lsort -command xcomp $dern_ordre]
    } else {
	set prem_ordre [lsort -command ycomp $prem_ordre]
	set dern_ordre [lsort -command ycomp $dern_ordre]	
    } 
    # Affichage nom marqueurs et fl�che premier cot�
    # - gauche en mode vertical
    # - haut en mode horizontal
    set nbmrq [llength $prem_ordre]

    set xo [lindex [lindex $prem_ordre 0] 1]
    set yo [lindex [lindex $prem_ordre 0] 2]

    set xd [lindex [lindex $prem_ordre end] 1]
    set yd [lindex [lindex $prem_ordre end] 2]
    
    # Calcul de la position du nom du marqueur et du pas

    if {$Orientation == "horizontal"} {
	set nm_step [expr ($xd - $xo) / ($nbmrq - 1)]
	set xnm $xo
	set ynm [expr $margeV / 2]
	set nm_anc s
    } else {
	set nm_step [expr ($yd - $yo) / ($nbmrq - 1)]
	set xnm [expr $margeH / 2]
	set ynm $yo
	set nm_anc e
    } 

    # to get the position of the previous locus
    set iimrq 0

    foreach imrq $prem_ordre {

	# affiche le nom du marqueur
	set mrq [lindex $imrq 0]

	if {$Orientation == "horizontal"} {

	    #the position depends on the position
	    # of the previous loci layered
	    
	    set cit [create text 0 0 -text $mrq -font $itk_option(-locfont)]
	    set cml [expr [lindex [bbox $cit] 2] - [lindex [bbox $cit] 0]]
	    delete $cit
	    if { $iimrq != 0 } {
		set ml [expr [lindex [bbox $it] 2] - [lindex [bbox $it] 0]]
		set xnm [expr $xnm - $ml/2.0 - $cml/2.0 - (($mh * [$itk_interior.optAffichage cget -zoomMrq]) / 100.0)]
		delete $cit
	    } else {
		set xnm [expr $xnm - $cml/2]
	    }
	}

	set it [create text \
		    $xnm \
		    $ynm \
		    -anchor $nm_anc \
		    -justify center \
		    -text $mrq \
		    -tag "etiq$mrq mrq lienMrq" \
		    -fill [$itk_component(colorDialog) cget -mrqtxt] \
		    -font $itk_option(-locfont)]
	
	set mh [expr [lindex [bbox $it] 3] - [lindex [bbox $it] 1]]
	
	# Dessine une fleche, du nom du marqueur 
	# vers sa position sur la premi�re carte
	    
	set xm [lindex $imrq 1]
	set ym [lindex $imrq 2]

	if {$Orientation == "horizontal"} {
	    create line \
		$xnm \
		[expr $ynm + 2] \
		$xm \
		[expr $ym - 2] \
		-arrow last \
                -fill [$itk_component(colorDialog) cget -mrqlineoff] \
		-tag "etiq$mrq lienMrq"
	} else {
	    create line \
		[expr $xnm + 2] \
		$ynm \
		[expr $xm - 2] \
		$ym \
		-arrow last \
                -fill [$itk_component(colorDialog) cget -mrqlineoff] \
		-tag "etiq$mrq lienMrq"
	}

	if {$Orientation == "horizontal"} {
	    #old style
	    #set xnm [expr $xnm + $nm_step]
	} else {
	    set ynm [expr $ynm + $nm_step]
	}

	incr iimrq

    }	

    # Affichage nom marqueurs et fl�ches deuxi�me cot�
    # - droite en mode vertical
    # - bas en mode horizontal
    set nbmrq [llength $dern_ordre]

    set xo [lindex [lindex $dern_ordre 0] 1]
    set yo [lindex [lindex $dern_ordre 0] 2]

    set xd [lindex [lindex $dern_ordre end] 1]
    set yd [lindex [lindex $dern_ordre end] 2]

    # Calcul de la position du nom du marqueur et du pas

    if {$Orientation == "horizontal"} {
	set nm_step [expr ($xd - $xo) / ($nbmrq - 1)]
	set xnm $xo
	set ynm [expr $yd + ($margeV / 2)]
	set nm_anc n
    } else {
	set nm_step [expr ($yd - $yo) / ($nbmrq - 1)]
	set xnm [expr $xd + ($margeH / 2)]
	set ynm $yo
	set nm_anc w
    }

    # to get the position of the previous locus
    set iimrq 0

    foreach imrq $dern_ordre {

	# affiche le nom du marqueur
	set mrq [lindex $imrq 0]

	if {$Orientation == "horizontal"} {

	    #the position depends on the position
	    # of the previous loci layered
	    
	    set cit [create text 0 0 -text $mrq -font $itk_option(-locfont)]
	    set cml [expr [lindex [bbox $cit] 2] - [lindex [bbox $cit] 0]]
	    delete $cit
	    if { $iimrq != 0 } {
		set ml [expr [lindex [bbox $it] 2] - [lindex [bbox $it] 0]]
		set xnm [expr $xnm - $ml/2.0 - $cml/2.0 - (($mh * [$itk_interior.optAffichage cget -zoomMrq]) / 100.0)]
		delete $cit
	    } else {
		set xnm [expr $xnm - $cml/2]
	    }
	}

	create text \
	    $xnm \
	    $ynm \
	    -anchor $nm_anc \
	    -justify center \
	    -text $mrq \
	    -tag "etiq$mrq mrq lienMrq" \
	    -fill [$itk_component(colorDialog) cget -mrqtxt] \
	    -font $itk_option(-locfont)

	set mh [expr [lindex [bbox $it] 3] - [lindex [bbox $it] 1]]

	# Dessine une fl�che, du nom du marqueur 
	# vers sa position sur la derni�re carte
	set xm [lindex $imrq 1]
	set ym [lindex $imrq 2]

	if {$Orientation == "horizontal"} {
	    create line \
		$xnm \
		[expr $ynm - 2] \
		$xm \
		[expr $ym + 2] \
		-arrow last \
                -fill [$itk_component(colorDialog) cget -mrqlineoff] \
		-tag "etiq$mrq lienMrq"
	} else {
	    create line \
		[expr $xnm - 2] \
		$ynm \
		[expr $xm + 2] \
		$ym \
		-arrow last \
                -fill [$itk_component(colorDialog) cget -mrqlineoff] \
		-tag "etiq$mrq lienMrq"
	}

	if {$Orientation == "horizontal"} {
	    # old style
	    #set xnm [expr $xnm + $nm_step]
	} else {
	    set ynm [expr $ynm + $nm_step]
	}

	incr iimrq
    }	
}

    
#
# Dessine le tas lorsque l'application est inactive
# L'activit� est engendr�e par l'envoie de messages, ici des instructions
# pour les options d'affichage. On attend donc que tous les messages soient
# arriv�s pour proc�der � l'affichage.
# Dans l'ordre, le premier message entraine un affichage retard�. Les messages
# suivants sont supprim�s mais la modification des param�tres est enregistr�e
# et sera prise en compte lorsque l'affichage se fera.
#
body CGtasVisuel::dessiner {} {

    if {$_pending != ""} {
        return
    }
    _dessiner

#    set _pending [after idle [code $this _dessiner]]
}


#
# Dessine la partie centrale de la repr�sentation du tas, c'est � dire
# les cartes les marqueurs et les liens qui les relient
#
body CGtasVisuel::_dessiner {} {
    
    set _pending ""

    configure -cursor watch

    set maxlong 0.0

    # Calcul du nombre de cartes, en faire une fonction
    set nbCartes 0
    foreach mp $cartbrut {
	incr nbCartes [expr [llength  $mp] - 2]
    }
    
    # R�cup�re le score de la meilleure carte
    set scoreMax [lindex [lindex $cartbrut 0] 1]
    
    # Construction de la liste des cartes
    set marqueurs {}

    set lidj {}
    set ltype {}
    set lrid {}

    foreach mp $cartbrut {
	# � exploiter + tard
	set idm [lindex $mp 0]

	set scm [lindex $mp 1]
	
	set lm  [lrange $mp 2 end]
	
	
	# we need a first pass to find out the longest map
	# in case we need to normalize the order that 
	#does not have a length
	foreach la_carte $lm {
	    # is of the dataset
	    set idj [lindex $la_carte 0]
	    # type of map
	    set dst $datasetype($idj)
	    
	    if { $dst != "order"} {
		set long [lindex $la_carte end]
		
		if {$long > $maxlong} {
		    set maxlong $long
		}
	    }
	}

	foreach la_carte $lm {
	    # to store the real id

	    lappend lrid $idm

	    # is of the dataset
	    set idj [lindex $la_carte 0]
	    # type of map
	    set dst $datasetype($idj)
	  
	    lappend lidj $idj
	    lappend ltype $dst	    
	    
	    # chromosome info expunged in case
	    
	    set lao_carte {}
	    set liseg {}
	    set listp {}

	    #remplissage de la liste des marqueurs
	    if { $dst == "order"} {
		set ii 3
		#passe pour collecter des infos
		set preseg -1
		set j 0
		
		for {set i 2} {$i < [llength $la_carte]} {incr i $ii} {

		    lappend listp [lindex $la_carte [expr $i + 1]]
		    
		    set seg [lindex $la_carte [expr $i + 2]]
		    if {$seg != $preseg} {
			set oinf($seg,card) 1
			set oinf($seg,max) [lindex $la_carte [expr $i + 1]]
			set oinf($seg,dernier) [lindex $la_carte [expr $i]]
			set oinf($seg,min) [lindex $la_carte [expr $i + 1]]
			set oinf($seg,premier) [lindex $la_carte [expr $i]]
			set oinf($seg,num) $j
			set oinf($j,idc) $seg
			if {[lindex $la_carte [expr $i + 1]] < [lindex $la_carte [expr $i + 4]]} {
			    set oinf($seg,sens) 0
			} else {
			    set oinf($seg,sens) 1 
			}
			
			incr j
		    } else {
			set oinf($seg,card) [expr $oinf($seg,card) + 1]
			if {[lindex $la_carte [expr $i + 1]] < $oinf($seg,min)} {
			    set oinf($seg,min) [lindex $la_carte [expr $i + 1]]
			    set oinf($seg,premier) [lindex $la_carte [expr $i]]
			}
			
			if {[lindex $la_carte [expr $i + 1]] > $oinf($seg,max)} {
			    set oinf($seg,max) [lindex $la_carte [expr $i + 1]]
			    set oinf($seg,dernier) [lindex $la_carte [expr $i]]
			}
			
		    }
		    set preseg $seg
		 
		}

		set nbc [expr $j]
		set tt 0
		set nbmonc 0
		
		for {set i 0} {$i < $nbc} {incr i 1} {
		    set tt [expr $tt + $oinf($oinf($i,idc),max) - $oinf($oinf($i,idc),min)]
		    set nbmonc [expr $nbmonc + $oinf($oinf($i,idc),card)]
		    
                    if {$nbmonc == $nbc} {
			set distba [expr round($tt / 1)]
		    } else {
			set distba [expr round($tt / ($nbmonc - $nbc))]
		    }
		    set ttt [expr ($distba * ($nbc - 1)) + $tt]
                    if {$ttt == 0} {
			set rat [expr $maxlong / 1]
		    } else {
			set rat [expr $maxlong / $ttt]
		    }
		}
		    
	    } else {
		set ii 2
	    }

	    if { $dst == "order"} {
		set starter [expr 0.0 - $distba]
		set tp 0.0
		set cp -1
	    }

	    for {set i 2} {$i < [llength $la_carte]} {incr i $ii} {
		if { [lsearch $marqueurs [set mrq [lindex $la_carte $i]]] == -1} {
		    lappend marqueurs $mrq
		}
		
		if { $dst == "order"} {
		    lappend liseg [lindex $la_carte [expr $i + 2]]
		    #the name
		    lappend lao_carte [lindex $la_carte $i]
		    #the pos normalized to the longest
		    #set im [expr ($i - 2)/$ii]
		    #set nbm [expr ([llength $la_carte] - 2)/$ii]
		    #set pos [expr $im * $maxlong / ($nbm - 1)]

		    if {$cp != [lindex $la_carte [expr $i + 2]]} {
			set starter [expr $starter + $distba + $tp]
			set cp [lindex $la_carte [expr $i + 2]]
			set tp [expr $oinf($cp,max) - $oinf($cp,min)]
		    }
		   

		    if {$oinf([lindex $la_carte [expr $i + 2]],sens) == 0} {

			set pospba [ expr [lindex $la_carte [expr $i + 1]] - $oinf([lindex $la_carte [expr $i + 2]],min) + $starter]

		    } else {
			set pospba [ expr  $oinf([lindex $la_carte [expr $i + 2]],max) - [lindex $la_carte [expr $i + 1]] + $starter]
		    }
		    set pos [expr $pospba * $rat]
		    
		    lappend lao_carte $pos
		}
	    }
	    
	    set sco [lindex $la_carte 1]

	    if { $dst == "order"} {
		set la_carte $lao_carte
	    } else {
		set la_carte [lreplace $la_carte 0 1]
	    }
	    
	    lappend la_carte $scoreMax
	    lappend la_carte $scm
		lappend liste_carte [list $la_carte $liseg $listp]
	}
    }
    
    set scoreMin [lindex $la_carte end]
    
    if {[$itk_interior.optAffichage cget -mode] == "horizontal"} {


	# redimenssionnement dynamique et � la louche
	# de la zone, il serrait possible de prende
	# en compte la fonte

	# set ToileLargeur [expr [llength $marqueurs] * 100]
	set ToileLargeur [MaxX]
 
	set ToileHauteur [expr $nbCartes * 100]

	# Les cartes sont repr�sent�es par un trait horizontal
        #
	# Calcul du facteur d'agrandissement horizontal
        set echelleH [expr ($ToileLargeur * [$itk_interior.optAffichage cget -zoomMrq]) / 100.0]

	
	set echelleH $ToileLargeur
	#set $echelleH $ToileLargeur
	#set distMinMax [expr $echelleH - (2 * $margeH)]
	#set distMinMax [expr $echelleH - $margeH]
	set distMinMax $echelleH
	set facteurH [expr $distMinMax / $maxlong]

	# Calcul du facteur d'agrandissement vertical
        set echelleV [expr ($ToileHauteur * [$itk_interior.optAffichage cget -zoomMap]) / 100.0]
	set distMinMax [expr $echelleV - (2 * $margeV)]

	if {$scoreMax == $scoreMin} {
		set facteurV $distMinMax
	} else {
		set facteurV [expr $distMinMax / ($scoreMax - $scoreMin)]
	}

    } else {

	# redimenssionnement dynamique et � la louche
	# de la zone, il serrait possible de prende
	# en compte la fonte

	set ToileLargeur [expr $nbCartes * 100]
 
	#set ToileHauteur [expr [llength $marqueurs] * 50 + 100]

	set ToileHauteur [MaxY]

	# Les cartes sont repr�sent�es par un trait vertical
        #
	# Calcul du facteur d'agrandissement horizontal
        set echelleH [expr ($ToileLargeur * [$itk_interior.optAffichage cget -zoomMap]) / 100.0]

	#set distMinMax [expr $echelleH - (2 * $margeH)]
	set distMinMax $echelleH

	if {$scoreMax == $scoreMin} {
		set facteurH $distMinMax
	} else {
		set facteurH [expr $distMinMax / ($scoreMax - $scoreMin)]
	}

	# Calcul du facteur d'agrandissement vertical
        set echelleV [expr ($ToileHauteur * [$itk_interior.optAffichage cget -zoomMrq]) / 100.0]
	set distMinMax [expr $echelleV - (2 * $margeV)]
	set facteurV [expr $distMinMax / $maxlong]

    }

    if {$cartes != ""} {
	set cartes {}
	set premier_passage  0
    } else {
	set premier_passage 1
        for {set i 0} {$i < $nbCartes} {incr i} {
            set etiquetees($i) 0
        }
    }

    set indice 0
    
    foreach carteseg $liste_carte {

	set carte [lindex $carteseg 0]
	set lis [lindex $carteseg 1]
	set listp [lindex $carteseg 2]
	set type [lindex $ltype $indice]
	set rid [lindex $lrid $indice]
	set dsi [lindex $lidj $indice]
	set une_carte [cgcartevisuel ::\#auto $this $indice $rid $type $dsi $carte \
			   [$itk_interior.optAffichage cget -mode] \
			   [$itk_interior.optAffichage cget -repreCarte] \
			   [$itk_interior.optAffichage cget -repreMrq] \
			   [$itk_interior.optAffichage cget -repreVraisem] \
			   [$itk_interior.optAffichage cget -repreEtiq] \
			   $lis \
			   $listp]
	
	if {$indice == 0} {
	    set premiere_carte $une_carte
	}
	
	lappend cartes $une_carte

	set etiquetees($indice) 0

        incr indice
    }

    set derniere_carte $une_carte

    # set marqueurs [$une_carte cget -ordre]

    # Les cartes ne sont pas dessin�es dans la boucle pr�c�dente
    # car il manque des informations
    foreach carte $cartes {
        $carte dessiner_1
	$carte dessiner_2
    }

    # cr�ation du tableau de gestion des �tiquettes
    if {!$premier_passage} {
        unset etiquette
    }

    foreach carte $cartes {
        foreach mrq [$carte cget -ordre] {
            set etiquette($carte,$mrq) 0
        }
    }

    # Pour que les �tiquettes apparaissent par dessus les liens entre marqueurs
    # elles doivent etre cr�es apr�s
    set indice 0
    foreach carte $cartes {
        # On met � jour les �tiquettes qui �taient pr�sentes
        if {$etiquetees($indice)} {
            $carte afficheInfo coulMrq etiquette
        }
        incr indice
    }

    # Attribution de valeur par d�faut lors du premier passage pour la position
    # de la roulette et la couleur des liens s�lectionn�s de chaque marqueur
   #  if {$premier_passage} {
#         set indiceCoulMrq -1
#         foreach mrq $marqueurs {
#             set rouletteMrq($mrq) 0

#             set indiceCoulMrq [expr [incr indiceCoulMrq] \
#                     % [llength $tabCouleurs]]
#             set coulMrq($mrq) [lindex $tabCouleurs $indiceCoulMrq]
#         }
#     }

    set indiceCoulMrq -1
    foreach mrq $marqueurs {
	if {[array names rouletteMrq $mrq] == {}} {
	
	    set rouletteMrq($mrq) 0
	    
	    set indiceCoulMrq [expr [incr indiceCoulMrq] \
				   % [llength $tabCouleurs]]
	    set coulMrq($mrq) [lindex $tabCouleurs $indiceCoulMrq]
	}
    }

    # Affiche les noms des marqueurs sur les cot�s
    # et les fl�ches qui les relient aux premi�re et derni�re cartes
    _postDessiner $premiere_carte $derniere_carte

    # Actualisation des marqueurs et liens
    if {!$premier_passage} {
        foreach mrq $marqueurs {
            set tmp $rouletteMrq($mrq)
            set rouletteMrq($mrq) 0
            for {set i $tmp} {$i > 0} {incr i -1} {
                rouletteMrq $mrq
            }
        }
    }
    configure -cursor ""

    # to adjust the canvas each tile to the upper left corner

    xview moveto 0
    yview moveto 0
}


#
# On efface et d�truit la partie graphique
# Le param�tre 'action' permet d'indiquer une r�initialisation compl�te
# valable notament lorsque le jeu de donn�es ou la s�lection de marqueurs
# changent (actions possibles : memorise ou reset)
#
body CGtasVisuel::effacer {{action memorise}} {

    if {$action == "memorise"} {
        # Recherche les cartes s�lectionn�es
        # et portant une �tiquette par marqueur
        # puis les d�truit
        set i 0
        foreach carte $cartes {
            set etiquetees($i) [$carte cget -info]
            delete object $carte
            incr i
        }
    } else {
        foreach carte $cartes {
            delete object $carte
        }
        set cartes ""
    }

    # destruction des objets graphiques
    delete all
}


#
# Y a t-il quelque chose de dessin� ?
#
body CGtasVisuel::vide {} {

    if {$cartes == ""} {
        return 1
    } else {
        return 0
    }
}

#
#
#

body CGtasVisuel::tasser { unit nbc cb lds} {

    set cartbrut $cb

    foreach ds $lds {
	set datasetype([lindex $ds 0]) [lindex $ds 1]
    }
    
    effacer

    dessiner

}


#
#
#

body CGtasVisuel::charger {fic} {

    set cartbrut {}

    set fileid [open $fic {RDONLY}]

    while {[gets $fileid ligne] > 0} {
	
	lappend cartbrut $ligne
    }
    
    close $fileid

    effacer
    dessiner
 
}

#
#
#
body CGtasVisuel::OptAffichage {} {
    
    . configure -cursor watch
    
    $itk_component(optAffichage) activate
    
    if {$cartes != ""} {
	effacer
	dessiner
    }

    . configure -cursor ""
}

#
#
#
body CGtasVisuel::ColorDial {} {
    
    . configure -cursor watch
    
    if {[$itk_component(colorDialog) activate]} {
	set couleurs [$itk_component(colorDialog) cget -couleurs]
	
        if {![vide]} {
            effacer memorise
            dessiner
        }
    }
    
    . configure -cursor ""
}

#
#
#
body CGtasVisuel::FontDial {} {
     global CGWhaschanged
    . configure -cursor watch

    set res [SelectFont .fontdlg -parent . -font $itk_option(-locfont)]
    
    if {$res != ""} {
	set CGWhaschanged 1
	set itk_option(-locfont) $res
	
	effacer
	dessiner
    }
   
    . configure -cursor ""
}

#
#
#
body CGtasVisuel::menuPrint {} {

    . configure -cursor watch

    if {[$itk_component(pcd) activate]} {
        $itk_component(pcd) print
    }
    
    . configure -cursor ""
}
#
#affectation dans le tableau des premi�res coordonn�es d'un marqueur sur une carte
#
body CGtasVisuel::affectppm {m x y} {
    set PremPosMrq($m) [list $x $y]
}

#
#affectation dans le tableau des derni�res coordonn�es d'un marqueur sur une carte
#
body CGtasVisuel::affectdpm {m x y} {
    set DernPosMrq($m) [list $x $y]
}

#
#affectation des coordonn�es?
#
body CGtasVisuel::affectionppm { m } {
    if { [array names PremPosMrq $m] != {} } {
	return 1
    } else {
	return 0
    }
}

body CGtasVisuel::MaxY {} {

    # each group of map has the same number of loci

    set xmap [lindex $cartbrut 0]

    if {[$itk_interior.optAffichage cget -mode] == "horizontal"} {
    } else {
	#the longer map is the one that has the more loci
	#we do not need to consider the longueur map
	#but all the loci

	set maxnbm [llength $marqueurs]

	set it [create text \
		    0 \
		    0 \
		    -text "LociNULL" \
		    -font $itk_option(-locfont)]

	set lh [expr [lindex [bbox $it] 3] - [lindex [bbox $it] 1]]

	delete $it

	return [expr 2 * $maxnbm * $lh]

    }
}

body CGtasVisuel::MaxX {} {

    # each group of map has the same number of loci

    set xmap [lindex [lindex $cartbrut 0] 2]

    if {[$itk_interior.optAffichage cget -mode] == "horizontal"} {
 
	#the longer map is the one that has the more loci
	#we do not need to consider the longueur map
	#but all the loci
	# set maxnbm 0
	set maxnbm [llength $marqueurs]

	# we have to sum each locus size

	#assuming it is not an "order"

	# first locus
	
	set loc [lindex $marqueurs 0]

	set it [create text \
		    0 \
		    0 \
		    -text $loc \
		    -font $itk_option(-locfont)]
	
	set lh [expr [lindex [bbox $it] 3] - [lindex [bbox $it] 1]]
	
	set sll [expr [lindex [bbox $it] 2] - [lindex [bbox $it] 0]]

	delete $it

	foreach loc [lrange $marqueurs 1 end] {

	    set it [create text \
			0 \
			0 \
			-text $loc \
			-font $itk_option(-locfont)]
	    
	    set sll [expr $sll + [lindex [bbox $it] 2] - [lindex [bbox $it] 0]]
	    
	    delete $it
	}

	return [expr $sll + (($lh * ($maxnbm - 1)* [$itk_interior.optAffichage cget -zoomMrq]) / 100.0)]

    } else {
    }
}

configbody CGtasVisuel::locfont {
}

#-----------------------------------------------------------------------------
# acc�der � la valeur d'une variable.
#-----------------------------------------------------------------------------
# Param�tres : 
# - variabl : le nom d'une variable pr�fix� par -
# Retourne :
# la valeur de la variable
# Remarque :
# C'est diff�rent de cget dans la mesure o� il ne s'agit pas de configuration 
#-----------------------------------------------------------------------------
body CGtasVisuel::get { variabl } {
    switch -- $variabl {
	-minSpace {return [expr ($minSpace * [$itk_interior.optAffichage cget -zoomMap]) / 100.0]}
	-couleurs {return $couleurs}
	-cartes {return $cartes}
	-facteurH {return $facteurH}
	-facteurV {return $facteurV}
	-margeH {return $margeH}
	-margeV {return $margeV}
	-echelleH {return $echelleH}
	-echelleV {return $echelleV}
	-logtxt {return [$itk_component(colorDialog) cget -logtxt]}
	-logfle {return [$itk_component(colorDialog) cget -logfle]}
	-carte {return [$itk_component(colorDialog) cget -carte]}
	-mrqtxt {return [$itk_component(colorDialog) cget -mrqtx]}
	-mrqtxtsel {return [$itk_component(colorDialog) cget -mrqtxtsel]}
	-mrqlineon  {return [$itk_component(colorDialog) cget -mrqlineon]}
	-mrqlineoff {return [$itk_component(colorDialog) cget -mrqlineoff]}
	-infoline {return [$itk_component(colorDialog) cget -infoline]}
	-infotxt {return [$itk_component(colorDialog) cget -infotxt]}
	-infobox {return [$itk_component(colorDialog) cget -infobox]}
	-infofont {return [$itk_component(colorDialog) cget -infofont]}
	-mrqfont {return [$itk_component(colorDialog) cget -mrqfont]}
	-logfont {return [$itk_component(colorDialog) cget -logfont]}
	-locfont {return $itk_option(-locfont)}
    }
}

body CGtasVisuel::flipSeg {} {

    # r�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]

    # liste des tags de l'identificateur
    set listTag [gettags $tagId]


    # on recherche celui qui commence par "::cGcarteVisuel*"
    set iTag [lsearch $listTag ::cGcarteVisuel*]

    # on regarde si c'est un segment
    if {[lsearch $listTag seg*] == -1} {
	return
    } else {
	set segnum [string range [lindex $listTag [lsearch $listTag seg*]] 4 end]
    }

    # ce tag est le nom de la carte qui a re�u l'�v�nement (clic souris)
    set carte [lindex $listTag $iTag]

    # on demande � la carte d'inverser ce segment
    $carte flipSeg $segnum
}

body CGtasVisuel::flipSeg2 {nc nj li} {
    
    #l� on accede

    set vnc [expr $nc/([llength [lindex $cartbrut 0]] - 2)]

    set carte [lindex $cartbrut $vnc]

    set isc 2
    
    foreach scar [lrange $carte 2 end] {
	if {[lindex $scar 0] == $nj} {
	    set iseg $isc
	}
	incr isc
	
    }

    set segs [lindex $carte $iseg]

    #l� on retourne

    set seg [lrange $segs [expr [lindex $li 0] * 3 + 2]  [expr [lindex $li end] * 3 + 4]]

    #l� on retourne
    
    set segr {}

    set segle [lindex $seg end]

    set segsi [llength $seg]

    for {set i 0} {$i < $segsi} {incr i 3} {
	lappend segr [lindex $seg [expr $segsi - 3 - $i]]
	lappend segr [lindex $seg [expr $segsi - 2 - $i]]
	lappend segr [lindex $seg [expr $segsi - 1 - $i]]
	#lappend segr [expr $segle - [lindex $seg [expr $segsi - 1 - $i]]]
    }
    
    #l� on recompose

    set nsegs [concat [lrange $segs 0 [expr [lindex $li 0] * 3 + 1]] $segr [lrange $segs [expr [lindex $li end]* 3 + 5] end]]

    set ncarte [lreplace $carte $iseg $iseg $nsegs]

    set cartbrut [lreplace $cartbrut $vnc $vnc $ncarte]
   
    effacer

    dessiner
}


body CGtasVisuel::pushUpSeg {} {

    # r�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]

    # liste des tags de l'identificateur
    set listTag [gettags $tagId]

    set listTag $lisTagMemo
    

    # on recherche celui qui commence par "::cGcarteVisuel*"
    set iTag [lsearch $listTag ::cGcarteVisuel*]

    # on regarde si c'est un segment
    if {[lsearch $listTag seg*] == -1} {
	return
    } else {
	set segnum [string range [lindex $listTag [lsearch $listTag seg*]] 4 end]
    }

    # ce tag est le nom de la carte qui a re�u l'�v�nement (clic souris)
    set carte [lindex $listTag $iTag]

    # on demande � la carte d'inverser ce segment
    $carte pushUpSeg $segnum
}

body CGtasVisuel::pushUpSeg2 {nc nj li lu} {

    set vnc [expr $nc/([llength [lindex $cartbrut 0]] - 2)]

    set carte [lindex $cartbrut $vnc]

    set isc 2
    
    foreach scar [lrange $carte 2 end] {
	if {[lindex $scar 0] == $nj} {
	    set iseg $isc
	}
	incr isc
	
    }

    set segs [lindex $carte $iseg]

    #l� on shift

    set seg [lrange $segs [expr [lindex $li 0] * 3 + 2]  [expr [lindex $li end] * 3 + 4]]

    #l� on shift
    
    set segu [lrange $segs [expr [lindex $lu 0] * 3 + 2]  [expr [lindex $lu end] * 3 + 4]]
    
    if {[lindex $li 0] > [lindex $lu 0]} {
	
	set nsegs [concat [lrange $segs 0 [expr [lindex $lu 0] * 3 + 1]] $seg $segu [lrange $segs [expr [lindex $li end] * 3 + 5] end]]
    } else {
	set nsegs [concat [lrange $segs 0 1] $segu [lrange $segs [expr [lindex $li end] * 3 + 5] [expr [lindex $lu 0] * 3 + 1]] $seg]

    }
   
    #l� on recompose


    set ncarte [lreplace $carte $iseg $iseg $nsegs]

    set cartbrut [lreplace $cartbrut $vnc $vnc $ncarte]
   
    effacer

    dessiner
}

# m�thode prot�g�e(inspiration Hirarchy): _post x y
#
# Si la souris se trouve sur un item, 
# c'est celui le menu de la carte qui est affich� (provisoire)
# sinon c'est celui du tas.
# ----------------------------------------------------------------------
body CGtasVisuel::MapMenu {w x y} {

    set rx [expr [winfo rootx $itk_component(canvas)]+$x]
    set ry [expr [winfo rooty $itk_component(canvas)]+$y]    
	
    # r�cup�ration de l'identificateur de la zone graphique courante
    set tagId [find withtag current]
    
    # liste des tags de l'identificateur
    set lisTagMemo [gettags $tagId]
  
    # if this is related to a segment
    # so far actions are only available on segment

    set iTag [lsearch $lisTagMemo seg_*]

    if {$iTag != -1} {
	tk_popup $itk_component(cartMenu) $rx $ry
    }
    
}