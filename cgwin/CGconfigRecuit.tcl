#      Fichier	     Version   Date et heure    Auteur
#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigRecuit.tcl,v 1.3 2002-11-07 16:59:02 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres de la m�thode du Recuit
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------


#
# Options usuelles.
#
itk::usual CGconfigRecuit {
    keep -background -cursor -foreground
# -modality
}

class CGconfigRecuit {
    inherit itk::Widget

    lappend auto_path .

    itk_option define -tries tries Tries 10
    itk_option define -tinit tinit Tinit 0.3
    itk_option define -tfinal tfinal Tfinal 0.1
    itk_option define -cooling cooling Cooling 0.9

    constructor {args} {}
    public method get {arg}

}

proc cgconfigrecuit {pathName args} {
    uplevel CGconfigRecuit $pathName $args
}


body CGconfigRecuit::constructor {args} {

    itk_component add commentaires {
        label $itk_interior.commentaires \
                -text "Specific parameters of the local search method Annealing"
    }
    pack $itk_component(commentaires) -padx 4 -pady 5

    itk_component add tries {
        iwidgets::spinint $itk_interior.tries     \
	    -range {1 500} -width 3 \
	    -labeltext "Number of tries:"
    }
    pack $itk_component(tries) -padx 10 -pady 10 -anchor e
    
    itk_component add tinit {
        spinreal $itk_interior.tinit    \
	    -range {0.0 300.0} -width 4 \
	    -labeltext "Initial temperature:"
    }
    pack $itk_component(tinit) -padx 10 -pady 10 -anchor e
    
    itk_component add tfinal {
        spinreal $itk_interior.tfinal \
	    -range {0.0 300.0} -width 4 \
	    -labeltext "Final temperature:"
    }
    pack $itk_component(tfinal) -padx 10 -pady 10 -anchor e
    
    itk_component add cooling {
        spinreal $itk_interior.cooling \
	    -range {0.0 1.0} -width 4 \
	    -step 0.05 \
	    -labeltext "Cooling speed:"
    }
    pack $itk_component(cooling) -padx 10 -pady 10 -anchor e

    eval itk_initialize $args
}


configbody CGconfigRecuit::tries {

    set nbr $itk_option(-tries)
    if {$nbr < 1 || $nbr > 500} {
        error "wrong value \"tries\": should be between 1 and 5O0"
    } else {
        $itk_component(tries) delete 0 end
        $itk_component(tries) insert 0 $nbr
    }
}


configbody CGconfigRecuit::tinit {

    set nbr $itk_option(-tinit)
    if {$nbr < 0.0 || $nbr > 300.0} {
        error "wrong value \"tinit\": should be between 0.0 and 300.0"
    } else {
        $itk_component(tinit) delete 0 end
        $itk_component(tinit) insert 0 $nbr
    }
}


configbody CGconfigRecuit::tfinal {

    set nbr $itk_option(-tfinal)
    if {$nbr < 0.0 || $nbr > 300.0} {
        error "wrong value \"tfinal\": should be between 0.0 and 300.0"
    } else {
        $itk_component(tfinal) delete 0 end
        $itk_component(tfinal) insert 0 $nbr
    }
}


configbody CGconfigRecuit::cooling {

    set nbr $itk_option(-cooling)
    if {$nbr < 0.0 || $nbr > 1.0} {
        error "wrong value \"cooling\": should be between 0.0 and 1.0"
    } else {
        $itk_component(cooling) delete 0 end
        $itk_component(cooling) insert 0 $nbr
    }
}

body CGconfigRecuit::get {arg} {
    
    switch -- $arg {
        "-tries" { return [$itk_component(tries) get]}
        "-tinit" { return [$itk_component(tinit) get]}
        "-tfinal" { return [$itk_component(tfinal) get]}
        "-cooling" { return [$itk_component(cooling) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}
