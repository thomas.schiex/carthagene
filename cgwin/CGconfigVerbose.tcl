#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigVerbose.tcl,v 1.4 2003-02-25 16:11:22 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : widget verbose : impl�mentation pour les valeurs par d�faut
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Resources par d�faut
#

#
# Options usuelles.
#
itk::usual CGconfigVerbose {
    keep -background -cursor -foreground
}

class CGconfigVerbose {
    inherit ::iwidgets::Radiobox
    
    lappend auto_path .
    
    itk_option define -verbose verbose Verbose 0
    

    constructor {args} {}
    public method cget {arg}

}

proc cgconfigverbose {pathName args} {
    uplevel CGconfigVerbose $pathName $args
}


body CGconfigVerbose::constructor {args} {
    
    add very\
	-text "  very   " \
	-indicatoron 1
    
	add fairly \
	-text "  fairly  " \
	-indicatoron 1
    
    add not_f \
	-text " not very " \
	-indicatoron 1

    buttonconfigure 1 -value 1
    buttonconfigure 0 -value 2
    buttonconfigure 2 -value 0
    
    eval itk_initialize $args
}

configbody CGconfigVerbose::verbose {
    
    set nbr $itk_option(-verbose)
    if {$nbr != 0 && $nbr != 1 && $nbr != 2}   {
        error "wrong value \"verbose\": should be 0 1, or 2"
    } else {
	select [ expr 2 - $nbr]
    }
}

body CGconfigVerbose::cget {arg} {
    
    return $itk_option($arg)
    
}
