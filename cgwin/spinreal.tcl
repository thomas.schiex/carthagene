#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: spinreal.tcl,v 1.3 2002-11-07 16:59:17 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Widget pour la saisie de nombre r�els
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Default resources.
#
option add *Spinreal.relief sunken widgetDefault
option add *Spinreal.labelPos w widgetDefault
option add *Spinreal.labelMargin 2 widgetDefault
option add *Spinreal.auxLabelPos e widgetDefault
option add *Spinreal.auxLabelMargin 2 widgetDefault

#
# Usual options.
#
itk::usual Spinreal {
    keep -background -borderwidth -cursor -foreground -highlightcolor \
	 -highlightthickness -insertbackground  -insertborderwidth \
	 -insertofftime -insertontime -insertwidth -labelfont \
	 -selectbackground -selectborderwidth -selectforeground \
	 -textbackground -textfont
}

# ------------------------------------------------------------------
#                            SPINREAL
# ------------------------------------------------------------------
class Spinreal {
    inherit iwidgets::Spinner 

    constructor {args} {
	Spinner::constructor -validate real
    } {}

    itk_option define -range range Range "" 
    itk_option define -step step Step 0.1 
    itk_option define -wrap wrap Wrap true 

    public method up {}
    public method down {}
}

proc spinreal {pathName args} {
    uplevel Spinreal $pathName $args
}


# ------------------------------------------------------------------
#                        CONSTRUCTOR
# ------------------------------------------------------------------
body Spinreal::constructor {args} {
    eval itk_initialize $args
    
    $itk_component(entry) delete 0 end
    
    if {[lindex $itk_option(-range) 0] == ""} {
	$itk_component(entry) insert 0 "0.0"
    } else { 
	$itk_component(entry) insert 0 [lindex $itk_option(-range) 0] 
    }
}

# ------------------------------------------------------------------
#                             OPTIONS
# ------------------------------------------------------------------

# ------------------------------------------------------------------
# OPTION: -range
#
# Set min and max values for spinner.
# ------------------------------------------------------------------
configbody Spinreal::range {
    if {$itk_option(-range) != ""} {
	if {[llength $itk_option(-range)] != 2} {
	    error "wrong # args: should be\
		    \"$itk_component(hull) configure -range {begin end}\""
    	}
    	set min [lindex $itk_option(-range) 0]
    	set max [lindex $itk_option(-range) 1]
    	if {![regexp {^-?[0-9]*.[0-9]+$} $min]} {
    	    error "bad range option \"$min\": begin value must be\
		    an real"
    	}
    	if {![regexp {^-?[0-9]*.[0-9]+$} $max]} {
    	    error "bad range option \"$max\": end value must be\
		    an real"
    	}
    	if {$min > $max} {
    	    error "bad option starting range \"$min\": must be less\
		    than ending: \"$max\""
    	}
    } 
}

# ------------------------------------------------------------------
# OPTION: -step
#
# Increment spinner by step value.
# ------------------------------------------------------------------
configbody Spinreal::step {
}

# ------------------------------------------------------------------
# OPTION: -wrap
#
# Specify whether spinner should wrap value if at min or max.
# ------------------------------------------------------------------
configbody Spinreal::wrap {
}

# ------------------------------------------------------------------
#                            METHODS
# ------------------------------------------------------------------

# ------------------------------------------------------------------
# METHOD: up
#
# Up arrow button press event.  Increment value in entry.
# ------------------------------------------------------------------
body Spinreal::up {} {
    
    set val [$itk_component(entry) get]

    if {[lindex $itk_option(-range) 0] != ""} {
        set min_range [lindex $itk_option(-range) 0]
        set max_range [lindex $itk_option(-range) 1]
	
	#
	# Check boundaries.
	#
	if {$val >= $min_range && $val < $max_range} {
	    set val [expr $val + $itk_option(-step)]
	    
	    #
	    # Re-check boundaries.
	    #
	    if {$val >= $min_range && $val <= $max_range} {
		$itk_component(entry) delete 0 end
		$itk_component(entry) insert 0 $val
	    } else {
		
		#
		# This is wrap when -step > 1.
		#
		if {$itk_option(-wrap)} {
		    if {$val > $max_range} {
			$itk_component(entry) delete 0 end
			$itk_component(entry) insert 0 $min_range
		    } else {
			uplevel #0 $itk_option(-invalid)
		    }
		} else {
		    uplevel #0 $itk_option(-invalid)
		}
	    }
	    
	} else {
	    if {$itk_option(-wrap)} {
		if {$val == $max_range} {
		    $itk_component(entry) delete 0 end
		    $itk_component(entry) insert 0 $min_range 
		} else {
		    uplevel #0 $itk_option(-invalid)
		}
	    } else {
		uplevel #0 $itk_option(-invalid)
	    }
	}
    } else {
	
	#
	# No boundaries.
	#
	set val [expr $val + $itk_option(-step)]
	$itk_component(entry) delete 0 end
	$itk_component(entry) insert 0 $val
    }
}

# ------------------------------------------------------------------
# METHOD: down 
#
# Down arrow button press event.  Decrement value in entry.
# ------------------------------------------------------------------
body Spinreal::down {} {
    
    set val [$itk_component(entry) get]

    if {[lindex $itk_option(-range) 0] != ""} {
        set min_range [lindex $itk_option(-range) 0]
        set max_range [lindex $itk_option(-range) 1]
	
	#
	# Check boundaries.
	#
	if {$val > $min_range && $val <= $max_range} {
	    set val [expr $val - $itk_option(-step)]
	    
	    #
	    # Re-check boundaries.
	    #
	    if {$val >= $min_range && $val <= $max_range} {
		$itk_component(entry) delete 0 end
		$itk_component(entry) insert 0 $val
	    } else {
		
		#
		# This is wrap when -step > 1.
		#
		if {$itk_option(-wrap)} {
		    if {$val < $min_range} {
			$itk_component(entry) delete 0 end
			$itk_component(entry) insert 0 $max_range
		    } else {
			uplevel #0 $itk_option(-invalid)
		    }
		} else {
		    uplevel #0 $itk_option(-invalid)
		}
	    }
	    
	} else {
	    if {$itk_option(-wrap)} {
		if {$val == $min_range} {
		    $itk_component(entry) delete 0 end
		    $itk_component(entry) insert 0 $max_range
		} else {
		    uplevel #0 $itk_option(-invalid)
		}
	    } else {
		uplevel #0 $itk_option(-invalid)
	    }
	}
    } else {
	
	#
	# No boundaries.
	#
	set val [expr $val - $itk_option(-step)]
	$itk_component(entry) delete 0 end
	$itk_component(entry) insert 0 $val
    }
}
