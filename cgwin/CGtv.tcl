#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGtv.tcl,v 1.6 2004-12-16 10:05:30 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : dialogue pour le graphique
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# Usual options.
#
# itk::usual CGtvDialog {
#     keep -activebackground -activerelief -background -borderwidth -cursor \
# 	 -elementborderwidth -foreground -highlightcolor -highlightthickness \
# 	 -insertbackground -insertborderwidth -insertofftime -insertontime \
# 	 -insertwidth -jump -labelfont -modality -selectbackground \
# 	 -selectborderwidth -selectforeground -textbackground -textfont \
# 	 -troughcolor
# }

# ------------------------------------------------------------------
#                           SELECTIONDIALOG
# ------------------------------------------------------------------
class CGtv {
    inherit iwidgets::Shell

    constructor {args} {}
    
    public method tasser {unit nbc cb lds}

}


proc cgtv {pathName args} {
    uplevel CGtv $pathName $args
}


body CGtv::constructor {args} {
    #
    # Set the borderwidth to zero.
    #
    component hull configure -borderwidth 0
    
    itk_component add li {
	
	label $itk_interior.li
    } {
	
    }
    
    pack $itk_component(li) -side bottom -fill x
    # Instantiation des deux, 1 pou l'instatnt
    #
    itk_component add tv {
	cgtasvisuel $itk_interior.tv \
	    -wli $itk_component(li)
    } 

    pack $itk_component(tv) -after $itk_component(li) -fill both -expand yes
    set itk_interior [$itk_component(tv) childsite]
    
  
    eval itk_initialize $args

    configure -title "CarthaG�ne : Map viewer"
}   
#
#
#

body CGtv::tasser { unit nbc cb lds} {
 
    $itk_component(tv) tasser $unit $nbc $cb $lds

}
