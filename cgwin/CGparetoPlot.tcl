#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGparetoPlot.tcl,v 1.8 2005-11-16 13:47:45 degivry Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Canvas for the Pareto Plot
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
package require Plotchart

#
# a way to improve the package and more precisely the ZOOM
# symbols are not resized
# this function overide the original one

proc ::Plotchart::ScaleItems { w xcentre ycentre factor } {
    variable scaling
    
    # item to be zoomed
    set itemz {} 
    # item to be moved
    set itemm {}
    
    foreach item [$w find all] {
	if { [lsearch [$w gettags $item] map] == -1} {
	    lappend itemz $item
	} else {
	    lappend itemm $item
	}
	
    }

    # zoom step

    foreach item  $itemz {
	
	$w scale $item $xcentre $ycentre $factor $factor
    }

    # move step

    foreach item  $itemm {

	# original "center of the symbol"
	set bb [$w bbox $item]
	set icx [expr [lindex $bb 0] + ([lindex $bb 2] - [lindex $bb 0])/2]
	set icy [expr [lindex $bb 1] + ([lindex $bb 3] - [lindex $bb 1])/2]
	# new "center of the symbol"
	set ncx [expr (($icx - $xcentre) * $factor) + $xcentre]
	set ncy [expr (($icy - $ycentre) * $factor) + $ycentre]
	# the move
	set vx [expr $ncx - $icx]
	set vy [expr $ncy - $icy]

	$w move $item $vx $vy
	
    }



    foreach {xc yc} [pixelToCoords $w $xcentre $ycentre] {break}
    
    set rfact               [expr {1.0/$factor}]
    set scaling($w,xfactor) [expr {$scaling($w,xfactor)*$factor}]
    set scaling($w,yfactor) [expr {$scaling($w,yfactor)*$factor}]
    set scaling($w,xmin)    [expr {(1.0-$rfact)*$xc+$rfact*$scaling($w,xmin)}]
    set scaling($w,xmax)    [expr {(1.0-$rfact)*$xc+$rfact*$scaling($w,xmax)}]
    set scaling($w,ymin)    [expr {(1.0-$rfact)*$yc+$rfact*$scaling($w,ymin)}]
    set scaling($w,ymax)    [expr {(1.0-$rfact)*$yc+$rfact*$scaling($w,ymax)}]
}

#
# Options usuelles.
#
itk::usual CGparetoPlot {
    keep -background -cursor -foreground -height -width
}


#
#
#
class CGparetoPlot {
    inherit iwidgets::Scrolledcanvas
    
    # Stockage brut de l'information 
    
    common infobrut {}
    common vx {}
    common mvx {}
    common vy {}
    common mvy {}
    common mapid {}
    common mmapid {}
    common itemid {}
    common minx
    common miny
    common maxx
    common maxy
    common ticstepx
    common ticstepy
    common startx
    common starty
    common endx
    common endy
    common ticlix {}
    common ticliy {}
    common ppchart {}
    common mstatus {}
    #the label info
    common wli
    
    constructor {args} {}
    

    public method plot {pl}
    private method dbl_raise {x y}
    private method quantize_normal_tics {arg guide}
    public  method displaythemap {}
    public  method provideInfo {}
    public  method eraseInfo {}
    public method menuPrint {}
    public method launchMenu {w x y}

    itk_option define -wli wli Wli "" {
        set wli $itk_option(-wli)
    }

}

proc cgparetoplot { pathName args } {
    uplevel CGparetoPlot $pathName $args
}

#option add *CGparetoPlot.height 500 widgetDefault
#option add *CGparetoPlot.width 500 widgetDefault

body CGparetoPlot::constructor { args } {
    
    eval itk_initialize $args
    
    uplevel #0 "bind $itk_component(canvas) <Button-3> \"$this launchMenu %W %x %y\""
    
    bind map <Button-1> "$this displaythemap"
    bind map <Enter> "$this provideInfo"
    bind map <Leave> "$this eraseInfo"
    
    itk_component add Menu {
	menu $itk_component(canvas).menu \
	    -tearoff 0 \
	} {
	    usual
	    ignore -tearoff
	    rename -cursor -menucursor menuCursor Cursor
	}
    
    $itk_component(Menu) add command -label "Print..." \
	-command "$this menuPrint"
    
    itk_component add pcd {
	cgcanvasprintdialog $itk_interior.pcd \
	    -modality application
    }
    

    $itk_component(pcd) setcanvas $itk_component(canvas)
}


body CGparetoPlot::plot {pl} {        
    
    if {$ppchart != {}} {

	# plot need to be reset
	delete $ppchart
	delete all
	set vx {}
	set vy {}
    }
    
    set infobrut $pl
    
    set nbp [llength $pl]
    set minx [lindex [lindex $pl [expr $nbp - 1]] 2]
    foreach p $pl {
	set x [lindex $p 2]
	if {[string compare $x "nan"] != 0 & [string compare $x "inf"] != 0} {
	    lappend vx $x
	    if {  $x < $minx} {
		set minx $x
	    }
	}
	set x [lindex $p 3]
	if {[string compare $x "nan"] != 0 & [string compare $x "inf"] != 0} {
	    lappend vx $x
	    if {  $x < $minx} {
		set minx $x
	    }
	}
	lappend vy [lindex $p 1]
	lappend mapid [lindex $p 0]
    }

    set miny [lindex $vy [expr $nbp -1]]
    set maxx [lindex $vx 0]
    set maxy [lindex $vy 0]

    set ticstepx [format "%.2f" [quantize_normal_tics [expr $maxx - $minx] 10]]   
    set ticstepy [quantize_normal_tics [expr $maxy - $miny] 10]

    set startx [format "%.2f" [expr $ticstepx * floor($minx / $ticstepx)]]
    set endx [format "%.2f" [expr $ticstepx * ceil($maxx / $ticstepx)]]
    set starty [expr $ticstepy * floor($miny / $ticstepy)]
    set endy  [expr $ticstepy * ceil($maxy / $ticstepy)]
    
    set me [winfo parent [namespace tail $this]]

    set me [namespace tail $this]

    # could be set in a better way

    $me configure -background white

    set ppchart [::Plotchart::createXYPlot $me  [list $starty $endy $ticstepy] [list $startx $endx $ticstepx]]

    $me configure -background grey

    # could be very convenientexit



    ::Plotchart::setZoomPan $me

    #does not seem to be implemented so far

    #$ppchart xconfig -ticklines 1
    #$ppchart xconfig -ticklength 10
    #$ppchart yconfig -ticklines qsdf
    #$ppchart xconfig -ticklength 10p

    $ppchart title "Pareto frontier approximation"
    $ppchart ytext "log10-likelihood"
    $ppchart xtext "number of breakpoints"
    $ppchart dataconfig frontier -colour red -type both
    $ppchart dataconfig frontier -symbol dot
    $ppchart dataconfig dominated -colour green -type symbol
    $ppchart dataconfig dominated -symbol plus
    $ppchart dataconfig newfrontier -colour blue -type both
    $ppchart dataconfig newfrontier -symbol dot
    $ppchart dataconfig balanced -colour red -type symbol
    $ppchart dataconfig balanced -symbol upfilled

    foreach p $pl {

	

	if {[lindex $p 4] == "frontier" || [lindex $p 4] == "balanced"} {


	    if {[string compare [lindex $p 3] "nan"] != 0 & [string compare [lindex $p 3] "inf"] != 0} {
		
		$ppchart plot newfrontier [lindex $p 1] [lindex $p 3]
		
		#to specify a binding
		set iid [lindex [$this find withtag data] end]
		$this addtag map withtag $iid
		
		#to keep the ItemId corresponding to the MapId
		lappend itemid $iid
		lappend mmapid [lindex $p 0]
		lappend mvx [lindex $p 3]
		lappend mvy [lindex $p 1]
		lappend mstatus [lindex $p 4]
	    }


	    if {[string compare [lindex $p 2] "nan"] != 0 & [string compare [lindex $p 2] "inf"] != 0} {
		
		$ppchart plot frontier [lindex $p 1] [lindex $p 2]
		
		set iid [lindex [$this find withtag data] end]
		$this addtag map withtag $iid
		
		#to keep the ItemId corresponding to the MapId
		lappend itemid $iid
		lappend mmapid [lindex $p 0]
		lappend mvx [lindex $p 2]
		lappend mvy [lindex $p 1]
		lappend mstatus [lindex $p 4]
	    }
	   
	    
	} else {

	    if {[string compare [lindex $p 2] "nan"] != 0 & [string compare [lindex $p 2] "inf"] != 0} {
		
		$ppchart plot dominated [lindex $p 1] [lindex $p 2]
	    
		set iid [lindex [$this find withtag data] end]
		$this addtag map withtag $iid
		
		#to keep the ItemId corresponding to the MapId
		lappend itemid $iid
		lappend mmapid [lindex $p 0]
		lappend mvx [lindex $p 2]
		lappend mvy [lindex $p 1]
		lappend mstatus [lindex $p 4]
	    }

	}
	

	
    }

    #needs to be plotted  over!

    foreach p $pl {
	if {[lindex $p 4] == "balanced"} {
	    
	    if {[string compare [lindex $p 3] "nan"] != 0 & [string compare [lindex $p 3] "inf"] != 0} {
		
		$ppchart plot balanced [lindex $p 1] [lindex $p 3]
	    
		#to specify a binding
		set iid [lindex [$this find withtag data] end]
		$this addtag map withtag $iid
		
		#to keep the ItemId corresponding to the MapId
		lappend itemid $iid
		lappend mmapid [lindex $p 0]
		lappend mvx [lindex $p 3]
		lappend mvy [lindex $p 1]
		lappend mstatus [lindex $p 4]
	    }
	    
	}
    }
    
}

body CGparetoPlot::dbl_raise {x y} {
    set i [expr abs($y)]
    set val 1.0

    while {[set i [expr $i - 1]] >= 0} {
	set val [expr $val * $x]
    }

    if {$y < 0} {
	return [expr 1.0 / $val]
    } else {
	return $val
    }
}

#inspirated by gnuplot.

body CGparetoPlot::quantize_normal_tics {arg guide} {
    #order of magnitude of argument
    set power [dbl_raise 10.0 [expr floor(log10($arg))]]
    # approx number of decades
    set xnorm [expr $arg / $power]
    #approx number of tic posns per decade
    set posns [expr $guide / $xnorm]

    if {$posns > 24} {
	# half a smaller unit --- shouldn't happen
	return [expr $power / 24]
    } elseif {$posns > 12} {
	# one smaller unit
	return [expr $power / 12]
    } elseif {$posns > 6} {
        # 2 smaller units = one-6th of a unit
	return [expr $power / 6]
    } elseif {$posns > 4} {
	# 3 smaller units = quarter unit
	return [expr $power / 4]
    } elseif {$posns > 2} {
	# 6 smaller units = half a unit
	return [expr $power / 2]
    } elseif {$posns > 1} {
	# 0, 1, 2, ..., 11
	return [expr $power]
    } elseif {$posns > 0.5} {
	# 0, 2, 4, ..., 10
	return [expr $power * 2]
    } elseif {$posns > [expr 1.0/3]} {
	# 0, 3, 6, 9 
	return [expr $power * 3]
    } else {
	#getting desperate... the ceil is to make sure we
	#go over rather than under - eg plot [-10:10] x*x
	#gives a range of about 99.999 - tics=xnorm gives
	#tics at 0, 99.99 and 109.98  - BAD !
	#This way, inaccuracy the other way will round
	#up (eg 0->100.0001 => tics at 0 and 101
	#I think latter is better than former
	return [expr $power * ceil($xnorm)]
    }
}

body CGparetoPlot::displaythemap {} {
        
    set iid [find withtag current]
    
    set lmid [lsearch $itemid $iid]

    ::maprintg h [lindex $mmapid $lmid]
}

body CGparetoPlot::provideInfo {} {
        
    set iid [find withtag current]
    
    set lmid [lsearch $itemid $iid]

    $wli configure -text "Id: [lindex $mmapid $lmid] Breakpoints: [lindex $mvy $lmid] Log10-like: [lindex $mvx $lmid] Status : [lindex $mstatus $lmid]"

}

body CGparetoPlot::eraseInfo {} {
        
    set iid [find withtag current]
    
    set lmid [lsearch $itemid $iid]

    $wli configure -text ""

}

#
#
#
body CGparetoPlot::menuPrint {} {

    . configure -cursor watch

    if {[$itk_component(pcd) activate]} {
        $itk_component(pcd) print
    }
    
    . configure -cursor ""
}

# m�thode prot�g�e(inspiration Hirarchy): _post x y
#
# Si la souris se trouve sur un item, 
# c'est celui le menu de la carte qui est affich� (provisoire)
# sinon c'est celui du tas.
# ----------------------------------------------------------------------
body CGparetoPlot::launchMenu {w x y} {

    set rx [expr [winfo rootx $itk_component(canvas)]+$x]
    set ry [expr [winfo rooty $itk_component(canvas)]+$y]
    

    tk_popup $itk_component(Menu) $rx $ry
}
