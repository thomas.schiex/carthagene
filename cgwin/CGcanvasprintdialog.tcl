#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGcanvasprintdialog.tcl,v 1.1 2004-11-16 11:43:59 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Module 	: cgwin 
# Projet	: CartaGene
#
# Description : just to provide enhancement of the original Canvasprintdialog
#
# Divers :  The single difference it provide a dialog to the CGCanvasprintbox,
# And not the original Canvasprintbox...
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# Option database default resources:
#
option add *CGCanvasprintdialog.filename "canvas.ps" widgetDefault
option add *CGCanvasprintdialog.hPageCnt 1 widgetDefault
option add *CGCanvasprintdialog.orient landscape widgetDefault
option add *CGCanvasprintdialog.output printer widgetDefault
option add *CGCanvasprintdialog.pageSize A4 widgetDefault
option add *CGCanvasprintdialog.posterize 0 widgetDefault
option add *CGCanvasprintdialog.printCmd lpr widgetDefault
option add *CGCanvasprintdialog.printRegion "" widgetDefault
option add *CGCanvasprintdialog.vPageCnt 1 widgetDefault
option add *CGCanvasprintdialog.title "Canvas Print Dialog" widgetDefault
option add *CGCanvasprintdialog.master "." widgetDefault

#
# Usual options.
#
itk::usual CGCanvasprintdialog {
	keep -background -cursor -foreground -modality 
}

# ------------------------------------------------------------------
# CANVASPRINTDIALOG
# ------------------------------------------------------------------
class CGCanvasprintdialog {
	inherit iwidgets::Dialog

	constructor {args} {}   
	destructor {}

	method deactivate {args} {}
	method getoutput {} {}
	method setcanvas {canv} {}
	method refresh {} {}
	method print {} {}
}

#
# Provide a lowercased access method for the CGCanvasprintdialog class.
# 
proc cgcanvasprintdialog {args} {
	uplevel CGCanvasprintdialog $args
}

# ------------------------------------------------------------------
# CONSTRUCTOR 
#
# Create new file selection dialog.
# ------------------------------------------------------------------
body CGCanvasprintdialog::constructor {args} {
	component hull configure -borderwidth 0

	# 
	# Instantiate a file selection box widget.
	#
	itk_component add cpb {
		cgcanvasprintbox $itk_interior.cpb
	} {
	#	usual
		keep -printregion -output -printcmd -filename -pagesize \
		     -orient -stretch -posterize -hpagecnt -vpagecnt
	}
	pack $itk_component(cpb) -fill both -expand yes

	#
	# Hide the apply and help buttons.
	#
	buttonconfigure OK -text Print
	buttonconfigure Apply -command [code $this refresh] -text Refresh
	hide Help

	eval itk_initialize $args
}   

# ------------------------------------------------------------------
# METHOD: deactivate
#
# Redefines method of dialog shell class. Stops the drawing of the
# thumbnail (when busy) upon deactivation of the dialog.
# ------------------------------------------------------------------
body CGCanvasprintdialog::deactivate {args} {
	$itk_component(cpb) stop
	return [eval Shell::deactivate $args]
}

# ------------------------------------------------------------------
# METHOD: getoutput
#
# Thinwrapped method of canvas print box class.
# ------------------------------------------------------------------
body CGCanvasprintdialog::getoutput {} {
	return [$itk_component(cpb) getoutput]
}

# ------------------------------------------------------------------
# METHOD: setcanvas
#
# Thinwrapped method of canvas print box class.
# ------------------------------------------------------------------
body CGCanvasprintdialog::setcanvas {canv} {
	return [$itk_component(cpb) setcanvas $canv]
}

# ------------------------------------------------------------------
# METHOD: refresh
#
# Thinwrapped method of canvas print box class.
# ------------------------------------------------------------------
body CGCanvasprintdialog::refresh {} {
	return [$itk_component(cpb) refresh]
}

# ------------------------------------------------------------------
# METHOD: print
#
# Thinwrapped method of canvas print box class.
# ------------------------------------------------------------------
body CGCanvasprintdialog::print {} {
	return [$itk_component(cpb) print]
}
