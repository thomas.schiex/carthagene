#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGcgDialog.tcl,v 1.3 2002-11-07 16:58:08 bouchez Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Boite de dialogue pour la configuration du calcul des groupes
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------


#
#
# Usual options.
#
itk::usual CGcgDialog {
    keep -activebackground -activerelief -background -borderwidth -cursor \
	 -elementborderwidth -foreground -highlightcolor -highlightthickness \
	 -insertbackground -insertborderwidth -insertofftime -insertontime \
	 -insertwidth -jump -labelfont -modality -selectbackground \
	 -selectborderwidth -selectforeground -textbackground -textfont \
	 -troughcolor
}

#
#
#
class CGcgDialog {
    inherit iwidgets::Dialog

    constructor {args} {} 
    public method get {arg}
}


#
# Provide a lowercased access method for the Buttonbox class.
# 
proc cgcgdialog {pathName args} {
    uplevel CGcgDialog $pathName $args
}
 
#
#
#
#
body CGcgDialog::constructor {args} {
    #
    # Set the borderwidth to zero.
    #
    component hull configure -borderwidth 0
    
    itk_component add cg {
	cgconfiggroup $itk_interior.cg
    }
    
    pack $itk_component(cg) -fill both -expand yes

    hide Help

    buttonconfigure Apply \
	-command CGWMrungroup \
	-text Try \
    
    set cmd [concat $itk_component(cg) "configure \
-lodthres \[" $itk_component(cg) " cget -lodthres\] \
-disthres \[" $itk_component(cg) " cget -disthres\]; " \
		 $this "deactivate 0" ]
    
    buttonconfigure Cancel \
	-command $cmd
    
    set cmd [concat  $itk_component(cg) "configure \
-lodthres \[" $itk_component(cg) " get -lodthres\] \
-disthres \[" $itk_component(cg) " get -disthres\] ;" \
		 $this "deactivate 1" ]
    
    buttonconfigure OK \
	-command $cmd
    eval itk_initialize $args
}

body CGcgDialog::get {arg} {
    
    switch -- $arg {
	"-lodthres" { return [$itk_component(cg) get -lodthres]}
	"-disthres" { return [$itk_component(cg) get -disthres]}
        default {
            error "unknown parameter $arg."
        }
    }
}

