#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGconfigGlouton.tcl,v 1.4 2003-07-07 13:41:02 chabrier Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: ihm
# Projet	: CartaGene
#
# Description : Classe de gestion des param�tres de la m�thode Glouton
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------


#
# Options usuelles.
#
itk::usual CGconfigGlouton {
    keep -background -cursor -foreground
}

#<
#
#
#>
class CGconfigGlouton {
    inherit itk::Widget

    itk_option define -nbiter nbiter Nbiter 0
    itk_option define -nbrun nbrun Nbrun 1
    itk_option define -taboolenmin taboolenmin Taboolenmin 1 
    itk_option define -taboolenmax taboolenmax Taboolenmax 15
    itk_option define -anytime anytime Anytime 0

    constructor {args} {}
    public method get {arg}

}


proc cgconfigglouton {pathName args} {
    uplevel CGconfigGlouton $pathName $args
}


body CGconfigGlouton::constructor {args} {

    itk_component add commentaires {
        label $itk_interior.commentaires \
                -text "Specific parameters of the local search method Greedy"
    }
    pack $itk_component(commentaires) -padx 4 -pady 5

    itk_component add nbrun {
        iwidgets::spinint $itk_interior.nbrun     \
                -range {1 100} -width 3 \
                -labeltext "Number of main loops:"
    }
    pack $itk_component(nbrun) -padx 10 -pady 10 -anchor e

    itk_component add nbiter {
        iwidgets::spinint $itk_interior.nbiter    \
                -range {0 100} -width 3 \
                -labeltext "Number of iterations:"
    }
    pack $itk_component(nbiter) -padx 10 -pady 10 -anchor e

    itk_component add taboolenmin {
        iwidgets::spinint $itk_interior.taboolenmin \
                -range {1 100} -width 3 \
                -labeltext "Minimum length of the taboo list:"
    }
    pack $itk_component(taboolenmin) -padx 10 -pady 10 -anchor e

    itk_component add taboolenmax {
        iwidgets::spinint $itk_interior.taboolenmax \
                -range {1 100} -width 3 \
                -labeltext "Maximum length of the taboo list:"
    }
    pack $itk_component(taboolenmax) -padx 10 -pady 10 -anchor e

    itk_component add anytime {
        iwidgets::spinint $itk_interior.anytime \
                -range {0 100} -width 3 \
                -labeltext "The ratio to control the tradeoff between search speed and solution quality:"
    }
    pack $itk_component(anytime) -padx 10 -pady 10 -anchor e
    

    eval itk_initialize $args

}


configbody CGconfigGlouton::nbrun {

    set nbr $itk_option(-nbrun)
    if {$nbr < 1 || $nbr > 100} {
        error "wrong value \"-nbrun $nbr\": should be between 1 and 100"
    } else {
        $itk_component(nbrun) delete 0 end
        $itk_component(nbrun) insert 0 $nbr
    }
}


configbody CGconfigGlouton::nbiter {

    set nbr $itk_option(-nbiter)
    if {$nbr < 0 || $nbr > 100} {
        error "wrong value \"-nbiter $nbr\": should be between 0 and 100"
    } else {
        $itk_component(nbiter) delete 0 end
        $itk_component(nbiter) insert 0 $nbr
    }
}


configbody CGconfigGlouton::taboolenmin {

    set nbr $itk_option(-taboolenmin)
    if {$nbr < 0 || $nbr > 100} {
        error "wrong value \"-taboolenmin $nbr\": should be between 0 and 100"
    } else {
        $itk_component(taboolenmin) delete 0 end
        $itk_component(taboolenmin) insert 0 $nbr
    }
}


configbody CGconfigGlouton::taboolenmax {

    set nbr $itk_option(-taboolenmax)
    if {$nbr < 0 || $nbr > 100} {
        error "wrong value \"-taboolenmax $nbr\": should be between 0 and 100"
    } elseif {$nbr < $itk_option(-taboolenmin)} {
        error "wrong value \"-taboolenmax $nbr\": should be >= taboolenmin"
    } else {
        $itk_component(taboolenmax) delete 0 end
        $itk_component(taboolenmax) insert 0 $nbr
    }
}

configbody CGconfigGlouton::anytime {

    set nbr $itk_option(-anytime)
    if {$nbr < 0 || $nbr > 100} {
        error "wrong value \"-anytime $nbr\": should be between 0 and 100"
    } else {
        $itk_component(anytime) delete 0 end
        $itk_component(anytime) insert 0 $nbr
    }
}

body CGconfigGlouton::get {arg} {

    switch -- $arg {
        "-nbiter" { return [$itk_component(nbiter) get]}
        "-nbrun" { return [$itk_component(nbrun) get]}
        "-taboolenmax" { return [$itk_component(taboolenmax) get]}
        "-taboolenmin" { return [$itk_component(taboolenmin) get]}
        "-anytime" { return [$itk_component(anytime) get]}
        default {
            error "unknown parameter $arg."
        }
    }
}
