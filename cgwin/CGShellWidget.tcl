#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: CGShellWidget.tcl,v 1.12 2010-06-21 15:25:06 dleroux Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module 	: cgwin
# Projet	: CartaGene
#
# Description : Un widget Tcl Shell
#
# Divers : inspir� de Welch
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# options usuelles
#
itk::usual CGShellWidget {
    keep -activebackground -activerelief -background -borderwidth -cursor \
         -elementborderwidth -foreground -highlightcolor -highlightthickness \
         -insertbackground -insertborderwidth -insertofftime -insertontime \
         -insertwidth -jump -labelfont -selectbackground -selectborderwidth \
         -selectforeground -textbackground -textfont -troughcolor 
}


# ------------------------------------------------------------------
# 
# ------------------------------------------------------------------
class CGShellWidget {
    inherit iwidgets::Scrolledtext 

    constructor {args} {}
    public method EvalTypein {}
    public method EvalNoTyped {command}
    public method Eval {command}
    
    public method ResetAlias {}
    public method PutsAlias {args}
    public method GetsAlias {args}
 
    public method OpenAlias {args}
    public method CloseAlias {args}
    public method WinfoAlias {args}
    public method PackageAlias {args}
    public method CGPPAlias {args}

    variable prompt "CG> "
    variable fidlog
    common pris 0
}

#
# Provide a lowercased access method for the Buttonbox class.
# 
proc cgshellwidget {pathName args} {
    uplevel CGShellWidget $pathName $args
}
 
    
# ------------------------------------------------------------------
#                 
# ------------------------------------------------------------------
body CGShellWidget::constructor {args} {

    # configuration des couleurs

    $itk_component(text) tag configure prompt -foreground blue
    $itk_component(text) tag configure result -foreground orange
    $itk_component(text) tag configure error -foreground red
    $itk_component(text) tag configure output -foreground green4

    # police 

    configure -textfont -*-courier-medium-r-normal-*

    # l'esclave

    interp create Slave
    if { $::tcl_platform(platform) == "windows" } {
        interp eval Slave {lappend ::auto_path [file dirname [file dirname [info script]]]}
    } else {
        interp eval Slave {lappend ::auto_path /usr/share/carthagene}
    }
    interp eval Slave package require CGsh

    if { $::tcl_platform(platform) == "windows" } {
        interp eval Slave source ~/carthagenerc.tcl
        #initialisation
        set fidlog  [open cgwlog[pid].txt w]
    } else { 
        interp eval Slave source ~/.carthagenerc
        #initialisation
        set fidlog  [open /tmp/.cgwlog[pid] w]
    }
    #interp eval Slave source ~/.carthagenerc
    interp alias Slave reset {} ResetAlias
    interp alias Slave puts {} $this PutsAlias
    interp alias Slave gets {} $this GetsAlias
    interp alias Slave open {} $this OpenAlias
    interp alias Slave close {} $this CloseAlias
    interp alias Slave winfo {} $this WinfoAlias
    interp alias Slave package {} $this PackageAlias
    interp alias Slave .cgpp {} $this CGPPAlias

    # mise en place du prompt
    $itk_component(text) insert insert $prompt prompt
    puts -nonewline $fidlog $prompt

    # initialisation du rep�re de limite
    $itk_component(text) mark set limit insert
    $itk_component(text) mark gravity limit left
    focus $itk_component(text)

    # quelques "bindings"

    bind $itk_component(text) <Return> "[scope $this] EvalTypein; break"

    bind $itk_component(text) <BackSpace> {
	if {[%W tag nextrange sel 1.0 end] != ""} {
	    %W delete sel.first sel.last
	} elseif [%W compare insert > limit] {
	    %W delete insert-1c
	    %W see insert
	}
	break
    }
    
    bind $itk_component(text) <Key> {
	if [%W compare insert < limit] {
	    %W mark set insert end
	}
    }

    eval itk_initialize $args
}   

# ------------------------------------------------------------------
#                       
# ------------------------------------------------------------------

body CGShellWidget::EvalNoTyped {command} {

    if {$pris} {
 	after 10 [scope $this] EvalNoTyped $command
 	return
    }

    set pris 1
    set string "$command ([clock format [clock seconds] -format "%T"])\n"
    $itk_component(text) insert insert $string
    puts -nonewline $fidlog $string
    $itk_component(text) mark set limit insert

    set res [Eval $command]

    set pris 0
    
    return $res
}


body CGShellWidget::EvalTypein {} {

    set command [$itk_component(text) get limit end]

    set string " ([clock format [clock seconds] -format "%T"])\n"
    $itk_component(text) insert insert $string
    puts -nonewline $fidlog $string

    if [info complete $command] {
	$itk_component(text) mark set limit insert
	return [Eval $command]
    }
}

body CGShellWidget::Eval {command} {
    
    set result {}
    # la valeur seulement
    set rresult {}

    $itk_component(text) mark set insert end

    if [catch {Slave eval $command} result] {
	
	$itk_component(text) insert insert $result error
	puts -nonewline $fidlog $result
    } else {
	$itk_component(text) insert insert $result result
	puts -nonewline $fidlog $result
	set rresult $result
    }

    if {[$itk_component(text) compare insert != "insert linestart"]} {
	$itk_component(text) insert insert \n
	puts -nonewline $fidlog \n
    }

    $itk_component(text) insert insert $prompt prompt
    puts -nonewline $fidlog $prompt
    $itk_component(text) see insert
    $itk_component(text) mark set limit insert


    
    return $rresult
}

body CGShellWidget::ResetAlias {} {
    interp delete Slave

    interp create Slave
    #load {} Tk Slave
    interp alias Slave reset {} ResetAlias
    interp alias Slave puts {} $this PutsAlias
    interp alias Slave gets {} $this GetsAlias
    interp alias Slave open {} $this OpenAlias
    interp alias Slave close {} $this CloseAlias
    interp alias Slave winfo {} $this WinfoAlias
    interp alias Slave package {} $this PackageAlias
    interp alias Slave .cgpp {} $this CGPPAlias
  
}

body CGShellWidget::PutsAlias {args} {
    
    if {[llength $args] > 3}  {
	error "invalid arguments"
    }
    
    set newline "\n"
    if {[string match "-nonewline" [lindex $args 0]]} {
	set newline ""
	set args [lreplace $args 0 0]
    }
    if {[llength $args] == 1} {
	set chan stdout
	set string [lindex $args 0]$newline
    } else {
	set chan [lindex $args 0]
	set string [lindex $args 1]$newline
    }
    if [regexp (stdout) $chan] {
	mark gravity limit right
	insert limit $string output
	puts -nonewline $fidlog $string
	see limit
	mark gravity limit left
    } elseif [regexp (stderr) $chan] {
	mark gravity limit right
	insert limit $string error
	puts -nonewline $fidlog $string
	see limit
	mark gravity limit left
    } else {
        #puts stdout "About to puts $chan \"$string\""
	puts -nonewline $chan $string
    }
}

body CGShellWidget::GetsAlias {args} {
    
    return [gets [lindex $args 0]]

}


body CGShellWidget::OpenAlias {args} {
        return [open [lindex $args 0] [lindex $args 1]]
}

body CGShellWidget::CloseAlias {args} {
        return [close [lindex $args 0]]
}

body CGShellWidget::WinfoAlias {args} {
        return [winfo [lindex $args 0] [lindex $args 1]]
}

body CGShellWidget::PackageAlias {args} {
        return [package [lindex $args 0] [lindex $args 1]]
}

body CGShellWidget::CGPPAlias {args} {
	if {[llength $args]==1} {
        return [.cgpp [lindex $args 0]]
  } else {
        return [.cgpp [lindex $args 0] [lindex $args 1]]
  }
}

