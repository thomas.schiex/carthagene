#-----------------------------------------------------------------------------
# 
# $Id: cgsave.test.tcl,v 1.1 2003-01-14 17:23:04 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test cgsave-1.1 {complet et normal} {
    cgrestart
    dsload Data/bc.cg
    dsload Data/rh.cg
    dsmergor 1 2
    mrkmerge [mrkid  MS9] [mrkid MS10]
    mrkmerge [mrkid  MS12] [mrkid MS14]
    mrkmerge [mrkid  MS12] [mrkid MS13]
    sem
    annealing 1 100.0 0.1 0.5
    cgsave tmp
    set temoin [heapget k 0]
    set temoinu {}
    foreach map $temoin {
	lappend temoinu [lreplace $map 0 0]
    }
    unset temoin
    cgrestart
    source tmp
    exec rm tmp
    set teste  [heapget k 0]
    set testu {}
    foreach map $teste {
	lappend testu [lreplace $map 0 0]
    }
    unset teste
    string compare [join $temoinu] [join $testu]} {0}