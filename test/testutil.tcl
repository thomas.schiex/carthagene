#-----------------------------------------------------------------------------
# 
# $Id: testutil.tcl,v 1.2 2002-10-16 15:59:20 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : utilitaires pour tester les sorties �crans.
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# proc�dures permettant d'expoiter la trace
# debutrace se positionne au d�but du texte � tester
# litrace lit le texte � tester et transforme en liste testable
#
proc debutrace {} {
    global tracid
    global argv0
    set postest [string first "tcl" $argv0]
    puts $postest
    set firesnom "./"
    append firesnom [string range $argv0 0 [expr $postest - 1]]
    append firesnom "res"
    set tracid [ open $firesnom r]
    while {[gets $tracid ligne] >= 0} {}
}

proc litrace {} {
    global tracid
    
    while {[gets $tracid ligne] >= 0} {
	lappend buftra $ligne 
    }
    close $tracid
    return [lrange $buftra 0 [expr [llength $buftra] - 1]]
}

