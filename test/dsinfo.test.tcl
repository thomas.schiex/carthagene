#-----------------------------------------------------------------------------
# 
# $Id: dsinfo.test.tcl,v 1.1 2003-01-09 10:55:11 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test dsinfo-1.1 {jeu unique haploid RH} {
    cgrestart
    dsload Data/rh.cg
    debutrace
    dsinfo
    litrace} {{} {Data Sets :} ----------: {ID        Data Type    markers individuals             filename constraints merging} { 1       haploid RH         53         118                rh.cg}}

test dsinfo-1.2 {jeu unique backcross} {
    cgrestart
    dsload Data/bc.cg
    debutrace
    dsinfo
    litrace} {{} {Data Sets :} ----------: {ID        Data Type    markers individuals             filename constraints merging} { 1     f2 backcross         20         208                bc.cg}}

test dsinfo-1.3 {jeu unique intercross} {
    cgrestart
    dsload Data/mouse.raw
    debutrace
    dsinfo
    litrace} {{} {Data Sets :} ----------: {ID        Data Type    markers individuals             filename constraints merging} { 1       intercross        308          46            mouse.raw}}

test dsinfo-2.1 {plusieurs jeux} {
    cgrestart
    dsload Data/bc.cg
    dsload Data/con.cg
    debutrace
    dsinfo
    litrace} {{} {Data Sets :} ----------: {ID        Data Type    markers individuals             filename constraints merging} { 1     f2 backcross         20         208                bc.cg} { 2       constraint          4                           con.cg           2}}

test dsinfo-2.2 {plus de  jeux} {
    cgrestart
    dsload Data/rh.cg
    dsload Data/bc.cg
    dsload Data/con.cg
    debutrace
    dsinfo
    litrace} {{} {Data Sets :} ----------: {ID        Data Type    markers individuals             filename constraints merging} { 1       haploid RH         53         118                rh.cg} { 2     f2 backcross         20         208                bc.cg} { 3       constraint          4                           con.cg           2}}

test dsinfo-3.1 {fusions sur l'ordre} {
    cgrestart
    dsload Data/rh.cg
    dsload Data/bc.cg
    dsload Data/con.cg
    dsmergor 1 2
    dsmergor 3 4
    debutrace
    dsinfo
    litrace} {{} {Data Sets :} ----------: {ID        Data Type    markers individuals             filename constraints merging} { 1       haploid RH         53         118                rh.cg} { 2     f2 backcross         20         208                bc.cg} { 3       constraint          4                           con.cg           2} { 4  merged by order         59         326                                    1   2} { 5  merged by order         59         328                                    3   4}}
