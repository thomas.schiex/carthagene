#-----------------------------------------------------------------------------
# 
# $Id: heapsizeget.test.tcl,v 1.1 2003-01-09 12:50:50 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : heapsize
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#

source defs
set VERBOSE 1
#
# tester ce qu'il y a dans le tas
#

test heapsizeget-1.1 {agrandissement du tas} {
    cgrestart
    heapsize 7
    dsload Data/bc1.cg
    sem
    flips 3 3.0 1
    heapsize 127
    heapsizeget} {127}