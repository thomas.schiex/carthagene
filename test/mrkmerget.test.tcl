#-----------------------------------------------------------------------------
# 
# $Id: mrkmerget.test.tcl,v 1.2 2003-01-10 09:57:12 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test mrkmerget-0.1 {pas de donn�es} {
    cgrestart
    mrkmerget} {}

test mrkmerget-1.1 {simple} {
    cgrestart
    dsload Data/bc.cg
    mrkmerge [mrkid  MS9] [mrkid MS10]
    mrkmerge [mrkid  MS12] [mrkid MS14]
    mrkmerge [mrkid  MS12] [mrkid MS13]
    mrkmerget} {{9 10 } {12 14 13 }}