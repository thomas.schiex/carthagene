#-----------------------------------------------------------------------------
# 
# $Id: dsload.test.tcl,v 1.1 2003-01-09 10:55:11 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test dsload-1.1 {haploid RH} {
    cgrestart
    dsload Data/rh.cg} {{1 haploid RH 53 118 /home/chabrier/CartaGene/dev/test/Data/rh.cg}}

test dsload-1.2 {backcross} {
    cgrestart
    dsload Data/bc.cg} {{1 f2 backcross 20 208 /home/chabrier/CartaGene/dev/test/Data/bc.cg}}

test dsload-1.3 {intercross} {
    cgrestart
    dsload Data/mouse.raw} {{1 intercross 308 46 /home/chabrier/CartaGene/dev/test/Data/mouse.raw}}

test dsload-1.4 {constraint} {
    cgrestart
    dsload Data/bc.cg
    dsload Data/con.cg} {{2 constraint 2 /home/chabrier/CartaGene/dev/test/Data/con.cg}}


