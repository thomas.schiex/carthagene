#-----------------------------------------------------------------------------
# 
# $Id: dsmergen.test.tcl,v 1.1 2003-12-02 15:08:18 chabrier Exp $
#
# Module        : test
# Projet        : CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test dsmergen-1.1 { Three ri self} {
    cgrestart
    dsload Data/IL.txt
    dsload Data/IP.txt
    dsload Data/LP.txt
    dsmergen 1 2
    dsmergen 3 4} {{5 merged genetic 206 144}}
