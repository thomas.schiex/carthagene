set arrow 1 from 7,graph 0 to 7,-1013.383 nohead lt 3 lw 1
set arrow 2 from 41,graph 0 to 41,-957.848 nohead lt 4 lw 1
set title 'Mouse chromosome 1'
set ylabel 'Log10-likelihood'
set xlabel 'Number of breakpoints with human genome'
set key left top

plot "chr1pareto" using 4:6 title 'Traditional mapping criterion' with linespoints, "chr1pareto" using 4:8 title 'Comparative mapping criterion' with linespoints, "chr1balanced" using 4:8 notitle with points, "chr1rh" using 4:6 notitle with points

pause -1

set term postscript eps color dashed "helvetica" 18
set output "chr1mlecolor.eps"
replot

set term postscript eps monochrome dashed "helvetica" 18
set output "chr1mle.eps"
replot
