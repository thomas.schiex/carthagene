#-----------------------------------------------------------------------------
# 
# $Id: mapordget.test.tcl,v 1.1 2003-01-10 09:57:12 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test mapordget-0.1 {pas de donn�es} {
    cgrestart
    mapordget 0} {}

test maporget-1.1 {simple} {
    cgrestart
    dsload Data/bc.cg
    sem
    mapordget 0} {1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20}