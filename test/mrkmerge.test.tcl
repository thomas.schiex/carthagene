#-----------------------------------------------------------------------------
# 
# $Id: mrkmerge.test.tcl,v 1.1 2003-01-10 09:57:12 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biométrie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "système de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test mrkmerge-1.1 {fusion de marqueurs sélectionnés} {
    cgrestart
    dsload Data/bc.cg
    mrkmerge [mrkid  MS9] [mrkid MS10]
    mrkmerge [mrkid  MS12] [mrkid MS14]
    mrkmerge [mrkid  MS12] [mrkid MS13]
    mrkselget} {1 2 3 4 5 6 7 8 9 11 12 15 16 17 18 19 20}

test mrkmerge-1.2 {fusion de marqueurs NON sélectionnés} {
    cgrestart
    dsload Data/bc.cg
    mrkselset {1 2 3 4 5 6 7 8 9 10 11 15 16 17 18 19 20}
    mrkmerge [mrkid  MS9] [mrkid MS10]
    mrkmerge [mrkid  MS12] [mrkid MS14]
    mrkmerge [mrkid  MS12] [mrkid MS13]
    mrkselget} {1 2 3 4 5 6 7 8 9 11 15 16 17 18 19 20}
