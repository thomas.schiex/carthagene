#-----------------------------------------------------------------------------
# 
# $Id: heapsize.test.tcl,v 1.4 2003-01-09 12:50:50 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : heapsize
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#

source defs
set VERBOSE 1
#
# tester ce qu'il y a dans le tas
#

test heapsize-1.1 {agrandissement du tas} {
    cgrestart
    heapsize 7
    dsload Data/bc1.cg
    sem
    flips 3 3.0 1
    heapsize 127
    heapget k 0} {{6 -451.32 {1 -451.32 MS4 0.0 MS5 3.0 MS6 6.7 MS11 28.2 MS13 36.2 MS16 48.9 MS17 64.7 MS8 105.1 MS7 111.0 MS3 125.2 MS2 125.8 MS9 149.4 MS12 164.7 MS15 168.9 MS19 200.8 MS20 203.6 MS1 393.6}} {5 -451.97 {1 -451.97 MS4 0.0 MS5 3.0 MS6 6.7 MS11 28.2 MS13 36.2 MS16 48.9 MS17 64.7 MS8 105.1 MS7 111.0 MS2 125.7 MS3 126.3 MS9 149.8 MS12 165.1 MS15 169.2 MS19 200.9 MS20 203.7 MS1 393.8}} {4 -452.11 {1 -452.11 MS4 0.0 MS5 3.0 MS6 6.7 MS11 28.2 MS13 36.2 MS16 48.9 MS17 64.7 MS8 105.1 MS7 111.0 MS3 125.2 MS2 125.8 MS9 149.4 MS12 164.7 MS15 168.9 MS20 202.8 MS19 205.6 MS1 395.6}} {3 -452.77 {1 -452.77 MS4 0.0 MS5 3.0 MS6 6.7 MS11 28.2 MS13 36.2 MS16 48.9 MS17 64.7 MS8 105.1 MS7 111.0 MS2 125.7 MS3 126.3 MS9 149.8 MS12 165.0 MS15 169.3 MS20 202.9 MS19 205.7 MS1 395.7}} {2 -454.09 {1 -454.09 MS6 0.0 MS5 4.0 MS4 7.1 MS11 35.2 MS13 43.2 MS16 55.9 MS17 71.7 MS8 112.2 MS7 118.1 MS3 132.2 MS2 132.9 MS9 156.4 MS12 171.8 MS15 176.0 MS19 207.9 MS20 210.7 MS1 400.7}} {1 -454.58 {1 -454.58 MS4 0.0 MS5 3.0 MS6 6.9 MS11 29.9 MS13 37.3 MS17 68.2 MS16 86.9 MS8 113.6 MS7 119.6 MS3 134.0 MS2 134.6 MS9 158.2 MS12 173.5 MS15 177.7 MS19 209.6 MS20 212.5 MS1 402.5}} {0 -454.74 {1 -454.74 MS6 0.0 MS5 4.0 MS4 7.1 MS11 35.2 MS13 43.2 MS16 55.9 MS17 71.7 MS8 112.2 MS7 118.1 MS2 132.8 MS3 133.4 MS9 156.9 MS12 172.1 MS15 176.3 MS19 208.0 MS20 210.8 MS1 400.8}}}

test heapsize-1.2 {r�duction du tas plein} {    
    cgrestart
    heapsize 7
    dsload Data/bc1.cg
    sem
    flips 3 3.0 1
    heapsize 3
    heapget k 0} {{2 -451.32 {1 -451.32 MS4 0.0 MS5 3.0 MS6 6.7 MS11 28.2 MS13 36.2 MS16 48.9 MS17 64.7 MS8 105.1 MS7 111.0 MS3 125.2 MS2 125.8 MS9 149.4 MS12 164.7 MS15 168.9 MS19 200.8 MS20 203.6 MS1 393.6}} {1 -451.97 {1 -451.97 MS4 0.0 MS5 3.0 MS6 6.7 MS11 28.2 MS13 36.2 MS16 48.9 MS17 64.7 MS8 105.1 MS7 111.0 MS2 125.7 MS3 126.3 MS9 149.8 MS12 165.1 MS15 169.2 MS19 200.9 MS20 203.7 MS1 393.8}} {0 -452.11 {1 -452.11 MS4 0.0 MS5 3.0 MS6 6.7 MS11 28.2 MS13 36.2 MS16 48.9 MS17 64.7 MS8 105.1 MS7 111.0 MS3 125.2 MS2 125.8 MS9 149.4 MS12 164.7 MS15 168.9 MS20 202.8 MS19 205.6 MS1 395.6}}}



