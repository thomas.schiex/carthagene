#-----------------------------------------------------------------------------
# 
# $Id: dsget.test.tcl,v 1.1 2003-01-09 10:55:11 chabrier Exp $
#
# Module 	: test
# Projet	: CartaGene
#
# Description : 
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# initialisation du "syst�me de test" de Tcl
#
source defs

set VERBOSE 1

source testutil.tcl

test dsget-1.1 {jeu unique haploid RH} {
    cgrestart
    dsload Data/rh.cg
    dsget} {{1 "radiated hybrid" /home/chabrier/CartaGene/dev/test/Data/rh.cg 53 118}}

test dsget-1.2 {jeu unique backcross} {
    cgrestart
    dsload Data/bc.cg
    dsget} {{1 "f2 backcross" /home/chabrier/CartaGene/dev/test/Data/bc.cg 20 208}}

test dsget-1.3 {jeu unique intercross} {
    cgrestart
    dsload Data/mouse.raw
    dsget} {{1 "intercross" /home/chabrier/CartaGene/dev/test/Data/mouse.raw 308 46}}

test dsget-2.1 {plusieurs jeux} {
    cgrestart
    dsload Data/bc.cg
    dsload Data/con.cg
    dsget} {{1 "f2 backcross" /home/chabrier/CartaGene/dev/test/Data/bc.cg 20 208} {2 "constraint" /home/chabrier/CartaGene/dev/test/Data/con.cg 2}}

test dsget-2.2 {plus de  jeux} {
    cgrestart
    dsload Data/rh.cg
    dsload Data/bc.cg
    dsload Data/con.cg
    dsget} {{1 "radiated hybrid" /home/chabrier/CartaGene/dev/test/Data/rh.cg 53 118} {2 "f2 backcross" /home/chabrier/CartaGene/dev/test/Data/bc.cg 20 208} {3 "constraint" /home/chabrier/CartaGene/dev/test/Data/con.cg 2}}

test dsget-3.1 {fusions sur l'ordre} {
    cgrestart
    dsload Data/rh.cg
    dsload Data/bc.cg
    dsload Data/con.cg
    dsmergor 1 2
    dsmergor 3 4
    dsget} {{1 "radiated hybrid" /home/chabrier/CartaGene/dev/test/Data/rh.cg 53 118} {2 "f2 backcross" /home/chabrier/CartaGene/dev/test/Data/bc.cg 20 208} {3 "constraint" /home/chabrier/CartaGene/dev/test/Data/con.cg 2} {4 "merged by order" 1 2 59 326} {5 "merged by order" 3 4 59 328}}
