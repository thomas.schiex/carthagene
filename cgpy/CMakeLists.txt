INCLUDE(FindPythonLibs)
INCLUDE(FindPythonInterp)
MESSAGE(STATUS "I: ${PYTHON_INCLUDE_PATH} L: ${PYTHON_LIBRARIES}")

EXECUTE_PROCESS(COMMAND ${PYTHON_EXECUTABLE} -c "import sys; print sys.path[1]" OUTPUT_VARIABLE PYTHON_INSTALL_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
MESSAGE(STATUS "python install dir : ${PYTHON_INSTALL_DIR}")

ADD_LIBRARY(cgpy SHARED ${calcul_src} cgpy.cc)
# FIXME ! use Find*** something for python-dev and boost::python !
SET_TARGET_PROPERTIES(cgpy PROPERTIES
	COMPILE_FLAGS "-I${PYTHON_INCLUDE_PATH} -ggdb -Wall -Wno-unused-result"
	LINK_FLAGS "-lboost_python ${PYTHON_LIBRARIES}"
	LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib/python/CarthaGene"
)

configure_file(__init__.py ${CMAKE_BINARY_DIR}/lib/python/CarthaGene/__init__.py COPYONLY)
INSTALL(FILES __init__.py DESTINATION ${PYTHON_INSTALL_DIR}/CarthaGene)
INSTALL(TARGETS cgpy DESTINATION ${PYTHON_INSTALL_DIR}/CarthaGene)

