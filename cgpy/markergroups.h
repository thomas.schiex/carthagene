#ifndef _CGPY_MARKERGROUPS_H_
#define _CGPY_MARKERGROUPS_H_
#include "inc.h"

class MarkerGroups {
	void _mk_group(NodintPtr p) {
		list x;
		while(p) {
			x.append(Marker(p->vertex));
			p=p->next;
		}
		groups.append(x);
	}
	void _mk_groups(NodptrPtr p) {
		while(p) {
			_mk_group(p->first);
			p=p->next;
		}
	}
public:
	double dt, lt;
	list groups;
	MarkerGroups() : dt(Cartage.DisThres), lt(Cartage.LODThres), groups() {
		_mk_groups(Cartage.Group);
	}
};



#endif

