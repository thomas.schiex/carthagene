#ifndef _CGPY_DATASET_H_
#define _CGPY_DATASET_H_
#include "inc.h"

class DataSet {
private:
	friend class CGContext;
	BioJeu* b;
	int tp_mat_size;
	DoubleVector* tp_fr;
	DoubleVector* tp_dh;
	DoubleVector* tp_lod;

	void build_matrices() {
		switch(b->Cross) {
		case Mge:
		case Mor:
		case Ordre:
		case Con:
			tp_mat_size=0;
			return;
		default:;
		};
		tp_mat_size = b->NbMarqueur+1;
		tp_fr = new DoubleVector[tp_mat_size];
		tp_dh = new DoubleVector[tp_mat_size];
		tp_lod = new DoubleVector[tp_mat_size];
		for(int i=0;i<tp_mat_size;++i) {
			tp_fr[i] = DoubleVector(&b->TwoPointsFR[i], &tp_mat_size);
			tp_dh[i] = DoubleVector(&b->TwoPointsDH[i], &tp_mat_size);
			tp_lod[i] = DoubleVector(&b->TwoPointsLOD[i], &tp_mat_size);
		}
		twoPointsFR.set_buffer(&tp_fr, &tp_mat_size);
		twoPointsDH.set_buffer(&tp_dh, &tp_mat_size);
		twoPointsLOD.set_buffer(&tp_lod, &tp_mat_size);
	}

	void build_markers() {
		int* im = b->_imrk();
		DataSet* ds;
		switch(b->Cross) {
		case Mge:
		case Mor:
			ds = extract<DataSet*>(data[0]);
			markers.append(ds->markers);
			ds = extract<DataSet*>(data[1]);
			markers.append(ds->markers);
			break;
		default:;
			for(int i=0;i<b->NbMarqueur;++i) {
				markers.insert(i, Marker(Cartage.NomMarq[im[i+1]], im[i+1]));
			}
		};
	}

	void build_data() {
		BioJeuMerged*bjm;
		BioJeuSingle*bjs;
		switch(b->Cross) {
		case Mor:
		case Mge:
			bjm = dynamic_cast<BioJeuMerged*>(b);
			cout << "Build_Data Merged" << bjm->BJgauche << " " << bjm->BJdroite << endl;
			data.append(DataSet(bjm->BJgauche));
			data.append(DataSet(bjm->BJdroite));
			break;
		case Con:
			break;
		case Ordre:
			break;
		case Unk:
			break;
		default:
			bjs = dynamic_cast<BioJeuSingle*>(b);
			cout << "Build_Data Single " << type() << ", " << bjs->NbMarqueur << " markers, " << bjs->TailleEchant << " samples" << endl;
			for(int i=0;i<bjs->NbMarqueur;++i) {
				/*data.append(new PyCArray<Obs>(&bjs->Echantillon[1+i], &bjs->TailleEchant));*/
				data.append(list(PyCArray<Obs>(&bjs->Echantillon[1+i], &bjs->TailleEchant)));
			}
			return;
		};
	}

	void build_all() {
		build_data();
		build_markers();
		build_matrices();
	}
public:
	list markers;
	list data;
	DoubleMatrix twoPointsFR;
	DoubleMatrix twoPointsDH;
	DoubleMatrix twoPointsLOD;

	DataSet() : b(0), markers(), data(), twoPointsFR(), twoPointsDH(), twoPointsLOD() {}
	DataSet(BioJeu*x) : markers(), data(), twoPointsFR(), twoPointsDH(), twoPointsLOD() { b=x; build_all(); }
	~DataSet() {}
	static DataSet* fromFile(object& filename) {
		DataSet* ret;
		Cartage.CharJeu(extract<char*>(filename)); 
		ret = new DataSet(Cartage.dernierJeuCharge);
		Marker::rebuild_list();
		return ret;
	}
	int cgid() const { return b?b->Id:-1; }
	static DataSet* clone(const DataSet& d) {
		DataSet* ret = new DataSet(Cartage.ClonaJeu(d.b->Id));
		return ret;
	}
	static list all() {
		list ret;
		for(int i=1;i<=Cartage.NbJeu;++i) {
			ret.insert(i-1, DataSet(Cartage.dernierJeuCharge));
		}
		return ret;
	}

	static DataSet* merge_gen(DataSet* d1, DataSet* d2) {
		Cartage.MerGen(d1->b->Id, d2->b->Id);
		return new DataSet(Cartage.ArbreJeu);
	}

	static DataSet* merge_ord(DataSet* d1, DataSet* d2) {
		cout << "debug merge_ord before" <<endl;
		Cartage.MergOr(d1->b->Id, d2->b->Id);
		cout << "debug merge_ord after : " << (void*)Cartage.ArbreJeu << endl;
		return new DataSet(Cartage.ArbreJeu);
	}

	static DataSet* rhconv(DataSet* rh, const char* newType) {
		int id = rh->b->Id;
		BioJeu* b = NULL;
		Cartage.StopFlag = 0;
		try {
			switch(Cartage.Jeu[id]->Cross) {
				case RH:
				case RHD:
					break;
				case RHE:
					PyErr_SetString(PyExc_TypeError, "RH with Errors DataSets can't be converted this way");
					throw_error_already_set();
				default:
					PyErr_SetString(PyExc_TypeError, "DataSet must be an haploid|diploid RH dataset");
					throw_error_already_set();
			};
			if(!strcmp(newType, "diploid")) {
				b=Cartage.ClonaJeu(id, RHD);
			} else if(!strcmp(newType, "haploid")) {
				b=Cartage.ClonaJeu(id, RH);
			} else if(!strcmp(newType, "error")) {
				b=Cartage.ClonaJeu(id, RHE);
			} else {
				PyErr_SetString(PyExc_TypeError, "DataSet must be an haploid|diploid RH dataset");
				throw_error_already_set();
			}
		} catch(BioJeu::NotImplemented& e) {
			PyErr_SetString(PyExc_TypeError, "DataSet must be an haploid|diploid RH dataset");
			throw_error_already_set();
		}
		fflush(stdout);
		return new DataSet(b);
	}

	char obs2chr(Obs o) const { try { return dynamic_cast<BioJeuSingle*>(b)->obs2chr(o); } catch(std::exception&e) { return '#'; } }

	std::string type() { const char*dt=b->GetDataType(); return std::string(dt?dt:"(unset)"); }

	double set_breakpoint_lambda(double coefAutoFit, double newlambda) const {
		return Cartage.SetBreakPointLambda(b->Id, coefAutoFit, newlambda);
	}
};



#endif

