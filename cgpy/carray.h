#ifndef _CGPY_CARRAY_H_
#define _CGPY_CARRAY_H_

#include "inc.h"

template<typename T> class PyCArray;

template<typename T>
std::ostream& operator << (std::ostream& os, PyCArray<T> p) {
	return os << p.__str__();
}

/* Helper for translating any C array into a Python list */
template <typename T>
class PyCArray {
protected:
	T** buffer;
	int* _size;
public:
	PyCArray<T>& operator=(const PyCArray<T>& p) { buffer=p.buffer; _size=p._size; return *this; }
	typedef T* iterator;
	PyCArray() : buffer(0), _size(0) { /*cout << "Default Ctor SHOULDN'T be used.";*/ }
	PyCArray(T** bufp, int* bufszp) { set_buffer(bufp, bufszp); }
	virtual ~PyCArray() {}

	void set_buffer(T** bufp, int* bufszp) {
		/*cout << "bufp=" << (void*)bufp << " bufszp=" << (void*)bufszp << endl;*/
		buffer = bufp;
		_size = bufszp;
		/*cout << "buffer=" << (void*)buffer << " _size=" << (void*)_size << endl;*/
	}

	int __len__() {
		return _size?*_size:0;
	}

	T* begin() { return buffer&&_size?*buffer:0; }
	T* end() { return buffer&&_size?*buffer+*_size:0; }

	T __getitem__(int index) {
		/*cout << "buf@" << (void*)buffer << '[' << *_size << "] #" << index << endl;*/
		if(!_size||index<0||index>=*_size) {
			PyErr_SetString(PyExc_IndexError, "Index out of range");
			throw_error_already_set();
		}
		return (*buffer)[index];
	}

	void __setitem__(int index, T value) {
		if(!(buffer&&_size)||index<0||index>=*_size) {
			PyErr_SetString(PyExc_IndexError, "Index out of range");
			throw_error_already_set();
		}
		(*buffer)[index] = value;
	}

	/*T& operator[](size_t i) { return (*buffer)[i]; }*/
	/*const T& operator[](size_t i) const { return (*buffer)[i]; }*/
	T operator[](size_t i) { return (*buffer)[i]; }
	const T operator[](size_t i) const { return (*buffer)[i]; }

	size_t size() const { return *_size; }

	std::string __str__() const {
		std::ostringstream oss;
		oss << '[';
		if(*_size>0) {
			oss << **buffer;
			for(int i=1;i<*_size;++i) {
				oss << ", " << (*buffer)[i];
			}
		}
		oss << ']';
		return oss.str();
	}
};


template <typename T>
class ManagedArray : public PyCArray<T> {
public:
	ManagedArray() : PyCArray<T>() {}
	ManagedArray(T** buf, int* sz) : PyCArray<T>(buf, sz) {}
	virtual ~ManagedArray() { delete[] *PyCArray<T>::buffer; }
};

typedef ManagedArray<int> IntArray;

#endif

