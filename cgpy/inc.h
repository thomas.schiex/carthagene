#ifndef _CGPY_INC_H_
#define _CGPY_INC_H_

#include "../config.h"

#include "../calcul/CartaGene.h"
#include "../calcul/Tas.h"
#include "../calcul/BioJeu.h"
#include "../calcul/BioJeuSingle.h"
#include "../calcul/BioJeuMerged.h"
#include "../calcul/BJS_OR.h"
#include "../calcul/BJS_CO.h"
#include "../calcul/tsp.h"
#include <boost/python.hpp>
#include <boost/python/copy_non_const_reference.hpp>
#include <boost/python/stl_iterator.hpp>
#include <vector>
#include <string>
#include <iostream>
/*#include <sstream>*/

using namespace boost::python;
/*using namespace std;*/
using std::cout;
using std::endl;


extern CartaGene Cartage;

void update_mapfactory_selections(int m2);

class Marker;
class DataSet;
class CGContext;
class MarkerGroups;

template <typename T> class PyCArray;

typedef PyCArray<Obs> ObsVector;
typedef PyCArray<double> DoubleVector;
typedef PyCArray<DoubleVector> DoubleMatrix;


#endif

