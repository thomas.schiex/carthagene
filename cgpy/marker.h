#ifndef _CGPY_MARKER_H_
#define _CGPY_MARKER_H_
#include "inc.h"

class Marker {
public:
	static list _all;
	std::string name;
	int id;
	Marker() : name(""), id(0) {}
	Marker(const char*n, int i) : name(n), id(i) {}
	Marker(int i) : name(Cartage.NomMarq[i]), id(i) {}
	~Marker() {}

	static void rebuild_list() {
		_all = list();
		for(int i=1;i<=Cartage.NbMarqueur;++i) {
			_all.append(Marker(Cartage.NomMarq[i], i));
		}
	}

	bool isMerged() const {
		return !!Cartage.Merged[id];
	}

	operator int() const { return id; }

	list mergedWith() {
		list ret;
		int i;
		/* add back-merges first */
		ret.append(this);
		i = Cartage.Merged[id];
		while(i) {
			ret.append(_all[i-1]);
			i=Cartage.Merged[i];
		}
		i=Cartage.Represents[id];
		while(i) {
			ret.append(_all[i-1]);
			i=Cartage.Represents[i];
		}
		return ret;
	}

	std::string to_str() const {
		return name;
	}

	std::string to_repr() const {
		std::ostringstream oss;
		oss << "<Marker #" << id << ' ' << name << '>';
		return oss.str();
	}

	bool __eq__(const Marker*c) const {
		return c->id==id;
	}

	int __cmp__(const Marker*c) const {
		return id-c->id;
	}

	int __hash__() const {
		return id;
	}

	static list all() { return _all; }

	static void merge(list dupes) {
		stl_input_iterator<object> begin(dupes), end;
		tuple t;
		int m1, m2;
		while(begin!=end) {
			t = extract<tuple>(*begin);
			m1 = extract<int>(t[0]);
			m2 = extract<int>(t[1]);
			while (Cartage.Merged[m2] != 0) { m2 = Cartage.Merged[m2]; }
			if(!Cartage.Merge(m1, m2)) {
				update_mapfactory_selections(m2);
			}
			++begin;
		}
	}
};



#endif

