#include "cgpy.h"

char bouf[2048];
char boufi[2048];
FILE *Fout=NULL;


CartaGene Cartage;

void hand(int sig)
{
  // enregistrement d'une demande d'arr�t.
#ifndef __WIN32__
  if (sig == SIGINT || sig == SIGXCPU) Cartage.StopFlag = 1;
  signal(SIGINT, hand);
  signal(SIGXCPU, hand);
#else
  if (sig == SIGINT) Cartage.StopFlag = 1;
  signal(SIGINT, hand);
#endif
}


class _cg_init {
public:
	_cg_init() {
		signal(SIGINT, hand);
#ifdef __WIN32__
		signal(SIGXCPU, hand);
#endif
		  Cartage.Version();
	}
} __CG_INIT;

list Marker::_all;
std::vector<CGContext*> CGContext::_all;


PyCArray<int>* carte_get_ordre(Carte* c) {
	return new PyCArray<int>(&c->ordre, &c->NbMarqueur);
}


PyCArray<double>* carte_get_tr(Carte* c) {
	return new PyCArray<double>(&c->tr, &c->NbMarqueur);
}


void update_mapfactory_selections(int m2) {
	std::vector<CGContext*>::iterator begin=CGContext::_all.begin(), end=CGContext::_all.end();
	int* sel, *selm2;
	CGContext*mf;
	while(begin!=end) {
		mf = *begin;
		cout << "on MapFactory " << mf << endl;
		sel = mf->cur.sel;
		selm2=sel;
		while((selm2-sel)<mf->cur.selsz&&*selm2!=m2) { ++selm2; }
		if((selm2-sel)<mf->cur.selsz) {
			memmove(selm2, selm2+1, sizeof(int)*(mf->cur.selsz-(selm2-sel)));
			--mf->cur.selsz;
			cout << "  <-> removed " << m2 << endl;
			mf->reinit_sel_list();
		} else {
			cout << "  <-> couldn't find " << m2 << endl;
		}
		++begin;
	}
}




#define enumval(x) value(#x, x)

#define DECL_ARRAY_TYPE_RO(__t, __n, __doc) class_<__t >(__n, __doc, no_init) \
		.def("__len__", &__t::__len__) \
		.def("__getitem__", &__t::__getitem__) \
		.def("__str__", &__t::__str__) \
		.def("__repr__", &__t::__str__) \
		.def("__iter__", iterator<__t >())

#define DECL_ARRAY_TYPE(__t, __n, __doc) class_<__t >(__n, __doc, no_init) \
		.def("__len__", &__t::__len__) \
		.def("__getitem__", &__t::__getitem__) \
		.def("__setitem__", &__t::__setitem__) \
		.def("__str__", &__t::__str__) \
		.def("__repr__", &__t::__str__) \
		.def("__iter__", iterator<__t >())


#define DECL_ARRAY_TYPE_RETURNING_OBJ_RO(__t, __n, __doc) class_<__t >(__n, __doc, no_init) \
		.def("__len__", &__t::__len__) \
		.def("__getitem__", &__t::__getitem__, return_value_policy<return_by_value>()) \
		.def("__str__", &__t::__str__) \
		.def("__repr__", &__t::__str__) \
		.def("__iter__", iterator<__t >())



void quietset(int flag) {
	Cartage.StopFlag = 0;
	Cartage.SetQuiet(flag);
}

void verbset(int flag) {
	Cartage.StopFlag = 0;
	Cartage.SetVerbose(flag);
}

void setrobustness(double r) {
	Cartage.SetRobustness(r);
}

void setnotrobust(double r) {
	Cartage.SetRobustness(1e100);
}

void settol(double fine, double coarse) {
	Cartage.SetTolerance(fine, coarse);
}

void cgversion() {
	Cartage.Version();
}

void cgstop() {
	Cartage.StopFlag = 0;
	raise(SIGINT);
	fflush(stdout);
}

void affmarq() {
	Cartage.StopFlag = 0;
	Cartage.AffMarq();
	fflush(stdout);
}

BOOST_PYTHON_MODULE(libcgpy) {
	def("set_verbose_flag", &verbset, (arg("verbose")), "Set verbosity flag");
	def("set_quiet_flag", &quietset, (arg("quiet")), "Make CarthaGene more quiet");
	def("set_tolerance", &settol, (arg("tol")), "Set the tolerance threshold");
	def("set_robustness", &setrobustness, (arg("robustness")), "Set the robustness threshold");
	def("set_not_robust", &setnotrobust, "Deactivate robustness");
	def("version", &cgversion, "Display version");
	def("stop", &cgstop, "Stop the currently running command");
	enum_<Obs>("Obs", "Enumeration of possible observation values")
		.enumval(Obs0000)
		.enumval(Obs0001)
		.enumval(Obs0010)
		.enumval(Obs0011)
		.enumval(Obs0100)
		.enumval(Obs0101)
		.enumval(Obs0110)
		.enumval(Obs0111)
		.enumval(Obs1000)
		.enumval(Obs1001)
		.enumval(Obs1010)
		.enumval(Obs1011)
		.enumval(Obs1100)
		.enumval(Obs1101)
		.enumval(Obs1110)
		.enumval(Obs1111)
		.export_values()
	;
	DECL_ARRAY_TYPE_RO(PyCArray<Obs>, "ObsList", "An immutable list of observations for a given marker");
	DECL_ARRAY_TYPE_RO(PyCArray<int>, "IntList", "An immutable list of integers");
	DECL_ARRAY_TYPE_RO(PyCArray<double>, "DoubleList", "An immutable list of double-precision floating-point values");
	DECL_ARRAY_TYPE_RETURNING_OBJ_RO(PyCArray<PyCArray<double> >, "DoubleMatrix", "An immutable list of lists of double-precision floating-point values (a matrix, row-major)");
	class_<Marker>("Marker", "Representation of a marker", no_init)
		/* properties */
		.def_readonly("name", &Marker::name, "The symbolic name of this marker")
		.def_readonly("id", &Marker::id, "The id of this marker inside the CarthaGene system")
		/* methods */
		.def("info", &affmarq, "Tell which data set this marker belongs to")
		.def("isMerged", &Marker::isMerged, "Predicate telling whether this marker has been merged into another one or not")
		.def("mergedWith", &Marker::mergedWith, "The full list of merged markers this marker belongs to (empty if the marker is not merged)")
		.def("__str__", &Marker::to_str)
		.def("__repr__", &Marker::to_repr)
		.def("__int__", &Marker::operator int, "Another way of retrieving the marker id")
		.def("__eq__", &Marker::__eq__, "Compare two markers")
		.def("__hash__", &Marker::__hash__, "Hash this marker")
		/* static methods */
		.def("all", &Marker::all, "The list of all markers known to the system").staticmethod("all")
		.def("merge", &Marker::merge, (arg("duplicates")), "Merge some markers. Argument is a list of 2-uples describing (marker to merge into, marker to be merged)").staticmethod("merge")
	;
	class_<DataSet>("DataSet", "A data set", no_init)
		/* properties */
		.def_readonly("markers", &DataSet::markers, "All the markers present in this dataset")
		.def_readonly("data", &DataSet::data, "All the information given by this dataset (only defined where applicable)")
		.add_property("type", &DataSet::type, "The type of this dataset")
		.add_property("id", &DataSet::cgid, "The id of this dataset inside the CarthaGene system")
		.def_readonly("twoPointsFR", &DataSet::twoPointsFR, "The 2pt FR matrix of this dataset")
		.def_readonly("twoPointsDH", &DataSet::twoPointsDH, "The 2pt DH matrix of this dataset")
		.def_readonly("twoPointsLOD", &DataSet::twoPointsLOD, "The 2pt LOD matrix of this dataset")
		/* methods */
		.def("obs2chr", &DataSet::obs2chr, (arg("obs_value")), "Convert an observation value to its corresponding character")
		/* static methods */
		.def("load", &DataSet::fromFile, (arg("filename")),
			return_value_policy<manage_new_object>(), "Create a new DataSet by loading data from a file")
			.staticmethod("load")
		.def("clone", &DataSet::clone, (arg("dataset")),
			return_value_policy<manage_new_object>(), "Create a new DataSet by cloning an existing DataSet")
			.staticmethod("clone")
		.def("rhconv", &DataSet::rhconv, (arg("dataset"), arg("new_RH_type")),
			return_value_policy<manage_new_object>(), "Clone an existing RH DataSet and convert the clone to another RH model (one of 'haploid', 'diploid', 'error')")
			.staticmethod("rhconv")
		.def("merge_gen", &DataSet::merge_gen, (arg("genetic_dataset1"), arg("genetic_dataset2")),
			return_value_policy<manage_new_object>(), "Merge two genetic datasets (static method)")
			.staticmethod("merge_gen")
		.def("merge_ord", &DataSet::merge_ord, (arg("genetic_dataset"), arg("order_dataset")),
			return_value_policy<manage_new_object>(), "Merge a dataset with an order dataset")
			.staticmethod("merge_ord")
		.def("set_breakpoint_lambda", &DataSet::set_breakpoint_lambda, (arg("coef_autofit"), arg("new_lambda")),
			"Set the breakpoint lambda values")
		.def("all", &DataSet::all, "Get the full list of currently loaded datasets").staticmethod("all")
	;
	class_<Carte>("Map", "A genetic/RH map", no_init)
		.def_readonly("converged", &Carte::Converged, "Convergence value")
		.def_readonly("id", &Carte::Id, "Id of this map inside the CarthaGene system")
		.def_readonly("n_markers", &Carte::NbMarqueur, "Number of markers in this map")
		.def_readonly("em_cost", &Carte::coutEM, "The EM cost of this map")
		.def_readonly("ret", &Carte::ret, "The retention rate of this map")
		.def("order", carte_get_ordre, return_value_policy<manage_new_object>(), "The marker order for this map")
		.def("tr", carte_get_tr, return_value_policy<manage_new_object>(), "The recombination rates for this map")
		.def("unconverge", &Carte::UnConverge, "Force a modified map to be considered as non estimated")
		.def("canonify", &Carte::Canonify, "Canonify a MAP.\nOrder is lexicographically minimized.")
		.def("canonify_mor", &Carte::CanonifyMor, "Canonify a MAP(Merged by order).")
		.def("__eq__", &Carte::SameMaps, "Compare two maps")
	;
	/*class_<CGContext>("MapFactory", "A context for creating maps", init<object, object>())*/
	class_<CGContext>("MapFactory", "A context for creating maps", init<object, object>(args("dataset", "marker_selection"), "Create a new context to build maps using a specific data set (single or merged) and a selection of markers (can be a list of marker IDs (int) or a list of Marker instances)"))
		.def_readonly("selection", &CGContext::seq, "(read-only) The marker selection this map factory relies on")
		.def_readonly("dataset", &CGContext::ds, "(read-only) The DataSet this map factory relies on")
		.def("all", &CGContext::all, "Get the full list of all currently active map factories").staticmethod("all")

		.def("groups", &CGContext::groups, return_value_policy<manage_new_object>(), "Find groups of markers within the selection")
		.def("find_duplicate_markers", &CGContext::dupes, "Find mergeable markers within the selection")

		.def("best", &CGContext::best, return_internal_reference<>(), "Fetch the current best map")
		.def("worst", &CGContext::worst, return_internal_reference<>(), "Fetch the current worst map")
		.def("equi", &CGContext::equi, "Set the equivalence flag of the heap")
		.def("stat", &CGContext::stat, "Display statistics about the heap of maps")
		.def("delta", &CGContext::delta, "Get the difference of likelihood between the best and second-to-best maps")
		.add_property("maps", &CGContext::maps, "Get the list of all maps in the heap")
		/* Printing */
		.def("printmap", &CGContext::print, (arg("map")), "Display a map")
		.def("printdmap", &CGContext::printd, (arg("map")), "Display a map")
		/* Map methods */
		.def("sem", &CGContext::sem,
			return_internal_reference<>(), "Compute a Single map (Single EM).\nsem uses the order of the selection of loci to compute a single map. This command also try to improve the heap with the resulting map.")
		.def("nicemapd", &CGContext::nicemapd,
			return_internal_reference<>(), "Provide quickly a (nice) map, using the 2-points distances.\nnicemapd uses the two points distance criterion to provide a map. Starting from each locus, the command builds gradually the best neighborhoods maps. At the end of the process, the command tries to insert the best map into the heap.")
		.def("nicemapl", &CGContext::nicemapl,
			return_internal_reference<>(), "Provide quickly a (nice) map, using the 2-points LOD criteria.\nnicemapl uses the two points LOD criterion to provide a map. Starting from each locus, the command builds gradually the best neighborhoods maps. At the end of the process, the command tries to insert the best map into the heap.")
		.def("mfmapd", &CGContext::mfmapd,
			return_internal_reference<>(), "Provide quickly a (nice) map, using the 2-points distances.\nmfmapd uses the two points distance criterion to provide a map. The command builds gradually a map by inserting the best (whose distances are minima) available pairs of markers. At the end of the process, the command tries to insert the map into the heap.")
		.def("mfmapl", &CGContext::mfmapl,
			return_internal_reference<>(), "Provide quickly a (nice) map, using the 2-points LOD criteria.\nmfmapl uses the two points LOD criterion to provide a map. The command builds gradually a map by inserting the best (whose LOD criteria are minima) available pairs of markers. At the end of the process, the command tries to insert the map into the heap.")
		.def("build", &CGContext::build, (arg("nc")),
			return_internal_reference<>(), "Build maps with two-points informations.\nbuild uses the two points LOD score criterion to provide maps. The command starts from a couple of loci. At each step of the algorithme, a locus is choosen and added to the previous map. All the maps correspondind to all the possible positions of the new marker are computed. The NbMap argument enable the command to build several maps at the same time. At the end of the process, the maps improving the current solution are inserted into the heap.")
		.def("annealing", &CGContext::annealing, (arg("tries"), arg("tInit"), arg("tFinal"), arg("cooling")),
			return_internal_reference<>(), "Find good maps using the annealing algorithm.\nannealing uses the multipoint LOD score criterion to provide maps. The command starts form the best map stored into the heap. Each time a better map is found, it is stored into the heap. Two changes and Three changes are randomly applied to the current map. The InitTemp argument correspond to the initial temperature. The FinalTemp argument correspond to the final temperature. The Cooling argument correspond to the cooling speed. The NbTries argument allows the user to increase the number of maps computed at each temperature.")
		.def("greedy", &CGContext::greedy, (arg("nr"), arg("ni"), arg("t_min"), arg("t_max"), arg("ratio")),
			return_internal_reference<>(), "Find good maps using the greedy algorithm.\ngreedy uses the multipoint LOD score criterion to provide maps. The command starts form the best map stored into the heap. Each time a better map is found, it is stored into the heap. The TabooMin argument correspond to the minimum size of the taboo list. The TabooMax argument correspond to the maximum size of the taboo list.The NbLoop argument correspond to the number of main loop to provide. The NbIter argument allows the user to increase the number of maps computed at each loop. The Anytime ratio controls the tradeoff between search speed and solution quality.")
		.def("algogen", &CGContext::algogen, (arg("n_gens"), arg("n_elem"), arg("sel_num"), arg("p_cross"), arg("p_mut"), arg("evol_fitness")),
			return_internal_reference<>(), "Find good maps using the genetic algorithm.\nalgogen uses the multipoint LOD criterion to provide maps. The command starts either form the best maps stored into the heap or from generated maps. Each time a better map is found, it is stored into the heap. The Nbgen argument correspond to the number of generation. The NbMap argument correspond to the size of the population. In case NbMap is set to 0, the command use the heap to initialize the first generation. The SelType argument correspond to the type of selection( 0 for the roulette wheel, 1 for the stochastic remainder without replacement). The ProbaCross argument correspond to the probability of crossing-over. The ProbaMut argument correspond to the probability of mutation. The EvolFitn is a flag to set the evolutive fitness(0 or 1).")
		.def("cg2tsp", &CGContext::cg2tsp, (arg("filename")),
			return_internal_reference<>(), "Convert current marker selection to TSP.\ncg2tsp converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem whose file name is given as parameter.")
		.def("paretolkh", &CGContext::paretolkh, (arg("resolution"), arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a Pareto frontier approximation.\nparetolkh inserts in the heap a collection of maps corresponding to different numbers of breakpoints w.r.t. to a given reference order data set. It uses the weighted sum for combining biological data (2-points loglikelihoods) with a given reference order (breakpoint distance). The current data set should be a merged by order between a biological data set and a reference order data set. It tries different coefficient values depending on the Resolution parameter (e.g. Resolution = number of markers). paretolkh converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5). In order to get non supported Pareto solutions, use CollectMaps >= 0. Try paretolkh 10 1 0 as default parameter values.")
		.def("paretolkhn", &CGContext::paretolkhn, (arg("resolution"), arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a Pareto frontier approximation.\nparetolkhn inserts in the heap a collection of maps corresponding to different numbers of breakpoints w.r.t. to a given reference order data set. It uses the weighted sum for combining biological data (normalized 2-points loglikelihoods) with a given reference order (breakpoint distance). The current data set should be a merged by order between a biological data set and a reference order data set. It tries different coefficient values depending on the Resolution parameter (e.g. Resolution = number of markers). paretolkhn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5). In order to get non supported Pareto solutions, use CollectMaps >= 0. Try paretolkhn 10 1 0 as default parameter values.")
		.def("paretogreedy", &CGContext::paretogreedy, ( arg("anytime"), arg("minbp")=-1, arg("maxbp")=-1 ),
			return_internal_reference<>(), "Improve a Pareto frontier approximation.\nparetogreedy uses the bi-objective multipoint LOD score criterion and breakpoint criterion to provide maps. The command starts from all the maps stored into the heap. The 2-change neighborhood is (partially, depending on the Anytime ratio, but at least 2*NbMarkers neighbors) visited for each map. Each time a non dominated map is found, it is stored into the heap. The algorithm stops when all the maps have been visited. The Anytime ratio controls the tradeoff between search speed and solution quality.")
		.def("mcmc", &CGContext::mcmc, ( arg("seed"), arg("nbiter"), arg("burning"), arg("slow_computations")=false, arg("lim_group_size")=10, arg("mcmcverbose")=true ),
			return_internal_reference<>(), "Provide a map distribution using Markov Chain Monte Carlo.\nmcmc is Markov Chain Monte Carlo algorithm for estimating the posterior distribution of the marker order. It starts from the best map found in the CarthaGene heap. In order to visit maps, it uses a stochastic-biased 2-opt operator based on 2-point likelihood genetic / Radiated Hybrid data. The random generator number is initialized with the RandomSeed parameter (should be a positive integer). The number of iterations of MCMC is controlled by the NbIter parameter. At the end, it gives a list of maps with their posterior probability. The first Burning iterations are not used to estimate the map distribution. The maps are inserted in the CarthaGene heap. The map distribution is written to a file names PID.mcmc where PID is the Process ID of the current carthagene session.\n\nThe format of the output file is as follows: the first line recalls the starting map then each line is a map from the distribution, sorted in decreasing (multipoint) posterior probability. On each line is indicated (in that order): the posterior probability using the multipoint likelihood, the posterior probability using the 2-pt approximation of the likelihood, the weight measuring the ratio of these two probabilities, the number of breakpoints with the reference order and finally the map given as the positions of the markers relative to the starting map (from 0 to N-1) where N is the number of markers.")
		.def("lkh", &CGContext::lkh, (arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a (nice) map, using 2-points loglikelihoods and Lin-Kernighan heuristic.\nlkh converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).")
		.def("ilkh", &CGContext::ilkh, (arg("nb_run"), arg("collect_maps"), arg("threshold"), arg("cost")),
			return_internal_reference<>(), "Provide a (nice) map, using dynamically updated 2-points loglikelihoods and Lin-Kernighan heuristic.\nilkh converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkh 20 0 0.01 100.")
		.def("lkhn", &CGContext::lkhn, (arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a (nice) map, using normalized 2-points loglikelihoods and Lin-Kernighan heuristic.\nlkhn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).")
		.def("ilkhn", &CGContext::ilkhn, (arg("nb_run"), arg("collect_maps"), arg("threshold"), arg("cost")),
			return_internal_reference<>(), "Provide a (nice) map, using dynamically updated normalized 2-points loglikelihoods and Lin-Kernighan heuristic.\nilkhn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhn 20 0 0.01 100.")
		.def("lkhd", &CGContext::lkhd, (arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a (nice) map, using 2-points distances and Lin-Kernighan heuristic.\nlkhd converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).")
		.def("ilkhd", &CGContext::ilkhd,
			return_internal_reference<>(), "Provide a (nice) map, using dynamically updated 2-points distances and Lin-Kernighan heuristic.\nilkhd converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhd 20 0 0.01 100.")
		.def("lkhl", &CGContext::lkhl, (arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a (nice) map, using 2-points LOD criteria and Lin-Kernighan heuristic.\nlkhl converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).")
		.def("ilkhl", &CGContext::ilkhl, (arg("nb_run"), arg("collect_maps"), arg("threshold"), arg("cost")),
			return_internal_reference<>(), "Provide a (nice) map, using dynamically updated 2-points LOD criteria and Lin-Kernighan heuristic.\nilkhl converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhl 20 0 0.01 100.")
		.def("lkhocb", &CGContext::lkhocb, (arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a (nice) map, using 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.\nlkhocb converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).")
		.def("ilkhocb", &CGContext::ilkhocb, (arg("nb_run"), arg("collect_maps"), arg("threshold"), arg("cost")),
			return_internal_reference<>(), "Provide a (nice) map, using dynamically updated 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.\nilkhocb converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhocb 20 0 0.01 100.")
		.def("lkhocbn", &CGContext::lkhocbn, (arg("nb_run"), arg("backtrack")),
			return_internal_reference<>(), "Provide a (nice) map, using normalized 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.\nlkhocbn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).")
		.def("ilkhocbn", &CGContext::ilkhocbn, (arg("nb_run"), arg("collect_maps"), arg("threshold"), arg("cost")),
			return_internal_reference<>(), "Provide a (nice) map, using dynamically updated normalized 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.\nilkhocbn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhocbn 20 0 0.01 100.")
		.def("mapocb", &CGContext::mapocb, (arg("map")),
			return_internal_reference<>(), "Compute the number of obligate chromosome breaks of a map.\nmapocb returns the value of the 2-points Obligate Chromosome Breaks criterion. The MapID should be the identifier of a map stored into the heap.")
		.def("polish", &CGContext::polish,
			return_internal_reference<>(), "Try to improve the best map by polishing.\npolish starts form the best map of the heap, an try to improve the solution. Taking each locus, one after the other, this commands tests all the possible position of this locus on the map, keeping the order of the other loci. Each time the solution is improved, the command tries to store the map into the heap.")
		.def("polishtest", &CGContext::polishtest, (arg("marker_subset")),
			return_internal_reference<>(), "Try to improve the map giving with markers by polishing.\npolishtest starts from the map giving on the command, an try to improve the solution. Taking each locus, one after the other, this commands tests all the possible position of this locus on the map, keeping the order of the other loci.")
		.def("flips", &CGContext::flips, (arg("window_size"), arg("threshold"), arg("f_iter")),
			return_internal_reference<>(), "Try to improve the best map by flipping.\nflips starts from the best map of the heap and try to improve the solution. Inside a window pushed on all the positions of the map, all the permutation are tested. Each time a better solution is found, the process prints it, and tries to insert it into the heap. The Winsize argument allows you to specify the window size. The Thres argument is a LOD threshold, that enables to see worse maps, that differs less than this value from the best map. If the iter flag is set to 1, each time a better solution is found, the command will start again from the best map.")
	;
	class_<MarkerGroups>("MarkerGroups", "A marker grouping based on current selection, a distance threshold, and a LOD threshold", no_init)
		.def_readonly("disthres", &MarkerGroups::dt)
		.def_readonly("lodthres", &MarkerGroups::lt)
		.def_readonly("groups", &MarkerGroups::groups)
	;
}
