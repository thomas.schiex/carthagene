#ifndef _CGPY_CGCONTEXT_H_
#define _CGPY_CGCONTEXT_H_
#include "inc.h"
#include "marker.h"
#include "markergroups.h"
#include "dataset.h"
#include "carray.h"


#define _MAP_INVOKE(_routine, _map, ...) \
	do { \
		prepare(); \
		Cartage.StopFlag=0; \
		_map._routine(__VA_ARGS__); \
		restore(); \
	} while(0)

#define _MAP_INVOKE_RET(_ret, _routine, _map, ...) \
	do { \
		prepare(); \
		Cartage.StopFlag=0; \
		_ret = _map._routine(__VA_ARGS__); \
		restore(); \
	} while(0)

#define _CG_INVOKE(_routine, ...) _MAP_INVOKE(_routine, Cartage ,##__VA_ARGS__)
#define _CG_INVOKE_RET(_ret, _routine, ...) _MAP_INVOKE_RET(_ret, _routine, Cartage ,##__VA_ARGS__)
#define RET_MAP return cur.heap->Best()




/* Helper for MapFactory */
class CGContext {
private:
	friend void update_mapfactory_selections(int m2);
	typedef struct {
		BioJeu* bj;
		int* sel;
		int selsz;
		Tas* heap;
	} context;

	context cur, backup;
	
	static std::vector<CGContext*> _all;

	void prepare() {
		backup.bj = Cartage.ArbreJeu;
		backup.sel = Cartage.MarkSelect;
		backup.selsz = Cartage.NbMS;
		backup.heap = Cartage.Heap;
		Cartage.ArbreJeu = cur.bj;
		Cartage.MarkSelect = cur.sel;
		Cartage.NbMS = cur.selsz;
		Cartage.Heap = cur.heap;
	}

	void restore() {
		Cartage.ArbreJeu = backup.bj;
		Cartage.MarkSelect = backup.sel;
		Cartage.NbMS = backup.selsz;
		Cartage.Heap = backup.heap;
	}

	void remove_from_vec() const {
		std::vector<CGContext*>::iterator b=_all.begin(), e=_all.end();
		while(b!=e) {
			if(*b==(CGContext*)this) {
				_all.erase(b);
				return;
			}
			++b;
		}
	}

	void reinit_sel_list() {
		seq = list();
		for(int i=0;i<cur.selsz;++i) {
			seq.append(Marker(cur.sel[i]));
		}
	}

public:
	object ds;
	list seq;
	static list all() { return list(_all); }
	CGContext()
		: ds(0), seq()
	{
		cur.bj = NULL;
		cur.sel = NULL;
		cur.selsz = 0;
		cur.heap = new Tas();
		cur.heap->Init(&Cartage);
		_all.push_back(this);
	}
	CGContext(object bj, object sel)
		: ds(bj), seq(sel)
	{
		DataSet*d=NULL;
		extract<DataSet*> xds(ds);
		extract<list> xsel(sel);
		if(!xds.check()) {
			PyErr_SetString(PyExc_TypeError, "Expected DataSet as first argument");
			throw_error_already_set();
		} else {
			d= xds();
		}
		if(!xsel.check()) {
			PyErr_SetString(PyExc_TypeError, "Expected list as second argument");
			throw_error_already_set();
		}

		stl_input_iterator<object> begin(sel), b(sel), end;
		cur.bj = d->b;
		cur.selsz=0;
		while(b!=end) {
			++cur.selsz;
			++b;
		}
		cur.sel = new int[cur.selsz];
		int* ptr = cur.sel;
		while(begin!=end) {
			extract<Marker*> xm(*begin);
			*ptr = xm.check()?xm()->id:extract<int>(*begin);
			/**ptr = extract<int>(*begin);*/
			++ptr;
			++begin;
		}
		cur.heap = new Tas();
		cur.heap->Init(&Cartage);
		_all.push_back(this);
	}
	virtual ~CGContext() {
		remove_from_vec();
		delete[] cur.sel;
	}

	list debug_sel() const {
		list l;
		for(int i=0;i<cur.selsz;++i) {
			l.append(cur.sel[i]);
		}
		return l;
	}

	/* Heap methods */
	const char* stat(double thres) { return cur.heap->Stat(thres); }

	void equi(int flag) { cur.heap->Equi(flag); }
	Carte* worst() { return cur.heap->Worst(); }
	Carte* best() { return cur.heap->Best(); }
	double delta() { return cur.heap->Delta(); }
	list maps() const {
		int* ids = cur.heap->IdSorted();
		list l;
		for(int i=0;i<cur.heap->HeapSize;++i) {
			l.append(cur.heap->MapFromId(ids[i]));
		}
		return l;
	}

	/* Find mergeable markers in selection */
	list dupes() {
		list ret;
		char** listm;
		int a, b;
		_CG_INVOKE_RET(listm, GetDouble);	/* DIRTY : there should NOT be any char* intermediary values */
		while(*listm) {
			if(2!=sscanf(*listm, "%d %d", &a, &b)) {
				throw std::exception();
			}
			ret.append(make_tuple(a, b));
			++listm;
		}
		return ret;
	}

	/* Compute groups */
	MarkerGroups* groups(double disthres, double lodthres) {
		MarkerGroups* ret;
		_CG_INVOKE(Groupe, disthres, lodthres);
		ret = new MarkerGroups();
		/* TODO : clean stuff from Cartage ? */
		return ret;
	}

	/* Map display */
	void print(Carte* m) {
		_CG_INVOKE(PrintMap, m);
		cur.bj->PrintMap(m);
	}

	void printd(Carte* m) {
		_CG_INVOKE(PrintDMap, m->Id, 0);
	}

	void printdr(Carte* m) {
		_CG_INVOKE(PrintDMap, m->Id, 1);
	}

	/* Map creators */
	Carte* sem() { _CG_INVOKE(SinglEM); RET_MAP; }

	Carte* nicemapd() { _CG_INVOKE(BuildNiceMap); RET_MAP; }
	Carte* nicemapl() { _CG_INVOKE(BuildNiceMapL); RET_MAP; }
	Carte* mfmapd() { _CG_INVOKE(BuildNiceMapMultiFragment); RET_MAP; }
	Carte* mfmapl() { _CG_INVOKE(BuildNiceMapLMultiFragment); RET_MAP; }
	Carte* build(int nc) { _CG_INVOKE(Build, nc); RET_MAP; }
	/* TODO : cartes framework (buildfw) */
	Carte* annealing(int tries, double tinit, double tfinal, double cooling) { _CG_INVOKE(Annealing, tries, tinit, tfinal, cooling); RET_MAP; }
	Carte* greedy(int nr, int ni, int tmin, int tmax, int ratio) { _CG_INVOKE(Greedy, nr, ni, tmin, tmax, ratio); RET_MAP; }
	Carte* algogen(int ngens, int nelem, int sel_num, float pcross, float pmut, int evol_fitness) { _CG_INVOKE(AlgoGen, ngens, nelem, sel_num, pcross, pmut, evol_fitness); RET_MAP; }
	Carte* cg2tsp(char* filename) { _CG_INVOKE(cg2tsp, filename); RET_MAP; }
	Carte* paretolkh(int resolution, int nbrun, int backtrack) { _CG_INVOKE(ParetoLKH, resolution, nbrun, backtrack, contribLogLike2pt1, contribLogLike2pt2); RET_MAP; }
	Carte* paretolkhn(int resolution, int nbrun, int backtrack) { _CG_INVOKE(ParetoLKH, resolution, nbrun, backtrack, normContribLogLike2pt1, normContribLogLike2pt2); RET_MAP; }
	/* TODO : paretoinfo* */
	/*IntArray* paretoinfo(int graphicalview, double ratio) {}*/
	Carte* paretogreedy(int anytime, int minbp=-1, int maxbp=-1) {
		if(minbp==-1&&maxbp==-1) {
			_CG_INVOKE(ParetoGreedy, anytime);
		} else {
			_CG_INVOKE(ParetoGreedy, anytime, minbp, maxbp);
		}
		RET_MAP;
	}

	Carte* mcmc(int seed, int nbiter, int burning, bool slow_computations=false, int lim_group_size=10, bool mcmcverbose=true)
	{ _CG_INVOKE(mcmc, seed, nbiter, burning, contribLogLike2pt1, contribLogLike2pt2, slow_computations, lim_group_size, mcmcverbose); RET_MAP; }

	Carte* lkh(int nbrun, int backtrack)
	{ _CG_INVOKE(lkh, nbrun, backtrack, contribLogLike2pt1, contribLogLike2pt2); RET_MAP; }
	Carte* ilkh(int nbrun, int collectmaps, double threshold, int cost)
	{ _CG_INVOKE(lkhiter, nbrun, collectmaps, threshold, cost, contribLogLike2pt1, contribLogLike2pt2); RET_MAP; }
	
	Carte* lkhn(int nbrun, int backtrack)
	{ _CG_INVOKE(lkh, nbrun, backtrack, normContribLogLike2pt1, normContribLogLike2pt2); RET_MAP; }
	Carte* ilkhn(int nbrun, int collectmaps, double threshold, int cost)
	{ _CG_INVOKE(lkhiter, nbrun, collectmaps, threshold, cost, normContribLogLike2pt1, normContribLogLike2pt2); RET_MAP; }
	
	Carte* lkhd(int nbrun, int backtrack)
	{ _CG_INVOKE(lkh, nbrun, backtrack, contribZero, contribHaldane); RET_MAP; }
	Carte* ilkhd(int nbrun, int collectmaps, double threshold, int cost)
	{ _CG_INVOKE(lkhiter, nbrun, collectmaps, threshold, cost, contribZero, contribHaldane); RET_MAP; }
	
	Carte* lkhl(int nbrun, int backtrack)
	{ _CG_INVOKE(lkh, nbrun, backtrack, contribZero, contribLOD); RET_MAP; }
	Carte* ilkhl(int nbrun, int collectmaps, double threshold, int cost)
	{ _CG_INVOKE(lkhiter, nbrun, collectmaps, threshold, cost, contribZero, contribLOD); RET_MAP; }
	
	Carte* lkhocb(int nbrun, int backtrack)
	{ _CG_INVOKE(lkh, nbrun, backtrack, contribZero, contribOCB); RET_MAP; }
	Carte* ilkhocb(int nbrun, int collectmaps, double threshold, int cost)
	{ _CG_INVOKE(lkhiter, nbrun, collectmaps, threshold, cost, contribZero, contribOCB); RET_MAP; }

	Carte* lkhocbn(int nbrun, int backtrack)
	{ _CG_INVOKE(lkh, nbrun, backtrack, contribZero, normContribOCB); RET_MAP; }
	Carte* ilkhocbn(int nbrun, int collectmaps, double threshold, int cost)
	{ _CG_INVOKE(lkhiter, nbrun, collectmaps, threshold, cost, contribZero, normContribOCB); RET_MAP; }

	Carte* mapocb(object mid) {
		int id;
		extract<Carte*> xc(mid);
		id = xc.check()?xc()->Id:extract<int>(mid);
		_CG_INVOKE(OCB, id);
		RET_MAP;
	}

	/* TODO : imputation */

	Carte* polish() { _CG_INVOKE(Polish); RET_MAP; }
	Carte* polishtest(object vm) {
		static int vmp[1<<17];	/* AAARRRGGGG */
		int i;
		stl_input_iterator<object> begin(vm), end;
		for(i=0;begin!=end;++i,++begin) {
			vmp[i] = extract<int>(*begin);
		}
		_CG_INVOKE(Polishtest, vmp, i);
		RET_MAP;
	}
	Carte* flips(int ws, double thres, int fiter) { _CG_INVOKE(Flips, ws, thres, fiter); RET_MAP; }
	/* TODO : paretoheapsize */

};


#endif

