#include <stdio.h>
#include <stdlib.h>
#include "LKH.h"

/* links between LKH and CarthaGene */
void tsp2cg(int a, long *b) {}
int MyStopFlag = 0;
int *CurrentStopFlag = &MyStopFlag;
int AlwaysComputeEM = 0;
int CartageQuietFlag = 0;

int main(int argc, char *argv[])
{
    int i;
    Problem *p = DefaultProblem();

    /* Problem specification */
    p->NAME = (char *)"ulysses16";
    p->TYPE = "TSP";
    p->EDGE_WEIGHT_TYPE = "GEO";
    p->DIMENSION = 16;

    /* Parameters */
    p->OPTIMUM = 6859;

    /* Data */
    p->X = (double *) malloc(17 * sizeof(double));
    p->Y = (double *) malloc(17 * sizeof(double));
    p->X[1] = 38.24;
    p->Y[1] = 20.42;
    p->X[2] = 39.57;
    p->Y[2] = 26.15;
    p->X[3] = 40.56;
    p->Y[3] = 25.32;
    p->X[4] = 36.26;
    p->Y[4] = 23.12;
    p->X[5] = 33.48;
    p->Y[5] = 10.54;
    p->X[6] = 37.56;
    p->Y[6] = 12.19;
    p->X[7] = 38.42;
    p->Y[7] = 13.11;
    p->X[8] = 37.52;
    p->Y[8] = 20.44;
    p->X[9] = 41.23;
    p->Y[9] = 9.10;
    p->X[10] = 41.17;
    p->Y[10] = 13.05;
    p->X[11] = 36.08;
    p->Y[11] = -5.21;
    p->X[12] = 38.47;
    p->Y[12] = 15.13;
    p->X[13] = 38.15;
    p->Y[13] = 15.35;
    p->X[14] = 37.51;
    p->Y[14] = 15.17;
    p->X[15] = 35.49;
    p->Y[15] = 14.32;
    p->X[16] = 39.36;
    p->Y[16] = 19.56;

    LKH(p);

    printf("Cost = %0.0lf\n", p->BestCost);
    printf("Tour: ");
    for (i = 1; i <= p->DIMENSION; i++)
        printf("%ld ", p->BestTour[i]);
    printf("\n");

    return 0;
}
