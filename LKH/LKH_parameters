Short description of the parameters to LKH:

PROBLEM_FILE = <string>
Specifies the name of the problem file.
 
Additional control information may be supplied in the following format:
 
ASCENT_CANDIDATES = <integer>
The number of candidate edges to be associated with each node during 
the ascent. The candidate set is complemented such that every candidate 
edge is associated with both its two end nodes.
Default: 50.

BACKTRACK_MOVE_TYPE = <integer>
Specifies the backtrack move type to be used in local search. A backtrack 
move allows for backtracking up to a certain level of the local search. 
A value K >= 2 signifies that a backtracking K-opt move is to be used as 
the first move in a sequence of moves. The value 0 signifies that no 
backtracking is to be used.
Default: 0. 
 
CANDIDATE_FILE = <string>
Specifies the name of a file to which the candidate sets are to be 
written. If, however, the file already exists, the candidate edges are 
read from the file. Each line contains a node number, the number of the 
dad of the node in the minimum spanning tree (0, if the node has no dad), 
the number of candidate edges emanating from the node, followed by the 
candidate edges. For each candidate edge its end node number and 
alpha-value are given.
 
COMMENT : <string>
A comment.

% : <string>
A comment.
 
DELAUNAY : [ YES | NO ]
Specifies whether the Delaunay candidate set is to be used.
Default: NO.
 
EOF
Terminates the input data. The entry is optional.
 
EXCESS = <real>
The maximum alpha-value allowed for any candidate edge is set to 
EXCESS times the absolute value of the lower bound of a solution 
tour (determined by the ascent).
Default: 1.0/DIMENSION.

GAIN23 : [ YES | NO ]
Specifies whether the Gain23 function is used,
Default: YES.

GREEDY: [ YES | NO ]
Specifies whether a greedy tour is to be computed at the beginning
of a run.
Default: NO.
 
INITIAL_PERIOD = <integer>
The length of the first period in the ascent.
Default: DIMENSION/2 (but at least 100). 
 
INITIAL_STEP_SIZE = <integer>
The initial step size used in the ascent.
Default: 1.
 
INITIAL_TOUR_FILE = <string>
Specifies the name of a file containing a tour to be used as the 
initial tour in the search. The tour is given by a list of integers 
giving the sequence in which the nodes are visited in the tour.
The tour is terminated by a -1.
 
INPUT_TOUR_FILE = <string>
Specifies the name of a file containing a tour. The tour is used to 
limit the search (the last edge to be excluded in a non-gainful move 
must not belong to the tour). In addition, the Alpha field of its 
edges is set to zero. The tour is given by a list of integers giving 
the sequence in which the nodes are visited in the tour. The tour is 
terminated by a -1. 
 
MAX_CANDIDATES = <integer> { SYMMETRIC }
The maximum number of candidate edges to be associated with each node.
The integer may be followed by the keyword SYMMETRIC, signifying 
that the candidate set is to be complemented such that every candidate 
edge is associated with both its two end nodes. 
Default: 5.
  
MAX_SWAPS = <integer>
Specifies the maximum number of swaps (flips) allowed in any search 
for a tour improvement.
Default: DIMENSION.
 
MAX_TRIALS = <integer>
The maximum number of trials in each run. 
Default: number of nodes (DIMENSION, given in the problem file).

MERGE_TOUR_FILE_1 = <string>
Specifies the name of a tour to be merged. The edges of the tour are 
added to the candidate sets with alpha-values equal to 0. 
 
MERGE_TOUR_FILE_2 = <string>
Specifies the name of a tour to be merged. The edges of the tour are  
added to the candidate sets with alpha-values equal to 0. 
 
MOVE_TYPE = <integer>
Specifies the move type to be used in local search. The value K >= 2
signifies that a K-opt move is to be used.
Default: 5.  
 
OPTIMUM = <real>
Known optimal tour length. If STOP_AT_OPTIMUM is YES, a run will be 
terminated if the tour length becomes equal to this value.
Default: -DBL_MAX

PI_FILE = <string>
Specifies the name of a file to which penalties (pi-values determined 
by the ascent) are to be written. If the file already exists, the 
penalties are read from the file, and the ascent is skipped. Each line 
of the file is of the form
      <integer> <integer>
where the first integer is a node number, and the second integer is 
the Pi-value associated with the node.
 
PRECISION = <integer>
The internal precision in the representation of transformed distances: 
   d[i][j] = PRECISION*c[i][j] + pi[i] + pi[j], 
where d[i][j], c[i][j], pi[i] and pi[j] are all integral.
Default: 100 (which corresponds to 2 decimal places).
 
RESTRICTED_SEARCH: [ YES | NO ]
Specifies whether the following search pruning technique is used: 
The first edge to be broken in a move must not belong to the currently 
best solution tour. When no solution tour is known, it must not belong 
to the minimum spanning 1-tree.   
Default: YES.         
 
RUNS = <integer>
The total number of runs. 
Default: 10.
 
SEED = <integer>
Specifies the initial seed for random number generation.
Default: 1.
 
STOP_AT_OPTIMUM : [ YES | NO ]
Specifies whether a run is stopped, if the tour length becomes equal
to OPTIMUM.
Default: YES.

SUBGRADIENT: [ YES | NO ]
Specifies whether the pi-values should be determined by subgradient 
optimization.
Default: YES.
 
TOUR_FILE = <string>
Specifies the name of a file to which the best tour is to be written.

TRACE_LEVEL = <integer>
Specifies the level of detail of the output given during the solution 
process. The value 0 signifies a minimum amount of output. The higher 
the value is the more information is given.
Default: 1.        
