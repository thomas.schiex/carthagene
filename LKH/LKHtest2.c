#include <stdio.h>
#include "LKH.h"

/* links between LKH and CarthaGene */
void tsp2cg(int a, long *b) {}
int MyStopFlag = 0;
int *CurrentStopFlag = &MyStopFlag;
int AlwaysComputeEM = 0;
int CartageQuietFlag = 0;

int main(int argc, char *argv[])
{
    int i;
    Problem *p = DefaultProblem();
    long C[] = { 0, 633, 0, 257, 390, 0, 91, 661, 228, 0, 412, 227,
        169, 383, 0, 150, 488, 112, 120, 267, 0, 80, 572, 196,
        77, 351, 63, 0, 134, 530, 154, 105, 309, 34, 29, 0,
        259, 555, 372, 175, 338, 264, 232, 249, 0, 505, 289, 262,
        476, 196, 360, 444, 402, 495, 0, 353, 282, 110, 324, 61,
        208, 292, 250, 352, 154, 0, 324, 638, 437, 240, 421, 329,
        297, 314, 95, 578, 435, 0, 70, 567, 191, 27, 346, 83,
        47, 68, 189, 439, 287, 254, 0, 211, 466, 74, 182, 243,
        105, 150, 108, 326, 336, 184, 391, 145, 0, 268, 420, 53,
        239, 199, 123, 207, 165, 383, 240, 140, 448, 202, 57, 0,
        246, 745, 472, 237, 528, 364, 332, 349, 202, 685, 542, 157,
        289, 426, 483, 0, 121, 518, 142, 84, 297, 35, 29, 36,
        236, 390, 238, 301, 55, 96, 153, 336, 0
    };

    /* Problem specification */
    p->NAME = (char *)"gr17";
    p->TYPE = "TSP";
    p->EDGE_WEIGHT_TYPE = "EXPLICIT";
    p->EDGE_WEIGHT_FORMAT = "LOWER_DIAG_ROW";
    p->DIMENSION = 17;

    /* Parameters */
    p->OPTIMUM = 2085;

    /* Data */
    p->C = C;

    LKH(p);

    printf("Cost = %0.0lf\n", p->BestCost);
    printf("Tour: ");
    for (i = 1; i <= p->DIMENSION; i++)
        printf("%ld ", p->BestTour[i]);
    printf("\n");

    return 0;
}
