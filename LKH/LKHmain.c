#include "LKH.h"
#include "LK.h"

/* dummy variables/functions to compile without CarthaGene */
void tsp2cg(int a, long *b) {}
int MyStopFlag = 0;
int *CurrentStopFlag = &MyStopFlag;
int AlwaysComputeEM = 0;
int CartageQuietFlag = 0;

#if 0

int main(int argc, char *argv[])
{
    Problem *p;
    Node *N;
    int i, j;

    if (argc >= 1)
        ParameterFileName = argv[1];
    ReadParameters();
    ReadProblem();

    p = DefaultProblem();
    p->ASCENT_CANDIDATES = AscentCandidates;
    p->BACKTRACK_MOVE_TYPE = BacktrackMoveType;
    p->EXCESS = Excess;
    p->INITIAL_PERIOD = InitialPeriod;
    p->INITIAL_STEP_SIZE = InitialStepSize;
    if (InitialTourFile) {
        assert(p->INITIAL_TOUR = (long *) calloc(Dimension, sizeof(long)));
        N = FirstNode;
        for (i = 0; i < Dimension; i++) {
            p->INITIAL_TOUR[i] = N->Id;
            N = N->InitialSuc;
        }
    }
    if (InputTourFile) {
        assert(p->INPUT_TOUR = (long *) calloc(Dimension, sizeof(long)));
        N = FirstNode;
        for (i = 0; i < Dimension; i++) {
            p->INITIAL_TOUR[i] = N->Id;
            N = N->OptimumSuc;
        }
    }
    p->MAX_CANDIDATES = MaxCandidates;
    p->MAX_SWAPS = MaxSwaps;
    p->MAX_TRIALS = MaxTrials;
    for (j = 0; j <= 1; j++) {
        if (MergeTourFile[j]) {
            assert(p->MERGE_TOUR[j] =
                   (long *) calloc(Dimension, sizeof(long)));
            N = FirstNode;
            for (i = 0; i < Dimension; i++) {
                p->MERGE_TOUR[j][i] = N->Id;
                N = N->MergeSuc[j];
            }
        }
    }
    p->MOVE_TYPE = MoveType;
    p->OPTIMUM = Optimum;
    p->PRECISION = Precision;
    p->RESTRICTED_SEARCH = RestrictedSearch;
    p->RUNS = Runs;
    p->SEED = Seed;
    p->SUBGRADIENT = Subgradient;
    p->SYMMETRIC_CANDIDATES = CandidateSetSymmetric;
    p->TRACE_LEVEL = TraceLevel;
    p->NAME = Name;
    p->TYPE = Type;
    p->DIMENSION = Dimension;
    assert( strcmp(EdgeWeightType, "EXPLICIT") );
    p->EDGE_WEIGHT_TYPE = EdgeWeightType;
    p->NODE_COORD_TYPE = NodeCoordType;
    assert(p->X = (double *) calloc(Dimension + 1, sizeof(double)));
    assert(p->Y = (double *) calloc(Dimension + 1, sizeof(double)));
    assert(p->Z = (double *) calloc(Dimension + 1, sizeof(double)));
    for (i = 1; i <= Dimension; i++) {
        p->X[i] = NodeSet[i].X;
        p->Y[i] = NodeSet[i].Y;
        p->Z[i] = NodeSet[i].Z;
    }
    LKH(p);
    return 0;
}

#endif /* 0 */

