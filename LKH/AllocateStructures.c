#include "LK.h"
#include "Hashing.h"
#include "Heap.h"

/*      
 * The AllocateStructures function allocates all necessary 
 * structures except nodes and candidates.
 */

#define Free(s) { free(s); s = 0; }

void AllocateStructures()
{
    long i;

    Free(Heap);
    Free(BestTour);
    Free(BetterTour);
    Free(HTable);
    Free(Rand);
    Free(CacheSig);
    Free(CacheVal);
    Free(SwapStack);

    MakeHeap(Dimension);
    BestTour = (long *) calloc((1 + Dimension), sizeof(long)); assert(BestTour);
    BetterTour = (long *) calloc((1 + Dimension), sizeof(long)); assert(BetterTour);
    HTable = (HashTable *) malloc(sizeof(HashTable)); assert(HTable);
    HashInitialize((HashTable *) HTable);
    srand(Seed);
    Rand = ( int *) malloc((Dimension + 1) * sizeof(long)); assert(Rand);
    for (i = 1; i <= Dimension; i++)
	Rand[i] = rand();
    srand(Seed);
    if (WeightType != EXPLICIT) {
	for (i = 1; i <= Dimension; i *= 2);
	CacheSig = (long *) calloc(i, sizeof(long)); assert(CacheSig);
	CacheVal = (long *) calloc(i, sizeof(long)); assert(CacheVal);
    }
    AllocateSegments();
    SwapStack =
	   (SwapRecord *) malloc((Dimension + 10) * sizeof(SwapRecord));
    assert(SwapStack);
}

/*      
 * The AllocateSegments function allocates the segments of the two-level 
 * tree.
 */

void AllocateSegments()
{
    Segment *S, *SPrev;
    long i;

    FreeSegments();
    GroupSize = (long double) sqrt((long double) Dimension);
    Groups = 0;
    for (i = Dimension, SPrev = 0; i > 0; i -= GroupSize, SPrev = S) {
	S = (Segment *) malloc(sizeof(Segment)); assert(S);
	S->Rank = ++Groups;
	if (!SPrev)
	    FirstSegment = S;
	else
	    Link(SPrev, S);
    }
    Link(S, FirstSegment);
}
