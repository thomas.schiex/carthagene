#include "LK.h"

/*      
 * The FreeStructures function frees all allocated structures.
 */

#define Free(s) { free(s); s = 0; }

void FreeStructures()
{
    if (FirstNode) {
	Node *N = FirstNode, *Next;
	do {
	    Next = N->Suc;
	    Free(N->CandidateSet);
	}
	while ((N = Next) != FirstNode);
	FirstNode = 0;
    }
    Free(NodeSet);
    FreeSegments();
    Free(CostMatrix);
    Free(BestTour);
    Free(BetterTour);
    Free(SwapStack);
    Free(HTable);
    Free(Rand);
    Free(CacheSig);
    Free(CacheVal);
    Free(Name);
    Free(Type);
    Free(EdgeWeightType);
    Free(EdgeWeightFormat);
    Free(EdgeDataFormat);
    Free(NodeCoordType);
    Free(DisplayDataType);
}

/*      
   The FreeSegments function frees the segments.
*/

void FreeSegments()
{
    if (FirstSegment) {
	Segment *S = FirstSegment, *SPrev;
	do {
	    SPrev = S->Pred;
	    Free(S);
	}
	while ((S = SPrev) != FirstSegment);
	FirstSegment = 0;

    }
}
