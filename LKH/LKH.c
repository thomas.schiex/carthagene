#include "LKH.h"
#include "LK.h"
#include "Heap.h"
extern void AllocateNodes();
extern int *CurrentStopFlag;
extern int AlwaysComputeEM;

/* A quick and dirty implementation of a LKH function */

Problem *DefaultProblem()
{
    Problem *p = (Problem *) calloc(1, sizeof(Problem));
    assert(p);
    p->ASCENT_CANDIDATES = 50;
    p->BACKTRACK_MOVE_TYPE = 0;
    p->DELAUNAY = 0;
    p->EXCESS = -1;
    p->INITIAL_PERIOD = -1;
    p->INITIAL_STEP_SIZE = 0;
    p->MAX_CANDIDATES = 5;
    p->MAX_SWAPS = -1;
    p->MAX_TRIALS = 0;
    p->MOVE_TYPE = 5;
    p->OPTIMUM = -DBL_MAX;
    p->PRECISION = 100;
    p->RESTRICTED_SEARCH = 1;
    p->RUNS = 0;
    p->SEED = 1;
    p->STOP_AT_OPTIMUM = 1;
    p->SUBGRADIENT = 1;
    p->SUBSEQUENT_MOVE_TYPE = 0;
    p->TIME_LIMIT = DBL_MAX;
    p->TRACE_LEVEL = 1;
    return p;
}

static char *Copy(const char *S)
{
    char *Buffer;
    if (S == 0 || strlen(S) == 0)
	return 0;
    Buffer = (char *) malloc(strlen(S) + 1);
    assert(Buffer);
    strcpy(Buffer, S);
    return Buffer;
}

static void Read_EDGE_WEIGHT_SECTION(Problem * p);

void LKH(Problem * p)
{
    long Id, Count, Alpha, i, j;
    Node *N, *From, *To, *Na, *Nb;
    Candidate *NN;

    long TrialsMin, TrialsMax, TrialSum, Successes, Run;
    double Cost, CostSum, WorstCost;
    double Time, TimeMin, TimeMax, TimeSum;
    double LastTime = GetTime();

    TrialSum = Successes = 0;
    CostSum = TimeSum = 0.0;
    TrialsMin = LONG_MAX;
    TrialsMax = 0;
    TimeMin = DBL_MAX;
    TimeMax = 0;

    WeightType = WeightFormat = -1;
    CoordType = NO_COORDS;
    Name = Type = EdgeWeightType = EdgeWeightFormat = 0;
    EdgeDataFormat = NodeCoordType = DisplayDataType = 0;
    Distance = 0;
    C = 0;
    D = 0;
    c = 0;

    Name = Copy(p->NAME);
    Type = Copy(p->TYPE);
    Dimension = p->DIMENSION;
    AllocateNodes();
    if (Type)
	for (i = 0; i < strlen(Type); i++)
	    Type[i] = (char) toupper(Type[i]);
    if (!strcmp(Type, "TSP"))
	ProblemType = TSP;
    else if (!strcmp(Type, "ATSP"))
	ProblemType = ATSP;
    else if (!strcmp(Type, "SOP")) {
	ProblemType = SOP;
	eprintf("(TYPE) Type not implemented: %s", Type);
    } else if (!strcmp(Type, "HCP"))
	ProblemType = HCP;
    else if (!strcmp(Type, "CVRP")) {
	ProblemType = CVRP;
	eprintf("(TYPE) Type not implemented: %s", Type);
    } else if (!strcmp(Type, "TOUR")) {
	ProblemType = TOUR;
	eprintf("(TYPE) Type not implemented: %s", Type);
    } else if (!strcmp(Type, "HPP"))
	ProblemType = HPP;
    else
	eprintf("Unknown TYPE: %s", Type);
    EdgeWeightType = Copy(p->EDGE_WEIGHT_TYPE);
    if (EdgeWeightType)
	for (i = 0; i < strlen(EdgeWeightType); i++)
	    EdgeWeightType[i] = (char) toupper(EdgeWeightType[i]);
    EdgeWeightFormat = Copy(p->EDGE_WEIGHT_FORMAT);
    if (EdgeWeightFormat) {
	for (i = 0; i < strlen(EdgeWeightType); i++)
	    EdgeWeightFormat[i] = (char) toupper(EdgeWeightFormat[i]);
	if (!strcmp(EdgeWeightFormat, "FUNCTION"))
	    eprintf("EDGE_WEIGHT_FORMAT not implemented: %s",
		    EdgeWeightFormat);
	else if (!strcmp(EdgeWeightFormat, "FULL_MATRIX"))
	    WeightFormat = FULL_MATRIX;
	else if (!strcmp(EdgeWeightFormat, "UPPER_ROW"))
	    WeightFormat = UPPER_ROW;
	else if (!strcmp(EdgeWeightFormat, "LOWER_ROW"))
	    WeightFormat = LOWER_ROW;
	else if (!strcmp(EdgeWeightFormat, "UPPER_DIAG_ROW"))
	    WeightFormat = UPPER_DIAG_ROW;
	else if (!strcmp(EdgeWeightFormat, "LOWER_DIAG_ROW"))
	    WeightFormat = LOWER_DIAG_ROW;
	else if (!strcmp(EdgeWeightFormat, "UPPER_COL"))
	    WeightFormat = UPPER_COL;
	else if (!strcmp(EdgeWeightFormat, "LOWER_COL"))
	    WeightFormat = LOWER_COL;
	else if (!strcmp(EdgeWeightFormat, "UPPER_DIAG_COL"))
	    WeightFormat = UPPER_DIAG_COL;
	else if (!strcmp(EdgeWeightFormat, "LOWER_DIAG_COL"))
	    WeightFormat = LOWER_DIAG_COL;
	else
	    eprintf("Unknown EDGE_WEIGHT_FORMAT: %s", EdgeWeightFormat);
    }
    NodeCoordType = Copy(p->NODE_COORD_TYPE);
    if (NodeCoordType)
	for (i = 0; i < strlen(NodeCoordType); i++)
	    NodeCoordType[i] = (char) toupper(NodeCoordType[i]);
    for (i = 1; i <= Dimension; i++) {
	if (p->X)
	    NodeSet[i].X = p->X[i];
	if (p->Y)
	    NodeSet[i].Y = p->Y[i];
	if (p->Z)
	    NodeSet[i].Z = p->Z[i];
    }
    if (!strcmp(EdgeWeightType, "EXPLICIT")) {
	WeightType = EXPLICIT;
	Distance = Distance_EXPLICIT;
    } else if (!strcmp(EdgeWeightType, "EUC_2D")) {
	WeightType = EUC_2D;
	Distance = Distance_EUC_2D;
	c = c_EUC_2D;
    } else if (!strcmp(EdgeWeightType, "EUC_3D")) {
	WeightType = EUC_3D;
	Distance = Distance_EUC_3D;
	c = c_EUC_3D;
    } else if (!strcmp(EdgeWeightType, "MAX_2D")) {
	WeightType = MAX_2D;
	Distance = Distance_MAX_2D;
    } else if (!strcmp(EdgeWeightType, "MAX_3D")) {
	WeightType = MAX_3D;
	Distance = Distance_MAX_3D;
    } else if (!strcmp(EdgeWeightType, "MAN_2D")) {
	WeightType = MAN_2D;
	Distance = Distance_MAN_2D;
    } else if (!strcmp(EdgeWeightType, "MAN_3D")) {
	WeightType = MAN_3D;
	Distance = Distance_MAN_3D;
    } else if (!strcmp(EdgeWeightType, "CEIL_2D")) {
	WeightType = CEIL_2D;
	Distance = Distance_CEIL_2D;
	c = c_CEIL_2D;
    } else if (!strcmp(EdgeWeightType, "CEIL_3D")) {
	WeightType = CEIL_2D;
	Distance = Distance_CEIL_3D;
	c = c_CEIL_3D;
    } else if (!strcmp(EdgeWeightType, "GEO")) {
	WeightType = GEO;
	Distance = Distance_GEO;
    } else if (!strcmp(EdgeWeightType, "GEOM")) {
	WeightType = GEOM;
	Distance = Distance_GEOM;
    } else if (!strcmp(EdgeWeightType, "ATT")) {
	WeightType = ATT;
	Distance = Distance_ATT;
    } else if (!strcmp(EdgeWeightType, "XRAY1") ||
	       !strcmp(EdgeWeightType, "XRAY2") ||
	       !strcmp(EdgeWeightType, "SPECIAL"))
	eprintf("EDGE_WEIGHT_TYPE not implemented: %s", EdgeWeightType);
    else
	eprintf("Unknown EDGE_WEIGHT_TYPE: %s", EdgeWeightType);
    if (p->FIXED_EDGES) {
	for (i = 0; i < p->FIXED_EDGES_COUNT; i++) {
	    Na = &NodeSet[p->FIXED_EDGES[i][0]];
	    Nb = &NodeSet[p->FIXED_EDGES[i][1]];
	    if (!Na->FixedTo1)
		Na->FixedTo1 = Nb;
	    else if (!Na->FixedTo2)
		Na->FixedTo2 = Nb;
	    else
		eprintf("(FIXED_EDGES) Illegal fix: %ld to %ld", Na->Id,
			Nb->Id);
	    if (!Nb->FixedTo1)
		Nb->FixedTo1 = Na;
	    else if (!Nb->FixedTo2)
		Nb->FixedTo2 = Na;
	    else
		eprintf("(FIXED_EDGES) Illegal fix: %ld to %ld", Na->Id,
			Nb->Id);
	}
    }
    Read_EDGE_WEIGHT_SECTION(p);
    AscentCandidates = p->ASCENT_CANDIDATES;
    BacktrackMoveType = p->BACKTRACK_MOVE_TYPE;
    if (p->CANDIDATE_COUNT) {
	for (i = 0; i < Dimension; i++) {
	    Id = p->CANDIDATE_ID[i];
	    assert(Id >= 1 && Id <= Dimension);
	    N = &NodeSet[Id];
	    Id = p->CANDIDATE_DAD[i];
	    assert(Id >= 0 && Id <= Dimension);
	    N->Dad = Id ? &NodeSet[Id] : 0;
	    assert(N != N->Dad);
	    Count = p->CANDIDATE_COUNT[i];
	    assert(Count >= 0 && Count < Dimension);
	    N->CandidateSet = (Candidate *)
		malloc(((Count <
			 MaxCandidates ? Count : MaxCandidates) +
			1) * sizeof(Candidate));
	    for (j = 0, NN = N->CandidateSet; j < Count; j++) {
		Id = p->CANDIDATE_TO[i][j];
		assert(Id >= 0 && Id <= Dimension);
		Alpha = p->CANDIDATE_ALPHA[i][j];
		if (j < MaxCandidates) {
		    NN->To = &NodeSet[Id];
		    NN->Cost = D(N, NN->To);
		    NN->Alpha = Alpha;
		    NN++;
		}
	    }
	    NN->To = 0;
	}
    }
    Excess = p->EXCESS;
    InitialPeriod = p->INITIAL_PERIOD;
    InitialStepSize = p->INITIAL_STEP_SIZE;
    if (p->INITIAL_TOUR) {
	From = &NodeSet[p->INITIAL_TOUR[0]];
	for (i = 1; i < Dimension; i++) {
	    To = &NodeSet[p->INITIAL_TOUR[i]];
	    From->InitialSuc = To;
	    From = To;
	}
	To->InitialSuc = &NodeSet[p->INITIAL_TOUR[0]];
    }
    if (p->INPUT_TOUR) {
	From = &NodeSet[p->INPUT_TOUR[0]];
	for (i = 1; i < Dimension; i++) {
	    To = &NodeSet[p->INPUT_TOUR[i]];
	    From->OptimumSuc = To;
	    From = To;
	}
	To->OptimumSuc = &NodeSet[p->INPUT_TOUR[0]];
    }
    MaxCandidates = p->MAX_CANDIDATES;
    MaxSwaps = p->MAX_SWAPS;
    MaxTrials = p->MAX_TRIALS;
    for (j = 0; j <= 1; j++) {
	if (p->MERGE_TOUR[j]) {
	    From = &NodeSet[p->MERGE_TOUR[j][0]];
	    for (i = 1; i < Dimension; i++) {
		To = &NodeSet[p->MERGE_TOUR[j][i]];
		From->MergeSuc[j] = To;
		From = To;
	    }
	    To->MergeSuc[j] = &NodeSet[p->MERGE_TOUR[j][0]];
	}
    }
    MoveType = p->MOVE_TYPE;
    Optimum = p->OPTIMUM;
    if (p->PI_ID) {
	Id = p->PI_ID[0];
	assert(Id >= 1 && Id <= Dimension);
	FirstNode = Na = &NodeSet[Id];
	Na->Pi = p->PI[0];
	for (i = 1; i < Dimension; i++) {
	    Id = p->PI_ID[i];
	    assert(Id >= 1 && Id <= Dimension);
	    Nb = &NodeSet[Id];
	    Nb->Pi = p->PI[i];
	    Nb->Pred = Na;
	    Na->Suc = Nb;
	    Na = Nb;
	}
	FirstNode->Pred = Nb;
	Nb->Suc = FirstNode;
    }
    Precision = p->PRECISION;
    RestrictedSearch = p->RESTRICTED_SEARCH;
    Runs = p->RUNS;
    Seed = p->SEED;
    Subgradient = p->SUBGRADIENT;
    CandidateSetSymmetric = p->SYMMETRIC_CANDIDATES;
    TraceLevel = p->TRACE_LEVEL;

    if (ProblemType == -1)
	eprintf("TYPE is missing");
    if (Dimension <= 0)
	eprintf("DIMENSION is not positive (or not specified)");
    if (WeightType == -1 && ProblemType != HCP)
	eprintf("EDGE_WEIGHT_TYPE is missing");
    if (WeightType == EXPLICIT && WeightFormat == -1)
	eprintf("EDGE_WEIGHT_FORMAT is missing");
    if (WeightType != EXPLICIT && WeightType != SPECIAL
	&& WeightFormat != -1)
	eprintf("Conflicting EDGE_WEIGHT_TYPE and EDGE_WEIGHT_FORMAT (1)");
    if (ProblemType == ATSP && WeightType != EXPLICIT)
	eprintf("Conflicting TYPE and EDGE_WEIGHT_TYPE");
    if (ProblemType == ATSP && WeightFormat != FULL_MATRIX)
	eprintf("Conflicting TYPE and EDGE_WEIGHT_FORMAT");
    if (Seed == 0)
	Seed = 1;
    Swaps = 0;
    if (Precision == 0)
	Precision = 100;
    if (InitialStepSize == 0)
	InitialStepSize = 1;
    if (MaxSwaps < 0)
	MaxSwaps = Dimension;
    if (Runs == 0)
	Runs = 10;
    if (MaxCandidates > Dimension - 1)
	MaxCandidates = Dimension - 1;
    if (AscentCandidates > Dimension - 1)
	AscentCandidates = Dimension - 1;
    if (InitialPeriod < 0) {
	InitialPeriod = Dimension / 2;
	if (InitialPeriod < 100)
	    InitialPeriod = 100;
    }
    if (Excess < 0)
	Excess = 1.0 / Dimension;
    if (MaxTrials == 0)
	MaxTrials = Dimension;
    MakeHeap(Dimension);
    if (CostMatrix == 0 && Dimension <= MaxMatrixDimension && Distance != 0
	&& Distance != Distance_1 && Distance != Distance_ATSP) {
	Node *Ni, *Nj;
	CostMatrix =
	       (long *) calloc(Dimension * (Dimension - 1) / 2,
			       sizeof(long));
        assert(CostMatrix);
	Ni = FirstNode->Suc;
	do {
	    Ni->C = &CostMatrix[(Ni->Id - 1) * (Ni->Id - 2) / 2] - 1;
	    if (ProblemType != HPP || Ni->Id < Dimension)
		for (Nj = FirstNode; Nj != Ni; Nj = Nj->Suc)
		    Ni->C[Nj->Id] = Fixed(Ni, Nj) ? 0 : Distance(Ni, Nj);
	    else
		for (Nj = FirstNode; Nj != Ni; Nj = Nj->Suc)
		    Ni->C[Nj->Id] = 0;
	}
	while ((Ni = Ni->Suc) != FirstNode);
	WeightType = EXPLICIT;
	c = 0;
    }
    C = WeightType == EXPLICIT ? C_EXPLICIT : C_FUNCTION;
    D = WeightType == EXPLICIT ? D_EXPLICIT : D_FUNCTION;
    if (MoveType == 0)
	MoveType = 5;
    if (InputTourFileName)
	ReadTour(InputTourFileName, &InputTourFile);
    if (InitialTourFileName)
	ReadTour(InitialTourFileName, &InitialTourFile);
    for (i = 0; i <= 1; i++)
	if (MergeTourFileName[i])
	    ReadTour(MergeTourFileName[i], &MergeTourFile[i]);
    switch (MoveType) {
    case 2:
	BestMove = Best2OptMove;
	break;
    case 3:
	BestMove = Best3OptMove;
	break;
    case 4:
	BestMove = Best4OptMove;
	break;
    case 5:
	BestMove = Best5OptMove;
	break;
    }
    switch (BacktrackMoveType) {
    case 2:
	BacktrackMove = Backtrack2OptMove;
	break;
    case 3:
	BacktrackMove = Backtrack3OptMove;
	break;
    case 4:
	BacktrackMove = Backtrack4OptMove;
	break;
    case 5:
	BacktrackMove = Backtrack5OptMove;
	break;
    }

    if (TraceLevel >= 1)
	PrintParameters();
    CreateCandidateSet();
    AllocateStructures();
    if (TraceLevel >= 1)
	printf("Preprocessing time = %0.1lf sec.\n\n",
	       GetTime() - LastTime);

    if (Norm != 0) {
	BestCost = DBL_MAX;
	WorstCost = -DBL_MAX;
	Successes = 0;
    } else {
	/* The ascent has solved the problem! */
	Successes = 1;
	if (!AlwaysComputeEM) Runs = 0;
	RecordBetterTour();
	RecordBestTour();
	BestCost = WorstCost = Cost = CostSum = LowerBound;
	PrintBestTour();
    }
    TimeSum = 0;

    /* Find a specified number (Runs) of local optima */
    for (Run = 1; Run <= Runs; Run++) {
	LastTime = GetTime();
	Cost = FindTour();	/* using the Lin-Kerninghan heuristics */
	if (*CurrentStopFlag) break;
	if (Cost < BestCost) {
	    BestCost = Cost;
	    RecordBestTour();
	    PrintBestTour();
	}
	if (Cost < Optimum) {
	    Node *N;
	    N = FirstNode;
	    while ((N = N->OptimumSuc = N->Suc) != FirstNode);
	    Optimum = Cost;
	    printf("*** New optimum = %0.0lf ***\n\n", Optimum);
	}
	Time = GetTime() - LastTime;
	if (TraceLevel >= 1 && Trial > 0)
	    printf("Run %ld: Cost = %0.0lf, Time = %0.1lf sec.\n\n",
		   Run, Cost, Time);
	/* Update statistics */
	if (Cost > WorstCost)
	    WorstCost = Cost;
	if (Cost <= Optimum)
	    Successes++;
	CostSum += Cost;
	TrialSum += Trial;
	if (Trial < TrialsMin)
	    TrialsMin = Trial;
	if (Trial > TrialsMax)
	    TrialsMax = Trial;
	TimeSum += Time;
	if (Time < TimeMin)
	    TimeMin = Time;
	if (Time > TimeMax)
	    TimeMax = Time;
	srand(++Seed);
    }

    /* Report the results */
    if (TraceLevel >= 0) printf("Successes/Runs = %ld/%ld \n", Successes, Runs);
    if (Runs == 0) {
      Runs = 1;
      TrialsMin = TrialsMax = 0;
      TimeMin = 0;
    }
    if (TrialsMax > 0) {
      if (TraceLevel >= 0) printf
	("Cost.min = %0.0lf, Cost.max = %0.0lf, Cost.avg = %0.2lf\n",
	 BestCost, WorstCost, CostSum / Runs);
      if (Optimum == -DBL_MAX)
	Optimum = BestCost;
      if (TraceLevel >= 0) printf
	("Gap.min = %0.3lf%%, Gap.max = %0.3lf%%, Gap.avg = %0.3lf%%\n",
	 (BestCost - Optimum) / Optimum * 100.0,
	 (WorstCost - Optimum) / Optimum * 100.0,
	 (CostSum / Runs - Optimum) / Optimum * 100.0);
    }
    if (TraceLevel >= 0) printf("Trials.min = %ld, Trials.max = %ld, Trials.avg = %0.1lf\n",
	   TrialsMin, TrialsMax, 1.0 * TrialSum / Runs);
    if (TraceLevel >= 0) printf
      ("Time.min = %0.1lf sec., Time.max = %0.1lf sec., Time.avg = %0.1lf sec.\n",
       TimeMin, TimeMax, TimeSum / Runs);

    p->BestTour = (long *) calloc(Dimension + 1, sizeof(long));
    assert(p->BestTour);
    for (i = 1; i <= Dimension; i++)
	p->BestTour[i] = BestTour[i];
    p->BestCost = BestCost;
    p->LowerBound = LowerBound;
    FreeStructures();
    if (TraceLevel >= 0) printf("\n");
}

long *BestTour, Dimension, MaxCandidates, AscentCandidates, InitialPeriod,
    InitialStepSize, Precision, Runs, MaxTrials, MaxSwaps;
double BestCost, WorstCost, Excess, Optimum;
unsigned int Seed;
int Subgradient, TraceLevel, MoveType, BacktrackMoveType, RestrictedSearch;

Node *NodeSet, *FirstNode, *FirstActive, *LastActive, **Heap;
SwapRecord *SwapStack;
long Swaps, Norm, M, GroupSize, Groups, Trial, *BetterTour, *CacheVal,
    *CacheSig, *CostMatrix, MaxMatrixDimension = 2000;
double BetterCost, LowerBound;
unsigned long Hash;
int *Rand, Reversed;
Segment *FirstSegment;
void *HTable;

FILE *ParameterFile, *ProblemFile, *PiFile, *TourFile,
    *InputTourFile, *CandidateFile, *InitialTourFile, *MergeTourFile[2];
char *ParameterFileName, *ProblemFileName, *PiFileName, *TourFileName,
    *InputTourFileName, *CandidateFileName, *InitialTourFileName,
    *MergeTourFileName[2];
char *Name, *Type, *EdgeWeightType, *EdgeWeightFormat, *EdgeDataFormat,
    *NodeCoordType, *DisplayDataType;
int ProblemType = -1, WeightType = -1, WeightFormat = -1, CoordType =
    NO_COORDS, CandidateSetSymmetric = 0;

long (*Distance) (Node * Na, Node * Nb);
long (*C) (Node * Na, Node * Nb);
long (*D) (Node * Na, Node * Nb);
long (*c) (Node * Na, Node * Nb);
Node *(*BestMove) (Node * t1, Node * t2, long *G0, long *Gain);
Node *(*BacktrackMove) (Node * t1, Node * t2, long *G0, long *Gain);

static void Read_EDGE_WEIGHT_SECTION(Problem * p)
{
    Node *Ni, *Nj;
    long i, j, k, n, W;

    if (ProblemType != ATSP) {
	CostMatrix =
	       (long *) calloc(Dimension * (Dimension - 1) / 2,
			       sizeof(long));
        assert(CostMatrix);
	Ni = FirstNode->Suc;
	do {
	    Ni->C = &CostMatrix[(Ni->Id - 1) * (Ni->Id - 2) / 2] - 1;
	}
	while ((Ni = Ni->Suc) != FirstNode);
    } else {
	n = Dimension / 2;
        CostMatrix = (long *) calloc(n * n, sizeof(long));
	assert(CostMatrix);
	for (Ni = FirstNode; Ni->Id <= n; Ni = Ni->Suc)
	    Ni->C = &CostMatrix[(Ni->Id - 1) * n] - 1;
    }
    if (ProblemType == HPP)
	Dimension--;
    switch (WeightFormat) {
    case FULL_MATRIX:
	if (ProblemType == ATSP) {
	    long n = Dimension / 2;
	    for (i = 1; i <= n; i++) {
		Ni = &NodeSet[i];
		for (j = 1; j <= n; j++) {
		    if (!fscanf(ProblemFile, "%ld", &W))
			eprintf("Missing weight in EDGE_WEIGHT_SECTION");
		    Ni->C[j] = W;
		    if (i != j && W > M)
			M = W;
		}
		Nj = &NodeSet[i + n];
		if (!Ni->FixedTo1)
		    Ni->FixedTo1 = Nj;
		else if (!Ni->FixedTo2)
		    Ni->FixedTo2 = Nj;
		if (!Nj->FixedTo1)
		    Nj->FixedTo1 = Ni;
		else if (!Nj->FixedTo2)
		    Nj->FixedTo2 = Ni;
	    }
	    Distance = Distance_ATSP;
	    WeightType = -1;
	} else
	    k = 0;
	for (i = 1, Ni = FirstNode; i <= Dimension; i++, Ni = Ni->Suc) {
	    for (j = 1; j <= Dimension; j++) {
		W = p->C[k++];
		if (j < i)
		    Ni->C[j] = W;
	    }
	}
	break;
    case UPPER_ROW:
	k = 0;
	for (i = 1, Ni = FirstNode; i < Dimension; i++, Ni = Ni->Suc) {
	    for (j = i + 1, Nj = Ni->Suc; j <= Dimension;
		 j++, Nj = Nj->Suc) {
		W = p->C[k++];
		Nj->C[i] = W;
	    }
	}
	break;
    case LOWER_ROW:
	k = 0;
	for (i = 2, Ni = FirstNode->Suc; i <= Dimension; i++, Ni = Ni->Suc) {
	    for (j = 1; j < i; j++) {
		W = p->C[k++];
		Ni->C[j] = W;
	    }
	}
	break;
    case UPPER_DIAG_ROW:
	k = 0;
	for (i = 1, Ni = FirstNode; i <= Dimension; i++, Ni = Ni->Suc) {
	    for (j = i, Nj = Ni; j <= Dimension; j++, Nj = Nj->Suc) {
		W = p->C[k++];
		if (i != j)
		    Nj->C[i] = W;
	    }
	}
	break;
    case LOWER_DIAG_ROW:
	k = 0;
	for (i = 1, Ni = FirstNode; i <= Dimension; i++, Ni = Ni->Suc) {
	    for (j = 1; j <= i; j++) {
		W = p->C[k++];
		if (j != i)
		    Ni->C[j] = W;
	    }
	}
	break;
    case UPPER_COL:
	k = 0;
	for (j = 2, Nj = FirstNode->Suc; j <= Dimension; j++, Nj = Nj->Suc) {
	    for (i = 1; i < j; i++) {
		W = p->C[k++];
		Nj->C[i] = W;
	    }
	}
	break;
    case LOWER_COL:
	k = 0;
	for (j = 1, Nj = FirstNode; j < Dimension; j++, Nj = Nj->Suc) {
	    for (i = j + 1, Ni = Nj->Suc; i <= Dimension;
		 i++, Ni = Ni->Suc) {
		W = p->C[k++];
		Ni->C[j] = W;
	    }
	}
	break;
    case UPPER_DIAG_COL:
	k = 0;
	for (j = 1, Nj = FirstNode; j <= Dimension; j++, Nj = Nj->Suc) {
	    for (i = 1; i <= j; i++) {
		W = p->C[k++];
		if (i != j)
		    Nj->C[i] = W;
	    }
	}
	break;
    case LOWER_DIAG_COL:
	k = 0;
	for (j = 1, Nj = FirstNode; j <= Dimension; j++, Ni = Ni->Suc) {
	    for (i = j, Ni = Nj; i <= Dimension; i++, Ni = Ni->Suc) {
		W = p->C[k++];
		if (i != j)
		    Ni->C[j] = W;
	    }
	}
	break;
    }
    if (ProblemType == HPP)
	Dimension++;
}

void FreeProblem(Problem *p)
{
  if (p) {
    if (p->NAME) free(p->NAME);
    if (p->C) free(p->C);
    if (p->BestTour) free(p->BestTour);
    if (p->INITIAL_TOUR) free(p->INITIAL_TOUR);
    free(p);
  }
}
