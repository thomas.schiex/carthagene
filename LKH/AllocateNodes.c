#include "LK.h"

/*      
 * The AllocateNodes function allocates the nodes of the graph.
 */

void AllocateNodes()
{
    Node *Prev, *N;
    long i;

    free(NodeSet);
    if (Dimension <= 0)
	eprintf("DIMENSION is not positive (or not specified)");
    if (ProblemType == ATSP)
	Dimension *= 2;
    else if (ProblemType == HPP) {
	Dimension++;
	if (Dimension > MaxMatrixDimension)
	    eprintf("Dimension too large in HPP problem");
    }
    NodeSet = (Node *) calloc(Dimension + 1, sizeof(Node)); assert(NodeSet);
    for (i = 1; i <= Dimension; i++, Prev = N) {
	N = &NodeSet[i];
	if (i == 1)
	    FirstNode = N;
	else
	    Link(Prev, N);
	N->Id = i;
    }
    Link(N, FirstNode);
}
