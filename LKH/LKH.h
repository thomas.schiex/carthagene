#ifndef _LKH_H
#define _LKH_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Problem {
    /* Data part */
    char *NAME;
    const char *TYPE;
    long DIMENSION;
    const char *EDGE_WEIGHT_TYPE;
    const char *EDGE_WEIGHT_FORMAT;
    char *NODE_COORD_TYPE;
    double *X, *Y, *Z;
    long *C;
    long FIXED_EDGES_COUNT;
    long *FIXED_EDGES[2];

    /* Parameter part */
    long ASCENT_CANDIDATES;
    long BACKTRACK_MOVE_TYPE;
    long *CANDIDATE_ID;
    long *CANDIDATE_DAD;
    long *CANDIDATE_COUNT;
    long **CANDIDATE_TO;
    long **CANDIDATE_ALPHA;
    int DELAUNAY;
    double EXCESS;
    int GREEDY;
    long INITIAL_PERIOD;
    long INITIAL_STEP_SIZE;
    long *INITIAL_TOUR;
    long *INPUT_TOUR;
    long MAX_CANDIDATES;
    long MAX_SWAPS;
    long MAX_TRIALS;
    long *MERGE_TOUR[2];
    long MOVE_TYPE;
    double OPTIMUM;
    long *PI_ID;
    long *PI;
    long PRECISION;
    int RESTRICTED_SEARCH;
    long RUNS;
    unsigned int SEED;
    int STOP_AT_OPTIMUM;
    int SUBGRADIENT;
    long SUBPROBLEM_SIZE;
    long *SUBPROBLEM_TOUR;
    long SUBSEQUENT_MOVE_TYPE;
    int SYMMETRIC_CANDIDATES;
    double TIME_LIMIT;
    int TRACE_LEVEL;

    /* Result part */
    long *BestTour;
    double LowerBound;
    double BestCost;
} Problem;

Problem *DefaultProblem();

void LKH(Problem * p);

void FreeProblem(Problem * p);

#ifdef __cplusplus
}
#endif

#endif
