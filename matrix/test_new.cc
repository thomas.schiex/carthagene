#include <iostream>
#include <iomanip>
#include <vector>

#include <hash_map>

#include <algorithm>
#include <functional>

#define WITH_PARALLEL

#include "../parallel/parallel.h"
#include <semaphore.h>

#if 1
#define SYNCHRONIZATION_TRAITS tbb_sync
struct tbb_sync {
#if TBB_INTERFACE_VERSION >= 5000
	typedef tbb::mutex mutex_type;
	typedef tbb::spin_rw_mutex_v3 rw_spin_type;
	typedef tbb::spin_mutex spin_type;
	typedef tbb::interface5::condition_variable condvar_type;
	typedef tbb::interface5::unique_lock<tbb::mutex> unique_lock_type;
#else
/*#	warning Using old TBB ; not everything is available*/
	class mutex_type {
		public:
			typedef pthread_mutex_t handle_type;
			mutex_type() { pthread_mutex_init(&handle, NULL); }
			void lock() { pthread_mutex_lock(&handle); }
			void unlock() { pthread_mutex_unlock(&handle); }
			handle_type* native_handle() { return &handle; }
		private:
			handle_type handle;
	};
	class rw_spin_type {
		public:
			typedef void handle_type;
			void lock();
			void lock_read();
			void unlock();
	};
	class spin_type {
		public:
			typedef pthread_spinlock_t handle_type;
			spin_type() { pthread_spin_init(&handle, 0); }
			void lock() { pthread_spin_lock(&handle); }
			void unlock() { pthread_spin_unlock(&handle); }
		private:
			handle_type handle;
	};
	typedef mutex_type unique_lock_type;
	class condvar_type {
		public:
			typedef pthread_cond_t handle_type;
			condvar_type() { pthread_cond_init(&handle, NULL); }
			void notify_one() { pthread_cond_signal(&handle); }
			void notify_all() { pthread_cond_broadcast(&handle); }
			void wait(unique_lock_type&ul) { pthread_cond_wait(&handle, ul.native_handle()); }
		private:
			handle_type handle;
	};
#endif
};
#endif
#include "matrix_new.h"

using namespace matrix;
using namespace data;

/*struct no_sync {*/
	/*class mutex_type {*/
		/*public:*/
			/*mutex_type() {}*/
			/*~mutex_type() {}*/
			/*void lock() {}*/
			/*void unlock() {}*/
	/*};*/
	/*typedef Parallel::CondVar condvar_type;*/
/*};*/

struct spin_sync {
	class mutex_type {
		private:
			pthread_spinlock_t sl;
		public:
			mutex_type() { pthread_spin_init(&sl, 0); }
			~mutex_type() { pthread_spin_destroy(&sl); }
			void lock() { pthread_spin_lock(&sl); }
			void unlock() { pthread_spin_unlock(&sl); }
	};
	typedef Parallel::CondVar condvar_type;
};

struct sem_sync {
	class mutex_type {
		private:
			sem_t sl;
		public:
			mutex_type() { sem_init(&sl, 0, 1); }
			~mutex_type() { sem_destroy(&sl); }
			void lock() { sem_wait(&sl); }
			void unlock() { sem_post(&sl); }
	};
	typedef Parallel::CondVar condvar_type;
};

struct mutex_sync {
	typedef Parallel::Mutex mutex_type;
	typedef Parallel::CondVar condvar_type;
};

struct test_const {
	static const int i=42;
};

template<int i> class hop { public: void foo() { std::cout<<i<<std::endl; } };

template<typename toto> class pouet : public hop<toto::i> {};

#define REPEAT 1LL
#define ROWS 20000LL
#define COLS 20000LL

#define FMLA(i_, j_) ((i_+1)*65536+j_+1)

struct comp_int
{
	struct pagefile_name_provider_type {
		pagefile_name_provider_type(void*) {}
		operator const char*() const { return "my_pagefile.dat"; }
	};
	static const bool delete_pagefile_when_done = false;
	typedef int data_type;
	typedef void* context_type;
	static void clear_data(data_type&d) { d=0; }

	struct computation_handler_type {
		static void compute(context_type c, int i, int j, int& v) {
			v = FMLA(i, j);
		}
		static bool must_compute(context_type c, int &v) {
			return !v;
		}
	};
};

struct check_int : public comp_int {
	typedef void computation_handler_type;
};

void dummy(int i, int j, int v) {
	if(FMLA(i, j)-v) {
		(void)0;
	} else {
		(void)0;
	}
}

template <typename O, typename D, typename A, typename S>
O& operator<<(O& os, Matrix<D, A, S>& m) {
	for(int i=0;i<3;i++) {
		for(int j=0;j<3;j++) {
			os << m[i][j] << '\t';
		}
		os << std::endl;
	}
	return os;
}


struct scope_chrono {
	Utils::AccumChrono c;
	Utils::Tick t0;
	scope_chrono()
		: c()
	{ t0 = c.start(); }
	~scope_chrono() {
		c.stop(t0);
		std::cout << "Time taken : " << c.time() << std::endl;
	}
};


struct offsets {
	std::vector<int> ivec, jvec;
	Utils::ProgressDisplay disp;
	offsets(offset_type size)
		: ivec(), jvec(), disp(size)
	{
		ivec.resize(size);
		jvec.resize(size);
		for(int i=0;i<size;i++) {
			ivec[i] = jvec[i] = i;
		}
		std::random_shuffle(ivec.begin(), ivec.end());
		std::random_shuffle(jvec.begin(), jvec.end());
	}

	template <typename M, typename F>
		struct iterate_row {
			offsets& o;
			F& f;
			M& m;
			iterate_row(offsets&o_, F& f_, M& m_) : o(o_), f(f_), m(m_) {}
			iterate_row(const iterate_row& ir) : o(ir.o), f(ir.f), m(ir.m) {}
			iterate_row<M, F>& operator= (const iterate_row<M, F>& ir) {
				
			}
			void operator()(int i) {
				Utils::AccumChrono chrono;
				Utils::Tick t0 = chrono.start();
				std::vector<int>::iterator b, e=o.jvec.end();
				for(b=o.jvec.begin();b!=e;++b) {
					f(m, o.ivec[i], *b);
				}
				chrono.stop(t0);
				o.disp.step_done(chrono.time());
			}
		};

	template <typename M, typename F>
		void iterate_parallel(M&m, F f) {
			iterate_row<M, F> ir(*this, f, m);
			Parallel::Range<>().execute_range(ir, 0, ivec.size());
		}

	template <typename M, typename F>
		void iterate(M&m, F f) {
			std::vector<int>::iterator i, iend, j, jend;
			i = ivec.begin();
			iend = ivec.end();
			jend = jvec.end();
			while(i!=iend) {
				j = jvec.begin();
				while(j!=jend) {
					f(m, *i, *j);
					++j;
				}
				++i;
			}
		}
};

struct rw_do {
	template <typename M>
	void operator()(M&m, int i, int j) {
		m[i][j] = FMLA(i, j);
	}
};

struct ro_do {
	template <typename M>
	void operator()(M&m, int i, int j) {
		dummy(i, j, m[i][j]);
	}
};

struct second_pass {
	struct failure : public std::exception {
		~failure() throw() {}
		std::string msg;
		failure(const char*w) : msg(w) {}
		const char* what() const throw() {
			return msg.c_str();
		}
	};
	template <typename M>
	void operator()(M&m, int i, int j) {
		if(m[i][j]!=FMLA(i, j)) {
			char buf[256];
			sprintf(buf, "[%i,%i] : expected %i, got %i", i, j, FMLA(i, j), (int)m[i][j]);
			throw failure(buf);
		}
	}
};

template <typename S>
std::pair<int, int> test_ro(offset_type size) {
	offsets o(size);
	std::pair<int, int> ret;
	{
		Matrix<comp_int, Rectangular<>, S> mat(NULL, size, size);
		Utils::AccumChrono c;
		Utils::Tick t0 = c.start();
		o.iterate_parallel(mat, ro_do());
		c.stop(t0);
		ret.first = ((unsigned int)((size*size)/c.time()));
	}
	o.disp.reset();
	{
		Matrix<check_int, Rectangular<>, S> checkmat(NULL, size, size);
		Utils::AccumChrono c;
		Utils::Tick t0 = c.start();
		o.iterate_parallel(checkmat, second_pass());
		c.stop(t0);
		ret.second = ((unsigned int)((size*size)/c.time()));
	}
	std::cout << std::endl;
	return ret;
}

template <typename S>
unsigned int test_rw(offset_type size) {
	Utils::AccumChrono c;
	Utils::Tick t0 = c.start();
	Matrix<Int, Rectangular<>, S> mat(NULL, size, size);
	offsets o(size);
	o.iterate(mat, rw_do());
	c.stop(t0);
	return ((unsigned int)((size*size)/c.time()));
}

int main(int argc, char**argv) {
#if 0
	std::cout << "rw buffer" << std::endl;
	{
		scope_chrono chrono;
		Matrix<Int> rw_mat(NULL, 3, 3);
		for(int i=0;i<3;i++) for(int j=0;j<3;j++) {
			rw_mat[i][j] = i*10+j;
		}
		std::cout << rw_mat;
	}
#endif
#if 0
	std::cout << "ro buffer" << std::endl;
	{
		scope_chrono chrono;
		Matrix<comp_int> ro_mat(NULL, 3, 3);
		std::cout << ro_mat;
	}
#endif
#if 0
	std::cout << "ro disk (seek/read/write)" << std::endl;
	{
		scope_chrono chrono;
		Matrix<comp_int, Rectangular<>, storage::direct_file> ro_mat(NULL, 3, 3);
		std::cout << ro_mat;
	}
#endif
#if 0
	std::cout << "rw disk (seek/read/write) 1M" << std::endl;
	{
		scope_chrono chrono;
		Matrix<Int, Rectangular<>, storage::direct_file> rw_mat(NULL, 1000, 1000);
		for(int i=0;i<1000;++i) for(int j=0;j<1000;++j) rw_mat[i][j] = (i+1)*1000+j;
	}
#endif
#if 0
	std::cout << "rw buffer      1M : " << test_rw<storage::buffer>(1000) << std::endl;
	std::cout << "rw disk        1M : " << test_rw<storage::direct_file>(1000) << std::endl;
	std::cout << "rw mmap        1M : " << test_rw<storage::cached_file>(1000) << std::endl;
	std::cout << std::endl;
	std::cout << "ro buffer      1M : " << test_ro<storage::buffer>(1000) << std::endl;
	std::cout << "ro disk        1M : " << test_ro<storage::direct_file>(1000) << std::endl;
	std::cout << "ro mmap        1M : " << test_ro<storage::cached_file>(1000) << std::endl;
	std::cout << std::endl;
	std::cout << "rw buffer    100M : " << test_rw<storage::buffer>(10000) << std::endl;
	/*std::cout << "rw disk      100M : " << test_rw<storage::direct_file>(10000) << std::endl;*/
	std::cout << "rw mmap      100M : " << test_rw<storage::cached_file>(10000) << std::endl;
	std::cout << std::endl;
	std::cout << "ro buffer    100M : " << test_ro<storage::buffer>(10000) << std::endl;
	/*std::cout << "ro disk      100M : " << test_ro<storage::direct_file>(10000) << std::endl;*/
	std::cout << "ro mmap      100M : " << test_ro<storage::cached_file>(10000) << std::endl;
	std::cout << std::endl;
#endif
	std::cout << "sizeof(off_t) = " << sizeof(off_t) << std::endl;
	/*std::pair<int, int> hits_per_second = test_ro<storage::cached_file>(1024);*/
	/*std::pair<int, int> hits_per_second = test_ro<storage::cached_file>(50);*/
	std::pair<int, int> hits_per_second = test_ro<storage::boost_file>(50000);
	std::cout << "ro adapt       2.5G : " << hits_per_second.first << std::endl;
	std::cout << "ro adapt check 2.5G : " << hits_per_second.second << std::endl;

	return 0;
}

