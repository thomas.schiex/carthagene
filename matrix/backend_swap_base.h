#ifndef _MATRIX_BACKEND_SWAP_BASE_H_
#define _MATRIX_BACKEND_SWAP_BASE_H_

#include "backend_base.h"

extern "C" {
#include <sys/param.h> // for PATH_MAX in linux/limits.h
#include <unistd.h>
#include <stdlib.h> // for mkstemp
#include <string.h>
#include "ubiquitous_mmap.h"
/*#include <sys/mman.h>*/
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
}

#ifdef WIN32
#define O_NOATIME 0
#define S_IRGRP 0
#define S_IWGRP 0
#endif

#include <cassert>
#include <typeinfo>
#include <string>
/*#include <set>*/
#include <list>

#define PAGE_CACHE_BYTESIZE (1<<25)	/* 30 for 1Go, 25 for 32Mo... */

namespace matrix {
	namespace backend {

		typedef sync::default_traits sync_traits;

		namespace impl {
			class Page;

			const addressing_space::offset_type pagesize = 64*1024;	/* allow for compiler optimization (replace / by >>), we can be (almost) sure pagesize is aligned on a power of 2 and < 64K, it's currently 4096 bytes) */
			const addressing_space::offset_type max_page_count=PAGE_CACHE_BYTESIZE/pagesize;	/* guess pagesize is always aligned on a power of 2 */

			class Failure : public std::string {
				public:
					Failure(int err) : std::string(strerror(err)) {}
			};

			class CacheFile {
				private:
					/*int fd;*/
                    handle_type fd;
					offset_type size;
					std::string name;
					bool unlink_when_done;
#ifdef WIN32
                    HANDLE hMapFile;
#endif
				public:
					CacheFile(offset_type size_, const char*filename=NULL, bool uwd=true)
						: unlink_when_done(uwd)
#ifdef WIN32
                          /*, Base(0)*/
#endif
					{
						size = ((size_+pagesize-1)/pagesize)*pagesize; /* fugly, but no better idea */
						/*std::cerr << name << " size set to " << (size/1048576.) << " MB (there were " << size_ << " bytes requested), that is " << (size/pagesize) << " pages" << std::endl;*/
/*#ifndef _WIN32*/
                        open_file(filename);
/*#else*/
                        /*static char dummy[pagesize];*/
                        /*for(offset_type i=0;i<size;i+=pagesize) {*/
                            /*write(fd, &dummy, pagesize);*/
                        /*}*/
/*#endif*/
						size=size_;
					}
	~CacheFile() {
						/*std::cerr << "~CacheFile(" << name << ')' << std::endl;*/
						close_file();
						if(unlink_when_done) {
							if(unlink(name.c_str())) {
								std::cerr << "couldn't remove pagefile " << name
										  << " : " << strerror(errno) << std::endl;
							}
						}
					}
					offset_type count_pages() const {
						return size/pagesize;
					}
					const std::string& filename() const { return name; }

#ifdef WIN32
                    void close_file() {
                        /*UnmapViewOfFile(Base);*/
                        CloseHandle(hMapFile);
                        CloseHandle(fd);
                    }

                    void make_tempname(std::string& out) {
                        TCHAR szTempFileName[MAX_PATH];  
                        TCHAR lpTempPathBuffer[MAX_PATH];
                        GetTempPath(MAX_PATH, lpTempPathBuffer); 
                        GetTempFileName(lpTempPathBuffer, TEXT("MATRIX"), 0, szTempFileName);
                        out = szTempFileName;
                    }


                    void open_file(const char* filename) {
                        if(filename) {
                            name = filename;
                        } else {
                            make_tempname(name);
                        }
                        fd = CreateFile(name.c_str(), 
                                GENERIC_READ | GENERIC_WRITE, 
                                FILE_SHARE_READ | FILE_SHARE_WRITE, 
                                NULL, 
                                OPEN_ALWAYS, 
                                FILE_ATTRIBUTE_NORMAL, 
                                NULL);
                        if(fd == INVALID_HANDLE_VALUE)
                        {
                            std::cerr << "File " << name << " not opened! (error #" << GetLastError() << ')' << std::endl;
                            throw std::string("Can't open swap file");
                        }

                        struct stat st;
                        if(stat(name.c_str(), &st)) {
                            throw std::string("Can't stat() swap file");
                        }
                        if(st.st_size<size) {
    						char dummy=0;
                            unsigned long sizeLS = (unsigned long)(size&0xFFFFFFFF);
                            long sizeMS = (unsigned long)(size>>32);
                            SetFilePointer(fd, sizeLS-1, &sizeMS, 0);
	    					/*fseek(fd, size-1, SEEK_SET);*/
                            unsigned long unused;
		    				if(WriteFile(fd, &dummy, 1, &unused, NULL)==-1) {
			    				std::cerr << "Can't write to swap file " << name << " :( " << strerror(errno) << std::endl;
				    			throw std::string("Can't write swap file");
					    	}
                        }
                        hMapFile = CreateFileMapping(fd, NULL, PAGE_READWRITE, 0, size, NULL);
                        /*Base = (char*)MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, size);*/
                    }

					void* map_page(offset_type offset) {
						/*void* ret = mmap(NULL, pagesize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, offset);*/
						/*if(ret==MAP_FAILED) {*/
							/*std::cerr << "mmap failed : " << strerror(errno) << std::endl;*/
							/*throw Failure(errno);*/
						/*}*/
						/*return ret;*/

                        /*return Base+offset;*/

                        void* ret = (char*)MapViewOfFile(hMapFile, FILE_MAP_READ|FILE_MAP_WRITE, offset>>32, offset&((1LL<<32)-1), pagesize);
						if(!ret) {
							/*std::cerr << "mmap failed : " << strerror(errno) << std::endl;*/
							throw Failure(errno);
						}
                        return ret;
					}
#else
                    void close_file() {
                        close(fd);
                    }

                    void open_file(const char* filename) {
						if(filename) {
							name = filename;
							fd = open(name.c_str(),
									O_CREAT|O_NOATIME|O_RDWR,
									S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
						} else {
							char path[PATH_MAX];
							strcpy(path, "pagefile_XXXXXX");
							fd = mkstemp(path);
							name=path;
						}
						if(fd==-1) {
							std::cerr << "Couldn't open swap file " << name.c_str() << " because of error " << strerror(errno) << std::endl;
							throw std::string("Can't write swap file");
						}

                        struct stat st;
                        if(stat(name.c_str(), &st)) {
                            throw std::string("Can't stat() swap file");
                        }
                        if(st.st_size<size) {
    						char dummy=0;
	    					lseek(fd, size-1, SEEK_SET);
		    				if(write(fd, &dummy, 1)==-1) {
			    				std::cerr << "Can't write to swap file " << name << " :( " << strerror(errno) << std::endl;
				    			throw std::string("Can't write swap file");
					    	}
                        }
                    }

					void* map_page(offset_type offset) {
						void* ret = mmap(NULL, pagesize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, offset);
						if(ret==MAP_FAILED) {
							/*std::cerr << "mmap failed : " << strerror(errno) << std::endl;*/
							/*throw Failure(errno);*/
							return NULL;
						}
						int mret;
						if((mret=madvise(ret, pagesize, MADV_RANDOM))) {
							/*std::cerr << "madvise failed : " << strerror(mret) << std::endl;*/
						}
						return ret;
					}
#endif
			};
		}
	}
}

#endif

