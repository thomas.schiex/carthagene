#ifndef _MATRIX_BACKEND_H_
#define _MATRIX_BACKEND_H_

#include "base.h"
#include "addressing_space.h"

#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
#	define BACKEND_INHERITANCE : public Generic<DATA_TRAITS, SYNC_TRAITS>
#else
#	define BACKEND_INHERITANCE
#endif

namespace matrix {
	namespace backend {
		using ::matrix::addressing_space::offset_type;
		using ::matrix::constraints::check_not_void;

        struct disallow_partial_storage {};

		template <typename CHT>
			struct check_allow_partial_storage {
				enum { value = true };
			};

		template <> struct check_allow_partial_storage<void> {
			enum { value = false };
		};

		template <> struct check_allow_partial_storage<disallow_partial_storage> {
			enum { value = false };
		};

		struct partial_storage {
			template <typename D>
				static const bool is_compatible(D*x) {
					return	/*check_not_void<
								typename D::computation_handler_type
							>::value
								&&*/
							check_allow_partial_storage<
								typename D::computation_handler_type
							>::value;
				}
		};

		struct full_storage {
			template <typename D>
				static const bool is_compatible(D*x) {
					return true;
				}
		};

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			class Generic {
				public:
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename DATA_TRAITS::context_type context_type;
					virtual ~Generic() {}
					virtual data_type* raw_get(offset_type o) = 0;
					virtual void init(offset_type size) = 0;
					virtual void lock_data(offset_type o) = 0;
					virtual void unlock_data(offset_type o) = 0;
					void raw_set(offset_type o, const data_type& d) const { *raw_get(o) = d; }
					void raw_set(offset_type o, const data_type d) const { *raw_get(o) = d; }
			};

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			class Buffer BACKEND_INHERITANCE {
				public:
					typedef full_storage storage_type;
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename DATA_TRAITS::context_type context_type;
					Buffer()
#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
						: Generic<DATA_TRAITS, SYNC_TRAITS>(), buffer(0)
#else
						: buffer(0)
#endif
					{}
					Buffer(void*M)
#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
						: Generic<DATA_TRAITS, SYNC_TRAITS>(), buffer(0)
#else
						: buffer(0)
#endif
					{}
					~Buffer() { if(buffer) { delete[] buffer; } }
					void init(offset_type size) {
						if(buffer) { delete[] buffer; }
						buffer = new data_type[size];
					}
					data_type* raw_get(offset_type o) {
						return &buffer[o];
					}
					void lock_data(offset_type o) {}
					void unlock_data(offset_type o) {}
				private:
					data_type* buffer;
			};

		class CacheStats {
			private:
				offset_type hits_;
				offset_type misses_;
				offset_type size_;
				offset_type inuse_;
			public:
				CacheStats() : hits_(0), misses_(0), size_(0), inuse_(0) {}
				void hit() { hits_++; }
				void miss() { misses_++; }
				void set_size(offset_type sz) { size_=sz; }
				void new_value() { misses_++; inuse_++; }

				float occupation() const { return 100.f*inuse_/size_; }
				float raw_efficiency() const { return 100.f*hits_/(hits_+misses_); }
				float efficiency() const { return 100.f*hits_/(hits_+misses_-inuse_); }
				int hits() const { return hits_; }
				int misses() const { return misses_; }
				int inuse() const { return inuse_; }
				int size() const { return size_; }

				void reset() { hits_=misses_=inuse_=0; }

				void print(std::ostream&os = std::cout) const {
					os << "=== cache statistics ======" << std::endl;
					os << "cache size           : " << size() << std::endl;
					os << "cache hits           : " << hits() << std::endl;
					os << "cache misses         : " << misses() << std::endl;
					os << "cache inuse          : " << inuse() << std::endl;
					os << "cache occupation     : " << occupation() << '%' << std::endl;
					os << "cache efficiency     : " << efficiency() << '%' << std::endl;
					os << "cache raw efficiency : " << raw_efficiency() << '%' << std::endl;
				}
		};

		inline std::ostream& operator << (std::ostream&os, CacheStats&cs) { cs.print(os); return os; }
	}

}

#endif

