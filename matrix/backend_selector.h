#ifndef _MATRIX_BACKEND_SELECTOR_H_
#define _MATRIX_BACKEND_SELECTOR_H_

#include "backend_base.h"
#include "backend_hashcache.h"
#include "backend_swap.h"

#include <typeinfo>
#include <cstring>

namespace matrix {

#define RETURN_IF_COMPAT(_name, _B, _pred) \
	if(_B<D, S>::storage_type::is_compatible((D*)0) \
				&& \
			(!ret) \
				&& \
			(_pred) \
			) { \
		try { \
			ret = new _B<D, S>(context); \
			ret->init(size); \
			backend_name = _name; \
			std::cerr << "selected backend `" _name "'." << std::endl; \
		} catch(const std::bad_alloc e) { \
			std::cerr << "couldn't select backend `" _name "', there is not enough memory available" << std::endl; \
			ret = NULL; \
		} \
	}

#define RETURN_IF_COMPAT_BY_NAME(_n, _b) RETURN_IF_COMPAT(_n, _b, !strcmp(backend_name, _n))

	namespace backend {
		template <typename D, typename S>
			Generic<D, S>*
				selector(typename D::context_type context, offset_type size, const char*backend_name=NULL) {
					Generic<D, S>* ret = NULL;
					if(!backend_name) {
						backend_name="none";
						/*RETURN_IF_COMPAT("buffer", Buffer, true);*/
						/*RETURN_IF_COMPAT("hashcache", HashCache, true);*/
						RETURN_IF_COMPAT("swap", Swap, true);
						/*std::cerr << "selected matrix backend `"<<backend_name<<"'"<<std::endl;*/
					} else {
						/*RETURN_IF_COMPAT_BY_NAME("buffer", Buffer);*/
						/*RETURN_IF_COMPAT_BY_NAME("hashcache", HashCache);*/
						RETURN_IF_COMPAT_BY_NAME("swap", Swap);
					}
					return ret;
				}
	}
}


#endif

