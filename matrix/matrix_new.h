#ifndef _MATRIX_H_
#define _MATRIX_H_


#include <iostream>
#include <iomanip>

#include "base.h"
#include "data_source.h"
#include "addressing_space.h"
#include "pagecache.h"
#include "ubiquitous_mmap.h"
#include "storage.h"
#include "middleware.h"


namespace matrix {
	using namespace data;
	using namespace addressing_space;

	/** Handle special cases depending on the addressing space of a matrix.
	 *
	 * Typically, TriangularWithoutMainDiag requires checking that row and column indices are different,
	 * whereas Rectangular and Triangular don't need any test before calling backend_get.
	 */
	template <typename M, typename A>
		struct configurable_fetcher_impl {
			static const typename M::value_type const_get(M*m, offset_type i, offset_type j) {
				/*std::cerr << "generic const_get(i,j)" << std::endl;*/
				return m->raw_const_get(i, j);
			}
			static typename M::reference_type get(M*m, offset_type i, offset_type j) {
				/*std::cerr << "generic get(i,j)" << std::endl;*/
				return m->raw_get(i, j);
			}
		};

	template <typename M, typename O>
		struct configurable_fetcher_impl< M, TriangularWithoutMainDiag<O> > {
			static const typename M::reference_type const_get(M*m, offset_type i, offset_type j) {
				static typename M::data_type nil;
				/*std::cerr << "triangularW/Omaindiag get(i,j)" << std::endl;*/
				if(i==j) {
					return typename M::reference_type();
				}
				return m->raw_const_get(i, j);
			}
			static typename M::reference_type get(M*m, offset_type i, offset_type j) {
				static typename M::data_type nil;
				/*std::cerr << "triangularW/Omaindiag get(i,j)" << std::endl;*/
				if(i==j) {
					return typename M::reference_type();
				}
				return m->raw_get(i, j);
			}
		};

	/** Front-end to configurable_fetcher_impl
	 */
	template <typename M>
		struct configurable_fetcher
			: public configurable_fetcher_impl
			  			<M, typename M::addressing_space> {};
	

	/** The main matrix class, parametrized by :
	 *  - the data traits,
	 *  - the addressing space (defaults to Rectangular<zero>),
	 *  - the synchronization traits (defaults to system defaults),
	 *  - the use of locking reference-to-value (defaults to no locking)
	 */
/*#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
/*#define backend_ backend_*/
/*#define B_ backend_->*/
	/*template <	typename DATA_TRAITS,*/
				/*typename ADDRESSING_SPACE = Rectangular<>,*/
				/*typename SYNC_TRAITS=sync::default_traits,*/
				/*bool use_locking_data_reference=false>*/
/*#else*/
#define backend_ this
#define B_ BACKEND::
	template <	typename DATA_TRAITS,
				typename ADDRESSING_SPACE = Rectangular<>,
			 	typename STORAGE=storage::buffer >
/*#endif*/
		class Matrix
			: protected middleware::selector<DATA_TRAITS, ADDRESSING_SPACE, STORAGE>::middleware_type
		{
			public:
				typedef DATA_TRAITS data_traits;
				typedef ADDRESSING_SPACE addressing_space;
				typedef typename ADDRESSING_SPACE::origin addressing_origin;
				typedef typename DATA_TRAITS::context_type context_type;
				typedef typename middleware::selector<DATA_TRAITS, ADDRESSING_SPACE, STORAGE>::middleware_type middleware_type;
				typedef typename middleware_type::value_type value_type;
				typedef typename middleware_type::reference_type reference_type;
				typedef typename middleware_type::storage_type storage_type;

				typedef Matrix<	DATA_TRAITS,
								ADDRESSING_SPACE,
								STORAGE>
						matrix_type;

				struct row {
					private:
						matrix_type& _;
						offset_type r;
					public:
						row(matrix_type& m, offset_type r_) : _(m), r(r_) {}
						reference_type operator[](offset_type c) {
							return _.get(r, c);
						}
						value_type operator[](offset_type c) const {
							return _.const_get(r, c);
						}
				};

				Matrix(context_type _, offset_type r, offset_type c)
					/*: Provider<DATA_TRAITS, SYNC_TRAITS>(_),*/
					: middleware_type(_, r, c)
				{}

				virtual ~Matrix() {
                    /*std::cerr << "Destroying Matrix instance" << std::endl;*/
                }

				reference_type get(offset_type i, offset_type j) {
					return configurable_fetcher<matrix_type>::get(this, i, j);
				}

				value_type const_get(offset_type i, offset_type j) const {
					return configurable_fetcher<matrix_type>::const_get(this, i, j);
				}

				row operator[](offset_type r) { return row(*this, r); }
				const row operator[](offset_type r) const { return row(*this, r); }

			private:
				friend struct configurable_fetcher_impl<matrix_type, addressing_space>;

		};
}

#endif

