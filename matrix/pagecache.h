#ifndef _MATRIX_PAGECACHE_H_
#define _MATRIX_PAGECACHE_H_

#include <list>

extern "C" {
#include <sys/param.h> // for PATH_MAX in linux/limits.h
#include <unistd.h>
#include <stdlib.h> // for mkstemp
#include <string.h>
#include "ubiquitous_mmap.h"
/*#include <sys/mman.h>*/
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
}

#ifdef WITH_PARALLEL
#  include <tbb/queuing_rw_mutex.h>
#endif

namespace matrix {
	namespace storage {
		namespace cache {
			using addressing_space::offset_type;

#ifndef WIN32
#define pagesize (((4<<10)*256)*4)
#else
#define pagesize (1<<20)
#endif

			class Page {
                public:
                    Page* next;
				private:
                    friend class PageCache;
					void* buffer;
					handle_type fd;
					offset_type offset;
					/*int do_sync_counter;*/
				public:
					Page()
					  : next(0), buffer(0), fd(), offset(0)
					{}
					bool isMapped() const { return !!buffer; }
					bool isSwapped() const { return !buffer; }

					void init(handle_type f_, offset_type o) {
						fd=f_;
						offset=o;
                        buffer=NULL;
                        next=NULL;
					}
					void* operator *() const { return buffer; }
					bool unmap() {
						msync(buffer, pagesize, MS_SYNC);
						int ret;
						if((ret=munmap(buffer, pagesize))) {
							/*std::cerr << "munmap failed : " << strerror(ret) << std::endl;*/
							return false;
						}
						buffer=NULL;
						return true;
					}
					void* map() {
                        errno=0;
                        /*std::cerr << "trying to mmap fd="<<fd<<" offset="<<offset<<" size="<<pagesize<<std::endl;*/
#ifdef WIN32
						buffer = MapViewOfFile(fd, FILE_MAP_READ|FILE_MAP_WRITE, offset>>32, offset&0xFFFFFFFF, pagesize);
						return buffer;
#else
						buffer = mmap(NULL, pagesize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, offset);
						if(buffer==MAP_FAILED) {
							/*std::cerr << "mmap failed : " << strerror(errno) << std::endl;*/
							buffer = NULL;
							/*throw Failure(errno);*/
							return NULL;
						}
						int mret;
						if((mret=madvise(buffer, pagesize, MADV_RANDOM))) {
							/*std::cerr << "madvise failed : " << strerror(mret) << std::endl;*/
						}
                        /*std::cerr << "   => @" << buffer << std::endl;*/
						return buffer;
#endif
					}
					~Page() {}
			};

			class PageCache {
				private:
					/*std::vector<Page*> fifo;*/

					/*typedef std::list<Page*> fifo_type;*/
					/*fifo_type fifo;*/

                    Page* fifo;
                    Page** tail;

					/*size_t cursor;*/
					/*sync::default_traits::spin_type spin;*/
					/*sync::default_traits::mutex_type mutex;*/
#ifdef WITH_PARALLEL
					tbb::queuing_rw_mutex rw_lock;
#endif

					bool unmap_one() {
                        Page ** prev, * cur;
                        prev = &fifo;
                        cur = fifo;
                        if(!cur) {
                            return false;
                        }
                        unsigned long counter = 0;
                        while(cur && !cur->unmap()) {
                            prev = &cur->next;
                            cur = cur->next;
                            ++counter;
                        }
                        if(!cur) {
							/*std::cout << "couldn't unmap any of the " << counter << " currently mapped pages" << std::endl;*/
							/*throw std::string();*/
							return false;
                        }
                        /*std::cerr << "unm-mapped page @" << cur << std::endl;*/
                        *prev = cur->next;
                        cur->next = NULL;
                        return true;
						/*fifo_type::iterator i, j;*/
						/*for(i=fifo.begin(), j=fifo.end();i!=j&&!(*i)->unmap();++i);*/
						/*if(i==j) {*/
							/*std::cout << "couldn't unmap any of the " << fifo.size() << " currently mapped pages" << std::endl;*/
							/*throw std::string();*/
							/*return false;*/
						/*}*/
						/*fifo.erase(i);*/
						/*return true;*/
					}

					void new_page(Page*p) {
						/*fifo.push_back(p);*/

                        //p->next = fifo;
                        //fifo = p; /* technically, it's a stack, and not a FIFO. */

                        (*tail) = p; /* there, it's a real FIFO. */
                        p->next = NULL;
                        tail = &p->next;
					}
					PageCache() : fifo(0)
#ifdef WITH_PARALLEL
                                  , rw_lock()
#endif
                   /*, spin(), mutex(), stats()*/ {
                        tail = &fifo;
						/*stats.set_size(max_page_count);*/
						/*fifo.clear();*/
					}
					PageCache(const PageCache&) {}
					friend class utils::Singleton<PageCache>;
				public:
					/*CacheStats stats;*/
					void* access_page(Page*p) {
#if 0
						sync::scope_lock<sync::default_traits::mutex_type> access_lock(mutex);
						/*std::cerr << "access_page(" << p << ')' << std::endl;*/
						//spin.lock();	/* fast */
						if(p->isMapped()) {
							/*stats.hit();*/
							/*std::cerr << "hit!" << std::endl;*/
							void* ret = **p;
							//spin.unlock();	/* fast */
							return ret;
						}
						/*std::cerr << "miss!" << std::endl;*/
						//mutex.lock();
						//spin.unlock();	/* fast */
						new_page(p);
						/*p->lock_slot();*/	/* prevent race condition in advance */
						/*stats.miss();*/
						void*ret;
						while((!(ret = p->map())) && unmap_one()); 
						
						//mutex.unlock();
						return ret;
#elseif 0
						/*std::cerr << "access_page(" << p << ')' << std::endl;*/
						spin.lock();	/* fast */
						if(p->isMapped()) {
							/*stats.hit();*/
							/*std::cerr << "hit!" << std::endl;*/
							void* ret = **p;
							spin.unlock();	/* fast */
							return ret;
						}
						/*std::cerr << "miss!" << std::endl;*/
						mutex.lock();
						spin.unlock();	/* fast */
						new_page(p);
						/*p->lock_slot();*/	/* prevent race condition in advance */
						/*stats.miss();*/
						void*ret;
						while((!(ret = p->map())) && unmap_one()); 
						
						mutex.unlock();
						return ret;
#else
						{
#ifdef WITH_PARALLEL
							tbb::queuing_rw_mutex::scoped_lock slock(rw_lock, false);
#endif
                            void* ret = **p;
							if(ret) {
								/*stats.hit();*/
								/*std::cerr << "hit!" << std::endl;*/
								return ret;
							}
							/*std::cerr << "miss!" << std::endl;*/
#ifdef WITH_PARALLEL
							slock.upgrade_to_writer();
                            /* wow ! recheck for mmapped region here because of a devious race condition :
                             * - reader A and B want to access page P at the same time
                             *   the reader lock allows them to check concurrently
                             *   the mapping doesn't exist, they both want to upgrade to writer
                             *   now the context is thread-safe, but BOTH A and B want to mmap the page => FAIL.
                             */
                            ret = **p;
                            if(ret) {
                                return ret;
                            }
#endif
                            /*std::cerr << "REQUESTED PAGE DATA FROM HANDLE @" << p << " buffer=" << (**p) << std::endl;*/
							/*p->lock_slot();*/	/* prevent race condition in advance */
							/*stats.miss();*/
                            /* try to m-map a page; if it fails, keep trying as long as another page could be unm-mapped */
							while((!(ret = p->map())) && unmap_one()); 
							if(ret) {
								new_page(p);
                                /*std::cerr << "GOT IT @" << ret << std::endl;*/
							}
							return ret;
						}
#endif
					}
					void remove_page(Page*p) {
#ifdef WITH_PARALLEL
						tbb::queuing_rw_mutex::scoped_lock slock(rw_lock, true);
#endif
						/*mutex.lock();*/
#if 0
						fifo_type::iterator i, j, k=fifo.end();
						for(i=fifo.begin(), j=fifo.end();i!=j;++i) {
							if(*i==p) {
								k=i;
							}
						}
						if(k!=j) {
							fifo.erase(k);
						}
#endif
                        Page ** prev, * cur;
                        prev = &fifo;
                        cur = fifo;
                        while(cur && cur!=p) {
                            prev = &cur->next;
                            cur = cur->next;
                        }
                        if(cur) {
                            *prev = cur->next;
                        }
						/*mutex.unlock();*/
					}
                    void remove_pages_by_handle(handle_type h) {
                        Page ** prev, * cur;
                        prev = &fifo;
                        cur = fifo;
                        while(cur) {
                            while(cur && cur->fd!=h) {
                                /*std::cerr << "  skipping page mapping fd(" << cur->fd << ")+" << cur->offset << " @" << cur->buffer << std::endl;*/
                                prev = &cur->next;
                                cur = cur->next;
                            }
                            if(cur) {
                                /*std::cerr << "* removing page mapping fd(" << cur->fd << ")+" << cur->offset << " @" << cur->buffer << std::endl;*/
                                cur->unmap();
                                /* *prev = cur->next; */
                                cur = cur->next;
                                *prev = cur;
                                if(!cur) {
                                    /* update tail ! */
                                    tail = &fifo;
                                }
                            }
                        }
                    }
			};
		}
	}
}

#endif

