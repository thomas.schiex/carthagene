#include <iostream>
#include <iomanip>
#include <vector>

#include <hash_map>

#define WITH_PARALLEL

#include "../parallel/parallel.h"
#include <semaphore.h>

#if 1
#define SYNCHRONIZATION_TRAITS tbb_sync
struct tbb_sync {
#if TBB_INTERFACE_VERSION >= 5000
	typedef tbb::mutex mutex_type;
	typedef tbb::spin_rw_mutex_v3 rw_spin_type;
	typedef tbb::spin_mutex spin_type;
	typedef tbb::interface5::condition_variable condvar_type;
	typedef tbb::interface5::unique_lock<tbb::mutex> unique_lock_type;
#else
/*#	warning Using old TBB ; not everything is available*/
	class mutex_type {
		public:
			typedef pthread_mutex_t handle_type;
			mutex_type() { pthread_mutex_init(&handle, NULL); }
			void lock() { pthread_mutex_lock(&handle); }
			void unlock() { pthread_mutex_unlock(&handle); }
			handle_type* native_handle() { return &handle; }
		private:
			handle_type handle;
	};
	class rw_spin_type {
		public:
			typedef void handle_type;
			void lock();
			void lock_read();
			void unlock();
	};
	class spin_type {
		public:
			typedef pthread_spinlock_t handle_type;
			spin_type() { pthread_spin_init(&handle, 0); }
			void lock() { pthread_spin_lock(&handle); }
			void unlock() { pthread_spin_unlock(&handle); }
		private:
			handle_type handle;
	};
	typedef mutex_type unique_lock_type;
	class condvar_type {
		public:
			typedef pthread_cond_t handle_type;
			condvar_type() { pthread_cond_init(&handle, NULL); }
			void notify_one() { pthread_cond_signal(&handle); }
			void notify_all() { pthread_cond_broadcast(&handle); }
			void wait(unique_lock_type&ul) { pthread_cond_wait(&handle, ul.native_handle()); }
		private:
			handle_type handle;
	};
#endif
};
#endif
#include "matrix.h"

using namespace matrix;
using namespace data;

/*struct no_sync {*/
	/*class mutex_type {*/
		/*public:*/
			/*mutex_type() {}*/
			/*~mutex_type() {}*/
			/*void lock() {}*/
			/*void unlock() {}*/
	/*};*/
	/*typedef Parallel::CondVar condvar_type;*/
/*};*/

struct spin_sync {
	class mutex_type {
		private:
			pthread_spinlock_t sl;
		public:
			mutex_type() { pthread_spin_init(&sl, 0); }
			~mutex_type() { pthread_spin_destroy(&sl); }
			void lock() { pthread_spin_lock(&sl); }
			void unlock() { pthread_spin_unlock(&sl); }
	};
	typedef Parallel::CondVar condvar_type;
};

struct sem_sync {
	class mutex_type {
		private:
			sem_t sl;
		public:
			mutex_type() { sem_init(&sl, 0, 1); }
			~mutex_type() { sem_destroy(&sl); }
			void lock() { sem_wait(&sl); }
			void unlock() { sem_post(&sl); }
	};
	typedef Parallel::CondVar condvar_type;
};

struct mutex_sync {
	typedef Parallel::Mutex mutex_type;
	typedef Parallel::CondVar condvar_type;
};

struct test_const {
	static const int i=42;
};

template<int i> class hop { public: void foo() { std::cout<<i<<std::endl; } };

template<typename toto> class pouet : public hop<toto::i> {};

#define REPEAT 1LL
#define ROWS 20000LL
#define COLS 20000LL

#ifndef CAN_SWITCH_BACKEND_AT_RUNTIME
#define MY_BACKEND backend::Buffer<Int, SYNC>, 
#else
#define MY_BACKEND
#endif

template<typename SYNC, bool use_locking>
double test_matrix_op(const char*label) {
	Matrix<Int, MY_BACKEND Rectangular<>, SYNC, use_locking> matrix(NULL, ROWS, COLS);
	Utils::Tick t0 = Utils::Tick::now();
	for(int repeat=0;repeat<REPEAT;++repeat) {
		for(int i=0;i<ROWS;++i) {
			for(int j=0;j<COLS;++j) {
				matrix[i][j]=i*100+j+1;
			}
		}
	}
	double t = (Utils::Tick::now()-t0).seconds();
	std::cout << std::setw(14) << label << ' ' << std::setw(15) << (ROWS*COLS*REPEAT/t/1000000.) << " Mset.s-¹, " << t << std::endl;
	return t;
}

void sink(int t) {}

extern double waste_avg;

double calibrate() {
	std::cerr << "calibrating..." << std::endl;
	Utils::ProgressDisplay prog_set(ROWS);
	Utils::Tick t0 = Utils::Tick::now();
	for(int i=0;i<ROWS;++i) {
		Utils::Tick t = Utils::Tick::now();
		for(int j=0;j<COLS;++j) {
			sink(0);
		}
		prog_set.step_done((Utils::Tick::now()-t).seconds());
	}
	return (Utils::Tick::now()-t0).seconds();
}

double waste_avg=calibrate();

double waste(int N) {
	return waste_avg*N*(1./(ROWS*COLS));
}

template <typename M>
double generic_test(M&matrix, const char* label) {
	Utils::ProgressDisplay prog_set(ROWS);
	Utils::Tick t0 = Utils::Tick::now();
	for(int i=0;i<ROWS;++i) {
		Utils::Tick t = Utils::Tick::now();
		for(int j=0;j<COLS;++j) {
			sink(matrix[i][j] = i*100+j+1);
		}
		prog_set.step_done((Utils::Tick::now()-t).seconds());
	}
	double t = (Utils::Tick::now()-t0).seconds()-waste(ROWS*COLS);
	std::cout << std::setw(14) << label << ' ' << std::setw(15) << (ROWS*COLS/t/1000000.) << " Mset.s-¹, " << t << std::endl;
	int toto = 0;
	t0 = Utils::Tick::now();
	Utils::ProgressDisplay prog_lget(ROWS);
	for(int i=0;i<ROWS;++i) {
		Utils::Tick t = Utils::Tick::now();
		for(int j=0;j<COLS;++j) {
			sink(matrix[i][j]);
		}
		prog_lget.step_done((Utils::Tick::now()-t).seconds());
	}
	sink(toto);
	t = (Utils::Tick::now()-t0).seconds()-waste(ROWS*COLS);
	std::cout << std::setw(14) << label << ' ' << std::setw(15) << (ROWS*COLS/t/1000000.) << " Mget.s-¹ (linear), " << t << std::endl;
	matrix::utils::Singleton<matrix::backend::CacheStats>::instance()->reset();
	/*matrix::backend::impl::PageCache::stats.reset();*/
	Utils::ProgressDisplay prog_get(1000);
	t0 = Utils::Tick::now();
	srandom(0xbadc0de);
	int R = 1000000/COLS;
	for(int i=0;i<R;++i) {
		Utils::Tick t = Utils::Tick::now();
		for(int k=0;k<COLS;++k) {
			sink(matrix[random()%ROWS][random()%COLS]);
		}
		/*toto ^= matrix[random()%ROWS][random()%COLS];*/
		prog_get.step_done((Utils::Tick::now()-t).seconds());
	}
	sink(toto);
	/*std::cerr << "final toto = " << toto << std::endl;*/
	t = (Utils::Tick::now()-t0).seconds()-waste(R*COLS);
	std::cout << std::setw(14) << label << ' ' << std::setw(15) << (.000001*R*COLS/t) << " Mget.s-¹ (random), " << t << std::endl;
	return t;
}

template<typename SYNC, bool use_locking>
double test_matrix_set(const char*label) {
	Matrix<Int, MY_BACKEND Rectangular<>, SYNC, use_locking> matrix(NULL, ROWS, COLS);
	Utils::Tick t0 = Utils::Tick::now();
	for(int repeat=0;repeat<REPEAT;++repeat) {
		for(int i=0;i<ROWS;++i) {
			for(int j=0;j<COLS;++j) {
				matrix.set(i, j, i*100+j+1);
			}
		}
	}
	double t = (Utils::Tick::now()-t0).seconds();
	std::cout << std::setw(14) << label << ' ' << std::setw(15) << (ROWS*COLS*REPEAT/t/1000000.) << " Mset.s-¹, " << t << std::endl;
	return t;
}

template<typename SYNC, bool use_locking>
double test_matrix_set_get(const char*label) {
	Matrix<Int, MY_BACKEND Rectangular<>, SYNC, use_locking> matrix(NULL, ROWS, COLS);
	double t = generic_test(matrix, label);
	return t;
}

double test_array_array(const char*label) {
	/*Matrix< Int, SYNC, Rectangular<> > matrix(NULL, ROWS, COLS);*/
	int** matrix = new int*[ROWS];
	for(int i=0;i<ROWS;++i) {
		matrix[i]=new int[COLS];
	}
	double t = generic_test(matrix, label);
	for(int i=0;i<ROWS;++i) {
		delete[] matrix[i];
	}
	delete[] matrix;
	return t;
}

double test_array_set(const char*label) {
	/*Matrix< Int, SYNC, Rectangular<> > matrix(NULL, ROWS, COLS);*/
	int* matrix = new int[ROWS*COLS];
	Utils::Tick t0 = Utils::Tick::now();
	for(int repeat=0;repeat<REPEAT;++repeat) {
		for(int i=0;i<ROWS;++i) {
			for(int j=0;j<COLS;++j) {
				matrix[i*COLS+j]=i*100+j+1;
			}
		}
	}
	double t = (Utils::Tick::now()-t0).seconds();
	std::cout << std::setw(14) << label << ' ' << std::setw(15) << (ROWS*COLS*REPEAT/t/1000000.) << " Mset.s-¹, " << t << std::endl;
	delete[] matrix;
	return t;
}


class scope_test {
	public:
		scope_test() { std::cout << "scope_test ctor" << std::endl; }
		~scope_test() { std::cout << "scope_test dtor" << std::endl; }
};


#if 0
template <bool L>
	void test_all_syncs_op() {
		std::string _;
		test_matrix_op<matrix::sync::dummy, L>((_+"[] no_sync"+(L?"+l":"")).c_str());
		test_matrix_op<spin_sync, L>((_+"[] spin"+(L?"+l":"")).c_str());
		test_matrix_op<sem_sync, L>((_+"[] sem"+(L?"+l":"")).c_str());
		test_matrix_op<mutex_sync, L>((_+"[] mutex"+(L?"+l":"")).c_str());
	}

template <bool L>
	void test_all_syncs_set() {
		std::string _;
		test_matrix_set<matrix::sync::dummy, L>((_+"set no_sync"+(L?"+l":"")).c_str());
		test_matrix_set<spin_sync, L>((_+"set spin"+(L?"+l":"")).c_str());
		test_matrix_set<sem_sync, L>((_+"set sem"+(L?"+l":"")).c_str());
		test_matrix_set<mutex_sync, L>((_+"set mutex"+(L?"+l":"")).c_str());
	}
#endif

int main(int argc, char**argv) {
	std::cout << "sizeof mutex = " << sizeof(pthread_mutex_t) << std::endl;
	std::cout << "sizeof spin = " << sizeof(pthread_spinlock_t) << std::endl;
	std::cout << "sizeof condvar = " << sizeof(pthread_cond_t) << std::endl;
	try {
		int* fail = new int[1LL<<35];
		sink((int)(long long)fail);
	} catch(const std::bad_alloc& e) {
		std::cerr << "caught std::bad_alloc " << e.what() << std::endl;
	}
	std::cout << std::setprecision(2) << std::fixed;
	/*scope_test k;*/
	/*pouet<test_const> p;*/
	/*p.foo();*/
	/*std::cout << "sizeof(test_const)="<<sizeof(test_const)<<std::endl;*/
	test_array_array("bare [][]");
	/*test_array_set("bare []");*/
	/*test_matrix_set_get<mutex_sync, false>("matrix none");*/
	/*test_matrix_set_get<matrix::sync::dummy, true>("matrix mutex+l");*/
	/*std::cout << matrix::backend::impl::PageCache::stats;*/
	test_matrix_set_get<matrix::sync::dummy, true>("matrix no_sync+l");


#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
	/*Matrix<Int, Rectangular<>, matrix::sync::dummy, false> matrix(NULL, ROWS, COLS);*/
	/*if(!matrix.switch_backend("hashcache", true)) {*/
		/*std::cout << "couldn't switch to hashcache backend for immed<int> matrix !" << std::endl;*/
	/*}*/
#endif

	/*for(int i=0;i<ROWS;++i) {*/
		/*for(int j=0;j<COLS;++j) {*/
			/*std::cout << matrix[i][j] << ' ';*/
		/*}*/
		/*std::cout << std::endl;*/
	/*}*/

	if(check_not_void<void>::value) {
		std::cout << "NOT STRIPPED" << std::endl;
	}

	std::cout << *matrix::utils::Singleton<matrix::backend::CacheStats>::instance();

	return 0;
}

