#ifndef _MATRIX_BACKEND_HASHCACHE_H_
#define _MATRIX_BACKEND_HASHCACHE_H_

#include "backend_base.h"

namespace matrix {
	namespace backend {
		using addressing_space::offset_type;


		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			class HashCache BACKEND_INHERITANCE {
				private:
					void* owner;
					struct Key {
						void* M;
						offset_type offset;
						Key() : M(0), offset(0) {}
					};
					struct CacheData {
						int lock;	/* prevent overwrite while >0 */
						Key key;
						typename DATA_TRAITS::data_type data;
						CacheData() : lock(0), key(), data() {}
					};

					std::vector<CacheData> cache;

					size_t hash(void* M, offset_type offset) {
						/* FIXME devise a REAL hash function */
#						define HASH_PRIME 0xB570F5F
						offset ^= ((offset_type)M)*HASH_PRIME;
						offset%=cache.size();
						/*std::cout << "hashed to " << offset << std::endl;*/
						return offset;
					}
					CacheStats& hashcache_stats;
				public:
					typedef partial_storage storage_type;
					typedef typename DATA_TRAITS::data_type data_type;


					HashCache(void*_)
#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
						: Generic<DATA_TRAITS, SYNC_TRAITS>(), owner(_)
						, hashcache_stats(*utils::Singleton<CacheStats>::instance())
#else
						: owner(_)
#endif
					{
						/*if(!hashcache_stats.size()) { setSize(19); }*/
						setSize(19);
					}
					~HashCache() {}
					void init(offset_type size) {}
					void set(int i, int j, data_type*d) {}
					void setSize(unsigned int exp2) {
						offset_type size;
						if(exp2<=0||exp2>=32) {
							/* FIXME should whine */
							return;
						}
						size = (1<<exp2)-1;
						cache.resize(size);
						hashcache_stats.set_size(size);
					}
					typename DATA_TRAITS::data_type* raw_get(offset_type o) {
						size_t h = hash(owner, o);
						CacheData* cd = &cache[h];
						if((cd->key.M==owner)&&(cd->key.offset==o)) {
							hashcache_stats.hit();
						} else {
							(void)(cd->key.M?hashcache_stats.miss():hashcache_stats.new_value());
							if(cd->key.M) {
								hashcache_stats.miss();
							} else {
								hashcache_stats.inuse();
							}
							cd->key.M=owner;
							cd->key.offset=o;
							DATA_TRAITS::clear_data(cd->data);
						}
						return &cd->data;
					}
					void lock_data(offset_type o) {}
					void unlock_data(offset_type o) {}
			};
	}
}

#endif

