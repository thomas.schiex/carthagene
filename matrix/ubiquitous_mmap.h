#ifndef _UBIQUITOUS_MMAP_H_
#define _UBIQUITOUS_MMAP_H_

#ifndef _WIN32

#include <sys/mman.h>

typedef int handle_type;

#else

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <malloc.h>
#include <io.h>

#define PROT_READ   0x0001
#define PROT_WRITE  0x0002
#define PROT_EXEC   0x0004
#define PROT_NONE   0x0008

#define MAP_SHARED  0x0001
#define MAP_PRIVATE 0x0002
#define MAP_FIXED   0x0004

#define MAP_FAILED  ((void *)-1)

#define handle_type HANDLE

void *mmap(void* address,
                  unsigned int size,
                  unsigned int protection,
                  unsigned int flags,
                  handle_type  fd,
                  int          offset);

int munmap(void *addr, unsigned int size);

#define madvise(...) 0
#define msync(...)
#define mkstemp(x) fileno(tmpfile())

#endif

#endif

