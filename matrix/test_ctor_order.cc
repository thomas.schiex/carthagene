#include <iostream>

struct A {
	A() { std::cout << "new A" << std::endl; }
};

struct B {
	B() { std::cout << "new B" << std::endl; }
};

struct C : public B, A {
	C() : B(), A() { std::cout << "new C" << std::endl; }
};

int main(int argc, char**argv) {
	C toto;
	return 0;
}

