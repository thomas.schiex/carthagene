#ifndef _MATRIX_BACKEND_SWAP_LRU_H_
#define _MATRIX_BACKEND_SWAP_LRU_H_

#include "backend_swap_base.h"

namespace matrix {
	namespace backend {
		namespace impl {
			class Page {
				private:
					Page* prev_;
					Page* next_;
					void* buffer;
					CacheFile* f;
					offset_type offset;
					offset_type slots_in_use;
				public:
					void add_prev(Page*n) {
						n->prev_=prev_;
						prev_->next_=n;
						prev_=n;
						n->next_=this;
						/*next = next==this ? n : next;*/
					}
					void add_next(Page*n) {
						n->next_=next_;
						next_->prev_=n;
						next_=n;
						n->prev_=this;
						/*prev = prev==this ? n : prev;*/
					}
					void remove() {
						prev_->next_=next_;
						next_->prev_=prev_;
					}
					void list_init(Page*force) { prev_=next_=force; }
					Page* prev() const { return prev_; }
					Page* next() const { return next_; }
					/*sync_traits::mutex_type mutex;*/
					Page()
					  : prev_(this), next_(this), buffer(0), f(0), offset(0), slots_in_use(0)
					  /*, mutex()*/
					{}
					bool can_unmap() const { return !slots_in_use; }
					bool isMapped() const { return !!buffer; }
					bool isSwapped() const { return !buffer; }
					void init(CacheFile*f_, offset_type o) {
						f=f_;
						offset=o;
						slots_in_use=0;
					}
					void* operator *() const { return buffer; }
					bool unmap() {
						/*mutex.lock();*/
						if(slots_in_use) {
							std::cerr << "[unmap] " << this << ' ' << buffer << " +" << offset << " forbidden (lock=" << slots_in_use << ')' << std::endl;
							return false;
						}
						msync(buffer, pagesize, MS_SYNC);
						munmap(buffer, pagesize);
						/*std::cerr << "[unmap] " << this << ' ' << buffer << " +" << offset << std::endl;*/
						buffer=NULL;
						/*mutex.unlock();*/
						return true;
					}
					void lock_slot() { ++slots_in_use; /*std::cerr << "lock slot("<<slots_in_use<<')'<<std::endl;*/ }	/* FIXME : all that need sync */
					void unlock_slot() { --slots_in_use; /*std::cerr << "unlock slot("<<slots_in_use<<')'<<std::endl;*/ }
					void* map() {
						/*mutex.lock();*/
						buffer = f->map_page(offset);
						/*std::cerr << "[  map] " << buffer << " +" << offset << std::endl;*/
						/*mutex.unlock();*/
						return buffer;
					}
					~Page() {
						/*if(buffer) {*/
							/*unmap();*/
						/*}*/
					}
			};

			class PageCache {
				private:
					//static Page* mru;	/* list head */
					//static Page* lru;	/* list tail */
					Page pages;
					Page dummy_page;
					offset_type count;
					sync::default_traits::spin_type mutex;
					PageCache() : pages(), dummy_page(), count(0), mutex(), stats() {
						stats.set_size(max_page_count);
						dummy_page.list_init(&pages);
						pages.list_init(&dummy_page);
						count=1;
					}
					PageCache(const PageCache&) {}
					friend class utils::Singleton<PageCache>;
				public:
					CacheStats stats;
					void* access_page(Page*p) {
						mutex.lock();
						if(p==pages.next()) {
							stats.hit();
							mutex.unlock();
							return **p;
						}
						else if(p->isMapped()) {
							p->remove();
							pages.add_next(p);
							stats.hit();
							mutex.unlock();
							return **p;
						} else if(count>=max_page_count) {
							Page* tmp = pages.prev();
							do {
								while(	( !tmp->unmap() )	/* page is still in use, try next one */
											&&
										( (tmp=tmp->prev()) != &pages )
									);	/* exception to the count limit : won't fail if all pages are locked. */
								if(tmp!=&pages) {
									tmp->remove();
									--count;
								}
							} while( (tmp!=&pages) && (count>=max_page_count) );
							stats.miss();		/* new value, but doesn't increase population */
						} else {
							stats.new_value();	/* also a miss */
						}
						++count;
						pages.add_next(p);
						mutex.unlock();
						return p->map();
					}
					void remove_page(Page*p) {
						p->unmap();
						/*p->remove(mru, lru);*/
						p->remove();
						--count;
						if(pages.next()==&pages && pages.prev()==&pages) {
							std::cerr << "re-insert dummy page into page cache (count=" << count << ')' << std::endl;
							pages.list_init(&dummy_page);
							dummy_page.list_init(&pages);
							count=1;
						}
					}
			};

		}

	}
}

#endif

