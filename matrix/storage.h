#ifndef _MATRIX_STORAGE_H_
#define _MATRIX_STORAGE_H_

//#include "cache_file.h"
#include "base.h"
#include "addressing_space.h"
#include "check_drive.h"
//#include "pagecache.h"

#include <boost/iostreams/device/mapped_file.hpp>   /* gaaaaaahhh boost::nightmare ! */
#include <iostream> /* std::ios_base */

extern "C" {
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
}

#ifdef WIN32
#   define SEP "\\"
#else
#   define SEP "/"
#endif

namespace matrix {
	namespace storage {
		namespace impl {
			using addressing_space::offset_type;

			struct storage_init_failure {
				virtual ~storage_init_failure() {}
			};

			struct storage_requires_explicit_filename : public storage_init_failure {};

			/** Direct memory buffer */
			template <typename DATA_TRAITS>
				struct buffer {
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename DATA_TRAITS::context_type context_type;
					data_type* data;
					buffer(const context_type c) : data(0) {}
					virtual ~buffer() {
						/*std::cerr << "Destroying buffer instance" << std::endl;*/
						if(data) { delete[] data; }
					}
					void init(offset_type size) {
						try {
							if(!(data = new data_type[size])) {
								throw storage_init_failure();
							}
						} catch(const std::bad_alloc& sba) {
							throw storage_init_failure();
						}
						memset(data, 0, size*sizeof(data_type));
					}
					data_type read(offset_type ofs) { return data[ofs]; }
					void write(offset_type ofs, data_type d) { data[ofs] = d; }
				};

			

			/** Helper template to initialize the filename in file-based storages */
			template<typename DATA_TRAITS, bool have_provider>
				struct init_file_name_impl;

			/** Helper template to initialize the filename in file-based storages : have_filename_provider specialization */
			template<typename DATA_TRAITS>
				struct init_file_name_impl<DATA_TRAITS, true> {
					init_file_name_impl(typename DATA_TRAITS::context_type owner, std::string& filename) {
						filename = typename DATA_TRAITS::pagefile_name_provider_type(owner);
					}
				};

			/** Helper template to initialize the filename in file-based storages : dont_have_filename_provider specialization */
			template<typename DATA_TRAITS>
				struct init_file_name_impl<DATA_TRAITS, false> {
					init_file_name_impl(typename DATA_TRAITS::context_type owner, std::string& filename) {}
				};

			/** Helper template to initialize the filename in file-based storages : facade */
			template<typename DATA_TRAITS>
				struct init_file_name : public init_file_name_impl<DATA_TRAITS, constraints::check_not_void<typename DATA_TRAITS::pagefile_name_provider_type>::value> {
					typedef init_file_name_impl<DATA_TRAITS, constraints::check_not_void<typename DATA_TRAITS::pagefile_name_provider_type>::value> naming_scheme;
					init_file_name(typename DATA_TRAITS::context_type owner, std::string& filename)
						: naming_scheme(owner, filename)
					{}
				};
/*#ifndef WIN32*/

            struct file {
                std::string filename;
                file() : filename() {}
                static std::string& cache_path() {
                    static std::string path;
                    return path;
                }

                offset_type filesize() const {
                    struct stat st;
                    if(stat(filename.c_str(), &st)) {
                        /* FIXME: whine */
                        return 0;
                    }
                    return st.st_size;
                }

                void check_before_open(offset_type size) {
                    offset_type needed_size;
                    if(size >= filesize()) {
                        needed_size = size - filesize();
                    } else {
                        needed_size = 0;
                    }
                    /*std::cout << "check_before_open size=" << size << " filesize()=" << filesize() << " needed_size=" << needed_size << std::endl;*/
                    if(!check::disk::is_local(filename)) {
                        std::cout
                            << "WARNING: the path " << filename << " belongs to a remote (network) drive."
                            << std::endl
                            << "The overall performance will be severely impacted by the network speed."
                            << std::endl
                            << "You should consider setting the directory for cache files to a local drive using the command cg_set2ptcachepath."
                            << std::endl << std::endl;
                    }
                    if(!check::disk::is_free_space_big_enough(filename, (double) needed_size)) {
                        std::stringstream ss;
                        ss << "Not enough free space (need ";
                        if(size>(1024*1024*1024)) {
                            ss << ((offset_type) (.5 + size/(1024*1024*1024))) << " free gigabytes)";
                        } else if(size>1048576) {
                            ss << ((offset_type) (.5 + size/1048576)) << " free megabytes)";
                        } else if(size>1024) {
                            ss << ((offset_type) (.5 + size/1024)) << " free kilobytes)";
                        } else {
                            ss << ((offset_type) size) << " free bytes)";
                        }
                        throw ss.str();
                    }
                }

                const char* make_path(const char* fname, offset_type size) {
                    if(cache_path() == "") {
                        filename = fname;
                        check_before_open(size);
                        return fname;
                    }
                    std::string f = fname;
                    std::replace(f.begin(), f.end(), '/', '!');
                    std::replace(f.begin(), f.end(), '~', '-');
                    std::replace(f.begin(), f.end(), '\\', '!');
                    std::replace(f.begin(), f.end(), ':', '_');
                    filename = cache_path() + SEP + f;
                    check_before_open(size);
                    return filename.c_str();
                }
            };

			/** Base class for both file storage mechanisms */
			template <typename DATA_TRAITS>
				struct file_base : public file {
					typedef typename DATA_TRAITS::data_type data_type;
					handle_type fd;
					offset_type size;
					/*sync::default_traits::mutex_type mutex;*/

					file_base(typename DATA_TRAITS::context_type c)
						: fd(), /*filename(),*/ size(0)/*, mutex()*/
					{
#ifndef WIN32
						fd = -1;
#endif
						init_file_name<DATA_TRAITS>(c, filename);
					}

					virtual ~file_base() {
						/*std::cerr << "Destroying file_base instance" << std::endl;*/
					}

					void deinit() {
						this->close();
						if(DATA_TRAITS::delete_pagefile_when_done) {
							/*std::cout << "Destroying file " << filename << std::endl;*/
							unlink(filename.c_str());
						}
					}

#ifndef WIN32
					void open(offset_type size=1) {
						/*mutex.lock();*/
						if(filename.size()) {
																					// measure on a 1M file
							int open_opts = O_CREAT|O_NOATIME|O_RDWR;   			// 973946 
							/*int open_opts = O_CREAT|O_NOATIME|O_RDWR|O_NONBLOCK;   	// */
							/*int open_opts = O_CREAT|O_NOATIME|O_RDWR|O_DIRECT;   	// 992457 but slower on bigger file */
							/*int open_opts = O_CREAT|O_NOATIME|O_RDWR|O_ASYNC;   	// 966625*/
							fd = ::open(make_path(filename.c_str(), size),
                                        open_opts, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
						} else {
							char path[PATH_MAX];
							strcpy(path, make_path("pagefile_XXXXXX", size));
							fd = mkstemp(path);
							filename=path;
						}
                        if(fd == -1) {
							std::cerr << "Couldn't open cache file " << filename.c_str() << " because of error " << strerror(errno) << std::endl;
							throw std::string("Can't write cache file");
						/*} else {*/
                            /*std::cerr << "cache file fd = " << fd << std::endl;*/
                        }
						if(size&&!filesize()) {
							lseek64(fd, size-1, SEEK_SET);
							char c=0;
							if(::write(fd, &c, 1)!=1) {
								/* whine */
							}
						}
                        std::cout << "opened cache file " << filename << " as fd " << fd << std::endl;
						/*mutex.unlock();*/
					}

					void close() {
						if(fd!=-1) {
							/*mutex.lock();*/
							::close(fd);
							/*mutex.unlock();*/
						}
					}
#else
                    void make_tempname(std::string& out) {
                        TCHAR szTempFileName[MAX_PATH];  
                        TCHAR lpTempPathBuffer[MAX_PATH];
                        GetTempPath(MAX_PATH, lpTempPathBuffer); 
                        GetTempFileName(lpTempPathBuffer, TEXT("MATRIX"), 0, szTempFileName);
                        out = szTempFileName;
                    }

					void open(offset_type size=1) {
						/*mutex.lock();*/
						if(!filename.size()) {
							make_tempname(filename);
						}

                        fd = CreateFile(make_path(filename.c_str(), size),
                                GENERIC_READ | GENERIC_WRITE,
                                FILE_SHARE_READ | FILE_SHARE_WRITE,
                                NULL,
                                OPEN_ALWAYS,
                                FILE_ATTRIBUTE_NORMAL,
                                NULL);
                        if(fd == INVALID_HANDLE_VALUE)
                        {
                            std::cerr << "File " << filename << " not opened! (error #" << GetLastError() << ')' << std::endl;
                            throw std::string("Can't open swap file");
                        }

                        struct stat st;
                        if(stat(filename.c_str(), &st)) {
                            throw std::string("Can't stat() swap file");
                        }
                        if(st.st_size!=size) {
    						char dummy=0;
                            unsigned long sizeLS = (unsigned long)(size&0xFFFFFFFF);
                            long sizeMS = (unsigned long)(size>>32);
                            SetFilePointer(fd, sizeLS-1, &sizeMS, 0);
	    					/*fseek(fd, size-1, SEEK_SET);*/
                            unsigned long unused;
		    				if(WriteFile(fd, &dummy, 1, &unused, NULL)==-1) {
			    				std::cerr << "Can't write to swap file " << filename << " :( " << strerror(errno) << std::endl;
				    			throw std::string("Can't write swap file");
					    	}
                        }
                        /*Base = (char*)MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, size);*/
						/*mutex.unlock();*/
					}

					void close() {
                        CloseHandle(fd);
					}
#endif
				};




#ifndef WIN32
			/** Direct file access using pread/pwrite */
			template <typename DATA_TRAITS>
				struct direct_file : public file_base<DATA_TRAITS> {
					typedef file_base<DATA_TRAITS> file_type;
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename DATA_TRAITS::context_type context_type;
					using file_type::open;
					using file_type::fd;
					using file_type::deinit;
					/*using file_type::mutex;*/
					template <typename X> void ignore_return_value(X x) {}

					context_type context;

					direct_file(const context_type c)
						: file_base<DATA_TRAITS>(c), context(c)
					{}

					virtual ~direct_file() { deinit(); }

					void init(offset_type size) {
						file_type::open(size*sizeof(data_type));
					}

					 data_type read(offset_type ofs) {
						data_type ret;
						/*mutex.lock();*/
						/*lseek(fd, ofs*sizeof(data_type), SEEK_SET);*/
						/*ignore_return_value(::read(fd, &ret, sizeof(data_type)));*/
						/*mutex.unlock();*/
						ignore_return_value(::pread(fd, &ret, sizeof(data_type), ofs*sizeof(data_type)));
						return ret;
					}

					 void write(offset_type ofs, data_type dat) {
						 /*mutex.lock();*/
						 /*lseek(fd, ofs*sizeof(data_type), SEEK_SET);*/
						 /*discard(::write(fd, &dat, sizeof(data_type)));*/
						 /*mutex.unlock();*/
						 ignore_return_value(::pwrite(fd, &dat, sizeof(data_type), ofs*sizeof(data_type)));
					 }
				};
#endif

			/** Cached file access using mmap/munmap */
			template <typename DATA_TRAITS>
				struct cached_file : public file_base<DATA_TRAITS> {
					std::vector<cache::Page> pages;
					cache::PageCache* pagecache;

					typedef file_base<DATA_TRAITS> file_type;
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename DATA_TRAITS::context_type context_type;
#ifdef WIN32
                    HANDLE hMapFile;
					void open(offset_type size=1) {
						/* align size on page boundary */
						size = (size+pagesize-1)&~((offset_type)(pagesize-1));
						file_type::open(size);
                        hMapFile = CreateFileMapping(fd, NULL, PAGE_READWRITE, 0, size, NULL);
					}
					void close() {
						file_type::close();
                        CloseHandle(hMapFile);
					}
#else
					using file_type::open;
					using file_type::close;
#endif
					using file_type::fd;
					using file_type::deinit;

					cached_file(const context_type c)
						: file_base<DATA_TRAITS>(c), pagecache(utils::Singleton<cache::PageCache>::instance())
					{
						/*std::cerr << "new cached_file()" << std::endl;*/
					}

					virtual ~cached_file() {
						/*std::cerr << "Destroying cached_file instance" << std::endl;*/
						std::vector<cache::Page>::iterator i=pages.begin(), j=pages.end();
						/*while(i!=j) { cache::Page* p = &*i; if(p->isMapped()) { pagecache->remove_page(p); } ++i; }*/
						pagecache->remove_pages_by_handle(fd);
						pages.clear();
						deinit();
					}

					void init(offset_type size) {
						offset_type pagecount = (size*sizeof(data_type)+pagesize-1)/pagesize;  /* fugly, but no better idea */
						size = pagecount*pagesize;

						/*file_type::open(size/sizeof(data_type));*/
						open(size);

						pages.resize(pagecount+1);
						std::cerr << "pagefile has " << pages.size() << " pages in " << size << " bytes." << std::endl;
						std::vector<cache::Page>::iterator
							begin=pages.begin(),
							end=pages.end();
						offset_type ofs=0;
						while(begin!=end) {
#ifdef WIN32
							(*begin).init(hMapFile, ofs);
#else
							(*begin).init(fd, ofs);
#endif
							ofs+=pagesize;
							++begin;
						}
					}
#if 0

#ifndef WIN32
					void* map_page(offset_type offset) {
						void* ret = mmap(NULL, pagesize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, offset);
						if(ret==MAP_FAILED) {
							/*std::cerr << "mmap failed : " << strerror(errno) << std::endl;*/
							/*throw Failure(errno);*/
							return NULL;
						}
						int mret;
						if((mret=madvise(ret, pagesize, MADV_RANDOM))) {
							/*std::cerr << "madvise failed : " << strerror(mret) << std::endl;*/
						}
						return ret;
					}
#else
					void* map_page(offset_type offset) {
                        void* ret = (char*)MapViewOfFile(hMapFile, FILE_MAP_READ|FILE_MAP_WRITE, offset>>32, offset&0xFFFFFFFF, pagesize);
						if(!ret) {
							std::cerr << "mmap failed : " << strerror(errno) << std::endl;
							/*throw Failure(errno);*/
							throw std::string(strerror(errno));
						}
                        return ret;
					}
#endif
#endif

					data_type* __getter(offset_type ofs) {
						ofs *= sizeof(data_type);
						offset_type pagenum = ofs/pagesize;
						offset_type dataofs = ofs%pagesize;
						cache::Page* page = &pages[pagenum];
						return (data_type*)(((char*)pagecache->access_page(page))+dataofs);
					}

					data_type read(offset_type ofs) {
						return *__getter(ofs);
					}

					void write(offset_type ofs, data_type d) {
						*__getter(ofs) = d;
					}
				};
/*#endif*/
			template <typename DATA_TRAITS>
				struct boost_file {
					typedef typename DATA_TRAITS::data_type data_type;
					boost::iostreams::mapped_file mf;
					boost::iostreams::mapped_file_params mfp;
					data_type* data;

					boost_file(typename DATA_TRAITS::context_type c)
						: mf(), mfp()
					{
						init_file_name<DATA_TRAITS>(c, mfp.path);
					}

					virtual ~boost_file() {
						if(mf.is_open()) {
							mf.close();
						}
						if(DATA_TRAITS::delete_pagefile_when_done) {
							/*std::cout << "Destroying file " << mfp.path << std::endl;*/
							unlink(mfp.path.c_str());
						}
					}

					offset_type filesize() const {
						struct stat st;
						if(stat(mfp.path.c_str(), &st)) {
							/* whine */
							return 0;
						}
						return st.st_size;
					}

					void init(offset_type size) {
						mfp.mode = std::ios_base::in | std::ios_base::out;
						size *= sizeof(data_type);
						if(!mfp.path.size()) {
							throw storage_requires_explicit_filename();
						}
						if(size&&size!=filesize()) {
							int fd = ::open(mfp.path.c_str(), O_TRUNC|O_WRONLY|O_CREAT, S_IRWXU);
							if(fd==-1) {
								std::cout << "error creating cache file " << mfp.path << " : " << strerror(errno) << std::endl;
							}
							int c=0;
							::lseek64(fd, size-1, SEEK_SET);
							if(::write(fd, &c, 1)!=1) {
								/* whine */
							}
							::close(fd);
							/*std::cout << "Reinitialized cache file " << mfp.path << " to " << (filesize()*(1/(1024.*1024.*1024.))) << " GB." << std::endl;*/
						} else {
							/*std::cout << "Reusing cache file " << mfp.path << " of " << (filesize()*(1/(1024.*1024.*1024.))) << " GB." << std::endl;*/
						}
						/*mfp.new_file_size = filesize()!=size ? size : 0;*/
						/*mfp.new_file_size = 0;*/
						/*mfp.length = 0;*/
						mf.open(mfp);
						if(mfp.new_file_size) {
							/*std::cerr << "initializing file " << mfp.path << " with 0's" << std::endl;*/
							/* file has been created/overwritten, initialize with 0's */
							/*for(offset_type i=0;i<size;++i) {*/
								/*mf.data()[i] = 0;*/
							/*}*/
						}
						data = (data_type*) mf.data();
					}

					void close() {
						if(mf.is_open()) {
							mf.close();
						}
					}

					data_type read(offset_type ofs) {
						/*return *(data_type*)&(mf.data()[ofs*sizeof(data_type)]);*/
						return data[ofs];
					}

					void write(offset_type ofs, data_type d) {
						/**(data_type*)(&mf.data()[ofs*sizeof(data_type)]) = d;*/
						data[ofs] = d;
					}
				};

			template <typename DATA_TRAITS>
				struct sliced_boost_file {
					typedef typename DATA_TRAITS::data_type data_type;
					std::vector<boost::iostreams::mapped_file> mf;
					std::string path;
					std::vector<char*> data;

					sliced_boost_file(typename DATA_TRAITS::context_type c)
						: mf(), data()
					{
						init_file_name<DATA_TRAITS>(c, path);
					}

					virtual ~sliced_boost_file() {
						for(unsigned int i=0;i<data.size();++i) {
							if(mf[i].is_open()) {
								mf[i].close();
							}
						}
						if(DATA_TRAITS::delete_pagefile_when_done) {
							for(unsigned int i=0;i<data.size();++i) {
								const char* tmp_path = slice_path(i);
								/*std::cout << "Destroying file " << tmp_path << std::endl;*/
								unlink(tmp_path);
							}
						}
					}

					const char* slice_path(int i) const {
						static char tmp_path[PATH_MAX];
						sprintf(tmp_path, "%s.%i", path.c_str(), i);
						return tmp_path;
					}

					offset_type filesize() const {
						struct stat st;
						offset_type ret=0;
						for(unsigned int i=0;i<data.size();++i) {
							if(stat(slice_path(i), &st)) {
								/* whine */
								return 0;
							}
							ret += st.st_size;
						}
						return ret;
					}

					void slice_init(int i, offset_type size) {
						const char* path = slice_path(i);
						int fd = ::open(path, O_TRUNC|O_WRONLY|O_CREAT, S_IRWXU);
						if(fd==-1) {
							std::cout << "error creating cache file " << path << " : " << strerror(errno) << std::endl;
						}
						int c=0;
						::lseek64(fd, size-1, SEEK_SET);
						if(::write(fd, &c, 1)!=1) {
							/* whine */
						}
						::close(fd);
						/*std::cout << "Initialized cache slice " << path << " to " << (size*(1/(1024.*1024.*1024.))) << " GB." << std::endl;*/
					}

#define SLICE_BITS 30
#define SLICE_MASK ((1<<SLICE_BITS)-1)
					void init(offset_type size) {
						size *= sizeof(data_type);
						if(!path.size()) {
							throw storage_requires_explicit_filename();
						}
						offset_type n_slices = 1+(size >> SLICE_BITS);
						data.reserve(n_slices);
						data.insert(data.begin(), n_slices, 0);
						if(size&&size!=filesize()) {
							unsigned int i;
							for(i=0;i<n_slices-1;++i) {
								slice_init(i, 1<<SLICE_BITS);
							}
							slice_init(i, sizeof(data_type)+(size&SLICE_MASK));
							/*std::cout << "Initialized " << (filesize()*(1/(1024.*1024.*1024.))) << " GB of disk cache in " << n_slices << " slices." << std::endl;*/
						} else {
							/*std::cout << "Reusing " << (filesize()*(1/(1024.*1024.*1024.))) << " GB of disk cache in " << n_slices << " slices." << std::endl;*/
						}
						for(unsigned int i=0;i<n_slices;++i) {
							boost::iostreams::mapped_file_params mfp;
							mfp.path = slice_path(i);
							mfp.mode = std::ios_base::in | std::ios_base::out;
							//mf.open(mfp);
							mf.insert(mf.end(), boost::iostreams::mapped_file());
							mf.back().open(mfp);
							data[i] = mf.back().data();
						}
					}

					void close() {
						for(unsigned int i=0;i<data.size();++i) {
							if(mf[i].is_open()) {
								mf[i].close();
							}
						}
					}

					data_type read(offset_type ofs) {
						/*return *(data_type*)&(mf.data()[ofs*sizeof(data_type)]);*/
						offset_type segofs = ofs*sizeof(data_type);
						offset_type ss = segofs>>SLICE_BITS;
						offset_type so = segofs&SLICE_MASK;
						return *(data_type*)(data[ss]+so);
					}

					void write(offset_type ofs, data_type d) {
						/**(data_type*)(&mf.data()[ofs*sizeof(data_type)]) = d;*/
						offset_type segofs = ofs*sizeof(data_type);
						*(data_type*)(data[segofs>>SLICE_BITS]+(segofs&SLICE_MASK)) = d;
					}
				};


			/* Slight abstraction to use either storage type A or B, depending on whether A::init fails or not. Useful to select automatically between buffer and disk storage. A::init MAY fail, B::init MUST NOT fail. */
			template <typename DATA_TRAITS, typename A, typename B>
				struct adaptive_storage : public A, public B {
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename DATA_TRAITS::context_type context_type;
					enum selection { use_A, use_B };
					selection in_use;
					adaptive_storage(const context_type c) : A(c), B(c), in_use() {}
					virtual ~adaptive_storage() {
						/*std::cerr << "Destroying adaptive_storage instance" << std::endl;*/
					}
					void init(offset_type size) {
						try {
							A::init(size);
							in_use = use_A;
							/*std::cout << "adaptive_storage : using " << typeid(A).name() << std::endl;*/
						} catch(const storage_init_failure sif) {
							B::init(size);
							in_use = use_B;
							/*std::cout << "adaptive_storage : using " << typeid(B).name() << std::endl;*/
						}
					}

					data_type read(offset_type ofs) {
						switch(in_use) {
							case use_A: return A::read(ofs);
							case use_B: return B::read(ofs);
						};
					}

					void write(offset_type ofs, data_type d) {
						switch(in_use) {
							case use_A: A::write(ofs, d); break;
							case use_B: B::write(ofs, d); break;
						};
					}
				};
		}

		/* Publish storage backends in a non-template way */

		struct buffer {
			template <typename DATA_TRAITS>
				struct storage_type {
					typedef impl::buffer<DATA_TRAITS> impl; /* written this way to avoid inheritance */
				};
			};

#if 0
		struct direct_file {
			template <typename DATA_TRAITS>
				struct storage_type {
					typedef impl::direct_file<DATA_TRAITS> impl;
				};
			};

		struct cached_file {
			template <typename DATA_TRAITS>
				struct storage_type {
					typedef impl::cached_file<DATA_TRAITS> impl;
				};
			};

		struct buffer_or_swap {
			template <typename DATA_TRAITS>
				struct storage_type {
					typedef impl::adaptive_storage<DATA_TRAITS, impl::buffer<DATA_TRAITS>, impl::cached_file<DATA_TRAITS> > impl;
				};
		};
#endif

		namespace workaround {
			template <typename DATA_TRAITS, int N> struct pagefile;
			/*template <typename DATA_TRAITS> pagefile<DATA_TRAITS, 4> { typedef impl::sliced_boost_file<DATA_TRAITS> impl; };*/
			template <typename DATA_TRAITS> struct pagefile<DATA_TRAITS, 4> {
# 				ifdef WIN32
				typedef impl::boost_file<DATA_TRAITS> impl;
#				else
				typedef impl::cached_file<DATA_TRAITS> impl; /* boost::mapped_file doesn't seem to handle well large files in linux32 */
#				endif
			};
			/*template <typename DATA_TRAITS> struct pagefile<DATA_TRAITS, 4> { typedef impl::boost_file<DATA_TRAITS> impl; };*/
			template <typename DATA_TRAITS> struct pagefile<DATA_TRAITS, 8> { typedef impl::boost_file<DATA_TRAITS> impl; };
		}

		struct pagefile {
			template <typename DATA_TRAITS> struct storage_type {
				/*typedef impl::boost_file<DATA_TRAITS> impl;*/
//#				if defined(WORKAROUND_CACHE_FILE) /*&& !defined(WIN32)*/
				typedef typename impl::cached_file<DATA_TRAITS> impl;
/*#				else*/
				/*typedef typename workaround::pagefile<DATA_TRAITS, sizeof(void*)>::impl impl;*/
/*#				endif*/
			};
		};

		struct buffer_or_file {
			template <typename DATA_TRAITS>
				struct storage_type {
					/*typedef impl::adaptive_storage<DATA_TRAITS, impl::buffer<DATA_TRAITS>, impl::pagefile<DATA_TRAITS> > impl;*/
					typedef impl::adaptive_storage<DATA_TRAITS, impl::buffer<DATA_TRAITS>, typename pagefile::storage_type<DATA_TRAITS>::impl> impl;
				};
		};
	}
}

#endif

