#ifndef _MATRIX_BASE_H_
#define _MATRIX_BASE_H_

#ifndef NDEBUG
#  include <cstdio>
#  include <iostream>
#  define DBG(_x_) do { std::cerr << _x_; } while(0)
#else
#  define DBG(_o_)
#endif

#define SWAP(_x, _y) do { _x^=_y; _y^=_x; _x^=_y; } while(0)

#include <vector>
#include <string>
#include <sstream>


namespace matrix {
	namespace utils {
		template <typename C>
			class Singleton {
				private:
					Singleton() {}
					Singleton(const Singleton<C>&h) {}
				public:
					static C* instance() {
						static C instance_;	/* ensures proper destruction on exit :) */
						return &instance_;
					}
			};

	}

	namespace sync {

		template<class M>
			struct scope_lock {
				M&m;
				scope_lock(M&_) : m(_) { m.lock(); }
				~scope_lock() { m.unlock(); }
			};

		struct dummy {
			class mutex_type {
				public:
					typedef void handle_type;
					void lock() { /*std::cerr << "dummy mutex lock" << std::endl;*/ }
					void unlock() { /*std::cerr << "dummy mutex unlock" << std::endl;*/ }
					void* native_handle() { return NULL; }
			};
			class rw_spin_type {
				public:
					typedef void handle_type;
					void lock_read() {}
					void lock() {}
					void unlock() {}
			};
			class spin_type {
				public:
					typedef void handle_type;
					void lock() { /*std::cerr << "dummy spin lock" << std::endl;*/ }
					void unlock() { /*std::cerr << "dummy spin unlock" << std::endl;*/ }
			};
			typedef mutex_type unique_lock_type;
			class condvar_type {
				public:
					typedef void handle_type;
					void notify_one() {}
					void notify_all() {}
					void wait(unique_lock_type& m) {}
			};
		};
#	ifdef SYNCHRONIZATION_TRAITS
		typedef SYNCHRONIZATION_TRAITS default_traits;
#	else
		typedef dummy default_traits;
#	endif
		namespace detail {
			inline void _(...) {}

			struct check_mutex_type : protected default_traits::mutex_type {
				public:
					typedef default_traits::mutex_type M;
					void check_lock() { _(static_cast<void(M::*)()>(&M::lock)); }
					void check_unlock() { _(static_cast<void(M::*)()>(&M::unlock)); }
#if 0
					typedef default_traits::mutex_type::handle_type H;
					void check_native_handle() { _(static_cast<H*(M::*)()>(&M::native_handle)); }
#endif
			};

			struct check_rw_spin_type : public default_traits::rw_spin_type {
				public:
					typedef default_traits::rw_spin_type S;
					void check_lock_read() { _(static_cast<void (S::*)()>(&S::lock_read)); }
					void check_lock() { _(static_cast<void (S::*)()>(&S::lock)); }
					void check_unlock() { _(static_cast<void (S::*)()>(&S::unlock)); }
			};

			struct check_spin_type : public default_traits::spin_type {
				public:
					typedef default_traits::spin_type S;
					void check_lock() { _(static_cast<void (S::*)()>(&S::lock)); }
					void check_unlock() { _(static_cast<void (S::*)()>(&S::unlock)); }
			};

			struct check_condvar_type : public default_traits::condvar_type {
#if 0
				public:
					typedef default_traits::condvar_type C;
					typedef default_traits::condvar_type::handle_type H;
					typedef default_traits::unique_lock_type U;
					void check_notify_one() { _(static_cast<void (C::*)()>(&C::notify_one)); }
					void check_notify_all() { _(static_cast<void (C::*)()>(&C::notify_all)); }
					void check_wait() { _(static_cast<void (C::*)(U&)>(&C::wait)); }
#endif
            };
		}
	}

	namespace exception {
		class Base : public std::exception {};
		class Dimension : public Base {};
	}
	
	namespace constraints {
		template <typename T> struct check_not_void { static const bool value=true; };
		template <> struct check_not_void<void> { static const bool value=false; };
	}

/*#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
	/*template <typename DATA_TRAITS, typename ADDRESSING_SPACE, typename SYNC_TRAITS, bool use_locking_data_reference>*/
/*#else*/
	template <typename DATA_TRAITS, typename ADDRESSING_SPACE, typename STORAGE>
/*#endif*/
		class Matrix;

}

#endif

