#include "backend_swap_base.h"


int fd;

#define PAGESIZE 4096

void init_swap() {
	fd = open("temp", O_CREAT|O_NOATIME|O_RDWR, S_IRUSR|S_IWUSR);
	lseek(fd, 1<<20, SEEK_SET);
	char dummy=0;
	write(fd, &dummy, 1);
}

void* do_pmap(int pnum, int count) {
	return mmap(NULL, count*PAGESIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, pnum*PAGESIZE);
}

void sync_unmap(void*ptr, int count) {
	msync(ptr, count*PAGESIZE, MS_SYNC);
	munmap(ptr, count*PAGESIZE);
}

void async_unmap(void*ptr, int count) {
	msync(ptr, count*PAGESIZE, MS_ASYNC);
	munmap(ptr, count*PAGESIZE);
}


