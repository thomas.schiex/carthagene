#ifndef _MATRIX_ADDR_H_
#define _MATRIX_ADDR_H_

#include <stdint.h>
#include <vector>

#include <ext/hash_map>
using __gnu_cxx::hash_map;

namespace matrix {
	namespace addressing_space {
		/*typedef unsigned long long offset_type;*/
		/*typedef unsigned long offset_type;*/
		namespace detail {
			template <int N> struct find_word_size {};
			template <> struct find_word_size<4> { typedef int32_t word_type; };
			template <> struct find_word_size<8> { typedef int64_t word_type; };
			typedef find_word_size<sizeof(void*)> my_word_size;
		}
		/*typedef detail::my_word_size::word_type offset_type;*/
		typedef int64_t offset_type;

		struct off_by_one { static void _(offset_type& x) { --x; } };
		struct zero { static void _(offset_type& x) {} };

		struct DirectAddressingSpace {
			typedef offset_type row_key;
			typedef offset_type column_key;
		};

		template <class _conv = zero>
		struct Triangular : public DirectAddressingSpace {
			typedef _conv origin;
			Triangular(offset_type r, offset_type c) { set_dimension(r, c); }
			void set_dimension(offset_type r, offset_type c) {
				if(r!=c) { throw exception::Dimension(); }
				rows=r;
				/*std::cerr << "set matrix dimension to ("<<r<<','<<c<<"), that is "<<get_storage_size()<<" elements."<<std::endl;*/
			}
			offset_type make_offset(offset_type& i, offset_type& j) const {
				if(i<j) SWAP(i, j);
				offset_type r=i, c=j;
				_conv::_(r); _conv::_(c);
				return c+((((offset_type)r)*(r+1))>>1);
			}
			offset_type get_storage_size() const { return (((offset_type)rows)*(rows+1))>>1; }
			protected: offset_type rows;
		};

		template <class _conv = zero>
		struct TriangularWithoutMainDiag : public DirectAddressingSpace {
			typedef _conv origin;
			TriangularWithoutMainDiag(offset_type r, offset_type c) { set_dimension(r, c); }
			void set_dimension(offset_type r, offset_type c) {
				if(r!=c) { throw exception::Dimension(); }
				rows=r;
				/*std::cerr << "set matrix dimension to ("<<r<<','<<c<<"), that is "<<get_storage_size()<<" elements."<<std::endl;*/
			}
			offset_type make_offset(offset_type& i, offset_type& j) const {
				if(i<j) SWAP(i, j);
				offset_type r=i, c=j;
				_conv::_(r); _conv::_(c);
				return c+((((offset_type)r)*(r-1))>>1);
			}
			offset_type get_storage_size() const { return (((offset_type)rows)*(rows-1))>>1; }
			protected: offset_type rows;
		};

		template <class _conv = zero>
		struct Rectangular : public DirectAddressingSpace {
			typedef _conv origin;
			Rectangular(offset_type r, offset_type c) { set_dimension(r, c); }
			void set_dimension(offset_type r, offset_type c) { rows=r; cols=c; }
			offset_type make_offset(offset_type i, offset_type j) const {  _conv::_(i); _conv::_(j); return cols*((offset_type)i)+j; }
			offset_type get_storage_size() const { return rows*cols; }
			protected: offset_type rows, cols;
		};

		template <typename K, typename I>
			struct dimension_handler {
				hash_map<I, K> key_by_index;
				hash_map<K, I> index_by_key;
				struct iterator {
				};
			};

		template <typename row_traits, typename column_traits, class _conv = zero>
		struct Abstract : public Rectangular<_conv> {
			typedef typename row_traits::key_type row_key_type;
			typedef typename column_traits::key_type column_key_type;
			typedef typename row_traits::index_type row_index_type;
			typedef typename column_traits::index_type column_index_type;


			Abstract(offset_type r, offset_type c) : Rectangular<_conv>(r, c) {}
		};
	}

}

#endif

