/* Code slightly derived from http://www.mail-archive.com/guile-devel@gnu.org/msg05078.html */

#ifdef _WIN32

#include "ubiquitous_mmap.h"
#include <stdio.h>
#include <io.h>
/*
 * Implementation of mmap()/munmap() replacement for Windows.
 */

typedef struct {
    unsigned int prot_flag;
    DWORD        win_flag;
} Protection_Scheme_t;

typedef struct _MapList_t {
    HANDLE             hMap;
    void              *Base;
    struct _MapList_t *Next;
} MapList_t;

static const Protection_Scheme_t Protection_Scheme[] = {
    { PROT_READ,                      PAGE_READONLY          },
    { PROT_READ|PROT_WRITE,           PAGE_READWRITE         },
    { PROT_READ|PROT_WRITE|PROT_EXEC, PAGE_EXECUTE_READWRITE },
    { PROT_EXEC,                      PAGE_EXECUTE           },
    { PROT_READ|PROT_EXEC,            PAGE_EXECUTE_READ      },
};

static MapList_t *MapList = NULL;

#define DEBUG(fmt, ...) fprintf(stderr, "mmap:" fmt ,##__VA_ARGS__)

void *mmap(void* address,
                  unsigned int size,
                  unsigned int protection,
                  unsigned int flags,
                  HANDLE       hFile,
                  int          offset)
{
  HANDLE       hMapFile;
  DWORD        dwProtect, dwAccess;
  void        *Base;
  MapList_t   *Item;
  unsigned int x;

  if (hFile == INVALID_HANDLE_VALUE) {
      DEBUG("err: hFile: invalid handle value %lu\n", GetLastError());
    return MAP_FAILED;
  }

  /* Search protection schemes */
  for (dwProtect=PAGE_NOACCESS, x=0;
       x < (sizeof(Protection_Scheme)/sizeof(Protection_Scheme_t));
       x++)
  {
    if (Protection_Scheme[x].prot_flag == protection)
    {
      dwProtect = Protection_Scheme[x].win_flag;
      break;
    }
  }

  if (flags & MAP_PRIVATE) {
    dwAccess = FILE_MAP_COPY;
    dwProtect = PAGE_WRITECOPY;
  } else
  if ((protection & PROT_WRITE))
    dwAccess = FILE_MAP_WRITE;
  else
    dwAccess = FILE_MAP_READ;

  /* Create mapping object */
  hMapFile = CreateFileMapping(hFile, NULL, dwProtect, 0, size, NULL);
  if (hMapFile == INVALID_HANDLE_VALUE) {
      DEBUG("err: hMapFile: invalid handle value %lu\n", GetLastError());
    return MAP_FAILED;
  }

  /* Select which portions of the file we need (entire file) */
  Base = MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, offset, size);

  if (Base == NULL) {
    /* Free the mapping object */
    CloseHandle(hMapFile);
    DEBUG("err: couldn't get mapping base %lu\n", GetLastError());
    return MAP_FAILED;
  }

  /* Allocate item for list mmaps... */
  Item = (MapList_t *)malloc(sizeof(MapList_t));
  if (Item == NULL) {
    DEBUG("err: couldn't alloc mapping list %lu\n", GetLastError());
    UnmapViewOfFile(Base);
    CloseHandle(hMapFile);

    return MAP_FAILED;
  }

  Item->hMap = hMapFile;
  Item->Base = Base;
  Item->Next = MapList;

  if (MapList == NULL)
    MapList = Item;

  DEBUG("info: got Base=%p\n", Base);

  return Base;
}

int munmap(void *addr, unsigned int size)
{
    UnmapViewOfFile(addr);
#if 0
  MapList_t *Item, *Prev;

  Prev = NULL;
  Item = MapList;

  while (Item != NULL) {
    if (Item->Base == addr) {
      UnmapViewOfFile(Item->Base);
      CloseHandle(Item->hMap);

      /* Delete this item from linked list */
      if (Prev != NULL)
        Prev->Next = Item->Next;
      else
        MapList = Item->Next;

      free(Item);

      return 0;
    }
    Prev = Item;
    Item = Item->Next;
  }
  return -1;
#endif
  return 0;
}

#endif /* _WIN32 */

