#ifndef _MATRIX_BACKEND_SWITCHER_H_
#define _MATRIX_BACKEND_SWITCHER_H_

#include "base.h"
#include "addressing_space.h"

#include "backend_base.h"
#include "backend_selector.h"

#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
#	define BACKEND_INHERITANCE : public Generic<DATA_TRAITS, SYNC_TRAITS>
#else
#	define BACKEND_INHERITANCE
#endif

namespace matrix {
	namespace backend {
		using ::matrix::addressing_space::offset_type;
		using ::matrix::constraints::check_not_void;

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			class BackendSwitcher {
				public:
					typedef BackendSwitcher<DATA_TRAITS, SYNC_TRAITS> backend_type;
					typedef typename DATA_TRAITS::context_type context_type;
					typedef typename DATA_TRAITS::data_type data_type;
					typedef DATA_TRAITS data_traits;
					typedef SYNC_TRAITS sync_traits;

					BackendSwitcher(context_type _) : backend_(0), ctxt(_), size(0) {}
					~BackendSwitcher() { if(backend_) { delete backend_; } }
					
					void init(offset_type sz) {
						size = sz;
						backend_ = create_default();
					}
					template <typename M>
					bool switch_backend(M* matrix, const char* backend_name, bool eager) {
						backend_type* b = backend::selector<DATA_TRAITS, SYNC_TRAITS>(
											ctxt, size, backend_name);
						if(!b) {
							return false;
						}
						if(backend_) {
							if(eager) {
								/* TODO */
							}
							delete backend_;
						}
						backend_=b;
						return true;
					}

					data_type* raw_get(offset_type o) { return backend_->raw_get(o); }
					void raw_set(offset_type o, data_type* d) { backend_->raw_set(o, d); }
					void lock_data(offset_type o) { backend_->lock_data(o); }
					void unlock_data(offset_type o) { backend_->unlock_data(o); }

				protected:
					Generic<DATA_TRAITS, SYNC_TRAITS>* backend_;
					Generic<DATA_TRAITS, SYNC_TRAITS>* create_default() {
						/*return backend::selector(this, "swap");*/
						return backend::selector<DATA_TRAITS, SYNC_TRAITS>(ctxt, size, (const char*)NULL);
					}
				private:
					context_type ctxt;
					offset_type size;
			};

	}

}

#endif

