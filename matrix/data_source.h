#ifndef _MATRIX_DATA_SOURCE_H_
#define _MATRIX_DATA_SOURCE_H_

#include "base.h"

namespace matrix {
	namespace data {

		typedef long long int int64_t;
		typedef int64_t fence_resource_type;

		template <typename SYNC_TRAITS>
			class Synchronizable {
				public:
					typedef typename SYNC_TRAITS::mutex_type lock_type;
					Synchronizable() : mutex__() {}
					void lock() { mutex__.lock(); }
					void unlock() { mutex__.unlock(); }
					lock_type* mutex_handle() { return &mutex__; }
				private:
					lock_type mutex__;
			};
	}

	namespace detail {
		static inline void _(...) {}

		template <bool P> struct fence_type_size;

		template <> struct fence_type_size<true> {
			enum { fence_type_is_bigger_than_data_type = 0 };
		};

		template <> struct fence_type_size<false> {
			enum { dummy };
		};

		template <typename DATA_TRAITS>
		struct fence_type_fits_in_data_type
			: public fence_type_size<
				sizeof(typename DATA_TRAITS::data_type)
					>=
				sizeof(typename DATA_TRAITS::fence_type)>
		{};

		template <typename X>
			struct typesize {
				char size[sizeof(X)];
			};

		template <typename DATA_TRAITS>
			struct check_fence_type_is_not_bigger_than_data_type {
				char fence_type[sizeof(typename DATA_TRAITS::fence_type)];
				char pad[sizeof(typename DATA_TRAITS::data_type)-sizeof(typename DATA_TRAITS::fence_type)];
			};



		template <typename context_type, typename pagefile_name_provider_type>
			struct pagefile_name_provider {
				typedef context_type C;
				typedef pagefile_name_provider_type P;
				pagefile_name_provider() {
					check_cast_to_const_char();
				}
				void check_cast_to_const_char() {
					_(static_cast<const char* (P::*)() const>(&P::operator const char*));
				}
			};

		template <typename context_type>
			struct pagefile_name_provider<context_type, void> {};

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			struct immed_traits
				: public pagefile_name_provider
				  	<typename DATA_TRAITS::context_type,
					 typename DATA_TRAITS::pagefile_name_provider_type>
			{
				typedef typename SYNC_TRAITS::spin_type spin_type;
				typedef typename SYNC_TRAITS::condvar_type condvar_type;
				typedef typename DATA_TRAITS::data_type data_type;
			};

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			struct computed_traits
				: public immed_traits<DATA_TRAITS, SYNC_TRAITS>
			{
				typedef typename DATA_TRAITS::context_type context_type;
				struct computation_handler : protected DATA_TRAITS::computation_handler_type {
					typedef typename DATA_TRAITS::context_type context_t;
					typedef typename DATA_TRAITS::data_type data_t;
					typedef typename DATA_TRAITS::computation_handler_type C;
					void check_compute() {
						_(static_cast<void(C::*)(context_t, int, int, data_t*)>(&C::compute));
					}
					void check_must_compute() {
						_(static_cast<bool(C::*)(context_t, data_t*)>(&C::must_compute));
					}
					computation_handler() { _(&computation_handler::check_compute); _(&computation_handler::check_must_compute); }
				};
				computed_traits() { _(new computation_handler()); }
			};

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			struct fenced_computed_traits
				: public computed_traits<DATA_TRAITS, SYNC_TRAITS>
			{
				typedef typename DATA_TRAITS::data_type data_type;
				typedef typename DATA_TRAITS::fence_type fence_type;
				typedef typename DATA_TRAITS::fence_handler_type fence_handler_type;
				/*typedef typename DATA_TRAITS::fence_handler_type fence_handler_type;*/
				struct fence_handler : protected DATA_TRAITS::fence_handler_type {
					typedef data::fence_resource_type fence_resource_type;
					typedef typename DATA_TRAITS::data_type data_t;
					typedef typename DATA_TRAITS::fence_type fence_t;
					typedef typename DATA_TRAITS::fence_handler_type F;
					void check_is_fenced() {
						_(static_cast<bool (F::*)(data_t*)>(&F::is_fenced));
					}
					void check_fence() {
						_(static_cast<void (F::*)(fence_resource_type, data_t*)>(&F::fence));
					}
					void check_get_resource() {
						_(static_cast<fence_resource_type (F::*)(data_t*) const>(&F::get_resource));
					}
					fence_handler() {
						_(fence_type_fits_in_data_type<DATA_TRAITS>::fence_type_is_bigger_than_data_type);
						check_fence_type_is_not_bigger_than_data_type<DATA_TRAITS> test;
						_(test);
						_(&fence_handler::check_is_fenced);
						_(&fence_handler::check_get_resource);
						_(&fence_handler::check_fence);
					}
				};
				fenced_computed_traits() : computed_traits<DATA_TRAITS, SYNC_TRAITS>() { _(new fence_handler()); }
			};
	}
	namespace data {

		template <typename DATA_TRAITS, typename SYNC_TRAITS, bool COMPUTED, bool WITH_FENCE>
			class ComputationHandler;

		/* No computation */
		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			class ComputationHandler<DATA_TRAITS, SYNC_TRAITS, false, false>
				: public Synchronizable<SYNC_TRAITS>
				, private detail::immed_traits<DATA_TRAITS, SYNC_TRAITS>
			{
				public:
					typedef typename DATA_TRAITS::context_type context_type;
					typedef typename DATA_TRAITS::data_type data_type;
					void compute(context_type context, int i, int j, data_type* d) {}
			};

		/* Unfenced computation (simple wrapper) */
		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			class ComputationHandler<DATA_TRAITS, SYNC_TRAITS, true, false>
				: public Synchronizable<SYNC_TRAITS>,
				  public DATA_TRAITS::computation_handler_type
				, private detail::computed_traits<DATA_TRAITS, SYNC_TRAITS>
			{
				public:
					typedef typename DATA_TRAITS::computation_handler_type computation_handler_type;
					typedef typename DATA_TRAITS::fence_handler_type fence_handler_type;
					typedef typename DATA_TRAITS::context_type context_type;
					typedef typename DATA_TRAITS::data_type data_type;
					void compute(context_type context, int i, int j, data_type* d) {
						if(computation_handler_type::must_compute(context, d)) {
							computation_handler_type::compute(context, i, j, d);
						}
					}
			};

		/* Fenced computation */
		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			/*class ComputationHandler<DATA_TRAITS, SYNC_TRAITS, typename DATA_TRAITS::computation_handler_type, typename DATA_TRAITS::fence_type>*/
			class ComputationHandler<DATA_TRAITS, SYNC_TRAITS, true, true>
				: public Synchronizable<SYNC_TRAITS>,
				  public DATA_TRAITS::computation_handler_type,
				  public DATA_TRAITS::fence_handler_type
				, private detail::fenced_computed_traits<DATA_TRAITS, SYNC_TRAITS>
			{
				private:
					typedef typename SYNC_TRAITS::condvar_type condvar_type;
					typedef typename SYNC_TRAITS::mutex_type mutex_type;
					class fres_handle;
					typedef std::vector<fres_handle*> fres_vector;
					fres_vector alloc;
					fres_vector pool;
					/*mutex_type handle_lock;*/
					class fres_handle {
						private:
							unsigned int refcount;
							int id_;
							condvar_type cond_;
						public:
							fres_handle(int n)
								: refcount(0), id_(n), cond_()
							{}
							void ref() { ++refcount; }
							void unref(std::vector<fres_handle*>&a) {
								--refcount;
								if(!refcount) {
									/*std::cerr << "fres_handle::alloc releasing " << this << std::endl << std::flush;*/
									a.push_back(this);
								}
							}
							int id() const { return id_; }
							condvar_type& cond() { return cond_; }
					};
					fres_handle* alloc_handle() {
						//::matrix::sync::scope_lock<mutex_type> alloc_lock(handle_lock); /* without stutter */
						if(alloc.size()) {
							fres_handle* ret = alloc.back();
							alloc.pop_back();
							/*std::cerr << "fres_handle::alloc reusing " << ret << std::flush << std::endl << std::flush;*/
							return ret;
						}
						/*std::cerr << "fres_handle::alloc new" << std::endl << std::flush;*/
						pool.push_back(new fres_handle(pool.size()));
						return pool.back();
					}
					int comps, waits, accesses;
				public:
					typedef typename DATA_TRAITS::fence_handler_type fence_handler_type;
					typedef data::fence_resource_type fence_resource_type;
					typedef typename DATA_TRAITS::computation_handler_type computation_handler_type;
					typedef typename DATA_TRAITS::context_type context_type;
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename SYNC_TRAITS::unique_lock_type unique_lock_type;
					typedef Synchronizable<SYNC_TRAITS> sync;
					using Synchronizable<SYNC_TRAITS>::mutex_handle;
					using Synchronizable<SYNC_TRAITS>::lock;
					using Synchronizable<SYNC_TRAITS>::unlock;
					void compute(context_type context, int i, int j, data_type* d) {
						if(fence_handler_type::is_fenced(d)) {
							unique_lock_type ult(*mutex_handle());
							/*lock();*/
							fres_handle* h = pool[fence_handler_type::get_resource(d)];
							h->ref();
							/*fence_handler_type::wait(mutex_handle(), d);*/
							/*condvar_type* cond = (condvar_type*)fence_handler_type::get_resource(d);*/
							condvar_type& cond = h->cond();
							while(fence_handler_type::is_fenced(d)) {
								/*std::cerr << "waiting for fenced value at "<<std::dec<<i<<','<<j<<std::endl;*/
								/*cond.wait(*mutex_handle());*/
								cond.wait(ult);
							}
							/*std::cerr << "done waiting for fenced value at "<<i<<','<<j<<std::endl;*/
							h->unref(alloc);
							++waits;
							/*unlock();*/
						} else if(computation_handler_type::must_compute(context, d)) {
							lock();
							data_type temp;
							/*condvar_type cond;*/
							/*std::cerr << "computing value at "<<std::dec<<i<<','<<j<<std::endl;*/
							/*fence_resource_type res;*/
							fres_handle* h = alloc_handle();
							h->ref();
							condvar_type& cond = h->cond();
							/*fence_handler_type::fence(static_cast<fence_resource_type*>(h), mutex_handle(), d);*/
							fence_handler_type::fence(h->id(), d);
							unlock();
							computation_handler_type::compute(context, i, j, &temp);
							/*fence_handler_type::release(static_cast<fence_resource_type*>(h), mutex_handle(), d, &temp);*/
							lock();
							/*fence_handler_type::release(&res, mutex_handle(), d, &temp);*/
							*d = temp;
							/*std::cerr << "done computing value at "<<i<<','<<j<<std::endl;*/
							++comps;
							cond.notify_all();
							h->unref(alloc);
							unlock();
						}
						++accesses;
					}
					ComputationHandler()
						: alloc(), pool()
						  , comps(0), waits(0), accesses(0)
					{}
					virtual ~ComputationHandler() {
						while(pool.size()) {
							delete pool.back();
							pool.pop_back();
						}
						std::cerr << "in " << std::dec << accesses << " accesses, performed " << comps << " computations and waited " << waits << " times"<<std::endl;
					}
			};

		using ::matrix::constraints::check_not_void;

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			struct ComputationHandlerSelector
				: public ComputationHandler
				  <	DATA_TRAITS, SYNC_TRAITS,
					check_not_void<typename DATA_TRAITS::
						computation_handler_type
					 >::value,
					check_not_void<typename DATA_TRAITS::
						fence_handler_type
					>::value >
			{};




		template <typename DATA_TRAITS, typename SYNC_TRAITS>
		class Provider
			: public ComputationHandlerSelector<DATA_TRAITS, SYNC_TRAITS>
		{
			private:
				typename DATA_TRAITS::context_type context_;
			protected:
				void check(int i, int j, typename DATA_TRAITS::data_type* d) {
					compute(context_, i, j, d);
				}
			public:
				Provider(typename DATA_TRAITS::context_type _) : context_(_) {}
				typename DATA_TRAITS::context_type context() const { return context_; }
		};

		/* traits */
		template<typename D>
			struct immed
			{
				typedef void computation_handler_type;
				typedef void fence_handler_type;
				typedef void fence_type;
				typedef void pagefile_name_provider_type;
				/*static const bool delete_pagefile_when_done = true;*/
				static const bool delete_pagefile_when_done = false;
				typedef D data_type;
				typedef void* context_type;
				static void clear_data(D&d) { d=0; }
			};
		
		typedef immed<int> Int;
		typedef immed<double> Double;
	}
}

#endif

