#ifndef _MATRIX_CHECK_DISK_H_
#define _MATRIX_CHECK_DISK_H_

#include <iostream>
#include <cstring>

#ifdef WIN32
#  include <windows.h>

namespace check {
    class disk {
        private:

            static std::string _wd() {
                char ret[4096];
                GetCurrentDirectory(4096, ret);
                return std::string(ret);
            }


            static bool _is_root(std::string& path) {
                return path.size()>1 && path[0]>='A' && path[0]<='Z' && path[1]==':' || path[0] == '\\';
            }


            static std::string get_drive(std::string& path) {
                if(_is_root(path) && path.size()>1 && path[1]==':') {
                    return path.substr(0, 2);
                } else {
                    std::string wd = _wd();
                    return get_drive(wd);
                }
            }

        public:
            static bool is_local(std::string& path) {
                std::string drive = get_drive(path);
                UINT type = GetDriveType(drive.c_str());
                switch(type) {
                    case DRIVE_REMOVABLE:
                    case DRIVE_FIXED:
                    case DRIVE_RAMDISK:
                        return true;
                    /* output error/warning message for these following cases : */
                    case DRIVE_UNKNOWN:
                    case DRIVE_NO_ROOT_DIR:
                    case DRIVE_REMOTE:
                    case DRIVE_CDROM:
                    default:;
                };
                return false;
            }


            static bool is_free_space_big_enough(std::string& path, double size) {
                ULARGE_INTEGER free;
                GetDiskFreeSpaceEx(path.substr(0, path.find_last_of("\\/")).c_str(), &free, NULL, NULL);
                return ((double) free.QuadPart) > size;
            }
    };
}

#else /* def WIN32 */

#  include <unistd.h>
#  include <libgen.h>
#  include <sys/vfs.h>
#  include <sstream>
#  include <vector>
#  include <errno.h>
#  include <stdlib.h>

/* From man 2 statfs : */
#  define ADFS_SUPER_MAGIC      0xadf5
#  define AFFS_SUPER_MAGIC      0xADFF
#  define BEFS_SUPER_MAGIC      0x42465331
#  define BFS_MAGIC             0x1BADFACE
#  define CIFS_MAGIC_NUMBER     0xFF534D42
#  define CODA_SUPER_MAGIC      0x73757245
#  define COH_SUPER_MAGIC       0x012FF7B7
#  define CRAMFS_MAGIC          0x28cd3d45
#  define DEVFS_SUPER_MAGIC     0x1373
#  define EFS_SUPER_MAGIC       0x00414A53
#  define EXT_SUPER_MAGIC       0x137D
#  define EXT2_OLD_SUPER_MAGIC  0xEF51
#  define EXT2_SUPER_MAGIC      0xEF52 /* manpage and linux/magic.h define this to 0xEF53 which is also EXT3 magic. seems wrong. */
#  define EXT3_SUPER_MAGIC      0xEF53
#  define HFS_SUPER_MAGIC       0x4244
#  define HPFS_SUPER_MAGIC      0xF995E849
#  define HUGETLBFS_MAGIC       0x958458f6
#  define ISOFS_SUPER_MAGIC     0x9660
#  define JFFS2_SUPER_MAGIC     0x72b6
#  define JFS_SUPER_MAGIC       0x3153464a
#  define MINIX_SUPER_MAGIC     0x137F /* orig. minix */
#  define MINIX_SUPER_MAGIC2    0x138F /* 30 char minix */
#  define MINIX2_SUPER_MAGIC    0x2468 /* minix V2 */
#  define MINIX2_SUPER_MAGIC2   0x2478 /* minix V2, 30 char names */
#  define MSDOS_SUPER_MAGIC     0x4d44
#  define NCP_SUPER_MAGIC       0x564c
#  define NFS_SUPER_MAGIC       0x6969
#  define NTFS_SB_MAGIC         0x5346544e
#  define OPENPROM_SUPER_MAGIC  0x9fa1
#  define PROC_SUPER_MAGIC      0x9fa0
#  define QNX4_SUPER_MAGIC      0x002f
#  define REISERFS_SUPER_MAGIC  0x52654973
#  define ROMFS_MAGIC           0x7275
#  define SMB_SUPER_MAGIC       0x517B
#  define SYSV2_SUPER_MAGIC     0x012FF7B6
#  define SYSV4_SUPER_MAGIC     0x012FF7B5
#  define TMPFS_MAGIC           0x01021994
#  define UDF_SUPER_MAGIC       0x15013346
#  define UFS_MAGIC             0x00011954
#  define USBDEVICE_SUPER_MAGIC 0x9fa2
#  define VXFS_SUPER_MAGIC      0xa501FCF5
#  define XENIX_SUPER_MAGIC     0x012FF7B4
#  define XFS_SUPER_MAGIC       0x58465342
#  define _XIAFS_SUPER_MAGIC    0x012FD16D

namespace check {
    class disk {
        private:


            static std::string _wd() {
                char ret[4096];
                return std::string(getcwd(ret, 4096));
            }


            static std::string canonify_path(std::string& path) {
                std::string absolut;

                /* 1: make path absolute. */
                if(path[0]=='~') {
                    absolut = getenv("HOME");
                    absolut += path.substr(1);
                } else if(path[0]!='/') {
                    absolut = _wd();
                    absolut += '/';
                    absolut += path;
                } else {
                    absolut = path;
                }

                /* 2: find limits of path tokens. */
                std::vector<std::string::iterator> sep;
                std::string::iterator i = absolut.begin(), j = absolut.end();
                while(i!=j) {
                    if(*i == '/') {
                        sep.push_back(i);
                    }
                    ++i;
                }
                sep.push_back(i);

                /* 3: tokenize path, removing transient "." and couples of (something, ".."). */
                std::vector<std::string> elems;
                std::string::iterator start = absolut.begin();
                std::vector<std::string::iterator>::iterator endi = sep.begin(), endj = sep.end();
                while(endi != endj) {
                    if(start == *endi) {
                        start = *endi+1;
                        ++endi;
                        continue;
                    }
                    std::string elem(start, *endi);
                    start = *endi+1;
                    if(elem == ".") {
                        ++endi;
                        continue;
                    } else if(elem == "..") {
                        if(elems.size()) {
                            elems.pop_back();
                        }
                        ++endi;
                        continue;
                    }
                    elems.push_back(elem);
                    ++endi;
                }

                /* 4: reconstruct canonical path string. */
                std::stringstream ss;
                std::vector<std::string>::iterator ei = elems.begin(), ej = elems.end();
                while(ei!=ej) {
                    ss << '/';
                    ss << *ei;
                    ++ei;
                }

                return ss.str();
            }

        public:

            static bool is_free_space_big_enough(std::string& path, double size) {
                struct statfs st;
                std::string cpath = canonify_path(path).c_str();
                if(statfs(dirname((char*)cpath.c_str()), &st)) {
                    /* error */
                    std::cerr << "Couldn't retrieve filesystem information for path "
                              << cpath << " : " << strerror(errno) << std::endl;
                    return false;
                }
                double free_size = st.f_bfree;
                free_size *= st.f_bsize;
                /*std::cout << "DEBUG FREE SPACE  free=" << free_size << " needed=" << size << std::endl;*/
                return free_size > size;
            }


            static bool is_local(std::string& path) {
                struct statfs st;
                std::string cpath = canonify_path(path).c_str();
                if(statfs(dirname((char*)cpath.c_str()), &st)) {
                    /* error */
                    std::cerr << "Couldn't retrieve filesystem information for path "
                              << cpath << " : " << strerror(errno) << std::endl;
                    return false;
                }
                switch(st.f_type) {
                    /* I don't know many of those filesystems. */
                    case ADFS_SUPER_MAGIC:
                    case AFFS_SUPER_MAGIC:
                    case BEFS_SUPER_MAGIC:
                    case BFS_MAGIC:
                    case CODA_SUPER_MAGIC:
                    case COH_SUPER_MAGIC:
                    case CRAMFS_MAGIC:
                    case DEVFS_SUPER_MAGIC:
                    case EFS_SUPER_MAGIC:
                    case EXT_SUPER_MAGIC:
                    case EXT2_OLD_SUPER_MAGIC:
                    case EXT2_SUPER_MAGIC:
                    case EXT3_SUPER_MAGIC:
                    case HFS_SUPER_MAGIC:
                    case HPFS_SUPER_MAGIC:
                    case HUGETLBFS_MAGIC:
                    case ISOFS_SUPER_MAGIC:
                    case JFFS2_SUPER_MAGIC:
                    case JFS_SUPER_MAGIC:
                    case MINIX_SUPER_MAGIC:
                    case MINIX_SUPER_MAGIC2:
                    case MINIX2_SUPER_MAGIC:
                    case MINIX2_SUPER_MAGIC2:
                    case NCP_SUPER_MAGIC:
                    case NTFS_SB_MAGIC:
                    case OPENPROM_SUPER_MAGIC:
                    case PROC_SUPER_MAGIC:
                    case QNX4_SUPER_MAGIC:
                    case REISERFS_SUPER_MAGIC:
                    case SYSV2_SUPER_MAGIC:
                    case SYSV4_SUPER_MAGIC:
                    case VXFS_SUPER_MAGIC:
                    case XENIX_SUPER_MAGIC:
                    case XFS_SUPER_MAGIC:
                    case _XIAFS_SUPER_MAGIC:
                        return true;
                    case UFS_MAGIC:
                    case SMB_SUPER_MAGIC:
                    case CIFS_MAGIC_NUMBER:
                    case NFS_SUPER_MAGIC:
                    case MSDOS_SUPER_MAGIC:
                    case ROMFS_MAGIC:
                    case TMPFS_MAGIC:
                    case UDF_SUPER_MAGIC:
                    case USBDEVICE_SUPER_MAGIC:
                    default:
                        return false;
                };
            }
    };
}

#endif /* else / def WIN32 */

#ifdef TESTING
int main(int argv, char** argc) {
    for(int i=1;i<argv;++i) {
        std::cout << argc[i] << std::endl;
        std::string tmp(argc[i]);
        /*std::cout << " ------> " << canonify_path(tmp) << std::endl;*/
        std::cout << " local ? " << check::disk::is_local(tmp) << std::endl;
        std::cout << " > 4GB free ? " << check::disk::is_free_space_big_enough(tmp, 4.*1024.*1024.*1024.) << std::endl;
    }
    return 0;
}
#endif /* def TESTING */

#endif /* ndef _MATRIX_CHECK_DISK_H_ */
