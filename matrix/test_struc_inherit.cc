#include <iostream>

struct A {
	int a;
};

struct B : public A {
	int b;
	int c;
};


int main(int argc, char**argv) {
	B* b = new B();
	std::cout << "a @" << (((char*)(&b->a)) - (char*)b) << std::endl;
	std::cout << "b @" << (((char*)(&b->b)) - (char*)b) << std::endl;
	std::cout << "c @" << (((char*)(&b->c)) - (char*)b) << std::endl;
	return 0;
}

