#include <iostream>
#include <iomanip>
#include <vector>
#include <list>

#include "../config.h"
/*#undef WITH_PARALLEL*/

#include "../parallel/parallel.h"
#include <semaphore.h>

#if TBB_INTERFACE_VERSION < 5000
#	include <pthread.h>
#endif

#ifdef WITH_PARALLEL
#define SYNCHRONIZATION_TRAITS SYNCHRONIZATION_TRAITS
struct SYNCHRONIZATION_TRAITS {
#if TBB_INTERFACE_VERSION >= 5000
	typedef tbb::mutex mutex_type;
	typedef tbb::spin_rw_mutex_v3 rw_spin_type;
	typedef tbb::spin_mutex spin_type;
	typedef tbb::interface5::condition_variable condvar_type;
	typedef tbb::interface5::unique_lock<tbb::mutex> unique_lock_type;
#else
#	warning Using old TBB ; not everything is available
	class mutex_type {
		public:
			typedef pthread_mutex_t handle_type;
			mutex_type() { pthread_mutex_init(&handle, NULL); }
			void lock() { pthread_mutex_lock(&handle); }
			void unlock() { pthread_mutex_unlock(&handle); }
			handle_type* native_handle() { return &handle; }
		private:
			handle_type handle;
	};
	class rw_spin_type {
		public:
			typedef void handle_type;
			void lock();
			void lock_read();
			void unlock();
	};
	class spin_type {
		public:
			typedef pthread_spinlock_t handle_type;
			spin_type() { pthread_spin_init(&handle, 0); }
			void lock() { pthread_spin_lock(&handle); }
			void unlock() { pthread_spin_unlock(&handle); }
		private:
			handle_type handle;
	};
	typedef mutex_type unique_lock_type;
	class condvar_type {
		public:
			typedef pthread_cond_t handle_type;
			condvar_type() { pthread_cond_init(&handle, NULL); }
			void notify_one() { pthread_cond_signal(&handle); }
			void notify_all() { pthread_cond_broadcast(&handle); }
			void wait(unique_lock_type&ul) { pthread_cond_wait(&handle, ul.native_handle()); }
		private:
			handle_type handle;
	};
#endif
};
#	include "matrix.h"
#else
#	undef SYNCHRONIZATION_TRAITS
#	include "matrix.h"
	typedef matrix::sync::dummy SYNCHRONIZATION_TRAITS;
#endif


using namespace matrix;
using namespace data;
using namespace addressing_space;

#if 1
#include "BioJeu.h"
#include "BioJeuSingle.h"
/*#include "twopoint.h"*/

#define DBLINT(_x_) (*(int64_t*)&_x_)
#define HEXDBL(_x_) std::hex << std::setw(16) << DBLINT(_x_)
#define XORDBL(_a, _b) std::hex << std::setw(16) << (DBLINT(_a)^DBLINT(_b))

typedef SYNCHRONIZATION_TRAITS::mutex_type mutex_type;
typedef SYNCHRONIZATION_TRAITS::spin_type spin_type;

const int64_t fence_marker = { 0xFFFFFFFFFFFFFFFFLL };	/* -nan for a double */

using matrix::data::fence_resource_type;

struct twopoint_traits {
	struct data_type {
		double LOD;
		double FR;
		data_type& operator=(const data_type&d) { LOD=d.LOD; FR=d.FR; return *this; }
	};
	typedef BioJeu* context_type;
	struct fence_type {
		int64_t fence;
		fence_resource_type fres;
	};
	struct fence_handler_type {
		bool is_fenced(data_type*d) {
			return ((fence_type*)d)->fence==fence_marker;
		}
		void fence(fence_resource_type res, data_type*d) {
			fence_type*f = (fence_type*) d;
			f->fence = fence_marker;
			f->fres = res;
		}
		fence_resource_type get_resource(data_type*d) const { return ((fence_type*)d)->fres; }
	};
	class pagefile_name_provider_type {
		private:
			std::string n;
		public:
			pagefile_name_provider_type(context_type context) {
				try {
					BioJeuSingle*bj = dynamic_cast<BioJeuSingle*>(context);
					n=bj->NomJeu;
					n+=".2pt-test";
				} catch(const std::bad_cast& bc) {
					n="";
				}
			}
			operator const char*() const {
				return n=="" ? NULL : n.c_str();
			}
	};
	static const bool delete_pagefile_when_done = false;	/* we want to keep previously computed two-point data */
	struct computation_handler_type {
		void compute(context_type c, int i, int j, data_type*d) {
			d->LOD = c->ComputeOneTwoPoints(j, i, c->Epsilon2, &d->FR);
		}
		bool must_compute(context_type c, data_type*d) { return d->LOD==0&&d->FR==0; }
	};
	static void clear_data(data_type&d) { d.LOD=d.FR=0; }
};




char bouf[2048];
char boufi[2048];
FILE *Fout;

#include <sstream>

typedef Matrix<	twopoint_traits,
				TriangularWithoutMainDiag<off_by_one>,
				SYNCHRONIZATION_TRAITS,
				true>
		two_point_matrix;

typedef std::list<std::pair<int, int> > discrep_list;

struct matrix_compare {
	BioJeu*& bj;
	two_point_matrix& test;
	int& total_small_discrepancy;
	int& total_big_discrepancy;
	Utils::ProgressDisplay& pdisp;
	discrep_list& dl;
	matrix::sync::default_traits::mutex_type mutex;
	matrix_compare(BioJeu*&a, two_point_matrix& b, int& c, int&d, Utils::ProgressDisplay& e, discrep_list& f)
		: bj(a), test(b), total_small_discrepancy(c), total_big_discrepancy(d), pdisp(e), dl(f), mutex()
	{}
	void operator()(int i) {
		/*volatile double value;*/
		/*volatile double orig;*/
		Utils::Tick t = Utils::Tick::now();
		/*for(int j=1;j<=i;++j) {*/
		/*for(int j=i;j<=bj->NbMarqueur;++j) {*/
		for(int j=1;j<=bj->NbMarqueur;++j) {
			volatile double value = test[i][j].LOD;
			volatile double orig = bj->GetTwoPointsLOD(i, j);
			mutex.lock();
			bool avoid_double=false;
			/*if(orig!=value) {*/
				/*value = test[i][j].LOD;*/
				/*orig = bj->GetTwoPointsLOD(i, j);*/
			/*}*/
			if(orig!=value) {
				double delta_rel = 100.*fabs(orig-value)/orig;
				//std::cerr << std::endl << "LOD discrepancy at ("<<i<<','<<j<<"), have "<<orig<<" vs "<<value<<" ("<<delta_rel<<"% difference)"<<std::endl;
				if(delta_rel > 1.e-10) {
					/*std::stringstream s;*/
					/*std::cerr << s.str();*/
					++total_big_discrepancy;
				} else {
					++total_small_discrepancy;
				}
				dl.push_back(std::pair<int, int>(i, j));
				//std::cerr /*<< std::endl*/ << std::dec << '(' << i << ',' << j << ") LOD xor = [" << XORDBL(orig, value) << ']' << std::endl;
				avoid_double=true;
			}
			mutex.unlock();
			value = test[i][j].FR;
			orig = bj->GetTwoPointsFR(i, j);
			mutex.lock();
			/*if(orig!=value) {*/
				/*value = test[i][j].FR;*/
				/*orig = bj->GetTwoPointsFR(i, j);*/
			/*}*/
			if(orig!=value) {
				/*std::stringstream s;*/
				double delta_rel = 100.*fabs(orig-value)/orig;
				//std::cerr << std::endl << "FR discrepancy at ("<<i<<','<<j<<"), have "<<orig<<" vs "<<value<<" ("<<delta_rel<<"% difference)"<<std::endl;
				if(delta_rel > 1.e-10) {
					/*std::cerr << s.str();*/
					++total_big_discrepancy;
				} else {
					++total_small_discrepancy;
				}
				if(!avoid_double) {
					dl.push_back(std::pair<int, int>(i, j));
				}
				//std::cerr << std::endl << std::dec << '(' << i << ',' << j << ") FR xor = [" << XORDBL(orig, value) << ']' << std::endl;
			}
			mutex.unlock();
		}
		/*mutex.lock();*/
		/*std::cerr << std::setw(10) << total_big_discrepancy << ' ' << std::setw(10) << total_small_discrepancy << ' ';*/
		pdisp.step_done((Utils::Tick::now()-t).seconds());
		/*mutex.unlock();*/
	}
};


int main(int argc, char**argv) {
	CartaGene Cartage;
	discrep_list dl;
	/*TwoPoint::Backend::Factory::set_default("buffer");	*/
	Utils::Tick charjeu_t0 = Utils::Tick::now();
	std::cout << "Loaded : " << Cartage.CharJeu(argv[1]) << std::endl;
	std::cerr << "... in " << (Utils::Tick::now()-charjeu_t0).seconds() << " seconds." << std::endl;
	BioJeu*bj = Cartage.Jeu[1];
#if 0
	CartaGene C2;
	C2.CharJeu(argv[1]);
	BioJeu*bj2 = C2.Jeu[1];
	for(int i=1;i<=bj->NbMarqueur;++i) {
		for(int j=1;j<=bj->NbMarqueur;++j) {
			if(bj->GetTwoPointsLOD(i, j)!=bj2->GetTwoPointsLOD(i, j)) {
				std::cerr<<"LOD "<<i<<','<<j<<std::endl;
			}
			if(bj->GetTwoPointsFR(i, j)!=bj2->GetTwoPointsFR(i, j)) {
				std::cerr<<"FR "<<i<<','<<j<<std::endl;
			}
			if(bj->GetTwoPointsLOD(i, j)!=bj->GetTwoPointsLOD(i, j)) {
				std::cerr<<"HAHA LOD "<<i<<','<<j<<std::endl;
			}
			if(bj->GetTwoPointsFR(i, j)!=bj->GetTwoPointsFR(i, j)) {
				std::cerr<<"HAHA FR "<<i<<','<<j<<std::endl;
			}
		}
	}
#endif
	two_point_matrix test(bj, bj->NbMarqueur, bj->NbMarqueur);
	/*for(int i=1;i<=bj->NbMarqueur;++i) {*/
		/*std::cout << std::setw(8) << i << ':' << ' ' << std::endl;*/
		/*for(int j=1;j<=bj->NbMarqueur;++j) {*/
			/*std::cout << std::setw(10) << test[i][j].LOD << ' ';*/
		/*}*/
	/*}*/
	int total_small_discrepancy=0;
	int total_big_discrepancy=0;
	Utils::ProgressDisplay pdisp(bj->NbMarqueur);
	matrix_compare mc(bj, test, total_small_discrepancy, total_big_discrepancy, pdisp, dl);
	Parallel::Range<Parallel::Raw>().execute_range(mc, 1, bj->NbMarqueur+1);
	std::cerr << " got "<<total_big_discrepancy<<" discrepancies that were >= 1.e-10 %" << std::endl;
	std::cerr << " got "<<total_small_discrepancy<<" discrepancies that were < 1.e-10 %" << std::endl;
	std::cerr << " recorded " << dl.size() << " distinct discrepancies" << std::endl;

	int lod_discrep = 0;
	int fr_discrep = 0;
	int recomputed = 0;

#if 0
	std::cerr << "now checking discrepancies..." << std::endl;
	std::list<std::pair<int,int> >::iterator i = dl.begin(), j=dl.end();
#	if 1
	while(i!=j) {
		twopoint_traits::data_type& d = test[(*i).first][(*i).second];
		double LOD=d.LOD;
		double FR=d.FR;
		twopoint_traits::clear_data(d);
		twopoint_traits::data_type& retry = test[(*i).first][(*i).second];
		double RLOD=retry.LOD;

		std::cerr << std::setprecision(20);
		if((retry.LOD!=LOD) || (retry.FR!=FR)) {
			++recomputed;
			/*std::cerr << LOD<<','<<FR<<" => "<<retry.LOD<<','<<retry.FR<<std::endl;*/
		}
		if(bj->GetTwoPointsLOD((*i).first, (*i).second)!=retry.LOD) {
			/*std::cerr << "  fixed LOD"<<std::endl;*/
			++lod_discrep;
		}
		if(bj->GetTwoPointsFR((*i).first, (*i).second)!=retry.FR) {
			/*std::cerr << "  fixed FR"<<std::endl;*/
			++fr_discrep;
		}
		++i;
	}
	std::cerr << "out of " << dl.size() << " recomputed values, " << recomputed << " were different" << std::endl;
	std::cerr << "there were still " << lod_discrep << " discrepancies in LOD values and " << fr_discrep << " in FR values" << std::endl;
	i = dl.begin();
#	endif
	while(i!=j) {
		twopoint_traits::data_type& d = test[(*i).first][(*i).second];
		double LOD = bj->GetTwoPointsLOD((*i).first, (*i).second);
		double FR = bj->GetTwoPointsFR((*i).first, (*i).second);
		/*if((LOD!=d.LOD) || (FR!=d.FR)) {*/
			std::cerr << std::dec << (*i).first << ',' << (*i).second << '\t' << test.make_offset((*i).first, (*i).second)
				<< "\t["<<XORDBL(LOD, d.LOD)
				<< "]\t["<<XORDBL(FR, d.FR) << ']' << std::endl;
		/*}*/
		++i;
	}
#endif
#ifdef DUMP_MATRICES
	for(int r=1;r<=bj->NbMarqueur;++r) {
		printf("%3d :", r);
		for(int c=1;c<=bj->NbMarqueur;++c) {
			printf("%5.2f ", test[r][c].LOD);
		}
		printf("\n");
	}
	for(int r=1;r<=bj->NbMarqueur;++r) {
		printf("%3d :", r);
		for(int c=1;c<=bj->NbMarqueur;++c) {
			printf("%5.2f ", test[r][c].FR);
		}
		printf("\n");
	}
#endif
}

#if 0

struct twopoint_traits_cond {
	typedef struct { double LOD; double FR; } data_type;
	typedef BioJeu* context_type;
	struct fence_resource_type {
		pthread_cond_t cond;
		fence_resource_type() {
			pthread_cond_init(&cond, NULL);
		}
		~fence_resource_type() {
			pthread_cond_destroy(&cond);
		}
	};
	struct fence_type {
		double fence;
		pthread_cond_t* cond;
	};
	struct fence_handler_type {
		bool is_fenced(data_type*d) {
			return ((fence_type*)d)->fence==fence_marker.d;
		}
		void wait(mutex_type*m, data_type*f) {
			pthread_cond_wait(((fence_type*)f)->cond, m->native_handle());
		}
		void fence(fence_resource_type&res, mutex_type*m, data_type*d) {
			fence_type*f = (fence_type*) d;
			f->fence = fence_marker.d;
			f->cond = &res.cond;
			m->unlock();
		}
		void release(fence_resource_type&res, mutex_type*m, data_type*d, data_type* newd) {
			m->lock();
			*d = *newd;
			pthread_cond_broadcast(&res.cond);
		}
	};
	class pagefile_name_provider_type {
		private:
			std::string n;
		public:
			pagefile_name_provider_type(context_type context) {
				try {
					BioJeuSingle*bj = dynamic_cast<BioJeuSingle*>(context);
					n=bj->NomJeu;
					n+=".2pt-test";
				} catch(const std::bad_cast& bc) {
					n="";
				}
			}
			operator const char*() const {
				return n=="" ? NULL : n.c_str();
			}
	};
	static const bool delete_pagefile_when_done = false;	/* we want to keep previously computed two-point data */
	struct computation_handler_type {
		void compute(context_type c, int i, int j, data_type*d) {
			d->LOD = c->ComputeOneTwoPoints(i, j, c->Epsilon2, &d->FR);
		}
		bool must_compute(context_type c, data_type*d) { return d->LOD==0&&d->FR==0; }
	};
	static void clear_data(data_type&d) { d.LOD=d.FR=0; }
};
#endif

/*typedef twopoint_traits_spin twopoint_traits;*/

#else
int main(int argc, char**argv) {
	std::cout << sizeof(void*) << std::endl;
	return 0;
}
#endif

