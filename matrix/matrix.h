#ifndef _MATRIX_H_
#define _MATRIX_H_


#if !(defined(SWAP_IS_FIFO)||defined(SWAP_IS_LRU))
#	warning Swap caching method unspecified. Using FIFO
#	define SWAP_IS_FIFO
#endif

#include <iostream>
#include <iomanip>

#include "base.h"
#include "data_source.h"
#include "addressing_space.h"
#include "backend_base.h"
#include "backend_hashcache.h"
#include "backend_swap.h"
/*#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
#	include "backend_switcher.h"
/*#endif*/


namespace matrix {
	using namespace data;
	using namespace addressing_space;

#if 0
	/** Selector for reference-to-value implementations, depending on the value of \param use_locking.
	 *
	 * When \param use_locking is false, equivalent to plain M::data_type&.
	 * When \param use_locking is true, creates an object behaving like M::data_type& that prevents cache invalidation for
	 * the concerned data address while the reference is in use.
	 */
	template <typename M, bool use_locking>
		struct data_reference_selector_impl;

	template <typename M>
		struct data_reference_selector_impl<M, true> {
			private:
				typename M::data_type& _;
				offset_type o;
				typename M::backend_type* b;
			public:
				typedef data_reference_selector_impl<M, true> type;
				data_reference_selector_impl(typename M::data_type&d,
										offset_type o_,
										typename M::backend_type*b_)
					: _(d), o(o_), b(b_)
				{ b->lock_data(o); }
				~data_reference_selector_impl() { b->unlock_data(o); }
				operator typename M::data_type() const { return _; }
				operator typename M::data_type&() { return _; }
				type& operator=(typename M::data_type&d) { _=d; return *this; }
				type& operator=(typename M::data_type d) { _=d; return *this; }
				static type
					create(	typename M::backend_type* b,
							offset_type o,
							typename M::data_type& d)
					{ return type(d,o,b); }
		};

	template <typename M>
		struct data_reference_selector_impl<M, false> {
			public:
				typedef typename M::data_type& type;
				static type
					create(	typename M::backend_type* b,
							offset_type o,
							typename M::data_type& d)
					{ return d; }
		};

	template <typename M>
		struct data_reference_selector
			: public data_reference_selector_impl<M, M::have_data_locking>
		{};
#endif

	/** Handle special cases depending on the addressing space of a matrix.
	 *
	 * Typically, TriangularWithoutMainDiag requires checking that row and column indices are different,
	 * whereas Rectangular and Triangular don't need any test before calling backend_get.
	 */
	template <typename M, typename A>
		struct configurable_fetcher_impl {
			static typename M::reference_type get(M*m, int i, int j) {
				/*std::cerr << "generic get(i,j)" << std::endl;*/
				return m->backend_get(i, j);
			}
			static const typename M::data_type const_get(M*m, int i, int j) {
				/*std::cerr << "generic const_get(i,j)" << std::endl;*/
				return m->backend_const_get(i, j);
			}
		};

	template <typename M, typename O>
		struct configurable_fetcher_impl< M, TriangularWithoutMainDiag<O> > {
			static const typename M::reference_type const_get(M*m, int i, int j) {
				static typename M::data_type nil;
				/*std::cerr << "triangularW/Omaindiag get(i,j)" << std::endl;*/
				if(i==j) {
					return typename M::reference_type();
				}
				return m->backend_get(i, j);
			}
			static typename M::reference_type get(M*m, int i, int j) {
				static typename M::data_type nil;
				/*std::cerr << "triangularW/Omaindiag get(i,j)" << std::endl;*/
				if(i==j) {
					return typename M::reference_type();
				}
				return m->backend_get(i, j);
			}
		};

	/** Front-end to configurable_fetcher_impl
	 */
	template <typename M>
		struct configurable_fetcher
			: public configurable_fetcher_impl
			  			<M, typename M::addressing_space> {};
	

	/** The main matrix class, parametrized by :
	 *  - the data traits,
	 *  - the addressing space (defaults to Rectangular<zero>),
	 *  - the synchronization traits (defaults to system defaults),
	 *  - the use of locking reference-to-value (defaults to no locking)
	 */
/*#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
/*#define backend_ backend_*/
/*#define B_ backend_->*/
	/*template <	typename DATA_TRAITS,*/
				/*typename ADDRESSING_SPACE = Rectangular<>,*/
				/*typename SYNC_TRAITS=sync::default_traits,*/
				/*bool use_locking_data_reference=false>*/
/*#else*/
#define backend_ this
#define B_ BACKEND::
	template <	typename DATA_TRAITS,
				typename ADDRESSING_SPACE = Rectangular<>,
				typename SYNC_TRAITS=sync::default_traits,
				bool use_locking_data_reference=false,
			 	typename BACKEND=backend::BackendSwitcher<DATA_TRAITS, SYNC_TRAITS> >
/*#endif*/
		class Matrix
			: public data::Provider<DATA_TRAITS, SYNC_TRAITS>,
			  public ADDRESSING_SPACE
/*#			ifndef CAN_SWITCH_BACKEND_AT_RUNTIME*/
			  , public BACKEND
/*#			endif*/

		{
			public:
				using Synchronizable<SYNC_TRAITS>::lock;
				using Synchronizable<SYNC_TRAITS>::unlock;
				typedef DATA_TRAITS data_traits;
				typedef SYNC_TRAITS sync_traits;
				typedef ADDRESSING_SPACE addressing_space;
				typedef typename ADDRESSING_SPACE::origin addressing_origin;
				typedef typename DATA_TRAITS::context_type context_type;
				typedef typename DATA_TRAITS::data_type data_type;
				static const bool have_data_locking=use_locking_data_reference;

/*#				ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
					/*typedef backend::Generic<	DATA_TRAITS,*/
												/*SYNC_TRAITS>*/
							/*backend_type;*/
/**/
					/*typedef Matrix<	DATA_TRAITS,*/
									/*ADDRESSING_SPACE,*/
									/*SYNC_TRAITS,*/
									/*use_locking_data_reference>*/
							/*matrix_type;*/
/*#				else*/
					typedef Matrix<	DATA_TRAITS,
									ADDRESSING_SPACE,
									SYNC_TRAITS,
									use_locking_data_reference,
									BACKEND>
							matrix_type;

					typedef BACKEND backend_type;
/*#				endif*/

				typedef
					typename backend_type::reference_type
					reference_type;

				struct row {
					private:
						matrix_type* _;
						int r;
					public:
						row(matrix_type* m, int r_) : _(m), r(r_) {}
						reference_type& operator[](int c) {
							return _->get(r, c);
						}
						const data type operator[](int c) const {
							return _->const_get(r, c);
						}
				};

				Matrix(context_type _, int r, int c)
					: Provider<DATA_TRAITS, SYNC_TRAITS>(_),
					  ADDRESSING_SPACE(r, c)
/*#					ifndef CAN_SWITCH_BACKEND_AT_RUNTIME*/
					  , BACKEND(_)
/*#					endif*/
				{
/*#					ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
						/*backend_ = create_default();*/
/*#					else*/
						B_ init(ADDRESSING_SPACE::get_storage_size());
/*#					endif*/
				}

				virtual ~Matrix() {
/*#					ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
						/*if(backend_) {*/
							/*delete backend_;*/
						/*}*/
/*#					endif*/
				}

				/* FIXME : race condition in { unlock(); return val; } */
				reference_type get(int i, int j) {
					return configurable_fetcher<matrix_type>::get(this, i, j);
				}

				const data_type const_get(int i, int j) const {
					return configurable_fetcher<matrix_type>::const_get(this, i, j);
				}

				void set(int i, int j, const data_type& d) {
					lock();
					data_type* dptr = B_ raw_get(ADDRESSING_SPACE::make_offset(i, j));
					*dptr = d;
					unlock();
				}

				row operator[](int r) { return row(this, r); }
				const row operator[](int r) const { return row(this, r); }

/*#				ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
					/*bool switch_backend(const char* backend_name, bool eager) {*/
						/*backend_type* b = backend::selector(this, backend_name);*/
						/*if(!b) {*/
							/*return false;*/
						/*}*/
						/*if(backend_) {*/
							/*if(eager) {*/
								/* TODO */
							/*}*/
							/*delete backend_;*/
						/*}*/
						/*backend_=b;*/
						/*return true;*/
					/*}*/
/*#				endif*/
			private:

/*#				ifdef CAN_SWITCH_BACKEND_AT_RUNTIME*/
					/*backend_type* backend_;*/
					/*backend_type* create_default() {*/
						/*//return backend::selector(this, "swap");*/
						/*return backend::selector(this, NULL);*/
					/*}*/
/*#				endif*/

				friend struct configurable_fetcher_impl<matrix_type, addressing_space>;

				const data_type backend_const_get(int i, int j) {
					return backend_->raw_const_get(ADDRESSING_SPACE::make_offset(i, j));
				}

				reference_type backend_get(int i, int j) {
					return backend_->raw_get(ADDRESSING_SPACE::make_offset(i, j));
#if 0
					/*lock();*/
					/*std::cerr << "get("<<i<<','<<j<<") @";*/
					offset_type ofs = ADDRESSING_SPACE::make_offset(i, j);
					data_type* d = B_ raw_get(ofs);
					/*std::cerr<<ofs<< " ("<<i<<','<<j<<')';*/
					/*unlock();*/
					check(i, j, d);
					reference_type ret =
						data_reference_selector<matrix_type>
						::create(backend_, ofs, *d);
					/*std::cerr <<  ((data_type)ret) << std::endl;*/
					return ret;
#endif
				}

		};
}

#endif

