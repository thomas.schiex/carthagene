#ifndef _MATRIX_RORW_BACKEND_H_
#define _MATRIX_RORW_BACKEND_H_

#include <iostream>
#include <map>
#include "base.h"
#include "data_source.h"
#include "addressing_space.h"
//#include "cell_lock.h"

namespace matrix {
	namespace middleware {
		using addressing_space::offset_type;

		struct cell_lock {
			/*template <typename X> void lock_cell(X x) {}*/
			/*template <typename X> void unlock_cell(X x) {}*/
		}; /* dummy type cuz I wanna deactivate that locking sh*t */

		/*! Middleware for matrices of computed values.
		 *
		 * Provides read-only values. Values are only computed once on demand.
		 */
		template <typename DATA_TRAITS, typename ADDRESSING_SPACE, typename STORAGE_TYPE>
		class readonly : public STORAGE_TYPE::template storage_type<DATA_TRAITS>::impl, ADDRESSING_SPACE {
		public:
			typedef readonly<DATA_TRAITS, ADDRESSING_SPACE, STORAGE_TYPE> middleware_type;
			typedef typename DATA_TRAITS::computation_handler_type computation_handler_type;
			typedef typename DATA_TRAITS::context_type context_type;
			typedef typename DATA_TRAITS::data_type data_type;
			typedef const data_type reference_type;
			typedef const data_type value_type;
			typedef typename STORAGE_TYPE::template storage_type<DATA_TRAITS>::impl storage_type;
			readonly(context_type c, offset_type rows, offset_type columns)
				: storage_type(c), ADDRESSING_SPACE(rows, columns), /*locks(),*/ context(c)
			{
				storage_type::init(ADDRESSING_SPACE::get_storage_size());
			}
			virtual ~readonly() {
				/*std::cerr << "Destroying middleware instance" << std::endl;*/
			}
			data_type __getter(offset_type i, offset_type j) {
				offset_type ofs = ADDRESSING_SPACE::make_offset(i, j);
				/*locks.lock_cell(ofs);*/
				data_type v = storage_type::read(ofs);
				if(computation_handler_type::must_compute(context, v)) {
					computation_handler_type::compute(context, i, j, v);
					storage_type::write(ofs, v);
				}
				/*locks.unlock_cell(ofs);*/
				return v;
			}
			reference_type raw_get(offset_type i, offset_type j) {
				return __getter(i, j);
			}
			value_type raw_const_get(offset_type i, offset_type j) {
				return __getter(i, j);
			}
		private:
			/*cell_lock locks;*/
			context_type context;
		};

		/*! Middleware for matrices of unspecified values.
		 *
		 * Provides read/write access to values, with locking around the life
		 * of reference-to-value objects, to ensure synchronization.
		 */
		template <typename DATA_TRAITS, typename ADDRESSING_SPACE, typename STORAGE_TYPE>
		class readwrite : public STORAGE_TYPE::template storage_type<DATA_TRAITS>::impl, ADDRESSING_SPACE {
		public:
			typedef readwrite<DATA_TRAITS, ADDRESSING_SPACE, STORAGE_TYPE> middleware_type;
			typedef typename DATA_TRAITS::computation_handler_type computation_handler_type;
			typedef typename DATA_TRAITS::context_type context_type;
			typedef typename DATA_TRAITS::data_type data_type;
			typedef readwrite<DATA_TRAITS, ADDRESSING_SPACE, STORAGE_TYPE> backend_type;
			typedef const data_type value_type;
			typedef typename STORAGE_TYPE::template storage_type<DATA_TRAITS>::impl storage_type;
			struct reference_type {
				backend_type* b;
				offset_type ofs;
				reference_type(backend_type*b_, offset_type o)
					: b(b_), ofs(o)
				{}
				reference_type& operator=(const data_type& dt) { b->write(ofs, dt); return *this; }
				~reference_type() {}
				operator const data_type () const { return b->read(ofs); }
			};
			readwrite(context_type c, offset_type rows, offset_type columns)
				: storage_type(c), ADDRESSING_SPACE(rows, columns), /*locks(),*/ context(c)
			{
				storage_type::init(ADDRESSING_SPACE::get_storage_size());
			}
			virtual ~readwrite() {}
			reference_type raw_get(offset_type i, offset_type j) {
				offset_type ofs = ADDRESSING_SPACE::make_offset(i, j);
				/*locks.lock_cell(ofs);*/
				return reference_type(this, ofs);
			}
			value_type raw_const_get(offset_type i, offset_type j) {
				offset_type ofs = ADDRESSING_SPACE::make_offset(i, j);
				/*locks.lock_cell(ofs);*/
				value_type v = storage_type::read(ofs);
				/*locks.unlock_cell(ofs);*/
				return v;
			}
		private:
			cell_lock locks;
			context_type context;
			friend struct reference_type;
		};

		template <typename CHT>
			struct middleware_impl_selector {
				template <typename D, typename A, typename S> struct middleware_impl : public readwrite<D, A, S> {
					typedef readonly<D, A, S> middleware_type;
				};
			};

		template <>
			struct middleware_impl_selector<void> {
				template <typename D, typename A, typename S> struct middleware_impl : public readwrite<D, A, S> {
					typedef readwrite<D, A, S> middleware_type;
				};
			};

		template <typename D, typename A, typename S, typename CHT>
			struct selector_impl : public readonly<D, A, S> {
				typedef readonly<D, A, S> middleware_type;
				typedef typename middleware_type::storage_type storage_type;
			};
		template <typename D, typename A, typename S>
			struct selector_impl<D, A, S, void> : public readwrite<D, A, S> {
				typedef readwrite<D, A, S> middleware_type;
				typedef typename middleware_type::storage_type storage_type;
			};
		template <typename D, typename A, typename S>
			struct selector : public selector_impl<D, A, S, typename D::computation_handler_type>
			{
				typedef typename selector_impl<D, A, S, typename D::computation_handler_type>::middleware_type middleware_type;
				typedef typename middleware_type::storage_type storage_type;
			};
	}
}

#endif

