#ifndef _MATRIX_H_
#define _MATRIX_H_


#if !(defined(SWAP_IS_FIFO)||defined(SWAP_IS_LRU))
#	warning Swap caching method unspecified. Using FIFO
#	define SWAP_IS_FIFO
#endif

#include <iostream>
#include <iomanip>

#include "base.h"
#include "data_source.h"
#include "addressing_space.h"
#include "backend_base.h"
#include "backend_hashcache.h"
#include "backend_swap.h"
#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
#	include "backend_selector.h"
#endif


#include <hash_map>
//#include <unordered_map>
#include <vector>


namespace matrix {
	using namespace data;
	using namespace addressing_space;

	template <typename K>
		class DimView {
			std::hash_map<K, int> by_key;
			std::vector<K> by_index;
			class iterator;
			void swap(K k1, K k2);
			void remove(K k);
			void add(K k, int idx);
			iterator begin() const;
			iterator end() const;
		};

	template <typename DATA_TRAITS, typename ROW_KEY, typename COL_KEY>
		class MatrixView {
			private:
				struct mview_backend_base {
					virtual ~mview_backend_base() {}
					virtual typename DATA_TRAITS::reference_type get(ROW_KEY, COL_KEY) = 0;
				};
				template <typename M>
					struct mview_backend : public mview_backend_base {
						M* m;
						mview_backend_base(M*_) : m(_) {}
						typename M::reference_type get(int r, int c) {
							return m->get(r, c);
						}
					};
				mview_backend_base* mvb;
			public:
				DimView<ROW_KEY> rows;
				DimView<COL_KEY> columns;
				typename M::reference_type get(ROW_KEY r, COL_KEY c) {
					return mvb->get(rows[r], cols[c]);
				}
		};

	/** The main matrix class, parametrized by :
	 *  - the data traits,
	 *  - the addressing space (defaults to Rectangular<zero>),
	 *  - the synchronization traits (defaults to system defaults),
	 *  - the use of locking reference-to-value (defaults to no locking)
	 */
#ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
#define backend_ backend_
#define B_ backend_->
	template <	typename DATA_TRAITS,
				typename ADDRESSING_SPACE = Rectangular<>,
				typename SYNC_TRAITS=sync::default_traits,
				bool use_locking_data_reference=false>
#else
#define backend_ this
#define B_ BACKEND::
	template <	typename DATA_TRAITS,
			 	typename BACKEND,
				typename ADDRESSING_SPACE = Rectangular<>,
				typename SYNC_TRAITS=sync::default_traits,
				bool use_locking_data_reference=false>
#endif
		class Matrix
			: public data::Provider<DATA_TRAITS, SYNC_TRAITS>,
#			ifndef CAN_SWITCH_BACKEND_AT_RUNTIME
			  public BACKEND,
#			endif
			  public ADDRESSING_SPACE

		{
			public:
				using Synchronizable<SYNC_TRAITS>::lock;
				using Synchronizable<SYNC_TRAITS>::unlock;
				typedef DATA_TRAITS data_traits;
				typedef SYNC_TRAITS sync_traits;
				typedef ADDRESSING_SPACE addressing_space;
				typedef typename ADDRESSING_SPACE::origin addressing_origin;
				typedef typename DATA_TRAITS::context_type context_type;
				typedef typename DATA_TRAITS::data_type data_type;
				static const bool have_data_locking=use_locking_data_reference;

#				ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
					typedef backend::Generic<	DATA_TRAITS,
												SYNC_TRAITS>
							backend_type;

					typedef Matrix<	DATA_TRAITS,
									ADDRESSING_SPACE,
									SYNC_TRAITS,
									use_locking_data_reference>
							matrix_type;
#				else
					typedef Matrix<	DATA_TRAITS,
									BACKEND,
									ADDRESSING_SPACE,
									SYNC_TRAITS,
									use_locking_data_reference>
							matrix_type;

					typedef matrix_type backend_type;
#				endif

				typedef typename
					data_reference_selector<	matrix_type,
												use_locking_data_reference>
					::type reference_type;

				struct row {
					private:
						matrix_type* _;
						int r;
					public:
						row(matrix_type* m, int r_) : _(m), r(r_) {}
						data_type& operator[](int c) {
							return _->get(r, c);
						}
						const data_type& operator[](int c) const {
							return _->get(r, c);
						}
				};

				Matrix(context_type _, int r, int c)
					: Provider<DATA_TRAITS, SYNC_TRAITS>(_)
#					ifndef CAN_SWITCH_BACKEND_AT_RUNTIME
					  , BACKEND(_)
#					endif
				{
					ADDRESSING_SPACE::set_dimension(r, c);
#					ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
						backend_ = create_default();
#					else
						B_ init(ADDRESSING_SPACE::get_storage_size());
#					endif
				}

				~Matrix() {
#					ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
						if(backend_) {
							delete backend_;
						}
#					endif
				}

				/* FIXME : race condition in { unlock(); return val; } */
				reference_type get(int i, int j) {
					return configurable_fetcher<matrix_type>::get(this, i, j);
				}

				const reference_type get(int i, int j) const {
					return configurable_fetcher<matrix_type>::const_get(this, i, j);
				}

				void set(int i, int j, const data_type& d) {
					lock();
					data_type* dptr = B_ raw_get(ADDRESSING_SPACE::make_offset(i, j));
					*dptr = d;
					unlock();
				}

				row operator[](int r) { return row(this, r); }
				const row operator[](int r) const { return row(this, r); }

#				ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
					bool switch_backend(const char* backend_name, bool eager) {
						backend_type* b = backend::selector(this, backend_name);
						if(!b) {
							return false;
						}
						if(backend_) {
							if(eager) {
								/* TODO */
							}
							delete backend_;
						}
						backend_=b;
						return true;
					}
#				endif
			private:

#				ifdef CAN_SWITCH_BACKEND_AT_RUNTIME
					backend_type* backend_;
					backend_type* create_default() {
						/*return backend::selector(this, "swap");*/
						return backend::selector(this, NULL);
					}
#				endif

				friend struct configurable_fetcher_impl<matrix_type, addressing_space>;

				reference_type backend_get(int i, int j) {
					/*lock();*/
					/*std::cerr << "get("<<i<<','<<j<<") @";*/
					offset_type ofs = ADDRESSING_SPACE::make_offset(i, j);
					data_type* d = B_ raw_get(ofs);
					/*std::cerr<<ofs<< " ("<<i<<','<<j<<')'<<std::endl;*/
					/*unlock();*/
					check(i, j, d);
					reference_type ret =
						data_reference_selector<matrix_type,
												have_data_locking>
						::create(backend_, ofs, *d);
					return ret;
				}

		};
}

#endif

