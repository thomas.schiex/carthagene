#ifndef _MATRIX_BACKEND_SWAP_FIFO_H_
#define _MATRIX_BACKEND_SWAP_FIFO_H_

namespace matrix {
	namespace backend {
		namespace impl {
			class Page {
				private:
					offset_type slots_in_use;
					void* buffer;
					CacheFile* f;
					offset_type offset;
					int do_sync_counter;
					/*sync_traits::mutex_type mutex;*/
				public:
					Page()
					  : slots_in_use(0), buffer(0), f(0), offset(0)
					  /*, mutex()*/
					{}
					bool isMapped() const { return !!buffer; }
					bool isSwapped() const { return !buffer; }

					void init(CacheFile*f_, offset_type o) {
						f=f_;
						offset=o;
						slots_in_use=0;
					}
					void* operator *() const { return buffer; }
					bool can_unmap() const { return !slots_in_use; }
					bool unmap() {
						/*mutex.lock();*/
						if(!can_unmap()) {
							std::cerr << "[unmap] " << this << ' ' << buffer << " +"
								<< offset << " forbidden (lock=" << slots_in_use << ')' << std::endl;
							return false;
						}
						/*++do_sync_counter;*/
						/*do_sync_counter &= 0x1;*/
						msync(buffer, pagesize, MS_SYNC);
						int ret;
						/*if((ret=madvise(buffer, pagesize, MADV_DONTNEED))) {*/
							/*std::cerr << "madvise failed : " << strerror(ret) << std::endl;*/
						/*}*/
						if((ret=munmap(buffer, pagesize))) {
							std::cerr << "munmap failed : " << strerror(ret) << std::endl;
						}
						/*std::cerr << "[unmap] " << this << ' ' << buffer << " +" << offset << std::endl;*/
						buffer=NULL;
						/*mutex.unlock();*/
						return true;
					}
					void lock_slot() {
						++slots_in_use;
						/*std::cerr << "lock slot("<<slots_in_use<<')'<<std::endl;*/
					}	/* FIXME : all that need sync */
					void unlock_slot() {
						--slots_in_use;
						/*std::cerr << "unlock slot("<<slots_in_use<<')'<<std::endl;*/
					}
					void* map() {
						/*mutex.lock();*/
						buffer = f->map_page(offset);
						/*std::cerr << "[  map] " << buffer << " +" << offset << std::endl;*/
						/*mutex.unlock();*/
						return buffer;
					}
					~Page() {
						/*if(buffer) {*/
							/*unmap();*/
						/*}*/
					}
			};

			class PageCache {
				private:
					/*std::vector<Page*> fifo;*/
					std::list<Page*> fifo;
					/*size_t cursor;*/
					sync::default_traits::spin_type spin;
					sync::default_traits::mutex_type mutex;

					bool unmap_one() {
						/*Page* p;*/
						std::list<Page*>::iterator i, j;
						for(i=fifo.begin(), j=fifo.end();i!=j&&!(*i)->unmap();++i);
						if(i==j) {
							std::cout << "couldn't unmap any of the " << fifo.size() << " currently mapped pages" << std::endl;
							return false;
						}
						/*fifo.remove(*i);*/
						fifo.erase(i);
						return true;
						
						/*size_t cursor_mustnt_cycle = cursor;*/
						/*if(fifo[cursor]&&!fifo[cursor]->unmap()) {*/
							/*do {*/
								/*++cursor %= max_page_count;*/
							/*} while(cursor!=cursor_mustnt_cycle*/
									/*&&*/
									/*fifo[cursor]*/
									/*&&*/
									/*!fifo[cursor]->unmap());*/
						/*}*/
						/*return cursor!=cursor_mustnt_cycle;*/
					}

					void new_page(Page*p) {
						fifo.push_back(p);
#if 0
						if(fifo[cursor]!=p) {
							unmap_one();
							/*std::cerr << "new_page("<<p<<") overwrites "<<fifo[cursor]<<std::endl;*/
							fifo[cursor]=p;
							++cursor %= max_page_count;
						}
#endif
					}
					PageCache() : fifo(), /*cursor(0),*/ spin(), mutex(), stats() {
						stats.set_size(max_page_count);
						fifo.clear();
						/*fifo.reserve(max_page_count);*/
						/*fifo.resize(max_page_count);*/
					}
					PageCache(const PageCache&) {}
					friend class utils::Singleton<PageCache>;
				public:
					CacheStats stats;
					/*static PageCache* create() {*/
						/*static PageCache*instance=NULL;*/
						/*if(!instance) {*/
							/*instance = new PageCache();*/
						/*}*/
						/*return instance;*/
					/*}*/
					void* access_page(Page*p) {
						/*std::cerr << "access_page(" << p << ')' << std::endl;*/
						spin.lock();	/* fast */
						if(p->isMapped()) {
							/*stats.hit();*/
							void* ret = **p;
							/*p->lock_slot();*/	/* prevent race condition in advance */
							spin.unlock();	/* fast */
							return ret;
						}
						mutex.lock();
						spin.unlock();	/* fast */
						new_page(p);
						/*p->lock_slot();*/	/* prevent race condition in advance */
						/*stats.miss();*/
						void*ret;
						while((!(ret = p->map())) && unmap_one()); 
						
						mutex.unlock();
						return ret;
					}
					void remove_page(Page*p) {
						mutex.lock();
						std::list<Page*>::iterator i, j, k=fifo.end();
						for(i=fifo.begin(), j=fifo.end();i!=j;++i) {
							if(*i==p) {
								k=i;
							}
						}
						if(k!=j) {
							fifo.erase(k);
						}
						mutex.unlock();
						/*fifo.remove(p);*/
						/*for(size_t i=0;i<fifo.size();++i) {*/
							/*if(fifo[i]==p) { fifo[i]=NULL; }*/
						/*}*/
					}
			};
		}
	}
}

#endif

