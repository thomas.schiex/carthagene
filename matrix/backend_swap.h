#ifndef _MATRIX_BACKEND_SWAP_H_
#define _MATRIX_BACKEND_SWAP_H_

#include "backend_swap_base.h"

#if defined(SWAP_IS_LRU)
#	include "backend_swap_lru.h"
#elif defined(SWAP_IS_FIFO)
#	include "backend_swap_fifo.h"
#endif

namespace matrix {
	namespace backend {
		namespace impl {
			template<typename DATA_TRAITS, bool have_provider>
				struct init_f_and_name {
					init_f_and_name(typename DATA_TRAITS::context_type owner, offset_type size, CacheFile*& f, std::string& filename);
				};

			template<typename DATA_TRAITS>
				struct init_f_and_name<DATA_TRAITS, true> {
					init_f_and_name(typename DATA_TRAITS::context_type owner, offset_type size, CacheFile*& f, std::string& filename) {
						filename = typename DATA_TRAITS::pagefile_name_provider_type(owner);
						f = new impl::CacheFile(
									size*sizeof(typename DATA_TRAITS::data_type),
									filename.c_str(),
									DATA_TRAITS::delete_pagefile_when_done);
					}
				};

			template<typename DATA_TRAITS>
				struct init_f_and_name<DATA_TRAITS, false> {
					init_f_and_name(typename DATA_TRAITS::context_type owner, offset_type size, CacheFile*& f, std::string& filename) {
						f = new impl::CacheFile(
									size*sizeof(typename DATA_TRAITS::data_type),
									NULL,
									DATA_TRAITS::delete_pagefile_when_done);
						filename = f->filename();
					}
				};
		}

		template <typename DATA_TRAITS, typename SYNC_TRAITS>
			class Swap BACKEND_INHERITANCE {
				private:
					typename DATA_TRAITS::context_type owner;
					impl::CacheFile* f;
					std::string filename;
					std::vector<impl::Page> pages;
					impl::PageCache* pagecache;
					typename SYNC_TRAITS::spin_type spin;
					typedef sync::scope_lock<typename SYNC_TRAITS::spin_type> spin_scope_lock;
				public:
					typedef full_storage storage_type;
					typedef typename DATA_TRAITS::data_type data_type;
					typedef typename DATA_TRAITS::pagefile_name_provider_type pagefile_name_provider_type;
					static const bool have_pagefile_name_provider
						= constraints::check_not_void<pagefile_name_provider_type>::value;

					Swap(typename DATA_TRAITS::context_type _)
						: owner(_), f(0), filename(), pages(), pagecache(utils::Singleton<impl::PageCache>::instance()), spin()
					{}
					~Swap() {
						/*std::cerr << "~Swap(" << filename << ')' << std::endl;*/
						if(f) {
							delete f;
						}
						std::vector<impl::Page>::iterator i=pages.begin(), j=pages.end();
						while(i!=j) {
							impl::Page* p = &*i;
							if(p->isMapped()) {
								pagecache->remove_page(p);
							}
							++i;
						}
						pages.clear();
					}

					void init(offset_type size) {
						/*std::cout << "init disk cache" << std::endl;*/
						if(f) {
							delete f;
						}
						impl::init_f_and_name<	DATA_TRAITS,
												have_pagefile_name_provider>
							(owner, size, f, filename);
						/*if(have_pagefile_name_provider) {
							filename = pagefile_name_provider_type(owner);
							f = new impl::CacheFile(
										size*sizeof(data_type),
										filename.c_str(),
										DATA_TRAITS::delete_pagefile_when_done);
						} else {
							f = new impl::CacheFile(
										size*sizeof(data_type),
										NULL,
										DATA_TRAITS::delete_pagefile_when_done);
							filename = f->filename();
						}*/
						pages.resize(f->count_pages()+1);
						/*std::cerr << "pagefile has " << pages.size() << " pages" << std::endl;*/
						std::vector<impl::Page>::iterator
							begin=pages.begin(),
							end=pages.end();
						offset_type ofs=0;
						while(begin!=end) {
							(*begin).init(f, ofs);
							ofs+=impl::pagesize;
							++begin;
						}
						/*std::cerr << "pages bloat amounts to " << (pages.size()*sizeof(impl::Page)/1048576.) << " MB" << std::endl;*/
					}

					data_type* raw_get(offset_type o) {
						/*spin_scope_lock ssl(spin);*/
						/* FIXME dirty c/p from get_datum to gain access to page object */
						offset_type ofs = o*sizeof(data_type);
						offset_type pagenum = ofs/impl::pagesize;
						offset_type dataofs = ofs%impl::pagesize;
						impl::Page* page = &pages[pagenum];
						return (data_type*) (((char*)pagecache->access_page(page))+dataofs);
					}

					void lock_data(offset_type o) {
						spin_scope_lock ssl(spin);
						offset_type ofs = o*sizeof(data_type);
						offset_type pagenum = ofs/impl::pagesize;
						/*std::cerr << "lock slot " << o << std::endl;*/
						pages[pagenum].lock_slot();
					}
					void unlock_data(offset_type o) {
						spin_scope_lock ssl(spin);
						offset_type ofs = o*sizeof(data_type);
						offset_type pagenum = ofs/impl::pagesize;
						/*std::cerr << "unlock slot " << o << std::endl;*/
						pages[pagenum].unlock_slot();
					}
			};
	}
}

#endif

