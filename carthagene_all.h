#ifndef _CG_ALL_INCLUDES
#define _CG_ALL_INCLUDES

// Ensure that libc uses 64-bit file offsets and defines the *64 routines
#define _LARGEFILE64_SOURCE
#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#endif

#include "config.h"
#include "parallel/parallel.h"
#include "calcul/BioJeu.h"
#include "calcul/twopoint.h"
/*#include "matrix/matrix_new.h"*/
#include "calcul/ladj.h"
#include "calcul/Marker.h"
#include "calcul/AlgoGen.h"
#include "calcul/BioJeuMerged.h"
#include "calcul/BioJeuSingle.h"
#include "calcul/BJM_GE.h"
#include "calcul/BJM_OR.h"
#include "calcul/BJS_BC.h"
#include "calcul/BJS_BS.h"
#include "calcul/BJS_CO.h"
#include "calcul/BJS_IC_parallel.h"
#include "calcul/BJS_OR.h"
#include "calcul/BJS_RHD.h"
#include "calcul/BJS_RHE.h"
#include "calcul/BJS_RH.h"
#include "calcul/CartaGene.h"
#include "calcul/Carte.h"
#include "calcul/CGtypes.h"
#include "calcul/Constraints.h"
#include "calcul/Genetic.h"
/*#include "calcul/Greedy.h"*/
#include "calcul/KeyList.h"
#include "calcul/NOrComp.h"
#include "calcul/NOrCompMulti.h"
#ifdef _WITH_GMP
#  include <gmp.h>
#else
#  include "calcul/packedarray.h"
#endif
#include "calcul/QArrayUtils.h"
#include "calcul/QMatingOperator.h"
#include "calcul/QPolynomial.h"
#include "calcul/QPolynomialMatrix.h"
#include "calcul/smartkey.h"
#include "calcul/sync_traits.h"
#include "calcul/System.h"
#include "calcul/Tas.h"
#include "calcul/tsp.h"
#include "calcul/read_data.h"
#include "cgsh/cgsh.h"
#include "LKH/Hashing.h"
#include "LKH/Heap.h"
#include "LKH/LK.h"
#include "LKH/LKH.h"
#include "LKH/Segment.h"
#endif

