file copy -force cgwopt ~
file copy -force cgwdefalgo ~
file copy -force carthagenerc.tcl ~

set fid [open ~/carthagenerc.tcl r 0666]
set buff [read $fid]
close $fid

puts [pwd]

set buff2 ""
append  buff2 "lappend auto_path \"[pwd]\"\n" $buff

file attributes  ~/carthagenerc.tcl -readonly 0
set fid [open ~/carthagenerc.tcl RDWR]
puts $fid $buff2
close $fid
exit
