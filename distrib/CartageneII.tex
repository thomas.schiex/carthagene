\documentclass{article}

\usepackage{garamond}
\usepackage{a4}
\usepackage[dvips]{graphicx}
\usepackage{amssymb}
\usepackage{euler}

%---------------------------

\parindent 0pt
\parskip 4pt plus 2pt minus 2pt

\author{T. Schiex}
\title{Carthag{\`e}ne}
\date{June 1998}

\begin{document}
\maketitle

\section{Outbreds and Carthag{\`e}ne}

Carthag{\`e}ne can handle outbred data as far as phases are fixed (either known
or fixed to the most probable phases). Because this ability has evolved from 
a classical intercross situation using Mapmaker syntax, the syntax used to
encode such data is currently rather clumsy. This is expected to change one
of these days but you'd better not count on it\ldots

At one locus, consider the cross of $F_0|F_1 \times M_0|M_1$ where $F_0$,
$F_1$, $M_0$ and $M_1$ stand for the alleles on each haplotype of the father 
and the mother respectively. The genotype of the child obtained may be
either $F_0|M_0$, $F_1|M_0$, $F_0|M_1$ or $F_1|M_1$. Depending on the
heterozygocity of the parents, or the number of different alleles, on the
dominance or codominance of the markers, on the observations available on the 
child's phenotype, only a subset of these 4 possibilities is possible.
For example, in the ``usual'' F2 intercross situation, the two parents are
heterozygous and bear the same pair of alleles: $F_0 = M_0 =A$ and $F_1 =
M_1 =B$. In this simple case, observations on the phenotype of a child may
lead to different situations:
\begin{itemize}
\item the child is typed $A$ and the allele $A$ is not dominant. In this
  case the only possible genotype is $A|A$ or $M_0|F_0$. This is 
  encoded by the character \texttt{A} in MapMaker.
\item the child is typed $A$ and $A$ is dominant.  Then the set of possible
  genotypes is $\{A|A, A|B, B|A\}$, i.e, $\{F_0|M_0, F_0|M_1, F_1|M_0\}$.
  This is encoded by the character \texttt{D} in MapMaker.
\item the child is typed $B$ and the allele $B$ is not dominant. In this
  case the only possible genotype is $B|B$ or $M_1|F_1$.This is
  encoded by the character \texttt{B} in MapMaker.
\item the child is typed $B$ and $B$ is dominant.  Then the set of possible
  genotypes is $\{A|B, B|A, B|B\}$, i.e, $\{F_0|M_1, F_1|M_0, F_1,M_1\}$.
  This is encoded by the character \texttt{C} in MapMaker.
\item the child is typed $AB$ (the marker is codominant) then the set of
  possible genotypes is $\{A|B, B|A\}$, i.e, $\{F_0|M_1, F_1|M_0\}$. This
  is encoded by the character \texttt{H} in MapMaker.
\item the child is untyped at this locus. The set of possible genotypes
  includes all possible genotypes i.e $\{F_0|M_0, F_0|M_1, F_1|M_0,
  F_1,M_1\}$.  This is encoded by the character \texttt{-} in MapMaker.
\end{itemize}

In order to be able to cope with all phases known situations, including
cases where one parent is homozygous, when 3 or 4 different alleles are
present, Carthag{\`e}ne actually enables the user to express any subset of the 4
different possibilities. In order to do so, these 4 possibilities are
associated with a number:

\begin{itemize}
\item genotype $F_0|M_0$ is associated with 1
\item genotype $F_0|M_1$ is associated with 2
\item genotype $F_1|M_0$ is associated with 4
\item genotype $F_1|M_1$ is associated with 8
\end{itemize}

and the user will be able to tell Carthag{\`e}ne which set of genotype is
actually possible at a given locus by:
\begin{itemize}
\item adding up the numbers associated with each possible genotypes at the
  locus given the observations
\item converting this number to hexadecimal (base 16 where one counts
  1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f).
\end{itemize}

The following tables recapitulates all possible codes from \texttt{1} to
\texttt{f} and the corresponding set of possible genotypes at the locus.
\begin{center}
\begin{tabular}{cccc}
Notation & Synonym & Possible Genotypes \\\hline\hline
\texttt{1}& \texttt{A} & $F_0|M_0$\\\hline
\texttt{2}&   & $F_0|M_1$ \\\hline
\texttt{3}&   & $F_0|M_0$, $F_0|M_1$ \\\hline
\texttt{4}&   & $F_1|M_0$ \\\hline
\texttt{5}&   & $F_0|M_0$, $F_1|M_0$ \\\hline
\texttt{6}& \texttt{H} & $F_0|M_1$, $F_1|M_0$ \\\hline
\texttt{7}& \texttt{D} & $F_0|M_0, F_0|M_1$, $F_1|M_0$\\\hline
\texttt{8}& \texttt{B} & $F_1|M_1$ \\\hline
\texttt{9}&   & $F_0|M_0$, $F_1|M_1$ \\\hline
\texttt{a}&   & $F_0|M_1$, $F_1|M_1$ \\\hline
\texttt{b}&   & $F_0|M_0$, $F_0|M_1$, $F_1|M_1$\\\hline
\texttt{c}&   & $F_1|M_0$, $F_1|M_1$ \\\hline
\texttt{d}&   & $F_0|M_0$, $F_1|M_0$, $F_1|M_1$ \\\hline
\texttt{e}& \texttt{C} & $F_0|M_1$, $F_1|M_0$, $F_1|M_1$ \\\hline
\texttt{f}& \texttt{-} & $F_0|M_0$, $F_0|M_1$, $F_1|M_0$, $F_1|M_1$ \\\hline
\end{tabular}
\end{center}

Let see how this can be used in some practical phase-known outbred
situations according to the segregation ratio of the marker at the current
locus.

\paragraph{Segregation ratio 1:2:1}

This is the usual case in F2 intercross with codominance. In this case, the
parents $F_0|F_1$ and $M_0|M_1$ are such that typically $F_0 = M_0 = A$ and
$F_1 = M_1 = B$ and $AB$ codominate. The usual observations on the child are
either phenotype $A$, $B$ or $AB$. this lead to the following encoding:

\begin{itemize}
\item $A$: The only possible genotype is $A|A = M_0|F_0$: this is coded by
  \texttt{1} (the synonym \texttt{A} used in MapMaker can also be used).
\item $B$: The only possible genotype is $B|B= M_1|F_1$: this is coded by
  \texttt{8} (the synonym \texttt{B} in MapMaker can also be used).
\item $AB$: The only possible genotypes are $A|B = M_0|F_1$ and $B|A =
  M_1|F_0$: this is coded by \texttt{6}=4+2.  The synonym \texttt{H} used in
  MapMaker can also be used.
\end{itemize}

When missing data occurs, it is still possible to give partial information
to Carthag{\`e}ne. Eg., if the child is typed $A$ but the other allele is not
known. The child's genotype can be either $A|A = M_0|F_0$ or $A|B = M_0|F_1$
or $B|A = M_1|F_0$.  This is encoded by the character \texttt{7}=1+2+4
(synonym \texttt{D})

The character \texttt{e} = 14 = 8+4+2 (synonym \texttt{C}) encodes a
situation where the child has been typed $B$ but the other allele is not
known. In this case, the child's genotype can be $B|B = F_1|M_1$ or $A|B =
F_0|M_1$ or $B|A = F_1|M_0$.

\paragraph{Segregation ratio 3:1}

This type of segregation ratio occurs when dominance appears. Imagine $A$
dominates $B$, then it is impossible to distinguish between $A|A$, $A|B$ and
$B|A$. Precisely, if the child is typed $B$, then the character $8$ (or the
synonym \texttt{B} of MapMaker) will be used. Else the character \texttt{7}
= 1+2+4 (or the synonym \texttt{D} of MapMaker) will be used to represent
the fact that we simply know that the child genotype is either $A|A =
F_0|M_0$, $A|B =F_0|M_1$ or $B|A = F_1|M_0$.

Conversely, if $B$ dominates $A$, the code \texttt{1} (resp. \texttt{e})
will be used if the child is typed $A$ (resp. $B$). The respective synonyms
\texttt{A} and \texttt{C} of MapMaker can also be used.

\paragraph{Segregation ratio 1:1:1:1}

This occurs when different alleles appear in the father and in the mother.
For example in $A|B \times C|D$. In this case the child is either $A|C$ or
$A|D$ or $B|C$ or $B|D$ and the corresponding codes are respectively
\texttt{1} (or \texttt{A}), \texttt{2}, \texttt{4} and \texttt{8} (or
\texttt{B}).

When some data is missing, it is still possible to give information to
Carthag{\`e}ne. Imagine for example that the child is typed $A$ then the second
allele is unknown. Because we have 4 different alleles, we know for sure
that the first allele of the children is $A$. Therefore, the only possible
genotypes are $A|C$ (code \texttt{1}) or $A|D$ (code \texttt{2}) and the
corresponding code is \texttt{3}= 1+2.

Imagine that instead of having 4 alleles, we have 3 alleles in $A|B \times
A|C$. Then children may be $A|A$, $A|C$, $B|A$ or $B|C$ i.e., there is still
a 1:1:1:1 segregation ratio. If again the children is just typed $A$ then
there is more indetermination at hand: the child may be either $A|A$ (code
\texttt{1}), $A|C$ (\texttt{2}) or $B|A$ (code \texttt{4}). In this case, the
corresponding code is \texttt{7} = 1+2+4 (or the synonym \texttt{D}).

\paragraph{Segregation ratio 1:1}

Imagine the second parent is homozygous at current locus i.e., we cross
$A|B$ with $C|C$ (a backcross like situation), then the children genotypes
may either be $A|C$ or $B|C$ If we observe $A|C$ (or $A$ simply), it is not
know where does the $C$ come from i.e, the children may be $A|C_0$ or
$A|C_1$.  This case is encoded by \texttt{3} = 1+2.  This is the sum of
\texttt{1} and \texttt{2} which corresponds to the two possible cases: $A|C$
with $C$ coming from one grand-parent (code \texttt{1}) or the other (code
\texttt{2}).

Similarly, if we observe $B$, then we know that the first allele is $B$, the
second is $C$ but we don't where this allele comes from. The code will be
\texttt{c} = 12 = 4+8.

If the homozygocity appears on the first parent ($A|A \times B|C$) instead
of the second one and if we observe $B$ we get the code \texttt{5}, the sum
of \texttt{1} and \texttt{4} corresponding to the fact that the origin of
the first allele is unknown. If we observe $C$, we get the code \texttt{a}
which in hexadecimal corresponds to 10 which is the sum of \texttt{8} and
\texttt{2}.

\bibliography{../bib/Bio}
\bibliographystyle{alpha}

\end{document}


