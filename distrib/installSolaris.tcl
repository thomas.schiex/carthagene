file copy -force .cgwopt ~
file copy -force .cgwdefalgo ~
file copy -force .carthagenerc ~/.carthagenerc

set fid [open ~/.carthagenerc r 0666]
set buff [read $fid]
close $fid

puts [pwd]

set buff2 ""
append  buff2 "lappend auto_path \"[pwd]\"\n" $buff

set fid [open ~/.carthagenerc RDWR]
puts $fid $buff2
close $fid
exit
