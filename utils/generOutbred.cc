// Un generateur d'echantillon de recombinaisons outbred...



//----------------- INCLUDE files standard ------------------------------------

#include <stdlib.h>
#include <time.h>
#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <string>

int tr[256][256];
//------------------- Les classes utilisees -----------------------------------
typedef char* gamete;

class marqueur
{
public:
    double position;
    double frecomb;
  
  marqueur()  { position = frecomb = 0.0;};
  ~marqueur() {};
};
// Un marqueur est defini par sa position sur le chromosome et la FR entre lui et son precedent 

class individu
{
public:
  gamete pere;
  gamete mere;
  
  individu() {pere=NULL; mere=NULL;};
  void init(int n) {pere=new char[n+1];memset(pere,'0',n);pere[n]=0;                               mere=new char[n+1];memset(mere,'0',n);mere[n]=0;
                   };
  ~individu() {delete[] pere; delete[] mere;}; 
  void affiche (int n,ofstream & out){out<<pere<<"\t"<<mere<<"\t"; 
                                      out<<"\n";}; 
};
// Chaque individu est cree a partir d'un gamete paternel et un maternel


//------------- Generateur al�atoire de Simon ---------------------------------

double randomax ()
{ 
    static int premiere_fois = 0;
    char *demo;
    long heure;

// Le code qui suit initialise le generateur de nombre aleatoire  a une           valeur precise pour obtenir la meme suite de nombres aleatoires si DEMO        est present dans l'environnement d'appel a l'execution. Mais cette liste       de nombre est identique pour chaque creation de processus, ce qui donne        un caractere tres repetitifs aux taches. Ainsi, en ne definissant pas DEMO,    on initialise le random avec une valeur pseudo-aleatoire qui est l'heure       calendaire multipliee par le pid du processus 

  if (!premiere_fois) 
      {
	  premiere_fois = 1;
	  if (!(demo=getenv("DEMO")))
	    {
	      heure = (long) time(NULL);
	      srand48(heure * getpid());
	    }
	  else
	    {
	      srand48(atol(demo));
	    }
      }
    return drand48();
} 


// ---------------- Comparer la position de deux marqueurs --------------------

int compmarq(const void* x, const void* y)
{
    marqueur* a = (marqueur*)x;
    marqueur* b = (marqueur*)y;

    
    if ((a->position) > (b->position))
	return 1;
    else if ((a->position) < (b->position))
	return -1;
    else 
        return 0;
}


// ---------------- Calcule la f. de recomb. a partir de la distance ----------

// Fonction de Haldane, Independance + Poisson 
double Haldane(double distance)
{
    return ((1-exp(-2*fabs(distance)))/2);
}

// Fonction de Kosambi. 
double Kosambi(double distance)
{
    return (0.5*tanh(2*fabs(distance)));
}


// ---------------- G�n�rateur de marqueurs -----------------------------------

marqueur* genmarq(int nombre, double length)
{
    marqueur* Eux = new marqueur[nombre];

    int i;
    for (i=0; i<nombre; i++)  
       {
	Eux[i].position = randomax()*length;
       }
    
    qsort(Eux,nombre,sizeof(marqueur),compmarq);
    // Apres avoir cree les marqueurs en leur attribuant une position aleatoire       sur le chromosome, on les range selon leur ordre sur le chromosome

    // Puis on calcule la f. de recomb via Haldane
    double distance;
    for (i=1; i<nombre; i++)
      {
	distance = Eux[i-1].position-Eux[i].position;
	Eux[i].frecomb = Haldane(distance);
	// Eux[i].frecomb = Kosambi(distance);
      }
    
    return Eux;
    // On obtient en fait une carte genetique
}


// -------- G�n�rateur de carte uniforme (d constante entre marq.succ.) -------

marqueur* cartunif(int nombre, double length)
{
    marqueur* Eux = new marqueur[nombre];

    int i;
    for (i=0; i<nombre; i++)  
       {
	Eux[i].position = i*length/(nombre-1);
       }
    
    // Les marqueurs sont espaces de maniere reguliere, dans l'ordre de numerotation usuel croissant le long du chromosome

    // Puis on calcule la f. de recomb via Haldane
    double distance;
    for (i=1; i<nombre; i++)
      {
	distance = Eux[i-1].position-Eux[i].position;
	Eux[i].frecomb = Haldane(distance);
	// Eux[i].frecomb = Kosambi(distance);
      }
    
    return Eux;
    // On obtient en fait une carte genetique
}


//----------- Generateur de gam�te --------------------------------------------

gamete gengamete(int nombre, marqueur* modele)
{
    gamete parent;
    int parite;
    int i; 
	
          // On cree un gamete aleatoire d'apres le modele de carte genere 

          parent=new char[nombre+1];
          memset(parent,'0',nombre);parent[nombre]=0;
          if (randomax() < 0.5)
              {parent[0] ='1';}
          parite = parent[0]-'0';  
          for(i=1; i<nombre; i++)
	     {
	         if (randomax() < modele[i].frecomb)
		   parite = (1-parite);
		 parent[i] = '0'+parite;
                 // Si randomax < FR il y a eu recombinaison
	     }
          return parent;
}


//---------------- Generateur modele ------------------------------------------

individu* gensample(int taille, int nombre, marqueur* modele)
{
    individu* sample =new individu[taille];
    int i;

    for (i=0; i< taille; i++)
      {
       sample[i].pere=gengamete(nombre, modele);
       sample[i].mere=gengamete(nombre, modele);
       // Pour chaque individu de l'echantillon, on cree un gamete paternel et           un maternel 
     } 
    return sample;
}


//---------------- Generateur d'�chantillon -----------------------------------

individu* incomplet(int taille, int nombre,individu *echantillon, double proba,                    double probae)
{
    individu* sample =new individu[taille];
    int i,j;

    for (i=0; i< taille; i++)
      {
       sample[i].init(nombre); 

       for (j=0; j<nombre; j++)
         {
          if (randomax()<proba)
	        {
	         if (randomax() < probae)
		   sample[i].pere[j]='1'- echantillon[i].pere[j];
                 else 
                   sample[i].pere[j]=echantillon[i].pere[j];
		}
          else  sample[i].pere[j]='-';
          if (randomax()<proba)
	        {
	         if (randomax() < probae)
		   sample[i].mere[j]='1'-echantillon[i].mere[j];
                 else 
                   sample[i].mere[j]=echantillon[i].mere[j];
		}
          else  sample[i].mere[j]='-';
         // Pour chaque individu, on introduit erreurs et manques
	  }
       } 
    return sample;
}


//--------------------- Dump du modele ----------------------------------------

void pontemodele(int nombre,marqueur* marqs,ofstream & out)
{
  int i;
  
  out << "Carte:\n";
  for (i=0; i<nombre; i++)
    {
      out << "M" << i+1  << " � " << 100*marqs[i].position << " cM";
      if (i) 
	{
	 out << "\tdistance : " << 100*(marqs[i].position-marqs[i-1].position)              <<  " cM";
	 out << "\tF.Rec.: " << marqs[i].frecomb;
	}
      out << endl;
    }
}


//-------------------- Codage outbred -----------------------------------------

char code(individu* echant, int numero, int marq, int allelie)
{
char codage[9][14]={{'1','9','1','1','1','1','1','3','5','f','5','5','3','3'},
  		    {'6','2','2','2','2','2','2','3','a','f','a','a','3','3'},
                    {'6','4','4','4','4','4','4','c','5','f','5','5','c','c'},
                    {'8','9','8','8','8','8','8','c','a','f','a','a','c','c'},
                    {'7','b','3','7','3','b','3','3','f','f','f','f','f','3'},
                    {'7','d','5','7','5','5','d','f','5','f','f','5','f','f'},
                    {'e','d','c','c','e','c','d','c','f','f','f','f','c','f'},
                    {'e','b','a','a','e','b','a','f','a','f','a','f','f','f'},                     {'f','f','f','f','f','f','f','f','f','f','f','f','f','f'}};
  int ligne;

  ligne=tr[echant[numero].pere[marq]][echant[numero].mere[marq]];
  return codage[ligne][allelie];  
}


//------------------- Dump de l'echantillon -----------------------------------

void pontesample(int taille,int nombre,individu* echantillon,int allelie,ofstream & out)
{
  int i,j;

  out << "data type f2 intercross\n";
  out << taille << " " << nombre << " 0 0\n";
    
   for (i=0; i<nombre; i++)
     {
       out << "*M" << i+1 << "\t";
       
       
       for (j=0;j<taille; j++)
	 out << code(echantillon,j,i,allelie);
       out << endl;
     }
}


//------------- Le generateur de chromo lui-meme-------------------------------

int main(int argc, char *argv[])
{
  // Si erreur d'appel,on explique a l'utilisateur la liste des param. a donner
  if ((argc <  5) ||  (argc > 8)) 
    { 
      cout << "Usage: " << argv[0] << " choix n m f [a] [p] [p'] [l], o�\n\n";
      cout << "       choix est le choix de repartition des marqueurs sur le chromosome: uniforme(u)ou aleatoire(a) \n";
      cout << "       n est le nombre de marqueurs\n";
      cout << "       m est le nombre d'individus dans l'�chantillon\n";
      cout << "       f est le nom de base des fichiers de sortie \n";
      cout << "       a est le cas de repartition des alleles choisi, par defaut 2\n";
      cout << "       p est la proba d'etre informatif, par defaut 1.0\n";
      cout << "       p' est la probabilit� d'erreur, par defaut 0.0\n";
      cout << "       l la longueur totale du chromosome,  par defaut 1.0\n";
      return(1);
    }
 
  int NMarq = atoi(argv[2]);
  
  if (NMarq <= 1)
    { 
      cout << "Usage: " << argv[0] << " choix n m f [a] [p] [p'] [l], o�\n\n";
      cout << "       choix est le choix de repartition des marqueurs sur le chromosome: uniforme(u)ou aleatoire(a) \n";
      cout << "       n est le nombre de marqueurs\n";
      cout << "       m est le nombre d'individus dans l'�chantillon\n";
      cout << "       f est le nom de base des fichiers de sortie \n";
      cout << "       a est le cas de repartition des alleles choisi, par defaut 2\n";
      cout << "       p est la proba d'etre informatif, par defaut 1.0\n";
      cout << "       p' est la probabilit� d'erreur, par defaut 0.0\n";
      cout << "       l la longueur totale du chromosome,  par defaut 1.0\n";
      return(2);
    }
  
  int Taille = atoi(argv[3]);
  string OutName = argv[4];
  string RawName = new char [OutName.length()+5];
  string RamName = new char [OutName.length()+5];
  string TraceName = new char [OutName.length()+5];
  
  RawName = OutName+".raw";
  TraceName = OutName+".map";
  RamName = OutName+".ram";
  
  double Prob = 1.0;
  double ProbE = 0.0;
  double Length = 1.0;
  int Allelie=2;

  marqueur* Marqueurs;
   if (strcmp(argv[1],"a")==0)
    {Marqueurs = genmarq(NMarq,Length);}
   else if (strcmp(argv[1],"u")==0)
    {Marqueurs = cartunif(NMarq,Length);}
   else 
    { 
      cout << "Usage: " << argv[0] << " choix n m f [a] [p] [p'] [l], o�\n\n";
      cout << "       choix est le choix de repartition des marqueurs sur le chromosome:                 uniforme(u)ou aleatoire(a) \n";
      cout << "       n est le nombre de marqueurs\n";
      cout << "       m est le nombre d'individus dans l'�chantillon\n";
      cout << "       f est le nom de base des fichiers de sortie \n";
      cout << "       a est le cas de repartition des alleles choisi, par defaut 2\n";
      cout << "       p est la proba d'etre informatif, par defaut 1.0\n";
      cout << "       p' est la probabilit� d'erreur, par defaut 0.0\n";
      cout << "       l la longueur totale du chromosome,  par defaut 1.0\n";
      return(3);
    }

  if (argc >= 6)  Allelie = atoi(argv[5]);
  if ((Allelie<0)||(Allelie>13))
   {
   cout << "Usage:\n 0 -> f0=m0, f1=m1\n
                     1 -> f0=m1, f1=m0\n
                     2 -> tous les alleles sont differents\n
                     3 -> f0=m0\n
                     4 -> f1=m1\n 
                     5 -> f0=m1\n
                     6 -> f1=m0\n
                     7 -> m0=m1\n
                     8 -> f0=f1\n
                     9 -> f0=f1 et m0=m1 ou f0=f1=m0=m1\n
                     10-> f0=f1=m0\n
                     11-> f0=f1=m1\n
                     12-> f0=m0=m1\n
                     13-> f1=m0=m1\n";
   return(4);
   } 
  if (argc >= 7)  Prob = atof(argv[6]);
  if (argc >= 8) ProbE = atof(argv[7]);
  if (argc >= 9)  Length = atof(argv[8]);

  ofstream raw,trace,ram;
  ofstream & Raw = raw;
  ofstream & Ram = ram;
  ofstream & Trace = trace;

  int i,j;  
  for (i=0; i<256;i++)
       {
        for (j=0; j<256;j++) 
	  {tr[i][j] = 0;}
       }
   
  tr['0']['0'] = 0;
  tr['0']['1'] = 1;
  tr['1']['0'] = 2;
  tr['1']['1'] = 3;
  tr['0']['-'] = 4;
  tr['-']['0'] = 5;
  tr['1']['-'] = 6;
  tr['-']['1'] = 7;
  tr['-']['-'] = 8;

  Raw.open(RawName.c_str());
  Ram.open(RamName.c_str());
  Trace.open(TraceName.c_str());

  individu* Echantillon = gensample(Taille,NMarq,Marqueurs);
  individu* JDincomp = incomplet(Taille,NMarq,Echantillon,Prob,ProbE);
  //  On a cree la carte et l'echantillon complet
 
  pontemodele(NMarq,Marqueurs,Trace);
  Trace.close();

  // On trace la carte modele
  pontesample(Taille,NMarq,Echantillon,Allelie,Ram);
  Ram.close();
  // On renvoie le jeu de donnees cree sans erreurs ni manques
  
  pontesample(Taille,NMarq,JDincomp,Allelie,Raw);
  Raw.close();
  // On renvoie le jeu de donnees cree avec erreurs et manques

  delete[] Echantillon;
  delete[] JDincomp;
  delete[] Marqueurs; 
  
  return(0);
}

