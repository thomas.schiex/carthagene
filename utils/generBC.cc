// Un generateur d'echantillon de recombinaisons...



//----------------- INCLUDE files standard ------------------------------------

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

// these includes are needed if compiled on new g++ versions (>4.0?)
#include <climits>
#include <cstdlib>
#include <cstring>
#include <unistd.h> 

using namespace std;


//-----------------Classes utilisees-------------------------------------------

class marqueur
{
public:
    double position;
    double frecomb;
  
  marqueur()  { position = frecomb = 0.0;  };
  ~marqueur() {};
};

class individu
{
public:
  char* recomb;
  
  individu() {recomb = NULL;};
  void init(int n) { recomb = new char[n+1]; memset(recomb, '0', n); recomb[n]=0; };
  ~individu() {delete recomb; };
  
  void print() { cout << recomb << '\n'; };
};


//------------- Generateur al�atoire de Simon ---------------------------------

double randomax ()
{ 
    static int premiere_fois = 0;
    char *demo;
    long heure;

    // Le code qui suit initialise le generateur de nombre aleatoire  a une           valeur precise pour obtenir la meme suite de nombres aleatoires si DEMO        est presente dans l'environnement d'appel a` l'execution. Cependant,           cette liste de nombre est identique pour chaque creation de processus,         ce qui donne un caractere tres repetitifs aux taches. Ainsi, en ne             definissant pas DEMO, on initialise le randm avec une valeur pseudo-           aleatoire qui est l'heure calendaire multipliee par le pid du processus

  if (!premiere_fois) 
      {
	  premiere_fois = 1;
	  if (!(demo=getenv("DEMO")))
	    {
	      heure = (long) time(NULL);
	      srand48(heure * getpid());
	    }
	  else
	    {
	      srand48(atol(demo));
	    }
      }
    return drand48();
}


// ---------------- Comparer la position de deux marqueurs --------------------

int compmarq(const void* x, const void* y)
{
    marqueur* a = (marqueur*)x;
    marqueur* b = (marqueur*)y;

    
    if ((a->position) > (b->position))
	return 1;
    else if ((a->position) < (b->position))
	return -1;
    else return 0;
}


// ---------------- Calcule la f. de recomb. a partir de la distance ----------

// Fonction de Haldane, Independance + Poisson 
double Haldane(double distance)
{
    return ((1-exp(-2*fabs(distance)))/2);
}

// Fonction de Kosambi. 
double Kosambi(double distance)
{
    return (0.5*tanh(2*fabs(distance)));
}


// ---------------  G�n�rateur de marqueurs -------------------------

marqueur* genmarq(int nombre, double length)
{
    marqueur* Eux = new marqueur[nombre];

    int i;
    for (i=0; i<nombre; i++)  
      {
	Eux[i].position = randomax()*length;
	}
    
    qsort(Eux,nombre,sizeof(marqueur),compmarq);
    double distance;

    // On calcule la f. de recomb via Haldane
    for (i=1; i<nombre; i++)
      {
	distance = Eux[i-1].position-Eux[i].position;
	Eux[i].frecomb = Haldane(distance);
	// Eux[i].frecomb = Kosambi(distance);
      }
    
    return Eux;
}


// -------- G�n�rateur de carte uniforme (d constante entre marq.succ.) -------

marqueur* cartunif(int nombre, double length)
{
    marqueur* Eux = new marqueur[nombre];

    int i;
    for (i=0; i<nombre; i++)  
       {
	Eux[i].position = i*length/(nombre-1);
       }
    
    // Les marqueurs sont espaces de maniere reguliere, dans l'ordre de numerotation usuel croissant le long du chromosome

    // Puis on calcule la f. de recomb via Haldane
    double distance;
    for (i=1; i<nombre; i++)
      {
	distance = Eux[i-1].position-Eux[i].position;
	Eux[i].frecomb = Haldane(distance);
	// Eux[i].frecomb = Kosambi(distance);
      }
    
    return Eux;
    // On obtient en fait une carte genetique
}


//---------------- Generateur d'�chantillon -------------------------

individu* gensample(int taille, int nombre, marqueur* modele)
{
    individu* sample =new individu[taille];

    int parite;
    int i,j;

    for (i=0; i< taille; i++)
	{
	    sample[i].init(nombre);
	    if (randomax() < 0.5)
		sample[i].recomb[0] = '1';
	    parite = sample[i].recomb[0]-'0';

	    for(j=1; j<nombre; j++)
		{
		    if (randomax() < modele[j].frecomb)
			parite = (1-parite);
		    sample[i].recomb[j] = '0'+parite;
		}
	}
    return sample;
}


//-------------- Nombre de  de recomb. observ�es ---------------------

int nrec(int mi, int mj, int taille, individu* echantillon)
{
  int nij = 0;
  int k;
  
  for (k=0; k<taille; k++)
    if (echantillon[k].recomb[mi] != echantillon[k].recomb[mj]) nij++;
  
  return (nij);
}


//------------ Contribution elementaire au loglikelihood------------------

double weight(int mi, int mj, int taille, individu* echantillon)
{
  int nij = nrec(mi,mj,taille,echantillon);
  double rij = ((double) nij)/taille;
  
  if (rij>0.5) rij = 0.5; // est-ce bien raisonnable ?
  
  if (nij == 0.0) return 0.0;
  else return -(nij*log10(rij)+(taille-nij)*log10(1-rij));
}


//--------------------- Dump du modele -------------------------

void pontemodele(int nombre,marqueur* marqs,int taille, individu* echantillon, ofstream & out)
{
  int i;
  double ll =0.0;
  
  out << "Carte:\n";
  for (i=0; i<nombre; i++)
    {
      out << "M" << i+1  << " � " << 100*marqs[i].position << " cM";
      if (i)
	{
	  out << "\tdistance : " << 100*(marqs[i].position-marqs[i-1].position) <<  " cM";
	  out << "\tF.Rec. : " << marqs[i].frecomb << 
	    "\tObs. : " << ((double)nrec(i-1,i,taille, echantillon))/taille;
	  ll -= weight(i-1,i,taille,echantillon);
	}
      out << endl;
    }
  out << "Log likelihood: " <<  ll << ", (a la MM: " << taille*log10(0.5)+ ll << ")";
  out << endl;
  
}


//------------------- Dump de l'echantillon ------------------------

void pontesample(int taille, int nombre, int total, int base, individu* echantillon,double proba,
		 double probae, bool header, ofstream & out)
{
  int i,j;

  if (header) {
    out << "data type f2 backcross\n";
    out << taille << " " << total << " 0 0 0=A 1=H\n";
  }

   for (i=0; i<nombre; i++)
     {
       out << "*M" << i+1+base << "\t";
      
       for (j=0;j<taille; j++)
	 {
	   if (randomax()<proba)
	     {
	       if (randomax() < probae)
		 out << (echantillon[j].recomb[i] == '0') ? '1' : '0';
	       else
		 out << echantillon[j].recomb[i];
	     }
	   else
	     out << "-";
	 }
       out << endl;
     }
}

void Usage()
{
      cout << " mdist n m f [p] [p'] [l] [c]\n\n";
      cout << "       mdist is the marker distribution model on the chromosome(s):  uniforme(u) or random(a) \n";
      cout << "       n is the number of markers\n";
      cout << "       m is the number of individuals in the sample\n";
      cout << "       f is the basename for output files\n";
      cout << "       p is the probability of being informative (default to 1.0)\n";
      cout << "       p' is the error probability (by default 0.0)\n";
      cout << "       l is the chromosome length (Haldane, default to 1.0)\n";
      cout << "       c is the number of simulated chromosomes (default to 1)\n";
}

//------------- Le generateur de chromo lui-meme----------------------
int main(int argc, char *argv[])
{

  if ((argc <  5) ||  (argc > 9)) 
    {
		cout << "Usage: " << argv[0];
        Usage();
        return(1);
    }
  
  int NMarq = atoi(argv[2]);
  
  if (NMarq <= 1)
    { 
		cout << "Usage: " << argv[0];
        Usage();
        return(2);
    }
  
  int Taille = atoi(argv[3]);
  string OutName = argv[4];
  string RawName = new char [OutName.length()+5];
  string  TraceName = new char [OutName.length()+5];
  
  RawName = OutName+".raw";
  TraceName = OutName+".map";
  
  double Prob = 1.0;
  double ProbE = 0.0;
  double Length = 1.0;
  int    NChrom = 1;
  
  if (argc >= 6)  Prob = atof(argv[5]);
  if (argc >= 7) ProbE = atof(argv[6]);
  if (argc >= 8)  Length = atof(argv[7]);
  if (argc >= 9) NChrom = atoi(argv[8]);
  
  marqueur* Marqueurs;
  
  ofstream raw,trace;
  ofstream & Raw = raw;
  ofstream & Trace = trace;
  Raw.open(RawName.c_str());
  Trace.open(TraceName.c_str());
  individu* echantillon;

  for (int i = 1; i <= NChrom; i++) {

   if (strcmp(argv[1],"a")==0)
    {Marqueurs = genmarq(NMarq,Length);}
  else if (strcmp(argv[1],"u")==0)
    {Marqueurs = cartunif(NMarq,Length);}
  else 
    { 
		cout << "Usage: " << argv[0];
        Usage();
        return(3);
    }
	  
    echantillon = gensample(Taille,NMarq,Marqueurs);
    pontemodele(NMarq,Marqueurs,Taille, echantillon,Trace);
    pontesample(Taille,NMarq,NMarq*NChrom,(i-1)*NMarq,echantillon,Prob,ProbE,(i == 1),Raw);
    
    delete [] Marqueurs;
    delete[] echantillon;
  }

  Trace.close();
  Raw.close();
  return(0);
}
    
    
