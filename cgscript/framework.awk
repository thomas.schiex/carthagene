
BEGIN {
  line = "";
}

{
  sub(" *[(].*","",$0); 
  gsub("-"," ",$0); 
  gsub(" ","\n",$0); 
  line = $0;
}

END {
  printf("%s\n",line);
}
