#!/bin/sh

# usage: ./rh.sh RH_filename Order_filename

###########################################################################
# creates a file Order_filename.rh.log containing the resulting
# best map using the traditional RH mapping criterion 
# w.r.t. datasets Radiated Hybrids (RH_filename) and a Reference Order (Order_filename)
###########################################################################

quiet=0

biologicaldatafilename="$1"
orderdatafilename="$2"

logfilename="$2.rh.log"

######## Build a first map with a linear approximation of the traditional mapping criterion to the TSP

(echo "quietset $quiet" ; echo "cgout $logfilename" ; echo "dsload $biologicaldatafilename" ; echo "dsload $orderdatafilename" ; echo "dsmergor 1 2" ; echo "dsbpcoef 2 0" ; echo "lkh 1 -1" ; echo "bestprint" ; echo "paretobestprintd 1000" ; echo "exit") | tclsh
