set title 'Pareto frontier approximation'
set ylabel 'log10-likelihood'
set xlabel 'number of breakpoints'
set key left top

#set xrange [0:40]
#set yrange [-620:-520]

plot "tmponlypareto" using 4:6 title 'log10(P(data | order))' with linespoints, "tmpdominated" using 4:6 title 'Dominated orders' with points, "tmpbalanced" using 4:6 title 'Best order' with points, "tmponlypareto" using 4:8 title 'log10(P(order | data))' with linespoints

#plot "tmponlypareto" using 4:6 title 'log10(P(data | order))' with linespoints, "tmpdominated" using 4:6 title 'Dominated orders' with points, "tmpbalanced" using 4:6 title 'Best order' with points, "tmponlypareto" using 4:8 title 'log10(P(order | data))' with linespoints, "tmponlypareto" using 4:10 title 'log10(P(order | breakpoint))' with linespoints, "tmponlypareto" using 4:12 title 'log10(P(breakpoint))' with linespoints

pause -1

#set term postscript eps monochrome dashed "helvetica" 18
set term postscript eps color dashed "helvetica" 18
set output "pareto.eps"
replot
