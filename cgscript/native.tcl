#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: native.tcl,v 1.35.2.1 2012-05-31 08:02:51 dleroux Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module        : cgscript
# Projet        : CartaGene
#
# Description : features implemented in tcl also
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------
#
#to provide a dedicated way to Quit CarthaGene
#WARNING this command does not work inside .carthagenerc!!!
rename exit cg_exit

#puts "NATIVE.TCL NOW LOADED."

#
# Saving preferences CGW inspiration
# could be much nicer
#

proc CGWMSconfigSave {} {

    
    # pour windows
    if { $::tcl_platform(platform) == "windows" } {
	set fidopt  [open ~/cgwopt r]
	set lisopt [split [read $fidopt] "\r\n"]
    } else {
	set fidopt  [open ~/.cgwopt r]
	set lisopt [split [read $fidopt] "\n"]
    }

    close $fidopt

    # pour windows
    if { $::tcl_platform(platform) == "windows" } {
	set fidopt  [open ~/cgwopt w]
    } else {
	set fidopt  [open ~/.cgwopt w]
    }

    puts $fidopt [lindex $lisopt 0]
    puts $fidopt [lindex $lisopt 1]
    puts $fidopt [lindex $lisopt 2]
    puts $fidopt [lindex $lisopt 3]
    puts $fidopt [lindex $lisopt 4]
    puts $fidopt [lindex $lisopt 5]
    puts $fidopt [lindex $lisopt 6]
    puts $fidopt [lindex $lisopt 7]
    puts $fidopt [lindex $lisopt 8]
    puts $fidopt [lindex $lisopt 9]
    puts $fidopt [lindex $lisopt 10]
    puts $fidopt [lindex $lisopt 11]
    puts $fidopt [lindex $lisopt 12]
    puts $fidopt [lindex $lisopt 13]
    puts $fidopt [lindex $lisopt 14]
    puts $fidopt [lindex $lisopt 15]
    puts $fidopt [lindex $lisopt 16]
    puts $fidopt [lindex $lisopt 17]
    puts $fidopt [lindex $lisopt 18]
    puts $fidopt [lindex $lisopt 19]
    puts $fidopt [lindex $lisopt 20]
    puts $fidopt [lindex $lisopt 21]
    puts $fidopt [lindex $lisopt 22]
    puts $fidopt [lindex $lisopt 23]
    puts $fidopt [lindex $lisopt 24]
    puts $fidopt [lindex $lisopt 25]
    puts $fidopt [lindex $lisopt 26]
    puts $fidopt [lindex $lisopt 27]
    puts $fidopt [lindex $lisopt 28]
    puts $fidopt [lindex $lisopt 29]

    puts $fidopt "*optAffichage.mode: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -mode]"
    puts $fidopt "*optAffichage.repreCarte: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreCarte]"
    puts $fidopt "*optAffichage.repreMrq: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreMrq]"
    puts $fidopt "*optAffichage.zoomMrq: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -zoomMrq]" 
    puts $fidopt "*optAffichage.repreVraisem: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreVraisem]"
    puts $fidopt "*optAffichage.repreEtiq: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -repreEtiq]"
    puts $fidopt "*optAffichage.zoomMap: [.cggr.shellchildsite.tv.lwchildsite.optAffichage cget -zoomMap]"

    puts $fidopt "*colorDialog.logtxt: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -logtxt]"
    puts $fidopt "*colorDialog.logfle: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -logfle]"
    puts $fidopt "*colorDialog.carte: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -carte]"
    puts $fidopt "*colorDialog.mrqtxt: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqtxt]"
    puts $fidopt "*colorDialog.mrqtxtsel: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqtxtsel]"
    puts $fidopt "*colorDialog.mrqlineon: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqlineon]"
    puts $fidopt "*colorDialog.mrqlineoff: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqlineoff]"
    puts $fidopt "*colorDialog.infoline: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infoline]"
    puts $fidopt "*colorDialog.infotxt: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infotxt]"
    puts $fidopt "*colorDialog.infobox: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infobox]"

    puts $fidopt "*colorDialog.infofont: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -infofont]"
    puts $fidopt "*colorDialog.mrqfont: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -mrqfont]"
    puts $fidopt "*colorDialog.logfont: [.cggr.shellchildsite.tv.lwchildsite.colorDialog cget -logfont]"

    puts $fidopt "*tv.locfont: [.cggr.shellchildsite.tv get -locfont]"

    close $fidopt
}

proc exit {} {
    global CGWhaschanged
    
    if $CGWhaschanged {

	# a graphical function has been used
	if { [expr ! [winfo exists .cgcq]] } {
	    iwidgets::messagedialog .cgcq -title "CarthaGene Question" -modality application \
		-bitmap questhead -text "Save the configuration file?"
	    
	    .cgcq hide Help
	    .cgcq buttonconfigure OK -text "Yes"
	    .cgcq buttonconfigure Cancel -text "No"
	}

     	if [.cgcq activate] {
     	    CGWMSconfigSave 
     	}
    }
 
    cg_exit
}

#
# to help
#
proc help {} {
    
    # DL : changed "*" into "[a-zA-Z]*" to prevent listing of _* commands
    set cmdlist [info commands {::CG::[a-zA-Z]*}]
    
    # to store the comparative mapping commands
    set comparcom [info commands ::CG::pareto*]
    set comparcom [concat $comparcom [info commands ::CG::dsbp*]]
    foreach cmd $comparcom {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
    }
    puts "CartaGene commands : "
    puts "===================="
    puts "miscellaneous :"
    puts "---------------"
    foreach cmd [info commands ::CG::cg*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts "[format "%-15s %s" cgsave [cgsave -h]]"
    puts "[format "%-15s %s" cgexport [cgsave -h]]"
    puts ""
    puts "to manipulate data sets :"
    puts "-------------------------"
    foreach cmd [info commands ::CG::ds*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate groups :"
    puts "----------------------"
    foreach cmd [info commands ::CG::group*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate loci :"
    puts "--------------------"
    foreach cmd [info commands ::CG::mrk*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to manipulate the heap :"
    puts "------------------------"
    foreach cmd [info commands ::CG::heap*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts "[format "%-15s %s" heaprintg [heaprintg -h]]"
    puts "[format "%-15s %s" bestprint "Print the best map in the heap."]"
    puts "[format "%-15s %s" bestprintd "Print the best map in the heap, in detail."]"
    puts ""
    puts "to manipulate a map :"
    puts "---------------------"
    foreach cmd [info commands ::CG::map*] {
	set cmdlist [lreplace $cmdlist [set i [lsearch -exact $cmdlist $cmd]] $i]
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "to search for good maps :"
    puts "-------------------------"
    foreach cmd $cmdlist {
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
    puts ""
    puts "comparative mapping :"
    puts "---------------------"
    puts "[format "%-15s %s" paretoplotg [paretoplotg -h]]"
    puts "[format "%-15s %s" paretosave [paretosave -h]]"
    puts "[format "%-15s %s" paretobestprint "Print the best map in the Pareto frontier."]"
    puts "[format "%-15s %s" paretobestprintd "Print the best map in the Pareto frontier, in detail."]"
    foreach cmd $comparcom {
	puts "[format "%-15s %s" [namespace tail $cmd] [$cmd -h]]"
    }
#    puts "[format "%-15s %s" dsbp [dsbp -h]]"
    puts "[format "%-15s %s" mapbp [mapbp -h]]"
    puts ""
    puts "graphical commands :"
    puts "--------------------"
    puts "[format "%-15s %s" maprintg [maprintg -h]]"
    puts "[format "%-15s %s" heaprintg [heaprintg -h]]"
    puts "[format "%-15s %s" paretoplotg [paretoplotg -h]]"
    puts ""
    puts "Type '<command-name> -H' for more help with a particular command."
    puts "Some commands have a default behaviour :"
    puts "Type 'd_<command-name>' to use it."
    puts "To change the defaults, edit the '.carthagenerc'"
    
}

#
# to provide default behavior
#
proc d_dsload {} {

    set dsfile "../../Data/mouse0.raw"

    puts "by default : loads $dsfile"

    ::CG::dsload $dsfile

}

proc d_build {} {

    set nbmap 3

    puts "by default : builds $nbmap maps at the same time"

    ::CG::build $nbmap

}

proc d_flips {} {

    set winsize 5
    set lodthr 3.0
    set iter 0
    
    puts "by default : flips once, into a window size of $winsize, with a LOD thresholf of $lodthr."
    
    ::CG::flips $winsize $lodthr $iter
    
}	

proc d_annealing {} {
    
    set tries 0
    set tempi 100.0
    set tempf 0.1
    set cooling 0.9

    puts "by default : annealing starts from a temperature of $tempi, to stop at a temperature of $tempf."
    puts "The additional number of iteration per temperature is $tries, and the cooling speed is $cooling."

    ::CG::annealing $tries $tempi $tempf $cooling
}
	
proc d_greedy {} {

    set NbRun 1
    set NbIter 0
    set TabooLenMin 1
    set TabooLenMax 15
    set Anytime 0

    puts "by default : greedy does $NbRun loop, with an additional number of it�ration of $NbIter."
    puts "The minimum size of the taboo list is $TabooLenMin, the maximum size of the list is $TabooLenMax"
    puts "The Anytime ratio is set to 0."

    ::CG::greedy $NbRun $NbIter $TabooLenMin $TabooLenMax $Anytime
}

proc d_algogen {} {

    set nb_gens 10
    set nb_elements 0
    set selectype 0
    set pcross 0.3
    set pmut 0.5
    set evol_fitness 1

    puts "by default : algogen simulate $nb_gens generation, initializing the first one from the heap."
    puts "The type of selection is the roulette wheel."
    puts "The crossing over probability is $pcross"
    puts "The mutation probability is $pmut"
    puts "The fitness is evolutive"

    ::CG::algogen $nb_gens $nb_elements $selectype $pcross $pmut $evol_fitness
}

proc d_group {} {

    set DistThres 0.5 
    set	LODThres 3.0
    puts "by default : group uses a distance threshold of $DistThres,"
    puts "and a LOD threshold of $LODThres to identify linkage groups."

    ::CG::group $DistThres $LODThres
}

proc cgsave {arg} {
    global tcl_interactive
    #les contr�les :
    #un seul argument
    # -h -u -H ou filename
    
#    if {$tcl_interactive == 0} {
#    puts stderr "The shell does not provide this command, but the Data Menu does."
#    return }

    if {$arg =="-h"} {
	return "Save the state of the current session."
    }

    if {$arg =="-H"} {
	puts ""
	puts "Usage : cgsave \[-h | -H | -u| fileName\]"
	puts ""
	puts "Description : cgsave save the current state of the CarthaGene session that you are running. The session is dumped to a Tcl script. You may want to run this script later to retore your work. To do this, under the Carthagene/tcl shell type the command \"source\" followed by the name of the file you mentionned to this command. The file paths of your data sets may be relatives, in this case, your dump file will depend to your current work space."
	puts ""
	return 
    }

    if {$arg =="-u"} {
	return "cgsave fileName"
    }

    if {[file exist $arg]} {
	set answer "bid"
	while {$answer != ""  & $answer != "y" & $answer != "n"} {
	    puts -nonewline "The file $arg already exist, replacing it? (\[y\] or n):"
	    flush stdout;
	    set answer [gets stdin]
	}
	if {$answer == "n"} {
	    puts stderr "Session saving aborted!"
	    return
	}
    }
    
    if [catch {open $arg w} fid] {
	puts stderr "Cannot open $arg : $fid"
    } else {
	
	cgsavegui $fid

	close $fid
	puts "Your session has been successfully saved."
	puts "To restore it, type : source $arg"
    }
}

proc cgsavegui {fid} {

    puts $fid "\# CarthaGene session dump script"
    
    puts $fid "\#A: Resizing the heap."
    
    puts $fid "heapsize [::CG::heapsizeget]"
    
	puts $fid "\#B: Loading and Merging Data Sets"
    
    foreach jeu [::CG::dsget] {
	if {[lindex $jeu 1] == "merged by order"} {
		puts $fid "dsmergor [lindex $jeu 2] [lindex $jeu 3]"
#		set file2 [lindex $jeu 2]
#		regsub ".*/" $file2 "" file22
#		set file3 [lindex $jeu 3]
#		regsub ".*/" $file3 "" file33
#		puts $fid "dsmergor $file22 $file33"
	} elseif { [lindex $jeu 1] == "merged genetic"} {
	    puts $fid "dsmergen [lindex $jeu 2] [lindex $jeu 3]"
#		set file2 [lindex $jeu 2]
#		regsub ".*/" $file2 "" file22
#		set file3 [lindex $jeu 3]
#		regsub ".*/" $file3 "" file33
#		puts $fid "dsmergen $file22 $file33"
	} else {
	    puts $fid "dsload {[lindex $jeu 2]}"
#		set file2 [lindex $jeu 2]
#		regsub ".*/" $file2 "" file22
#	    puts $fid "dsload $file22"
	}
    }	
    
    puts $fid "\#C: Merging Markers."
    
    foreach merg [::CG::mrkmerget] {
	foreach merged [lrange $merg 1 end] {
	    puts $fid "mrkmerge [lindex $merg 0] $merged"
	}
    }
    
    puts $fid "\#D: Selecting a comparative mapping criterion if used."
    
    set jeu [lindex [lindex [::CG::dsget] end] 0]
    set coef [::CG::dsbpcoef $jeu 0]
	::CG::dsbpcoef $jeu $coef
	if {$coef != 0} {puts $fid "dsbpcoef $jeu $coef"} 

    puts $fid "\#E: Filling The Heap."
    
    foreach ord [heapordget] {
	puts $fid "mrkselset \{$ord\}"
	puts $fid "sem"
    }
    
    puts $fid "\#F: Selecting Markers."
    
    puts $fid "mrkselset \{[::CG::mrkselget]\}"

}

#
# Save chromosome to file
#
proc cgexport {args} {
    
    if { [llength $args] == 0 || [llength $args] > 3 || [llength $args] == 2} {
	puts stderr "wrong # args: should be -h | -H | -u | fileName mapName chromosomeName"
	puts stderr "while evaluating cgexport"
	return
    }
    
    if { [llength $args] == 1 } {
	set item [lindex $args 0]
	if {$item =="-h"} {
	    return "Export the current best map to a XML and space separated file."
	} elseif {$item =="-H"} {
	    puts ""
	    puts "Usage : cgexport \[-h | -H | -u | fileName mapName chromosomeName\]"
	    puts ""
	    puts "Description : cgexport exports the best map as a chromosome to two files. The first is a mapmaker format file whose name is the recceived fileName, the second is suffixed with .xml and contains an XML description readable by MCQTL� and BioMercator. If a file already exists, the command will append the current map to the end of the file. In fact two files are produced at the same time."	    
	    puts ""	    
	    return
	    
	} elseif {$item =="-u"} {
	    return "cgexport fileName mapName chromosomeName"
	} else {
	    puts stderr "wrong # args: should be -h | -H | -u | fileName mapName chromosomeName"
	    puts stderr "while evaluating cgexport"
	    return 
	}
    }

    set filename [lindex $args 0]
    set filenamexml $filename.xml
    set map [lindex $args 1]
    set chrom [lindex $args 2]

    # the heap must be filled

    if {[mapordget 0] == {}} {
	return
    }
    
    if {[file isdirectory $filename]} {
	puts stderr "Error : $filename is a directory, a regular file is expected."
	return
    }

    if {[file exist $filename]} { #.xml is supposed to exist also
	set answer "bid"
	while {$answer != ""  & $answer != "y" & $answer != "n"} {
	    puts "The file $filename already exist,"
            puts -nonewline "do you want to append the current map? (\[y\] or n):"
	    flush stdout;
	    set answer [gets stdin]
	}
	if {$answer == "n"} {
	    puts stderr "Export aborted!"
	    return
	}

	set filename1 $filename~
        file copy -force $filename $filename1

	set filename1xml $filenamexml~
	file rename -force $filenamexml $filename1xml

    }

    # a map file with the same name exist, has to be completed.
    if {[info exists filename1]} {
	
	if [catch {open $filename1 {RDONLY}} fid1] {
	    puts stderr "Error : Cannot open $filename1."
	    puts stderr "Export aborted!"
	    return
	}
	
	# to open the xml map
	if [catch {open $filename1xml {RDONLY}} fidxml1] {
	    puts stderr "Error : Cannot open $filename1xml."
	    puts stderr "Export aborted!"
	    return
	}
	
	if [catch {open $filenamexml {WRONLY CREAT}} fidxml] {
	    puts stderr "Error : Cannot open $filenamexml."
	    puts stderr "Export aborted!"
	    return
	}
    } else {
	# to creat the xml map
	if [catch {open $filenamexml {WRONLY CREAT}} fidxml] {
	    puts stderr "Error : Cannot open $filename1."
	    puts stderr "Export aborted!"
	    return
	}
    }


    if [catch {open $filename {WRONLY CREAT APPEND}} fid] {
	puts stderr "Error : Cannot open $filename."
	puts stderr "Export aborted!"
	return
    }

    if {[info exists filename1]} {

	cgexportgui $fid1 $fid $fidxml1 $fidxml $map $chrom

    } else {

	cgexportgui $fid $fidxml $map $chrom

    }

    if {[info exists filename1]} {
	close $fid1
	close $fidxml1
    }
    close $fid
    close $fidxml
}

#
# Save chromosome to file
#
proc cgexportgui {args} {
    
    if { [llength $args] == 6} {

	set fid1 [lindex $args 0]
	set fid [lindex $args 1]
	set fidxml1 [lindex $args 2]
	set fidxml [lindex $args 3]
	set map [lindex $args 4]
	set chrom [lindex $args 5]
	
	set lima {}
	set lichro {}
	
	# to collect the loci already present in the previous file

	while {[string length [set ligne [gets $fid1]]] > 0} {
	    
	    set liw [lrange [split $ligne] 2 end]
	    
	    lappend lichro [string trimleft [lrange [split $ligne] 0 0] "*"]
	    
	    for {set x 0} { $x < [llength $liw] } {incr x 2} {
		
		lappend lima [lindex $liw $x] 
	    }
	    
	}
	
	# a chromosome name must be uniq
	
	if { [lsearch -exact $lichro $chrom] != -1 } {
	    puts stderr "Error : The chromosome $chrom already exist into the file."
	    puts stderr "Export aborted!"
	    file copy -force $filename1 $filename
	    file rename -force $filename1xml $filenamexml
	    return
	}

	# a locus name must be uniq
	set nommark [mrknames [mapordget 0]]
	
	foreach locus $nommark {
	    if { [lsearch -exact $lima $locus] != -1 } {
		puts stderr "Error : The marker $locus already exist into the file."
		puts stderr "Export aborted!"
		file copy -force $filename1 $filename
		file rename -force $filename1xml $filenamexml
		return
	    }
	}

	while {[string length [set ligne [gets $fidxml1]]] > 0} {
	    set balise [lrange [split $ligne] 0 0]
	    if { $balise != "</GENETIC_MAP>" } {

		# to get the name of the map

		if { $balise == "<GENETIC_MAP" } {
		    puts $fidxml "<GENETIC_MAP name=\"$map\">"
		    regexp {\"(.*)\"} $ligne match mn
		    if { $mn != $map } {
			puts stderr "Warning : The name of the map has changed!."
		    }
		} else {
		    puts $fidxml $ligne
		}
	    }
	}
	
    } else {

	set fid [lindex $args 0]
	set fidxml [lindex $args 1]
	set map [lindex $args 2]
	set chrom [lindex $args 3]

	puts $fidxml "<GENETIC_MAP name=\"$map\">"
	
    }

    set cumul 0
    
    set nommark [mrknames [mapordget 0]]
    set sel [lindex [lindex [heapget h 1] 0] 2]
    set nb [llength $sel]
    set nbm [llength $nommark]

    set carte "*$chrom $nbm "

    puts $fidxml "<CHROMOSOME name=\"$chrom\">"   

    for {set x 2} { $x <  ( $nb-2 ) } {incr x} {

	set namm [lindex $sel $x]	
	set posm [lindex $sel [expr $x + 1]]
	puts $fidxml "<MARKER name=\"$namm\" position=\"$posm\">"
	
	append carte $namm " "
        incr x
	append carte [expr [lindex $sel [expr ($x + 2)]]-$cumul] " "
        set cumul [lindex $sel  [expr ($x + 2)]]
	
	
    }

    append carte [lindex $sel end-1]
    
    puts $fid  $carte
    
    puts $fidxml "</CHROMOSOME>"
    puts $fidxml "</GENETIC_MAP>"

    puts "export successful!"
}

#
# To avoid more checking, the path is expanded to a full path
# 

proc dsload {arg} {
    # cas du flag
    if {$arg == "-h" || $arg == "-H" || $arg == "-u"} {
	return [::CG::dsload $arg]
    } else {	
	# cas du fichier
	if { [info tclversion] >= 8.4 } {
	    ::CG::dsload [file normalize $arg]
	} else {
	    ::CG::dsload $arg
	}
    }
}


#
# To display of the heap from the shell
#

proc heaprintg {args} {

    global tcl_platform
    
    if { [llength $args] == 0 || [llength $args] > 2} {
	puts stderr "wrong # args: should be -h | -H | -u | UnitFlag NbMap"
	puts stderr "while evaluating heaprintg"
	return
    }
    
    if { [llength $args] == 1 } {
	set item [lindex $args 0]
	if {$item =="-h"} {
	    return "Draw the heap(current best maps) into a graphical display."
	} elseif {$item =="-H"} {
	    puts ""
	    puts "Usage : heaprintg \[-h | -H | -u | UnitFlag NbMap\]"
	    puts ""
	    puts "Description : heaprintg draw the current state of the heap. To pop up the configuration menu, click with the right mouse button on the background of the canvas."	    
	    puts ""	    
	    return
	    
	} elseif {$item =="-u"} {
	    return "heaprintg UnitFlag NbMap"
	}
    }
    
    package require CGwin
    wm withdraw .

    # pour windows
    if { $tcl_platform(platform) == "windows" } {
	option readfile ~/cgwopt
    } else {
	option readfile ~/.cgwopt
    }

    set cmd [concat ::CG::heapget [split $args]]

    set lm [eval $cmd]

    if {[llength [lindex $lm 0]] == 1} {
	return
    }

    set cmd [concat ::CG::dsget]

    set lds [eval $cmd]

    if { [expr ! [winfo exists .cggr]] } {
	#cgtasvisuel .cggr
	 
	#pack .cggr -expand yes -fill both

	cgtv .cggr

     }

    .cggr activate

    set cmd [concat .cggr tasser [split $args] {$lm} {$lds}]

    eval $cmd

}

#
# To display a single map from the shell
#

proc maprintg {args} {

    global tcl_platform

    if { [llength $args] == 0 || [llength $args] > 2} {
	puts stderr "wrong # args: should be -h | -H | -u | UnitFlag MapId"
	puts stderr "while evaluating maprintg"
	return
    }
    
    if { [llength $args] == 1 } {
	set item [lindex $args 0]
	if {$item =="-h"} {
	    return "Draw a map into a graphical display."
	} elseif {$item =="-H"} {
	    puts ""
	    puts "Usage : heaprintg \[-h | -H | -u | UnitFlag MapId\]"
	    puts ""
	    puts "Description : heaprintg draw a map. To pop up the configuration menu, click with the right mouse button on the background of the canvas."	    
	    puts ""	    
	    return
	    
	} elseif {$item =="-u"} {
	    return "heaprintg UnitFlag MapId"
	}
    }
    
    package require CGwin
    wm withdraw .

    # pour windows
    if { $tcl_platform(platform) == "windows" } {
	option readfile ~/cgwopt
    } else {
	option readfile ~/.cgwopt
    }

    set cmd [concat ::CG::mapget [split $args]]

    set lm [list [eval $cmd]]

    if {[llength [lindex $lm 0]] == 1} {
	return
    }

    set cmd [concat ::CG::dsget]

    set lds [eval $cmd]
    
    if { [expr ! [winfo exists .cggr]] } {
	
	cgtv .cggr
    }
    
    .cggr activate

    set cmd [concat .cggr tasser [split $args] {$lm} {$lds}]

    eval $cmd

}

#
# To display the pareto plot from the shell
#

proc paretoplotg {args} {
    
    if { [llength $args] == 0 || [llength $args] > 1} {
	puts stderr "wrong # args: should be -h | -H | -u | Lambda"
	puts stderr "while evaluating paretoplotg"
	return
    }
    
    if { [llength $args] == 1 } {
	set item [lindex $args 0]
	if {$item =="-h"} {
	    return "Draw the Pareto frontier."
	} elseif {$item =="-H"} {
	    puts ""
	    puts "Usage : heaprintg \[-h | -H | -u | Lambda\]"
	    puts ""
	    puts "Description : paretoplotg provides a Pareto plot. Lambda corresponds to the expected number of breakpoints between the true order and the reference order. Try Lambda = 1 as default value."	    
	    puts ""	    
	    return
	    
	} elseif {$item =="-u"} {
	    return "paretoplotg Lambda"
	}
    }
    
    if { $::tcl_platform(platform) != "windows" } {
	    package require CGwin
	    wm withdraw .
    }
    
    set cmd [concat ::CG::paretoinfog [split $args]]
    
    set li [eval $cmd]
    
    if {[llength $li] == 1} {
	return
    }
    
    if { [expr ! [winfo exists .cgpp]] } {
	
	cgpp .cgpp

	
    }
    
    .cgpp activate
    
    set cmd [concat .cgpp plot {$li}]
    
    eval $cmd
    
}

#
# To retrieve the orders by loci ids of the map stored into the heap
#

proc heapordget {} {
    
    set lo {}
    
    foreach map [::CG::heapget k 0] {
	lappend lo [::CG::mapordget [lindex $map 0]]
    } 
    return $lo
}

# returns the selection with group added
proc groupmerge {args} {
     foreach item $args {
         set loc " [::CG::groupget $item] "
         append sel $loc
     }
     ::CG::mrkselset $sel
}

#
# enables 2 release of greedy
#
proc greedy {args} {
    if { [llength $args] == 4 } {
	lappend args 0
    }
    set cmd [concat ::CG::greedy [split $args]]

    eval $cmd
}

# (DL) wrapper for mcmc ; handles -slow and -lgsz options


# (DL) simple routine to check for an option (with or without a parameter) in
# the argument list of a command. If the option is found, it is removed from
# the argument list (and its parameter as well). If it is not found, the
# default value is returned and the argument list unchanged.
proc check_opt {name witharg defaultvalue} {
        upvar args opts
	set pos [lsearch -exact $opts $name]
        #puts "$name: got pos $pos"
	if { $pos == -1 } {
		return $defaultvalue
	} else {
                #puts "before : $opts"
                set opts [lreplace $opts $pos $pos]
                #puts "   ... : $opts"
		if { $witharg } {
			set argpos $pos
			if { [llength $opts] > $argpos } {
				set ret [lindex $opts $argpos]
			}
                        set opts [lreplace $opts $pos $pos]
		} else {
			set ret [expr !$defaultvalue]
		}
                #puts " after : $opts"
	}
	return $ret
}


proc mcmc {args} {
        # default is slow_computations=1, -fast sets it to 0
        set slow [check_opt -fast 0 1]
        set lgsz [check_opt -lgsz 1 10]
        set verb [check_opt -verbose 0 0]
	#puts "\[tcl debug] slow=$slow lgsz=$lgsz verb=$verb"
        lappend args $slow $lgsz $verb
        #puts "got args : $args"
        if { [llength $args] != 6 } {
                _mcmc -H
        } else {
                set cmd [concat _mcmc $args]
                #puts $cmd
                eval $cmd
        }
}

proc dsave {args} {
        if { [llength $args] == 1 } {
                _dsave [lindex $args 0] {}
        } elseif { [llength $args] == 2 } {
                _dsave [lindex $args 0] [lindex $args 1]
        } else {
                _dsave -H
        }
}

proc mrkdoubleget {args} {
    if { [llength $args] == 0 } {
        _mrkdoubleget 0.0
    } else {
        _mrkdoubleget [lindex $args 0]
    }
}
