## awk -f sol2cg.awk ../denis/07mrks.txt 170.sol

BEGIN {
  first = 1;
}

FNR == NR && NF == 1 {
  n++;
  trans[n] = $1;
}

FNR != NR && NF == 1 && $1 >= 2 && $1 <= n+1 {
  if (first) {
    seq1[n1++] = trans[$1 - 1];
  } else {
    seq2[n2++] = trans[$1 - 1];
  }    
}

FNR != NR && NF == 1 && $1 == 1 {
  first = 0;
}

END {
  print "Solution order for Carthagene tclsh interface:";
  printf("mrkselset {");
  for (i=0; i<n2; i++) {
    printf("%d ", seq2[i]);
  } 
  for (i=0; i<n1; i++) {
    printf("%d ", seq1[i]);
  }
  printf("}\n");
  print "";
  print "Solution order for Carthagene at the C++ level:";
  printf("{");
  for (i=0; i<n2; i++) {
    printf("%d,", seq2[i]);
  } 
  for (i=0; i<n1; i++) {
    printf("%d,", seq1[i]);
  }
  printf("};\n");
}
