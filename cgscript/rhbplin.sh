#!/bin/sh

# usage: ./rhbplin.sh RH_filename Order_filename [lambda]

###########################################################################
# creates a file Order_filename.lin$lambda.log containing the resulting
# best map using the comparative RH mapping criterion 
# (with optional parameter lambda, the expected number of breakpoints)
# w.r.t. two datasets: Radiated Hybrids (RH_filename) and a Reference Order (Order_filename)
###########################################################################

quiet=0

biologicaldatafilename="$1"
orderdatafilename="$2"

if [[ $3 == "" ]] ; then lambda=1 ; else lambda=$3 ; fi

logfilename="$2.lin${lambda}.log"

######## Build a first map with a linear approximation of the comparative mapping criterion to the TSP

(echo "quietset $quiet" ; echo "cgout $logfilename" ; echo "dsload $biologicaldatafilename" ; echo "dsload $orderdatafilename" ; echo "dsmergor 1 2" ; echo "dsbplambda 2 1 $lambda" ; echo "lkh 1 -1" ; echo "bestprint" ; echo "paretobestprintd $lambda" ; echo "exit") | tclsh
