#-----------------------------------------------------------------------------
#                            CarthaGene
#
# Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
# Unite de Biometrie et Intelligence Artificielle, Toulouse, France
#
#  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
#
# $Id: contrib.tcl,v 1.20.2.2 2012-06-13 14:23:02 dleroux Exp $
#
#    This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Module        : cgscript
# Projet        : CartaGene
#
# Description : features implemented in tcl only
#
# Divers : 
#-----------------------------------------------------------------------------
#-INRA---------------------Station de Biom�trie et d'Intelligence Artificielle
#-----------------------------------------------------------------------------

#
# features implemented in tcl
#

# set operations in Tcl using lists
proc addElement { orglist elem } {
      upvar $orglist list

      if { [lsearch -exact $list $elem] == -1 } {
         lappend list $elem
      }
}
proc union { lista listb } {
      set result $lista

      foreach elem $listb {
         if { [lsearch -exact $lista $elem] == -1 } {
            lappend result $elem
         }
      }
      return $result
}
proc intersection { lista listb } {
      set result {}

      foreach elem $listb {
         if { [lsearch -exact $lista $elem] != -1 } {
            lappend result $elem
         }
      }
      return $result
}
proc exclusion { lista listb } {
      set result {}

      foreach elem $lista {
         if { [lsearch -exact $listb $elem] == -1 } {
            lappend result $elem
         }
      }
      return $result
}
proc hasElement { lista elem } {
      return [expr [lsearch $lista $elem] >= 0]
}
 
proc monogroup {sizethres distthres lodthres datafilename} {
	dsload $datafilename
	set n [group $distthres $lodthres]
	set lo {}
	for {set i 1} {$i <= $n} {incr i} {if {[llength [groupget $i]] >= $sizethres} {set lo [union $lo [groupget $i]]}}
	return $lo
}

# Find 2-point linkage group intersection for several RH panels
# Foreach panel, merge connected components whose size is greater than sizethres
proc multigroup args {

    if {$args =="-h"} {
	return "Merge large linkage groups found on several biological datasets given as arguments."
    }

    if {$args =="-H"} {
	puts ""
	puts "Usage : multigroup \[-h | -H | -u\] | SizeThres ListOfDistThres ListOfLODThres ListOfDataFileName"
	puts ""
	puts "Description : it loads each biological dataset found in ListOfDataFileName parameter and returns a list of markers such that each marker belongs to a sufficiently large linkage group (of size greater than SizeThres parameter) in ALL biological datasets. Linkage groups are computed by a 2-point analysis with ListOfDistThres and ListOfLODThres parameters which specify for each biological data set the distance and LOD thresholds, as used in the usual CarthaGene group command."
    puts "Examples : "
    puts "..." 
	puts "CG> multigroup 10 {0.5 0.8} {9 6} {panelRH1.cg panelRH2.cg}"
	puts ""
	return 
    }

    if {$args =="-u" || [llength $args] != 4} {
	return "multigroup SizeThres ListOfDistThres ListOfLODThres ListOfDataFileName"
    }

	set sizethres [lindex $args 0]
	set listofdistthres [lindex $args 1]
	set listoflodthres [lindex $args 2]
	set listofdatafilename [lindex $args 3]

    set lo [monogroup $sizethres [lindex $listofdistthres 0] [lindex $listoflodthres 0] [lindex $listofdatafilename 0]]
    for {set i 1} {$i <  [llength $listofdatafilename]} {incr i} {set lo [intersection $lo [monogroup $sizethres [lindex $listofdistthres $i] [lindex $listoflodthres $i] [lindex $listofdatafilename $i]]]}
    return $lo
}

proc framework1 {threshold delta name} {
    # run framework mapping
    set thepath [lindex $::auto_path [lsearch $::auto_path *carthagene]]
    if { $thepath == "" } { set thepath "." }
    set threshold_ [expr {$threshold * 1000}]
    set delta_ [expr {$delta * 1000}]
    if { $threshold == 0 } {
      ## DL debugging framework*
      #exec gdb --args $thepath/cgscript/framework_mst $name.tsp 0 $delta_
      set res [exec $thepath/cgscript/framework_mst $name.tsp 0 $delta_ | awk -f $thepath/cgscript/framework.awk > $name.sol]
    } else {
      ## DL debugging framework*
      #exec gdb --args $thepath/cgscript/framework $name.tsp $threshold_ $delta_
      set res [exec $thepath/cgscript/framework $name.tsp $threshold_ $delta_ | awk -f $thepath/cgscript/framework.awk > $name.sol]
    }
    # create initial marker selection file
    if [catch {open "$name.ord" w} fid] {
	puts stderr "Cannot open $name.ord : $fid"
	exit
    #} else {
    #        puts stdout "Got channel $fid for $name.ord"
    }
    foreach id [mrkselget] {
	puts $fid "$id"
    }
    close $fid
    # convert to marker order from the TSP solution
    #puts stdout "Now generating .sol from .ord"
    set order [exec awk -f $thepath/cgscript/framework2cg.awk $name.ord $name.sol]
    return $order
}

proc frameworkiter {thresholdmin thresholdmax delta name changemrksel} {
    set initorder [ mrkselget ]

	set bestsize 0
    set bestorder ""
	set threshold $thresholdmin
    while {$threshold <= $thresholdmax} \
    {
      set order [framework1 $threshold $delta $name]
      set size [llength $order]
      puts "$threshold ($size): $order"
      if {$size > $bestsize} \
      {
         set bestsize $size
         set bestorder $order
      }
      incr threshold
    } 

	if {$changemrksel == 1} \
    {
      mrkselset $bestorder
      sem
    }
    return $bestorder
}

proc framework args {

    #les contr�les :
    #un seul argument
    # -h -u -H ou un seuil et une liste contenant une commande

    if {$args =="-h"} {
	return "Build a framework map."
    }

    if {$args =="-H"} {
	puts ""
	puts "Usage : framework \[-h | -H | -u\] | ThresholdMin ThresholdMax Delta ChangeSel"
	puts ""
	puts "Description : it builds a framework map by selecting a subset of the current markers. Two adjacent markers in the framework map have their contribution to the 2-pt log10-likelihood of the map lower than a threshold the value of which varies in the range \[ThresholdMin,ThresholdMax\]. The robustness of the resulting map is controled by the parameter Delta. It ensures that it does not exist any other map containing the same set of markers and with a different order such that its 2-pt log10-likelihood is greater than the framework map likelihood minus Delta. For instance if Delta is equal to 3, then any other map is 1000 less likely. If ChangeSel is equal to 1 then the best framework map is inserted into the Carthagene heap (modifying the current marker list)."
    puts "Examples : "
    puts "..." 
	puts "CG> framework 5 30 3 0"
	puts "..."
	puts "CG> framework 9 9 3 1"
	puts ""
	return 
    }

    if {$args =="-u" || [llength $args] != 4} {
	return "framework ThresholdMin ThresholdMax Delta ChangeSel"
    }

    set thresholdmin [lindex $args 0]
    set thresholdmax [lindex $args 1]
    set delta [lindex $args 2]
    set changemrksel [lindex $args 3]

    # create TSP files
    cg2tsp tmp[pid].tsp
    frameworkiter $thresholdmin $thresholdmax $delta tmp[pid] $changemrksel
    return
}

proc frameworkn args {

    #les contr�les :
    #un seul argument
    # -h -u -H ou un seuil et une liste contenant une commande

    if {$args =="-h"} {
	return "Build a framework map."
    }

    if {$args =="-H"} {
	puts ""
	puts "Usage : frameworkn \[-h | -H | -u\] | ThresholdMin ThresholdMax Delta ChangeSel"
	puts ""
	puts "Description : it builds a framework map by selecting a subset of the current markers. Two adjacent markers in the framework map have their contribution to the 2-pt normalized log10-likelihood of the map lower than a threshold the value of which varies in the range \[ThresholdMin,ThresholdMax\]. The robustness of the resulting map is controled by the parameter Delta. It ensures that it does not exist any other map containing the same set of markers and with a different order such that its 2-pt normalized log10-likelihood is greater than the framework map likelihood minus Delta. For instance if Delta is equal to 3, then any other map is 1000 less likely. If ChangeSel is equal to 1 then the best framework map is inserted into the Carthagene heap (modifying the current marker list)."
    puts "Examples : "
    puts "..." 
	puts "CG> frameworkn 5 30 3 0"
	puts "..."
	puts "CG> frameworkn 9 9 3 1"
	puts ""
	return 
    }

    if {$args =="-u" || [llength $args] != 4} {
	return "frameworkn ThresholdMin ThresholdMax Delta ChangeSel"
    }

    set thresholdmin [lindex $args 0]
    set thresholdmax [lindex $args 1]
    set delta [lindex $args 2]
    set changemrksel [lindex $args 3]

    # create TSP files
    cg2tsp tmp[pid].tsp
    frameworkiter $thresholdmin $thresholdmax $delta normtmp[pid] $changemrksel
    return
}

proc frameworkl args {

    #les contr�les :
    #un seul argument
    # -h -u -H ou un seuil et une liste contenant une commande

    if {$args =="-h"} {
	return "Build a framework map."
    }

    if {$args =="-H"} {
	puts ""
	puts "Usage : frameworkl \[-h | -H | -u\] | ThresholdMin ThresholdMax Delta ChangeSel"
	puts ""
	puts "Description : it builds a framework map by selecting a subset of the current markers. Two adjacent markers in the framework map have their 2-pt LOD greater than a threshold the value of which varies in the range \[ThresholdMin,ThresholdMax\]. The robustness of the resulting map is controled by the parameter Delta. It ensures that it does not exist any other map containing the same set of markers and with a different order such that cumulative 2-pt LOD is greater than the framework map LOD minus Delta. If ChangeSel is equal to 1 then the best framework map is inserted into the Carthagene heap (modifying the current marker list)."
    puts "Examples : "
    puts "..." 
	puts "CG> frameworkl 5 30 3 0"
	puts "..."
	puts "CG> frameworkl 9 9 3 1"
	puts ""
	return 
    }

    if {$args =="-u" || [llength $args] != 4} {
	return "frameworkl ThresholdMin ThresholdMax Delta ChangeSel"
    }

    set thresholdmin [lindex $args 0]
    set thresholdmax [lindex $args 1]
    set delta [lindex $args 2]
    set changemrksel [lindex $args 3]

    # create TSP files
    cg2tsp tmp[pid].tsp
    set thepath [lindex $::auto_path [lsearch $::auto_path *carthagene]]
    if { $thepath == "" } { set thepath "." }
	set lodmax [exec awk -f $thepath/cgscript/lodmax.awk lodtmp[pid].tsp]
	exec awk -f $thepath/cgscript/shiftlod.awk lodtmp[pid].tsp lodtmp[pid].tsp > poslodtmp[pid].tsp
    set lodmax_ [expr {$lodmax / 1000}]
    set thresholdmin_ [expr {$lodmax_ - $thresholdmin}]
    set thresholdmax_ [expr {$lodmax_ - $thresholdmax}]
    frameworkiter $thresholdmax_ $thresholdmin_ $delta poslodtmp[pid] $changemrksel
    return
}

proc frameworkmst args {

    #les contr�les :
    #un seul argument
    # -h -u -H ou un seuil et une liste contenant une commande

    if {$args =="-h"} {
	return "Build a framework map."
    }

    if {$args =="-H"} {
	puts ""
	puts "Usage : frameworkmst \[-h | -H | -u\] | Delta ChangeSel"
	puts ""
	puts "Description : it builds a framework map by selecting a subset of the current markers. The robustness of the resulting map is controled by the parameter Delta. It ensures that it does not exist any other map containing the same set of markers and with a different order such that its 2-pt log10-likelihood is greater than the framework map likelihood minus Delta. For instance if Delta is equal to 3, then any other map is 1000 less likely. If ChangeSel is equal to 1 then the best framework map is inserted into the Carthagene heap (modifying the current marker list)."
    puts "Examples : "
    puts "..." 
	puts "CG> frameworkmst 3 0"
	puts "..."
	puts "CG> frameworkmst 3 1"
	puts ""
	return 
    }

    if {$args =="-u" || [llength $args] != 2} {
	return "frameworkmst Delta ChangeSel"
    }

    set thresholdmin 0
    set thresholdmax 0
    set delta [lindex $args 0]
    set changemrksel [lindex $args 1]

    # create TSP files
    cg2tsp tmp[pid].tsp
    frameworkiter $thresholdmin $thresholdmax $delta tmp[pid] $changemrksel
    return
}

proc frameworknmst args {

    #les contr�les :
    #un seul argument
    # -h -u -H ou un seuil et une liste contenant une commande

    if {$args =="-h"} {
	return "Build a framework map."
    }

    if {$args =="-H"} {
	puts ""
	puts "Usage : frameworknmst \[-h | -H | -u\] | Delta ChangeSel"
	puts ""
	puts "Description : it builds a framework map by selecting a subset of the current markers. The robustness of the resulting map is controled by the parameter Delta. It ensures that it does not exist any other map containing the same set of markers and with a different order such that its 2-pt normalized log10-likelihood is greater than the framework map likelihood minus Delta. For instance if Delta is equal to 3, then any other map is 1000 less likely. If ChangeSel is equal to 1 then the best framework map is inserted into the Carthagene heap (modifying the current marker list)."
    puts "Examples : "
    puts "..." 
	puts "CG> frameworknmst 3 0"
	puts "..."
	puts "CG> frameworknmst 3 1"
	puts ""
	return 
    }

    if {$args =="-u" || [llength $args] != 2} {
	return "frameworknmst Delta ChangeSel"
    }

    set thresholdmin 0
    set thresholdmax 0
    set delta [lindex $args 0]
    set changemrksel [lindex $args 1]

    # create TSP files
    cg2tsp tmp[pid].tsp
    frameworkiter $thresholdmin $thresholdmax $delta normtmp[pid] $changemrksel
    return
}

proc comparesizesgeq {list1 list2} {
    set l [llength $list1]
    if {[llength $list2] < $l} {set l [llength $list2]}
	for {set i 0} {$i < $l} {incr i} {if {[lindex $list1 $i] >= [lindex $list2 $i]} {return 1}}
	return 0
}

proc comparesizesgt {list1 list2} {
    set l [llength $list1]
    if {[llength $list2] < $l} {set l [llength $list2]}
	for {set i 0} {$i < $l} {incr i} {if {[lindex $list1 $i] > [lindex $list2 $i]} {return 1}}
	return 0
}

proc comparesizespareto {list1 list2} {
    set ok 0
    set l [llength $list1]
    if {[llength $list2] < $l} {set l [llength $list2]}
	for {set i 0} {$i < $l} {incr i} {
       if {[lindex $list1 $i] < [lindex $list2 $i]} {return 0}
       if {[lindex $list1 $i] > [lindex $list2 $i]} {set ok 1}
    }
	return $ok
}

proc getmaxsize {list1} {
    set max 0
    set l [llength $list1]
	for {set i 0} {$i < $l} {incr i} {
       # reject list if there is a negative value (i.e. all maps should be shortened)
	   if {[lindex $list1 $i] < 0} {return 0}
       if {[lindex $list1 $i] > $max} {set max [lindex $list1 $i]}
    }
	return $max
}

proc substractsizes {list1 list2} {
    set l [llength $list1]
    if {[llength $list2] < $l} {set l [llength $list2]}
    set res {}
	for {set i 0} {$i < $l} {incr i} {lappend res [expr [lindex $list1 $i] - [lindex $list2 $i]]}
	return $res
}

proc getmapsizes {list1} {
    set sizes {}
    set max [llength [lindex $list1 0]]
	set dsinfos [dsget]
	for {set i 2} {$i < $max} {incr i} {set pos [lindex [lindex [lindex [heapget h 1] 0] $i] 0] ; incr pos -1 ; set gdatatype [lindex [lindex $dsinfos $pos]  1] ; if {$gdatatype != "constraint" && $gdatatype != "order" && $gdatatype != "merged genetic" && $gdatatype != "merged by order"} {lappend sizes [lindex [lindex [lindex $list1 0] $i] end]}}
    return $sizes
}

proc squeeze args {

    #les contr�les :
    #un seul argument
    # -h -u -H ou un seuil et une liste contenant une commande

    if {$args =="-h"} {
	return "Expunge step by step non reliable loci from a framework map."
    }

    if {$args =="-H"} {
	puts ""
	puts "Usage : squeeze \[-h | -H | -u\] | {Thres1 Thres2...} \[Cmd\]\]"
	puts ""
	puts "Description : squeeze provide an procedure to remove step by "
	puts "step non reliable loci from a framework map. The command start "
	puts "from the best map stored inside the heap. Then it test the "
	puts "removing of a single markers. Each marker is removed separatly."
	puts "If the most shortened map has been shorten significatively"
	puts "accordind to a distance threshold in cMorgan (Haldane distance) for genetics data or cRay for Radiation Hybrids data, the locus is definitively"
	puts "removed. And the procedure is restarted. The default threshold"
	puts "is 50 cM/cR. At each restart, a command tries to disturb the current"
	puts "order of the loci keeped so far."
    puts "The default command that try to disturb the order is : "
	puts "{flips 3 0 2}. To change this, you need to provide a second "
	puts "parameter. It has to be a regular carthagene command stored"
	puts "inside a list."
    puts "Examples : "
    puts "..." 
	puts "CG> squeeze 50"
	puts "..."
	puts "CG> squeeze {50 100} {flips 5 0 2}"
	puts ""
	return 
    }

    if {$args =="-u"} {
	return "squeeze Thres \[Cmd\]"
    }

    # parameters
    quietset 1

    set distthres {50}

    set defpertub {flips 3 0 2}
    
    if {[lindex $args 0] != {}} {
	
	set distthres [lindex $args 0] 

	if {[lindex $args 1] != {}} {
	    
	    set defpertub [lindex $args 1]
	    
	}
    }


    # the liste gathering the removed loci

    set llr {}

    # first step retrieving the best map order (so far) and keeping it ?

    set fwmref [lindex [heapordget] 0]

    set fwmo $fwmref

    set shorten $distthres

    #the main loop

    while {[comparesizesgeq $shorten $distthres]} {
    
	# to make sur the order of the selection is the same than the order of the best map
	set fwmo [lindex [heapordget] 0]

	mrkselset $fwmo
	
	sem

	# the length of the map
	
	set fwmrs [getmapsizes [heapget h 1]]
	
	set ow {}
	set lx 0
	
	# loci that make the map longer are not considered.
	set shorten {}
	set maxss [llength $fwmrs]
    for {set ii 0} {$ii < $maxss} {incr ii} {lappend shorten 0}

	# foreach locus 
	for {set x 0} { $x < [llength $fwmo] } {incr x} {
	    
	    # remove
	    
	    set fwmw [lreplace $fwmo $x $x]
	    
	    # compute
	    
	    mrkselset $fwmw
	    
	    sem
	    
	    set fwmws [getmapsizes [heapget h 1]]
	    
	    #keeping the shortened one 
        set diffsizes [substractsizes $fwmrs $fwmws]
# puts "DEBUG $x $fwmrs $fwmws $diffsizes $shorten"
	    if {[getmaxsize $diffsizes] > [getmaxsize $shorten]} {
		set ow $fwmw
		set lx $x
		set shorten [substractsizes $fwmrs $fwmws]
	    }	
	    
	}
	
	# if the confition of end is not reached the current map is set 
	# to the one the locus has to be removed
        # the name of the locus is stored
        # and the map is tested
# puts "END $shorten $distthres"

	if {[comparesizesgeq $shorten $distthres]} {

	    set llr [linsert $llr end [mrkname [lindex $fwmo $lx]]]
	    mrkselset $ow
	    sem
	    eval $defpertub
	    
	} else {
	    mrkselset $fwmo
	    sem
	}
	
	set fwmo $ow
    }

    # if a at lesat one marker has been expunged
    if { $llr == {}} {
	puts ""
    puts ""
	puts "With the distance threshold of $distthres, no locus has been expunged!" 
    } else {
	puts ""
    puts ""
	puts "Those loci have been removed : $llr"
	puts "The best map is now :"
	heaprint
	    	
    }

}

### Find the best map using a comparative mapping approach
### w.r.t. two datasets: Radiated Hybrids (or backcross) and a Reference Order
proc paretofind {biologicaldatafilename orderdatafilename logfilename paretofilename lkhrepeatnumber fast2ptEM lambda verbosity} {
    # avoid trace information
    quietset $verbosity

    # save textual output in a log file
    if {$logfilename != ""} {
	cgout $logfilename
    }

    ## fast 2-point EM
    cg2pt $fast2ptEM

    # load biological and order datasets and merge them
    dsload $biologicaldatafilename
    dsload $orderdatafilename
    dsmergor 1 2

    # allows large heapsize for maintaining the pareto frontier
    paretoheapsize 2048

    # create a first pareto frontier from scratch
    paretolkh $lkhrepeatnumber 1 0

    ## multi-point EM
    cg2pt 0
    ## force to recompute EM for each map in the heap
    cgtolerance 0.0009 0.09 

    # print textual information on the pareto frontier
    # paretoinfog 1

    # print a summary of computation effort (only if quietset 0)
    cgstat 0

    # save the pareto frontier in a file
    if {$paretofilename != ""} {
	paretosave $paretofilename
    }
    
    # show the best map w.r.t. the biological and order datasets
    return [paretobestprintd $lambda]
}

### Improve the quality of maps in the case of missing orthologies
### should be run after paretofind
proc paretoimprove {previousparetofilename logfilename paretofilename size anytime lambda verbosity} {
    # avoid trace information
    quietset $verbosity

    # save textual output in a log file
    if {$logfilename != ""} {
	cgout $logfilename
    }

    # reload the pareto frontier found by paretolkh.tcl
    source $previousparetofilename

    # allows large heapsize for maintaining the pareto frontier
    paretoheapsize 2048
    
    # improve the current pareto frontier for the maps with a number of breakpoints between num1 and num2
    foreach e [paretoinfog $lambda] {if {[lindex $e 4] == "balanced"} {set id [lindex $e 0]}}
    set num1 [expr [mapbp 2 $id] - $size]
    set num2 [expr [mapbp 2 $id] + $size]
    paretogreedyr $anytime $num1 $num2

    # print textual information on the pareto frontier
    # paretoinfog 1

    # print a summary of computation effort (only if quietset 0)
    cgstat 0

    # save the pareto frontier in a file
    if {$paretofilename != ""} {
	paretosave $paretofilename
    }

    # show the best map w.r.t. the biological and order datasets
    return [paretobestprintd $lambda]
}

# print the best map in the pareto frontier
proc paretobestprint { lambda } {
    foreach e [paretoinfog $lambda] {if {[lindex $e 4] == "balanced"} {set id [lindex $e 0]}}
    maprint $id
    return $id
}

# print in detail the best map in the pareto frontier
proc paretobestprintd { lambda } {
    foreach e [paretoinfog $lambda] {if {[lindex $e 4] == "balanced"} {set id [lindex $e 0]}}
    maprintd $id
    return $id
}

# print gnuplot visualization of the best map in the pareto frontier
proc paretobestprintv { lambda } {
    foreach e [paretoinfog $lambda] {if {[lindex $e 4] == "balanced"} {set id [lindex $e 0]}}
    maprintv $id
}

# print the best map
proc bestprint {} {
    maprint [lindex [lindex [heapget k 1] 0 ] 0]
    return [lindex [lindex [heapget k 1] 0 ] 0]
}

# print in detail the best map
proc bestprintd {} {
    maprintd [lindex [lindex [heapget k 1] 0 ] 0]
    return [lindex [lindex [heapget k 1] 0 ] 0]
}

# print gnuplot visualization of the best map
proc bestprintv {} {
    maprintv [lindex [lindex [heapget k 1] 0 ] 0]
}

proc mrkdel { mrk } {
	# to get the id of the marker
	set id [mrkid $mrk]
	# to get the selection
	set sel [mrkselget]
	# to remove
	set pos [lsearch $sel $id]
	set nsel [lreplace $sel $pos $pos]
	# to set the selection
	mrkselset $nsel
}

# add a set of markers from their loci ids
proc mrkadd {args} {
     set loc "[mrkselget] "
     append loc $args
     mrkselset $loc
}

# remove a set of markers from their loci ids 

proc mrksub {args} {
     set loc " [mrkselget] "
     foreach item $args {
        regsub " $item " $loc " " res
        set loc $res
      }
      mrkselset $loc
}

# returns the ids of several markers
proc mrkids {args} {
     set args [join $args]
     foreach item $args {
     lappend loc [mrkid $item]
}
return $loc	    
}

#returns the names of several markers ids

proc mrknames {args} {
     set args [join $args]
     foreach item $args {
     lappend loc [mrkname $item]
}
return $loc	    
}

proc lkh1 {name nbrun} {
    # create LKH parameter file
    if [catch {open "$name.lkh" w} fid] {
	puts stderr "Cannot open $name.lkh : $fid"
	exit
    }
    puts $fid "PROBLEM_FILE = $name.tsp"
    puts $fid "TOUR_FILE = $name.sol"
    puts $fid "TRACE_LEVEL = 1"
    puts $fid "RUNS = $nbrun"
    close $fid
    # run LKH
    set thepath [lindex $::auto_path [lsearch $::auto_path *carthagene]]
    if { $thepath == "" } { set thepath "." }
    set res [exec echo $name.lkh | LKH | awk -f $thepath/cgscript/lkh.awk]
    puts $res
    # create initial marker selection file
    if [catch {open "$name.ord" w} fid] {
	puts stderr "Cannot open $name.ord : $fid"
	exit
    }
    foreach id [mrkselget] {
	puts $fid "$id"
    }
    close $fid
    # create a new marker order from the TSP solution
    set order [exec awk -f $thepath/cgscript/lkh2cg.awk $name.ord $name.sol | grep mrkselset]
    if [catch {open "$name.tcl" w} fid] {
	puts stderr "Cannot open $name.tcl : $fid"
	exit
    }
    puts $fid $order
    close $fid
    # read the order
    source $name.tcl
    # evaluate the solution in multipoint
    puts "and, in multi-points:"
    sem
    puts ""
    return $res
}

proc concorde1 {name} {
    # run Concorde
    set thepath [lindex $::auto_path [lsearch $::auto_path *carthagene]]
    if { $thepath == "" } { set thepath "." }
    set res [exec concorde $name.tsp | awk -f $thepath/cgscript/concorde.awk]
    puts $res
    # create initial marker selection file
    if [catch {open "$name.ord" w} fid] {
	puts stderr "Cannot open $name.ord : $fid"
	exit
    }
    foreach id [mrkselget] {
	puts $fid "$id"
    }
    close $fid
    # create modified solution file
    if [catch {open "$name.sol2" w} fid] {
	puts stderr "Cannot open $name.sol2 : $fid"
	exit
    }
    set solution [exec sed -f $thepath/cgscript/csol.sed $name.sol]
    puts $fid $solution
    close $fid
    # create a new marker order from the TSP solution
    set order [exec awk -f $thepath/cgscript/concorde2cg.awk $name.ord $name.sol2 | grep mrkselset]
    if [catch {open "$name.tcl" w} fid] {
	puts stderr "Cannot open $name.tcl : $fid"
	exit
    }
    puts $fid $order
    close $fid
    # read the order
    source $name.tcl
    # evaluate the solution in multipoint
    puts "and, in multi-points:"
    sem
    puts ""
    return $res
}

proc flkh {nbrun} {
    # create TSP files
    cg2tsp tmp[pid].tsp
    set initorder [ mrkselget ]
    puts "-----------------------------------------------------------"
    puts "--- Convert to TSP without taking into account unknowns ---"
    lkh1 tmp[pid] $nbrun
    puts "---------------------------------------------------------------------------"
    puts "--- Convert to TSP with normalization (by taking into account unknowns) ---"
    mrkselset $initorder
    lkh1 normtmp[pid] $nbrun
    puts "-------------------------------------------------------------------------------"
    puts "--- Convert to TSP with LOD as cost matrix ---"
    mrkselset $initorder
    lkh1 lodtmp[pid] $nbrun
    puts "----------------------------------------------------------------------------------"
    puts "--- Convert to TSP with Haldane / Ray distance matrix ---"
    mrkselset $initorder
    lkh1 disttmp[pid] $nbrun

    mrkselset $initorder
    return
}

proc fconcorde {} {
    # create TSP files
    cg2tsp tmp[pid].tsp
    set initorder [ mrkselget ]
    concorde1 tmp[pid]
    mrkselset $initorder
    return
}

proc fconcorden {} {
    # create TSP files
    cg2tsp tmp[pid].tsp
    set initorder [ mrkselget ]
    concorde1 normtmp[pid]
    mrkselset $initorder
    return
}

proc fconcorded {} {
    # create TSP files
    cg2tsp tmp[pid].tsp
    set initorder [ mrkselget ]
    concorde1 disttmp[pid]
    mrkselset $initorder
    return
}

proc fconcordeocb {} {
    # create TSP files
    cg2tsp tmp[pid].tsp
    set initorder [ mrkselget ]
    concorde1 ocbtmp[pid]
    mrkselset $initorder
    return
}

proc fconcordeocbn {} {
    # create TSP files
    cg2tsp tmp[pid].tsp
    set initorder [ mrkselget ]
    concorde1 ocbnormtmp[pid]
    mrkselset $initorder
    return
}

proc maprintv {mapId} {
    if [catch {open "tmpmap" w} fid] {
	puts stderr "Cannot open tmpmap : $fid"
	exit
    }
    foreach id [mapordget $mapId] {
	puts $fid "$id"
    }
    close $fid
    puts stderr "Press \[return\] to return to CarthaGene."
    set thepath [lindex $::auto_path [lsearch $::auto_path *carthagene]]
    if { $thepath == "" } { set thepath "." }
    exec $thepath/cgscript/map.plot
    return
}

#deprecated command
proc maprintc {mapId} {
    if [catch {open "tmpmap" w} fid] {
	puts stderr "Cannot open tmpmap : $fid"
	exit
    }
    set themap [lindex [lindex [mapget k $mapId] 2]]
    for {set x 2} { $x < [llength $themap] } {incr x 2} {
        set id [lindex $themap $x]
	puts $fid "$id"
    }
    close $fid

    set myorder ""
    foreach jeu [::CG::dsget] {
	if {[lindex $jeu 1] == "order"} {
		set myorder "[lindex $jeu 2]"
	}
    }	
    puts stderr "Kill image viewer to return to CarthaGene."
    set thepath [lindex $::auto_path [lsearch $::auto_path *carthagene]]
    if { $thepath == "" } { set thepath "." }
    exec $thepath/cgscript/comparativemapping.sh tmpmap $myorder
    return
}

proc paretosave {arg} {
    global tcl_interactive
    #les contr�les :
    #un seul argument
    # -h -u -H ou filename
    
#    if {$tcl_interactive == 0} {
#    puts stderr "The shell does not provide this command, but the Data Menu does."
#    return }

    if {$arg =="-h"} {
	return "Save the pareto frontier."
    }

    if {$arg =="-H"} {
	puts ""
	puts "Usage : paretosave \[-h | -H | -u| fileName\]"
	puts ""
	puts "Description : paretosave save the pareto frontier with the current state of the CarthaGene session that you are running. The session with the pareto frontier is dumped to a Tcl script. You may want to run this script later to retore your work. To do this, under the Carthagene/tcl shell type the command \"source\" followed by the name of the file you mentionned to this command. The file paths of your data sets may be relatives, in this case, your dump file will depend to your current work space."
	puts ""
	return 
    }

    if {$arg =="-u"} {
	return "paretosave fileName"
    }

    if {[file exist $arg]} {
	set answer "bid"
	while {$answer != ""  & $answer != "y" & $answer != "n"} {
	    puts -nonewline "The file $arg already exist, replacing it? (\[y\] or n):"
	    flush stdout;
	    set answer [gets stdin]
	}
	if {$answer == "n"} {
	    puts stderr "Session saving aborted!"
	    return
	}
    }
    
    if [catch {open $arg w} fid] {
	puts stderr "Cannot open $arg : $fid"
    } else {
	
	paretosavegui $fid

	close $fid
	puts "The pareto frontier has been successfully saved."
	puts "To restore it, type : source $arg"
    }
}

proc mrkmerges {args} {
    if { [llength $args] == 0 } {
        set thres 0.0
    } else {
        set thres [lindex $args 0]
    }
    foreach merg [mrkdoubleget $thres] {
	foreach merged [lrange $merg 1 end] {
	    mrkmerge [lindex $merg 0] $merged
	}
    }
}

proc paretosavegui {fid} {

    puts $fid "\# CarthaGene session dump script"
    
    puts $fid "\#A: Resizing the heap."
    
    puts $fid "heapsize [::CG::heapsizeget]"
    
	puts $fid "\#B: Loading and Merging Data Sets"
    
    foreach jeu [::CG::dsget] {
	if {[lindex $jeu 1] == "merged by order"} {
		puts $fid "dsmergor [lindex $jeu 2] [lindex $jeu 3]"
	} elseif { [lindex $jeu 1] == "merged genetic"} {
	    puts $fid "dsmergen [lindex $jeu 2] [lindex $jeu 3]"
	} else {
	    puts $fid "dsload [lindex $jeu 2]"
	}
    }	
    
    puts $fid "\#C: Merging Markers."
    
    foreach merg [::CG::mrkmerget] {
	foreach merged [lrange $merg 1 end] {
	    puts $fid "mrkmerge [lindex $merg 0] $merged"
	}
    }
    
    puts $fid "\#D: Filling The Heap."
    
    foreach mapid [paretoinfo 0 1] {
	set ord [mapordget $mapid]
	puts $fid "mrkselset \{$ord\}"
	puts $fid "sem"
    }
    
    puts $fid "\#E: Selecting Markers."
    
    puts $fid "mrkselset \{[::CG::mrkselget]\}"

}

