
# awk -f lodmax.awk lod.tsp

NF>=8 {
	for(i=1;i<=NF;i++) {
		if($i<s) s=$i;
	}
}

END {
	printf("%d",-s);
}
