
BEGIN {
  first = 1;
}

FNR == NR && NF == 1 {
  n++;
  trans[n] = $1;
}

FNR != NR && NF == 1 && $1 >= 1 && $1 <= n {
  if (first) {
    seq1[n1++] = trans[$1];
  } else {
    seq2[n2++] = trans[$1];
  }    
}

FNR != NR && NF == 1 && $1 == 0 {
  first = 0;
}

END {
  for (i=0; i<n2; i++) {
    printf("%d ", seq2[i]);
  } 
  for (i=0; i<n1; i++) {
    printf("%d ", seq1[i]);
  }
}
