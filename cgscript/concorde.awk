BEGIN {
  PRECISION = 1000;
}

/Optimal Solution/ {
  opt = $3 / 1000.;
}

/Number of bbnodes/ {
  nb = $4;
}

/Total Running Time/ {
  time = $4;
}

END {
  print "TSP: optimum=",opt,"bbnodes=",nb,"totaltime=",time,"sec.";
  exit 0;
}
