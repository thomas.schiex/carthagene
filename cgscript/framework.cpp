/*****************************
     Framework mapping
	 UBIA - INRA @ 2007
******************************/

// compile with a recent GNU g++ and BOOST Graph library installed
// g++ -O2 framework.cpp -o framework -static

//#ifdef __WIN32__
//#	include <boost\\config.hpp>
//#	include <boost\\property_map\\property_map.hpp>
//#	include <boost\\graph\\adjacency_list.hpp>
//#	include <boost\\graph\\johnson_all_pairs_shortest.hpp>
//#	include <boost\\graph\\dijkstra_shortest_paths.hpp>
//#else
#	include <boost/config.hpp>
#	include <boost/property_map/property_map.hpp>
#	include <boost/graph/adjacency_list.hpp>
//#include <boost/graph/graph_utility.hpp>
//#include <boost/graph/graphviz.hpp>
//#include <boost/graph/connected_components.hpp>
//#include <boost/graph/biconnected_components.hpp>
#	include <boost/graph/johnson_all_pairs_shortest.hpp>
#	include <boost/graph/dijkstra_shortest_paths.hpp>
//#include <boost/graph/minimum_degree_ordering.hpp>
//#endif

#include <assert.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <sstream>
using namespace std;

using namespace boost;

//************************* User defined parameters
const int VERBOSE = 1;
const int DIST = 100000; // maximum possible threshold given as command line parameter
//*************************************************

namespace boost
{
  struct edge_component_t
  {
    enum
    { num = 555 };
    typedef edge_property_tag kind;
  }
  edge_component;
}

typedef adjacency_list< setS, vecS, undirectedS, no_property, 
                        property< edge_weight_t, int, property < edge_component_t, std::size_t > > > Graph;
typedef graph_traits < Graph >::vertex_descriptor Vertex;
typedef graph_traits < Graph >::edge_descriptor Edge;

//----------------------------------------------------------------------------------------------------
// add edge (vari,varj) to graph G or change its weight if it already exists
static void addConstraint(int vari, int varj, Graph& G, int theweigth)
{
    property_map<Graph, edge_weight_t>::type weight = get(edge_weight, G);
	weight[add_edge( vari, varj, G).first] = theweigth;
}

//----------------------------------------------------------------------------------------------------
// returns true if v belongs to the given path
bool belong(int size, int *path, int v)
{
  for (int i=0; i<size; i++) {
	if (path[i] == v) return true;
  }
  return false;
}

//----------------------------------------------------------------------------------------------------
// returns true if there is no shortcuts, i.e. egdes between non consecutive vertices in the path with a cost lower than or equal to max(edge weights of the path) + threshold2
bool valid(int n, int *weights1, int *weights, int threshold1, int threshold2, int size, int *path, int *erri, int *errj)
{
  *erri = -1;
  *errj = -1;
  if (size <= 2) return true;
  for (int i=0; i<size-1; i++) {
	int maxw = weights[path[i]*n+path[i+1]];
	if (weights[path[0]*n+path[size-1]] + weights1[path[i]] + weights1[path[i+1]] <= weights[path[i]*n+path[i+1]] + weights1[path[0]] + weights1[path[size-1]] + threshold2) {
	  if (VERBOSE >= 2) cout << path[i]+1 << " " << path[i+1]+1 << " " << weights[path[i]*n+path[i+1]] << " better extremities found!!!"  << endl;
	  *erri = 0;
	  *errj = size-1;
	  return false;
	}
	if (i>0 && weights[path[0]*n+path[i+1]] + weights1[path[i]] <= weights[path[i]*n+path[i+1]] + weights1[path[0]] + threshold2) {
	  if (VERBOSE >= 2) cout << path[i]+1 << " " << path[i+1]+1 << " " << weights[path[i]*n+path[i+1]] << " better extremity found"  << endl;
	  *erri = 0;
	  *errj = i+1;
	  return false;
	}
	if (i<size-2 && weights[path[i]*n+path[size-1]] + weights1[path[i+1]] <= weights[path[i]*n+path[i+1]] + weights1[path[size-1]] + threshold2) {
	  if (VERBOSE >= 2) cout << path[i]+1 << " " << path[i+1]+1 << " " << weights[path[i]*n+path[i+1]] << " better extremity found"  << endl;
	  *erri = i;
	  *errj = size-1;
	  return false;
	}
	for (int j=i+2; j<size; j++) {
	  if (weights[path[j-1]*n+path[j]] > maxw) maxw = weights[path[j-1]*n+path[j]];
	  if (weights[path[i]*n+path[j]] <= maxw + (threshold2/2)) {
		if (VERBOSE >= 2) cout << path[i]+1 << " " << path[j]+1 << " " << weights[path[i]*n+path[j]] << " " << maxw << endl;
		*erri = i;
		*errj = j;
		return false;
	  }
	  if (i>0 && j<size-1 && 
		  ((weights[path[0]*n+path[i+1]] + weights[path[j-1]*n+path[size-1]] + weights1[path[i]] + weights1[path[j]] <= weights[path[i]*n+path[i+1]] + weights[path[j-1]*n+path[j]] + weights1[path[0]] + weights1[path[size-1]] + threshold2) ||
		   (weights[path[0]*n+path[j-1]] + weights[path[i+1]*n+path[size-1]] + weights1[path[i]] + weights1[path[j]] <= weights[path[i]*n+path[i+1]] + weights[path[j-1]*n+path[j]] + weights1[path[0]] + weights1[path[size-1]] + threshold2))) {
		if (VERBOSE >= 2) cout << path[i]+1 << " " << path[j]+1 << " " << weights[path[i]*n+path[j]] << " better extremities found!!!"  << endl;
		*erri = 0;
		*errj = size-1;
		return false;
	  }
	}
  }
  return true;
}

//----------------------------------------------------------------------------------------------------
// Finds the diameter of graph G in maximum number of vertices
// and returns its corresponding longest shortest path with its cost
//----------------------------------------------------------------------------------------------------
int diameter(Graph &G, int n, int *weights1, int *weights, int threshold1, int threshold2, int **D, int *path, int *cost)
{
  // find the extremities of the longest shortest path
  int first = -1;
  int last = -1;
  johnson_all_pairs_shortest_paths(G, D);
  int maxd = 0;
  for (int i = 0; i < num_vertices(G); ++i) {
    for (int j = 0; j < num_vertices(G); ++j) {
        if (D[i][j] < INT_MAX) {
		  if (D[i][j] - weights1[i] - weights1[j] > maxd) {
			maxd = D[i][j] - weights1[i] - weights1[j]; // prefer small Markov Chain prior !?
			first = i;
			last = j;
		  }
		}
    }
  }
  if (VERBOSE>=3) cout << "Diameter:" << maxd << endl;

  // finds the corresponding longest shortest path and its cost
  int size = 1;
  *cost = weights1[first] + weights1[last];
  if (VERBOSE>=3) cout << first+1;
  path[0] = first;
  while (first != last) {
	int best = -1;
	int delta = INT_MAX;
	for (int i = 0; i < num_vertices(G); ++i) {
	  if (i != first && !belong(size,path,i) && 
		  edge(vertex(first, G),vertex(i, G),G).second &&
		  D[first][i] < INT_MAX && D[i][last] < INT_MAX && D[first][i] + D[i][last] < delta) {
		best = i;
		delta = D[first][i] + D[i][last];
	  }
	}
	if (VERBOSE>=3) cout << " " << best+1;
	path[size] = best;
	*cost += weights[best*n+first];
	size++;
	first = best;
  }
  if (VERBOSE>=3) cout << endl;
  if (VERBOSE>=3) cout << "Size= " << size << endl;
  if (VERBOSE>=3) cout << "Cost= " << *cost << endl;
  return size;
}

//----------------------------------------------------------------------------------------------------
// Finds the shortest path between first and last vertices of graph G
// and returns its corresponding path with its cost
//----------------------------------------------------------------------------------------------------
int shortestpath(Graph &G, int n, int *weights1, int *weights, int threshold1, int threshold2, int first, int last, int *path, int *cost)
{
  // find shortest path between first and last
  vector<Vertex> P(num_vertices(G));
  vector<int> D(num_vertices(G));
  dijkstra_shortest_paths(G, vertex(first, G), predecessor_map(&P[0]).distance_map(&D[0]));
  if (D[last] == INT_MAX) {
	if (VERBOSE>=3) cout << "No shortest path!" << endl;
	return 0;
  }
  if (VERBOSE>=3) cout << "Shortest path: " << D[last] << endl;

  // finds the corresponding shortest path and its cost
  int size = 1;
  *cost = weights1[first] + weights1[last];
  path[0] = last;
  while (first != last) {
	path[size] = P[last];
	*cost += weights[P[last]*n+last];
	size++;
	last = P[last];
  }
  for (int j = 0; j < size/2; ++j) {
	int tmp = path[j];
	path[j] = path[size - j - 1];
	path[size - j - 1] = tmp;
  }
  for (int j = 0; j < size; ++j) {
	if (VERBOSE>=3) cout << " " << path[j]+1;
  }
  if (VERBOSE>=3) cout << endl;
  if (VERBOSE>=3) cout << "Size= " << size << endl;
  if (VERBOSE>=3) cout << "Cost= " << *cost << endl;
  return size;
}

//----------------------------------------------------------------------------------------------------
// Finds the longest simple path of a given TSP with the following constraints:
// * first proposed order has all the edges in the path with a cost lower than or equal to Threshold
// * there is no shortcuts, i.e. egdes between non consecutive vertices with a cost lower than or equal to max(consecutive edges costs between i and j)+Delta
//----------------------------------------------------------------------------------------------------
int main(int argc, char **argv)
{
  //***************************************
  // check and read command line parameters
  if (argc < 4 || argc > 5) {
	cerr << "Usage: " << argv[0] << " TSPFileName Threshold Delta [Distortion]" << endl;
	exit(EXIT_FAILURE);
  }
  ifstream file(argv[1]);
  if (!file) {
	cerr << "Could not open file " << argv[1] << endl;
	exit(EXIT_FAILURE);
  }
  int threshold1 = atoi(argv[2]);
  cout << "Threshold= " <<  threshold1 << endl;
  int threshold2 = atoi(argv[3]);
  cout << "Delta= " <<  threshold2 << endl;
  int option = -1;
  if (argc==5) {
	option = atoi(argv[4]);
  }
  cout << "Distortion= " << option << endl;

  //*************************************************************
  // parse TSP file (must contain an EDGE_WEIGHT_SECTION section)
  // read the number of markers
  string word;
  while (file && word != "DIMENSION:") {
	file >> word;
  }
  int n;
  file >> n;
  n--; // do not count dummy initial marker
  // skip header
  while (file && word != "EDGE_WEIGHT_SECTION") {
	file >> word;
  }
  // read dummy initial marker distances
  int* weights1 = new int[n];
  for (int i=0; i<n; i++) {
	file >> weights1[i];
  }
  // create BOOST graph
  Graph G;
  for (int i=0; i<n; i++) add_vertex(G);
  // create weight matrix
  int* weights = new int[n*n];
  // read marker distance upper_row matrix
  for (int i=0; i<n; i++) {
	for (int j=i+1; j<n; j++) {
	  int dist;
	  file >> dist;
	  weights[i*n+j] = dist;
	  weights[j*n+i] = dist;
	  // enforce triangle inequality
	  assert(option>=0 || threshold1+1 <= DIST);
	  if (dist <= threshold1) addConstraint(i,j,G,(option>=0)?dist+option:DIST+dist);
	}
  }

  //****************************************************************************************
  // finds longest shortest path and fixes the extremities using slow O(n*e*ln(n)) algorithm
  typedef int *int_ptr;
  int **D;
  D = new int_ptr[num_vertices(G)];
  for (int i = 0; i < num_vertices(G); ++i) D[i] = new int[num_vertices(G)];
  int* bestpath = new int[num_vertices(G)];
  int bestcost = -1;
  int bestsize = diameter(G, n, weights1, weights, threshold1, threshold2, D, bestpath, &bestcost);

  int* verybestpath = new int[num_vertices(G)];
  int verybestcost = INT_MAX;
  int verybestsize = 0;

  int oldsize = 0;
  while (1) {
	bool newpath = false;
	//*******************************************
	// finds longest simple path with constraints
	Graph bestG = G;
	int* curpath = new int[num_vertices(G)];
	for (int j = 0; j < bestsize; ++j) curpath[j] = bestpath[j];
	int cursize = bestsize;
	int curcost = bestcost;
	bool stop = false;
	while (!stop) {
	  stop = true;
	  int* bpath = new int[num_vertices(G)];
	  for (int j = 0; j < cursize; ++j) bpath[j] = curpath[j];
	  int bsize = cursize;
	  int bcost = INT_MAX; // allows longer paths to be visited in order to find more vertices to insert
	  int remove = -1;
	  // do not try to remove path extremities
	  for (int vertexpos = 1; vertexpos < cursize - 1; vertexpos++) {
		Graph G2 = G;
		if (VERBOSE>=3) cout << "Try removing vertex " << curpath[vertexpos]+1 << endl;
		clear_vertex(vertex(curpath[vertexpos],G2), G2);
		int* path = new int[n];
		int cost = -1;
		// fast O(n*ln(n)) shortest path algorithm to find a new path after one vertex removal
		int size = shortestpath(G2, n, weights1, weights, threshold1, threshold2, curpath[0], curpath[cursize-1], path, &cost);
		// if equal size then prefer lower cost paths
		if (size > bsize || (size==bsize && cost < bcost)) {
		  for (int j = 0; j < size; ++j) bpath[j] = path[j];
		  bsize = size;
		  bcost = cost;
		  remove = vertexpos;
		  stop = false;
		}
                delete[] path;
	  }
	  if (!stop) {
		if (VERBOSE>=3) cout << "******* Remove vertex " << curpath[remove]+1 << endl;	  
		clear_vertex(vertex(curpath[remove],G), G);
		cursize = bsize;
		curcost = bcost;
		for (int j = 0; j < cursize; ++j) {
		  curpath[j] = bpath[j];
		  if (VERBOSE>=3) cout << " " << curpath[j]+1;
		}
		if (VERBOSE>=3) cout << endl;
		if (VERBOSE>=3) cout << "Size= " << cursize << endl;
		if (VERBOSE>=3) cout << "Cost= " << curcost << endl;
		//		assert(cursize!=bestsize || curcost >= bestcost);
		if (cursize > bestsize) {
		  for (int j = 0; j < cursize; ++j) {
			bestpath[j] = curpath[j];
		  }
		  bestsize = cursize;
		  bestcost = curcost;
		  bestG = G;
		  newpath = true;
		  if (VERBOSE>=1) cout << bestsize << endl;
		}
	  }	  
          delete[] bpath;
	}
	G = bestG; // restore graph G when best path has been found

	for (int j = 0; j < bestsize; ++j) {
	  cout << " " << bestpath[j]+1;
	}
	cout << " (" << bestsize << "," << bestcost << ")" << endl;

	// check if there are no shortcuts with cost <= threshold2
	int erri;
	int errj;
	bool reduced = false;
	while (!valid(n,weights1,weights,threshold1,threshold2,bestsize,bestpath,&erri,&errj)) {
	  reduced = true;
	  int oldcost = bestcost;
	  if (option>=0) {
		int maxw = 0;
		int maxpos;
		for (int j = erri; j < errj; ++j) {
		  if (weights[bestpath[j]*n+bestpath[j+1]] > maxw) {
			maxw = weights[bestpath[j]*n+bestpath[j+1]];
			maxpos = j;
		  }
		}
		if (VERBOSE>=2) cout << "penalize: " << bestpath[maxpos]+1 << " " << bestpath[maxpos+1]+1 << " " << maxw << " " << bestpath[erri]+1 << " " << bestpath[errj]+1 << endl;
		addConstraint(bestpath[maxpos],bestpath[maxpos+1],G,weights[bestpath[erri]*n+bestpath[errj]]+DIST+1);
	  }
	  addConstraint(bestpath[erri],bestpath[errj],G,(option>=0)?weights[bestpath[erri]*n+bestpath[errj]]+option:DIST+weights[bestpath[erri]*n+bestpath[errj]]);
	  bestsize = shortestpath(G, n, weights1, weights, threshold1, threshold2, bestpath[0], bestpath[bestsize-1], bestpath, &bestcost);
	  for (int j = 0; j < bestsize; ++j) {
		cout << ((j>0 && weights[bestpath[j-1]*n+bestpath[j]]>threshold1)?"-":" ") << bestpath[j]+1;
	  }
	  cout << " (+" << weights[bestpath[erri]*n+bestpath[errj]] << " -" << oldcost - bestcost + weights[bestpath[erri]*n+bestpath[errj]] << ")" << endl;
	}
	if (bestsize > verybestsize || (bestsize == verybestsize && bestcost < verybestcost)) {
	  for (int j = 0; j < bestsize; ++j) {
		verybestpath[j] = bestpath[j];
	  }
	  verybestsize = bestsize;
	  verybestcost = bestcost;
	}
	if (bestsize <= oldsize && !newpath && !reduced) break;
	oldsize = bestsize;
        delete[] curpath;
  }

  if (VERBOSE>=2) cout << "<" << weights1[verybestpath[0]] << ">";
  for (int j = 0; j < verybestsize; ++j) {
	cout << " " << verybestpath[j]+1;
	if (VERBOSE>=2) cout << " <" <<  ((j==verybestsize-1)? weights1[verybestpath[verybestsize-1]]:weights[verybestpath[j]*n+verybestpath[j+1]]) << ">";
  }
  cout << " (" << verybestsize << "," << verybestcost << ")" << endl;

  for (int i = 0; i < num_vertices(G); ++i) delete[] D[i];
  delete[] D;
  delete[] bestpath;
  delete[] verybestpath;
  delete[] weights1;
  delete[] weights;

  return 0;
}
