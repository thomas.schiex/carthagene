BEGIN {
  PRECISION = 1000;
}

/Lower bound/ {
  gsub(",","");
  lb = $4 / 1000.;
} 

/Cost[.]min/ {
  opt = $3 / 1000.;
}

/RUNS/ {
  nb = $3;
}

/Time[.]avg/ {
  time = $7;
}

END {
  print "TSP: optimum=",opt,"lowerbound=",lb,"gap=",(opt-lb)/lb*100,"%","totaltime~=",time*nb,"sec.";
}
