#!/usr/bin/gnuplot

plot 'tmpmap' notitle
pause -1

#set term postscript eps monochrome dashed "helvetica" 26
set term postscript eps color dashed
set output "map.eps"
set size 1,1
set origin 0.,0.
replot
