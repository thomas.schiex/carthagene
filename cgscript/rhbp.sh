#!/bin/sh

# usage: ./rhbp.sh RH_filename Order_filename

###########################################################################
# creates two files containing the resulting pareto frontiers 
# and the best map using a comparative RH mapping approach
# w.r.t. two datasets: Radiated Hybrids (RH_filename) and a Reference Order (Order_filename)

# fichier_Ordre.lkhXX.pareto contains the initial frontier found by an approximation with the TSP
# fichier_Ordre.greedyXX.pareto contains the improved frontier found by a 2-opt greedy algorithm

# creates also two files containing a pareto frontier
# fichier_Ordre.lkhXX.pareto contains the initial frontier found by an approximation with the TSP
# fichier_Ordre.greedyXX.pareto contains the improved frontier found by a 2-opt greedy algorithm

# for further use of these pareto frontiers, use TCL commands:
# (to reload the frontier in the Carthagene heap)
# source fichier_Ordre.greedyXX.pareto
# (to show the best map)
# paretobestprintd 1
###########################################################################

######## Find the initial frontier found by an approximation with the TSP

# initial computational effort for creating a pseudo-pareto frontier (lkh in [1,NbMarqueurs])
lkh=20

# find an initial pseudo-pareto frontier using paretolkh
(echo "paretofind $1 $2 $2.lkh${lkh}.log $2.lkh${lkh}.pareto $lkh 1 1 1" ; echo "exit") | tclsh

######## Find the improved frontier found by a 2-opt greedy algorithm

# improve the previous frontier around the best map (size in [1,NbMarqueurs])
size=1

# limit 2-opt neighborhood exploration (anytime in [1,100])
anytime=25

# limit computation time for paretogreedyr (time in seconds)
ulimit -St 3600

# improve the frontier using paretogreedyr
(echo "paretoimprove $2.lkh${lkh}.pareto $2.greedy${anytime}.log $2.greedy${anytime}.pareto $size $anytime 1 1" ; echo "exit") | tclsh
