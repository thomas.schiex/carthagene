
# awk -f shiftlod.awk lod.tsp lod.tsp > poslod.tsp

FNR==NR && NF>=8 {
 for(i=1;i<=NF;i++) {
	 if($i<s) s=$i;
 }
}

FNR!=NR {
 if(FNR>=8) {
	 for (i=1;i<=NF;i++) $i-=s;
 };
 print $0;
}
