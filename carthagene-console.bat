@echo off

if not exist "%HOMEPATH%\carthagenerc.tcl"	copy bin\sample_carthagenerc	"%HOMEPATH%/carthagenerc.tcl"
if not exist "%HOMEPATH%\cgwopt" 		copy bin\sample_cgwopt		"%HOMEPATH%/cgwopt"
if not exist "%HOMEPATH%\cgwdefalgo" 		copy bin\sample_cgwdefalgo	"%HOMEPATH%/cgwdefalgo"

pushd
cd tcl
tkcon cgsh\cgbootstrap.tcl
popd

