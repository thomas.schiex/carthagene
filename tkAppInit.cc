/*
 * tkAppInit.c --
 *
 *	Provides a default version of the Tcl_AppInit procedure for use in
 *	wish and similar Tk-based applications.
 *
 * Copyright (c) 1993 The Regents of the University of California.
 * Copyright (c) 1994-1997 Sun Microsystems, Inc.
 *
 * See the file "license.terms" for information on usage and redistribution of
 * this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 * RCS: @(#) $Id: tkAppInit.cc,v 1.2.2.2 2011-03-15 17:07:58 dleroux Exp $
 */

#include "tk.h"
#include "locale.h"
#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*int Cartagene_Init(Tcl_Interp *);*/
/*int Cartagene_SafeInit(Tcl_Interp *);*/
/*#define Cartagene_Init Cartagene_SafeInit*/


/*
 *----------------------------------------------------------------------
 *
 * main --
 *
 *	This is the main program for the application.
 *
 * Results:
 *	None: Tk_Main never returns here, so this procedure never returns
 *	either.
 *
 * Side effects:
 *	Whatever the application does.
 *
 *----------------------------------------------------------------------
 */

volatile int run_gui=1;

int
main(
		int argc,			/* Number of command-line arguments. */
		char **argv)		/* Values of command-line arguments. */
{

	/*
	 * The following #if block allows you to change the AppInit function by
	 * using a #define of TCL_LOCAL_APPINIT instead of rewriting this entire
	 * file. The #if checks for that #define and uses Tcl_AppInit if it
	 * doesn't exist.
	 */

#ifndef TCL_LOCAL_APPINIT
#define TCL_LOCAL_APPINIT Tcl_AppInit
#endif
#ifndef TK_LOCAL_APPINIT
#define TK_LOCAL_APPINIT Tk_AppInit
#endif
	extern int TCL_LOCAL_APPINIT _ANSI_ARGS_((Tcl_Interp *interp));
	extern int TK_LOCAL_APPINIT _ANSI_ARGS_((Tcl_Interp *interp));

	/*
	 * The following #if block allows you to change how Tcl finds the startup
	 * script, prime the library or encoding paths, fiddle with the argv,
	 * etc., without needing to rewrite Tk_Main()
	 */

    Tk_Main(argc, argv, TK_LOCAL_APPINIT);

	return 0;			/* Needed only to prevent compiler warning. */
}



void read_cgrc(Tcl_Interp *interp) {
	char rcpath[256];
	char* home = getenv("HOME");
#ifdef __WIN32__
	sprintf(rcpath, "%s/carthagenerc.tcl", home);
#else
	sprintf(rcpath, "%s/.carthagenerc", home);
#endif
	if(run_gui) {
		Tcl_Eval(interp, "wm withdraw .");
	}
    Tcl_Eval(interp, "lappend auto_path .");
    Tcl_Eval(interp, "package require CGsh");
    if(run_gui) {
        Tcl_Eval(interp, "source gui/CGW.tcl");
    }
	Tcl_EvalFile(interp, rcpath);
}



/*
 *----------------------------------------------------------------------
 *
 * Tcl_AppInit --
 *
 *	This procedure performs application-specific initialization. Most
 *	applications, especially those that incorporate additional packages,
 *	will have their own version of this procedure.
 *
 * Results:
 *	Returns a standard Tcl completion code, and leaves an error message in
 *	the interp's result if an error occurs.
 *
 * Side effects:
 *	Depends on the startup script.
 *
 *----------------------------------------------------------------------
 */

int
Tk_AppInit(Tcl_Interp *interp)		/* Interpreter for application. */
{
	if (Tcl_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}
	if (Tk_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}
	Tcl_StaticPackage(interp, "Tk", Tk_Init, Tk_SafeInit);
#ifdef TK_TEST
	if (Tktest_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}
	Tcl_StaticPackage(interp, "Tktest", Tktest_Init,
			(Tcl_PackageInitProc *) NULL);
#endif /* TK_TEST */


	/*printf("Declaring static package Carthagene.\n");*/
	/*Tcl_StaticPackage(interp, "CarthaGene", Cartagene_Init,*/
			/*Cartagene_SafeInit);*/

	/*Tcl_Eval(interp, "package require CarthaGene 1.1");*/

	/*
	 * Call the init procedures for included packages. Each call should look
	 * like this:
	 *
	 * if (Mod_Init(interp) == TCL_ERROR) {
	 *     return TCL_ERROR;
	 * }
	 *
	 * where "Mod" is the name of the module.
	 */

	/*if(Cartagene_Init(interp) == TCL_ERROR) {*/
		/*return TCL_ERROR;*/
	/*}*/
	/*printf("All init supposedly done.\n");*/

	/*
	 * Call Tcl_CreateCommand for application-specific commands, if they
	 * weren't already created by the init procedures called above.
	 */

	/*
	 * Specify a user-specific startup file to invoke if the application is
	 * run interactively. Typically the startup file is "~/.apprc" where "app"
	 * is the name of the application. If this line is deleted then no user-
	 * -specific startup file will be run under any conditions.
	 */

	/*#ifdef __WIN32__*/
	/*Tcl_SetVar(interp, "tcl_rcFileName", "carthagenerc.tcl", TCL_GLOBAL_ONLY);*/
	/*#else*/
	/*Tcl_SetVar(interp, "tcl_rcFileName", "~/.carthagenerc", TCL_GLOBAL_ONLY);*/
	/*#endif*/

	/*if(run_gui) {*/
		/*printf("Running GUI now.\n");*/
		/*if(Tcl_EvalFile(interp, INSTALL_PREFIX "/share/carthagene/cgwin/CGW.tcl")==TCL_ERROR) {*/
			/*return TCL_ERROR;*/
		/*}*/
	/*} else {*/
	/*}*/

	/*read_cgrc(interp);*/

    Tcl_Eval(interp, "lappend auto_path .");
    /*Tcl_Eval(interp, "package require CGsh");*/
    /*if(run_gui) {*/
        /*Tcl_EvalFile(interp, "gui/CGW.tcl");*/
    /*}*/
	return TCL_OK;
}


int
Tcl_AppInit(Tcl_Interp *interp)		/* Interpreter for application. */
{
	if (Tcl_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}

#ifdef TCL_TEST
#ifdef TCL_XT_TEST
	if (Tclxttest_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}
#endif
	if (Tcltest_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}
	Tcl_StaticPackage(interp, "Tcltest", Tcltest_Init,
			(Tcl_PackageInitProc *) NULL);
	if (TclObjTest_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}
	if (Procbodytest_Init(interp) == TCL_ERROR) {
		return TCL_ERROR;
	}
	Tcl_StaticPackage(interp, "procbodytest", Procbodytest_Init,
		Procbodytest_SafeInit);
#endif /* TCL_TEST */

	/*Tcl_StaticPackage(interp, "CartaGene", Cartagene_Init,*/
		/*Cartagene_SafeInit);*/
	/*read_cgrc(interp);*/
	return TCL_OK;
}





/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 4
 * fill-column: 78
 * End:
 */
