//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
//  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
//
// $Id: cgsh.i,v 1.92.2.5 2012-06-13 14:19:49 dleroux Exp $
//
//    This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Module       : cgsh
// Projet       : CartaGene
//
// Description  : Définition de "l''interface" de SWIG.
// Ce fichier sert à générer le fichier d''interface de SWIG pour Tcl.
// Il sert aussi à générer la documentation.
//
// Ce fichier est très DELICAT à utiliser, car il contient du :
// - code SWIG
// - code C + librairie Tcl pour "améliorer" l''interface.
// - code LaTex pour la documentation utilisateur.
//-----------------------------------------------------------------------------


%module CartaGene

%include typemaps.i

%{
/*#include "config.h"*/
/*#include "cgsh.h"*/
/* carthagene_all.h is now the main include. precompiled. I disable it here because g++ can't actually include it after C tokens in the generated file. But it is -include'd anyways. So it's fine this way. */
/*#include "carthagene_all.h"*/
#include <signal.h>
Tcl_Interp *linterp;
Tcl_Channel fout;
Tcl_Channel ferr;
extern void hand(int); 
%}

%init %{

Tcl_InitStubs(interp, "8.4", 0);
/*assert(tclStubs || "This TCL installation doesn't support stubs.");*/

linterp = interp;
fout = Tcl_GetStdChannel(TCL_STDOUT);
ferr = Tcl_GetStdChannel(TCL_STDERR);
Tcl_PkgProvide(interp, "CarthaGene", CG_VERSION_STR);
signal(SIGINT, hand);
%}

%{
#ifdef ihm
#define HELPMESS(message, usage, use, description) \
  if (objc > 1) \
  {\
    Tcl_Obj * tcl_result;\
    int temphelp;\
    if ((arg1 = Tcl_GetStringFromObj(objv[1], &temphelp)) == NULL) \
      return TCL_ERROR; \
    if (strcmp(arg1,"-u") == 0) \
    {\
      tcl_result = Tcl_GetObjResult(interp);\
      Tcl_SetStringObj(tcl_result,\
	  	     use, -1);\
      return TCL_OK;\
    }\
    if (strcmp(arg1,"-h") == 0) \
    {\
      tcl_result = Tcl_GetObjResult(interp);\
      Tcl_SetStringObj(tcl_result,\
		     message, -1);\
      return TCL_OK;\
    }\
    if (strcmp(arg1,"-H") == 0) \
    {\
       sprintf(bouf,"\nUsage : %s\n\nDescription : %s\n\n", usage, description);\
       sprintf(boufi, "puts -nonewline {%s}", bouf); Tcl_Eval(linterp, boufi);\
       if (Fout != NULL) \
         fprintf(Fout,"\nUsage : %s\n\nDescription : %s\n\n", usage, description);\
       fflush(stdout);\
       return TCL_OK;\
    }\
  }
#else
#define HELPMESS(message, usage, use, description) \
  if (objc > 1) \
  {\
    int temphelp;\
    Tcl_Obj * tcl_result;\
    if ((arg1 = Tcl_GetStringFromObj(objv[1], &temphelp)) == NULL) \
      return TCL_ERROR; \
    if (strcmp(arg1,"-u") == 0) \
    {\
      tcl_result = Tcl_GetObjResult(interp);\
      Tcl_SetStringObj(tcl_result,\
	  	     use, -1);\
      return TCL_OK;\
    }\
    if (strcmp(arg1,"-h") == 0) \
    {\
      tcl_result = Tcl_GetObjResult(interp);\
      Tcl_SetStringObj(tcl_result,\
		     message, -1);\
      return TCL_OK;\
    }\
    if (strcmp(arg1,"-H") == 0) \
    {\
       sprintf(bouf,"\nUsage : %s\n\nDescription : %s\n\n", usage, description);\
       Tcl_Write(fout,bouf,strlen(bouf));\
       fflush(stdout);\
       return TCL_OK;\
    }\
  }
#endif
%}

%typemap(check) char help[3]()
{
  if (Fout != NULL)
    {
      int temphelp;
      fprintf(Fout, "\nCG_log>");
      for (int i = 0; i < objc; i++)
	fprintf(Fout, " %s", Tcl_GetStringFromObj(objv[i], &temphelp));
      fprintf(Fout, "\n");
    }
}


//----------------------- la commande dsload

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Load a Biological Data Set.", "dsload [-h | -H | -u | FileName]" ,"dsload FileName" ,"dsload loads a data set. The file format expected for a biologiacl data set is MapMaker like. To load a set of constraint, a specific format is required. The command returns a list containing either the ID of the Biological Data Set, The type of population, the number of individuals and the number of loci,  or the ID of Data Set and the number of constraint loaded.");
}

%typemap(in,numinputs=0) char **textout(char *temp)
{
  $1 = &temp;
}

%typemap(argout) char **textout
{
  Tcl_Obj *o;
  o = Tcl_NewStringObj(*$1, strlen(*$1));
  delete [] *$1;      
  Tcl_ListObjAppendElement(interp,$result,o);
  if (Fout != NULL)
    {
      int temphelp;
      fprintf(Fout, " %s\n", Tcl_GetStringFromObj($result, &temphelp));
    }
}

void dsload(char help[3], char FileName[256], char **textout);


//----------------------- la commande dsmergen

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Merge two Biological Data Sets.", "dsmergen [-h | -H | -u | SetID1 SetID2]", "dsmergen SetID1 SetID2", "dsmergen merges two Biological Data Set loaded, in a new data set which it creates. The Data Set Tree is updated, the root of the tree points to the new data set. The command returns a list containing either the ID of the Data Set, The type of population, the number of individuals and the number of loci, or the ID of Data Set and the number of constraimt loaded.");
}



void dsmergen(char help[3],int SetID1, int SetID2, char **textout);




//----------------------- la commande dsmergor

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Merge by order two Biological Data Sets.", "dsmergor [-h | -H | -u | SetID1 SetID2]", "dsmergor SetID1 SetID2", "dsmergor merges two Data Set loaded, in a new data set which it creates. This newly created Data Set enables to evaluate an order of makers. But in this case, the position is considered relative two the original Data Set. The Data Set Tree is updated, the root of the tree points to the new data set. The command returns a list containing the ID of the Biological Data Set, The type of population, the number of individuals and the number of loci.");
}



void dsmergor(char help[3],int SetID1, int SetID2, char **textout);




//----------------------- la commande dsinfo

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Summarize the current data sets.", "dsinfo [-h | -H | -u]" ,"dsinfo", "dsinfo prints a table, summarizing the biological informations loaded, Data Set by Data Set");
}



void dsinfo(char help[3]);




//----------------------- la commande dsget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get a list of the current data sets description.", "dsget [-h | -H | -u]" ,"dsget", "dsget returns a list of strings. Each string summarize a Data Set of the current session.");
}

%typemap(in,numinputs=0) char ***ltextout(char **temp)
{
  $1 = &temp;
}

%typemap(argout) char ***ltextout
{
  int idt = 0;
  while ((*$1)[idt] != 0)
    {
      Tcl_ListObjAppendElement(interp,
			       $result,
			       Tcl_NewStringObj((*$1)[idt],strlen((*$1)[idt])));
      delete [] (*$1)[idt++];
    }
  delete [] *$1;
  if (Fout != NULL)
    {
      int temphelp;
    fprintf(Fout, " %s\n", Tcl_GetStringFromObj($result, &temphelp));
    }
}



void dsget(char help[3], char ***ltextout);




//----------------------- la commande mrkinfo

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Tell which data set each marker belongs to.", "mrkinfo [-h | -H | -u]", "mrkinfo", "mrkinfo prints a table, summarizing the biological informations loaded, Marker by Data Marker");
}



void mrkinfo(char help[3]);




//----------------------- la commande mrkselset

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Set the marker list(by Id).", "mrkselset [-h | -H | -u | MrkList]", "mrkselset MrkList", "mrkselset enables to work on a sublist of the loci loaded. By default, each locus is selected.");
}

%typemap(in) int [ANY] (int temp[$dim0]) 
{
  int objcount ,i, objet;
  Tcl_Obj **objvect;

  if (Tcl_ListObjGetElements(interp,
                             $input,
                             &objcount,
                             &objvect) == TCL_ERROR)
    return TCL_ERROR;
  
  if (objcount >= $dim0)  
    { 
      interp->result = (char*)"Array size mismatch!"; 
      return TCL_ERROR; 
    } 
  
  for (i = 0; i < objcount; i++) 
   { 
     if (Tcl_GetIntFromObj(interp,  
                           objvect[i],   
                           &objet) == TCL_ERROR)  
        return TCL_ERROR; 
     temp[i] = objet; 
   }

  for (i = objcount; i < $dim0; i++)
    temp[i] = 0;
  
  $1 = temp;
}



void mrkselset(char help[3], int MrkList[NBM]);




//----------------------- la commande mrkselget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Return the loci selection into a list.", "mrkselget [-h | -H | -u]", "mrkselget", "mrkselget returns a list containing each selected locus. The markers are referenced by Id.");
}

%typemap(in,numinputs=0) int **lmiout(int *temp)
{
  $1 = &temp;
}

%typemap(argout) int **lmiout
{
  int idm = 0;
  while ((*$1)[idm] != 0)
    Tcl_ListObjAppendElement(interp,
                             $result,
                             Tcl_NewIntObj((*$1)[idm++]));
                             
  delete [] *$1;
  if (Fout != NULL)
    {
      int temphelp;
      fprintf(Fout, " %s\n", Tcl_GetStringFromObj($result, &temphelp));
    }
}



void mrkselget(char help[3], int **lmiout);




//----------------------- la commande mrkallget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Return all the loci into a list.", "mrkallget [-h | -H | -u]", "mrkallget", "mrkallget returns a list containing each locus. The markers are referenced by Id.");
}



void mrkallget(char help[3], int **lmiout);




//----------------------- la commande mrklod2p

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Print the two points LOD matrix.", "mrklod2p [-h | -H | -u]", "mrklod2p", "mrklod2p prints the two points LOD matrix of the loci selection list.");
}



void mrklod2p(char help[3]);




//----------------------- la commande mrkdouble

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Identifies pairs of markers with compatible typing.", "mrkdouble [-h | -H | -u]", "mrkdouble", "mrkdouble dumps all pairs of markers in the current selection which have compatible typing.");
}



void mrkdouble(char help[3]);




//----------------------- la commande mrkdist2p

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Print the two points distance matrix.", "mrkdist2p [-h | -H | -u | UnitFlag]", "mrkdist2p UnitFlag", "mrkdist2p prints the two points distance matrix of the loci selection list.  The UnitFlag argument allows to choose either the Kosambi(k) unit or the Haldane(h) unit. For radiated hybrids, this flag is not active.");
}



void mrkdist2p(char help[3], char UnitFlag[2]);




//----------------------- la commande mrkfr2p

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Print the two points FR/Breaks matrix.", "mrkfr2p [-h | -H | -u ]", "mrkfr2p", "mrkfr2p prints the two points FR/Breaks(RH) matrix of the loci selection list.  ");
}



void mrkfr2p(char help[3]);




//----------------------- la commande mrkmerge

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Merges two compatible markers.", "mrkmerge [-h | -H | -u | MrkID1 MrkId2]", "mrkmerge MrkID1 MrkId2", "mrkmerge merges two compatibles markers. The result of the merging is stored in the first marker. The other one is untouched and should not be selected.");
}



void mrkmerge(char help[3], int MrkID1, int MrkId2);

//----------------------- la commande mrkmerget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Retrive a list of merged markers.", "mrkmerget [-h | -H | -u]", "mrkmerget", "mrkmerget returns a list of list of ids of loci. The markers who are in the same list were merged together. The first locus of the sublist represents the others.");
}



void mrkmerget(char help[3],char ***ltextout);

//----------------------- la commande mrkdoubleget
				  
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Retrieve a list of pairs of markers with compatible typing.", "mrkdoubleget [-h | -H | -u] [LOD threshold]", "mrkdoubleget", "mrkdoubleget returns a list of pair of ids of loci. The markers who are in the same pair have compatible typing. These pairs are sorted by decreasing LOD. An optional LOD threshold can be provided to filter out unlikely pairs (default is 0.0).");
}



void _mrkdoubleget(char help[3], double lod_threshold, char ***ltextout);



//----------------------- la commande mrkname

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get the name of a locus.", "mrkname [-h | -H | -u | MrkID]", "mrkname MrkID", "mrkname returns a string containing the name of a knowned locus. The locus is referenced by its integer Id.");
}



void mrkname(char help[3], int MrkID, char **textout);




//----------------------- la commande mrkid

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get the integer id of a locus.", "mrkid [-h | -H | -u | MrkName]", "mrkid MrkName", "mrkid returns a integer. The locus is referenced by its name.");
}



int mrkid(char help[3], char * MrkName);




//----------------------- la commande mrklast

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get the integer id of the last known locus.", "mrklast [-h | -H | -u]", "mrklast", "mrklast returns the integer id of the last known locus.");
}



int mrklast(char help[3]);


//----------------------- la commande verbset

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Make the search process verbose.", "verbset [-h | -H | -u | Flag]", "verbset Flag", "verbset sets the level of verbosity of the search process. Three levels are available(0, 1 or 2). The verbosity has no effect when the the Quiet Flag is set to 1.");
}



void verbset(char help[3], int Flag);


//----------------------- la commande cgprecision

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Controls the number of digits printed for estimated distances.", "cgprecision [-h | -H | -u | Flag]", "cgprecision precision", "cgprecision sets the number of digits preinted for estimated distances in the detailed output for maps.");
}



void cgprecision(char help[3], int precision);



//----------------------- la commande quietset

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Make the search process quiet.", "quietset [-h | -H | -u | Flag]", "quietset Flag", "quietset makes the search process quiet or not. If the flag is set to 1, nothing is printed during the search process. If the flag is set to 0, the amount of printed information depends of the Verbosity.");
}



void quietset(char help[3], int Flag);




//----------------------- la commande sem

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Compute a Single map (Single EM).", "sem [-h | -H | -u]", "sem", "sem use the order of the selection of loci to compute a single map. This command also try to improve the heap with the resulting map.");
}



void sem(char help[3]);




//----------------------- la commande nicemapd

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide quickly a (nice) map, using the 2-points distances.", "nicemapd [-h | -H | -u]", "nicemapd", "nicemapd uses the two points distance criterion to provide a map. Starting from each locus, the command builds gradually the best neighborhoods maps. At the end of the process, the command tries to insert the best map into the heap.")
}



void nicemapd(char help[3]);




//----------------------- la commande nicemapl

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide quickly a (nice) map, using the 2-points LOD criteria.", "nicemapl [-h | -H | -u]", "nicemapl", "nicemapl uses the two points LOD criterion to provide a map. Starting from each locus, the command builds gradually the best neighborhoods maps. At the end of the process, the command tries to insert the best map into the heap.")
}



void nicemapl(char help[3]);


//----------------------- la commande mfmapd

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide quickly a (nice) map, using the 2-points distances.", "mfmapd [-h | -H | -u]", "mfmapd", "mfmapd uses the two points distance criterion to provide a map. The command builds gradually a map by inserting the best (whose distances are minima) available pairs of markers. At the end of the process, the command tries to insert the map into the heap.")
}



void mfmapd(char help[3]);


//----------------------- la commande mfmapl

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide quickly a (nice) map, using the 2-points LOD criteria.", "mfmapl [-h | -H | -u]", "mfmapl", "mfmapl uses the two points LOD criterion to provide a map. The command builds gradually a map by inserting the best (whose LOD criteria are minima) available pairs of markers. At the end of the process, the command tries to insert the map into the heap.")
}



void mfmapl(char help[3]);


//----------------------- la commande build

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Build maps with two-points informations.", "build [-h | -H | -u | NbMap]", "build NbMap", "build uses the two points LOD score criterion to provide maps. The command starts from a couple of loci. At each step of the algorithme, a locus is choosen and added to the previous map. All the maps correspondind to all the possible positions of the new marker are computed. The NbMap argument enable the command to build several maps at the same time. At the end of the process, the maps improving the current solution are inserted into the heap.")
}



void build(char help[3], int NbMap);




//----------------------- la commande buildfw

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Build a framework map.", "buildfw [-h | -H | -u | keepThres AddThres MrkList MrkTest]", "buildfw  keepThres  AddThres MrkList MrkTest", "buildfw starts either by choosing three loci, or by using the list of loci MrkList. The delta of LOD score of the two best maps you can build with three given loci is used as criterion. The greater the delta is the better the solution is. Then, step by step you try each available locus at each possible position. If there is no delta greater than the AddThres Threshold, the algorithme is stopped. If there is many order which delta of LOD score against the best order is smaller than the KeepThres Threshold, they are keepped for the next step. If the flag MrkTest is set to 0 no test, to 1 each remaining locus is tested on each position of the above construct framework, to 2 each remaining locus in the selection is tested on each position of the framework passed in the third argument (MrkList).")
}



void buildfw(char help[3], double keepThres, double AddThres, int MrkList[NBM], int MrkTest);




//----------------------- la commande annealing

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Find good maps using the annealing algorithm.", "annealing [-h | -H | -u | NbTries InitTemp FinalTemp Cooling]", "annealing NbTries InitTemp FinalTemp Cooling", "annealing uses the multipoint LOD score criterion to provide maps. The command starts form the best map stored into the heap. Each time a better map is found, it is stored into the heap. Two changes and Three changes are randomly applied to the current map. The InitTemp argument correspond to the initial temperature. The FinalTemp argument correspond to the final temperature. The Cooling argument correspond to the cooling speed. The NbTries argument allows the user to increase the number of maps computed at each temperature.");
}



void annealing(char help[3], int NbTries, double InitTemp, double FinalTemp, double Cooling);




//----------------------- la commande greedy

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Find good maps using the greedy algorithm.", "greedy [-h | -H | -u | NbLoop NbIter TabooMin TabooMax Anytime]", "greedy NbLoop NbIter TabooMin TabooMax Anytime", "greedy uses the multipoint LOD score criterion to provide maps. The command starts form the best map stored into the heap. Each time a better map is found, it is stored into the heap. The TabooMin argument correspond to the minimum size of the taboo list. The TabooMax argument correspond to the maximum size of the taboo list.The NbLoop argument correspond to the number of main loop to provide. The NbIter argument allows the user to increase the number of maps computed at each loop. The Anytime ratio controls the tradeoff between search speed and solution quality.");
}



void greedy(char help[3],int NbLoop, int NbIter, int TabooMin, int TabooMax, int Anytime);




//----------------------- la commande paretogreedy

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Improve a Pareto frontier approximation.", "paretogreedy [-h | -H | -u | Anytime]", "paretogreedy Anytime", "paretogreedy uses the bi-objective multipoint LOD score criterion and breakpoint criterion to provide maps. The command starts from all the maps stored into the heap. The 2-change neighborhood is (partially, depending on the Anytime ratio, but at least 2*NbMarkers neighbors) visited for each map. Each time a non dominated map is found, it is stored into the heap. The algorithm stops when all the maps have been visited. The Anytime ratio controls the tradeoff between search speed and solution quality.");
}



void paretogreedy(char help[3],int Anytime);



//----------------------- la commande paretogreedyr

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Improve a selection of the Pareto frontier approximation.", "paretogreedyr [-h | -H | -u | Anytime MinBP MaxBP]", "paretogreedyr Anytime MinBP MaxBP", "paretogreedyr uses the bi-objective multipoint LOD score criterion and breakpoint criterion to provide maps. The command starts from a selection of the maps stored into the heap such that their breakpoint distance to the reference order belongs to the range [MinBP,MaxBP]. The 2-change neighborhood is (partially, depending on the Anytime ratio, but at least 2*NbMarkers neighbors) visited for each map. Each time a non dominated map is found, it is stored into the heap. The algorithm stops when all the maps in the breakpoint range have been visited. The Anytime ratio controls the tradeoff between search speed and solution quality.");
}



void paretogreedyr(char help[3],int Anytime,int MinBP,int MaxBP);




//----------------------- la commande algogen

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Find good maps using the genetic algorithm.", "algogen [-h | -H | -u | NbGen NbMap SelType ProbaCross ProbaMut EvolFitn]", "algogen NbGen NbMap SelType ProbaCross ProbaMut EvolFitn", "algogen uses the multipoint LOD criterion to provide maps. The command starts either form the best maps stored into the heap or from generated maps. Each time a better map is found, it is stored into the heap. The Nbgen argument correspond to the number of generation. The NbMap argument correspond to the size of the population. In case NbMap is set to 0, the command use the heap to initialize the first generation. The SelType argument correspond to the type of selection( 0 for the roulette wheel, 1 for the stochastic remainder without replacement). The ProbaCross argument correspond to the probability of crossing-over. The ProbaMut argument correspond to the probability of mutation. The EvolFitn is a flag to set the evolutive fitness(0 or 1).");
}



void algogen(char help[3], 
             int NbGen,
             int NbMap,
             int SelType,
             float ProbaCross,
             float ProbaMut,
             int EvolFitn);


//----------------------- la commande cg2tsp

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Convert current marker selection to TSP.", "cg2tsp [-h | -H | -u | FileName]" ,"cg2tsp FileName" ,"cg2tsp converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem whose file name is given as parameter.");
}

void cg2tsp(char help[3], char FileName[256]);


//----------------------- la commande paretolkh

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a Pareto frontier approximation.", "paretolkh [-h | -H | -u | Resolution NbRun CollectMaps]" ,"paretolkh Resolution NbRun CollectMaps" ,"paretolkh inserts in the heap a collection of maps corresponding to different numbers of breakpoints w.r.t. to a given reference order data set. It uses the weighted sum for combining biological data (2-points loglikelihoods) with a given reference order (breakpoint distance). The current data set should be a merged by order between a biological data set and a reference order data set. It tries different coefficient values depending on the Resolution parameter (e.g. Resolution = number of markers). paretolkh converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5). In order to get non supported Pareto solutions, use CollectMaps >= 0. Try paretolkh 10 1 0 as default parameter values.");
}

void paretolkh(char help[3], int resolution, int nbrun, int backtrack);


//----------------------- la commande paretolkhn

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a Pareto frontier approximation.", "paretolkhn [-h | -H | -u | Resolution NbRun CollectMaps]" ,"paretolkhn Resolution NbRun CollectMaps" ,"paretolkhn inserts in the heap a collection of maps corresponding to different numbers of breakpoints w.r.t. to a given reference order data set. It uses the weighted sum for combining biological data (normalized 2-points loglikelihoods) with a given reference order (breakpoint distance). The current data set should be a merged by order between a biological data set and a reference order data set. It tries different coefficient values depending on the Resolution parameter (e.g. Resolution = number of markers). paretolkhn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5). In order to get non supported Pareto solutions, use CollectMaps >= 0. Try paretolkhn 10 1 0 as default parameter values.");
}

void paretolkhn(char help[3], int resolution, int nbrun, int backtrack);


//----------------------- la commande paretoinfo

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Display the Pareto frontier.", "paretoinfo [-h | -H | -u | GraphicalView Lambda]" ,"paretoinfo GraphicalView Lambda" ,"paretoinfo prints information about the Pareto frontier that can be extracted from the CarthaGene's heap. It also returns an ordered list of map identifiers belonging to the Pareto frontier in increasing number of breakpoints. If GraphicalView > 0 then a file called 'tmppareto' is created and displayed using gnuplot. If GraphicalView < 0 then each map order is printed. See also paretolkh and paretogreedy to create a Pareto frontier. The best map in the Pareto frontier is identified by the keywork 'balanced'. Lambda corresponds to the expected number of breakpoints between the true order and the reference order. Try Lambda = 1 as default value.");
}

%typemap(in,numinputs=0) int **lmapidout(int *temp)
{
  $1 = &temp;
}

%typemap(argout) int **lmapidout
{
  int idm = 0;
  while ((*$1)[idm] != -1)
    Tcl_ListObjAppendElement(interp,
                             $result,
                             Tcl_NewIntObj((*$1)[idm++]));
                             
  delete [] *$1;
  if (Fout != NULL)
    {
      int temphelp;
      fprintf(Fout, " %s\n", Tcl_GetStringFromObj($result, &temphelp));
    }
}


void paretoinfo(char help[3], int GraphicalView, double Lambda, int **lmapidout);

//----------------------- la commande paretoinfog

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Internal command.", "paretoinfog [-h | -H | -u | Lambda]" ,"paretoinfog Lambda" ,"paretoinfog is a internal command that returns a list of pareto frontier data (see also paretoinfo).");
}

%typemap(in,numinputs=0) char ****lpareto(char ***temp)
{
  $1 = &temp;
}

%typemap(argout) char ****lpareto
{
  int idl = 0;
  int idval = 0;
  Tcl_Obj * paretolist;
  
  if (*$1 != NULL)
    {  
      while ((*$1)[idl] != NULL)
	{
	  paretolist = Tcl_NewListObj(0,NULL);
	  
	  for (idval = 0; idval < 5; idval++) 
	    { 
	      Tcl_ListObjAppendElement(interp,
				       paretolist,
				       Tcl_NewStringObj((*$1)[idl][idval],
							strlen((*$1)[idl][idval])));
	      delete [] (*$1)[idl][idval];
	    }
	  
	  Tcl_ListObjAppendElement(interp,
				   $result,
				   paretolist);
	  delete [] (*$1)[idl++];
	}                           
      delete [] *$1;      
    } 
  else
    Tcl_ListObjAppendElement(interp,
			     $result,
			     Tcl_NewStringObj("",
					      1));
  if (Fout != NULL)
    {
      int temphelp;
      fprintf(Fout, " %s\n", Tcl_GetStringFromObj($result, &temphelp));
    }
}


void paretoinfog(char help[3], double Lambda, char ****lpareto);





// DL
//----------------------- la commande dsave

#define dsave_SHORT "Dump a subset of the given dataset to a file."
#define dsave_USAGE "dsave [-h | -H | -u] id filename"
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS(dsave_SHORT, dsave_USAGE, dsave_USAGE, dsave_SHORT " The subset is defined by the current marker selection. Additional information is output at the end of the file (whether imputation was performed, which markers weren't saved, which markers were merged).")
}
#undef dsave_SHORT
#undef dsave_USAGE

char* _dsave(char help[3], int id, char fileName[256]);


// DL
//----------------------- la commande cg_setparallel

#define cg_setparallel_SHORT "Specify the number of concurrent workers for parallelized commands."
#define cg_setparallel_USAGE "cg_setparallel [-h | -H | -u] num"
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS(cg_setparallel_SHORT, cg_setparallel_USAGE, cg_setparallel_USAGE, cg_setparallel_SHORT " The actual number of workers is the specified number minus one (there has to be a controller thread).")
}
#undef cg_setparallel_SHORT
#undef cg_setparallel_USAGE

void cg_setparallel(char help[3], int num);


// DL
//----------------------- la commande cg_setparallel

#define cg_getparallel_SHORT "Get the number of concurrent workers for parallelized commands."
#define cg_getparallel_USAGE "cg_getparallel [-h | -H | -u] num"
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS(cg_getparallel_SHORT, cg_getparallel_USAGE, cg_getparallel_USAGE, cg_getparallel_SHORT " The actual number of workers is the returned number minus one (there has to be a controller thread).")
}
#undef cg_getparallel_SHORT
#undef cg_getparallel_USAGE

int cg_getparallel(char help[3]);


// DL
//----------------------- la commande cg_set2ptcachepath

#define cg_set2ptcachepath_SHORT "Set the path where the .2pt cache files are stored. Empty means .2pt files are created wherever the dataset files reside."
#define cg_set2ptcachepath_USAGE "cg_set2ptcachepath [-h | -H | -u] path"
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS(cg_set2ptcachepath_SHORT, cg_set2ptcachepath_USAGE, cg_set2ptcachepath_USAGE, cg_set2ptcachepath_SHORT)
}
#undef cg_set2ptcachepath_SHORT
#undef cg_set2ptcachepath_USAGE

void cg_set2ptcachepath(char help[3], char* path);


// DL
//----------------------- la commande cg_get2ptcachepath

#define cg_get2ptcachepath_SHORT "Get the path where the .2pt cache files are stored."
#define cg_get2ptcachepath_USAGE "cg_get2ptcachepath [-h | -H | -u]"
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS(cg_get2ptcachepath_SHORT, cg_get2ptcachepath_USAGE, cg_get2ptcachepath_USAGE, cg_get2ptcachepath_SHORT)
}
#undef cg_get2ptcachepath_SHORT
#undef cg_get2ptcachepath_USAGE

char* cg_get2ptcachepath(char help[3]);


// DL
//----------------------- la commande dsclone

#define dsclone_SHORT "Clone a dataset."
#define dsclone_USAGE "dsclone [-h | -H | -u] id"
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS(dsclone_SHORT, dsclone_USAGE, dsclone_USAGE, dsclone_SHORT)
}
#undef dsclone_SHORT
#undef dsclone_USAGE

int dsclone(char help[3], int id);



// DL
//----------------------- la commande dsrhconv

#define dsrhconv_SHORT "Convert a radiated hybrid dataset into haploid|diploid|error."
#define dsrhconv_USAGE "dsrhconv [-h | -H | -u] id new_type"
%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS(dsrhconv_SHORT, dsrhconv_USAGE, dsrhconv_USAGE, dsrhconv_SHORT)
}
#undef dsrhconv_SHORT
#undef dsrhconv_USAGE

int dsrhconv(char help[3], int id, char*newType);



//----------------------- la commande mcmc

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a map distribution using Markov Chain Monte Carlo.", "mcmc [-h | -H | -u | RandomSeed NbIter Burning [-fast] [-lgsz <value>] [-verbose]]" ,"mcmc RandomSeed NbIter Burning [-fast] [-lgsz <value>] [-verbose]" ,"mcmc is Markov Chain Monte Carlo algorithm for estimating the posterior distribution of the marker order. It starts from the best map found in the CarthaGene heap. In order to visit maps, it uses a stochastic-biased 2-opt operator based on 2-point likelihood genetic / Radiated Hybrid data. The random generator number is initialized with the RandomSeed parameter (should be a positive integer). The number of iterations of MCMC is controlled by the NbIter parameter. At the end, it gives a list of maps with their posterior probability. The first Burning iterations are not used to estimate the map distribution. The maps are inserted in the CarthaGene heap. The map distribution is written to a file names PID.mcmc where PID is the Process ID of the current carthagene session.\n\nThe format of the output file is as follows: the first line recalls the starting map then each line is a map from the distribution, sorted in decreasing (multipoint) posterior probability. On each line is indicated (in that order): the posterior probability using the multipoint likelihood, the posterior probability using the 2-pt approximation of the likelihood, the weight measuring the ratio of these two probabilities, the number of breakpoints with the reference order and finally the map given as the positions of the markers relative to the starting map (from 0 to N-1) where N is the number of markers.");
}

void _mcmc(char help[3], int seed, int nbiter, int burning, int slow_computations, int lim_group_size, int mcmcverbose);


//----------------------- la commande lkh

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using 2-points loglikelihoods and Lin-Kernighan heuristic.", "lkh [-h | -H | -u | NbRun CollectMaps]" ,"lkh NbRun CollectMaps" ,"lkh converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).");
}

void lkh(char help[3], int nbrun, int backtrack);


//----------------------- la commande ilkh

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using dynamically updated 2-points loglikelihoods and Lin-Kernighan heuristic.", "ilkh [-h | -H | -u | NbRun CollectMaps Threshold Penalty]" ,"ilkh NbRun CollectMaps" ,"ilkh converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkh 20 0 0.01 100.");
}

void ilkh(char help[3], int nbrun, int collectmaps, double threshold, int cost);


//----------------------- la commande lkhn

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using normalized 2-points loglikelihoods and Lin-Kernighan heuristic.", "lkhn [-h | -H | -u | NbRun CollectMaps]" ,"lkhn NbRun CollectMaps" ,"lkhn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).");
}

void lkhn(char help[3], int nbrun, int backtrack);

//----------------------- la commande ilkhn

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using dynamically updated normalized 2-points loglikelihoods and Lin-Kernighan heuristic.", "ilkhn [-h | -H | -u | NbRun CollectMaps Threshold Penalty]" ,"ilkhn NbRun CollectMaps" ,"ilkhn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhn 20 0 0.01 100.");
}

void ilkhn(char help[3], int nbrun, int collectmaps, double threshold, int cost);


//----------------------- la commande lkhd

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using 2-points distances and Lin-Kernighan heuristic.", "lkhd [-h | -H | -u | NbRun CollectMaps]" ,"lkhd NbRun CollectMaps" ,"lkhd converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).");
}

void lkhd(char help[3], int nbrun, int backtrack);

//----------------------- la commande ilkhd

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using dynamically updated 2-points distances and Lin-Kernighan heuristic.", "ilkhd [-h | -H | -u | NbRun CollectMaps Threshold Penalty]" ,"ilkhd NbRun CollectMaps" ,"ilkhd converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhd 20 0 0.01 100.");
}

void ilkhd(char help[3], int nbrun, int collectmaps, double threshold, int cost);


//----------------------- la commande lkhl

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using 2-points LOD criteria and Lin-Kernighan heuristic.", "lkhl [-h | -H | -u | NbRun CollectMaps]" ,"lkhl NbRun CollectMaps" ,"lkhl converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).");
}

void lkhl(char help[3], int nbrun, int backtrack);

//----------------------- la commande ilkhl

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using dynamically updated 2-points LOD criteria and Lin-Kernighan heuristic.", "ilkhl [-h | -H | -u | NbRun CollectMaps Threshold Penalty]" ,"ilkhl NbRun CollectMaps" ,"ilkhl converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhl 20 0 0.01 100.");
}

void ilkhl(char help[3], int nbrun, int collectmaps, double threshold, int cost);


//----------------------- la commande lkhocb

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.", "lkhocb [-h | -H | -u | NbRun CollectMaps]" ,"lkhocb NbRun CollectMaps" ,"lkhocb converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).");
}

void lkhocb(char help[3], int nbrun, int backtrack);

//----------------------- la commande ilkhocb

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using dynamically updated 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.", "ilkhocb [-h | -H | -u | NbRun CollectMaps Threshold Penalty]" ,"ilkhocb NbRun CollectMaps" ,"ilkhocb converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhocb 20 0 0.01 100.");
}

void ilkhocb(char help[3], int nbrun, int collectmaps, double threshold, int cost);


//----------------------- la commande lkhocbn

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using normalized 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.", "lkhocbn [-h | -H | -u | NbRun CollectMaps]" ,"lkhocbn NbRun CollectMaps" ,"lkhocbn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic. If CollectMaps >= 0 then every tour found by LKH is inserted into the CarthaGene heap. Moreover, if positive, CollectMaps controls the backtrack move type of LKH (possible values are 0, 2, 3, 4 and 5).");
}

void lkhocbn(char help[3], int nbrun, int backtrack);

//----------------------- la commande ilkhocbn

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Provide a (nice) map, using dynamically updated normalized 2-points Obligate Chromosome Breaks criteria and Lin-Kernighan heuristic.", "ilkhocbn [-h | -H | -u | NbRun CollectMaps Threshold Penalty]" ,"ilkhocbn NbRun CollectMaps" ,"ilkhocbn converts the current marker selection and associated genetic / Radiated Hybrid data into a Traveling Salesman Problem which is solved using Keld Helsgaun's LKH software. Repeats NbRun times the heuristic, modifying the TSP objective function accordingly to the multipoint probabilistic parameters. If CollectMaps = 1 then every tour found by LKH is inserted into the CarthaGene heap, otherwise set CollectMaps to zero. Threshold specifies that Penalty should be added to a TSP edge cost if the multipoint Theta minus the 2-points Theta is greater than Threshold. Try e.g. ilkhocbn 20 0 0.01 100.");
}

void ilkhocbn(char help[3], int nbrun, int collectmaps, double threshold, int cost);


//----------------------- la commande mapocb

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Compute the number of obligate chromosome breaks of a map.", "mapocb [-h | -H | -u | MapID]", "mapocb", "mapocb returns the value of the 2-points Obligate Chromosome Breaks criterion. The MapID should be the identifier of a map stored into the heap.");
}



int mapocb(char help[3], int MapID);


//----------------------- la commande polish

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Try to improve the best map by polishing.", "polish [-h | -H | -u]", "polish", "polish starts form the best map of the heap, an try to improve the solution. Taking each locus, one after the other, this commands tests all the possible position of this locus on the map, keeping the order of the other loci. Each time the solution is improved, the command tries to store the map into the heap.");
}



void polish(char help[3]);




//----------------------- la commande polishtest

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Try to improve the map giving with markers by polishing.", "polish [-h | -H | -u | MrkList]", "polish", "polishtest starts from the map giving on the command, an try to improve the solution. Taking each locus, one after the other, this commands tests all the possible position of this locus on the map, keeping the order of the other loci.");
}

%typemap(in) int [ANY] (int temp[$dim0]) 
{
  int objcount ,i, objet;
  Tcl_Obj **objvect;

  if (Tcl_ListObjGetElements(interp,
                             $input,
                             &objcount,
                             &objvect) == TCL_ERROR)
    return TCL_ERROR;
  
  if (objcount >= $dim0)  
    { 
      interp->result = (char *)"Array size mismatch!"; 
      return TCL_ERROR; 
    } 
  
  for (i = 0; i < objcount; i++) 
   { 
     if (Tcl_GetIntFromObj(interp,  
                           objvect[i],   
                           &objet) == TCL_ERROR)  
        return TCL_ERROR; 
     temp[i] = objet; 
   }

  for (i = objcount; i < $dim0; i++)
    temp[i] = 0;
  
  $1 = temp;
}



void polishtest(char help[3], int MrkList[NBM]);




//----------------------- la commande flips

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Try to improve the best map by flipping.", "flips [-h | -H | -u | WinSize Thres Iter]", "flips WinSize Thres Iter", "flips starts from the best map of the heap and try to improve the solution. Inside a window pushed on all the positions of the map, all the permutation are tested. Each time a better solution is found, the process prints it, and tries to insert it into the heap. The Winsize argument allows you to specify the window size. The Thres argument is a LOD threshold, that enables to see worse maps, that differs less than this value from the best map. If the iter flag is set to 1, each time a better solution is found, the command will start again from the best map.");
}



void flips(char help[3], int WinSize, double Thres, int Iter);


//----------------------- la commande heapsize

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Resize the heap.", "heapsize [-h | -H | -u | NbMap]", "heapsize NbMap", "heapsize enable to resize the map container. If the heap of maps is filled, the best maps are preserved.");
}



void heapsize(char help[3], int NbMap);


//----------------------- la commande paretoheapsize

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Set the heap size used by pareto functions.", "paretoheapsize [-h | -H | -u | NbMap]", "paretoheapsize NbMap", "paretoheapsize sets the size of the map container used by paretolkh and paretogreedy functions. paretogreedy will resize the current heap using this value. paretolkh uses an internal heap the size of which will be set to this value (at the end of paretolkh function, the current heap size is set to the current number of markers). The default value is 1024.");
}



void paretoheapsize(char help[3], int NbMap);

//----------------------- la commande heapequiset

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("enable or disable the equivalence mode of the heap.", "heapequiset [-h | -H | -u| Flag]", "heapequiset Flag", "heapequiset enable to switch the equivalence mode of the heap. If the flag is set to 1 the equivalence mode is activated. Dealing with data sets merged by order has the side effect that many order encountered during a build or search process are equivalent. This mode propose to store only one representant for each class.");
}


void heapequiset(char help[3], int flag);



//----------------------- la commande heapsizeget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("get the size of the heap.", "heapsizeget [-h | -H | -u]", "heapsizeget", "heapsizeget enable to get the size of the the map container.");
}


int heapsizeget(char help[3]);



//----------------------- la commande heaprinto

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Display the heap of maps by privileging the order of the markers.", "heaprinto [-h | -H | -u |NbMrk Blank Comp]", "heaprinto NbMrk Blank Comp", "heaprinto displays for each map the positions of markers on the best map. The NbMrk allows you to materialize consecutive differences, if set to 0 all the positions are printed. If the Blank argument is set to 1, the positions inside a interval are replaced by -. Set to 1, the Comp argument makes the lines shorter by removing blanks.");
}



void heaprinto(char help[3], int NbMrk, int Blank, int Comp);




//----------------------- la commande heaprint

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Display the heap sorted.", "heaprint [-h | -H | -u]", "heaprint", "heaprint prints each map stored into the heap by order of LOD score. The information provided is limited to the order and the score.");
}



void heaprint(char help[3]);




//----------------------- la commande heaprintd

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Display the heap in detail, sorted.", "heaprintd [-h | -H | -u]", "heaprintd", "heaprintd  prints in detail each map stored into the heap by order of LOD score. All the information available is provided.");
}



void heaprintd(char help[3]);




//----------------------- la commande heapget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get the heap, in a list.", "heapget [-h | -H | -u | UnitFlag NbMap]", "heapget UnitFlag NbMap", "heapget returns information about the NbMap best maps into a list.");
}

%typemap(in,numinputs=0) char *****llmapout(char ****temp)
{
  $1 = &temp;
}

%typemap(argout) char *****llmapout
{
  int idlmap = 0;
  int idmap = 0;
  int idval = 0;
  Tcl_Obj * maplist;
  Tcl_Obj * lmaplist;
  
  if (*$1 != NULL)
    {      
      idlmap = 0;
      while ((*$1)[idlmap] != NULL)
	{
	  lmaplist = Tcl_NewListObj(0,NULL);
	  idmap = 0;
	  Tcl_ListObjAppendElement(interp,
				   lmaplist,
				   Tcl_NewStringObj((*$1)[idlmap][idmap][0],
						    strlen((*$1)[idlmap][idmap][0])));
	  Tcl_ListObjAppendElement(interp,
				   lmaplist,
				   Tcl_NewStringObj((*$1)[idlmap][idmap][1],
						    strlen((*$1)[idlmap][idmap][1])));
	  idmap++;
	  while ((*$1)[idlmap][idmap] != NULL)
	    {    
	      maplist = Tcl_NewListObj(0,NULL);
	      idval = 0;
	      while ((*$1)[idlmap][idmap][idval] != NULL)
		{
		  Tcl_ListObjAppendElement(interp,
					   maplist,
					   Tcl_NewStringObj((*$1)[idlmap][idmap][idval],
							    strlen((*$1)[idlmap][idmap][idval])));
		  delete [] (*$1)[idlmap][idmap][idval++];
		}
	      Tcl_ListObjAppendElement(interp,
				       lmaplist,
				       maplist);
	      delete [] (*$1)[idlmap][idmap++];
	    }
	  Tcl_ListObjAppendElement(interp,
				   $result,
				   lmaplist);
	  delete [] (*$1)[idlmap++];
	}
      delete [] (*$1);
    }
  else
    Tcl_ListObjAppendElement(interp,
			     $result,
			     Tcl_NewStringObj("",
					      1));
  if (Fout != NULL)
    {
      int temphelp;
      fprintf(Fout, " %s\n", Tcl_GetStringFromObj($result, &temphelp));
    }
}
  


void heapget(char help[3], char UnitFlag[2], int NbMap, char *****llmapout);


//----------------------- la commande maprint

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Print a map.", "maprint [-h | -H | -u | MapID]", "maprint", "maprint prints each locus at his position on the maps, and the log10-likelihood. The MapID should be the identifier of a map stored into the heap.");
}



void maprint(char help[3], int MapID);




//----------------------- la commande maprintd

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Print a map, in detail.", "maprintd [-h | -H | -u | MapID]", "maprintd", "maprintd prints all the information about the map. The MapID should be the identifier of a map stored into the heap.");
}



void maprintd(char help[3], int MapID);




//----------------------- la commande maprintdr

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Print a map reverse, in detail.", "maprintdr [-h | -H | -u | MapID]", "maprintdr", "maprintdr prints all the information about the map. The MapID should be the identifier of a map stored into the heap.");
}



void maprintdr(char help[3], int MapID);




//----------------------- la commande mapget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get a map, in a list.", "mapget [-h | -H | -u | UnitFlag MapID]", "mapget UnitFlag MapID", "mapget returns information about the map. The MapID should be the identifier of a map stored into the heap. The returned list contains the MapID, a global LOD score, as well as as many sublists than of partial orders. Each sublist contains the SetId, the partial LOD Score as well as the name of each loci and their respective relative position. The UnitFlag argument allows to choose either the Kosambi(k) unit or the Haldane(h) unit. For radiated hybrids, this flag is not active.");
}

%typemap(in,numinputs=0) char ****lmapout(char ***temp)
{
  $1 = &temp;
}

%typemap(argout) char ****lmapout
{
  int idmap = 0;
  int idval = 0;
  Tcl_Obj * maplist;
  
  if (*$1 != NULL)
    {      
      Tcl_ListObjAppendElement(interp,
			       $result,
			       Tcl_NewStringObj((*$1)[idmap][idval],
						strlen((*$1)[idmap][idval])));
      Tcl_ListObjAppendElement(interp,
			       $result,
			       Tcl_NewStringObj((*$1)[idmap][1],
						strlen((*$1)[idmap][1])));
      idmap++;
      while ((*$1)[idmap] != NULL)
	{
	  maplist = Tcl_NewListObj(0,NULL);
	  idval = 0;
	  while ((*$1)[idmap][idval] != NULL)
	    {
	      Tcl_ListObjAppendElement(interp,
				       maplist,
				       Tcl_NewStringObj((*$1)[idmap][idval],
							strlen((*$1)[idmap][idval])));
	      delete [] (*$1)[idmap][idval++];
	    }
	  Tcl_ListObjAppendElement(interp,
				   $result,
				   maplist);
	  delete [] (*$1)[idmap++];
	}                           
      delete [] *$1;      
    } 
  else
    Tcl_ListObjAppendElement(interp,
			     $result,
			     Tcl_NewStringObj("",
					      1));
  if (Fout != NULL)
    {
      int temphelp;
      fprintf(Fout, " %s\n", Tcl_GetStringFromObj($result, &temphelp));
    }
}
  


void mapget(char help[3], char UnitFlag[2], int MapID, char ****lmapout);



//----------------------- la commande mapordget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get a order of loci from a map, in a list.", "mapordget [-h | -H | -u | MapID]", "mapordget MapID", "mapordget returns the loci order of the map. The MapID should be the identifier of a map stored into the heap. The returned list contains loci by Id.");
}



void mapordget(char help[3], int MapID, int **lmiout);



//----------------------- la commande group

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Identify linkage groups.", "group [-h | -H | -u | DistThres LODThres]", "group DistThres LODThres", "group tries to identify the linkage groups of a Data Set by a two-points analysis. The command is applied to the selection of loci. The DistThres argument enables to set the distance threshold(a maximum). The LODThres argument enables to set LOD threshold(a minimum). The command prints each groups, and returns the number of identified groups. The groups are stored.")
}



int group(char help[3], double DistThres, double LODThres);




//----------------------- la commande groupget

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get a group by markers ID.", "groupget [-h | -H | -u | GrID]", "groupget GrID", "groupget gets a group previously detected by the command group. The GrID argument corrrespond to the group identifier. The group is returned as a list of loci index.")
}

void groupget(char help[3], int GrID, int **lmiout);


//----------------------- la commande dsbp

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Compute the number of breakpoints between one order data set and one biological data set.", "dsbp [-h | -H | -u | SetID1 SetID2]", "dsbp SetID1 SetID2", "dsbp computes the number of breakpoints between the order given by data set SetID1 and the initial order given by the biological data set SetId2. A breakpoint occurs when two consecutive markers in one set are not consecutive in the other set. Only common markers are taken into account.");
}

int dsbp(char help[3],int SetID1, int SetID2);


//----------------------- la commande dsbpcoef

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Modifies the coefficient factor value applied to the number of breakpoints for a given order data set.", "dsbpcoef [-h | -H | -u | SetID NewCoefValue]", "dsbpcoef SetID NewCoefValue", "dsbpcoef modifies the coefficient factor value applied to the number of breakpoints. This coefficient was first initialized in the order data set. A negative coefficient value has a special meaning. When a map is evaluated by Carthagene, it will use an exact comparative mapping log10likelihood criterion instead of an approximated penalized log10likelihood criterion. Note that Carthagene always uses the absolute value of the coefficient factor when converting into a Traveling Salesman Problem (see lkh commands). The function returns the previous coefficient factor value.");
}

double dsbpcoef(char help[3],int SetID, double NewCoefValue);


//----------------------- la commande dsbplambda

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Replaces the traditional criterion by the comparative mapping criterion.", "dsbplambda [-h | -H | -u | SetID CoefAutoFit NewLambdaValue]", "dsbplambda SetID CoefAutoFit NewLambdaValue", "dsbplambda sets the lambda parameter value used in the definition of the comparative mapping criterion. The parameter SetId corresponds to the reference order dataset numerical id. Set the CoefAutoFit parameter to 1 in order to replace the traditional criterion by the comparative mapping criterion. Then, all the Carthagene map printing, building or improving commands will use this new criterion. By default, set the NewLambdaValue parameter to 1. The function returns the previous lambda parameter value.");
}

double dsbplambda(char help[3],int SetID, int CoefAutoFit, double NewLambdaValue);

//----------------------- la commande mapbp

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Compute the number of breakpoints between one order data set and a map.", "mapbp [-h | -H | -u | SetID MapId]", "mapbp SetID MapId", "mapbp computes the number of breakpoints between the order given by data set SetID and a map. The MapID should be the identifier of a map stored into the heap. A breakpoint occurs when two consecutive markers in one set are not consecutive in the map. Only common markers are taken into account.");
}

int mapbp(char help[3],int SetID, int MapId);


//----------------------- la commande errorestimation

%typemap(in,numinputs=0) char help[3]()
{
 HELPMESS("Based on the current marker order, estimate the error rate (only available for haploid radiated hybrid data type).", "errorestimation [-h | -H | -u |  Id ]", "errorestimation //", "Estimation of the error rates for haploid radiated hybrids.");
}

void errorestimation(char help[3],int MapID);


//----------------------- la commande imputation

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Impute corrected genotypes according to the error model (only available for radiated hybrid data type.", "imputation [-h | -H | -u | Id ConversionCutoff CorrectionCutoff UnknownCorrectionCutoff FileName ]", "imputation FileName", "imputation of the true genotypes are cloned to a new dataset.");
}

void imputation(char help[3], int Id, double ConversionCutoff,double CorrectionCutoff, double UnknownCorrectionCutoff, char** textout );


//----------------------- la commande cgout

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Save the output to a file.", "dsload [-h | -H | -u | FileName]" ,"dsload FileName" ,"cgout enables to save the session output in a text file. If the file already exist, the ouput is appened. To stop the process, you need to call cgout one more time with a emtpy string.");
}

void cgout(char help[3], char FileName[256]);


//----------------------- la commande tolerance

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Sets EM convergence thresholds.", "cgtolerance [-h | -H | -u | Threshold1 Threshold2]", "cgtolerance Threshold1 Threshold2", "cgtolerance sets the thresholds used inside EM, the recombination/breakage/retention estimation algorithm. The first threshold is used for fine convergence for final maps, the second one is used for raw convergence in ordering commands to decide if further iterations are worth the effort.")
}


void cgtolerance(char help[3], double Threshold1, double Threshold2);


//----------------------- la commande cg2pt

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Compute approximated likelihoods based on 2-point estimation data.", "cg2pt [-h | -H | -u | Mode]", "cg2pt Mode", "cg2pt allows fast likelihood computations based on 2-point estimation data when Mode is set to a value different from zero. Otherwise, exact likelihood computations is performed by the EM algorithm.")
}



void cg2pt(char help[3], int mode);


//----------------------- la commande robustesse

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Sets reliability threshold for framework maps.", "cgrobustness [-h | -H | -u | Threshold]", "cgrobustness Threshold", "cgrobustness sets the threshold used to verify if the current best map is robust or not. A map M is not robust if there exists another map M' such that L(M') > L(M) + Threshold, where L(M) is the multipoint log likelihood of M. A common value for Threshold is -3.0. Each time a new map is inserted into the heap, the system verifies if the current best map is still robust. If not then it will stop any running search procedure.")
}



void cgrobustness(char help[3], double Threshold);


//----------------------- la commande inhiber robustesse

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Disable reliability test of framework maps.", "cgnotrobust [-h | -H | -u]", "cgnotrobust", "cgnotrobust disables the reliability test of framework maps. It is equivalent to call cgrobustness with Threshold equal to 1e100.")
}


void cgnotrobust(char help[3]);

//----------------------- la commande cgversion

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get the .", "cgversion [-h | -H | -u]", "cgversion", "cgversion gives the version of the software(CartaGene part).");
}

void cgversion(char help[3]);


//----------------------- la commande cgstat

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Get some performance information.", "cgstat [-h | -H | -u | LODThres]", "cgstat LODThres", "cgstat returns informations usefull to provide performance statistics. The LODThres argument is used to count the maps close to the best one inside this threshold.");
}


void cgstat(char help[3], double LODThres, char **textout);


//----------------------- la commande cgrestart

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Reset the application.", "cgrestart [-h | -H | -u]", "cgrestart", "cgrestart erases all computed information and loaded  information, of the system.");
}



void cgrestart(char help[3]);




//----------------------- la commande cglicense

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("Deprecated command.", "cglicense[-h | -H | -u | Key]", "cglicense Key", "cglicense is a deprecated command.");
}



void cglicense(char help[3], char* Key);




//----------------------- la commande cgstop

%typemap(in,numinputs=0) char help[3]()
{
  HELPMESS("To stop a running command, type ctrl-c.", "cgstop[-h | -H | -u ]", "cgstop", "The ctrl-c sequence will enable the user to emit a request of interruption of the current command. The request will be handled next time a map is computed.");
}

 
void cgstop(char help[3]);


/*//----------------------- la commande cg_get_matrix_mode*/

/*%typemap(in,numinputs=0) char help[3]()*/
/*{*/
  /*HELPMESS("Get the current default backend.", "cg_get_matrix_mode [-h | -H | -u ]", "cg_get_default_backend", "Get the name of the current default backend.");*/
/*}*/

 
/*void cg_get_matrix_mode(char help[3], char**textout);*/



/*//----------------------- la commande cg_set_matrix_mode*/

/*%typemap(in,numinputs=0) char help[3]()*/
/*{*/
  /*HELPMESS("Set the current default backend (one of 'nocache', 'buffer', 'hashcache', 'swap').", "cg_set_matrix_mode [-h | -H | -u ]", "cg_set_default_backend", "Set the name of the current default backend (one of 'nocache', 'buffer', 'hashcache', 'swap').");*/
/*}*/

 
/*void cg_set_matrix_mode(char help[3], char*backend);*/


