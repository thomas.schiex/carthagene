lappend auto_path [file dirname [file dirname [info script]]]
package require CGsh
if { [llength $argv] == 1 } {
	source [lindex $argv 0]
} else {
	package require tclreadline

	proc ::tclreadline::prompt1 {} {
		return "CG> "
	}

	dsload -h

	foreach cmd [info commands ::CG::*] {
		::tclreadline::readline add [$cmd -u]
	} 

	::tclreadline::Loop
}

