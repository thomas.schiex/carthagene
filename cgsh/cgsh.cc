//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
//  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
//
// $Id: cgsh.cc,v 1.74.2.9 2012-09-05 12:07:59 dleroux Exp $
//
//    This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Module 	: cgsh
// Projet	: CartaGene
//
// Description : Impl�mentation de l'interface entre CartaGene
// et le langage de script.
//
// Remarque : l'impl�mentation doit �tre triviale.
// C''est au niveau de la classe CartaGene que l'effort doit �tre fait.
//
// A commenter librement...l'effort �tant � faire dans cgsh.h et dans cgsh.i
//
//-----------------------------------------------------------------------------
//-INRA--------------------Station de Biom�trie et d''Intelligence Artificielle
//-----------------------------------------------------------------------------

#include "cgsh.h"
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <exception>
#include <ctime>

#include "tcl.h"

/*#include "../twopoint/twopoint.h"*/

/*#include "../calcul/BioJeuSingle.h"*/

/*#include "carthagene_all.h"*/


#if defined(__WIN32__)&&!defined(abs)
#define abs(_x_) ( (_x_) < 0 ? -(_x_) : (_x_) )
#endif


CartaGene Cartage;

char  bouf[2048];
char boufi[2048];
FILE *Fout;

class BioJeu;

extern Tcl_Interp *linterp;

void hand(int sig)
{
  // enregistrement d'une demande d'arr�t.
#ifndef __WIN32__
  if (sig == SIGINT || sig == SIGXCPU) Cartage.StopFlag = 1;
  Utils::Interrupt::flag(true);
  signal(SIGINT, hand);
  signal(SIGXCPU, hand);
#else
  if (sig == SIGINT) Cartage.StopFlag = 1;
  Utils::Interrupt::flag(true);
  signal(SIGINT, hand);
#endif
}


void dsload(char help[3], char *fileName, char **textout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  BioJeu* bjp = Cartage.CharJeu(fileName);
  if(!bjp) {
      *textout = strdup("");
  } else {
      switch(bjp->Cross) {
          case Con:
          case Ordre:
              asprintf(textout, "%d order %d %s", bjp->Id, bjp->NbMarqueur, fileName);
              break;
          case RH:
              asprintf(textout, "%d haploid RH %d %d %s",
                       bjp->Id, bjp->NbMarqueur, bjp->TailleEchant,
                       fileName);
              break;
          case RHD:
              asprintf(textout, "%d diploid RH %d %d %s",
                       bjp->Id, bjp->NbMarqueur, bjp->TailleEchant,
                       fileName);
              break;
          case RHE:
              asprintf(textout, "%d haploid RH with Errors %d %d %s",
                       bjp->Id, bjp->NbMarqueur, bjp->TailleEchant,
                       fileName);
              break;
          default:
              asprintf(textout, "%d %s %d %d %s", bjp->Id, bjp->GetDataType(),
                                                  bjp->NbMarqueur, bjp->TailleEchant,
                                                  fileName);
              break;
      };
  }
  flush_out();
}



char* _dsave(char help[3], int id, char *suffix)
{
  FILE*f;
  static char path[512];
  BioJeuSingle* bjs;
  char* orig_filename;
  try {
    bjs = dynamic_cast<BioJeuSingle*>(Cartage.Jeu[id]);
    orig_filename = bjs->NomJeu;
  } catch(std::exception& e) {
          /* TODO : initialize a new filename because this dataset had none in the first place. */
          orig_filename = (char*)"FIXME";
  }
  if(strlen(suffix)) {
    sprintf(path, "%s.%s", orig_filename, suffix);
  } else {
    time_t tmt;
    char datebuf[16];
    time(&tmt);
    strftime(datebuf, 15, "%F", localtime(&tmt));
    sprintf(path, "%s.%s", orig_filename, datebuf);
  }
  if(!(f = fopen(path, "w"))) {
    perror("opening file for writing");
  }
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Jeu[id]->DumpTo(f);
  fclose(f);
  flush_out();
  return path;
}

void cg_setparallel(char help[3], int num) {
#ifdef WITH_PARALLEL
	Parallel::WorkerPoolManager::setcount(num);
#else
	std::cerr << "CarthaGene was compiled without parallelization support. This command has no effect." << std::endl;
#endif
}

int cg_getparallel(char help[3]) {
#ifdef WITH_PARALLEL
	return Parallel::WorkerPoolManager::getcount();
#else
	std::cerr << "CarthaGene was compiled without parallelization support." << std::endl;
	return 1;
#endif
}

void cg_set2ptcachepath(char help[3], char* path) {
    matrix::storage::impl::file::cache_path() = path;
}

char* cg_get2ptcachepath(char help[3]) {
    return (char*) matrix::storage::impl::file::cache_path().c_str();
}

void whine(const char* msg=NULL)
{
	print_out("I'm afraid I can't do that !\n");
	if (msg != NULL) {
		print_out(msg);	
	}
}

int dsclone(char help[3], int id)
{
	BioJeu* b=NULL;
	try {
		b = Cartage.ClonaJeu(id);
	} catch(BioJeu::NotImplemented& e) {
		whine();
	}
	flush_out();
	return b?b->Id:-1;
}

int dsrhconv(char help[3], int id, char*newType)
{
        BioJeu* b=NULL;
	try {
		switch(Cartage.Jeu[id]->Cross) {
			case RH:
			case RHD:
				break;
			case RHE:
				return -1;
			default:
				whine("Please use imputation instead.\n");
				return -1;
		};
		if(!strcmp(newType, "diploid")) {
			b=Cartage.ClonaJeu(id, RHD);
		} else if(!strcmp(newType, "haploid")) {
			b=Cartage.ClonaJeu(id, RH);
		} else if(!strcmp(newType, "error")) {
			b=Cartage.ClonaJeu(id, RHE);
		} else {
			whine();
		}
	} catch(BioJeu::NotImplemented& e) {
		whine();
	}
	flush_out();
        return b?b->Id:-1;
}


void dsmergen(char help[3], int SetID1, int SetID2, char **textout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *textout = Cartage.MerGen(SetID1, SetID2);
   flush_out();
}

void dsmergor(char help[3], int SetID1, int SetID2, char **textout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *textout = Cartage.MergOr(SetID1, SetID2);
   flush_out();
}

void dsinfo(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.AffJeu();
  flush_out();
}

void dsget(char help[3], char ***ltextout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *ltextout = Cartage.GetArbre();
  flush_out();
}

void mrkinfo(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.AffMarq();
  flush_out();
}

void mrkselset(char help[3], int vm[NBM])
{

  /*int vmp[NBM];*/

  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);

  // d�compte du nombre de marqueurs

  int i = 0;
  /*FILE*debug = fopen("MRKSELSET.TXT", "wt");*/

  while (vm[i])
    {
            /*fprintf(debug, "counting markers... %i : %d\n", i, vm[i]);*/
      /*vmp[i] = vm[i];*/
      i++;
    }
  /*fclose(debug);*/

  /*Cartage.ChangeSel(vmp, i);*/
  Cartage.ChangeSel(vm, i);
  flush_out();
}

void mrkselget(char help[3], int **lmiout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.GetSel(lmiout);
  flush_out();
}

void mrkallget(char help[3], int **lmiout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.GetAll(lmiout);
  flush_out();
}

void mrklod2p(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.PrintTwoPointsLOD();
  flush_out();
}

void mrkdouble(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.DumpDouble();
  flush_out();
}

void mrkdist2p(char help[3], char UnitFlag[2])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.PrintTwoPointsDist(UnitFlag);
  flush_out();
}

void mrkfr2p(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.PrintTwoPointsFR();
  flush_out();
}

void mrkmerge(char help[3], int MrkID1, int MrkID2)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Merge(MrkID1,MrkID2);
  flush_out();
}

void mrkmerget(char help[3], char ***ltextout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *ltextout = Cartage.GetMerged();
  flush_out();
}

void _mrkdoubleget(char help[3], double lod_threshold, char ***ltextout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *ltextout = Cartage.GetDouble(lod_threshold);
  flush_out();
}

void mrkname(char help[3], int MrkID, char **textout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *textout = Cartage.GetMrkName(MrkID);
  flush_out();
}

int mrkid(char help[3], char *MrkName)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  int temp = Cartage.GetMrkId(MrkName);
  flush_out();
  return temp;
}

int mrklast(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  int temp = Cartage.LastMrk();
  flush_out();
  return temp;
}

void verbset(char help[3], int flag)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.SetVerbose(flag);
  flush_out();
}

void cgprecision(char help[3], int precision)
{
  Cartage.DistPrecision = ((abs(precision) > 5) ? 5 : abs(precision));
  flush_out();
}

void quietset(char help[3], int flag)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.SetQuiet(flag);
  flush_out();
}

void sem(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.SinglEM();
  flush_out();
}

void nicemapd(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.BuildNiceMap();
  flush_out();
}

void nicemapl(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.BuildNiceMapL();
  flush_out();
}

void mfmapd(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.BuildNiceMapMultiFragment();
  flush_out();
}

void mfmapl(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.BuildNiceMapLMultiFragment();
  flush_out();
}

void build(char help[3],
	   int NC)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Build(NC);
  flush_out();
}

void buildfw(char help[3],
	     double KeepThres,
	     double AddThres,
	     int vm[NBM],
	     int DM)
{
  static int vmp[NBM];

  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);

  // d�compte du nombre de marqueurs

  int i = 0;

  while (vm[i])
    {
      vmp[i] = vm[i];
      i++;
    }

  Cartage.BuildFW(KeepThres, AddThres, vmp, i, DM);
  flush_out();
}

void annealing(char help[3],
	       int Tries,
	       double Tinit,
	       double Tfinal,
	       double Cooling)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Annealing(Tries, Tinit, Tfinal, Cooling);
  flush_out();
}

void greedy(char help[3],
	    int NR,
	    int NI,
	    int TMin,
	    int TMax,
	    int Ratio)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Greedy(NR, NI, TMin, TMax, Ratio);
  flush_out();
}

void algogen( char help[3],
	      int nb_gens,
	      int nb_elements,
	      int selection_number,
	      float pcross,
	      float pmut,
	      int evolutive_fitness)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.AlgoGen(nb_gens,
		  nb_elements,
		  selection_number,
		  pcross,
		  pmut,
		  evolutive_fitness);
  flush_out();
}

void cg2tsp(char help[3], char *fileName)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.cg2tsp(fileName);
  flush_out();
}

void paretolkh(char help[3], int resolution, int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.ParetoLKH(resolution,nbrun,backtrack,contribLogLike2pt1,contribLogLike2pt2);
  flush_out();
}

void paretolkhn(char help[3], int resolution, int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.ParetoLKH(resolution,nbrun,backtrack,normContribLogLike2pt1,normContribLogLike2pt2);
  flush_out();
}

void paretoinfo(char help[3], int graphicalview, double ratio, int **lmapidout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.ParetoInfo(graphicalview,(char*)"~/carthagene/cgscript/",NULL,NULL,ratio,lmapidout);
  flush_out();
}

void paretoinfog(char help[3], double ratio, char ****lpareto)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *lpareto = Cartage.ParetoInfoG(ratio);
  flush_out();
}

void paretogreedy(char help[3],int Anytime)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.ParetoGreedy(Anytime);
  flush_out();
}

void paretogreedyr(char help[3],int Anytime,int MinBP,int MaxBP)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.ParetoGreedy(Anytime,MinBP,MaxBP);
  flush_out();
}

void _mcmc(char help[3], int seed, int nbiter, int burning, int slow_computations, int lim_group_size, int mcmcverbose)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.mcmc(seed,nbiter,burning,contribLogLike2pt1,contribLogLike2pt2, slow_computations, lim_group_size, mcmcverbose);
  flush_out();
}

void lkh(char help[3], int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkh(nbrun,backtrack,contribLogLike2pt1,contribLogLike2pt2);
  flush_out();
}

void ilkh(char help[3], int nbrun, int collectmaps, double threshold, int cost)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkhiter(nbrun,collectmaps,threshold,cost,contribLogLike2pt1,contribLogLike2pt2);
  flush_out();
}

void lkhn(char help[3], int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkh(nbrun,backtrack,normContribLogLike2pt1,normContribLogLike2pt2);
  flush_out();
}

void ilkhn(char help[3], int nbrun, int collectmaps, double threshold, int cost)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkhiter(nbrun,collectmaps,threshold,cost,normContribLogLike2pt1,normContribLogLike2pt2);
  flush_out();
}

void lkhd(char help[3], int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkh(nbrun,backtrack,contribZero,contribHaldane);
  flush_out();
}

void ilkhd(char help[3], int nbrun, int collectmaps, double threshold, int cost)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkhiter(nbrun,collectmaps,threshold,cost,contribZero,contribHaldane);
  flush_out();
}

void lkhl(char help[3], int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkh(nbrun,backtrack,contribZero,contribLOD);
  flush_out();
}

void ilkhl(char help[3], int nbrun, int collectmaps, double threshold, int cost)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkhiter(nbrun,collectmaps,threshold,cost,contribZero,contribLOD);
  flush_out();
}

void lkhocb(char help[3], int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkh(nbrun,backtrack,contribZero,contribOCB);
  flush_out();
}

void ilkhocb(char help[3], int nbrun, int collectmaps, double threshold, int cost)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkhiter(nbrun,collectmaps,threshold,cost,contribZero,contribOCB);
  flush_out();
}

void lkhocbn(char help[3], int nbrun, int backtrack)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkh(nbrun,backtrack,contribZero,normContribOCB);
  flush_out();
}

void ilkhocbn(char help[3], int nbrun, int collectmaps, double threshold, int cost)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.lkhiter(nbrun,collectmaps,threshold,cost,contribZero,normContribOCB);
  flush_out();
}

int mapocb(char help[3], int MapID)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  int res = Cartage.OCB(MapID);
  flush_out();
  return res;
}

void errorestimation(char help[3],int DataID )
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.ErrorEstimation(DataID);
  flush_out();
}

void imputation(char help[3], int DataID, double ConversionCutoff,double CorrectionCutoff,  double UnknownCorrectionCutoff, char **textout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *textout = Cartage.Imputation(DataID, ConversionCutoff,CorrectionCutoff,UnknownCorrectionCutoff);
  flush_out();
}


//void imputation2(char help[3], int id, double ConversionCutoff,double CorrectionCutoff,  double UnknownCorrectionCutoff)
//{
//  BioJeu* b = Cartage.Jeu[id];
//  /* FIXME est-ce vraiment utile d'écraser la sélection courante de CG au lieu de juste créer notre tableau temporaire ? */
//  int* mrksel_backup = Cartage.MarkSelect;
//  int nbms_backup = Cartage.NbMS;
//  /* initialise une sélection de tous les marqueurs du biojeu dans l'ordre du biojeu */
//  Cartage.MarkSelect = b->GetFullMarkSel();
//  Carte tmpmap(&Cartage, b->NbMarqueur, Cartage.MarkSelect);
//  /*Cartage.Imputation(ConversionCutoff,CorrectionCutoff,UnknownCorrectionCutoff,fileName);*/
//  b->ComputeEM(&tmpmap);
//  b->Imputation(ConversionCutoff, CorrectionCutoff, UnknownCorrectionCutoff, (char*)"");
//  delete[] Cartage.MarkSelect;
//  Cartage.NbMS = nbms_backup;
//  Cartage.MarkSelect = mrksel_backup;
//  flush_out();
//  return b->Id;
//}

void polish(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Polish();
  flush_out();
}

void polishtest(char help[3], int vm[NBM])
{

  static int vmp[NBM];

  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);

  // d�compte du nombre de marqueurs

  int i = 0;

  while (vm[i])
    {
      vmp[i] = vm[i];
      i++;
    }
  Cartage.Polishtest(vmp, i);
  flush_out();
}

void flips(char help[3], int ws, double Thres, int Fiter)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Flips(ws, Thres, Fiter);
  flush_out();
}

void heapequiset(char help[3], int flag)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Heap->Equi(flag);
  Cartage.ResizeHeap(Cartage.Heap->HeapSize);
  flush_out();
}

void heapsize(char help[3], int hs)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.ResizeHeap(hs);
  flush_out();
}

void paretoheapsize(char help[3], int hs)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.SetParetoHeapSize(hs);
  flush_out();
}

int heapsizeget(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  int temp = Cartage.Heap->MaxHeapSize;
  flush_out();
  return temp;
}

void heaprinto(char help[3], int NbMrk, int Blank, int Comp)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Heap->PrintO(NbMrk, Blank, Comp);
  flush_out();
}

void heaprint(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Heap->PrintSort();
  flush_out();
}

void heaprintd(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Heap->PrintDSort();
  flush_out();
}

void heapget(char help[3], char UnitFlag[2], int NbMap, char *****llmapout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *llmapout = Cartage.GetHeap(UnitFlag, NbMap);
  flush_out();
}

void maprint(char help[3], int MapID)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.PrintMap(MapID);
  flush_out();
}

void maprintd(char help[3], int MapID)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.PrintDMap(MapID, 0);
  flush_out();
}

void maprintdr(char help[3], int MapID)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.PrintDMap(MapID, 1);
  flush_out();
}

void mapget(char help[3], char UnitFlag[2], int MapID, char ****lmapout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *lmapout = Cartage.GetMap(UnitFlag, MapID);
  flush_out();
}

void mapordget(char help[3], int MapID, int **lmiout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.GetOrdMap(MapID, lmiout);
  flush_out();
}

int group(char help[3], double DistThres, double LODThres)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  int temp = Cartage.Groupe(DistThres, LODThres);
  flush_out();
  return temp;
}

void groupget(char help[3], int GrID, int **lmiout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.GetGroupeI(GrID, lmiout);
  flush_out();
}

int dsbp(char help[3], int SetID1, int SetID2)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  int res = Cartage.BreakPoints(SetID1, SetID2);
  flush_out();
  return res;
}

double dsbpcoef(char help[3], int SetID, double NewCoefValue)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  double res = Cartage.SetBreakPointCoef(SetID, NewCoefValue);
  flush_out();
  return res;
}

double dsbplambda(char help[3], int SetID, int CoefAutoFit, double NewLambdaValue)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  double res = Cartage.SetBreakPointLambda(SetID, CoefAutoFit, NewLambdaValue);
  flush_out();
  return res;
}

int mapbp(char help[3], int SetID, int MapId)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  int res = Cartage.BreakPointsMap(SetID, MapId);
  flush_out();
  return res;
}

void cgout(char help[3], char *fileName)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Tracer(fileName);
  flush_out();
}

void cgtolerance(char help[3], double Threshold1, double Threshold2)
{
  Cartage.SetTolerance(Threshold1,Threshold2);
  flush_out();
}

void cg2pt(char help[3], int mode)
{
  Cartage.SetEM2pt(mode);
  flush_out();
}

void cgrobustness(char help[3], double Threshold)
{
  Cartage.SetRobustness(Threshold);
  flush_out();
}

void cgnotrobust(char help[3])
{
  Cartage.SetRobustness(1e100);
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  flush_out();
}

void cgversion(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  signal(SIGINT, hand);
#ifndef __WIN32__
  signal(SIGXCPU, hand);
#endif
  Cartage.Version();
  flush_out();
}

void cgstat(char help[3], double LODThres, char **textout)
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  *textout = Cartage.Heap->Stat(LODThres);
  flush_out();
}

void cgrestart(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  Cartage.Clear();
  flush_out();
}

void cglicense(char help[3], char *Key)
{

  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  signal(SIGINT, hand);
#ifndef __WIN32__
  signal(SIGXCPU, hand);
#endif
  Cartage.License(Key);
  flush_out();
}

void cgstop(char help[3])
{
  Cartage.StopFlag = 0;
  Utils::Interrupt::flag(false);
  raise(SIGINT);
  flush_out();
}


/*void cg_get_matrix_mode(char help[3], char**textout) {*/
        /**textout=strdup(TwoPoint::Backend::Factory::get_default());*/
/*}*/

/*void cg_set_matrix_mode(char help[3], char*backend) {*/
        /*TwoPoint::Backend::Factory::set_default(backend);*/
/*}*/


