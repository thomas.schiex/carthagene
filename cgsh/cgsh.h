//-----------------------------------------------------------------------------
//                            CarthaGene
//
// Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex.
// Unite de Biometrie et Intelligence Artificielle, Toulouse, France
//
//  Copyright (C) 1997 INRA (Institut National de la Recherche Agronomique).
//
// $Id: cgsh.h,v 1.59.2.5 2012-06-13 14:19:49 dleroux Exp $
//
//
//    This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Module 	: cgsh
// Projet	: CartaGene
//
// Description  : Prototype de l''interface g�n�rale de la librairie
// pour le langage de script
//
// Chaque fonction correspond � une commande.
// Comme l''interface est produite automatiquement, il est IMPORTANT,
// de respecter un certain nombre de conventions.
//
// Il est judicieux de nommer les commandes et les argiuments de fa�on
// homog�ne, sans utiliser de majuscules, ni de car, sp�ciaux.
//
// Quelques principes de base pour nommer les commandes :
// - le nom des commandes commence par un pr�fix(ds pour Data Set, gr pour group ...) permettant de regrouper le plus possible les commandes en familles.
// - il est suivi d''un verbe(load, merge...)
// - enfin on peut rajouter quelques infos(idm pour ID de marqueur, nmm nom de marqueur....) permettant de rajouter un peu de s�mantique.
// - Penser � l''utilisateur final, trouver le compromis entre la concision et la s�mantique du mot.
//
// Exemple : grgetidm veut dire acquisition d''un group sous la forme d''une liste d''identifiant de marqueurs.
//
// Quelques principes de base pour nommer les arguments :
// - Ne pas ajouter des noms d'' arguments existant d�ja.
// - Les majuscules sont autoris�es(l''utilisateur n''a pas � les saisirs)
// - Assembler les mots du g�n�ral au pr�cis SetId, et nom pas IdSet.
//
// - les arguments "automatiques" :
// char help[3] est un argument OBLIGATOIRE. permettant d''impl�menter -h.
// char **textout est un argument standard
// pour retourner une chaine de caract�re � Tcl.
// int **lmiout est un argument standard
// pour retourner une liste d''indice de marqueur � Tcl.
//
// Encore un conseil :
// Il est juducieux de rajouter un minimum de commentaires
// devant chaque fonction pour la documentation technique automatique,
// en fran�ais.
//-----------------------------------------------------------------------------
//-INRA--------------------Station de Biom�trie et d''Intelligence Artificielle
//-----------------------------------------------------------------------------

#ifndef _INTERFACE_H
#define _INTERFACE_H

#include "../calcul/CartaGene.h"
#include "../calcul/tsp.h"

#define NBM 131072 // taille max d''une liste de marqueurs.

/** Chargement d'un jeu de donn�es.

    @param help pour l'option -h.
    @param FileName nom de fichier contenant un jeu de donn�es.
    @return textout une ligne de texte informative(out).
*/
void dsload(char help[3], char *FileName, char **textout);

/** Sauvegarde d'un jeu de donn�es.

    @param help pour l'option -h.
    @param id le num�ro du jeu de donn�es.
    @param suffixe à ajouter au nom du fichier original. Si vide (""), la date au format YYYY-MM-DD est utilisée.
    @return l
*/
char* _dsave(char help[3], int id, char suffix[256]);

/** Définit le nombre de threads concurrents pour les calculs parallélisés.

    @param help pour l'option -h.
    @param num le nombre de threads concurrents + 1 (thread contrôleur).
*/
void cg_setparallel(char help[3], int num);


/** Détermine le nombre de threads concurrents pour les calculs parallélisés.

    @param help pour l'option -h.
    @return le nombre de threads concurrents + 1 (thread contrôleur).
*/
int cg_getparallel(char help[3]);


/** Définit le chemin où stocker les fichiers .2pt.

    @param help pour l'option -h.
    @param path le chemin à utiliser
*/
void cg_set2ptcachepath(char help[3], char* path);


/** Détermine le chemin où les fichiers .2pt sont stockés.

    @param help pour l'option -h.
    @return le chemin utilié
*/
char* cg_get2ptcachepath(char help[3]);


/** Clonage d'un jeu de donn�es.

    @param help pour l'option -h.
    @param id le num�ro du jeu de donn�es à cloner.
    @return l
*/
int dsclone(char help[3], int id);

/** Conversion d'un jeu de donn�es RH*.

    @param help pour l'option -h.
    @param id le num�ro du jeu de donn�es à cloner.
    @param new_type "diploid" ou "haploid" ou "error".
    @return l
*/
int dsrhconv(char help[3], int id, char*new_type);

/** Cr�ation un jeu de donn�es fusionnant les deux autres sur l'ordre.

   @param help pour l'option -h.
   @param SetID1 identifiant d'un jeu existant.
   @param SetID2 identifiant d'un jeu existant.
   @param textout une ligne de texte informative(out).
*/
void dsmergor(char help[3], int SetID1, int SetID2, char **textout);


/** Cr�ation un jeu de donn�es fusionnant les deux autres g�n�tiquement.

   @param help pour l'option -h.
   @param SetID1 identifiant d'un jeu existant.
   @param SetID2 identifiant d'un jeu existant.
   @param textout une ligne de texte informative(out).
*/
void dsmergen(char help[3], int SetID1, int SetID2, char **textout);

/** Affichage d'un r�sum� sur tous les jeux de donn�es.

    @param help pour l'option -h
*/
void dsinfo(char help[3]);

/** retourne une liste des r�sum�s de tous les jeux de donn�es.

    @param help pour l'option -h
*/
void dsget(char help[3], char ***ltextout);

/** Affichage de tous les marqueurs et des BioJeus auquels ils appartiennent.

    @param help pour l'option -h
*/
void mrkinfo(char help[3]);

/** pour positionner la s�lection des marqueurs

    @param help pour l'option -h.
    @param MrkList vecteur de marqueurs.
  */
void mrkselset(char help[3], int MrkList[NBM]);

/** pour r�cup�rer la s�lection des marqueurs

    @param help pour l'option -h.
    @param lmiout une liste d'id de marqueurs(out).
  */
void mrkselget(char help[3], int **lmiout);

/** pour r�cup�rer la s�lection des marqueurs

    @param help pour l'option -h.
    @param lmiout une liste d'id de marqueurs(out).
  */
void mrkallget(char help[3], int **lmiout);

/** Affichage de la matrice LOD 2 points pour la s�lection

    @param help pour l'option -h
*/
void mrklod2p(char help[3]);

/** Affichage des marqueurs doubles

    @param help pour l'option -h
*/
void mrkdouble(char help[3]);

/** Affichage de la matrice distance  2 points pour la s�lection

    @param help pour l'option -h
*/
void mrkdist2p(char help[3], char UnitFlag[2]);

/** Affichage de la matrice FR  2 points pour la s�lection

    @param help pour l'option -h
*/
void mrkfr2p(char help[3]);

/** Fusion de deux marqueurs compatibles

   @param help pour l'option -h.
*/

void mrkmerge(char help[3], int MrkID1, int MrkId2);

/** To retrieve the merged Loci

   @param help pour l'option -h.
*/
void mrkmerget(char help[3], char ***ltextout);
void _mrkdoubleget(char help[3], double lod_threshold, char ***ltextout);


/** Convertion d'un identifiant num�rique en identifiant textuel.

   @param help pour l'option -h.
   @param MrkID identifiant d'un locus existant.
   @param textout un nom de marqueur, ou une chaine vide.
*/
void mrkname(char help[3], int MrkID, char **textout);

/** Et r�ciproquement.

   @param help pour l'option -h.
   @param MrkName identifiant d'un locus existant.
   @return un num�ro de marqueur ou -1.
*/
int mrkid(char help[3], char *MrkName);

/** Le num�ro du dernier marqueur
    .
    @param help pour l'option -h.
    @return un num�ro de marqueur.
*/
int mrklast(char help[3]);

/** Affectation de la verbosit�

    @param help pour l'option -h.
    @param Flag le niveau
*/
void verbset(char help[3], int Flag);

void cgprecision(char help[3], int precision);

/** Affectation du silencieux

    @param help pour l'option -h.
    @param Flag le niveau
*/
void quietset(char help[3], int Flag);


/** pour faire un simple EM sur l'ordre de la s�lection

    @param help pour l'option -h.
*/
void sem(char help[3]);

/** heuristique bas�e sur le crit�re de distance

    @param help pour l'option -h.
*/
void nicemapd(char help[3]);

/** heuristique bas�e sur le crit�re de distance

    @param help pour l'option -h.
*/
void mfmapd(char help[3]);

/** heuristique bas�e sur le crit�re de LOD

    @param help pour l'option -h.
*/
void nicemapl(char help[3]);

/** heuristique bas�e sur le crit�re de LOD

    @param help pour l'option -h.
*/
void mfmapl(char help[3]);

/** Build

    @param NbMap le nombre de cartes construites simultan�ments
*/
void build(char help[3],
	   int NbMap);

/** Build

    @param AddThres minimum n�cessaire � l'acceptation de l'insertion d'un marqueur
    @param keepThres Seuil � l'int�rieur duquel les ordres sont conserv�s.
    @param MrkList vecteur de marqueurs.
    @param MrkList entier qui indique que l'on veut tester les autres marqueurs 0 pas test, 1 construction et test, 2 test
*/
void buildfw(char help[3],
	     double KeepThres,
	     double AddThres,
	     int MrkList[NBM],
	     int Flag);

/** Recuit Simul�
    La recherche de la meilleure carte est faite avec l'algorithme de Recuit.
    Durant la recherche, les cartes trouv�es sont stock�es dans le tas et
    rang�es dans l'ordre de cout d�croissant.
    @param NbTries Nombre d'essai par temp�rature.
    @param InitTemp Temp�rature de d�but.
    @param FinalTemp Temp�rature finale.
    @param Cooling Vitesse de refroidissement.
*/
void annealing(char help[3],
	       int NbTries,
	       double InitTemp,
	       double FinalTemp,
	       double Cooling);

/** Lancement de Greedy.
    @param NbLoop Nombre de boucles principales.
    @param NbIter Nombre d'it�rations.
    @param TabooMin TabooMin.
    @param TabooMax TabooMax.
*/
void greedy(char help[3],
	    int NbLoop,
	    int NbIter,
	    int TabooMin,
	    int TabooMax,
	    int Ratio);

/** Algorithme genetique.
    @param NbGen: Nombre de generations (integer)
    @param NbMap: Nombre d'elements dans la population (integer)
    @param SelType:# Selection type :(integer)
    #	0 -> roulette wheel
    #	1 -> stochastic remainder without replacement
    @param ProbaCross: Probabilities of crossover and mutation (float)
    @param ProbaMut: Probabilities of crossover and mutation (float)
    @param EvolFitn:Evolutive Fitness (integer)
    (the fitness of the same element changes with the generation number)

*/
void algogen( char help[3],
	      int NbGen,
	      int NbMap,
	      int SelType,
	      float ProbaCross,
	      float ProbaMut,
	      int EvolFitn);

void cg2tsp(char help[3], char *fileName);
void paretolkh(char help[3], int resolution, int nbrun, int backtrack);
void paretolkhn(char help[3], int resolution, int nbrun, int backtrack);
void paretoinfo(char help[3], int graphicalview, double ratio, int **lmapidout);
void paretoinfog(char help[3], double ratio, char ****lpareto);
void paretogreedy(char help[3],int Anytime);
void paretogreedyr(char help[3],int Anytime,int MinBP,int MaxBP);
void _mcmc(char help[3], int seed, int nbiter, int burning, int slow_computations, int lim_group_size, int mcmcverbose);
void lkh(char help[3], int nbrun, int backtrack);
void ilkh(char help[3], int nbrun, int collectmaps, double threshold, int cost);
void lkhn(char help[3], int nbrun, int backtrack);
void ilkhn(char help[3], int nbrun, int collectmaps, double threshold, int cost);
void lkhd(char help[3], int nbrun, int backtrack);
void ilkhd(char help[3], int nbrun, int collectmaps, double threshold, int cost);
void lkhl(char help[3], int nbrun, int backtrack);
void ilkhl(char help[3], int nbrun, int collectmaps, double threshold, int cost);
void lkhocb(char help[3], int nbrun, int backtrack);
void ilkhocb(char help[3], int nbrun, int collectmaps, double threshold, int cost);
void lkhocbn(char help[3], int nbrun, int backtrack);
void ilkhocbn(char help[3], int nbrun, int collectmaps, double threshold, int cost);
int mapocb(char help[3], int MapID);


/** Estimation de l'erreur **/

void errorestimation(char help[3], int id);
/** pour l'imputation des typages erronées **/
/* Only available for haploid radiated hybrid data type : BJS_RH */
void imputation(char help[3], int DataID, double ConversionCutoff,double CorrectionCutoff, double UnknownCorrectionCutoff, char** textout );

/** pour faire un polish sur la meilleure carte du tas

    @param help pour l'option -h.
*/
void polish(char help[3]);

/** pour faire un polish sur la meilleure carte du tas avec liste
    de marqueurs
    @param MrkList vecteur de marqueurs.
    @param help pour l'option -h.
*/
void polishtest(char help[3], int MrkList[NBM]);

/** pour faire des flips sur la meilleure carte du tas

    @param help pour l'option -h.
    @param WinSize taille de lafen�tre
    @param Thres le seuil
    @param Iter flag pour it�ratif ou non

*/
void flips(char help[3], int WinSize, double Thres, int Iter);

/** to set the equivalence mode of the heap

    @param help pour l'option -h.
    @param flag 0 ou 1
*/
void heapequiset(char help[3], int flag);

/** to set the size of the heap

    @param help pour l'option -h.
    @param hs la taille du tas
*/
void heapsize(char help[3], int NbMap = 5);

/** to set the size of the heap used by pareto functions

    @param help pour l'option -h.
    @param hs la taille du tas
*/
void paretoheapsize(char help[3], int NbMap = 1024);

/** to get the size of the heap

    @param help to get help.
    @return the size of the heap
*/
int heapsizeget(char help[3]);

/** pour un affichage avec analyse du tas

    @param help pour l'option -h.
    @param NbMrk le nombre de marqueur min permettant de consid�rer un interval
    @param Blank si 1 carac. blanc pour l'interval sinon l'indice
    @param Comp  si 1 affichage des bornes uniquement
*/
void heaprinto(char help[3], int NbMrk, int Blank, int Comp);

/** pour un affichage du tas

    @param help pour l'option -h.
*/
void heaprint(char help[3]);

/** pour un affichage d�taill� du tas

    @param help pour l'option -h.
*/
void heaprintd(char help[3]);

/** pour r�cup�rer les cartes sous la forme d'une liste

    @param help pour l'option -h.
    @param UnitFlag k ou h
    @param NbMap le nombre de cartes � retourner � partir de la meilleure
    @return llmaplout liste de listes de cartes.
*/
void heapget(char help[3], char UnitFlag[2], int NbMap, char *****llmapout);

/** Affichage simple d'une carte

    @param help pour l'option -h.
    @param nbmap le num�ro de la carte dans le tas
*/
void maprint(char help[3], int MapID);

/** Affichage d�taill� d'une carte

    @param help pour l'option -h.
    @param nbmap le num�ro de la carte dans le tas
*/
void maprintd(char help[3], int MapID);

/** Affichage d�taill� d'une carte "� l'envers"

    @param help pour l'option -h.
    @param nbmap le num�ro de la carte dans le tas
*/
void maprintdr(char help[3], int MapID);

/** pour r�cup�rer une carte sous la forme d'une liste

    @param help pour l'option -h.
    @param UnitFlag k ou h
    @param nbmap le num�ro de la carte dans le tas
    @return lmapout listes de cartes.
*/

void mapget(char help[3], char UnitFlag[2], int MapID, char ****lmapout);

/** pour r�cup�rer une carte sous la forme d'une liste

    @param help pour l'option -h.
    @param MapID le num�ro de la carte dans le tas
    @return lmiout liste d'indices de locus
*/
void mapordget(char help[3], int MapID, int **lmiout);

/** Calcul et affiche les groupes d'un jeu.

    @param help pour l'option -h.
    @param DistThres Seuil de distance.
    @param LODThres Seuil de LOD.
    @return le nombre de groupes trouv�s.
*/
int group(char help[3], double DistThres, double LODThres);

/** R�cup�ration d''un groupe par id de marqueurs.

    @param help pour l'option -h.
    @param GrID id d'un groupe pr�alablement calcul�
    @param lmiout une liste d'id de marqueurs(out).
*/
void groupget(char help[3], int GrID, int **lmiout);

/** Computes the number of breakpoints between two order data set

   @param help pour l'option -h.
   @param SetID1 identifiant d'un jeu d'ordre existant.
   @param SetID2 identifiant d'un jeu existant.
*/
int dsbp(char help[3], int SetID1, int SetID2);

/** Modifies the coefficient factor applied to the number of breakpoints for a given order data set

   @param help pour l'option -h.
   @param SetID identifiant d'un jeu d'ordre existant.
*/
double dsbpcoef(char help[3], int SetID, double NewCoefValue);

/** Modifies the lambda parameter value for a given order data set

   @param help pour l'option -h.
   @param SetID identifiant d'un jeu d'ordre existant.
*/
double dsbplambda(char help[3], int SetID, int CoefAutoFit, double NewLambdaValue);

/** Computes the number of breakpoints between one order data set and a map

   @param help pour l'option -h.
   @param SetID identifiant d'un jeu d'ordre existant.
   @param MapID identifiant d'une carte existante.
*/
int mapbp(char help[3], int SetID, int MapId);

/** Sp�cifier le nom du fichier de trace.

    @param help pour l'option -h.
    @param FileName nom de fichier texte.
*/
void cgout(char help[3], char *FileName);

/** Fixer les tolerances de convergence

    @param help pour l'option -h.
    @param Threshold1 seuil de convergence fin
    @param Threshold2 seuil de convergence grossier
*/
void cgtolerance(char help[3], double Threshold1,double Threshold2);

/** Fixer le mode de calcul de la vraisemblance

    @param help pour l'option -h.
    @param Mode
*/
void cg2pt(char help[3], int mode);

/** Fixer le seuil de fiabilit� des cartes frameworks

    @param help pour l'option -h.
    @param Threshold �cart de vraisemblance relatif � la meilleure carte
*/
void cgrobustness(char help[3], double Threshold);

/** Inhiber le seuil de fiabilit� des cartes frameworks

    @param help pour l'option -h.
*/
void cgnotrobust(char help[3]);

/** Obtenir la version

    @param help pour l'option -h.
*/
void cgversion(char help[3]);

/** pour obtenir des informations permettant de faire des stats

    @param help pour l'option -h.
    @param LODThres Seuil de LOD.
    @return textout une ligne de texte informative(out).
*/

void cgstat(char help[3], double LODThres, char **textout);

/** Red�marrage � vide de Cartagene

    @param help pour l'option -h.
*/
void cgrestart(char help[3]);

/** Entre la clef de license

    @param help pour l'option -h.
    @param Key la clef.
*/
void cglicense(char help[3], char *Key);

/** Arr�te une commande

    @param help pour l'option -h.
*/
void cgstop(char help[3]);

/*void cg_get_matrix_mode(char help[3], char**textout);*/
/*void cg_set_matrix_mode(char help[3], char*backend);*/
#endif
