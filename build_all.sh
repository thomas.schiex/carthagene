#!/bin/bash

DIR=$PWD

#JOBS="-j 9"

export DO_STATUS=""

function do_helper() {
	$*
	export DO_STATUS=$?
}

function do_() {
	DEBUG=0
	#DEBUG=1
	test $DEBUG == 0 && do_helper $* || echo  '>> ' $*
	test $DO_STATUS != 0 && echo "Command \"$*\" failed with exit code $DO_STATUS. Aborting."
	test $DO_STATUS != 0 && exit -1
}

function do_can_fail() {
	#DEBUG=1
	DEBUG=0
	test $DEBUG == 0 && do_helper $* || echo  '>> ' $*
}

PATH_BACKUP=$PATH

CF=$CFLAGS
CXXF=$CXXFLAGS

function build_one() {
	echo BUILDCONF : $1 $2 $3 $4
	BUILDDIR="build"
        mkdir -p $BUILDDIR
        touch $BUILDDIR/.ARK_NOBACKUP   # pas de sauvegarde
	CMARGS="-D_EMBED_LIBS=OFF -DWITH_FRAMEWORK=ON -DWITH_PARALLEL=ON -DWITH_TESTS=OFF -DWORKAROUND_CACHE_FILE=ON"
    CFLAGS="$CF"
    CXXFLAGS="$CXXF"
	case $1 in
		win32)
			BUILDDIR="$BUILDDIR/win32"
			CMARGS="$CMARGS -DCMAKE_TOOLCHAIN_FILE=$DIR/Toolchain-mingw32.cmake"
            export BOOST_ROOT=$HOME/win32
            export BOOST_INCLUDEDIR=$BOOST_ROOT/include
            export BOOST_LIBRARYDIR=$BOOST_ROOT/lib
			;;
		linux32)
            CFLAGS="-m32 $CF"
            CXXFLAGS="-m32 $CXXF"
			BUILDDIR="$BUILDDIR/linux32"
			CMARGS="$CMARGS -DCMAKE_INSTALL_PREFIX=/usr -D_32BIT=ON"
			PATH=/usr/bin32:$PATH_BACKUP
			;;
		linux64)
			BUILDDIR="$BUILDDIR/linux64"
			CMARGS="$CMARGS -DGENERATE_DOC=ON -DCMAKE_INSTALL_PREFIX=/usr -D_32BIT=OFF"
			#CMARGS="$CMARGS -DGENERATE_DOC=OFF -DCMAKE_INSTALL_PREFIX=/usr -D_32BIT=OFF"
			PATH=$PATH_BACKUP
			;;
	esac
	case $3 in
		lkh)
			BUILDDIR="$BUILDDIR-LKH"
			CMARGS="$CMARGS -DWITH_LKH=ON"
			;;
		*)
			CMARGS="$CMARGS -DWITH_LKH=OFF"
			;;
	esac
	case $4 in
		gmp)
			BUILDDIR="$BUILDDIR-GMP"
			CMARGS="$CMARGS -DWITH_GMP=ON"
			;;
		*)
			CMARGS="$CMARGS -DWITH_GMP=OFF"
			;;
	esac
	case $2 in
		opt)
			BUILDDIR="$BUILDDIR/Release"
			CMARGS="$CMARGS -DCMAKE_BUILD_TYPE=RelWithDebInfo"
			;;
		debug)
			BUILDDIR="$BUILDDIR/Debug"
			CMARGS="$CMARGS -DCMAKE_BUILD_TYPE=Debug"
			;;
	esac
	do_ mkdir -p $BUILDDIR
	ROOT=$PWD
	do_ pushd $BUILDDIR
	CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS" do_ cmake $CMARGS ../../..
	do_ make VERBOSE=1 $JOBS

	[ "$1" == "linux64" ] && (
		echo "PWD=$PWD"
		[ "$3" != "lkh" ] && do_ cp doc/user/User.pdf $ROOT/Doc-carthagene.pdf || do_ cp doc/user/User.pdf $ROOT/Doc-carthagene-nonfree.pdf;
		[ "$3" != "lkh" ] && do_ cp doc/user/User.ps.gz $ROOT/Doc-carthagene.ps.gz || do_ cp doc/user/User.ps.gz $ROOT/Doc-carthagene-nonfree.ps.gz;
	)

	do_can_fail sudo make $JOBS package			# can fail because of improper support of RPM cross-build
	[ "$1" != "win32" ] && do_ sudo cpack -G STGZ
	#do_ make package_source
	do_ popd
	do_ mkdir -p packages
	echo "=============================================="
	echo "Copying generated packages..."
	do_can_fail cp -v $BUILDDIR/carthagene-*.{deb,sh,rpm,exe} packages 2> /dev/null
	echo "=============================================="
}


case `uname -m` in
	*64)	ARCH="linux64 linux32"
		echo "Compiling 64-bit AND 32-bit binaries."
		;;
	*6)	ARCH="linux32"
		echo "Not compiling 64-bit binaries, only 32-bit."
		;;
esac


OPT="opt"
#OPT="debug"
LKH="lkh _"
GMP="_"


if [ "x$1" == "x" ]; then
    ALL="$ARCH win32"
    rm -fv $PWD/packages/*.*
else
    ALL="$*"
fi
 
for platform in $ALL; do
#for platform in linux32; do
	for build in $OPT; do
		for lkh in $LKH; do
			for gmp in $GMP; do
				# attempt to build everything in parallel
				(build_one $platform $build $lkh $gmp)
				echo
			done;
		done;
	done;
done

echo "#############"
echo "##"
echo "## packages are available in $PWD/packages :"
echo "##"
echo "#############"

ls -l packages

