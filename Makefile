#============================================================================
# Main Makefile pour CartaGene
#
# $Id: Makefile,v 1.25 2008-05-09 10:36:07 tschiex Exp $
#
# This is a GNU Makefile. It may not work with a plain make.
#============================================================================
#
# comment this line if you are not allowed to use the LKH package (See README file) 
export LKH_PATH = LKH
#

# uncomment this line for distribution
export DISTRIB = true
export CXX = g++

dircalcul = calcul
dirilc = cgsh
dirihm = cgwin
dirdoc = doc/user
version = 1.1

PWD = $(shell pwd)

ifeq ($(strip $(LKH_PATH)),)
subdirs = $(dircalcul) $(dirilc) 
else
subdirs = $(dircalcul) $(LKH_PATH) $(dirilc) 
endif

all: shared_lib

shared_lib: 
	for dir in $(subdirs) ; do \
		(cd $$dir; $(MAKE)) \
	done;

tclshrc:	$(dirilc)/.tclshrc
	sed 's|""|"$(PWD)"|' < $(dirilc)/.tclshrc > tclshrc

install:	$(dirilc)/libcartagene.so  tclshrc
	cp $(dirihm)/.cgwopt ~/
	cp $(dirihm)/.cgwdefalgo ~/
	cp -i tclshrc ~/.tclshrc
	echo To run the shell interface, just run 'tclsh'
	echo To run the graphical interface,
	echo you must put $(PWD)/cgwin in your path
	echo and launch CGW.tcl

doc:
	cd $(dirdoc); make User.pdf

#prepare: MAINTENANT DANS makefile de distrib ->  tag source
#	-rm -rf interfaces
#	mv -f test/Data doc/user/exemple
#	-rm -rf test
#	-rm -f ToDo
#	mv -f distrib/License .
#	mv -f distrib/GPL.txt .
#	mv -f distrib/QPL.txt .
#	mv -f distrib/README .
#	\rm -rf distrib
#	\rm -rf web
#	sed 's|X.X|$(version)|' < cgsh/pkgIndex.tcl > tmpfile
#	mv -f tmpfile cgsh/pkgIndex.tcl
#	sed 's|X.X|$(version)|' < cgscript/pkgIndex.tcl > tmpfile
#	mv -f tmpfile cgscript/pkgIndex.tcl
#	make clean
#	cd $(dirilc); make cgsh_wrap.c
#	cd ..;  ln -s dev CarthaGene-source-$(version); tar cvhfz CarthaGene-source-$(version).tgz --exclude '*/CVS*' CarthaGene-source-$(version)

clean:
	-rm -f tclshrc
	for dir in $(subdirs) ; do \
		(cd $$dir; $(MAKE) $@) \
	done;


