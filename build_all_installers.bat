@echo off

rem call "C:\Program Files\Microsoft Visual Studio 9.0\VC\bin\vcvars32.bat"

rem pushd

C:


rem ###########################################################################
rem ###########################################################################


echo "===================================================================="
echo " BUILDING WITHOUT LKH"
echo "=========================="

if not exist C:\CARTHAGENE_BUILD_NO_LKH		mkdir C:\CARTHAGENE_BUILD_NO_LKH

cd C:\CARTHAGENE_BUILD_NO_LKH
cmake -D_WITH_LKH=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo -G "NMake Makefiles" X:\carthagene
rem nmake VERBOSE=1 package
nmake package
rem nmake neotest


rem ###########################################################################
rem ###########################################################################


echo "===================================================================="
echo " BUILDING WITH LKH"
echo "=========================="

if not exist C:\CARTHAGENE_BUILD_LKH		mkdir C:\CARTHAGENE_BUILD_LKH

cd C:\CARTHAGENE_BUILD_LKH
cmake -D_WITH_LKH=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo -G "NMake Makefiles" X:\carthagene
rem nmake VERBOSE=1 package
nmake package
rem nmake neotest

rem ###########################################################################
rem ###########################################################################


if not exist C:\CARTHAGENE_INSTALLERS		mkdir C:\CARTHAGENE_INSTALLERS
copy C:\CARTHAGENE_BUILD_NO_LKH\CarthaGene-1.2.0-win32.exe C:\CARTHAGENE_INSTALLERS
copy C:\CARTHAGENE_BUILD_LKH\CarthaGene-1.2.R-win32.exe C:\CARTHAGENE_INSTALLERS





explorer C:\CARTHAGENE_INSTALLERS

rem popd
x:
