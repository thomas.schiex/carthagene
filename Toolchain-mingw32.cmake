
# Très largement inspiré du fichier Toolchain-mingw32 de VLE
# http://vle.toulouse.inra.fr/mediawiki/index.php/Cross_compilation_Win32
#


# Le nom de la plate-forme cible.
SET(CMAKE_SYSTEM_NAME Windows)
SET(CMAKE_SYSTEM_VERSION 1)
 
# Les variables qui définissent les compilateur à utiliser.
SET(CMAKE_C_COMPILER /usr/bin/i586-mingw32msvc-gcc)
SET(CMAKE_CXX_COMPILER /usr/bin/i586-mingw32msvc-c++)

# Une variable qui définit les chemins où chercher les bibliothèques et
# autres fichiers d'entêtes pour la plate-forme cible. Le deuxième chemin
# est ici égal à la variable $prefix
SET(CMAKE_FIND_ROOT_PATH /usr/i586-mingw32msvc $ENV{HOME}/win32)
 
# Quelques définition de variables à passer au script de compilation Cmake.
SET(CMAKE_BUILD_TYPE RelWithDebInfo)
#SET(Boost_ADDITIONAL_VERSIONS "1.39" "1.39.0")
#SET(BOOST_ROOT           /home/goth/tmp/win32)
#SET(BOOSTROOT            /home/goth/tmp/win32)
#SET(BOOST_INCLUDEDIR     /home/goth/tmp/win32/include)
#SET(BOOST_LIBRARYDIR     /home/goth/tmp/win32/lib)
#SET(GTKMM_BASEPATH       /home/goth/tmp/win32)
#SET(MINGW_BASEPATH       /home/goth/tmp/win32)
#SET(Boost_COMPILER       "-mgw42")
 
# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

#add_definitions(_DEBUG _WINDOWS _USRDLL CARTA_EXPORTS __MINGW32__)
add_definitions(-D_DEBUG -D_WINDOWS -D_USRDLL -DCARTA_EXPORTS -D__MINGW32__)
set(GLOBAL_DEFS "-D_DEBUG -D_WINDOWS -D_USRDLL -DCARTA_EXPORTS -D__MINGW32__")
#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--add-stdcall-alias,--dll")
set(BOOST_IOSTREAMS_DLL "$ENV{HOME}/win32/lib/libboost_iostreams-gcc44-mt-1_40.dll")
set(BOOST_IOSTREAMS_LIB "${BOOST_IOSTREAMS_DLL}.a")
set(LIBERTY "$ENV{HOME}/win32/mingw/lib/libiberty.a")
