#include "cxxtest/TestSuite.h"
#include "calcul/CartaGene.h"

#include <iostream>

#define DATA_PATH "doc/user/exemple/Data/"

#define DISABLED

class TestCartage : public CxxTest::TestSuite {
	public:
		void testDummy() {
			TS_ASSERT(1);
		}
		void DISABLED testMultipleDsload1() {
			CartaGene c1;
			CartaGene c2;
			c1.SetQuiet(1);
			c2.SetQuiet(1);
			c1.CharJeu(DATA_PATH "bc.cg");
			c1.SinglEM();
			double referenceScore = c1.Heap->Best()->coutEM;

			c2.CharJeu(DATA_PATH "bc1.cg");
			c2.CharJeu(DATA_PATH "bc.cg");
			c2.SinglEM();
			double scoreAfterTwoDsloads = c2.Heap->Best()->coutEM;
			double delta = c2.Jeu[2]->Epsilon2;
			std::cerr << "epsilon2 = " << delta << std::endl;
			std::cerr << "epsilon1 = " << c2.Jeu[2]->Epsilon1 << std::endl;
			TS_ASSERT_DELTA(scoreAfterTwoDsloads, referenceScore, delta);
		}

		void disabled_testMultipleDsload2() {
			CartaGene c1;
			CartaGene c2;
			c1.SetQuiet(1);
			c2.SetQuiet(1);
			c1.CharJeu(DATA_PATH "bc1.cg");
			c1.SinglEM();
			double referenceScore = c1.Heap->Best()->coutEM;

			c2.CharJeu(DATA_PATH "bc.cg");
			c2.CharJeu(DATA_PATH "bc1.cg");
			c2.SinglEM();
			double scoreAfterTwoDsloads = c2.Heap->Best()->coutEM;
			double delta = c2.Jeu[2]->Epsilon2;
			std::cerr << "epsilon2 = " << delta << std::endl;
			TS_ASSERT_DELTA(scoreAfterTwoDsloads, referenceScore, delta);
		}
};

