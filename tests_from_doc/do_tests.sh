#!/bin/bash

export CG_TESTING=true

LS="ls -I CVS"

TEST_SECTIONS=$($LS suite)

RESULTS=test_results.txt

#SEP="~"

DO_TESTS=true

TEE=false

SECTIONS=""


# Check that Data/ exists here.
DATADIR=$(cd $(dirname $0)/../doc/user/exemple/Data && pwd)
[ -L Data ] || ln -s $DATADIR ./Data



function parse_argument() {
	if [[ "x$*" == "x" ]]; then
		return
	fi
	case $1 in
		failed)
			echo_failed
			exit
			;;
		diff)
			shift
			diff_tests $*
			exit
			;;
		-skip)
			DO_TESTS=false
			shift
			parse_argument $*
			;;
		-tee)
			TEE=true
			shift
			parse_argument $*
			;;
		-h|--help)
			echo "Usage : $0 [-tee] [-skip] [<test name>...]"
			echo "        $0 diff <test name>..."
			echo "        $0 failed"
			exit
			;;
		*)
			if [[ "x$1" != "x" ]]; then
				SECTIONS="$SECTIONS $1"
				shift
				parse_argument $*
			else
				echo "Use $0 -h or --help to know what to do."
				exit 1
			fi
			;;
	esac
}

function diff_tests() {
	if [[ $# == 0 ]]; then
		return;
	fi
	vimdiff results/$1.tmp REF/$1.tmp
	shift
	diff_tests $*
}

function clean_up() {
	TMP=/tmp/cg_test.tmp
	# Full line sed expressions (mostly //d)
	sed -e "/ *\\r$/d" \
		-e '/CarthaGene version/d' -e '/^$/d' \
		$1 -i
	# Multiline replacements
	#tr '\n' "$SEP" < $1 > $TMP.1

	# Sed expressions :
	# - remove all empty lines ending with a carriage return (NOT linefeed)
	# - replace durations by # (totaltime= 1.23)
	# - replace absolute pathes by relative pathes (always concerning Data/some_data.set)
	# - replace date in the format YYYY-MM-DD by "DATE"
	# - fix a strange bug in the output of the multigroup use case (stray \r in the middle of the 2nd filename)
	# - replace durations by # (CPU Time...: 1.23)
	# - replace durations by # in cgstat
	# - replace duration by # in mcmc output
	sed	-e "s,totaltime= [0-9.]*,totaltime= #,g" \
		-e "s,/[a-zA-Z0-9_/-]*/\(Data/[^ /]\),\1,g" \
		-e "s,[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\},DATE,g" \
		-e "s,panelR.\?.\?H2\.id,panelRH2.id,g" \
		-e "s,CPU Time (sec\(onde\)\?s): [0-9.]*,CPU Time (sec\1s): #,g" \
		-e "s,\({[0-9]\+ \)[0-9.]\{8\}\( [0-9]* [0-9.]\{8\} \)[0-9.]\{8\}\( [^{]*}\),\1#\2#\3,g" \
		-e "s,\( time elapsed:\)[0-9.]*\( sec\.\),\1#\2,g" \
        -e "/opened cache file.*/d" \
		$1 -i

		#$TMP.1 > $TMP.2
		#-e "s,\(dsload *\([^$SEP]*\)$SEP{[^\/]*\)/[^$SEP]*\2},\1\2},g" \
	#tr "$SEP" '\n' < $TMP.2 > $TMP.3
	#cp $TMP.3 $1
}


function cg_run() {
	DEBUG=false
	if $TEE; then
		if $DEBUG; then
			$1 < suite/$2.ex | tee $3
		else
			$1 < suite/$2.ex 2>/dev/null | tee $3
		fi
	else
		if $DEBUG; then
			$1 < suite/$2.ex > $3
		else
			$1 < suite/$2.ex 2>/dev/null > $3
		fi
	fi
}


function do_one_test() {
	GOOD=$(grep OK $RESULTS|wc -l)
	BAD=$(grep FAIL $RESULTS|wc -l)
	TOTAL=$(($GOOD + $BAD))
	OUTPUT=results/$1.tmp
	REFERENCE=REF/$1.tmp
	if [[ ! -f $REFERENCE ]]; then
		make_reference $1 $REFERENCE
	fi
	printf "Running test #%s/%s\t(%s good, %s bad)\r" $(($TOTAL + 1)) $COUNT $GOOD $BAD
	#echo -n "* $test..."
	mkdir -p results/$(dirname $1)
	printf "%-20.20s : " $1 >> $RESULTS
	rm -f $OUTPUT
	cg_run carthagene $1 $OUTPUT
	clean_up $OUTPUT
	(diff $OUTPUT $REFERENCE > /dev/null 2>&1 && echo "OK" || echo "FAIL") > /tmp/cg_res
	#cat /tmp/cg_res
	cat /tmp/cg_res >> $RESULTS
}

function make_reference() {
	printf "Creating reference output for test %s          \n" $1
	mkdir -p $(dirname $2)
	cg_run carthagene-1.2 $1 $2
	clean_up $2
}


function do_section() {
	SECTION=suite/$1
	TESTS=$($LS $SECTION|sed 's,\.ex,,g')
	#cat $RESULTS > .results.tmp
	echo ---in-section-$1---
	for test in $TESTS; do
		do_one_test $1/$test
	done
	printf "                                                                       \r"
}

function echo_failed() {
	echo $1$(grep FAIL $RESULTS|sed 's/ .*//')
}

parse_argument $*

if $DO_TESTS; then	

	if [[ "x$SECTIONS" != "x" ]]; then
		TEST_SECTIONS=$SECTIONS
	fi

	date > $RESULTS

	COUNT=$(cd suite&&$LS $TEST_SECTIONS|grep -v :|wc -w)
	for section in $TEST_SECTIONS; do
		do_section $section $TEST_SECTIONS
	done

	rm -f *.mcmc *.log StupidMap* *tmp*

fi

OK=$(grep OK $RESULTS|wc -l)
FAIL=$(grep FAIL $RESULTS|wc -l)
TOTAL=$(grep OK\\\|FAIL $RESULTS|wc -l)

if [[ "$TOTAL" == "0" ]]; then
	echo "Please perform some tests to view their statistics !"
	exit
fi

RATIO_PC=$(($OK * 100 / $TOTAL))
RATIO_FR=$((($OK * 10000 / $TOTAL) % 100))

echo	"==============================================="
echo	"= Result statistics                           ="
echo	"==============================================="
printf  "= Total number of tests : %19s =\n"  $TOTAL
printf  "= Passed : %34s =\n" $OK
printf  "= Failed : %34s =\n" $FAIL
printf  "= Ratio : %31s.%-2s%% =\n" $RATIO_PC $RATIO_FR
echo	"==============================================="
if [[ "$FAIL" == "0" ]]; then
	echo "All tests successful."
else
	echo_failed "Failed tests : "
fi

