
set(CG_VERSION_MAJOR 1 CACHE STRING "Major version number")
set(CG_VERSION_MINOR 3 CACHE STRING "Minor version number")
set(CG_VERSION_PATCH beta CACHE STRING "Patch number")

set(CG_VERSION "${CG_VERSION_MAJOR}.${CG_VERSION_MINOR}.${CG_VERSION_PATCH}" CACHE INTERNAL "Full version number")

set(CG_NAME "carthagene" CACHE INTERNAL "Software name")


###############################################################################
###############################################################################
###############################################################################
###############################################################################


set(CG_PACK_NAME "${CG_NAME}" CACHE INTERNAL "Software package name")
set(CG_NAME_COMPLETE "${CG_NAME}-${CG_VERSION}" CACHE INTERNAL "Complete software name")

IF(WITH_LKH)
	set(CG_NAME_COMPLETE "${CG_NAME_COMPLETE}-nonfree" CACHE INTERNAL "Complete software name")
	set(CG_PACK_NAME "${CG_PACK_NAME}-nonfree" CACHE INTERNAL "Software package name")
	set(CG_NAME "${CG_NAME}-nonfree" CACHE INTERNAL "Software name")
ENDIF(WITH_LKH)

IF(BIGMEM)
	set(CG_NAME_COMPLETE "${CG_NAME_COMPLETE}-bigmem" CACHE INTERNAL "Complete software name")
	set(CG_PACK_NAME "${CG_PACK_NAME}-bigmem" CACHE INTERNAL "Software package name")
	set(CG_NAME "${CG_NAME}-bigmem" CACHE INTERNAL "Software name")
ENDIF(BIGMEM)

set(CG_PACK_NAME ${CG_PACK_NAME}-${CG_VERSION} CACHE INTERNAL "Software package name")

IF(_32BIT)
	MESSAGE(STATUS "Setting up 32bit build")
	#IF(NOT CMAKE_C_FLAGS MATCHES "-m32")
		set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -m32" CACHE INTERNAL "C Compiler flags" FORCE)
	#ENDIF(NOT CMAKE_C_FLAGS MATCHES "-m32")
	#IF(NOT CMAKE_CXX_FLAGS MATCHES "-m32")
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32" CACHE INTERNAL "C++ Compiler flags" FORCE)
	#ENDIF(NOT CMAKE_CXX_FLAGS MATCHES "-m32")
    set(CMAKE_C_FLAGS_${CMAKE_BUILD_TYPE} "${CMAKE_C_FLAGS_${CMAKE_BUILD_TYPE}} -m32" CACHE INTERNAL "" FORCE)
    set(CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE} "${CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE}} -m32" CACHE INTERNAL "" FORCE)
	set(CMAKE_SYSTEM_PROCESSOR "i386" CACHE INTERNAL "System processor type")
	SET(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "i386" CACHE INTERNAL "Architecture for debian package")
	SET(CPACK_RPM_PACKAGE_ARCHITECTURE "i586" CACHE INTERNAL "Architecture for RPM package")
ENDIF(_32BIT)


