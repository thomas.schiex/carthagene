

OPTION(TEST "Build test binary, and only that (THIS IS NOT THE UNIT-TESTS-ENABLING OPTION)" OFF)
OPTION(WITH_LKH "Compile and link against LKH code (see licence agreement)" OFF)
OPTION(WITH_FRAMEWORK "Compile framework binaries (requires boost-graph)" ON)
OPTION(GENERATE_DOC "Generate the full user documentation" OFF)
OPTION(BIGMEM "Allow for much bigger datasets (and much bigger memory overload)" ON)
OPTION(PROFILE "Compile a big binary for profiling" OFF)
OPTION(WITH_PARALLEL "Compile parallelized version using Intel Thread Building Blocs (requires libtbb2 or libtbb3 installed)" OFF)
OPTION(WITH_PYTHON "Build Python extension (experimental)" OFF)
OPTION(WITH_TESTS "Build unit tests" ON)
OPTION(WORKAROUND_CACHE_FILE "Use own implementation of pagefile even on systems where boost::mapped_file works well (linux64, win32)" OFF)

IF(${CMAKE_SYSTEM_PROCESSOR} MATCHES ".*64")
	OPTION(_32BIT "Compile 32-bit binaries and libs" OFF)
ELSE(${CMAKE_SYSTEM_PROCESSOR} MATCHES ".*64")
	SET(_32BIT ON CACHE STRING "Compile 32-bit binaries and libs")
ENDIF(${CMAKE_SYSTEM_PROCESSOR} MATCHES ".*64")

IF(NOT WIN32)
    #OPTION(_EMBED_LIBS
    #	"Embed libc, libstdc++ and libm for increased compatibility" OFF)
ELSE(NOT WIN32)
    SET(GLOBAL_DEFS "${GLOBAL_DEFS} -D_CRT_SECURE_NO_WARNINGS")
ENDIF(NOT WIN32)


