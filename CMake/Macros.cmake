MACRO(cg_conf_file srcfile destdir)
	SET(tmp ${CMAKE_INSTALL_PREFIX})
	SET(CMAKE_INSTALL_PREFIX ${CMAKE_BINARY_DIR})
	GET_FILENAME_COMPONENT(e ${srcfile} EXT)
	#MESSAGE(STATUS "DEBUG cg_conf_file got ext ${e}")
	GET_FILENAME_COMPONENT(ntmp ${srcfile} NAME)
	IF(e MATCHES ".*\\.in")
		STRING(REPLACE ".in" "" n ${ntmp})
	ELSE(e MATCHES ".*\\.in")
		SET(n ${ntmp})
	ENDIF(e MATCHES ".*\\.in")
	CONFIGURE_FILE(${srcfile} ${CMAKE_BINARY_DIR}/${destdir}/${n})
	SET(CMAKE_INSTALL_PREFIX ${tmp})
ENDMACRO(cg_conf_file)

MACRO(precompile_header main_target pch_tgt header)
    add_custom_target(${pch_tgt} ALL DEPENDS ${CMAKE_BINARY_DIR}/CMakeCache.txt)
    get_directory_property(inc INCLUDE_DIRECTORIES)
    SET(flags)
    FOREACH(d ${inc})
        set(flags ${flags} -I${d})
    ENDFOREACH(d)
    GET_TARGET_PROPERTY(tmp ${main_target} COMPILE_FLAGS)
    IF(NOT tmp MATCHES NOTFOUND)
        STRING(REPLACE " " ";" tmp "${tmp}")
        LIST(APPEND flags ${tmp})
    ENDIF(NOT tmp MATCHES NOTFOUND)
    GET_TARGET_PROPERTY(tmp ${main_target} COMPILE_DEFINITIONS)
    IF(NOT tmp MATCHES NOTFOUND)
        STRING(REPLACE " " ";" tmp "${tmp}")
        LIST(APPEND flags ${tmp})
    ENDIF(NOT tmp MATCHES NOTFOUND)
    GET_TARGET_PROPERTY(tmp ${main_target} COMPILE_FLAGS_${CMAKE_BUILD_TYPE})
    IF(NOT tmp MATCHES NOTFOUND)
        STRING(REPLACE " " ";" tmp "${tmp}")
        LIST(APPEND flags ${tmp})
    ENDIF(NOT tmp MATCHES NOTFOUND)
    GET_TARGET_PROPERTY(tmp ${main_target} COMPILE_DEFINITIONS_${CMAKE_BUILD_TYPE})
    IF(NOT tmp MATCHES NOTFOUND)
        STRING(REPLACE " " ";" tmp "${tmp}")
        LIST(APPEND flags ${tmp})
    ENDIF(NOT tmp MATCHES NOTFOUND)
    #STRING(REPLACE " " ";" tmp "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE}}")
    #MESSAGE(STATUS "Current cxx_flags : ${tmp}")
    #LIST(APPEND flags ${tmp})
    MESSAGE(STATUS "flags = ${flags}")
    STRING(TOUPPER "${CMAKE_BUILD_TYPE}" conf)
    #FOREACH(header ${ARGN})
        get_filename_component(hbase ${header} NAME_WE)
		get_filename_component(hname ${header} NAME)
		get_filename_component(dirname ${header} PATH)
		set(output ${CMAKE_BINARY_DIR}/${hname}.gch)
        #ADD_PRECOMPILED_HEADER(${pch_tgt} ${header})
        MESSAGE(STATUS "Header ${header} marked for precompilation")
        SET(tmp ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${conf}} ${flags} ${header} -o ${output})
        STRING(REPLACE " " ";" cmdline "${tmp}")
		set(${hdr_var} ${${hdr_var}} ${output})
        message("PRECOMP ${cmdline}")
		ADD_CUSTOM_COMMAND(
			OUTPUT ${output}
			COMMAND ${CMAKE_CXX_COMPILER}
            ARGS ${cmdline}
			DEPENDS ${header}
			#MAIN_DEPENDENCY ${header}
			IMPLICIT_DEPENDS CXX ${header}
			#WORKING_DIRECTORY ${dirname}
			COMMENT "Precompiling header ${header}"
			#COMMENT ""
			#VERBATIM
		)
        ADD_CUSTOM_TARGET(PCH_${hbase} DEPENDS ${output})
		#ADD_CUSTOM_TARGET(pch_${hname} DEPENDS ${output})
        add_dependencies(${pch_tgt} PCH_${hbase})
		SET_SOURCE_FILES_PROPERTIES(${output} PROPERTIES GENERATED TRUE)
        SET_SOURCE_FILES_PROPERTIES(${header} PROPERTIES OBJECT_DEPENDS ${output})
        execute_process(COMMAND touch ${CMAKE_BINARY_DIR}/${hname})
		#SET(HEADER_TO_PRECOMPILE ${HEADER_TO_PRECOMPILE} pch_${hname})
        #ENDFOREACH(header)
    #ADD_CUSTOM_TARGET(PCH_cleanup ALL COMMAND rm -f ${${hdr_var}} DEPENDS ${CMAKE_BINARY_DIR}/CMakeCache.txt COMMENT "Cleaning precompiled headers" DEPENDS ${CMAKE_BINARY_DIR}/CMakeCache.txt)
    #ADD_CUSTOM_TARGET(PCH ALL DEPENDS ${${hdr_var}})
    GET_TARGET_PROPERTY(tmp ${main_target} COMPILE_FLAGS)
    SET_TARGET_PROPERTIES(${main_target} PROPERTIES COMPILE_FLAGS "-include ${hname} ${tmp}")
ENDMACRO(precompile_header)


