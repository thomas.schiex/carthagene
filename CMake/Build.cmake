
# Generate build number

execute_process(
    COMMAND bash -c "cd ${PROJECT_SOURCE_DIR} && git log | grep -c 'commit '"
    OUTPUT_VARIABLE BUILD_NUMBER
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )

# How many dirty files in the current build ?
execute_process(
    COMMAND bash -c "cd ${PROJECT_SOURCE_DIR} && git status -s -uno | wc -l"
    OUTPUT_VARIABLE DIRTY_FILES
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )


INCLUDE_DIRECTORIES(${TCL_INCLUDE_PATH})

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-deprecated")


set(calcul_src
	${PROJECT_SOURCE_DIR}/calcul/CartaGene.cc
	${PROJECT_SOURCE_DIR}/calcul/BioJeu.cc
	${PROJECT_SOURCE_DIR}/calcul/read_data.cc
	${PROJECT_SOURCE_DIR}/calcul/BioJeuSingle.cc
	${PROJECT_SOURCE_DIR}/calcul/BioJeuMerged.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_CO.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_RH.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_RHD.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_RHE.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_IC.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_IC_parallel.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_BC.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_OR.cc
	${PROJECT_SOURCE_DIR}/calcul/Genetic.cc
	${PROJECT_SOURCE_DIR}/calcul/System.cc
	${PROJECT_SOURCE_DIR}/calcul/Carte.cc
	${PROJECT_SOURCE_DIR}/calcul/BJM_GE.cc
	${PROJECT_SOURCE_DIR}/calcul/Tas.cc
	${PROJECT_SOURCE_DIR}/calcul/BJM_OR.cc
	${PROJECT_SOURCE_DIR}/calcul/Annealing.cc
	${PROJECT_SOURCE_DIR}/calcul/Constraints.cc
	${PROJECT_SOURCE_DIR}/calcul/Greedy.cc
	${PROJECT_SOURCE_DIR}/calcul/Build.cc
	${PROJECT_SOURCE_DIR}/calcul/BuildFW.cc
	${PROJECT_SOURCE_DIR}/calcul/AlgoGen.cc
	${PROJECT_SOURCE_DIR}/calcul/tsp.cc
	${PROJECT_SOURCE_DIR}/calcul/QPolynomial.cc
	${PROJECT_SOURCE_DIR}/calcul/QPolynomialMatrix.cc
	${PROJECT_SOURCE_DIR}/calcul/QMatingOperator.cc
	${PROJECT_SOURCE_DIR}/calcul/BJS_BS.cc
	${PROJECT_SOURCE_DIR}/calcul/NOrCompMulti.cc
	${PROJECT_SOURCE_DIR}/calcul/mcmc.cc
	${PROJECT_SOURCE_DIR}/calcul/packedarray.c
	# 2pt matrices
	${PROJECT_SOURCE_DIR}/calcul/twopoint.cc
	)


IF(WIN32)
	SET(calcul_src ${calcul_src} ${PROJECT_SOURCE_DIR}/calcul/drand48.c ${PROJECT_SOURCE_DIR}/matrix/mmap_win32.c)
ENDIF(WIN32)


IF(WITH_LKH)
	INCLUDE(LKH_list.cmake)
	#INCLUDE(LKH-2_list.cmake)	# NOT (YET ?) WORKING.

	IF(UNIX AND NOT ${CMAKE_BUILD_TYPE} MATCHES "Debug")
		if(PROFILE)
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive -O2 -funroll-loops")
		ELSE(PROFILE)
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive -O2 -funroll-loops -fomit-frame-pointer")
		ENDIF(PROFILE)
	ENDIF(UNIX AND NOT ${CMAKE_BUILD_TYPE} MATCHES "Debug")
ENDIF(WITH_LKH)

IF(WITH_PARALLEL)
	IF(WIN32)
		FIND_LIBRARY(PARALLEL_LIBRARIES tbb ONLY_CMAKE_FIND_ROOT_PATH)
	ELSE(WIN32)
		FIND_LIBRARY(PARALLEL_LIBRARIES tbb)
	ENDIF(WIN32)
	IF(PARALLEL_LIBRARIES MATCHES "^$|.*-NOTFOUND")
		IF(WIN32)
			MESSAGE(STATUS "Couldn't automatically find libtbb ! assuming $ENV{HOME}/win32/lib/tbb.dll")
			SET(PARALLEL_LIBRARIES "$ENV{HOME}/win32/lib/tbb.dll")
		ELSE(WIN32)
			MESSAGE(STATUS "Couldn't automatically find libtbb ! assuming -ltbb")
			SET(PARALLEL_LIBRARIES "-ltbb")
		ENDIF(WIN32)
	ENDIF(PARALLEL_LIBRARIES MATCHES "^$|.*-NOTFOUND")
    # special quirky dependency for my home-made libtbb.dll
    FIND_LIBRARY(tmp1 stdc++-6 ONLY_CMAKE_FIND_ROOT_PATH)
    IF(tmp1 MATCHES "NOTFOUND")
        SET(tmp1 )
    ENDIF(tmp1 MATCHES "NOTFOUND")
    FIND_LIBRARY(tmp2 gcc_s_dw2-1 ONLY_CMAKE_FIND_ROOT_PATH)
    IF(tmp2 MATCHES "NOTFOUND")
        SET(tmp2 )
    ENDIF(tmp2 MATCHES "NOTFOUND")
    SET(TBB_QUIRK ${tmp1} ${tmp2})
	IF(WIN32)
		SET(CMAKE_CXX_FLAGS "-mthreads")
		SET(CMAKE_C_FLAGS "-mthreads")
	ELSE(WIN32)
		SET(CMAKE_CXX_FLAGS "-pthread")
		SET(CMAKE_C_FLAGS "-pthread")
	ENDIF(WIN32)
    IF(WIN32)
        SET(CMAKE_EXE_LINKER_FLAGS "--enable-auto-import -mthreads")
    ELSE(WIN32)
        SET(CMAKE_EXE_LINKER_FLAGS "-pthread")
    ENDIF(WIN32)
	MESSAGE(STATUS "PARALLEL_LIBRARIES =  ${PARALLEL_LIBRARIES}")
ELSE(WITH_PARALLEL)
	SET(PARALLEL_LIBRARIES "" CACHE STRING "pouet" FORCE)
ENDIF(WITH_PARALLEL)

SET(SWIG_FLAGS -tcl8 -c++ -namespace -prefix CG)
SET(SWIG_OUTPUT ${PROJECT_SOURCE_DIR}/cgsh/cgshTCL_wrap.cxx) 

add_custom_command(
	OUTPUT ${SWIG_OUTPUT}
	COMMAND ${SWIG_EXECUTABLE}
	ARGS ${SWIG_FLAGS} -o ${SWIG_OUTPUT} cgsh.i
	MAIN_DEPENDENCY ${PROJECT_SOURCE_DIR}/cgsh/cgsh.i
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/cgsh
	COMMENT "Invoke SWIG to generate TCL wrapper for CarthaGene"
	VERBATIM
	)

SET_SOURCE_FILES_PROPERTIES(${SWIG_OUTPUT} PROPERTIES GENERATED TRUE)
#SET_SOURCE_FILES_PROPERTIES(${SWIG_OUTPUT} PROPERTIES GENERATED TRUE COMPILE_FLAGS "-Wno-uninitialized")
#SET_SOURCE_FILES_PROPERTIES(
#		#${SWIG_OUTPUT}
#		calcul/neotest.cc
#		${calcul_src}
#		#${swig_generated_source}
#	PROPERTIES
#		OBJECT_DEPENDS "${Precompiled_Headers}"
#)



IF(CMAKE_BUILD_TYPE MATCHES "Rel.*")
    #add_definitions(-DNDEBUG)
    SET(GLOBAL_DEFS "${GLOBAL_DEFS} -DNDEBUG")
	#message(" * * * * * NDEBUG !! * * * * *")
ENDIF(CMAKE_BUILD_TYPE MATCHES "Rel.*")


IF(WITH_FRAMEWORK AND NOT TEST)
	find_package(Boost 1.40.0)

    #IF(NOT WIN32)
		include_directories(${Boost_INCLUDE_DIRS})
    #ENDIF(NOT WIN32)

	add_executable(framework ${PROJECT_SOURCE_DIR}/cgscript/framework.cpp)
	add_executable(framework_mst ${PROJECT_SOURCE_DIR}/cgscript/framework_mst.cpp)

	IF(WIN32)
        #set_target_properties(framework PROPERTIES
        #	COMPILE_FLAGS "-Wall -D${HOST_ARCH} -I \"C:\\\\Program Files\\\\boost\\\\boost_1_40\""
        #	LINKER_FLAGS "C:\\\\Program Files\\\\boost\\\\boost_1_40\\\\lib\\\\libboost_graph-vc90-mt-s-1_40.lib"
        #	)
        #set_target_properties(framework_mst PROPERTIES
        #	COMPILE_FLAGS "-Wall -D${HOST_ARCH} -I \"C:\\\\Program Files\\\\boost\\\\boost_1_40\""
        #	LINKER_FLAGS "C:\\\\Program Files\\\\boost\\\\boost_1_40\\\\lib\\\\libboost_graph-vc90-mt-s-1_40.lib"
        #	)
	ELSE(WIN32)
		set_target_properties(framework PROPERTIES
			COMPILE_FLAGS "-Wall -D${HOST_ARCH} ${GLOBAL_DEFS}"
			RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/share/carthagene/cgscript
			)
		set_target_properties(framework_mst PROPERTIES
			COMPILE_FLAGS "-Wall -D${HOST_ARCH} ${GLOBAL_DEFS}"
			RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/share/carthagene/cgscript
			)
	ENDIF(WIN32)
ENDIF(WITH_FRAMEWORK AND NOT TEST)

IF(NOT TEST)
    IF(WIN32)
        add_executable(tk ${PROJECT_SOURCE_DIR}/tkAppInit.cc)
        target_link_libraries(tk ${CG_LIB_TCL} ${PARALLEL_LIBRARIES} ${Boost_LIBRARIES})
    ENDIF(WIN32)
	add_library(cartagene SHARED
		${SWIG_OUTPUT}
		${calcul_src}
		${PROJECT_SOURCE_DIR}/cgsh/cgsh.cc
		${swig_generated_sources})
	set_target_properties(cartagene PROPERTIES
		COMPILE_FLAGS "-Wall ${CG_DEF_TCLSTUB} -Dtcl -Dihm -D${HOST_ARCH} -Wno-deprecated ${GLOBAL_DEFS} -fPIC -Winvalid-pch"
		LINK_FLAGS "${STATIC_LIBS}"
		LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/share/carthagene/cgsh"
		)
	target_link_libraries(cartagene
		${GMP_LIBRARIES}
		${CG_LIB_TCL}
		${PARALLEL_LIBRARIES}
		${Boost_LIBRARIES}
        ${BOOST_IOSTREAMS_LIB}
		)
	add_dependencies(cartagene PCH)
ELSE(NOT TEST)
	add_executable(cartagene_test
		${PROJECT_SOURCE_DIR}/calcul/neotest.cc
		${calcul_src}
		)
	set_target_properties(cartagene_test PROPERTIES
		COMPILE_FLAGS "-Wall -D${HOST_ARCH} ${GLOBAL_DEFS}"
		LINK_FLAGS "${STATIC_LIBS}"
		)
	add_dependencies(cartagene_test PCH)
	target_link_libraries(cartagene_test ${PARALLEL_LIBRARIES} ${Boost_LIBRARIES})
ENDIF(NOT TEST)


IF(PROFILE)
	add_executable(cartagene_profiled
		${PROJECT_SOURCE_DIR}/calcul/neotest.cc
		${calcul_src}
		)
	set_target_properties(cartagene_profiled PROPERTIES
		COMPILE_FLAGS "-pg -Wall -D${HOST_ARCH} ${GLOBAL_DEFS}"
		LINK_FLAGS "-pg ${STATIC_LIBS}"
		)
	add_dependencies(cartagene_profiled PCH)
	target_link_libraries(cartagene_profiled ${PARALLEL_LIBRARIES} ${Boost_LIBRARIES})
ENDIF(PROFILE)

IF(WITH_TESTS)
	IF(WIN32)
		ADD_CUSTOM_TARGET(ut ALL
			COMMAND ./unit_tests/cg_tests.exe 2>/dev/null
			WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
			DEPENDS cg_tests)
	ELSE(WIN32)
		ADD_CUSTOM_TARGET(ut ALL
			COMMAND ./unit_tests/cg_tests 2>/dev/null
			WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
			DEPENDS cg_tests)
	ENDIF(WIN32)
ENDIF(WITH_TESTS)

precompile_header(cartagene Precompiled_Headers
	${PROJECT_SOURCE_DIR}/carthagene_all.h
)


