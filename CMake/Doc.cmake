IF(WIN32)
	# We can't possibly compile that teX document and the examples under zindozs.
	# expect doc to be generated and found in doc/user/User.pdf
	MESSAGE(STATUS "DEBUG doc import ${CMAKE_SOURCE_DIR}/Doc-${CG_NAME}.pdf => ${CMAKE_BINARY_DIR}/doc/CarthaGeneUserManual.pdf")
	CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/Doc-${CG_NAME}.pdf ${CMAKE_BINARY_DIR}/doc/CarthaGeneUserManual.pdf COPYONLY)
	INSTALL(FILES ${CMAKE_BINARY_DIR}/doc/CarthaGeneUserManual.pdf DESTINATION doc)
ELSE(WIN32)
	IF(_32BIT)
		CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/Doc-${CG_NAME}.pdf ${CMAKE_BINARY_DIR}/doc/CarthaGeneUserManual.pdf COPYONLY)
		INSTALL(FILES ${CMAKE_BINARY_DIR}/doc/user/User.pdf ${CMAKE_BINARY_DIR}/doc/user/User.ps.gz DESTINATION share/doc/carthagene)
	ELSE(_32BIT)
		INCLUDE(FindLATEX)
		ADD_SUBDIRECTORY(doc)
		INSTALL(FILES ${CMAKE_BINARY_DIR}/doc/user/User.pdf ${CMAKE_BINARY_DIR}/doc/user/User.ps.gz DESTINATION share/doc/carthagene)
	ENDIF(_32BIT)
ENDIF(WIN32)

