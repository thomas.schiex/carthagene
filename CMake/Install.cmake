
IF(UNIX AND NOT TEST)
	#IF(_FRONTEND)
	#	INSTALL(PROGRAMS ${PROJECT_SOURCE_DIR}/carthagene DESTINATION bin)
	#	#INSTALL(TARGETS CG DESTINATION share/carthagene)
	#ENDIF(_FRONTEND)

	FOREACH(f ${tcl_cgsh} ${tcl_cgscript} ${tcl_cgscript_ex} ${tcl_cgwin} ${tcl_cgwin_img} ${PROJECT_SOURCE_DIR}/dotfiles/sample_cgwopt ${PROJECT_SOURCE_DIR}/dotfiles/sample_cgwdefalgo)
		STRING(REPLACE "${PROJECT_SOURCE_DIR}" "${CMAKE_BINARY_DIR}/share/carthagene" tmp "${f}")
		STRING(REPLACE "cgwin/" "gui/" dest "${tmp}")
		configure_file("${f}" "${dest}" COPYONLY)
	ENDFOREACH(f)

	SET(ENV{PATH} "${CMAKE_BINARY_DIR}/bin:$ENV{PATH}")

	cg_conf_file("${CMAKE_SOURCE_DIR}/cgsh/cgbootstrap.tcl.in" share/carthagene/cgsh)
	cg_conf_file("${CMAKE_SOURCE_DIR}/carthagene.in" bin)
	cg_conf_file("${CMAKE_SOURCE_DIR}/doc/user/exemple/ex2tmp.tcl.in" doc/user/exemple)
	CONFIGURE_FILE("${CMAKE_SOURCE_DIR}/doc/user/exemple/tmp2tex.tcl" doc/user/exemple/tmp2tex.tcl COPYONLY)
	cg_conf_file("${CMAKE_SOURCE_DIR}/cgsh/pkgIndex.tcl.in" share/carthagene/cgsh)
	cg_conf_file("${CMAKE_SOURCE_DIR}/cgscript/pkgIndex.tcl.in" share/carthagene/cgscript)
	cg_conf_file("${CMAKE_SOURCE_DIR}/cgscript/init.tcl.in" share/carthagene/cgscript)
	cg_conf_file("${CMAKE_SOURCE_DIR}/cgwin/pkgIndex.tcl.in" share/carthagene/gui)
	cg_conf_file("${CMAKE_SOURCE_DIR}/cgwin/init.tcl.in" share/carthagene/gui)

	INSTALL(TARGETS cartagene DESTINATION share/carthagene/cgsh)

	INSTALL(PROGRAMS ${CMAKE_BINARY_DIR}/inst/carthagene DESTINATION bin)

	IF(WITH_FRAMEWORK)
		INSTALL(TARGETS framework framework_mst DESTINATION share/carthagene/cgscript)
	ENDIF(WITH_FRAMEWORK)
	INSTALL(FILES ${tcl_cgsh} DESTINATION share/carthagene/cgsh)
	INSTALL(FILES ${tcl_cgscript} DESTINATION share/carthagene/cgscript)
	INSTALL(PROGRAMS ${tcl_cgscript_ex} DESTINATION share/carthagene/cgscript)
	INSTALL(FILES ${tcl_cgwin} DESTINATION share/carthagene/gui)
	INSTALL(FILES ${tcl_cgwin_img} DESTINATION share/carthagene/gui/images)
	INSTALL(FILES	"${CMAKE_BINARY_DIR}/dotfiles/sample_carthagenerc"
		dotfiles/sample_cgwopt
		dotfiles/sample_cgwdefalgo
		DESTINATION share/carthagene)
ENDIF(UNIX AND NOT TEST)

#IF(TEST)
#	ADD_EXECUTABLE(neotest calcul/neotest.cc ${calcul_src})
#	add_definitions(-D${HOST_ARCH})
#	IF(WITH_LKH)
#		add_definitions(-DLKHLICENCE)
#	ENDIF(WITH_LKH)
#	IF(WIN32)
#		set(CMAKE_BUILD_TYPE RelWithDebInfo)
#	ENDIF(WIN32)
#ENDIF(TEST)


IF(WIN32 AND NOT TEST)
	INSTALL(FILES ${PROJECT_SOURCE_DIR}/carthagene.bat ${PROJECT_SOURCE_DIR}/carthagene-console.bat DESTINATION bin)
    INSTALL(TARGETS cartagene DESTINATION tcl/cgsh)
	INSTALL(TARGETS tk DESTINATION tcl)
	IF(WITH_FRAMEWORK)
		INSTALL(TARGETS framework framework_mst DESTINATION tcl/cgscript)
	ENDIF(WITH_FRAMEWORK)
	INSTALL(FILES ${tcl_cgsh} DESTINATION tcl/cgsh)
	INSTALL(FILES ${PARALLEL_LIBRARIES} ${TBB_QUIRK} ${BOOST_IOSTREAMS_DLL} DESTINATION tcl)
	INSTALL(FILES ${tcl_cgscript} DESTINATION tcl/cgscript)
	INSTALL(FILES ${tcl_cgwin} DESTINATION tcl/gui)
	INSTALL(FILES ${tcl_cgwin_img} DESTINATION tcl/gui/images)
	INSTALL(FILES cg.ico
		${CMAKE_BINARY_DIR}\\\\dotfiles\\\\sample_carthagenerc
		dotfiles\\\\sample_cgwopt
		dotfiles\\\\sample_cgwdefalgo
		DESTINATION bin)

	SET(CMAKE_INSTALL_MFC_LIBRARIES ON)

	SET(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS_SKIP ON)
	INCLUDE(InstallRequiredSystemLibraries)
	INSTALL(FILES ${CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS} DESTINATION tcl/cgsh)

	## ADD-INS :

	# - GNU sed & awk
	FILE(GLOB gnupack_dirs ${PROJECT_SOURCE_DIR}/distrib/gnupack/*/)
	INSTALL(DIRECTORY ${gnupack_dirs} DESTINATION ".")

	# - concorde + cygwin1.dll
	# FIXME : we get a SIGABRT when invoking concorde.exe. Cause unknown.
	#INSTALL(FILES	${PROJECT_SOURCE_DIR}/distrib/concordepack/concorde.exe
	#		${PROJECT_SOURCE_DIR}/distrib/concordepack/cygwin1.dll
	#	DESTINATION bin)

ENDIF(WIN32 AND NOT TEST)


## Packaging

IF(NOT TEST)
	#MESSAGE(STATUS "debug CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}")
	INCLUDE(CMakeCPack.cmake)
ENDIF(NOT TEST)


