########################################################################################
# Files to generate from templates
# Mostly for replacing ${CG_VERSION_MAJOR} and ${CG_VERSION_MINOR} in files.
#

configure_file(	"${CMAKE_SOURCE_DIR}/cgsh/cgbootstrap.tcl.in"
		"${CMAKE_BINARY_DIR}/inst/cgsh/cgbootstrap.tcl")

configure_file(	"${CMAKE_SOURCE_DIR}/carthagene.in"
		"${CMAKE_BINARY_DIR}/inst/carthagene")

configure_file(	"${CMAKE_SOURCE_DIR}/config.h.in"
		"${CMAKE_SOURCE_DIR}/config.h")

configure_file(	"${CMAKE_SOURCE_DIR}/cgsh/pkgIndex.tcl.in"
		"${CMAKE_BINARY_DIR}/inst/cgsh/pkgIndex.tcl")

configure_file(	"${CMAKE_SOURCE_DIR}/cgscript/pkgIndex.tcl.in"
		"${CMAKE_BINARY_DIR}/inst/cgscript/pkgIndex.tcl")

configure_file(	"${CMAKE_SOURCE_DIR}/cgscript/init.tcl.in"
		"${CMAKE_BINARY_DIR}/inst/cgscript/init.tcl")

configure_file(	"${CMAKE_SOURCE_DIR}/cgwin/pkgIndex.tcl.in"
		"${CMAKE_BINARY_DIR}/inst/cgwin/pkgIndex.tcl")

configure_file(	"${CMAKE_SOURCE_DIR}/cgwin/init.tcl.in"
		"${CMAKE_BINARY_DIR}/inst/cgwin/init.tcl")

configure_file(	"${CMAKE_SOURCE_DIR}/dotfiles/sample_carthagenerc.in"
		"${CMAKE_BINARY_DIR}/dotfiles/sample_carthagenerc")



add_executable(${PROJECT_SOURCE_DIR}/cgscript/map.plot IMPORTED)
add_executable(${PROJECT_SOURCE_DIR}/cgscript/comparativemapping.sh IMPORTED)
add_executable(${PROJECT_SOURCE_DIR}/cgscript/rhbplin.sh IMPORTED)
add_executable(${PROJECT_SOURCE_DIR}/cgscript/rhbp.sh IMPORTED)
add_executable(${PROJECT_SOURCE_DIR}/cgscript/rh.sh IMPORTED)

FILE(GLOB tcl_cgsh
	${PROJECT_SOURCE_DIR}/cgsh/*.tcl
	${CMAKE_BINARY_DIR}/inst/cgsh/cgbootstrap.tcl
	${CMAKE_BINARY_DIR}/inst/cgsh/pkgIndex.tcl
	)
SET(tcl_cgscript_ex
	${PROJECT_SOURCE_DIR}/cgscript/map.plot
	${PROJECT_SOURCE_DIR}/cgscript/pareto.plot
	${PROJECT_SOURCE_DIR}/cgscript/comparativemapping.sh
	${PROJECT_SOURCE_DIR}/cgscript/rh.sh
	${PROJECT_SOURCE_DIR}/cgscript/rhbp.sh
	${PROJECT_SOURCE_DIR}/cgscript/rhbplin.sh
	)
FILE(GLOB tcl_cgscript
	${PROJECT_SOURCE_DIR}/cgscript/*.tcl
	${PROJECT_SOURCE_DIR}/cgscript/tclIndex
	${PROJECT_SOURCE_DIR}/cgscript/*.awk
	${PROJECT_SOURCE_DIR}/cgscript/*.sed
	${CMAKE_BINARY_DIR}/inst/cgscript/pkgIndex.tcl
	${CMAKE_BINARY_DIR}/inst/cgscript/init.tcl
	)
FILE(GLOB tcl_cgwin ${PROJECT_SOURCE_DIR}/cgwin/*.tcl ${PROJECT_SOURCE_DIR}/cgwin/tclIndex ${CMAKE_BINARY_DIR}/inst/cgwin/pkgIndex.tcl ${PROJECT_BINARY_DIR}/inst/cgwin/init.tcl)
FILE(GLOB tcl_cgwin_img ${PROJECT_SOURCE_DIR}/cgwin/images/*.gif)


