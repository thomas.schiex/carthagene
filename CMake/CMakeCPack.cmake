IF(UNIX AND NOT WIN32)
	SET(CPACK_GENERATOR "DEB;RPM")
	#SET(CPACK_GENERATOR "DEB")
ELSE(UNIX AND NOT WIN32)	
	SET(CPACK_GENERATOR "NSIS")
ENDIF(UNIX AND NOT WIN32)

SET(CPACK_SOURCE_IGNORE_FILES ".*\\\\.mcmc" "/cgperl/" "/build.*/" ".*\\\\.swp$" ".*\\\\.swo$" "~$" "/distrib/" "^CVS$" "/my-code/" "/release/" "/tcl/" "/web/" "^\\\\.cvsignore$" "/interfaces/" "/CMake/" "/.*/CVS/")

SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "CarthaGene is a multipopulation integrated genetic and radiated hybrid mapping tool")
SET(CPACK_PACKAGE_VENDOR "CarthaGene development team")
SET(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_SOURCE_DIR}/README")
SET(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
SET(CPACK_PACKAGE_VERSION_MAJOR ${CG_VERSION_MAJOR})
SET(CPACK_PACKAGE_VERSION_MINOR ${CG_VERSION_MINOR})
IF(WITH_LKH)
	SET(CPACK_PACKAGE_VERSION_PATCH "${CG_VERSION_PATCH}R")
ELSE(WITH_LKH)
	SET(CPACK_PACKAGE_VERSION_PATCH "${CG_VERSION_PATCH}")
ENDIF(WITH_LKH)

SET(CPACK_DEBIAN_PACKAGE_DEPENDS "libc6 (>= 2.4), tcl8.4 | tcl8.5, tk8.4 | tk8.5, tclreadline, bwidget, iwidgets4, tcllib, tklib, blt, itcl3 | itcl3.1, itk3 | itk3.1, libtbb2, libboost-graph1.40.0")
#SET(CPACK_RPM_PACKAGE_REQUIRES "glibc, tcl, tk, tclreadline, bwidget, iwidgets, tcllib, tklib, blt, itcl, itk")

#SET(CPACK_PACKAGE_INSTALL_DIRECTORY "carthagene-${CG_VERSION_MAJOR}.${CG_VERSION_MINOR}")

SET(CPACK_PACKAGE_CONTACT "Thomas Schiex <thomas.schiex@toulouse.inra.fr>")
SET(CPACK_PACKAGE_MAINTAINER "Damien Leroux <damien.leroux@toulouse.inra.fr>")

IF(WIN32 AND NOT UNIX)
	# There is a bug in NSI that does not handle full unix paths properly. Make
	# sure there is at least one set of four (4) backlasshes.
	SET(CPACK_PACKAGE_ICON "${PROJECT_SOURCE_DIR}\\\\cg.ico")
	SET(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\cg.ico")
	SET(CPACK_NSIS_DISPLAY_NAME "${CPACK_PACKAGE_INSTALL_DIRECTORY} ${CG_NAME_COMPLETE}")
	SET(CPACK_NSIS_HELP_LINK "https://mulcyber.toulouse.inra.fr/docman/?group_id=5")
	SET(CPACK_NSIS_URL_INFO_ABOUT "https://mulcyber.toulouse.inra.fr/projects/carthagene/")
	SET(CPACK_NSIS_CONTACT "Thomas Schiex <thomas.schiex@toulouse.inra.fr>")
	SET(CPACK_NSIS_MODIFY_PATH ON)
	#SET(CPACK_PACKAGE_EXECUTABLES "carthagene.bat" "bin\\\\cg.ico")
	SET(CPACK_NSIS_EXTRA_INSTALL_COMMANDS "
		CreateShortCut \\\"$SMPROGRAMS\\\\$STARTMENU_FOLDER\\\\CarthaGene.lnk\\\" \\\"$INSTDIR\\\\bin\\\\carthagene.bat\\\" \\\"\\\" \\\"$INSTDIR\\\\bin\\\\cg.ico\\\" \\\"\\\" SW_SHOWMINIMIZED
		CreateShortCut \\\"$SMPROGRAMS\\\\$STARTMENU_FOLDER\\\\CarthaGene Console.lnk\\\" \\\"$INSTDIR\\\\bin\\\\carthagene-console.bat\\\" \\\"\\\" \\\"$INSTDIR\\\\bin\\\\cg.ico\\\" \\\"\\\" SW_SHOWMINIMIZED
		CreateShortCut \\\"$SMPROGRAMS\\\\$STARTMENU_FOLDER\\\\Documentation.lnk\\\" \\\"$INSTDIR\\\\doc\\\\CarthaGeneUserManual.pdf\\\" \\\"\\\" \\\"$INSTDIR\\\\bin\\\\cg.ico\\\" \\\"\\\" SW_SHOWMINIMIZED
	")
	SET(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS "
		!insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
		Delete \\\"$SMPROGRAMS\\\\$MUI_TEMP\\\\CarthaGene.lnk\\\"
		!insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
		Delete \\\"$SMPROGRAMS\\\\$MUI_TEMP\\\\CarthaGene Console.lnk\\\"
		!insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
		Delete \\\"$SMPROGRAMS\\\\$MUI_TEMP\\\\Documentation CarthaGene.lnk\\\"
	")
	SET(CPACK_PACKAGE_FILE_NAME "${CG_PACK_NAME}-${CMAKE_SYSTEM_NAME}")
ELSE(WIN32 AND NOT UNIX)
	#SET(CPACK_STRIP_FILES "CG")
	#SET(CPACK_SOURCE_STRIP_FILES "")
	SET(CPACK_PACKAGE_FILE_NAME "${CG_PACK_NAME}-${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}")
ENDIF(WIN32 AND NOT UNIX)

#MESSAGE(STATUS =====================================================================)
#MESSAGE(STATUS CPACK_GENERATOR \" \" ${CPACK_GENERATOR})
#MESSAGE(STATUS =====================================================================)
#IF(CPACK_GENERATOR MATCHES \"STGZ\")
#	MESSAGE(STATUS \"Including local install script\")
#	INSTALL(PROGRAMS local_install.sh DESTINATION .)
#ENDIF(CPACK_GENERATOR MATCHES \"STGZ\")

INCLUDE(CPack)
