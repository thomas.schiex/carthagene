#ifndef _CG_PARALLEL_TBB_RANGE_H_
#define _CG_PARALLEL_TBB_RANGE_H_

namespace Parallel {

	class xrange {
		private:
			size_t b_;
			size_t e_;
		public:
			xrange(size_t b, size_t e) : b_(b), e_(e) {}
			/*typedef size_t const_iterator;*/
			class iterator : public std::iterator<std::bidirectional_iterator_tag, size_t, size_t> {
				private:
					value_type _;
				public:
					iterator(value_type x) : _(x) {}
					iterator(const iterator& x) : _(x._) {}
					value_type operator*() const { return _; }
					value_type& operator*() { return _; }
					void operator++() { ++_; }
					void operator++(int x) { ++_; }
					void operator--() { --_; }
					void operator--(int x) { --_; }
					bool operator==(const iterator&x) const { return _==x._; }
					bool operator!=(const iterator&x) const { return _!=x._; }
			};
			typedef iterator const_iterator;
			const_iterator begin() const { return iterator(b_); }
			const_iterator end() const { return iterator(e_); }
	};


	template <class Func>
	class FuncRanger {
		private:
			Func& f;
			FuncRanger() {}
		public:
			FuncRanger(Func& _) : f(_) {}
			template <typename T>
				void operator() (tbb::blocked_range<T>& r) const {
					T b=r.begin(), e=r.end();
					while(!(Utils::Interrupt::flag()||b==e)) {
						f(b);
						++b;
					}
				}
	};

	class NoCopy {
		private:
			NoCopy() {}
		public:
			template <class F>
				class Wrapped {
					private:
						F& f;
						Wrapped() {}
					public:
						Wrapped(F&f_) : f(f_) {}
						template <class T> void operator()(T&t) {
							if(Utils::Interrupt::flag()) {
								return;
							}
							f(t);
						}
						template <class T> void operator()(T&t) const {
							if(Utils::Interrupt::flag()) {
								return;
							}
							f(t);
						}
				};
			template <class F>
				static Wrapped<F> wrap(F&f) {
					return Wrapped<F>(f);
				}
	};


	template <class X = Raw>
	class Range : public X {
		public:
			template <typename V, class F>
				Range<X>& execute_slices(F& f, V start, V end, V grain=1) {
					X::before();
					tbb::task_scheduler_init init(WorkerPoolManager::getcount());
					tbb::blocked_range<V> r(start, end, grain);
					/*std::cout << "ParallelRange::slices " << (void*)&f << std::endl;*/
					tbb::parallel_for(r, FuncRanger<F>(f));
					X::after();
					return *this;
				}
			template <class F>
				Range<X>& execute_range(F& f, size_t start, size_t end) {
					X::before();
					tbb::task_scheduler_init init(WorkerPoolManager::getcount());
					xrange r(start, end);
					/*std::cout << "ParallelRange::range " << (void*)&f << std::endl;*/
					tbb::parallel_for_each(r.begin(), r.end(), NoCopy::wrap(f));
					X::after();
					return *this;
				}
	};
}


#endif

