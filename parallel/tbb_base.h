#ifndef _CG_PARALLEL_TBB_BASE_H_
#define _CG_PARALLEL_TBB_BASE_H_

#include <tbb/tbb.h>
#include <tbb/tick_count.h>
#include <tbb/tbb_thread.h>
#include <tbb/spin_mutex.h>
#if TBB_VERSION_MAJOR < 3
	extern "C" {
#		include <pthread.h>
	}
#else
#	include <tbb/compat/condition_variable>
#endif

#include <iostream>
namespace Parallel {

#	if 0 || TBB_INTERFACE_VERSION < 5003	
//#     warning "TBB_INTERFACE_VERSION < 5003"
		class Mutex {
			private:
				pthread_mutex_t mutex;
			public:
				Mutex() { pthread_mutex_init(&mutex, NULL); }
				~Mutex() { pthread_mutex_destroy(&mutex); }
				void lock() { pthread_mutex_lock(&mutex); }
				void unlock() { pthread_mutex_unlock(&mutex); }
				pthread_mutex_t* native_handle() { return &mutex; }
		};

		class CondVar {
			private:
				/*pthread_mutex_t* mutex;*/
				pthread_cond_t cond;
			public:
				CondVar() { pthread_cond_init(&cond, NULL); }
				~CondVar() { pthread_cond_destroy(&cond); }
				void notify_one() {
					pthread_cond_broadcast(&cond);
				}
				void notify_all() {
					pthread_cond_broadcast(&cond);
				}
				void wait(Mutex*mutex) {
					pthread_cond_wait(&cond, mutex->native_handle());
				}
		};
#	else
//#     warning "TBB_INTERFACE_VERSION recent enough !"
		/*typedef tbb::interface5::condition_variable CondVar;*/
		typedef tbb::mutex Mutex;
#	endif
		typedef tbb::spin_mutex Spin;
}

namespace Utils {
	typedef tbb::tick_count Tick;
	typedef tbb::tick_count::interval_t Interval;
	typedef tbb::tbb_thread Thread;
}

#endif

