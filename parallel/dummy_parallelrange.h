#ifndef _CG_PARALLEL_DUMMY_RANGE_H_
#define _CG_PARALLEL_DUMMY_RANGE_H_

namespace Parallel {
	template <class X = class Raw>
	class Range : public X {
		public:
			template <typename V, class F>
			Range<X>& execute_slices(F& f, V start, V end, V grain=1) {
				V slice;
				for(V i=start;i<end&&!Utils::Interrupt::flag();) {
					X::before();
					slice = i+grain;
					slice = slice > end ? end : slice;
					for(;i<slice;++i) { f(i); }
					X::after();
				}
				return *this;
			}

			template <class F>
			Range<X>& execute_range(F& f, unsigned long start, unsigned long end) {
				for(unsigned long i=start;i<end&&!Utils::Interrupt::flag();++i) {
					X::before();
					f(i);
					X::after();
				}
				return *this;
			}
	};
}

#endif

