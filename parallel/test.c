#include <iostream>
#include <sstream>
#include <iomanip>
#define WITH_PARALLEL
#include "parallel.h"

#if 0
void compute_fibo(unsigned int n, unsigned int*u0, unsigned int*u1) {
	unsigned int sub0, sub1, tmp0, tmp1;
	switch(n) {
		case 0: *u0 = 1; *u1=0; break;
		case 1: *u1 = 1; *u0=0; break;
		default:
				*u0=1;
				*u1=0;
				sub0=0;
				sub1=1;
				for(unsigned int sub=2;sub<=n;++sub) {
					tmp0=*u0;
					tmp1=*u1;
					*u0+=sub0;
					*u1+=sub1;
					sub0=tmp0;
					sub1=tmp1;
				}
	};
}

unsigned long long int fibo(unsigned int n) {
	switch(n) {
		case 0:
		case 1:
			return 1;
		default:;
				return fibo(n-1)+fibo(n-2);
	};
}

class do_toto {
	private:
		AccumChrono& chrono;
	public:
		do_toto(AccumChrono&c) : chrono(c) {}
		do_toto(const do_toto& d) : chrono(d.chrono) {}
		void operator() (size_t &i) {
			tbb::tick_count t = chrono.start();
			std::ostringstream mystr;
			unsigned int a, b;
			compute_fibo(i, &a, &b);
			fibo(40);
			mystr << (void*)this << " " << i << ' ' << a << "*u0 + " << b << "*u1" << std::endl;
			/*std::cout << mystr.str();*/
			chrono.stop(t);
		}
};

#define RANGE_START 0
#define RANGE_END 200


void test_foreach() {
	AccumChrono accum;
	double delta = ParallelRange<Timer>().execute_range(do_toto(accum), RANGE_START, RANGE_END).time();
	double total_seconds = accum.time();
	std::cout << "foreach : " << delta << " taken (" << total_seconds << " total => " << (100.*total_seconds/delta) << "% speedup)"<< std::endl;
}


void test_batch() {
	tbb::tick_count t0 = tbb::tick_count::now();
	AccumChrono accum;
	size_t begin=RANGE_START, end=RANGE_END;
	double delta = ParallelRange<Timer>().execute_slices(do_toto(accum), begin, end).time();
	double total_seconds = accum.time();
	std::cout << "batch : " << delta << " taken (" << total_seconds << " total => " << (100.*total_seconds/delta) << "% speedup)"<< std::endl;
}
#endif

int main(int argc, char**argv) {
	Parallel::Mutex m;
	/*std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(2);*/
	/*test_foreach();*/
	/*std::cout << std::endl << std::endl << std::endl;*/
	/*test_batch();*/
	return 0;
}

