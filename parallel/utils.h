#ifndef _CG_PARALLEL_UTILS_H_
#define _CG_PARALLEL_UTILS_H_

#include <cmath>
#include <cstdio>

extern "C" {
#include <unistd.h>
#include <time.h>
#ifndef WIN32
#include <sys/resource.h>
#elif !defined(WITH_PARALLEL)
#include <windows.h>
#endif
}


namespace Parallel {
	template<typename T>
	class Sync {
		private:
			T _;
			Mutex m;
		public:
			Sync() : _(), m() {}
			/*Sync(T x) : _(x), m() {}*/
			Sync(const T t) : _(t), m() {}
			~Sync() {}
			Sync<T>& operator = (const Sync<T>& t) {
				m.lock();
				_=t._;
				m.unlock();
				return *this;
			}
			operator T() const { return _; }
	};
}

namespace Utils {

	struct Interrupt {
		static bool flag(int x=-1) {
			static bool _ = false;
			if(x!=-1) {
				_ = x>0;
			}
			return _;
		};
	};

	class AccumChrono {
		public:
			Parallel::Sync<double> accum;
			AccumChrono() : accum() {}
			Utils::Tick start() { return Tick::now(); }
			void stop(Tick t0) { accum = (Tick::now()-t0).seconds() + accum; }
			double time() const { return accum; }
	};

#if 0
	class ThreadChrono {
			double now() const {
				struct timespec ts;
				clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts);
				return ts.tv_sec+ts.tv_nsec*(1./1000000000.);
			}
		public:
			Parallel::Sync<double> accum;
			ThreadChrono() : accum() {}
			double start() { return now(); }
			void stop(double t0) { accum = now()-t0 + accum; }
			double time() const { return accum; }
	};
#endif

	class UserThreadChrono {
		public:
			static double now() {
#ifndef WIN32
				struct rusage ru;
				getrusage(RUSAGE_THREAD, &ru);
				return ru.ru_utime.tv_sec+ru.ru_utime.tv_usec*(1./1000000.);
#else
				FILETIME cre, xit, ker, usr;
				GetThreadTimes(GetCurrentThread(), &cre, &xit, &ker, &usr);
#define CONV_LDW_TO_S (1.e-7) // Microsoft states FILETIME has values with 100ns resolution, hence 1e-7 s.
#define CONV_HDW_TO_S (429.4967296) // 2^32*1.e-7
				return usr.dwLowDateTime*CONV_LDW_TO_S + usr.dwHighDateTime*CONV_HDW_TO_S;
#endif
			}
			Parallel::Sync<double> accum;
			UserThreadChrono () : accum() {}
			double start() { return now(); }
			void stop(double t0) { accum = now()-t0 + accum; }
			double time() const { return accum; }
	};

#	define _conv_S2M (1./60.)
#	define _conv_S2H (_conv_S2M/60.)
#	define _conv_S2D (_conv_S2H/24.)

	class ProgressDisplay {
		private:
			Utils::Tick t0;
			size_t max;
			/*Parallel::Mutex mutex;*/
			Parallel::Spin mutex;
			size_t done;
			double total_time;
			double grain;
			size_t counter;
			double last_display_time;
#ifdef WITH_PARALLEL
			Utils::Thread disp_thread;
#endif

			static void printHMS(double seconds) {
				int D = seconds*_conv_S2D;
				int H = fmod(seconds*_conv_S2H, 24.);
				int M = fmod(seconds*_conv_S2M, 60.);
				double S = fmod(seconds, 60.);
				fprintf(stderr, "%3id %2.2i:%2.2i:%06.3lf", D, H, M, S);
			}

		public:
			ProgressDisplay(size_t _, double g=.2)
				: t0(Utils::Tick::now()),
				  max(_),
				  mutex(),
				  done(0),
				  total_time(0),
				  grain(g),
				  counter(0),
				  last_display_time(-1)
#ifdef WITH_PARALLEL
				  , disp_thread(do_display, this)
#endif
			{}
			~ProgressDisplay() {
#ifdef WITH_PARALLEL
				disp_thread.join();
#endif
			}
			static void do_display(ProgressDisplay* pdisp) {
#ifdef WITH_PARALLEL
				while(!pdisp->done) {
					tbb::this_tbb_thread::sleep(tbb::tick_count::interval_t(pdisp->grain));
				}
#endif
				fprintf(stderr, "Progress | Speed  ");
				fprintf(stderr, "|  Time elapsed    ");
				fprintf(stderr, "|  Estimated total ");
				fprintf(stderr, "|  Remaining\n");
#ifdef WITH_PARALLEL
				while(!pdisp->do_one_display()) {
					tbb::this_tbb_thread::sleep(tbb::tick_count::interval_t(pdisp->grain));
				}
#endif
			}
			void init(size_t _, double g=.2) {
				max = _;
				done = 0;
				total_time = 0;
				grain = g;
				counter = 0;
				last_display_time = -1;
			}
			void reset() {
				done = 0;
				total_time = 0;
				counter = 0;
				last_display_time = -1;
			}
			inline bool do_one_display() {
				double wallclock = (Utils::Tick::now()-t0).seconds();
				if((done==max)||((wallclock-last_display_time)>=grain)) {
					counter=0;
					last_display_time = wallclock;
					double speedup = total_time/wallclock;
					double progress = ((double)done)/max;
					double est_total = wallclock/progress;
					double ETA = est_total-wallclock;

					/*fprintf(stderr, "%6.2lf %% done ; speed % 5.2lfx ; time running ",*/
					fprintf(stderr, "%6.2lf %% | % 5.2lfx |", progress*100, speedup);
					printHMS(wallclock);
					/*fprintf(stderr, " ; estimated total ");*/
					fputs(" |", stderr);
					printHMS(est_total);
					fputs(" |", stderr);
					/*fprintf(stderr, " ; ETA ");*/
					printHMS(ETA);
					putc('\r', stderr);
					bool last_one = done==max;
					if(last_one) {
						putc('\n', stderr);
					} else {
						fflush(stderr);
					}
					return last_one;
				}
				return false;
			}
			void step_done(double computation_time) {
				mutex.lock();
				total_time+=computation_time;
#ifndef WITH_PARALLEL
				if(!done) {
					fprintf(stderr, "Progress | Speed  ");
					fprintf(stderr, "|  Time elapsed    ");
					fprintf(stderr, "|  Estimated total ");
					fprintf(stderr, "|  Remaining\n");
				}
#endif
				++done;
				/*if((done==max)||(++counter==grain)) {*/
#ifndef WITH_PARALLEL
				do_one_display();
#endif
				mutex.unlock();
			}
	};

}

namespace Parallel {
	struct Raw {
		void before() {}
		void after() {}
	};

#if 0
	struct Timer {
		Utils::Tick t;
		Utils::AccumChrono c;
		void before() { t=c.start(); }
		void after() { c.stop(t); }
		double time() { return c.accum; }
	};
#else
	struct Timer {
		double t;
		Utils::UserThreadChrono c;
		void before() { t=c.start(); }
		void after() { c.stop(t); }
		double time() { return c.accum; }
	};
#endif


	struct WorkerPoolManager {
#ifdef WITH_PARALLEL
		static size_t& __getnum() {
			using tbb::task_scheduler_init;
			static size_t numworkers=task_scheduler_init::default_num_threads();
			return numworkers;
		}
		static const size_t getcount() { return __getnum(); }
		static void setcount(const size_t s) { __getnum() = s; }
#else
		static const size_t getcount() { return 1; }
		static void setcount(const size_t) {}
#endif
	};
}


#endif

