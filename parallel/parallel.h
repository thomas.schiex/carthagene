#ifndef _CG_PARALLEL_H_
#define _CG_PARALLEL_H_

/*#include "config.h"*/

#ifdef WITH_PARALLEL
#	include "tbb_base.h"
#	include "utils.h"
#	include "tbb_parallelrange.h"
#else
#	include "dummy_base.h"
#	include "utils.h"
#	include "dummy_parallelrange.h"
#endif	/* WITH_PARALLEL */
#endif

