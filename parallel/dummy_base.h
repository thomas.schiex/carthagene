#ifndef _CG_PARALLEL_DUMMY_BASE_H_
#define _CG_PARALLEL_DUMMY_BASE_H_

extern "C" {
#include <sys/time.h>
}

namespace Utils {
	struct Tick : public timeval {
		static Tick now() {
			Tick t;
			gettimeofday(&t, (struct timezone*)0);
			return t;
		}
	};

	class Interval {
		private:
			double _;
		public:
			Interval(Tick& a, Tick& b)
				: _((b.tv_sec-a.tv_sec)+(b.tv_usec-a.tv_usec)*.000001)
			{}
			double seconds() const { return _; }
	};
}

inline Utils::Interval operator-(Utils::Tick b, Utils::Tick a) { return Utils::Interval(a, b); }

namespace Parallel {
	class Mutex {
		public:
			inline void lock() {}
			inline void unlock() {}
	};

	class Spin {
		public:
			inline void lock() {}
			inline void unlock() {}
	};

	class CondVar {
		public:
			CondVar() {}
			inline void notify_one() {}
			inline void notify_all() {}
			inline void wait(Mutex*m) {}
	};
}

#endif

