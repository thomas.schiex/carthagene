import re
import sys
import os


def find_and_change_to(haystack, needles):
    for n in needles:
        if n[0] in haystack:
            return n[1]


def proc_name(x):
    return find_and_change_to(x, (('Windows', 'i386'), ('i386', 'i386'),
                                  ('x86_64', 'IA64')))


re_vers = re.compile(r'.*-(([0-9]+([.][0-9a-zA-Z]+)+)).*')


def version(x):
    v = re_vers.match(x).groups()[1]
    return ('nonfree' in x or '.R' in x) and v + '-nonfree' or v


def package(x):
    return find_and_change_to(x, (('Linux-i386', 'GNU/Linux 32 bits'),
                                  ('Linux-x86_64', 'GNU/Linux 64 bits'),
                                  ('Windows', 'Microsoft Windows')))


def filetype(x):
    for ext, ret in (('.deb', None), ('.rpm', None), ('', 'Other')):
        if x.endswith(ext):
            return ret or ext


def XML_file(x):
    return '			<file processor="%s" filetype="%s" filename="%s" />' % (
           proc_name(x), filetype(x), x)


def XML_version(version, files, notes='', changes=''):
    ret = ['		<version name="%s" preformatted="true">' % version]
    ret += ['			<note>%s</note>' % notes]
    ret += ['			<change>%s</change>' % changes]
    ret += map(XML_file, files)
    ret += ['		</version>']
    return '\n'.join(ret)


def XML_package(package, versions):
    ret = ['	<package name="%s">' % (package)]
    for v in versions.keys():
        ret.append(XML_version(v, versions[v]))
    ret += ['	</package>']
    return '\n'.join(ret)


def XML_fusionforge(packages):
    ret = ['<fusionforge plugin="transfert">']
    ret += [XML_package(p, packages[p]) for p in packages.keys()]
    ret += ['</fusionforge>']
    return '\n'.join(ret)


samples = filter(lambda x: not x.endswith('.xml'), os.listdir('packages'))

FILENAMES = samples

XML_HEADER = """
<fusionforge plugin="transfert">
"""

XML_FOOTER = """
</fusionforge>"""

VERSION_BASE = None
PACKAGES = {}
for s in FILENAMES:
    PACKAGES[package(s)] = {}
for s in FILENAMES:
    v = version(s)
    PACKAGES[package(s)][v] = []
    VERSION_BASE = (VERSION_BASE is None and v
                    or len(v) < len(VERSION_BASE) and v
                    or VERSION_BASE)
for s in FILENAMES:
    PACKAGES[package(s)][version(s)].append(s)


xmlfilename = 'packages/carthagene-' + str(VERSION_BASE) + '.xml'

print >> open(xmlfilename, 'w'), XML_fusionforge(PACKAGES)

print "Description generated in", xmlfilename


print "please now issue the following command in ./packages/:"
print "ls -c1 | xargs -I - scp -",
print "dleroux@mulcyber:/var/lib/gforge/chroot/ftproot/carthagene/."
