# carthaperl.pl -- perl interface to carthagene library libcartagene.so
#	uses Inline::Tcl version 0.10 from Inline-Tcl-0.10.tar.gz
#	    version 0.09 from CPAN does not work
#
#	Inline-Tcl-0.10.tar.gz is available from
#           http://ttul.org/~rrsadler/Inline-Tcl/Inline-Tcl-0.10.tar.gz 
#	 
#   snelling at email.marc.usda.gov
#


use strict;
no strict "subs";
require Inline::Tcl;
use Inline Tcl => << '__END_Tcl__';

load /home/snelling/carthagene/CarthaGene/cgsh/libcartagene.so

proc dsLoad { file } {
  ::CG::dsload $file
}

proc dsMerge { dsa dsb } {
puts "merging $dsa $dsb"
  ::CG::dsmergen $dsa $dsb
}

proc dsMergOr { dsa dsb } {
puts "merging $dsa $dsb"
  ::CG::dsmergor $dsa $dsb
}

proc cgEM {} {
  ::CG::sem
}

proc mrklod2p {} { ::CG::mrklod2p }
proc mrkdist2p {} { ::CG::mrkdist2p h }

proc buildfw { keep add markers test } {
puts "buildfw $keep $add $markers $test"
  ::CG::buildfw $keep $add $markers $test
}
proc huh { h1 h2 } {
puts "huh $h1 $h2"
}

proc mapordget { map file } {
# write to file - workaround carthagene 
#    output pointer *lmiout hosed by redirecting stdout ????

   ::CG::cgout $file
   ::CG::mapordget $map
   ::CG::cgout ""
}

proc setMrkOrd { markerOrder } {
puts "setting order"

puts "markers $markerOrder"
   ::CG::mrkselset $markerOrder
}

proc dsInfo {} { ::CG::dsinfo }

proc mrkInfo {} { ::CG::mrkinfo }

proc mrkMerge { m1 m2 } {
  ::CG::mrkmerge $m1 $m2
}

proc heapPrint {} { ::CG::heaprint }

proc polishMap {} { ::CG::polish }

proc flipMap { window } {
  # handle repeated flips with perl - carthagene repeated flips continues
  #   with small changes in likelihood (less than precision of output)
  #
  #   This could/should be coded without defaults - call with ::CG::flips options
  ::CG::flips $window -.1 0
}

proc anneal { tries initTemp finalTemp coolRate } {
  ::CG::annealing $tries $initTemp $finalTemp $coolRate
}

proc greedy { nbLoop nbIter tabooMin tabooMax } {
  ::CG::greedy $nbLoop $nbIter $tabooMin $tabooMax
}

proc printMap { map } {
  ::CG::maprintd $map
}

proc printMapR { map } {
  ::CG::maprintdr $map
}

proc CGrestart {} {
  ::CG::cgrestart
}
__END_Tcl__

sub mapOrdGet {
  my $map = shift;
  my $file = $$ . ".maporder";

  mapordget( $map, $file );
  open( MO, "<", $file );
  while( <MO> ){
    my $l = $_;
    $l =~ s/\n//;
    $l =~ s/^\s+//;
    $l =~ s/\s+$//;
    if( ! ( $l =~ /CG/ ) ){
      if( length( $l ) > 0 ){
        print "$l\n";
      }
    }
  }
  close( MO );
  unlink $file;
}

sub redirTcl {	#capture tcl stdout in file
  my $file = shift;

  open( OLDOUT, ">&STDOUT" );
  open ( STDOUT, ">$file" );
  select( STDOUT ); $| = 1;
}

sub redirStd {	#redirect stdout back to stdout
  open( STDOUT, ">&main::OLDOUT" );
}

sub sEM {
  redirTcl( "$$.sEM.res" );
  cgEM;
  redirStd();

  undef my $like;

  open( EM, "<", "$$.sEM.res" );
  while( <EM> ){
    my $l = $_;
    if( m/log10-like/ ){
      $l =~ s/://g;
      $l =~ s/=//g;
      $l =~ s/\s+/ /g;
      my @a = split( / /, $l );
      $like = $a[3];
    }
  }
  close( EM );
  print "like = $like\n";
  return( $like );
}

sub readHeap {
  my $file = shift;

  open( HF, "<", $file );

  undef my %heap;
  while( <HF> ){
    if( /^Map/ ){
      my $l = $_;
      $l =~ s/://g;
      $l =~ s/=//g;
      $l =~ s/\s+/ /g;

      my @a = split( / /, $l );
      $heap{$a[1]} = $a[3];
    }
  }
  close( HF );
  foreach my $h ( sort { $heap{$a} <=> $heap{$b} } keys %heap ){
    print "$h $heap{$h}\n";
  }
  return( %heap );
}

sub bestMap {
  my $arg = shift;
  my %heap = %{$arg};

  my $maxLik = -99999;
  my $bestMap = -1;
  undef my %best;
  foreach my $h ( keys %heap ){
    $maxLik = $maxLik > $heap{$h} ? $maxLik : $heap{$h};
    $bestMap = $maxLik == $heap{$h} ? $h : $bestMap;
  }
  $best{$bestMap} = $heap{$bestMap};
print "best $bestMap $heap{$bestMap} $best{$bestMap}\n";
  return( %best );
}

sub getCurrentBest {
  my $heapFile = "$$.curHeap";
  redirTcl( $heapFile );
  heapPrint;
  redirStd();
  my %heap = readHeap( $heapFile );
  my %best = bestMap( \%heap );
  my @bestKey = keys %best;
  my $curBest = $best{$bestKey[0]};
  return( %best );
}

sub loadSaveMap {
  my $file = shift;
  my $mapFile = "$file.map";
  my $mrkFile = "$file.markers";
  CGrestart();
  dsLoad( $file );
  redirTcl( $mrkFile );
  mrkInfo();
  redirStd();
  my $like = sEM();
  saveMap( { map => 0, file => $mapFile } );
  return( $like );
}

sub saveMap {
  my $arg = shift;
  my %par = %{$arg};

  my $map = $par{map};
  my $file = $par{file};

  saveOrder( {map=>$map, file=>"$file.ord"} );

  redirTcl( $file );
  printMap( $map );
  redirStd();

  redirTcl( $file.".rev" );
  printMapR( $map );
  redirStd();
}

sub saveOrder {
  my $arg = shift;
  my %par = %{$arg};

  my $map = $par{map};
  my $file = $par{file};

print "map order $map\n";
mapOrdGet( $map );

  redirTcl( $file );
  mapOrdGet( $map );
  redirStd();
}

sub markerId {
  undef my %markers;
  my $file = shift;
  open( MF, "<", $file );
  while( <MF> ){
    my $l = $_;
    $l =~ s/^\s+//g;
    $l =~ s/\s+/ /g;
    if( $l =~ m/^\d/ ){ #stripped line starts with digit
      my @r = split( / /, $l );
      $markers{$r[1]} = $r[0];  # hash marker_name => marker_num
    }
  }
  close( MF );
  return( %markers );
}

sub readMap {
  my $file = shift;

  while(1){
    if( -s $file ){
      last;
    }
    print "waiting for $file\n";
    sleep 1;
  }

  open( MF, "<", $file );

  undef my %pos;

  while( <MF> ){
    my $l = $_;
    $l =~ s/^\s+//g;
    $l =~ s/\s+/ /g;
    $l =~ s/\n$//;

    if( $l =~ /^\d/ && !( $l =~ /like/ ) ){
      my @r = split( / /, $l );
      my $lr = $r[$#r];
      $lr =~ s/\-//g;
      if( $lr =~ /\d$/ || length($lr) == 0 ){
        if( exists ( $pos{$r[2]} ) ){
          if( $r[0]-1 != $pos{$r[2]} ){
            print "conflicting order $r[2] $pos{$r[2]} $r[0]-1\n";
          }
        } else {
          $pos{$r[2]} = $r[0]-1;	#count from zero
        }
      }
    }
  }
  close( MF );
  return( %pos );
}

sub readMapOrder {
  my $map=shift;
  my $markers = shift;
  my $order = "$map.ord";
  my $o = 0;

}
  
sub readMapPositions {
  my $file = shift;
  open( MF, "<", $file );

  undef my %pos;
  my @kosDistance = ( 0 );
  my $cumDistance = 0.;

  while( <MF> ){
    undef my @r;
    my $l = $_;
    $l =~ s/^\s+//g;
    $l =~ s/\s+/ /g;

    if( $l =~ m/^\d/ ){
      @r = split( / /, $l );
    }
    if( ( scalar @r ) == 12 ){
      $cumDistance += $r[7];
      push @kosDistance, $cumDistance;
    }
  }
  close( MF );
  return( @kosDistance );
}

sub regress {
  my $arg = shift;
  my %data = %{$arg};

  my $n = 0;
  my $sumX = 0;
  my $sumXX = 0;
  my $sumY = 0;
  my $sumXY = 0;

  foreach my $d ( values %data ){
    my @xy = @$d;
    $sumX += $xy[0];
    $sumXX += $xy[0] * $xy[0];
    $sumY += $xy[1];
    $sumXY += $xy[0] * $xy[1];
    $n++;
  }
  my $beta = ( $n*$sumXY - $sumX*$sumY ) /
             ( $n*$sumXX - $sumX*$sumX );
  my $intercept = ( $sumY - $beta*$sumX ) / $n;

#  print "intercept $intercept beta $beta\n"; 

  foreach my $d ( values %data ){
    my @xy = @$d;
    my $pred = $intercept + $beta * $xy[0];
#    print "$xy[0] $xy[1] $pred\n";
  }

  my %sol = ( intercept => $intercept, 
	      beta => $beta );
  return( %sol );
}

sub polishAndFlip {
  my $curBest = shift;
  my $window = shift;
  my $oldBest = $curBest;
  my $startBest = $curBest;
  print "$oldBest $curBest\n";

  my $flipRounds = 0;
  my $polishRounds = 0;
  my $repeatRounds = 0;
  my $heapFile = "$$.curHeap";

  undef my $lastMap;

  while ( 1 ) {
    while ( 1 ) {
      # repeated polishing - continue until likelihood does not improve
      polishMap;
      $polishRounds++;
      redirTcl( $heapFile );
      heapPrint;
      redirStd();
      my %heap = readHeap( $heapFile );
      my %best = bestMap( \%heap );
      my @bestKey = keys %best;
      $curBest = $best{$bestKey[0]};
      print "old $oldBest cur $curBest\n";
      if ( $curBest <= $oldBest ) {
	saveMap( {map=>$bestKey[0], file=>"$$.map.polish.$repeatRounds"} );
	$lastMap = "$$.map.polish.$repeatRounds";
	last;
      }
      $oldBest = $curBest;
    }
    if( $flipRounds > 0 && $oldBest == $startBest ){
      #we've polished and flipped at least once - 
      #  the last polish didn't change things so
      #  we don't need to flip again.
      last;
    }
    while ( 1 ) {
      # repeated flips - continue until likelihood does not improve
      flipMap( $window );
      $flipRounds++;
      redirTcl( $heapFile );
      heapPrint;
      redirStd();
      my %heap = readHeap( $heapFile );
      my %best = bestMap( \%heap );
      my @bestKey = keys %best;
      $curBest = $best{$bestKey[0]};
      if ( $curBest <= $oldBest ) {
	saveMap( {map=>$bestKey[0], file=>"$$.map.flip.$repeatRounds"} );
	$lastMap = "$$.map.flip.$repeatRounds";
	last;
      }
      $oldBest = $curBest;
    }
    $repeatRounds++;
    print "polished and flipped start $startBest cur $curBest\n";
    print "  round $repeatRounds polishes $polishRounds flips $flipRounds\n";
    if ( $curBest <= $startBest ) {
      last;
    }
    $startBest = $curBest;
  }
  return( $lastMap );
}

sub compareIntegratedMap {
  my $arg = shift;
  my %par = %{$arg};

  my %iId = %{$par{iMarkers}};

  my %lmOrd = %{$par{lmOrd}};
  my %lmMap = %{$par{lmMap}};
  my %rhOrd = %{$par{rhOrd}};
  my %rhMap = %{$par{rhMap}};

  my $mFile = $par{mapFile};
  my $rFile = $par{resFile};

  undef my %iMap;
  my %iOrd = readMap( $mFile );
  my @iPos = readMapPositions( $mFile );
  foreach my $m ( keys %iId ) {
    $iMap{$m} = $iPos[$iOrd{$m}];
  }

  redirTcl( $rFile );
  print "marker id intOrd intPos lmOrd lmPos rhOrd rhPos\n";
  foreach my $m ( keys %iId ) {
    my $lOrd = ".";
    my $lPos = ".";
    my $rOrd = ".";
    my $rPos = ".";
    if( exists( $lmOrd{$m} ) ){
      $lOrd = $lmOrd{$m};
      $lPos = $lmMap{$m};
    }
    if( exists( $rhOrd{$m} ) ){
      $rOrd = $rhOrd{$m};
      $rPos = $rhMap{$m};
    }
    print "$m $iId{$m} $iOrd{$m} $iMap{$m} $lOrd $lPos $rOrd $rPos\n";
  }
  redirStd();
}
