#ifndef _TCL_SYSTEM_H
#define _TCL_SYSTEM_H

#ifdef OUTPUT_SYSTEM_DEFINED
#error "There can be only one binding at a time."
#endif

#define OUTPUT_SYSTEM_DEFINED


#include <cstring>
#include <string>
#include <vector>
#include "tcl.h"
#include <stdarg.h>
#include <malloc.h>

extern Tcl_Interp *linterp;

#ifdef __MINGW32__
extern "C" {
    int vasprintf(char **strp, const char *fmt, va_list ap);
    int asprintf(char **strp, const char *fmt, ...);
}
#endif

inline void print_out(const char* fmt, ...) {
    va_list va;
    char* str = NULL;
    va_start(va, fmt);
    vasprintf(&str, fmt, va);
    va_end(va);
    //fprintf(stderr, "str=%p\n", str);
    if(!str) {
        fputs("<<couldn't allocate output string>>\n", stderr);
        return;
    }
    if(Fout != NULL) {
        fputs(str, Fout);
    }
    char* cmd = NULL;
    asprintf(&cmd, "puts -nonewline {%s}; flush stdout", str);
    //fprintf(stderr, "cmd=%p\n", cmd);
    if(!cmd) {
        fputs("<<couldn't allocate Tcl output string>>\n", stderr);
        free(str);
        return;
    }
    Tcl_Eval(linterp, cmd);
    free(cmd);
    free(str);
}


inline void print_err(const char* fmt, ...) {
    va_list va;
    char* str = NULL;
    va_start(va, fmt);
    vasprintf(&str, fmt, va);
    va_end(va);
    //fprintf(stderr, "str=%p\n", str);
    if(!str) {
        fputs("<<couldn't allocate output string>>\n", stderr);
        return;
    }
    if(Fout != NULL) {
        fputs(str, Fout);
    }
    char* cmd = NULL;
    asprintf(&cmd, "puts -nonewline stderr {%s}; flush stderr", str);
    //fprintf(stderr, "cmd=%p\n", cmd);
    if(!cmd) {
        fputs("<<couldn't allocate Tcl output string>>\n", stderr);
        free(str);
        return;
    }
    Tcl_Eval(linterp, cmd);
    free(cmd);
    free(str);
}

// inline void print_err(const char* fmt, ...) {
//     static char buf[4096];
//     va_list va;
//     va_start(va, fmt);
//     sprintf(buf, "puts -nonewline {%s}; flush stdout", fmt);
//     std::string cmd = string_format(buf, va);
//     if(Fout != NULL) {
//         va_start(va, fmt);
//         vfprintf(Fout, fmt, va);
//     }
//     Tcl_Eval(linterp, cmd.c_str());
//     /*return Tcl_Write(Tcl_GetStdChannel(TCL_STDERR), buf.c_str(), buf.size());*/
// }

inline void flush_out() {
    /*Tcl_Flush(Tcl_GetStdChannel(TCL_STDOUT));*/
    /*if(Fout != NULL) {*/
        /*fflush(Fout);*/
    /*}*/
    Tcl_Eval(linterp, "flush stdout");
}

inline void flush_err() {
    /*Tcl_Flush(Tcl_GetStdChannel(TCL_STDERR));*/
    /*if(Fout != NULL) {*/
        /*fflush(Fout);*/
    /*}*/
    Tcl_Eval(linterp, "flush stderr");
}

#endif
