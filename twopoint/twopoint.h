#ifndef _CG_2PT_H_
#define _CG_2PT_H_


#define USE_DISK_CACHE
#undef NDEBUG

#include "../parallel/parallel.h"

#include "base.h"
#include "backend_base.h"
#include "backend_memory.h"
#include "backend_file.h"
#include "matrices.h"

#endif

