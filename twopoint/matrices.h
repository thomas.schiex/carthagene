#ifndef _CG_2PT_MATRICES_H_
#define _CG_2PT_MATRICES_H_

namespace TwoPoint {
	class Matrices {
		private:
            double (*DHfunc)(double);
			BioJeu* bj;
			Backend::Generic* backend;
		public:
			Matrices(BioJeu* bj_, Backend::Generic* backend_=NULL);
			~Matrices() { delete backend; }
			void changeBackend(bool eager, Backend::Generic* nb);
			double getLOD(int i, int j) {
				return backend->get(i, j)->LOD;
			}
			double getFR(int i, int j) {
				return backend->get(i, j)->FR;
			}
			double getDH(int i, int j);
			inline unsigned int computation_count() const { return backend->computation_count(); }
	};
}

#endif

