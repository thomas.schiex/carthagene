#ifndef _CG_2PT_BACKEND_BASE_H_
#define _CG_2PT_BACKEND_BASE_H_

#ifndef NDEBUG
#  include <cstdio>
#  include <iostream>
#  define DBG(_x_) do { std::cerr << _x_; } while(0)
#else
#  define DBG(_x_)
#endif

#include <vector>

class BioJeu;

namespace TwoPoint {
	namespace Backend {

		namespace impl {
			class CacheStats {
				private:
					unsigned long long hits_;
					unsigned long long misses_;
					unsigned long long size_;
					unsigned long long inuse_;
				public:
					CacheStats() : hits_(0), misses_(0), size_(0), inuse_(0) {}
					void hit() { hits_++; }
					void miss() { misses_++; }
					void set_size(unsigned long long sz) { size_=sz; }
					void new_value() { misses_++; inuse_++; }

					float occupation() const { return 100.f*inuse_/size_; }
					float raw_efficiency() const { return 100.f*hits_/(hits_+misses_); }
					float efficiency() const { return 100.f*hits_/(hits_+misses_-inuse_); }
					int hits() const { return hits_; }
					int misses() const { return misses_; }
					int inuse() const { return inuse_; }
					int size() const { return size_; }

					void print(std::ostream&os = std::cout) const;
			};
			inline std::ostream& operator << (std::ostream&os, CacheStats&cs) { cs.print(os); return os; }
		}


		class ComputationHook {
			public:
				virtual ~ComputationHook() {}
				virtual void start(int i, int j) {};
				virtual void end(Data*d) {};
				virtual bool disposable() const { return false; }
				static void dispose(ComputationHook*ch) {
					if(ch->disposable()) {
						delete ch;
					}
				}
		};

		extern ComputationHook* const no_hook;

		class ChainedComputationHook : public ComputationHook {
			private:
				ComputationHook* front, * back;
				ChainedComputationHook(ComputationHook*f, ComputationHook*b)
					: front(f), back(b) {}
				~ChainedComputationHook() {
					ComputationHook::dispose(front);
					ComputationHook::dispose(back);
				}
			public:
				void start(int i, int j) {
					back->start(i, j);
					front->start(i, j);
				}
				void end(Data*d) {
					front->end(d);
					back->end(d);
				}
				static ComputationHook* create(ComputationHook*f, ComputationHook*b) {
					if(f==no_hook) {
						return b;
					}
					if(b==no_hook) {
						return f;
					}
					return new ChainedComputationHook(f, b);
				}
		};

		class Generic {
			protected:
				BioJeu* bj;
				Parallel::Mutex mutex;
				off_t n_computations;
				void ComputeSync(int i, int j, TwoPoint::Data* d, ComputationHook*hook=NULL);
			public:
				Generic(BioJeu*_) : bj(_), mutex(), n_computations(0) {}
				virtual ~Generic() {}
				virtual void init(off_t size) = 0;
				virtual void raw_get(int i, int j, TwoPoint::Data**dptr, ComputationHook**hptr) = 0;
				virtual void set(int i, int j, TwoPoint::Data* d) = 0;
				void lock() { mutex.lock(); }
				void unlock() { mutex.unlock(); }
				off_t computation_count() const { return n_computations; }
				TwoPoint::Data* get(int i, int j);
				BioJeu* source() const { return bj; }
		};

		class ComputationHookSetter : public ComputationHook {
			private:
				Generic* dest;
				int I, J;
			public:
				ComputationHookSetter(Generic* d) : dest(d) {}
				void start(int i, int j) { I=i; J=j; }
				void end(Data*d) { dest->set(I, J, d); }
				bool disposable() const { return true; }
		};

		class Chain : public Generic {
			private:
				Generic* front, * back;
			public:
				Chain(Generic*f, Generic*b) : Generic(f->source()), front(f), back(b) { if(f->source()!=b->source()) { throw std::bad_alloc(); } }
				~Chain() {
					DBG("Chain performed " << computation_count() << " computations" << std::endl);
					DBG("Front performed " << front->computation_count() << " computations" << std::endl);
					DBG("Back performed " << back->computation_count() << " computations" << std::endl);
					delete front;
					delete back;
				}
				void init(off_t size) {
					front->init(size);
					back->init(size);
				}
				void raw_get(int i, int j, Data** dptr, ComputationHook**hptr) {
					front->raw_get(i, j, dptr, hptr);
					if((*dptr)->must_compute()) {
						ComputationHook* bhptr;
						back->raw_get(i, j, dptr, &bhptr);
						ComputationHookSetter* chs = new ComputationHookSetter(front);
						 *hptr = ChainedComputationHook::create(*hptr, ChainedComputationHook::create(bhptr, chs));
					}
				}
				void set(int i, int j, Data*d) {
					front->set(i, j, d);
					back->set(i, j, d);
				}
		};


		namespace Factory {
			void set_default(const char* backend);
			void set_default(std::string&backend);
			const char* get_default();
			Generic* create(const char*backend, BioJeu*bj);
			Generic* create_default(BioJeu*bj);
			static inline Generic* create(std::string&backend, BioJeu*_) { return create(backend.c_str(), _); }
		}


		class Triangular : public Generic {
			protected:
				inline static unsigned long long mkofs(int i, int j) {
					if(i<j) {
						i^=j;
						j^=i;
						i^=j;
					}
					//--i;	/* turns out CarthaGene counts from 1 */
					//--j;	/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

					unsigned long long ofs = i;

					/*std::cerr << "   got ofs="<< (((ofs*(ofs-1))>>1)+j) << std::endl;*/
					return ((ofs*(ofs-1))>>1)+j;
				}
				inline static unsigned long long mksize(unsigned int i) { return mkofs(i-1, i-1)+1; }
			public:
				Triangular(BioJeu*_) : Generic(_) {}
		};

		class NoCache : public Generic {
			private:
				int I, J;
				TwoPoint::Data d;
			public:
				NoCache(BioJeu*_) : Generic(_), I(-1), J(-1), d() {}

				void init(off_t size) {
					I=J=-1;
				}

				void raw_get(int i, int j, Data**dptr, ComputationHook**hptr) {
					/*DBG("no_cache::raw_get("<<i<<','<<j<<')'<<std::endl);*/
					if(i<j) {
						i^=j;
						j^=i;
						i^=j;
					}
					if((i!=I)||(j!=J)) {
						I=i;
						J=j;
						d.clear();
					}
					*dptr = &d;
				}

				void set(int i, int j, TwoPoint::Data*nd) {
					lock();
					d.FR=nd->FR;
					d.LOD=nd->LOD;
					I=i;
					J=j;
					unlock();
				}
		};
	}
}

#endif

