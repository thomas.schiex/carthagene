#if !defined(_CG_2PT_BACKEND_FILE_H_) && defined(USE_DISK_CACHE)
#define _CG_2PT_BACKEND_FILE_H_

extern "C" {
#include <unistd.h>
#include <stdlib.h> // for mkstemp
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
}

namespace TwoPoint {
	namespace Backend {
		namespace Disk {
			namespace impl {
				const unsigned int pagesize = getpagesize()*sizeof(TwoPoint::Data);	/* ensure alignment on page size AND on data size */
				/*const unsigned int pagesize;*/

				class Failure : public std::string {
					public:
						Failure(int err) : std::string(strerror(err)) {}
				};

				class CacheFile {
					private:
						int fd;
						off_t size;
						/*char name[16];*/
						std::string name;
						Parallel::Mutex mutex;
					public:
						CacheFile(off_t size_, const char*filename);
						~CacheFile();
						void* map_page(off_t offset) {
							mutex.lock();
							/*std::cout << "map_page " << fd << " " << offset << " " << pagesize;*/
							void* ret = mmap(NULL, pagesize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, offset);
							/*std::cout << " => " << ret << std::endl;*/
							if(!ret) {
								throw new Failure(errno);
							}
							mutex.unlock();
							return ret;
						}
						off_t count_pages() const {
							return 1+size/pagesize;
						}
				};

				struct Page : public ComputationHook {
					Page* next;
					Page* prev;
					void* buffer;
					CacheFile* f;
					off_t offset;
					size_t slots_in_use;
					/*Parallel::Mutex mutex;*/
					Page()
					  : next(0), prev(0), buffer(0), f(0), offset(0), slots_in_use(0)
					  /*, mutex()*/
					{}
					bool isMapped() const { return !!buffer; }
					bool isSwapped() const { return !buffer; }
					void init(CacheFile*f_, off_t o) {
						f=f_;
						offset=o;
					}
					bool unmap() {
						/*msync(buf, pagesize, MS_SYNC|MS_INVALIDATE);*/
						/*mutex.lock();*/
						if(slots_in_use) {
							return false;
						}
						if(buffer) {
							munmap(buffer, pagesize);
							buffer=NULL;
						}
						/*mutex.unlock();*/
						return true;
					}
					void lock_slot() { ++slots_in_use; }	/* FIXME : all that need sync */
					void unlock_slot() { --slots_in_use; }
					void* map() {
						/*mutex.lock();*/
						if(!buffer) {
							buffer = f->map_page(offset);
						}
						/*mutex.unlock();*/
						return buffer;
					}
					~Page() {
						if(buffer) {
							unmap();
						}
					}
					void remove(Page*&head, Page*&tail) {
						/*mutex.lock();*/
						if(!(next||prev)) {
							return;
						}
						if(tail==this) {
							/* assume next==NULL */
							tail=prev;
						} else {
							next->prev=prev;
						}
						if(head==this) {
							/* assume prev==NULL */
							head=next;
						} else {
							prev->next=next;
						}
						next=prev=NULL;
						/*mutex.unlock();*/
					}
					void insert(Page*& head, Page*& tail) {
						/*mutex.lock();*/
						if(next||prev) {
							return;
						}
						if(!tail) {
							tail = this;
						}
						if(head) {
							head->prev = this;
						}
						this->prev=NULL;
						this->next=head;
						head=this;
						/*mutex.unlock();*/
					}

					/* ComputationHook */
					void start(int i, int j) { lock_slot(); }
					void end(Data*d) { unlock_slot(); }
					bool disposable() const { return false; }
				};

				class PageCache {
					private:
						static Page* mru;	/* list head */
						static Page* lru;	/* list tail */
						static unsigned long long count;
						static const unsigned long long max_count;
						static Parallel::Mutex mutex;
					public:
						static TwoPoint::Backend::impl::CacheStats stats;
						static void init() {
							stats.set_size(impl::PageCache::max_count);
						}
						static void* access_page(Page*p) {
							mutex.lock();
							if(p==mru) {
								stats.hit();
								mutex.unlock();
								return p->buffer;
							}
							if(p->isMapped()) {
								p->remove(mru, lru);
								p->insert(mru, lru);
								stats.hit();
								mutex.unlock();
								return p->buffer;
							} else if(count==max_count) {
								Page* r = lru;
								r->remove(mru, lru);
								r->unmap();
								stats.miss();		/* new value, but doesn't increase population */
							} else {
								count+=1;
								stats.new_value();	/* also a miss */
							}
							p->insert(mru, lru);
							mutex.unlock();
							return p->map();
						}
				};
			}

			class Cache : public Triangular {
				private:
					impl::CacheFile* f;
					std::vector<impl::Page> pages;
					void* get_datum(int i, int j) {
						off_t ofs = mkofs(i, j)*sizeof(Data);
						off_t pagenum = ofs/impl::pagesize;
						off_t dataofs = ofs%impl::pagesize;
						return ((char*)impl::PageCache::access_page(&pages[pagenum]))+dataofs;	/* void* is not supposed to handle pointer arithmetic */
					}
				public:
					Cache(BioJeu*_) : Triangular(_), f(0), pages() {
						if(!impl::PageCache::stats.size()) {
							impl::PageCache::init();
						}
					}
					~Cache() { if(f) { delete f; } pages.clear(); }

					void init(off_t size);

					void raw_get(int i, int j, Data**dptr, ComputationHook**hptr) {
						/* FIXME dirty c/p from get_datum to gain access to page object */
						off_t ofs = mkofs(i, j)*sizeof(Data);
						off_t pagenum = ofs/impl::pagesize;
						off_t dataofs = ofs%impl::pagesize;
						impl::Page* page = &pages[pagenum];
						*hptr = page;
						*dptr = (Data*) (((char*)impl::PageCache::access_page(page))+dataofs);
					}

					void set(int i, int j, TwoPoint::Data* newdat) {
						lock();
						TwoPoint::Data* d = (TwoPoint::Data*) get_datum(i, j);
						d->LOD=newdat->LOD;
						d->FR=newdat->FR;
						unlock();
					}
			};
		}
	}
}

#endif

