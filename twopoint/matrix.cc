#include <typeinfo>
#include "../calcul/BioJeu.h"
#include "../calcul/BioJeuSingle.h"
#include "../calcul/Genetic.h"
#include "twopoint.h"
#include "../parallel/parallel.h"

namespace TwoPoint {
	Data Data::null;

	namespace Backend {
		namespace impl {
			void CacheStats::print(std::ostream&os) const {
				os << "=== cache statistics ======" << std::endl;
				os << "cache size           : " << size() << std::endl;
				os << "cache hits           : " << hits() << std::endl;
				os << "cache misses         : " << misses() << std::endl;
				os << "cache inuse          : " << inuse() << std::endl;
				os << "cache occupation     : " << occupation() << '%' << std::endl;
				os << "cache efficiency     : " << efficiency() << '%' << std::endl;
				os << "cache raw efficiency : " << raw_efficiency() << '%' << std::endl;
			}
		}

		ComputationHook _no_hook;
		ComputationHook* const no_hook = &_no_hook;

		namespace Factory {
			std::string default_backend = "nocache";
			void set_default(const char* backend) { default_backend = backend; }
			void set_default(std::string&backend) { default_backend = backend; }
			const char* get_default() { return default_backend.c_str(); }
			Generic* create_default(BioJeu*_) { return create(default_backend, _); }
			Generic* create(const char*backend, BioJeu*_) {
				if(!strcmp(backend, "nocache")) {
					return new NoCache(_);
				} else if(!strcmp(backend, "buffer")) {
					return new Memory::Buffer(_);
				} else if(!strcmp(backend, "hashcache")) {
					return new Chain(new Memory::Cache(_), new Disk::Cache(_));
				} else if(!strcmp(backend, "swap")) {
					return new Disk::Cache(_);
				}
				return NULL;
			}
		}

		namespace Memory {
			std::vector<Cache::CacheData> Cache::cache;
			TwoPoint::Backend::impl::CacheStats Cache::stats;
			/*unsigned long long Cache::size = 0;*/
			/*unsigned long long Cache::hits = 0;*/
			/*unsigned long long Cache::misses = 0;*/
			/*unsigned long long Cache::inuse = 0;*/
		}

		TwoPoint::Data* Generic::get(int i, int j) {
			/*DBG("get("<<i<<','<<j<<')'<<std::endl);*/
			if(i==j) {
				return &TwoPoint::Data::null;
			}
			if(i==0||j==0) {
				throw "Unexpected matrix indices";
			}
			//--i; /* turns out CarthaGene counts from 1 */
			//--j; /* ... */
			TwoPoint::Data*d;
			ComputationHook*h = no_hook;
			lock();
			raw_get(i-1, j-1, &d, &h);
			ComputeSync(i, j, d, h);
			ComputationHook::dispose(h);
			unlock();
			return d;
		}

		static const union {
			const char _[sizeof(double)];
			double d;
		} hacky_nan = {{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }};
		static const double COMPUTING_IN_PROGRESS = hacky_nan.d;
		struct DataFence {
			double fence;
			Parallel::CondVar* cond;
		};

		void Generic::ComputeSync(int i, int j, TwoPoint::Data* d, ComputationHook*h) {
			DataFence*f = (DataFence*)d;
			h->start(i, j);
			if(d->must_compute()) {
				double LOD, FR;
				Parallel::CondVar cond;
				f->fence=COMPUTING_IN_PROGRESS;
				f->cond = &cond;
				++n_computations;
				unlock();

				LOD = bj->ComputeOneTwoPoints(i, j, bj->Epsilon2, &FR);
				
				lock();
				d->LOD=LOD;
				d->FR=FR;
				cond.notify_all();
			} else {
				while(f->fence == COMPUTING_IN_PROGRESS) {
					f->cond->wait(&mutex);
				}
			}
			h->end(d);
		}

		namespace Disk {
			namespace impl {
				Page* PageCache::mru=NULL;	/* list head */
				Page* PageCache::lru=NULL;	/* list tail */
				Parallel::Mutex PageCache::mutex;
				unsigned long long PageCache::count=0;
#				define PAGE_CACHE_BYTESIZE (1<<25)	/* 30 for 1Go, should be 32Mo there */
				const unsigned long long PageCache::max_count=PAGE_CACHE_BYTESIZE/pagesize;	/* guess pagesize is always aligned on a power of 2 */
				TwoPoint::Backend::impl::CacheStats PageCache::stats;

				CacheFile::CacheFile(off_t size_, const char*filename) : mutex() {
					char dummy=0;
					/*strcpy(name, "cg_cache_XXXXXX");*/
					/*fd = mkstemp(name);*/
					name = filename;
					name+= ".2pt";
					fd = open(name.c_str(),
							/*O_CREAT|O_DIRECT|O_NOATIME|O_RDWR,*/
							O_CREAT|O_NOATIME|O_RDWR,
							S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
					if(fd==-1) {
						std::cerr << "Couldn't open swap file " << name.c_str() << " because of error " << strerror(errno) << std::endl;
						throw std::string("Can't write swap file");
					}
					size = ((size_+pagesize-1)/pagesize)*pagesize; /* fugly, but no better idea */
					lseek(fd, size-1, SEEK_SET);
					if(write(fd, &dummy, 1)==-1) {
						std::cerr << "Can't write to swap file " << name << " :( " << strerror(errno) << std::endl;
						throw std::string("Can't write swap file");
					}
					size=size_;
				}

				CacheFile::~CacheFile() {
					close(fd);
				}
			}

			void Cache::init(off_t size) {
				/*std::cout << "init disk cache" << std::endl;*/
				if(f) {
					delete f;
				}
				std::string nom;
				try {
					nom = dynamic_cast<BioJeuSingle*>(bj)->NomJeu;
				} catch(std::bad_cast bc) {
					nom = "cg_cache_FIXME";
				}
				/*std::cout << "NomJeu : " << nom << std::endl;*/
				/*f = new impl::CacheFile(size*((size+1)>>1)*sizeof(TwoPoint::Data), nom.c_str());*/
				f = new impl::CacheFile(mksize(size)*sizeof(Data), nom.c_str());
				pages.resize(f->count_pages());
				std::vector<impl::Page>::iterator
					begin=pages.begin(),
					end=pages.end();
				off_t ofs=0;
				while(begin!=end) {
					(*begin).init(f, ofs);
					ofs+=impl::pagesize;
					++begin;
				}
			}
		}

	}

	//	namespace Disk {
	//		namespace impl {
	//			const unsigned int pagesize = getpagesize()*sizeof(TwoPoint::Data);	/* ensure alignment on page size AND on data size */
	//		}
	//	}


	void Matrices::changeBackend(bool eager, Backend::Generic* nb)
	{
		if(eager) {
			for(int i=0;i<=bj->NbMarqueur;i++) {
				for(int j=0;j<=bj->NbMarqueur;j++) {
					nb->set(i, j, backend->get(i, j));
				}
			}
		}
		delete backend;
		backend = nb;
	}

	double Matrices::getDH(int i, int j) {
		return DHfunc(backend->get(i, j)->FR);
		/*double fr = backend->get(bj, i, j)->FR;*/
		/*return  bj->HasRH() ? Theta2Ray(fr) : Haldane(fr);*/
	}

	Matrices::Matrices(BioJeu* bj_, Backend::Generic* backend_)
		: bj(bj_), backend(backend_?backend_:Backend::Factory::create_default(bj_))
		{
			backend->init(bj->NbMarqueur+1);
			DHfunc = bj->HasRH() ? Theta2Ray : Haldane;
		}

}

