#ifndef _CG_2PT_BASE_H_
#define _CG_2PT_BASE_H_

namespace TwoPoint {
	struct Data {
		double LOD;
		double FR;
		Data() : LOD(0), FR(0) {}
		static Data null;
		bool must_compute() const { return (LOD==0)&&(FR==0); }
		void clear() { LOD=FR=0; }
	};
}

#endif

