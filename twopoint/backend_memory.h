#ifndef _CG_2PT_BACKEND_MEMORY_H_
#define _CG_2PT_BACKEND_MEMORY_H_


namespace TwoPoint {
	namespace Backend {
		namespace Memory {
			class Buffer : public Triangular {
				private:
					std::vector<Data> data;
				public:
					Buffer(BioJeu*_) : Triangular(_), data() {}
					~Buffer() { data.clear(); }
					void init(off_t size) {
						/*data.resize(size*((size+1)>>1));*/
						data.resize(mksize(size));
						DBG("init size="<<mksize(size)<<std::endl);
					}
					void set(int i, int j, TwoPoint::Data*d) {
						TwoPoint::Data* dat = &data[mkofs(i, j)];
						dat->LOD = d->LOD;
						dat->FR = d->FR;
					}
					void raw_get(int i, int j, Data**dptr, ComputationHook**hptr) {
						/*std::cerr << "raw_get " << i << "," << j << " @" << mkofs(i,j) << std::endl;*/
						*dptr = &data[mkofs(i, j)];
						*hptr = no_hook;
						/*ComputeSync(i, j, dat);*/
						/*return dat;*/
					}
			};

			class Cache : public Triangular {
				private:
					struct Key {
						BioJeu* BJ;
						unsigned long long offset;
						Key() : BJ(0), offset(0) {}
					};
					struct CacheData {
						Key key;
						TwoPoint::Data data;
						CacheData() : key(), data() {}
					};

					static std::vector<CacheData> cache;

					size_t hash(BioJeu* BJ, unsigned long long offset) {
						/* FIXME devise a REAL hash function */
#						define HASH_PRIME 0xB570F5F
						offset ^= ((unsigned long long)BJ)*HASH_PRIME;
						offset%=cache.size();
						/*std::cout << "hashed to " << offset << std::endl;*/
						return offset;
					}
				public:
					static TwoPoint::Backend::impl::CacheStats stats;

					Cache(BioJeu*_) : Triangular(_) { if(!stats.size()) { setSize(19); } }
					~Cache() {}
					void init(off_t size) {}
					void set(int i, int j, TwoPoint::Data*d) {}
					static void setSize(unsigned int exp2) {
						unsigned long long size;
						if(exp2<=0||exp2>=32) {
							/* FIXME should whine */
							return;
						}
						size = (1<<exp2)-1;
						cache.resize(size);
						stats.set_size(size);
					}
					void raw_get(int i, int j, Data**dptr, ComputationHook**hptr) {
						*hptr = no_hook;
						unsigned long long ofs = mkofs(i, j);
						size_t h = hash(bj, ofs);
						CacheData* cd = &cache[h];
						/*std::cout << "get("<<i<<','<<j<<")%"<<h<<' ';*/
						if((cd->key.BJ==bj)&&(cd->key.offset==ofs)) {
							stats.hit();
							/*std::cout << "hit";*/
						} else {
							(void)(cd->key.BJ?stats.miss():stats.new_value());
							if(cd->key.BJ) {
								stats.miss();
								/*std::cout << "miss";*/
							} else {
								stats.inuse();
								/*std::cout << "new";*/
							}
							cd->key.BJ=bj;
							cd->key.offset=ofs;
							cd->data.clear();
						}
						/*std::cout << std::endl;*/
						*dptr =  &cd->data;
						return;
					}
			};
		}
	}
}

#endif

