%\documentclass[pdf,capsules,slideColor,colorBG]{prosper}
\documentclass[ps,capsules,slideBW,colorBG]{prosper}

\usepackage[latin1]{inputenc}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{color}
\definecolor{limegreen}{rgb}{0.196078,0.803922,0.196078}

\Logo(11.6,6.95){\rotatebox{270}{\usefont{T1}{pag}{b}{n}\small\textcolor{limegreen}{INRA}}}

\newcommand{\cart}{\textsc{\textmd{Car\raise.5ex\hbox{t}\kern-0.64em\lower.57ex\hbox{h}aG�ne}}}

\newcommand\cod[1]{\textbf{\texttt{#1}}}
\newcommand\emm[1]{\textcolor{limegreen}{#1}}

\title{An introduction to \cart}
\subtitle{{\small A genetic and RH integrated mapping tool}}
\author{\href{http://www.inra.fr/bia/T/schiex/}{{Thomas Schiex}}\\
  M. Bouchez, P. Chabrier, C. Gaspin}
\institution{INRA (Toulouse), France}
\slideCaption{Introduction to \cart}

\begin{document}
\maketitle

%----------------------------------------------------------------
\begin{slide}{\bf Genetic and RH maps}
  
  Given a dataset (\cod{dsload}) of genetic/RH data for a given
  population/panel on a given set of markers $M$, a genetic or RH map
  is defined by:
  
  \begin{itemize}
  \item a set of markers $N=\{m_1,\ldots m_n\} \subseteq M$
  \item which is ordered (eg. $m_1 <\cdots < m_n$)
  \item with a distance between each pair of adjacent markers ($d(m_i,m_{i+1})$)
  \end{itemize}
  
  The genetic/RH mapping problem: find a map (order+distances) that
  best explains the data set.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf What is a good map ?}
  
  Non parametric approach: a map that minimizes the number of
  compulsory crossovers/breaks, maximizes the sum of 2-points LOD\ldots
  
  Parametric approach: given a probabilistic model of the underlying
  phenomenon, a map that maximizes the probability of the data
  (likelihood).
  
  Parameters: probability of recombination/breakage between 2 adj.
  markers $\theta_{ij}$ (probability of retention $r$)
  
  \cart\ always uses true multipoint maximum likelihood as the
  criteria to evaluate a given marker sequence.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Probabilistic models in \cart}

  \begin{itemize}
  \item Backcross: as in MapMaker. Dedicated EM.
  \item RIL (sib/self): as in mapMaker. Dedicated EM.
  \item Intercross: as in MapMaker.
  \item Phase know outbreds (1:1, 1:2:1, 1:1:1:1 seg. ratio)
  \item haploid RH: Dedicated EM.
  \item Diploid RH.
  \end{itemize}
  
  The ``Dedicated EM'' code can run more than 2 orders of magnitude
  faster than traditional EM implementation (MapMaker, RHMAP).

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Working with multiple populations}
  
  \begin{itemize}
  \item Consensus mapping (\cod{dsmergen}): one map for all
    populations. Implicit assumption that all populations have the
    same marker ordering and distances. Can be used only for similar
    population types (eg.  backcross with outbreds)
    
  \item Simultaneous mapping (\cod{dsmergor}): one map per
    population. The only assumption is that all populations shares the
    same markers ordering. Can be used to merge eg. genetic and RH
    data.
  \end{itemize}

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Taking into account extra information  \cart}
  
  Some information on the supposedly ``true'' marker ordering can be
  injected in \cart\ using the \cod{constraints} dataset type.\\[5mm]
  
  Composed of triplets of markers $m_i,m_j,m_k$ plus a loglikelihood
  penalty $p$.\\[5mm]
  
  Semantics: maps such that $m_j$ is not in between $m_i$ and $m_k$
  have their loglikelihood penalized by $p$. Only usable with
  \cod{dsmergor}.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Computing linkage groups}
  
  Same as in MapMaker: given a (Haldane/Ray) distance threshold
  $\theta_{\max}$ and a LOD threshold $\ell_{\min}$, put in the same group 2
  ``related'' markers \emph{i.e.}, markers that have both:
  \begin{enumerate}
  \item a pairwise distance below $\theta_{\max}$
  \item a LOD above $\ell_{\min}$
  \end{enumerate}
  
  Catching: 2 unrelated markers can be put in the same group if they
  are related to ``related'' markers (\cod{group},\cod{groupget}).
  
  2-points information is computed when a data set is loaded
  (\cod{dsload}, \cod{mrklod2p}, \cod{mrkfr2p}).

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Ordering markers and the TSP}
  
  Ina group of $n$ markers, there are $\frac{n!}{2}$ differents orders.\\[2mm]

  Under strong hypothesis (eg. BC data with no missing), the maximum
  likelihood marker ordering problem is equivalent to the\ldots\\[2mm]
  
  \textbf{Wandering Salesman Problem}: given $n$ cities and available
  routes connecting cities (with distances). Find a path that goes
  exactly once through each city once and that minimizes the overall distance.\\[2mm]

  One of the most studied optimization problem in computer science.
  Know to be potentially very hard (\textsf{NP}-hard).

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Ordering markers: use TSP link}

  \begin{itemize}
  \item \textbf{exhaustive search}: not possible for $n>8$.
  \item \textbf{building heuristics}: build a ``clever'' map using 2-point
    information.
  \item \textbf{improving heuristics}: improve an existing map using a
    systematic simple mechanism.
  \item \textbf{stochastic search}: improve an existing map by
    stochastic/clever perturbations.
  \end{itemize}
  
  All ``improving'' methods can be used as map checking methods
  (good = cannot be improved).

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Good maps \& the Heap}
  
  A good map is not only a max. likelihood map.  It is a reliable map
  (such that no alternative order has comparable likelihood).\\[5mm]
  
  As far as the set of ``active markers'' (\cod{mrkselset},
  \cod{mrkselget}) is unchanged, \cart\ always remember the $k$ best
  maps explored by any of the ordering process.\\[5mm]
  
  The set of these $k$ best maps is called ``The Heap'' and is central
  in all the mapping process in \cart.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf The Heap}
  
  \begin{itemize}
  \item is browsable (\cod{heaprints}, \cod{heaprintds},
    \cod{heapget}\ldots)
  \item its size $k$ is user adjustable (must be a power of $2$), can
    be huge (eg. 2048) w/o bad cpu-intensive side-effects
    (\cod{heapsize}).
  \item the current best map of the heap is the implicit target of all
    ``order improving'' methods (\cod{heapget 1}).
  \end{itemize}
  
  After sufficient search, the heap provides information on the map
  landscape around the best map (and therefore on its support).

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Looking to the heap}
  
  \cod{heaprinto} $n$ \emph{comp} \emph{blank}:
  
  For each map in the heap, compares the sequence of the markers with
  the best sequence.
  \begin{itemize}
  \item if $n > 0$: only markers that moved are visualized and
    contiguous segments of more than $n$ markers that moved are put in
    brackets.
  \item if \emph{comp} is set, the output is unaligned. This is useful
    when the maps include a large number of markers.
  \item if \emph{blank} is set, the segments which have been moved and
    whose length is $>n$ are only represented by their extremities.
  \end{itemize}
  

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Heuristics building method}
  
  First: one must build an initial map.
  \begin{itemize}
  \item by hand: specify a marker ordering and ask for max. likelihood
    estimation. \cod{markselset} + \cod{sem} : single EM.
  \item using a TSP heuristics (Nearest Neighbor).\\[2mm]
    
    \cod{nicemapl}: uses 2-points LOD (strongest LOD with the last
    inserted marker is inserted).\\[2mm]
    
    \cod{nicemapd}: uses 2-pt distances (markser closest to the last
    inserted marker is inserted).
  \end{itemize}
  
  Warning: 2-points distances/LOD may be undefined/null when merging
  data-sets.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Heuristics building method}
  
  \cod{build} $nb$: 
  \begin{enumerate}
  \item Start from the $nb$ pairs of markers having maximum loglike.
  \item In each of the $nb$ maps, insert the marker with the strongest mean
    LOD in all possible positions.
  \item Select the $nb$ best maps and repeat to 2.
  \end{enumerate}
  
  Old automatic building procedure. Does comprehensive mapping (always
  inserts all markers). Now largely superseded by \cod{buildfw}.
  
\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Framework building method}
  
  Builds reliable but incomplete maps.
  
  \cod{buildfw} $\Delta_{\min}$ $\Delta_{keep}$ $S$ $c$ ($S=\{\}, c= 0$)
  \begin{enumerate}
  \item Start from all possible pairs of markers.
    
  \item For all available maps, a new marker is inserted in all
    possible positions.  The marker ``reliability'' is defined as the
    difference $\delta$ in loglike between the best and the second best
    insertion position. A marker can be inserted only if this
    difference is larger than $\Delta_{\min}$.

  \item From all these new maps, keep only those such that $\delta \geq \Delta_{keep}$.
  \item repeat to 2.
  \end{enumerate}
  
\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Framework building method II}
  
  The process stops when no marker with sufficient quality exists.

  \begin{itemize}
  \item $S$: a marker ordering to start from (rather than all pairs).
    Used to extend an existing ``reliable'' map.
  \item $c=1$: when no marker with sufficient quality exists, tries to
    independently insert all remaining markers in all possible
    intervals.
    
    For each such marker: reports the best insertion position
    (\cod{+}) and how far in loglike all other positions are (support
    for the best position).
  \end{itemize}
  
  The $c$ flag allows to do framework mapping followed by a
  comprehensive mapping of all remaining markers wrt. the framework.
  
\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Heuristics improving methods}
  
  Start from a non empty heap (best taken). All maps considered are
  candidate for the heap.

  \begin{itemize}
  \item \cod{flips} $w$ $\Delta_{\max}$ \emph{Iter}: tests all maps
    obtained by all possible permutations inside a sliding window of
    size $w$.
    
    Reports all permutations that lead to a loglike. within
    $\Delta_{\max}$ of the best loglike.
    
    If an improved map is found and if \emph{Iter} is set to 1, the
    process is reiterated on the new best map.
    
  \item \cod{polish}: each marker in the map is tested in all possible
    intervals. Reports the matrix of the $\Delta$ in loglike.
  \end{itemize}

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Stochastic search: general principles}

  \begin{enumerate}
  \item We start from the best order available in the heap.
  \item We perturbate this order to get a new order (called a
    neighbor). The neighbor chosen may be chosen randomly
    or�''smartly''. The loglike may increase or decrease.
  \item depending on some tests (which may include stochasticity) we
    either ``move'' to this new order or stay were we are.
  \item we repeat from 2.
  \end{enumerate}
  
  The whole process may be repeated several time.
  
  The possible perturbations (the neighborhood) is crucial: use known
  TSP neighborhood.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Stochastic search: general principles}

\includegraphics[width=0.9\textwidth]{pay.eps}

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Neighborhood}
  
  Famous $2$-OPT and $3$-OPT neighborhoods for the TSP adpated for the
  WSP.
  
  \begin{itemize}
  \item $2$-OPT: choose 2 markers and invert the delimited submap.
  \item $3$-OPT: choose 3 markers and swap the two delimited submaps.
  \end{itemize}
  
  \centerline{\includegraphics[scale=0.4]{2change}}

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Simulated annealing}
  
  Exploits an analogy with metallurgy/thermodynamics.

  \begin{itemize}
  \item a state of the system -- a map $m$
  \item the energy of the state -- the opposite of the loglike of the map
  \end{itemize}
  The probability of accepting a state of increasing energy is
  determined by the Boltzmann's distribution.
  
  We start at an initial temperature $T_i$ from an initial map (state)
  $m$ with an energy (opposite of loglike) $-\ell(m)$ and slowly cool
  down the system while perturbating it til it reaches $T_{\min}$.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Simulated annealing}
  
  \begin{algorithm}
    $m\gets$ initial map\;
    $T \gets T_i$\;
    \While{$T > T_{min}$}
    {
      \For{$m=1$ � $m=\mathit{NbTries}$}
      {
        $x' \gets$ RandomNeighbor{($x$)}\;
        $\delta \gets \ell(m) - \ell(m')$\;
        \lIf{$(\delta\leq 0)$}{$m\gets m'$\;}
        \lElse{
          \If{random$(0,1) \leq e^{\frac{-\delta}{T}}$}
          {$m \gets m'$\;}
        }
      }
      $T \gets T.\alpha$\;
    }
    \Return{$(m,\ell(m))$}\;
  \end{algorithm}

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Simulated annealing parameters}

  \cod{annealing} \emph{NbTries} $T_i$ $T_{\min}$ $\alpha$

  \begin{enumerate}
  \item $T_i$ can be chosen arbitrarily. It si automatically adjusted.
    I usually use $100$.  
  \item $LPlateau$ should be larger than $\frac{n.n-1}{2}$
  \item $\alpha$ is close to $1$. This fixes the length of the search
    (fast/slow cooling)
  \item $T_{\min}$ should be small enough to avoid a premature end.
\end{enumerate}

You must play with the parameters $\alpha$ and $T_{\min}$. There is no
clear methodology to set them up.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Taboo search (\cod{greedy})}
  
  Starting from the best map in the heap and for a given number of
  steps:
  \begin{enumerate}
  \item Move to the best $2$-OPT neighbor
  \item unless this move is \textbf{taboo} (has been used recently)
  \item one can nevertheless violate the taboo if the move improves
    over the best known solution.
  \end{enumerate}
  
  This can be repeated several (\emph{NbLoop}) times.

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Taboo search (\cod{greedy})}

  \cod{greedy} \emph{NbLoop} \emph{NbExtra} \emph{TabooMin} \emph{TabooMax} 

  \begin{itemize}
  \item The taboo search is repeated \emph{NbLoop} times (try $1$ first)
  \item The number of moves is autoadjusted but you can give extra
    moves using \emph{NbExtra}. Default: use $0$.
  \item A move is taboo if it has been recently used. The definition
    of ``recently'' varies stochastically during search between
    \emph{TabooMin} \emph{TabooMax}. These are percentages.  Typically
    try $1$ and $20$ but your milleage may vary.
  \end{itemize}

\end{slide}
%----------------------------------------------------------------
\begin{slide}{\bf Final points}

\begin{itemize}
\item Not all the commands are accessible under the graphical interface.
\item But, you may type them in the shell available in graphical interface
\item The shell level includes a complete simple programming langage
  (Tcl).  You can completely automate your mapping strategy.
\end{itemize}

\end{slide}

\end{document}

