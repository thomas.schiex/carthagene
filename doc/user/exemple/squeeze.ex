# load two datasets and find markers which appear in linkage groups
# of size greater than 4 in the two datasets
set mrkselection [multigroup 4 {0.5 0.5} {6 6} {Data/panelRH1.id Data/panelRH2.id}]

# merge the two panels
dsmergor 1 2

# set the current selection of markers
mrkselset $mrkselection

# remove double markers
mrkdouble
mrkmerges

# find a good map
lkhn 1 -1

# remove dubious markers
squeeze {50 60} {lkhn 1 -1}
bestprintd