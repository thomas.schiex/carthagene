#!/bin/sh
#-----------------------------------------------------------------------------
# 
# $Id: tmp2tex.tcl,v 1.12 2010-01-19 10:11:15 dleroux Exp $
#
# Module       : cgsh/example
# Projet       : CartaGene
#
# Description  : resume et texify les exemples.
#
#-----------------------------------------------------------------------------
#-INRA--------------------Station de Biometrie et d''Intelligence Artificielle
#-----------------------------------------------------------------------------
#\
exec tclsh $0 $@

#source ~/.tclshrc
#lappend auto_path "/usr/share/carthagene"
#package require CGsh


set leficid [open  [lindex $argv 0] r]

puts "\{\\small\\begin{verbatim}"

#
# deuxi�me passe
#

set resum 0
set notronc 0

foreach ligne [split [read $leficid] \n] {
       
    set ligne [string trimright $ligne]

    if {[string first  "@debutnotronc" $ligne] == 0 } {
	set notronc 1
    } elseif {[string first  "@finotronc" $ligne] == 0 } {
	set notronc 0
    } elseif {[string first  "@debutresum" $ligne] == 0 } {
	set resum 1
    } elseif {[string first  "@finresum" $ligne] == 0 } {
	set resum 0
    } elseif {[string first  "@debutout" $ligne] == 0 } {
	puts "\\end{verbatim}\}"
	set savenotronc $notronc
	set notronc 2
    } elseif {[string first  "@finout" $ligne] == 0 } {
	set notronc $savenotronc
	puts "\{\\small\\begin{verbatim}"
    } elseif {$resum == 0 && $notronc == 0} {
	if {[string length $ligne] > 78} {
	    set ligne "[string range $ligne 0 74]..."
	}
	puts $ligne
    } elseif {$resum == 0 && $notronc != 2} {
	while {[string length $ligne] > 78} {
	    set tligne "[string range $ligne 0 76]\\"
	    puts $tligne
	    set ligne [string range $ligne 77 end]
	}
	puts $ligne
    } elseif {$notronc == 2} {
	puts $ligne
    }
}

close $leficid

puts "\\end{verbatim}\}"


