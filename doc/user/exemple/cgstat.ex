# we load a dataset
dsload Data/bc.cg
#compute groups
@group 0.1 7
#and select group 4
@mrkselset [groupget 4]
# compute a first map
@nicemapd
# check the quality of this order
@flips 2 0 0
#check some statistics and ask for the number of maps within 1 of the optimum
cgstat 1