dsload Data/rh1_true.cg
dsload Data/order1_ref.cg
dsmergor 1 2
#speed up likelihood computation (2-point mode)
cg2pt 1
#build a Pareto frontier from scratch
@paretolkh 10 1 0
#restore multipoint likelihood computation
cg2pt 0
#recompute multipoint likelihood estimates for every map in the heap
cgtolerance 0.0009 0.09
#improve the Pareto frontier only near the current best map
@paretogreedyr 25 4 8
#show the best map in the improved frontier, in details
paretobestprintd 1
