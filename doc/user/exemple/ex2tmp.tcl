#!/bin/sh
#-----------------------------------------------------------------------------
# 
# $Id: ex2tmp.tcl,v 1.9 2010-01-18 15:27:20 dleroux Exp $
#
# Module       : cgsh/Example
# Projet       : CartaGene
#
# Description  : execute les exemples "au format .ex"
#
#-----------------------------------------------------------------------------
#-INRA--------------------Station de Biometrie et d''Intelligence Artificielle
#-----------------------------------------------------------------------------
#\
exec tclsh $0 $@

lappend auto_path "/usr/share/carthagene"
package require CGsh

puts "@debutresum"
#source ~/.tclshrc
#source ~/.carthagenerc
puts "@finresum"
set leficid [open  [lindex $argv 0] r]

#
# premi�re passe
#
set mode 0

foreach ligne [split [read $leficid] \n] {

    if {[string first  "(" $ligne] == 0 } {
	puts "@debutout"
	set mode 1
    } elseif  {[string first  ")" $ligne] == 0 } {
	puts "@finout"
	set mode 0
    } elseif {$mode == 1} {
	puts $ligne
    } elseif {[string first  "\#" $ligne] == 0 } {
    # cas des commentaires    
	puts $ligne
	# cas des commandes � resumer
    } elseif {[string first  "@" $ligne] == 0 } {
	set filigne [string range $ligne 1 end] 
	puts "CG> $filigne"
	puts "..."
	puts "@debutresum"
	eval $filigne
	puts "@finresum"
	# commands to hide
    } elseif {[string first  "%" $ligne] == 0 } {
	set filigne [string range $ligne 1 end] 
	puts "@debutresum"
	eval $filigne
	puts "@finresum"
	# cas des lignes � ne pas couper
    } elseif {[string first  "\\" $ligne] == 0 } {
	set filigne [string range $ligne 1 end]
        puts "@debutnotronc"
	puts "CG> $filigne"
	puts [eval $filigne]
	puts "@finotronc"
    } else {
	# affichage de la commande    
	puts "CG> $ligne"
	# evaluation de la commande
	puts [eval $ligne]	
    }
}
#
# deuxieme passe resumons certaines commandes
#

close $leficid



