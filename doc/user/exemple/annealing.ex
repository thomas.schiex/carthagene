# we first load a dataset
dsload Data/rh.cg
# perform linkage group detection (output omitted)
@group 0.3 3
# select group number 10
mrkselset [groupget 10]
# put a (stupid) map in the heap
sem
# use a (fast) annealing command 
annealing 100 100 0.1 0.9
# we check the map with a large flip
flips 9 0 0
# the map found was indeed optimal