# we first load a data set
dsload Data/bc.cg
# and ask for markers that can be merged
mrkdouble
# then merge markers 9 and 10 in 9.
mrkmerge [mrkid MS9] [mrkid MS10]
#...
mrkmerge [mrkid MS12] [mrkid MS13]
mrkmerge [mrkid MS12] [mrkid MS14]
#retrive the information
mrkmerget