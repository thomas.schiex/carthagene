# starting with a heap of size 7
heapsize 7
# filling it
dsload Data/bc1.cg
\sem
@flips 3 3.0 1
\heaprint
# keeping the 3 best one.
heapsize 3
\heaprint
