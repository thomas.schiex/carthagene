help
(
\section{Loading data}
\label{ex1}

Now we will begin by loading a dataset. The corresponding command is
\texttt{dsload}. If you want to learn more about this command, you can
use the ``-H'' flag:
)
dsload -H
(
To load the first population (it should be available in the
\verb|Data/mouse.raw| file\footnote{This file has been extracted from
  the MapMaker 3 distribution.}~:
)
dsload Data/mouse.raw
(
Upon loading, CarthaGene computes all the two-points statistics (LOD,
distances...) and returns the id of the population, here 1, the type
of the population (intercross), the number of individuals (46) and the
number of markers (308).

We can now have a look to the the populations loaded using the
\texttt{dsinfo} (dataset information) command.
)
dsinfo
(
We can check that the population number 1, the last loaded, is the
current population.

We can now give a look to the markers that have been mentionned in the
loaded dataset.  To display the loci, their numerical id and the data
set they come from, we can use the \texttt{mrkinfo} command:
)
@mrkinfo
(

\section{Linkage groups and 2-Points estimations}
\label{groups}

In CarthaGene up to and including version 1.2, all the two points
information (LODs, distances, recombination ratios\ldots) is computed
at load time. The 1.3 beta version is smarter and will compute this
information lazily, cache it in a file so that it will not be
recomputed each titime and use a multithreaded algorithm to do so.

When two points information is available, we can directly try to group
the markers in linkage groups. This is done using the \texttt{group}
command that specifies a distance and a LOD threshold: two markers
whose 2 point distance (Haldane/Ray) is below the given distance
threshold and whose 2 points LOD is above the LOD threshold will be
put in the same linkage group. Users that don't want to use one of the
two thresholds can simply set the threshold they want to ignore to an
extreme value (distance threshold set to zero, LOD threshold set to an
arbitrary large number).

Let's try this with a 30cM distance threshold and a $3.0$ LOD
threshold:
)
group  0.3 3.0
(
In this case, we get 23 groups. In the sequel we will work on the
group 10. To focus on this specific group, we will get the list of the
markers in the group using the \texttt{groupget} command and then
select them using the \texttt{mrkselset} command. This can be achieved
easyly in the graphical interface with a few clicks. If you use the
shell, do as follows:
)
groupget 10
(
To automagically select the markers of the group, one can use the
following syntax (using $[$ and $]$) that simply replaces what appears
between $[$ and $]$ with the result of the command in between (this is
called a macro-expansion). We can therefore select the group 10 with
the following command:
)
mrkselset [groupget 10]
(
Note that the current marker selection is not only a set of markers
but also a default markers ordering. We can look at the LOD matrix
beetween each pair of markers using the \texttt{mrklod2p} command.
)
mrklod2p
(
Two-points LOD are also used in the \texttt{mrkdouble} command which
detects pairs of markers that have \emph{compatible} genotypes on each
individuals. Such pairs of markers should be merged in one marker to
simplify the search for an optimal map. See the \ref{mergm} section on
this topic. In practice, two markers can be compatible on all
individuals and nevertheless be unlinked. In this case they should not
be merged together. 
)
mrkdouble
(
You see that 5 pairs of markers are ``double markers''.   We will ignore
this issue for now and work on the whole group.

We can also have a look to the 2-points distance matrix using the
\texttt{mrkdist2p} command. Haldane (\texttt{h}) or Kosambi
 (\texttt{k}) can be used. Ray distance is automatically selected for
radiated hybrid data (whether you specify \texttt{h} or \texttt{k}).
)
mrkdist2p h
(
You can also look to 2-points recombination ratio (breakage ratio for
RH data) using \texttt{mrkfr2p}:
)
mrkfr2p
(
\section{Building maps from scratch}

We can now start to try to build maps. All the maps built go in a
specific storage structure called the ``heap'' that remembers the $k$
best maps found during all the map search process. To build a first
map in order to get a first map in the heap, we will simply directly
ask CarthaGene to assess the quality of the default order specified in
the \texttt{mrkselset} command. This is done using the \verb|sem|
command. This procedure compute true multipoint maximum likelihood of
the current order and prints the corresponding map (the markers order
used for printing can be reversed w.r.t. the marker selection. See
section~\ref{maprintdr}).
)
sem
(
To try to build less stupid maps, we can try to use heuristics
building procedures. The two simple procedures \texttt{nicemapl} and
\texttt{nicemapd} build reasonably good maps using respectively
2-points LOD and 2-points distances as guide (trying to put strong
LOD/small distances close together). The two slightly more complex
procedures \texttt{mfmapl} and \texttt{mfmapd} tend to provide better
results. Both classes of heuristics are derived from usual travelling
salesman problem heuristics. In all cases, the true multipoint maximum
likelihood of the order is then computed and the map printed.
)
nicemapl
nicemapd
(
The loglikelihoods of the maps found using these two heuristics are
-72.96 and -70.86 respectively.  We can have a closer look to the maps
build up to this point by asking for a detailed view of all the maps
stored in ``the heap''. This is achieved using the \texttt{heaprintd}
command:
)
heaprintd
(
So far there are only our 3 maps in the heap. We can try to build
other maps using a smarter heuristics procedure called \texttt{build}.
This command incrementally includes markers, always choosing the best
loglikelihood and the best insertion point. Because it is too
``greedy'' in its choices, this procedure can be performed in parallel
on several maps, always keeping the $k$ best maps. So, the command
takes one parameter $k$ to specify the number of map built at the same
time.
)
build 10
(
No better map was found. We may now shift to so-called ``improving''
methods. These methods cannot start from scratch and are dedicated at
improving available maps.

\section{Map improving methods}

As a first trial, we can use the local search algorithms. In this
example, we will use the ``simulated annealing'' algorithm embodied in
the \verb|annealing| command. The command takes some parameters to
specify how the search takes place (and how much cpu-time it will
consume). Here we use 100 moves per level (the larger the better the
search and the more expensive it is), an initial temperature of 50 (this
is automatically reajusted), a final temperature of 0.1 (the
smaller, the better the search and the more expensive too) and a
cooling factor of $0.8$ (always strictly less than $1$, the closer to
$1$, the better the search and the more expensive too). This a very
fast search (use other parameters in practice).
)
annealing 100 50 0.1 0.8
(
When the annealing process improves over the best known solution, a
``+'' is printed. In our case, no better map could be found. Now we
would like to see the best maps found (which are stored in the heap),
ordered by loglikelihood:
)
heaprint
(
The best map is always the last map. We see that on this data set
there are several maps with different orders and a close (or equal)
likelihood. This is not surprising if you remember that the
\texttt{mrkdouble} command detected several ``double'' markers.
Usually, swapping 2 ``double markers'' in a map will not change the
loglikelihood.

Another local search algorithm is the ``taboo search'' algorithm
embodied in the \verb|greedy| command. The command takes some
parameters to specify how the search takes place (and how much
cpu-time it will consume). Here we use 3 loops (the larger the better
the search and the more expensive it is), an additional number of
iterations of 0, the minimum size of the taboo list is 1\% of the
neighborhood and the maximum size of the list is 15\%.
)
greedy 3 1 1 15 0
(
The algorithm prints a '-' when the map could not be improved, a ``+''
when the best map has improved and a ``*'' to say that the serach
process has detected that it is stuck in a region it has already
explored. In this case, it starts from another random order. Here,
again, no improvement was obtained. Note that the map improving
commands can be used as map validating commands using the
\texttt{cgrobustness} command (see~\ref{cgrobustness}).

\section{Order validating methods}

We can try to test how good this map is using a ``verification
algorithm''. The usual method of flipping markers inside a window 
 (\texttt{ripple} command in mapmaker) is called \texttt{flips} in
CarthaGene.  It takes 3 parameters: the size of the flipping window, a
printing threshold on the difference of loglikelihood with the best
map and a last flag that says if the flips command must be reiterated
if a better map is found.  Here we will use a window of 4 markers, all
maps whose loglikelihood is better or within 1.0 LOD unit of the
loglikelihood of the best map will be printed and the flips command
will be reiterated on the new improved map if such a map is found.  
)
flips 4 1 1
(

Here we see that by flipping markers in the original best map, we have
not improved the loglikelihood. We also see that several alternative
orders exist. As said before, this is due to the existence of double
markers. For example, a line such as \verb|[1 0 - -]- - - - - - - - -- - -0.00|
says that by swapping the first and second markers of the
map we don't change the likelihood. These are markers 20 and 38 (top
rows). We can ask for the name of these 2 markers:
)
mrkname 20
mrkname 38
(
These two markers were effectively detected as double by the
\texttt{mrkdouble} command. Ideally, all the previous searches should
be repeated after merging the double markers which are strongly
linked.

Another validation procedure is the \texttt{polish} command. It takes
each marker successively and tries to insert it in all possible
intervals. The variation in loglikelihood is reported for each marker
and each destination interval.
)
polish 
(
We can see again the `double'' markers effect (with $0.0$ LOD
differences). Otherwise, all markers seems to be relatively firmly
placed with high LODs, all above $2.1$.

Let us have a look in detail to the best map, the map 12.
)
maprintd 12
(
This map will be kept as the best comprehensive map of the group in this
tutorial. In practice, longer local search algorithm must be performed and other
verification procedures should be used. 

\section{Building framework maps}

Framework maps are maps that are supposedly firmly ordered (weakly
ordered markers are not included). Such maps can be build by
CarthaGene using a stepwise marker insertion method called
\verb|buildfw|. 

This command uses an incremental insertion method. It tries to insert
non inserted markers in a current map. For a given marker that could
be inserted, it tries to insert the marker at all possible position of
the map. The difference in loglikelihood between the best insertion
and the second best insertion is used to qualify the marker.  The
marker inserted in practice is the marker that maximizes this
difference and such that this difference is larger than a first
threshold (Adding Threshold). The marker is inserted at its optimal
position. However, if there are orders whose difference in
loglikelihood with this best position is less than a second threshold
 (the Keep Threshold), they are also kept as possible new starting
point for the next marker to be inserted. 

Note that the build process may start from an empty map or from an
existing order (that must contain at least 3 markers). Finally, when
all insertable markers have been inserted, the map is kept aside and
the remaining marker are tentatively inserted, one by one, in all
possible positions in this map and then removed. For each such marker,
the command reports how the loglikelihood changes wrt to the best
inseryion point. The best position is marked with a ``+''.  The
difference in loglikelihood between the best position and each
position are printed out if this difference is less than a threshold.
When the last parameter of \texttt{buildfw} is set to 0, this last
step (testing all remaining loci) is not performed. More complex
postprocessing are possible, see the reference user documentation.

Let us test this command on our data. We will use a Keep and Adding
threshold of $3.0$ LODs, starting the build process from the empty map
and trying to position remaining markers.
)
buildfw 3 3 {} 1
(
Here the map produced contains only 7 markers.  The additional
uninserted markers list (A059, D022, M030, M076, M034, T018, L078,
L010) contain all the 5 ``possible double markers'' identified by the
\texttt{mrkdouble} command which is good news.  The final map produced
is automatically used as the best map. It is supposed to be firmly
ordered as we can check with a \verb|flips| command:
)
flips 6 3.0 1
(
which finds no better order at $3.0$ LOD units.

We can check with a \verb|polish| command:
)
polish
(
which finds no better order at $3.0$ LOD units.

To check the quality of the map even more thoroughly, we can use map
improving commands such as \texttt{annealing} or \texttt{greedy}. In
the process of trying to improve the current framework map, these
commands may identify a map whose loglikelihood, if not better than
the current map's loglikelihood, may still be close enough to prove
that the current map is not a valid framework map. In order to stop
the search process as soon as such a map is found, we can use the
\texttt{cgrobustness} command. The threshold given to the command
indicates that every search should stop as soon as a map is found
whose loglikelihood is within the threshold of the best map's
loglikelihood.  
)
cgrobustness 3.0
(
 We now try to improve the map using the greedy command.
)
greedy 1 0 5 30 
(
which does not find any counter-example that proves that our map is
not a framework map. We now remove the previous threshold using the
\texttt{cgnorobust} command:
)
cgnotrobust
(

\section{Merging data sets}

Now, we want to merge two different dataset (populations) into one.
Two types of merging are available:
\begin{itemize}
\item \texttt{dsmergen}: merges the two dataset completely. For a
  given pair of successive markers in the order, the probabilistic
  model for the data has one single recombination ratio estimation for
  the 2 datasets. This produces consensus maps with consensus
  distances\footnote{When used on two data sets of the same type, this
    merging process corresponds to the addition of information on new
    individuals (on new or known markers). It must not be used when
    new markers are types on existing individuals. In this case, the
    data-set file must directly be edited by adding new lines for the
    new markers (and changing the number of markers in the header).}.

\item \texttt{dsmergor}: merges two datasets by order only. For a given
  pair of successive markers in the order, the probabilistic model for
  the data has one single recombination ratio estimation for each of
  the 2 datasets. This produces consensus orders with per data set
  distances.
\end{itemize}

In this tutorial, we will merge two data sets by ``order'' which is
the most complex case. The merge by order process is used because we
want to merge genetic recombination data and radiated hybrid data and
there is no simple way of relating the 2 distances (RH data being
essentially physical). Actually, genetic and RH data nicely complement
each other for \emph{ordering} markers. Genetic data leads to myopic
ordering: set of close markers cannot be reliably ordered because
usually no recombination can be observed between them. On the contrary
RH data leads to hypermetropic ordering: set of closely related
markers can be reliably ordered but distant groups are sometimes
difficult to order because too many breaks occurred between them. The
data used here is coming from a single group representing one porcine
chromosome.

First, let us examine each dataset independently and see what the
\texttt{buildfw} command can produce from each of these. We first
reinitialize CarthaGene using the \texttt{cgrestart} command (to keep
markers numerical Ids low) and load the new data-set:
)
cgrestart
dsload Data/bc1.cg 
buildfw 3.0 3.0 {} 1
(
The BC dataset is a good quality dataset. Few markers could not be
placed in the framework map. The ``quality'' of this map should
naturally be checked more thoroughly using other ``validating'' and
``improving'' commnds. We now perform the same analysis on RH data:
)
dsload Data/rh1.cg 
buildfw 3.0 3.0 {} 1 
(
Since radiated hybrid data usually have very few missing information,
it is possible to use specific ordering methods to build very good
maps. Several commands in \cart\ can directly translate a RH dataset
into a specific travelling salesman problem instance such that the
optimal solution of this TSP instance will also yield the optimal
map. This translation~\cite{bendor00} is faithful when there is no
missing information but is still very effective when few missing
exists. \cart\ automatically reevaluates all the maps generated using
multi-point maximum log-likelihood. Here we will try to reorder the
framework map we built using the \texttt{lkhn} command (lk stands for
Lin-Kernighan heuristics~\cite{Lin.et.al73,Helsgaun00}, a famous
algorithm used to solve travelling salesman problem instances.):
)
lkhn 1 1
(
The same map with a $-125.81$ loglikelihood is found which is a very
good indication that the order is indeed optimal.

The RH dataset is less order informative than the BC dataset. However,
merging the two data sets will help in ordering the markers. Because
we are merging populations with not directly related parameters (RH
and genetic distances), we will use the \verb|dsmergor| command.  The
command takes two parameters that specify the numerical id of the two
populations to be merged. To see the numerical ids, we can use
\texttt{dsinfo}.
)
dsinfo
dsmergor 1 2
dsinfo
(
To better see how markers are shared between the 2 populations, we
can use the \texttt{mrkinfo} command:
)
@mrkinfo
(
We can now try to build a framework map for the hybrid genetic/RH data set. 
)
buildfw 3.0 3.0 {} 1
(
Thanks to RH data, a new genetic marker that could not previously be
inserted is now sufficiently strongly ordered to be inserted.
Furthermore, all new RH markers have been inserted. The final
insertion map also shows that MS2 is replaced by MS3. The two markers
are probably close one to the other and cannot both be inserted,

As mentioned earlier, further work should include a thorough
validation of this order.

\section{Dealing with ``double markers''}
\label{mergm}

In one of the previous examples (section~\ref{ex1}), we noted that
several markers were identified as ``double markers'' by the
\texttt{mrkdouble} procedure. These double markers corresponds to pairs
of markers such that the observed genotypes are compatible with the
assumption that the 2 markers are the same. There are two cases where
such a situation may occur:
\begin{itemize}
\item the two markers are effectively closely related (or the same) on
  the chromosome. In this case it is a good idea to try to merge them
  in one since this will simplify the ordering process.
\item the two markers are totally unrelated markers in two different
  populations/panels. In this case, it is not a good idea to merge
  them. Such markers can often be placed using multipoint maximum
  likelihood.
\end{itemize}

To distinguish between these two cases, \texttt{mrkdouble} indicates
the 2-points LOD between the pair of potential double markers. If this
LOD is small, then the two markers should not be merged in one.

In the \texttt{mouse.raw} dataset, all pairs of double markers have
strong LODs. Let us reload that file, perform grouping and select
group 10 has we previously did: 
)
@dsload Data/mouse.raw
@group  0.3 3.0
@mrkselset [groupget 10]
(
If you issue the \texttt{mrkdouble} command, you should see the
following:
)
mrkdouble
(
Let's merge \texttt{A079} and \texttt{M030} together. This can be
achieved using the mrkmerge command. The command actually merges the
obervation of both markers into one and leaves the other unchanged.
The command takes the numerical id of the 2 markers as arguments. If
you don't know this numerical id, the command mrkid can be used as in
the example below.
)
mrkselget
mrkmerge [mrkid A079] [mrkid M030]
(
Automatically, the marker 132 has been removed from the list of
selected markers:
)
mrkselget
(
The same can be done with each pair of markers detected as double
markers. Note that it is possible that after merging two markers, some
previously detected double markers become separated (eg. on BC data,
one individual, if marker M1 is typed ``1'', marker M3 is type ``0''
and marker M2 is typed ``-'', then M1-M2 and M2-M3 are two pairs of
``double markers'' but once M1 and M2 are merged, then M3 cannot be
merged with the (m1-M2) pair). Although this is an unlikely situation,
CarthaGene checks this in practice and will check compatibility before
merging. In the example, all pairs can be merged without problem.
)
mrkmerge [mrkid L029] [mrkid L010]
mrkmerge [mrkid A036] [mrkid M034]
mrkmerge [mrkid M237] [mrkid M076]
mrkmerge [mrkid T035] [mrkid L078]
mrkselget
mrklod2p
(

We can now try to build a comprehensive map. In this simple case, we
just try the \texttt{nicemapd} command followed by a \texttt{flips 5 0
0}.
)
nicemapd
flips 5 0 0
@heaprintd
(
The best map found can be printed using \texttt{heaprintd}. The best
map has number 7. We can later visualize it using the \texttt{maprintd}
command:
)
maprintd 7
(
You can see that the merged markers are indicated on the map. Looking
onto the heap for alterated ordering, the situation has much improved
with respect to the previous situation (where several orders with
identical log-likelihood existed). We can have a look to the situation
using the \texttt{heaprinto} command:
)
heaprinto 3 0 0
