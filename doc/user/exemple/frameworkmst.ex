dsload Data/rh02_true.id

# merge markers with similar RH vectors
mrkmerges

# build a framework map such that any other map is 100 less likely
# and update the current marker selection to this framework map
frameworknmst 2 1
