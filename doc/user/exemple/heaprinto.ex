dsload Data/bc1.cg
dsload Data/rh1.cg
dsmergor 1 2
sem
@flips 5 3.0 1
# To print all the positions.
heaprinto 0 0 0
# To see consecutive differences of length greater than 3.
heaprinto 3 0 0
# To see the ends of the consecutive differences only.
heaprinto 3 1 0
# To make the line shorter.
heaprinto 3 1 1