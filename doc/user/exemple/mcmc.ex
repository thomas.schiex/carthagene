dsload Data/rh12_true.cg
dsload Data/order12_ref.cg
dsmergor 1 2

#select the comparative mapping criterion
dsbplambda 2 1 1

#find a first good map
lkhn 1 -1

#compute map distribution starting from this initial map
#see result in ProcessID.mcmc output file
mcmc 1 1000 500
