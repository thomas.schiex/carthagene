dsload Data/rh1_true.cg
dsload Data/order1_ref.cg
dsmergor 1 2
#select the comparative mapping criterion
dsbplambda 2 1 1
#find a first good map
lkh 1 -1
#try to improve this map (could be very slow depending on the last parameter value)
greedy 1 0 5 10 5
#show the best map in the heap
bestprint
