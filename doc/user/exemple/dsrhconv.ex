#we load a RH dataset 
dsload Data/rh1.cg
# we realize we want to deal with errors. We look for the dataset number
dsinfo
# and create a new dataset with the error model
dsrhconv 1 error 
# the number is the data set number of the new data set
