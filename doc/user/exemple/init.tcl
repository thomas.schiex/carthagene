#!/bin/sh
#-----------------------------------------------------------------------------
# 
# $Id: init.tcl,v 1.3 2010-01-18 15:27:20 dleroux Exp $
#
# Module       : cgsh/example
# Projet       : CartaGene
#
# Description  : pour utiliser CartaGene directement � travers tclsh.
#
#-----------------------------------------------------------------------------
#-INRA--------------------Station de Biom�trie et d''Intelligence Artificielle
#-----------------------------------------------------------------------------
#
# to load Cartagene
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! Next line is to modify to mention the path of the library of cartagene.!!
# !!the library should be located under the directory lib of the distibution!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#load ~/carthagene/cgsh/libcartagene.so
#\
exec carthagene $0 $@

#
# to make easier to use
# to make the commands visible
namespace inscope CG namespace export *
# to use without prefix
namespace import ::CG::*

#
# to pass to the unrestrained state
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! Next line is to modify to set the key.!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
::CG::cglicense CFRYHFJDZKRGWN

