# a very small heap for the example
heapsize 1
dsload Data/bc1.cg
sem
# we fill the heap with the best map
@flips 3 0 0 
# and look at it
heaprintd
# same thing with maps merged by order
dsload Data/rh1.cg
dsmergor 1 2
sem
@flips 3 3.0 1
# each map is a pair of map
heaprintd