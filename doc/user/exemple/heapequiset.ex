# we load and merge two datasets
@dsload Data/bc.cg
@dsload Data/rh.cg
@dsmergor 1 2
# we evaluate two different but equivalent orders
mrkselset {17 29 30 18 39 19}
sem
# we move the marker 18 which is only defined in the first dataset
mrkselset {17 29 18 30  39 19}
sem
# the two orders have entered the heap
heaprint
# we now activate the detection of equivalent orders
heapequiset 1
# the equivalent order is expunged
heaprint
# we evaluate a third equivalent order
mrkselset {17 18 29 30  39 19}
sem
# it did not entered the heap
heaprint
