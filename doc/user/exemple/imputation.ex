# we load an RH dataset with the error header set
dsload Data/rh1-error.cg
# we assume we know the markers order. We compute a map
sem
# we then ask for a corrected version. This clones the dataset, then imputes the errors.
imputation 1 0.1 0.5 0.9
# finally we can save this newly created dataset
# (the given 'imputation.cg' will be appended to the filename as an extension, see dsave)
dsave 2 imputation.cg
