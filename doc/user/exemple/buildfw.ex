# we first load a data set
dsload Data/rh.cg
# we perform linkage group detection (output omitted)
@group 0.3 3
# we select the group  10
mrkselset [groupget 10]
# we build a framework map for this group
buildfw 3 3 {} 1
# and check its reliability (any map at less than 3 LOD ?)
flips 4 3 0
# no.