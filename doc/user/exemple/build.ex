# we first load a data set 
dsload Data/rh.cg
# we perform group detection (output omitted)
@group 0.3 3
# we select group 10
mrkselset [groupget 10]
# we use the command build (20 parallel maps)
build 20
# and check the quality of the final map
flips 9 0 0
# it should be Ok.