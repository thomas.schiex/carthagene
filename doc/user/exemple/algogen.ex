# we load a dataset
dsload Data/rh.cg
# perform linkage group detection (output omitted)
@group 0.3 3
# we select the group 10
mrkselset [groupget 10]
# put one (stupid) map in the heap using the 'sem' command
sem
# launch the 'algogen' command
algogen 10 8 1 0.6 0.1 1
# check the reliability of the result with a large flip
flips 9 0 0
# the map was optimal