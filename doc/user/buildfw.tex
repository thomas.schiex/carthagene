%-----------------------------------------------------------------------------
%                            CarthaGene
%
% Martin Bouchez, Patrick Chabrier, Christine Gaspin and Thomas Schiex. 
% Unite de Biometrie et Intelligence Artificielle, Toulouse, France
%
% Copyright 1997 INRA (Institut National de la Recherche Agronomique).
% This file is distributed under the terms of the Q Public License version 1.0.
% 
% $Id: buildfw.tex,v 1.11 2003-08-26 14:48:10 tschiex Exp $
%
% Description : documentation utilisateur
% Divers : 
%-----------------------------------------------------------------------------
\label{buildfw}

Tries to heuristically build a good framework map, \textit{i.e.}, a
map such that all alternatives orders have a log-likelihood not within
a given threshold of the framework map. The command may also be used
to build comprehensive maps (including all markers) by adjusting
thresholds.
    
\paragraph{Synopsis :}

The CarthaGene {\tt buildfw} command is invoked as either one of :
\begin{itemize}
\item {\tt buildfw} {\it Options}
\item {\tt buildfw} {\it keepThres} {\it AddThres} {\it MrkList} {\it MrkTest}
\end{itemize}

\paragraph{Description :} The procedure is an incremental insertion
procedure. It starts either by selecting a triplet of loci or by using
the list of loci \textit{MrkList}.  The difference in log-likelihood of
the two best maps you can build with three given loci is used as
criterion. The greater the difference the better the triplet. 

Then, each available locus is tentatively inserted at each possible
position.  If there is no locus that provides a difference in
log-likelihood larger than the \textit{AddThres} threshold, the
algorithm stops.  If there are many orders for which the difference of
log-likelihood is larger than the \textit{KeepThres} Threshold, they
are kept for the next step. The \textit{MrkTest} flag indicates if
post-processing is applied. If it is set to 0 no post-processing occurs.
If it is set to 1, each remaining locus is tentatively inserted at
each position in the previously built framework map and the difference
in log-likelihood between each possible insertion and the best one
(denoted by a '+') is reported. If it set to 2, no framework is built
in the previous stage, instead the \textit{MrkList} list of loci is
used and the same post-processing as above applies.  If the
\textit{MrkTest} equals 0 or 1, the framework's markers become the
list of currently active markers.

When \textit{MrkTest} flag is different from 0, further information on
the remaining markers that have not been integrated in the map is
provided in a table. Each remaining marker is represented on a line.
The information is displayed in 6 group of columns.


{\scriptsize\begin{verbatim}
BuildFW, remaining loci test :

      |                  |
      |                  |     Lod2pt         Dist2pt
      | 1 2 3 4 5 6 7 8  |  Left<-M->Right Left<-M->Right | 0->N    N->M | Weight Nb:W<AM  Id  | Name
    --|------------------|--------------------------------|--------------|---------------------|-------
M1702 |              + 0 |   2.16   23.97   115.1   2.5   |  680.8 106.7 |     0     2     9   | M1702
\end{verbatim}}

From left to right, we get:
\begin{itemize}
\item the first group of columns displays the name of the marker that
  has been tentatively inserted (M1702 above)
\item the second group of columns displays the result of inserting
  this marker in the framework map. The best possible insertion
  position is reported by a ``\texttt{+}''. When alternative insertion
  positions exists with a small difference in log-likelihood (less
  than \textit{KeepThres}), they are also reported by the nearest
  integer that represents this difference (eg ``\texttt{0}'' above
  corresponds to a difference in log-likelihood between in $[0,0.5[$).
\item the third group of columns indicates the relations of the
  inserted markers with its two neighbors in the best insertion
  position. The two-points LOD and the two-points distances (Haldane
  or centiRay depending on the dataset type) with the two flanking
  markers are displayed (when available). 
  
  When merged datasets are used, the two-points distances reported are
  computed only on informative data-sets and averaged among the
  informative data-sets. When RH data is used, and because different
  irradiation level (or mixed RH/genetic data) may have been used,
  distances are not averaged: only the first dataset merged is used.
  
\item the fourth group of columns aims at positioning the remaining
  markers wrt. the framework map. It first (\texttt{0->N}) indicates
  the multi-point distance from the left origin of the map to the left
  flanking marker estimated on the original framework map (before
  marker insertion).  If the marker best insertion is at the extreme
  left of the map, a \texttt{-} is displayed. Then (\texttt{N->M}), an
  estimation of the distance from this left flanking marker to the
  marker inserted is reported. This distance is simply computed by
  rescaling the multi-point distance between the two flanking markers
  using the ratio between the two-points distances from the inserted
  marker to the left and right flanking markers reported in the
  previous group of columns. If either the left or right flanking
  markers are undefined, the two-point distance alone is reported. If
  there is no available information on either of the flanking markers,
  \texttt{NA>} or \texttt{NA<} is printed (indicating the marker that
  generates the indetermination).

\item the $5^{th}$ group of columns successively reports:
  \begin{itemize}
  \item \texttt{Weight}: the sum of the difference of log-likelihood
    with the best map for all maps within \textit{KeepThres} of the
    best map. A large weight indicates a marker whose position is not
    well defined.
  \item \texttt{Nb:W<AM}: the number of insertion position where the
    difference in log-likelihood exceeds \textit{KeepThres}.
  \item \texttt{Id}: the marker numerical Id.
  \end{itemize}
\item the last column gives the name of the inserted marker again
  (same as first column).
\end{itemize}


%4d   column: Two values displayed :
%             the better place for inserting the marker M in the framework is: 
%               On left on the map ( M1702 |+               0 | ...)
%                 if the first marker of framework is'nt compatible
%                    0->N = Lend
%                    N->M = NA<
%                 else
%                     if at the same place of first marker in the framework
%                       0->N = Dist2pt(M,N+1)
%                       N->M = 0
%                     else
%                       0->N = -
%                       N->M = Dist2pt(M,N+1)
%               On right on the map ( M1702 |               0 + |...)
%                 if the last marker of framework is'nt compatible
%                    0->N = Rend
%                    N->M = NA>
%                 else
%                     if at the same place of last marker in the framework
%                        0->N = Dist2pt beetween the first marker of the map and N
%                        N->M = 0
%                     else
%                        0->N = Dist2pt beetween the first and last marker in the framework
%                        N->M = Dist2pt(N,M)
%               In the framework  ( M1702 |              + 0 |...)
%                 if the marker N is'nt compatible
%                    if the marker N+1 is'nt compatible
%                       0->N = -
%                       N->M = -
%                    else
%                        0->N = Dist2pt(M,N+1)
%                        N->M = NA<
%                 else
%                    if the marker N+1 is'nt compatible
%                       0->N = Dist2pt(N,M)
%                       N->M = NA>
%                    else
%                       if at the same place of marker N
%                          0->N = Dist2pt(N,M)
%                          N->M = 0
%                       else
%                         if at the same place of marker N+1
%                            0->N = Dist2pt(M,N+1)
%                            N->M = 0
%                         else
%                            0->N = Dist2pt(N,M)
%                            N->M = DistMpt(N,N+1)*Dist2p(N,M) / Dist2p(N,M)+Dist2p(M,N+1)
%5d   column: Sum( deltaLOD beetween best map and current map) when deltaLOD <= AM
%             Number o of above
%             Number of marker to be inserted


\paragraph{Arguments :}
\begin{itemize}
\item {\it Options} : \texttt{-u} to obtain the synopsis of the normal
  use, \texttt{-h} to print a one line description, \texttt{-H} to
  print a short help. 
\item \textit{KeepThres}: the minimum difference in log-likelihood
  between the best insertion point and the second best insertion point
  required for the map to be considered in the future.
\item \textit{AddThres}: the minimum difference in log-likelihood
  between the best insertion point and the second best insertion point
  required for a locus to be insertable. This threshold is also used
  to filter out the differences in log-likelihood reported by the
  postprocessing option (only differences lower than the threshold
  will be reported).
\item \textit{MrkList}: an order of markers to start from (may be
  empty or caontain at least 3 markers).
\item \textit{MrkTest}: a flag to select either framework mapping only
  (0), framework mapping followed by a postprocessing (1) or just the
  post-processing applied to (2)\textit{MrkList}.
\end{itemize}

\paragraph{Returns :} nothing but the set of active markers is
automatically set to the framework map built if the \textit{MrkTest} equal
0 or 1, not 2.


\paragraph{Example :}
\input{exemple/buildfw.tex}

\paragraph{See also:}

\begin{itemize}
\item {\tt build} (\ref{build})
\item {\tt heaprinto} (\ref{heaprinto})
\item {\tt cgrobustness} (\ref{cgrobustness})
\end{itemize}
